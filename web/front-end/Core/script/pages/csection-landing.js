(function(window, document, undefined) {
    "use strict";
  
    const players = ['iframe[src*="youtube.com"]', 'iframe[src*="vimeo.com"]'];
  
    const fitVids = document.querySelectorAll(players.join(","));
  
    if (fitVids.length) {
      for (let i = 0; i < fitVids.length; i++) {
        const fitVid = fitVids[i];
        const width = fitVid.getAttribute("width");
        const height = fitVid.getAttribute("height");
        const aspectRatio = height / width;
        const parentDiv = fitVid.parentNode;
  
        const div = document.createElement("div");
        div.className = "vid-yt";
        div.style.paddingBottom = aspectRatio * 100 + "%";
        parentDiv.insertBefore(div, fitVid);
        fitVid.remove();
        div.appendChild(fitVid);
  
        fitVid.removeAttribute("height");
        fitVid.removeAttribute("width");
      }
    }
  })(window, document);

$(document).ready(function() {
    $('.playbutton > img').click(function(){
        $('#vid-thumb').hide()
        $('#vid-yt').show()
    });

    // Slide enter
    $('a#first-btn').click(function(){
        $('#first-page').fadeOut('fast', function() {
            $(this).remove();
        })
        $('#second-page').addClass('d-flex');
    })
    $('a#first-btn-mob').click(function(){
      $('#first-page').fadeOut('fast', function() {
          $(this).remove();
      })
      $('#second-page').addClass('d-flex');
  })

    // Slideshow
    var slideIndex = 1;
    showSlides(slideIndex);

    // Next/previous controls
    function plusSlides(n) {
      showSlides(slideIndex += n);
    }

    function showSlides(n) {
      var i;
      var slides = $(".slides");
      if (n > slides.length) {slideIndex = slides.length}
      if (n < 1) {slideIndex = slides.length}
      for (i = 0; i < slides.length; i++) {
          slides[i].classList.remove('d-flex')
      }
      slides[slideIndex-1].classList.add('d-flex');
      if (n == slides.length) {
        $('span.next').attr('disabled', 'disabled').addClass('next-disabled').removeClass('next').children('a').removeClass('next-slide');
        } else {
        $('span.next-disabled').addClass('next').removeClass('next-disabled').children('a').addClass('next-slide');
        }
    }
    $('.next').click(function () {
      plusSlides(1);
      window.scrollTo(0,0);
    });
    $('.prev').click(function () {
      plusSlides(-1);
      window.scrollTo(0,0);
    });
    $('#btn-1').click(function () {
      plusSlides(1);
      window.scrollTo(0,0);
    });
    $('.next-disabled').click(function () {
      return
    });

    // Popup article
    $('a.toggle-article').click(function(){
      $('body').addClass('noscroll');
      $('footer').hide();
      $('.popup-article').fadeIn('slow');
      $(window).trigger('resize');
    })
    $('a.toggle-close').click(function(){
      $('body').removeClass('noscroll')
      $('footer').show()
      $('.popup-article').fadeOut()
    });
});

