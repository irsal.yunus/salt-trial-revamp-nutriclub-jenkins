// expert spesific category team
$(".service__content-body-item").click(function(e){
    e.preventDefault();
    let targetHeading = $(this).find(".desc > h3").text().trim();
    let targetDescription = $(this).find(".desc > span").text().trim();
    let expertType = $(".service__content-head > h2").text().trim();

    postDataLayer(
        `${targetHeading}`,
        `Expert Team`,
        `step 1 - ${expertType}`,
        `${targetDescription}`,
    )
});
