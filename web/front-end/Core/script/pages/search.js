let intervalAtSearch = 60000;
let intervalAtPopular = 60000;
let intervalAtSearchPage = 30000;
$(".search__results-lists").hide(); // search results lists when it is not used
$(".live-search").keyup(event => {
    let typed = $(".live-search").val();
    let stagesVal = "xxx";
    let endpointUrl = `${ES_URL}/find?word=${typed}&stages=${stagesVal}&desktop=${IS_DESKTOP}`;
    let searchResult = $(".search__results-lists");

    if(event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40){
        let listItem = searchResult.find("li");
        let selected = listItem.filter(".search__results-selected");
        let current;
        if(event.keyCode != 40 && event.keyCode != 38) return;
		listItem.removeClass("search__results-selected");
	
			if(listItem) {
				if(event.keyCode == 40) {
					// Down key
					if(!selected.length || selected.is(":last-child")) {
						current = listItem.eq(0);
					} else {
						current = selected.next();
					}
				} else if(event.keyCode == 38 ) {
					// Up key
					if(!selected.length || selected.is(":first-child")) {
						current = listItem.last();
					} else {
						current = selected.prev();
					}
				}

				current.addClass("search__results-selected");
				$(".live-search").val(selected.text().trim());
			}
    }else{
        $.ajax({
            url : endpointUrl,
            cache : true,
            type : "GET",
            success : function(data) {
                console.log(data);
                if(typed){
                    if(data != undefined && data.article != undefined){
                        searchResult.empty();
                        searchResult.show();
                        data.article.map(article => {
                            searchResult.append(`
                            <li class="search__results-lists-item">
                                <a href="${article.url}">${article.title}</a>  
                                <p>${article.content}</p>
                            </li>
                            `);
                        });
                    }else{
                        searchResult.empty();
                        searchResult.show();
                        searchResult.append(`
                        <li class="search__results-lists-item">
                            Data Tidak Ditemukan 
                        </li>
                        `);     
                        
                    }
                }else{
                    searchResult.empty();
                    searchResult.hide();
                }
            },
            error : function (){
                searchResult.empty();
                searchResult.show();
                searchResult.append(`
                <li class="search__results-lists-item">
                    Terjadi Kesalahan Silahkan Coba Lagi
                </li>
                `);     
            }
        });
    }
});
// get history cookie value
let history__cookie = [];
if (localStorage.getItem("history") != null){
    history__cookie = JSON.parse(localStorage.getItem("history")); 
}
//  get popular cookie value 
let popular__cookie = [];
if (localStorage.getItem("popular") != null){
    popular__cookie = JSON.parse(localStorage.getItem("popular")); 
}


// looping for history tag
// if the length is null
if(history__cookie.length == 0 || history__cookie == null){
    // append no result
    $(".search__results-history").append(`
        <p>Riwayat Pencarian Tidak Ada</p>
    `);
}else{
    // append the result
    history__cookie.map(data => {
        $(".search__results-history").append(`
        <a href="#" class="search__results-history-tag btn">${data.value}</a>
    `);
});
}
// looping for popular tag and append the result
     popular__cookie.map(data => {
        $(".search__results-populer").append(`
            <a href="#" class="search__results-populer-tag btn">${data.value}</a>
        `);
    });
// show delete icons when form is filled
$("#q").keyup(function(){
    if($(this).val() != ""){
        $(".close-query-icon").show();
    }else{
        $(".close-query-icon").hide();
    }
});

// click the close-query-icon to clear form in the search form 
$(".close-query-icon").on("click", function(){
    $("#q").val("");
    $(this).hide();
});


$(document).on("click", ".search__results-history-tag.btn, .search__results-populer-tag.btn", function(){
    localStorage.setItem("searchKey", $(this).text());
    $("#q").val($(this).text());

    setTimeout(function(){
        document.getElementById("submit").click();
    },300);
    
    //window.location.href = "/search";
});


/* DATA LAYER SEARCH */
$(document).ready(function(){
    dataLayer.push({
        "event": "sitesearch",
        "search_query": localStorage.getItem("searchKey"),
        "search_url": window.location.pathname,
    });
});

// parsing data to html and store it to localStorage
function parseSearchHtml(searches, classname, data_name){
    let target = $(`.${classname}`);
    let html_search = "";
   
    for(let index in searches){
        html_search += `<a href="#" class="${classname}-tag btn mr-1">${searches[index].value}</a>`;
    }
    localStorage.removeItem(`${data_name}`);
    target.html(null);
    if(localStorage.getItem(`${data_name}`) === null){
        localStorage.setItem(`${data_name}`, JSON.stringify(searches));
        target.html(html_search);
    }else if(JSON.parse(localStorage.getItem(`${data_name}`)).length != 0){
        localStorage.removeItem(`${data_name}`);
        localStorage.setItem(`${data_name}`, JSON.stringify(searches));
        target.html(html_search);
    }
     /*
        for history only
        check if history length value is 0 the add no history text
    */
    if(JSON.parse(localStorage.getItem("history")).length == 0){
        $(".search__results-history").html(null);
        $(".search__results-history").html("<p>Riwayat Pencarian Tidak Ada</p>");
        $(".search__results-btn").hide();
    }else{
        $(".search__results-btn").show();
    }
}
// get search data
function updateSearchData(url, tag_target, data_target){
    fetch(url)
        .then(response => response.json())
        .then(data => {
            parseSearchHtml(data, tag_target, data_target);
        });
}

// load ajax for the first time
$(document).ready(function(){
    updateSearchData("/search/history", "search__results-history", "history");
    updateSearchData("/search/popular", "search__results-populer", "popular");
});

if(window.location.pathname == "/search"){
    setInterval(function(){
        updateSearchData("/search/popular", "search__results-populer", "popular");
    }, intervalAtSearchPage);
}else{
    var hitIntervalHistory = setInterval(function(){
        updateSearchData("/search/history", "search__results-history", "history");
    }, intervalAtSearch);

    setInterval(function(){
        updateSearchData("/search/popular", "search__results-populer", "popular");
    }, intervalAtPopular);
}

// remove the existing popular localStorage value so the function above can be overwritten
localStorage.removeItem("popular");

$("a[data-toggle=\"pill\"]").on("shown.bs.tab", function (e) { 
    let target = e.target.attributes[5].value;
    let relatedTarget = e.relatedTarget.attributes[5].value;
    $(`.${target}`).addClass("show active");
    $(`.${relatedTarget}`).removeClass("show active");
  });

