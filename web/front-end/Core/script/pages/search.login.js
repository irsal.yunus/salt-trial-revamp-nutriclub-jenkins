$(document).ready(function() {
    // show modal confirm
    $(".search__results-btn").click(function() {
        // show modal confirm for deleting cookie
        $("#confirm-delete-modal").modal();
    });

    // remove button delete when there is not cookie
    if(localStorage.getItem("history") == null || JSON.parse(localStorage.getItem("history")).length == 0){
        // hide button if there is not cookie
        $(".search__results-btn").hide();
    }else{
        // show button when there is cookie
        $(".search__results-btn").show();
    }

    // trigger button for delete for delete cookie
    $(".modal__confirm-delete").click(function(){  
        // deleting localStorage
        localStorage.setItem("history", JSON.stringify([]));
        
        // declare url for ajax delete elastic search
        let baseUrl = "/search/history";

        // clearInterval(hitIntervalPopular);
        clearInterval(hitIntervalHistory);
        // if statement for deleting cookie and ajax for a deleting elastic
            $.ajax({
                url : baseUrl,
                type : "DELETE",
                // success status
                success : function(){            
                        $("#confirm-delete-modal").modal("hide");
                        $(".search__results-history").empty().append("<p>Riwayat Pencarian Tidak Ada</p>");
                        $(".search__results-btn").hide();
                    // hide modal for a moment
                    console.log("data berhasil terhapus")
                },
                // error status
                error : function(error){
                    console.log("data tidak berhasil terhapus", error);
                }
            });
        
    });
});
