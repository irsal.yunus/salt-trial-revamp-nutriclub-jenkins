// remove last item border bottom color
$(".card-column-big__content").last().css("border-bottom", "none");

$(document).ready(function(){
    $(".tool-list__wrapper").slick({
        slidesToShow: 4,
        infinite: false,
        arrows: false,
        variableWidth : true,
        dots: true,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3
              }
            },
            {
                breakpoint: 500,
                settings: {
                  slidesToShow: 3
                }
            },
            {
                breakpoint: 320,
                settings: {
                  slidesToShow: 2
                }
            }
          ]
    });
        
    $(".card-slider-one__wrapper").slick({
        slidesToShow: 4,
        infinite: true,
        arrows: true,
        dots: true,
        responsive: [
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 2
              }
            },
            {
                breakpoint: 320,
                settings: {
                  slidesToShow: 1
                }
            }
          ]
    });
        
    $(".card-slider-three__wrapper").slick({
        infinite: true,
        slidesToShow: 4,
        arrows: true,
        dots: true,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3
              }
            },
            {
                breakpoint: 500,
                settings: {
                  slidesToShow: 3
                }
            },
            {
                breakpoint: 320,
                settings: {
                  slidesToShow: 2
                }
            }
          ]
    });

});