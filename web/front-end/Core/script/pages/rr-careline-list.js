var tanggal = "",
    hari = "",
    jam = "",
    catalogueId="";

$(document).ready(function(){
    $(".date-list li:not(.disabled)").on("click", function(){
        tanggal = "";
        hari = "";
        jam = "";
        catalogueId="";
        $(".date-list li").each(function(){
            $(this).removeClass("selected");
        });
        $(this).addClass("selected");
        hari = $(this).children('.day').attr("value");
        tanggal = $(this).children('.date').attr("value");
        setActiveTime($(this).children(".date").attr("data-date"));
        getTimeList($(this).children(".date").attr("data-date"));
        checkVal();
    });
});

$(document).on('click', '.time-list li:not(.disabled)', function() {
    $(".time-list li").each(function(){
        $(this).removeClass("selected");
    });
    $(this).addClass("selected");
    jam = $(this).text();
    catalogueId = $(this).attr("data-catalogueId");
    checkVal();
});

function getTimeDate(){
    $("#main-register .day").text(hari);
    $("#main-register .date").text(tanggal);
    $("#main-register .time").text(jam);
}

function checkVal(){
    if(tanggal != "" && jam != ""){
        $(".register-button").removeClass("disabled");
        $(".register-button").removeAttr("disabled");
    }
    else {
        $(".register-button").addClass("disabled");
        $(".register-button").attr("disabled","disabled");
    }
}