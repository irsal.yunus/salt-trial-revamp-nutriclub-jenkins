$(document).ready(function() {
    $('.article-items').slick({
      infinite: true,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: false,
      variableWidth: true,
      dots: true,
      responsive: [
        {
            breakpoint: 768,
            settings: "unslick"
        }
    ]
    });
});