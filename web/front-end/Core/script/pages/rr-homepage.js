$(document).ready(function() {
    $('.slidable-hero-banner').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
    });
    
    var expert_card_count = $('.expert-card').length;

    if (expert_card_count > 4){
        $('.expert-card-slider').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: true,
            responsive: [
                {
                  breakpoint: 768,
                  settings: "unslick"
                }
            ]
        });
    }

    // var windowWidth = $(window).width();
    // $(".banner-image").each(function() {
    //     if (windowWidth < 767){
    //         var mobileImg = $(this).attr("bg-mobile");
    //         $(this).css('background-image', 'url(' +mobileImg + ')');
    //     }
    //     else{
    //         var desktopImg = $(this).attr("bg-desktop");
    //         $(this).css('background-image', 'url(' +desktopImg + ')');
    //     }
    // });

    $('.expert-card-slider').each(function(){  
        var highestBox = 0;
        
        $('.expert-card', this).each(function(){
            if($(this).height() > highestBox) {
                highestBox = $(this).height(); 
            }
        });  
              
        $('.expert-card',this).height(highestBox);
    }); 
});