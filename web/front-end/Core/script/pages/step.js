var step = new Vue({
    el: "#StepIntances",
    data: {
        location: "",
        grade: "",
        gradeByLocation: "",
        wizard: [],
        wizardID: "",
        isLoading: false,
        nextWizard: null,
        prevWizard: null,
        checkBtn: false,
        checkLevel: "",
        selectCty: 0,
        selectGrd: 0,
        selectedPln: null,
        selectPrev: null,
        additional_cost: {},
        setNull: false,
        main_cost: {},
        checkSelect: false,
        alertstep: false,
        isRq: false,
        shareImg: null,
        shareUrlFacebook: "https://www.facebook.com/sharer/sharer.php?u=",
        shareUrlTwitter: "https://twitter.com/intent/tweet?url=",
        shareUrlWa: "whatsapp://send?text=",
        selectedPlnDana: null,
        resultDataPlan: [],
        resultPDF: null,
    },

    created: function () {
        this.getLocation();
        this.getLavel();
    },
    methods: {
        getCostFirst: function () {
            this.isLoading = true;
            fetch(
                `/resilient-parental-calculator/cost?locationId=${this.selectCty}&gradeId=${this.selectGrd}&levelId=${this.wizard[0].id}`,
                {
                    method: "GET", // or 'PUT'
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    this.main_cost = data.data.main_cost.cost;
                    this.additional_cost = data.data.additional_cost;
                    this.wizardID = this.wizard[0].id;
                    this.isLoading = false;
                })
                .catch((error) => {
                    console.error("Error:", error);
                });
        },
        getCost: function (wizardStep) {
            this.isLoading = true;
            fetch(
                `/resilient-parental-calculator/cost?locationId=${this.selectCty}&gradeId=${this.selectGrd}&levelId=${wizardStep}`,
                {
                    method: "GET", // or 'PUT'
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    this.main_cost = data.data.main_cost.cost;
                    this.additional_cost = data.data.additional_cost;
                    this.isLoading = false;
                    this.alertstep = false;
                })
                .catch((error) => {
                    console.error("Error:", error);
                    this.isLoading = false;
                    this.alertstep = true;
                    this.main_cost = null;
                });
        },
        getLocation: function () {
            $.ajax({
                method: "GET",
                url: "/resilient-parental-calculator/location/",
                headers: {
                    "content-type": "application/json",
                },
            }).done(
                function (data) {
                    this.location = data.data;
                }.bind(this)
            );
        },
        getGRadeOnPrev: function () {
            $.ajax({
                method: "GET",
                url: `/resilient-parental-calculator/location/${this.selectCty}?level_code=${this.prevWizard}`,
                headers: {
                    "content-type": "application/json",
                },
            }).done(
                function (data) {
                    this.gradeByLocation = data.data.school_grades;
                }.bind(this)
            );
        },
        getGradeOnNext: function () {
            $.ajax({
                method: "GET",
                url: `/resilient-parental-calculator/location/${this.selectCty}?level_code=${this.nextWizard}`,
                headers: {
                    "content-type": "application/json",
                },
            }).done(
                function (data) {
                    this.gradeByLocation = data.data.school_grades;
                    if (this.nextWizard == "KULIAH") {
                        var dataLocation = this.location.find(
                            ({ code }) => code === "JKT"
                        );
                        var dataGrade = this.gradeByLocation.find(
                            ({ name }) => name == "Swasta"
                        );
                        this.selectGrd = dataGrade.id;
                        this.selectCty = dataLocation.id;
                        this.getCost(this.checkLevel);
                    }
                }.bind(this)
            );
        },
        getGradeByLocation: function (event) {
            if (this.checkBtn) {
                if (this.nextWizard == "TK") {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=TK`;
                } else if (this.nextWizard == "SD") {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=SD`;
                } else if (this.nextWizard == "SMP") {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=SMP`;
                } else if (this.nextWizard == "SMA") {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=SMA`;
                } else if (this.nextWizard == "KULIAH") {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=KULIAH`;
                } else {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=KB`;
                }
            } else {
                if (this.prevWizard == "KB") {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=KB`;
                } else if (this.prevWizard == "TK") {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=TK`;
                } else if (this.prevWizard == "SD") {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=SD`;
                } else if (this.prevWizard == "SMP") {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=SMP`;
                } else if (this.prevWizard == "SMA") {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=SMA`;
                } else {
                    var urlGrade = `/resilient-parental-calculator/location/${event.target.value}?level_code=KB`;
                }
            }

            $.ajax({
                method: "GET",
                url: urlGrade,
                headers: {
                    "content-type": "application/json",
                },
            }).done(
                function (data) {
                    this.gradeByLocation = data.data.school_grades;
                }.bind(this)
            );
        },
        storeResult: function (id, dataResult) {
            fetch(
                `/resilient-parental-calculator/disclaimer/result?transaction_id=${id}`,
                {
                    method: "POST", // or 'PUT'
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({ costs: dataResult }),
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    this.resultPDF = data.data;
                    this.shareImg = encodeURIComponent(data.data.share_file);
                 
                })
                .catch((error) => {
                    console.error("Error:", error);
                });
        },
        resetGrd: function () {
            this.selectGrd = "0";
        },
        getLavel: function () {
            $.ajax({
                method: "GET",
                url: "/resilient-parental-calculator/level/",
                headers: {
                    "content-type": "application/json",
                },
            }).done(
                function (data) {
                    this.wizard = data.data;
                }.bind(this)
            );
        },
        next: function (event) {
            var current = event.target.parentElement;
            var next = event.target.parentElement.nextElementSibling;
            //Add Class Active
            $("#progressbar li")
                .eq($("fieldset").index(next))
                .addClass("active");
            $("#progressbar li")
                .eq($("fieldset").index(current))
                .addClass("active");
            //show the next fieldset
            next.style.display = "block";
            next.style.opacity = "1";
            //hide current fieldset
            current.style.display = "none";
            current.style.position = "relative";
            current.style.opacity = "0";

            this.selectPrev = {
                location_id: this.selectCty,
                grade_id: this.selectGrd,
            };

            var nextArray = parseInt(event.target.parentElement.id) + 1;
            var wizard = this.wizard[nextArray];
            var getIDAttribute = event.target.parentElement.getAttribute(
                "stepid"
            );
            var checkStepDefault = event.target.parentElement.getAttribute(
                "stepcode"
            );
            var checkStep = event.target.parentElement.nextElementSibling.getAttribute(
                "stepcode"
            );
            var checkLevel = event.target.parentElement.nextElementSibling.getAttribute(
                "stepid"
            );

            this.nextWizard = checkStep;
            this.checkLevel= checkLevel;
            if (checkStep == "SMP") {
                if (this.selectedPln != null) {
                    this.isRq = true;
                    this.checkSelect = true;
                }
            }
            if (checkStep == "SMA") {
                // if (this.isRq) {
                //     this.checkSelect = true;
                // } else {
                //     this.checkSelect = false;
                // }
                if (this.selectedPln != null) {
                    this.isRq = true;
                    this.checkSelect = true;
                }
            }
            if (checkStep == "KULIAH") {
                if (this.isRq) {
                    this.checkSelect = false;
                } else {
                    this.checkSelect = false;
                }

                this.main_cost = null;
            }
            if (checkStepDefault === "KB") {
                var std = 2;
                this.main_cost = null;
                this.getCost(wizard.id);

                var saveData = {
                    level_id: parseInt(getIDAttribute),
                    grade_id: this.selectGrd,
                    location_id: this.selectCty,
                    total_study_year: std,
                    main_cost: this.selectedPln,
                    additional_cost: this.selectedPlnDana,
                };
                if (this.selectedPlnDana != null) {
                    this.selectedPlnDana = null;
                }
                if (this.selectedPln != null) {
                    this.resultDataPlan.push(saveData);
                    this.selectedPln = null;
                    this.selectedPlnDana = null;
                }
            }

            if (checkStepDefault == "TK") {
                var std = 2;
                this.main_cost = null;

                this.getCost(wizard.id);

                var saveData = {
                    level_id: parseInt(getIDAttribute),
                    grade_id: this.selectGrd,
                    location_id: this.selectCty,
                    total_study_year: std,
                    main_cost: this.selectedPln,
                    additional_cost: this.selectedPlnDana,
                };
                if (this.selectedPlnDana != null) {
                    this.selectedPlnDana = null;
                }
                if (this.selectedPln != null) {
                    this.resultDataPlan.push(saveData);
                    this.selectedPln = null;
                    this.selectedPlnDana = null;
                }
            }
            if (checkStepDefault === "SD") {
                var std = 6;
                this.main_cost = null;

                this.getCost(wizard.id);

                var saveData = {
                    level_id: parseInt(getIDAttribute),
                    grade_id: this.selectGrd,
                    location_id: this.selectCty,
                    total_study_year: std,
                    main_cost: this.selectedPln,
                    additional_cost: this.selectedPlnDana,
                };
                if (this.selectedPlnDana != null) {
                    this.selectedPlnDana = null;
                }
                if (this.selectedPln != null) {
                    this.resultDataPlan.push(saveData);
                    this.selectedPln = null;
                    this.selectedPlnDana = null;
                }
            }

            if (checkStepDefault === "SMP") {
                var std = 3;
                this.main_cost = null;

                this.getCost(wizard.id);

                var saveData = {
                    level_id: parseInt(getIDAttribute),
                    grade_id: this.selectGrd,
                    location_id: this.selectCty,
                    total_study_year: std,
                    main_cost: this.selectedPln,
                    additional_cost: this.selectedPlnDana,
                };
                if (this.selectedPlnDana != null) {
                    this.selectedPlnDana = null;
                }
                if (this.selectedPln != null) {
                    this.resultDataPlan.push(saveData);
                    this.selectedPln = null;
                    this.selectedPlnDana = null;
                }
            }
            if (checkStepDefault == "SMA") {
                var std = 3;
                this.main_cost = null;
                this.setNull = true;
                this.getCost(wizard.id);

                var saveData = {
                    level_id: parseInt(getIDAttribute),
                    grade_id: this.selectGrd,
                    location_id: this.selectCty,
                    total_study_year: std,
                    main_cost: this.selectedPln,
                    additional_cost: this.selectedPlnDana,
                };
                if (this.selectedPlnDana != null) {
                    this.selectedPlnDana = null;
                }
                if (this.selectedPln != null) {
                    this.resultDataPlan.push(saveData);
                    console.log(this.resultDataPlan);
                    this.selectedPln = null;
                    this.selectedPlnDana = null;
                }
            }

            if (checkStepDefault == "KULIAH") {
                var std = 4;
                var saveData = {
                    level_id: parseInt(getIDAttribute),
                    grade_id: this.selectGrd,
                    location_id: this.selectCty,
                    total_study_year: std,
                    main_cost: this.selectedPln,
                    additional_cost: this.selectedPlnDana,
                };
                if (this.selectedPln != null) {
                    this.resultDataPlan.push(saveData);
                    console.log(this.resultDataPlan);
                }

                var seen = new Set();
                var filteredArr = this.resultDataPlan.filter((el) => {
                    var duplicate = seen.has(el.level_id);
                    seen.add(el.level_id);
                    return !duplicate;
                });
                var cookieValue = document.cookie
                    .split("; ")
                    .find((row) => row.startsWith("rpc_transaction_id"))
                    .split("=")[1];
                setTimeout(this.storeResult(cookieValue, filteredArr), 3000);
            }
            document.documentElement.scrollTop = 0;
            this.getGradeOnNext();
            this.checkBtn = true;
        },
        previous: function (event) {
            var current_fs = event.target.parentElement;
            var previous_fs = event.target.parentElement.previousElementSibling;
            var prev_id = event.target.parentElement.previousElementSibling.getAttribute(
                "stepid"
            );
            var checkStep = event.target.parentElement.previousElementSibling.getAttribute(
                "stepcode"
            );
            document.documentElement.scrollTop = 0;
            this.prevWizard = checkStep;
            this.resultDataPlan.splice(
                this.resultDataPlan.findIndex((v) => v.id === prev_id),
                1
            );
            if (checkStep == "TK") {
                if (this.isRq) {
                    this.checkSelect = false;
                }
            }
            if (checkStep == "SD") {
                if (this.isRq) {
                    this.checkSelect = false;
                }
            }
            if (checkStep == "SMP") {
                if (this.isRq) {
                    this.checkSelect = false;
                }
            }
            //Remove class active
            $("#progressbar li")
                .eq($("fieldset").index(current_fs))
                .removeClass("active");

            //show the previous fieldset
            previous_fs.style.display = "block";
            previous_fs.style.opacity = "1";

            //hide the current fieldset with style
            current_fs.style.display = "none";
            current_fs.style.position = "relative";
            current_fs.style.opacity = "1";

            this.selectedPln = null;
            this.selectedPlnDana = null;

            this.selectCty = this.selectPrev.location_id;
            this.selectGrd = this.selectPrev.grade_id;

            var prevArray = parseInt(event.target.parentElement.id) - 1;
            var wizard = this.wizard[prevArray];
            this.getCost(wizard.id);
            this.wizardID = wizard.id;
            this.checkBtn = false;
            this.getGRadeOnPrev();
            this.setNull = false;
        },
        selectedPlan: function (id) {
            if (this.isRq == true) {
                if (this.nextWizard == "SMP") {
                    this.checkSelect = false;
                }
                if (this.nextWizard == "SMA") {
                    this.checkSelect = false;
                }
                if (this.nextWizard == "KULIAH") {
                    this.checkSelect = false;
                }
            }

            this.selectedPln = id;
        },
        selectedPlanDana: function (id) {
            this.selectedPlnDana = id;
        },
        convetRp: function (num) {
            var rupiah = "";
            var angkarev = num
                ? num.toString().split("").reverse().join("")
                : 0;
            for (var i = 0; i < angkarev.length; i++)
                if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + ".";
            return (
                "Rp. " +
                rupiah
                    .split("", rupiah.length - 1)
                    .reverse()
                    .join("") +
                ",-"
            );
        },
    },
});
