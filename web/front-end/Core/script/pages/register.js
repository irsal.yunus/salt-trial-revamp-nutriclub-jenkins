
var baseURL = $('#baseURL').val();
function changeDateStripToSlash(value){
    if(!Modernizr.inputtypes.date) return;
    
    let valueSplit = value.split("-").reverse().join("/");
    $("#ChildBirthDate").val(valueSplit);
    return valueSplit;
}

function changeDate(event){
    let date_value = event.target.value;
    if(date_value != ""){
        changeDateStripToSlash(date_value);
        $("#ChildBirthDateX").addClass("active");
        $("#ChildBirthPlaceHolder").hide();
    }
    else {
        $("#ChildBirthDateX").removeClass("active");
        $("#ChildBirthPlaceHolder").show();
    }  
}

//Set ChildBirthDateX into today maximum 
if($("#ChildBirthDateX").length > 0){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10){
            dd="0"+dd;
        } 
        if(mm<10){
            mm="0"+mm;
        } 

    today = yyyy+"-"+mm+"-"+dd;
    document.getElementById("ChildBirthDateX").setAttribute("max", today);
}

$(document).ready(function() {
  $(".show-hamil, .show-anak").removeClass("hidden-item");
  $(".btn-controller").click(function() {
      
      if (!$(this).attr("disabled")) {
          var _href = $(this).data("href");
          $("button").parents(_href).find(".btn-disabled").attr("disabled", "disabled");
          $(_href).find(".custom-radio__input").prop("checked", false);
          $(_href).find(".custom-radio").removeClass("active");
          console.log(_href);
          if (_href == "#step-4") {
              $("#triggerMisscall").attr("disabled", false);
              validateForm(_href);
          } else {
              $(".registration__content").hide();
              $(_href).show();
              $(window).scrollTop(0);
          }
      } else {
          console.log("button is disabled");
      }
  });

  $(".number-only").on("keyup", function(event) {
        console.log("key prss validate2 ");
        var regex = new RegExp("^[0-9]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
  });

  function validatePhoneNumber(phone) {
    //   var regexPhoneNumber = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]{10,13}$/g
    //   var regexPhoneNumber = /^[a-zA-Z0-9]{8,16}$/g
    var regexPhoneNumber = /^(^\+62\s?|^0)(\d{3,4}-?){2}\d{3,4}$/g;
      if (!regexPhoneNumber.test(String(phone).toLowerCase())) {
          return false;
      } else {
          return true;
      }
  }

  function validateEmail(email) {
    console.log(email);
      var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!regexEmail.test(String(email).toLowerCase())) {
          return false;
      } else {
          return true;
      }
  }


  function validateName(name) {
      var regexName = /^[a-zA-Z ]+$/;
      if (!regexName.test(String(name).toLowerCase())) {
          return false;
      } else {
          return true;
      }
  }
  

  function validateAgePregnant(age){
    var regexName = /^([1-9]|[1-3][0-9]|4[0-2])$/;
    if (regexName.test(String(age).toLowerCase())) {
        return true;
    } else {
        return false;
    }
  }


  function validateForm(param) {
      var countError = 0;
      var stagesID = parseInt($("[name=\"StagesID\"]:checked").val());
      console.log("----stagesID----");
      console.log(stagesID);
      if(stagesID == 7){
        //Hamil
        if (!validateAgePregnant($("#AgePregnant").val())) {
            $("#AgePregnant").addClass("is-invalid");
            $("#AgePregnant").parent().find(".invalid-feedback").text("Mohon isi usia kehamilan antara 1-42 minggu");
            console.log("7-1");
            countError++;
        } else {
            $("#AgePregnant").removeClass("is-invalid");
        }
      }
      else if(stagesID == 8){
        //Belum Hamil 
        if($("[name=\"rencana\"]:checked").length == 0){
            $("#RencanaKehamilan").addClass("is-invalid");
            $("#RencanaKehamilan").parent().find(".invalid-feedback").text("Mohon pilih rencana kehamilan");
            console.log("8-1");
            countError++;
        }
        else {
            $("#RencanaKehamilan").removeClass("is-invalid");
        }

      }
      else if(stagesID == 9){
        //Hamil & Punya Anak
        if (!validateAgePregnant($("#AgePregnant").val())) {
            $("#AgePregnant").addClass("is-invalid");
            $("#AgePregnant").parent().find(".invalid-feedback").text("Mohon isi usia kehamilan antara 1-42 minggu");
            console.log("9-1");
            countError++;
        } else {
            $("#AgePregnant").removeClass("is-invalid");
        }

        if ($("#ChildBirthDateX").val() === "") {
            $("#ChildBirthDateX").addClass("is-invalid");
            $("#ChildBirthDateX").parent().find(".invalid-feedback").text("Mohon isi tanggal lahir anak");
            console.log("9-2");
            countError++;
        } else {
            $("#ChildBirthDateX").removeClass("is-invalid");
        }
        if($("#ChildName").val() == ""){
            $("#ChildName").addClass("is-invalid");
            $("#ChildName").parent().find(".invalid-feedback").text("Mohon isi nama anak");
            console.log("9-3a");
            countError++;
        }else if (!validateName($("#ChildName").val())) {
            $("#ChildName").addClass("is-invalid");
            $("#ChildName").parent().find(".invalid-feedback").text("Nama Anak Tidak Valid");
            console.log("9-3b");
            countError++;
        } else {
            $("#ChildName").removeClass("is-invalid");
        }
        
      }
      else {
        //Anak only
        if ($("#ChildBirthDateX").val() === "") {
            $("#ChildBirthDateX").addClass("is-invalid");
            $("#ChildBirthDateX").parent().find(".invalid-feedback").text("Mohon isi tanggal lahir anak");
            console.log("10-1");
            countError++;
        } else {
            $("#ChildBirthDateX").removeClass("is-invalid");
        }
        if (!validateName($("#ChildName").val())) {
            $("#ChildName").addClass("is-invalid");
            $("#ChildName").parent().find(".invalid-feedback").text("Mohon isi nama anak");
            console.log("10-2");
            countError++;
        } else {
            $("#ChildName").removeClass("is-invalid");
        }
        //Belum Hamil 
        if($("[name=\"rencana\"]:checked").length == 0){
            $("#RencanaKehamilan").addClass("is-invalid");
            $("#RencanaKehamilan").parent().find(".invalid-feedback").text("Mohon pilih rencana kehamilan");
            console.log("8-1");
            countError++;
        }
        else {
            $("#RencanaKehamilan").removeClass("is-invalid");
        }
      } 
        if($("#Fullname").val() == ""){
            $("#Fullname").addClass("is-invalid");
            $("#Fullname").parent().find(".invalid-feedback").text("Mohon isi nama lengkap");
            console.log("a-2a");
            countError++;
        }else if (!validateName($("#Fullname").val())) {
                $("#Fullname").addClass("is-invalid");
                $("#Fullname").parent().find(".invalid-feedback").text("Nama lengkap tidak valid");
                console.log("a-2b");
                countError++;
            } else {
                $("#Fullname").removeClass("is-invalid");
            }
        if ($("#Email").val() == "") {
            $("#Email").addClass("is-invalid");
            $("#Email").parent().find(".invalid-feedback").text("Mohon isi alamat email");
            console.log("a-3a");
            countError++;
        } else if (!validateEmail($("#Email").val())) {
            $("#Email").addClass("is-invalid");
            $("#Email").parent().find(".invalid-feedback").text("Alamat email tidak valid");
            console.log("a-3b");
            countError++;
        } else {
            $("#Email").removeClass("is-invalid");
        }

        if ($("#Phone").val() == "") {
            $("#Phone").addClass("is-invalid");
            $("#Phone").parent().find(".invalid-feedback").text("Mohon isi nomor handphone");
            console.log("a-4a");
            countError++;
        } else if (!validatePhoneNumber($("#Phone").val())) {
            $("#Phone").addClass("is-invalid");
            $("#Phone").parent().find(".invalid-feedback").text("Nomor handphone tidak valid");
            console.log("a-4b");
            countError++;
        } else {
            $("#Phone").removeClass("is-invalid");
        }
        if ($("#Password").val() === "") {
            var getParent =  $("#Password").parent(".position-relative");
            getParent.siblings(".invalid-feedback").text("Mohon isi password");
            $(".invalid-feedback").addClass("d-block");
            $("#Password").addClass("is-invalid");
            console.log("a-5a");
            countError++;
        } else if($("#Password").val().length < 6) {
            var getParent =  $("#Password").parent(".position-relative");
            getParent.siblings(".invalid-feedback").text("Karakter Password Minimal 6");
            $(".invalid-feedback").addClass("d-block");
            $("#Password").addClass("is-invalid");
            console.log("a-5b");
            countError++;
        }else {
            var getParent =  $("#Password").parent(".position-relative");
            getParent.siblings(".invalid-feedback").empty();
            getParent.siblings(".invalid-feedback").removeClass("d-block");
            getParent.siblings(".invalid-feedback").removeClass("is-invalid");
        }
        if ($("#ConfirmPassword").val() === "") {
            var getParent = $("#ConfirmPassword").parent(".position-relative");
            getParent.siblings(".invalid-feedback").text("Mohon isi konfirmasi password");
            $("#ConfirmPassword").addClass("is-invalid");
            $(".invalid-feedback").addClass("d-block");
            console.log("a-6");
            countError++;
        } else if ($("#ConfirmPassword").val() != $("#Password").val()) {
            var getParent =  $("#ConfirmPassword").parent(".position-relative");
            getParent.siblings(".invalid-feedback").text("Konfirmasi password harus sama dengan password");
            $("#ConfirmPassword").addClass("is-invalid");
            console.log("a-7");
            $(".invalid-feedback").addClass("d-block");
            countError++;
        }
        else {
            var getParent =  $("#ConfirmPassword").parent(".position-relative");
            getParent.siblings(".invalid-feedback").removeClass("is-invalid");
            getParent.siblings(".invalid-feedback").removeClass("d-block");
            
        }

        if($("#IsAgree:checked").length == 0){
            $("#IsAgree").parents(".form-check").find(".invalid-feedback").text("Anda harus setuju dengan syarat dan ketentuan");
            $("#IsAgree").parents(".form-control").addClass("is-invalid");
            console.log("a-8");
            countError++;
        }

        $("#IsAgree").change(function(){
            if(this.checked){
                $(this).parents(".form-control").removeClass("is-invalid");
                $(this).parents(".form-check").find(".invalid-feedback").empty();
            }else{
                $(this).parents(".form-control").addClass("is-invalid");
                $(this).parents(".form-check").find(".invalid-feedback").text("Anda harus setuju dengan syarat dan ketentuan");
            }
        });

        $("#RencanaKehamilan").change(function(){
            if(!this.checked){
                $(".no-border").removeClass("is-invalid");
                $(this).parents(".hidden-item").find(".invalid-feedback").empty();
            }
        });


      console.log("countError -------" + countError);

      if(countError > 0){
        console.log("masih ada error");
        dataLayer.push({
          "event": "formsubmission",
          "form_name": "registration form",
          "form_step": "Step 3 - Data Diri",
          "form_reference_id": "",
          "submit_status": "error"
        });
        // $('.registration__scroll').animate({
        //     scrollTop: $('.is-invalid:visible:first').offset().top
        // }, 500);
      }
      else {
        console.log("sudah tidak ada error");
        $(".registration__content").hide();
        $(param).show();
        dataLayer.push({
          "event": "formsubmission",
          "form_name": "registration form",
          "form_step": "Step 3 - Data Diri",
          "form_reference_id": "",
          "submit_status": "success"
        });
      }

      // check if checkbox already checked then remove the is-invalid warning
  }
  $(".custom-radio__input").change(function() {
      var getActive = $(this).parent(".custom-radio");
      var getTarget = getActive.parent("div").siblings("div");
      if ($(this).is(":checked")) {
            getTarget.children(".custom-radio").removeClass("active");
          $(this).parent().addClass("active");
          $(this).parents(".registration__content").find(".btn-controller").attr("disabled", false);
      } else {
          console.log("unselected");
      }
  });

  $(".gender").change(function() {
      if ($(this).is(":checked")) {
          var _gender_text = $(this).data("text");
          $(".heading-gender").text(_gender_text);
      }
  });

  $(".kondisi-ibu").change(function() {
      console.log("change");
      if ($(this).is(":checked")) {
          var _show = $(this).data("show");
          $(".show-hamil, .show-anak, .show-belum").addClass("hidden-item");
          $(".show-hamil, .show-anak, .show-belum").hide();
          if (_show != "") {
              $(_show).show();
          }
      }
  });

  $(".registration__content input").on("keyup, change",function(){
      $(this).parent().find(".invalid-feedback").empty();
      $(this).removeClass("is-invalid");
  });
//   countdown function

 
$(".btn-miscall").click(function(){
    $("#triggerMisscall").attr("disabled", true);
    $.ajax({
        url: baseURL + '/api/OTPMember/RequestMisscallRegister?Phone=' + $('.phone-verify').val(),
        type: "POST",
        dataType: "JSON",
        success: function(data){
           console.log(data);

           if(data.StatusCode == "00"){
                var fiveMinutes = 60 * 2;
                startTimer(fiveMinutes);
               console.log("success 00");
               if(!$(this).hasClass("retry")){
                    $(".registration__content").hide();
                    $("#step-5").show();
               }
           }
           else {
               alert(data.StatusMessage);
           }

        }
    });
  });

  $(".btn-re-miscall").click(function(e){
      e.preventDefault();
    $(".retry").attr("disabled", true);
    $.ajax({
        url: baseURL + '/api/OTPMember/RequestMisscallRegister?Phone=' + $('.phone-verify').val(),
        type: "POST",
        dataType: "JSON",
        success: function(data){
           console.log(data);
           if(data.StatusCode == "00"){
               console.log("btn-retry");
                var fiveMinutes = 60 * 2;
               startTimer(fiveMinutes);
               console.log("success 00");
               
           }
           else {
               alert(data.StatusMessage);
           }

        }
    });
  });

  $(".otp-item").keyup(function(){
      if($("#otp-1").val() !== "" && $("#otp-2").val() !== "" && $("#otp-3").val() !== "" && $("#otp-4").val() !== "") {
        $(".btn-verifikasi").attr("disabled", false);
      } else{
        $(".btn-verifikasi").attr("disabled", true);
      }
  });
  $(".btn-verifikasi").click(function(){
    var _verif = $("#otp-1").val() + "" + $("#otp-2").val() + "" + $("#otp-3").val() + "" + $("#otp-4").val();
    console.log(_verif);

    $.ajax({
        url: baseURL + '/api/OTPMember/VerifyMisscallRegister?Phone=' + $('.phone-verify').val() + "&Token=" + _verif,
        type: "POST",
        dataType: "JSON",
        success: function(data){
           console.log(data);
           if(data.StatusCode == "00"){
               console.log("success 00");
               $("form[name=\"registration_form\"]").submit();
           }
           else {
               alert(data.StatusMessage);
           }
        }
    });        
  });

  $(".otp-item").on("keyup", function(event){
        if(event.keyCode !== 8 && event.keyCode !== 37 && event.keyCode !== 38 && event.keyCode !== 39 && event.keyCode !== 40){
            $(this).next().focus();
        }else{
            $(this).prev();
        }
  });
  $(".btn-eye-password").click(function(e){
    e.preventDefault();
    var target = $(this).siblings("input");
    var type = target.attr("type");
    $(this).toggleClass("active");
    if(type == "password"){
        target.attr("type", "text");
    } else{
        target.attr("type", "password");
    }
  });

});

$(document).ready(function() {
    $(".input-otp").keyup(function() {
        var inputVal = $(this).val();
        var characterReg = /^\d{1}$/;
        if (characterReg.test(inputVal)) {
            $(this).next().focus();
        }
    });
  });

  $(".gender").on("change", function(){
    localStorage.setItem("gender", $(this).val());
    setGender();
});


$("#Phone").on("keyup", function(){
    $("#phone-verify").val($(this).val());
});

function setGender(){
    var _gender = localStorage.getItem("gender");
    if(_gender == "M"){
        $("#kondisi-gender").text("Istri Anda");
    }
    else {
        $("#kondisi-gender").text("Anda");
    }
}

$("#btn-back-step-one").click(function(){
$("#step-2 .custom-radio").removeClass("active");
});
// clear all form when back to step 2 
$("#btn-back-step").click(function(){
    var target = $("#step-3").find(".form-control");
    target.val("");
    target.removeClass("is-invalid");
    $(".invalid-feedback").empty();
    $("#IsAgree").prop("checked", false);
    $("#confirm-no").prop("checked", false);
    $("#confirm-yes").prop("checked", false);
});

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    var myVar = setInterval(myTimer, 1000);
    function myTimer (){
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        console.log(minutes + ":" + seconds);
        $("#countdown").text(minutes + ":" + seconds);
        // display.textContent = minutes + ":" + seconds;
        if (--timer < 30) {
            $("#countdown").addClass("text-danger");
            if(timer < 0){
                timer = 0;
                $(".retry").attr("disabled", false);
                // $(".retry").prop('disabled', true); 
                clearInterval(myVar);
            }
        } else{
            $("#countdown").removeClass("text-danger");
        }
    }    
}

// $('#AgePregnant').keyup(function(){
//     console.log(isNaN($(this).val()))
// })