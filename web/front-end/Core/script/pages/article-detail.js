//check if hit point api
let isHit = false;
function storePoints(){
    if(IS_LOGGED_IN === "true"){
        let article_token = $("#article_token").val();
        let isThereToken = localStorage.getItem("article_token");
        if(isThereToken !== null){
            console.log("token is not null");
            let getAllArticleToken = localStorage.getItem("article_token");
            let article_tokens = JSON.parse(getAllArticleToken);
            let selectedToken = article_tokens.filter(article => article.token === article_token);
            if(selectedToken.length === 0){
                if(isHit === false){
                    isHit = true;
                    let tokenObject = {
                        token : article_token
                    };
                    $.post("/point/article", tokenObject)
                        .done(function(response){
                            if(response.StatusCode == "00"){
                                $("#modal-error").find(".modal-footer").remove();
                                alert(`Anda mendapatkan point sebesar ${response.Total} Points`, "Selamat");
                                let storeObject = [
                                    ...article_tokens,
                                    tokenObject
                                ];
                                localStorage.setItem("article_token", JSON.stringify(storeObject));
                            }else{
                                console.log("token already defined")
                            }
                        })
                }
            }
        }else{
            console.log("token is null");
            localStorage.setItem("article_token", JSON.stringify([]));
        }
    }
}

$(document).scroll(function(){
    let scrollTime = $(this).scrollTop();
    if(scrollTime >= ($(".article__wrapper").height() - $("footer").height())){
        storePoints();
    }
});

$("a[href^='#']").on("click", function() {
  var hash = this.hash;
  $("html, body").animate({
      scrollTop: $(hash).offset().top - 80
  }, 1500)
});
if($(".accordion__source-btn").hasClass("collapsed")){
  $(".accordion__source-btn").removeClass("collapsed");
}else{
  $(".accordion__source-btn").addClass("collapsed");
}

//DATA LAYER
$(document).ready(function(){

  var article_title = $("h1").text();
  var article_tag = $("h4").text();
  var article_published_date = $("#article_published_date").val();

  dataLayer.push({
    "article title": article_title,
    "article length": "",
    "article published date": article_published_date,
    "article tag": article_tag 
  });
});

article_length = getText(".article__body").trim().replace(/(^\s*)|(\s*$)/gi,"").replace(/[ ]{2,}/gi," ").replace(/\n /,"\n").split(' ').length;