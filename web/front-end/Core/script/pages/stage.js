// remove border bottom in card__articles-item
$(".card__articles-item a").last().css("border-bottom", "none");
// find col-7 in the tool-list__header that parent with tool-list and replace it with col-12 in the stage page
$(".tool-list__header").removeClass("col-7").addClass("col-12");

$(".stage-side .card-slider-big").slick({
    slideToShow: 3,
    infinite: true,
    arrows: true,
    dots: true
});


var tool_length = $(".stage-side .tool-list__wrapper a").length;

if(tool_length < 3){
	$(".stage-side .tool-list__wrapper").addClass("tool-adjust-mobile-desktop");
}
else if(tool_length >= 3 && tool_length < 7){
	$(".stage-side .tool-list__wrapper").addClass("tool-adjust-desktop");
}

console.log("has changed");


//DATA LAYER SEARCH


$(document).ready(function(){

	var search_query = localStorage.getItem("searchKey");
	var search_url = window.location.pathname;

	dataLayer.push({
		"event": "sitesearch",
		"search_query": search_query,
		"search_url": search_url,
	});
});
