$(document).ready(function () {
    $(".btn-agree").click(function () {
        localStorage.setItem("modal", 1);
    });
    $(".btnTime").click(function () {
        $(".modal-time").modal();
    });
    $("#closeModal").click(function () {
        $(".modal-time").modal("hide");
        $("html, body").animate(
            {
                scrollTop: $("#contact").offset().top - 150,
            },
            200
        );
    });
});

// check if there is value cookie
if (localStorage.getItem("modal") == null) {
    $(window).on("load", function () {
        $("#modalDisclaimer").modal({
            backdrop: "static",
            kayboard: false,
            show: true,
        });
    });
} else {
    $(window).on("load", function () {
        $("#modalDisclaimer").modal({
            backdrop: "static",
            kayboard: false,
            show: false,
        });
    });
}

$(window).on("load", function () {
    var pageURL = window.location.href;
    var url = pageURL.substring(pageURL.lastIndexOf("/") + 1);
    if (url == "#service") {
        $("html, body").animate(
            {
                scrollTop: $(url).offset().top - 500,
            },
            200
        );
    } else {
        $("html, body").animate(
            {
                scrollTop: $(url).offset().top - 150,
            },
            200
        );
    }
});
// Expert Team
$(".section-list-item").click(function(){
   let target = $(this).find("div span").text().trim();
   postDataLayer(
       `${target}`,
       `Expert Advisor`,
       `step 1 - ${target}`,
       `${target}`
   )
});


// Faq Datalayer
$(".parent-accord__item").click(function(){
    let title = $(this).find(".parent-accord__item-head h3").text().trim();
    let description = $(this).find(".parent-accord__item-head").siblings(".parent-accord__item-body").text().split("...").join("").trim();
    postDataLayer(
        `${title}`,
        `Expert Advisor`,
        `step 2 - ${title}`,
        `${description}`
    )
})