/* DATA LAYER All Pages */
/* -------------------- */

$(document).ready(function(){

	console.log("datalayer ready");
	var user_id = ''; //done
	var client_id = ''; //done
	var content_category = '';
	var content_type = '';
	var content_subcategory = '';
	var ad_blocker_status = ''; //done
	var video_present = ''; //done
	var reward_point = ''; //done
	var membership_status = ''; //done
	var page_ref = '';
	var article_length = '';
	
	if( window.canRunAds === undefined ){
		// adblocker detected, show fallback
		ad_blocker_status = 'true';
	}
	else {
		ad_blocker_status = 'false';
	}
	
	if($('.hamburger-menu__profile').length > 0){
		membership_status = 'yes';
	}
	else {
		membership_status = 'no'
	}
	if($('.video-detail').length > 0){
		video_present = 'true';
	}
	else {
		video_present = 'false';
	}
	
	user_id = $('.hamburger-menu__profile h4 span').text();
	client_id = $('.hamburger-menu__profile h4 span').text();
	reward_point = parseInt($('.point').text());
	content_category = $('#content_category').text();
	content_type = $('#content_type').val();
	content_subcategory = $('#content_subcategory').text();
    article_length = $(".article__body").text().trim().replace(/(^\s*)|(\s*$)/gi,"").replace(/[ ]{2,}/gi," ").replace(/\n /,"\n").split(' ').length;

	if($('#article_published_date').length > 0){
		content_category = $('.heading__article h4').text();
		content_subcategory = $('.heading__article h4').text();
	}

	dataLayer.push({
		'user_id': user_id,
		'content_category': content_category,
		'content_type': content_type,
		'content_subcategory': content_subcategory,
		'ad_blocker_status': ad_blocker_status,
		'video_present': video_present,
		'reward_point': reward_point,
		'membership_status': membership_status,
        'article_length' : article_length
	});
});

/* ------------------------ */
/* DATA LAYER LOGIN / LOGOUT */

$('.login__wrapper button[type="submit"]').click(function(){
	dataLayer_session('login', 'email');
});

$('.link-logout').click(function(){
	dataLayer_session('logout','');
});

function dataLayer_session(user_activity, login_method){
	dataLayer.push({
	'event': 'usersession',
	'user_activity': user_activity,
	'login_method': login_method
	});
}

/* ------------------------ */
/* DATA LAYER MENU HEADER & FOOTER */

//GENERAL PANEL TRACKING
$(document).on('click', '.elemen-panel', function(){
	var panel_name = $(this).data('panelname');
	var panel_position = $(this).data('panelposition');
	var panel_page_url = $(this).attr('href');
	var panel_page_title = $(this).data('pagetitle');
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

//BANNER PANEL TRACKING
$('.banner-slider__item a').click(function(){
	var panel_name = 'Banner';
	var panel_position = 'Slot ' + $(this).parent().index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = $(this).attr('href');
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

//CARD SLIDER ONE TRACKING
$('.card-slider-one__item').click(function(){
	var panel_name = 'Hal Yang Perlu Anda Ketahui';
	var panel_position = 'Slot ' + $(this).index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Stage ' + $(this).find('h5').text();
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

//CARD SLIDER ONE TRACKING
$('.card-slider-two__item').click(function(){
	var panel_name = 'Artikel Terbaru';
	var panel_position = 'Slot ' + $(this).index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Artikel detail';
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});
$('.card-slider-two__btn').click(function(){
	var panel_name = 'Artikel Terbaru';
	var panel_position = 'Baca Sekarang Slot ' + $(this).parents('.card-slider-two__item').index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Artikel detail';
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});
$('.card-slider-two .see-more').click(function(){
	var panel_name = 'Artikel Terbaru';
	var panel_position = 'Lihat Semua';
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Lihat Semua Artikel';
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

//KATEGORI ARTIKEL
$('.card-slider-three__item').click(function(){
	var panel_name = 'Kategori Artikel';
	var panel_position = 'Slot ' + $(this).index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Kategori Artikel  ' + $(this).find('h5').text();
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});
$('.card-slider-three .see-more').click(function(){
	var panel_name = 'Kategori Artikel';
	var panel_position = 'Lihat Semua';
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Lihat Semua Kategori';
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

//TOOL LIST
$('.tool-list__item').click(function(){
	var panel_name = 'Apa Saja yang Bisa Kami Bantu';
	var panel_position = 'Slot ' + $(this).index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Tool  ' + $(this).find('h5').text();
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});
$('.tool-list .see-more').click(function(){
	var panel_name = 'Apa Saja yang Bisa Kami Bantu';
	var panel_position = 'Lihat Semua';
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Lihat Semua Tools';
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

$('.card-two-column a').click(function(){
	var panel_name = 'Konten Kami';
	var panel_position = 'Slot ' + $(this).parent().index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Konten  ' + $(this).find('h5').text();
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

$('.card-banner-small a').click(function(){
	var panel_name = 'Download Button';
	var panel_position = 'Slot ' + $(this).index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Download App';
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

//ARTIKEL
$('.filter__item a').click(function(){
	var panel_name = 'Kategori Bar';
	var panel_position = 'Slot ' + $(this).parent().index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = 'Kategori ' + $(this).text();
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

$('.card__articles-item a').click(function(){
	var panel_name = 'Artikel Terbaru';
	var panel_position = 'Slot ' + $(this).parent().index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = $(this).find('.card__articles-text').text();
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

$('.video__list-item a').click(function(){
	var panel_name = 'Video Terkait';
	var panel_position = 'Slot ' + $(this).parent().index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = $(this).find('p').text();
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

$('.podcast__list a').click(function(){
	var panel_name = 'Podcast Terkait';
	var panel_position = 'Slot ' + $(this).index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = $(this).find('p').text();
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});

$('.card__articles-related-item a').click(function(){
	var panel_name = 'Artikel Terkait';
	var panel_position = 'Slot ' + $(this).parent().index();
	var panel_page_url = $(this).attr('href');
	var panel_page_title = $(this).find('.card__articles-related-text').text();
	dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title);
});


function dataLayer_menu(panel_name, panel_position, panel_page_url, panel_page_title){
	dataLayer.push({
		'event': 'paneltracking',
		'panel_name': panel_name,
		'panel_position': panel_position,
		'panel_page_url': panel_page_url,
		'panel_page_title': panel_page_title
	});
}

/* ------------------------ */
/* DATA LAYER SOCIAL FOOTER */

$('.social-ig').click(function(){
	dataLayer_social('Instagram', 'https://www.instagram.com/nutriclub_id/?hl=en');
});

$('.social-fb').click(function(){
	dataLayer_social('Facebook', 'https://www.facebook.com/NutriclubIndonesia/');
});

$('.social-yt').click(function(){
	dataLayer_social('Youtube', 'https://www.youtube.com/channel/UCx4BmO2RjaFfUv4HB685vDA');
});

function dataLayer_social(profile_account, profile_url){
	dataLayer.push({
		'event': 'followprofile',
		'profile_account': profile_account,
		'profile_url': profile_url
	});
}

/* ------------------------ */
/* DATA LAYER SOCIAL SHARE */

$('.socialmedia__sharing a:first-child').click(function(){
	dataLayer_share('facebook');
});
$('.socialmedia__sharing a:nth-child(2)').click(function(){
	dataLayer_share('twitter');
});

function dataLayer_share(engagement_detail){
	dataLayer.push({
		'event': 'articleengagement',
		'engagement_action': 'Share',
		'engagement_detail': engagement_detail
	});
}

/* ------------------------ */
/* DATA LAYER SOCIAL SHARE */

$('#step-1 .btn-controller').click(function(){
	dataLayer_form('Step 1 - Siapa Anda', 'success');
});
$('#step-2 .btn-controller').click(function(){
	dataLayer_form('Step 2 - Kondisi Anda', 'success');
});
$('#step-4 .btn-miscall').click(function(){
	dataLayer_form('Step 4 - OTP Miscall', 'success');
});
$('#step-5 .btn-verifikasi').click(function(){
	dataLayer_form('Step 5 - OTP Verifikasi', 'success');
});

function dataLayer_form(form_step, submit_status){
	dataLayer.push({
		'event': 'formsubmission',
		'form_name': 'registration form',
		'form_step': form_step,
		'form_reference_id': '',
		'submit_status': submit_status
	});
}

/* DATA LAYER VIDEO */

$('#player').click(function(){

	var video_url = $('#video_url').val();
	var video_title = $('.video-detail__description-title').text();
	var video_duration = $('#video_duration').val(); 
	var video_published_date = $('#video_published_date').val(); 
	var video_author_id = $('#video_author_id').val(); 

	dataLayer_video(video_url, video_title, video_duration, video_published_date, video_author_id);
});

function dataLayer_video(video_url, video_title, video_duration, video_published_date, video_author_id){
	dataLayer.push({ 
		'event': 'videoengagement', 
		'video_percentage': 'start / 25% / 50% / 75% / 100%', 
		'video_url': video_url, 
		'video_title': video_title, 
		'video_duration': video_duration,  
		'video_published_date': video_published_date,
		'video_author_id': video_author_id,
		'video_source': 'YouTube' 
	}); 
}



 console.log("fixed datalayer");


