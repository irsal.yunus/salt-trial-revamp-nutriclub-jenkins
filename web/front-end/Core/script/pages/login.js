$(document).ready(function(){
    
    $("div.showpassword").click(function(){
        $("i").toggleClass("blue-color");
        let input = $("input#password");
        if($("i").hasClass("blue-color")){
            input.attr("type", "text");
        }else{
            input.attr("type", "password");
        }
    });
    
    $("input#username, input#password").on("keyup change", function(){
        if($("input#username").val() != "" && $("input#password").val() != ""){
            $(".btn-secondary").removeAttr("disabled");
        }else{
            $(".btn-secondary").attr("disabled");
        }
    });
});

