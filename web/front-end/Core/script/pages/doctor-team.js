// helper
$.fn.NumericInput = function(){
    return this.each(function(){
        $(this).keydown(function(e){
            let key = e.charCode || e.keyCode || 0;
            return (
                key == 8  ||
                key == 9  ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105) ||
                (key == 187)
            )
        })
    })
}

$.fn.AddInvalid = function(message){
    let invalidHtml = `<div class="invalid-feedback">
        ${message}
     </div>`
    this.addClass("is-invalid")
    this.after(invalidHtml);
}

$.fn.removeInvalid = function(){
    this.removeClass("is-invalid");
    return this.find("invalid-feedback").remove();
}

$.fn.removeAlert = function(){
    this.keyup(function(){
        $(this)
            .removeClass("is-invalid")
            .find("invalid-feedback"
            ).remove()
        ;
    })
}

function validatePhoneNumber(phone) {
    var regexPhoneNumber = /^(^\+62\s?|^0)(\d{3,4}-?){2}\d{3,4}$/g;
    if (!regexPhoneNumber.test(String(phone).toLowerCase())) {
        return false;
    } else {
        return true;
    }
}

function validateEmail(email) {
    var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regexEmail.test(String(email).toLowerCase())) {
        return false;
    } else {
        return true;
    }
}

$("#issue").keyup(function(){
    let issueValue = $(this).val().length
    if($(this).val() >= 80){
        $(".maxCharacter").addClass("text-danger");
    }else{
        $(".maxCharacter").removeClass("text-danger");
        $(".maxCharacter").text(issueValue);
    }
})

let classNameForm = [
    "name",
    "email",
    "phonenumber",
    "issue",
    "issue-description",
    "topicquestion"
]

function getRemoveWhileType(...idName){
    var  [name, email, phonenumber, issue, issuedetection, topicquestion] = idName;
    $(`#${name},#${email}, #${phonenumber}, #${issue}, #${issuedetection}`).removeAlert();
    $(`#${topicquestion}`).change(function(){
        $( `#${topicquestion} option:selected` ).each(function() {
            if($(this).val() !== 0){
                $(`#${topicquestion}`).removeClass("is-invalid").find("invalid-feedback").remove()
            }
        });
    })
}





// initialize prevent number
$("#phonenumber").NumericInput();

$("#submitQuestion").submit(function(e){
    e.preventDefault();
    let error = 0;
    let name = $("#name");
    let email = $("#email");
    let phonenumber = $("#phonenumber");
    let topic = $("#topicquestion");
    let issue = $("#issue");
    let issueDescription = $("#issue-description");
    // name checking
    if(name.val() == ""){
        name
            .AddInvalid("Mohon isi nama lengkap")
        error++;
    }else{
        name
            .removeInvalid();
        error;
    }
    // email checking
    if(validateEmail(email.val()) === false){
        email.AddInvalid("Email tidak valid");
        error++;
    }else if(email.val() === ""){
        email.AddInvalid("Mohon isi alamat email");
        error++;
    }else{
        error--;
    }

    // phonenumberchecking
    if(!validateEmail(phonenumber.val())){
        phonenumber.AddInvalid("Nomor telpon tidak valid");
        error++;
    }else if(phonenumber.val() === ""){
        phonenumber.AddInvalid("Mohon isi nomor handphone");
        error++;
    }else{
        error--;
    }
    // topic
    if(topic.val() === null || topic.val() == 0){
        topic.AddInvalid("Mohon mengisi topik terkait");
        error++;
    }else{
        error--;
    }

    // issue
    if(issue.val() == ""){
        issue.AddInvalid("Mohon mengisi keluhan");
        error++;
    }else{
        error--;
    }

    if(issueDescription.val() == ""){
        issueDescription.AddInvalid("Mohon mengisi penjelasan keluhan lengkap");
        error++;
    }else{
        error--;
    }

    if(error === 0){
        $(this).submit();
    }else{
        getRemoveWhileType(...classNameForm);
        console.log("total error : ", error);
        e.preventDefault();
    }
})