if($(".ebook__content-btn").hasClass("collapsed")){
    $(".ebook__content-btn").removeClass("collapsed");
  }else{
    $(".ebook__content-btn").addClass("collapsed");
  }

function holdButtonToDownload(){
  if(IS_LOGGED_IN === "false"){
    $("a.ebook__content-btn").attr({
      href : "#",
      id : "holdDownload"
    });
    $("a.ebook__content-btn").removeAttr('target');
    $(document).on("click", "#holdDownload", function(){
      alert(`${message_login}`);
    });
  }
}

$(document).ready(function(){
  holdButtonToDownload();
})