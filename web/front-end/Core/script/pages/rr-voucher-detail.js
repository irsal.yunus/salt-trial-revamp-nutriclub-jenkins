$(document).ready(function() {
    if (isVoucherUsed === "1") {
        showHideButtonVoucher();
    }
});

function showVoucherCode(id, catalogueVoucherId) {
    if (isDefaultVoucher) {
        updateStatusVoucher(catalogueVoucherId);
    }

    if (!isDefaultVoucher) {
        redeem(id);
    }
}

function updateStatusVoucher(id) {
    $.ajax({
        type: 'GET',
        url: '/royal-rewards/voucher/update/' + id,
        success: function(data){
            if(data.StatusCode === '00') {
                location.reload();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {

        }
    });
}

function redeem(id) {
    $.ajax({
        type: 'GET',
        url: '/royal-rewards/voucher/redeem/' + id,
        success: function(data){
            if(data.StatusCode === '00') {
                showHideButtonVoucher();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {

        }
    });
}

function showHideButtonVoucher() {
    $(".register-button").addClass("disabled");
    $("#default").hide();
    $(".voucher-code").fadeIn();
    $(".copy-icon").fadeIn();
}

function copyCode() {
    var copyText = document.getElementById("code");
    var textArea = document.createElement("textarea");
    textArea.value = copyText.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
    $("#modal-copy").modal("show");
}