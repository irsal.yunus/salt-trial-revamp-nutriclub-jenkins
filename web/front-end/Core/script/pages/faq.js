var pageURL = window.location.href;
var pageHash = window.location.hash;
var url = pageURL.substring(pageURL.lastIndexOf("#") + 1);
console.log(".acc"+url);
$(window).on("load", function () {
    $("#accordion").accordion({
        icons: null,
        collapsible: true,
        animate: false,
        active: parseInt(url),
    });
    $("html, body").animate(
        {
            scrollTop: $(pageHash).offset().top - 100,
        },
        200
    );
});

$(".accordion-title").click(function(e){
    e.preventDefault();
    let targetPage = $(".faq__content-head h2").text();
    let targetId = $(this).attr("aria-controls");
    let description = $(this).siblings(`.accordion-body#${targetId}`).text().trim();
    let title = $(".accordion-title:nth-child(1) span").text().trim();
    postDataLayer(
       `${targetPage}`,
       "Expert Advisor",
       `step 2 - ${title}`,
       `${description}`,
    );
});