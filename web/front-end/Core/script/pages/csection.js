/* Nutriclub C-Section
   written by: SALT Indonesia
   All javascript functionalities for C-Section tools are in this file
*/
 
/* ------------------------------ */
/* PRE ASSESMENT PERHITUNGAN HPHT */
/* ------------------------------ */
$('#btn-preass').click(function(){
  $('.error-msg').empty();
  validatePreAss();
});

if($('#hpht').length > 0){
  $('#hpht').datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: '+0d',
    minDate: '-9m',
    dateFormat: 'dd/mm/yy'
  });
}

function validatePreAss(){
  var countError = 0;
  if($('#nama-mama').val() == ""){
    countError++;
    $('#error-nama').text("Nama Mama wajib diisi.");
  }
  if($('#hpht').val() == ""){
    countError++;
    $('#error-hpht').text("Hari Pertama Haid Terakhir (HPHT) wajib diisi.");
  }

  if(countError == 0){
    window.location.href = '/siap-lewati-caesar/questions';
  }
}


$('#hpht').change(function() {

  var date_hpht = $('#hpht').datepicker('getDate');
  localStorage.setItem('tanggal_hpht', convertDate(date_hpht));

  //ESTIMATE TANGGAL LAHIR. EP = HPHT + 270
  var date_ep = $('#hpht').datepicker('getDate', '+270d'); 
  date_ep.setDate(date_ep.getDate()+270); 
  $('#date-ep').val(convertDate(date_ep));
  localStorage.setItem('tanggal_kelahiran', convertDate(date_ep));

  //DATE TRIMESTER. TRI = (HPHT + 270) - 63
  var date_tri = $('#hpht').datepicker('getDate', '+207d'); 
  date_tri.setDate(date_tri.getDate()+207); 
  $('#date-tri').val(convertDate(date_tri));
  localStorage.setItem('tanggal_trimester_3', convertDate(date_tri));

  //IS TRIMESTER OR NOT. IF TRI > TODAY, NO. ELSE YES.
  var is_tri;
  if(date_tri < new Date()){
    is_tri = true;
  }
  else {
    is_tri = false;
  }
  $('#is-tri').val(is_tri);
  localStorage.setItem('is_trimester', is_tri);

  //MINGGU KEHAMILAN
  const date1 = $('#hpht').datepicker('getDate');
  const date2 = new Date();
  const diffTime = Math.abs(date2 - date1);
  const diffWeeks = Math.round(diffTime / (1000 * 60 * 60 * 24 * 7)); 
  $('#minggu-kehamilan').val(diffWeeks + ' minggu');
  localStorage.setItem('minggu_kehamilan', diffWeeks);
  localStorage.setItem('tanggal_input', convertDate(new Date()));
});


function convertDate(date){
  var dd = String(date.getDate()).padStart(2, '0');
  var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = date.getFullYear();
  date = dd + '/' + mm + '/' + yyyy;
  return(date);
}

$('#nama-mama').change(function(){
  localStorage.setItem('nama-mama', $(this).val());
});

/* ------------------------------ */
/* QUESTIONS */
/* ------------------------------ */

/* Initiate arrays for answers & trimester */
var answers = [0,0,0,0,0,0,0,0];
var answers_trimester = [0,0,0,0,0,0,0,0];
var total = 0;
var result;

/* Condition: if trimester third is true, show questions trimester */
if(localStorage.getItem("is_trimester") == "true"){
  //$('#question-4 .question-next').attr('data-href','#question-6a');
  $('#next-trimester').show();
  $('#next-form').hide();
}
else {
  $('#next-trimester').hide();
  $('#next-form').show();
}

/* Event handler for button prev & next */
$('.question-next, .question-prev').click(function(){
  var _dest = $(this).attr('data-href');
  $(_dest).siblings().removeClass('active');
  $(_dest).addClass('active');
  window.scrollTo(0,0);
});


/* Event handler for everytime an answer is clicked */
$('.question-action .btn').click(function(){
  $(this).addClass('active');
  $(this).siblings().removeClass('active');

  var this_val = parseInt($(this).attr('data-answer'));
  var this_index = parseInt($(this).parents('.question-choice').attr('data-index'));
  var this_type = $(this).parents('.question').attr('id');

  //for question 2 if answer is Yes/iya it will hide question page 3 no 1
  if(this_index == 1){
    if(this_val == 1){
      $('#q3-1').addClass('hidden');
      $('#question-3 .question-choices').addClass('force-center');
    }
    else {
      $('#q3-1').removeClass('hidden');
      $('#question-3 .question-choices').removeClass('force-center');
    }
  }
  
  //count the total number, update the arrays
  countTotal();
  updateAnswer(this_val, this_index, this_type);

  //local storage for each question
  var this_id = $(this).parents('.question-choice').attr('id');
  var this_text = $(this).text();
  localStorage.setItem(this_id, this_text);

  var _qlength = $(this).parents('.question').find('.question-choice').not('.hidden').length;
  var _alength = $(this).parents('.question').find('.question-action .btn.active').length;

  console.log(_qlength);
  console.log(_alength);

  if(_alength == _qlength){
    $(this).parents('.question').find('.question-next, .btn-primary').removeClass('disabled');
  }
  else {
    $(this).parents('.question').find('.question-next, .btn-primary').addClass('disabled');
  }

});

/* Function for update array*/
function updateAnswer(value, order, type){

  /* Condition if we want to update array TRIMESTER or REGULAR array */
  if(type == "question-6"){
    answers_trimester[order] = value;
    console.log("answers_trimester:")
    console.log(answers_trimester);
    localStorage.setItem("trimester_questions", JSON.stringify(answers_trimester));
  }
  else {
    answers[order] = value;
    console.log("answers:")
    console.log(answers);
    localStorage.setItem("questions", JSON.stringify(answers));
  }
  
}

/* Function to count total number*/
function countTotal(){
  total = 0;
  $('.question-action .btn.active').each(function(){
    total = total + parseInt($(this).attr('data-answer'));
  });
  console.log("total: " + total);
  localStorage.setItem("total", total);

  //DETERMINE THE RESULT
  if(total == 0){
    result = 'low';
  }
  else {
    if(localStorage.getItem("is_trimester") == "true"){
      result = 'high, trimester 3';
    }
    else {
      result = 'high';
    }
  }

  localStorage.setItem('result', result);

  //TEMP
  $('.result').html(result);
}

$('.question-did span').click(function(){
  $(this).parent().addClass('active');
});

$('.question-tutup').click(function(){
  $(this).parents('.question-did').removeClass('active');
});

/* ------------------ */
/* VALIDASI FORM */
/* ------------------ */


if($('#tanggal-anak').length > 0){
  $('#tanggal-anak').datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: '+0d',
    dateFormat: 'dd/mm/yy'
  });
}

$('#nama-lengkap-mama').val(localStorage.getItem("nama-mama"));
$('#usia-kehamilan-reg').val(localStorage.getItem("minggu_kehamilan") + " minggu");
$('.btn-result').click(function(){
  validateForm();
  // window.location.href = '/siap-lewati-caesar/result';
});

$('input, select').change(function(){
  $(this).parents('.form-group').find('.error-msg').empty();
});

$('#kondisi-ibu').change(function(){
  if($(this).val() == 7){
    $('.anak').hide();
    $('.hamil').show();
  }
  else if($(this).val() == 8){
    $('.anak').hide();
    $('.hamil').hide();
  }
  else if($(this).val() == 9){
    $('.anak').show();
    $('.hamil').show();
  }
  else if($(this).val() == 10){
    $('.anak').show();
    $('.hamil').hide();
  }
});

function validateForm(){
  var countError = 0;

  if($('#no-telp').val() == ""){
    countError++;
    console.log("a");
    $('#error-telp').text("No telp harus diisi");
  }

  if($('#email').val() == ""){
    countError++;
    console.log("b");
    $('#error-email').text("Email harus diisi");
  }
  else if(validateEmail($('email').val())){
    countError++;
    console.log("b-2");
    $('#error-email').text("Format email harus valid");
  }

  if($('#kondisi-ibu').val() == ""){
    countError++;
    console.log("c");
    $('#error-kondisi').text("Kondisi ibu harus diisi");
  }
  else if($('#kondisi-ibu').val() == 9 || $('#kondisi-ibu').val() == 10){
    if($('#nama-anak').val() == ""){
      console.log("d");
      countError++;
      $('#error-anak').text("Nama anak harus diisi");
    }
    if($('#tanggal-anak').val() == ""){
      console.log("e");
      countError++;
      $('#error-tanggal-anak').text("Tanggal lahir anak harus diisi");
    }
  }

  if($('#syarat-ketentuan:checked').length == 0){
    console.log("f");
    countError++;
    $('#error-syarat').text("Syarat & ketentuan harus disetujui");
  }

  if(countError == 0){

    createJSON();
    //window.location.href = '/siap-lewati-caesar/result';
  }
  else {
    console.log("masih ada error " + countError);
  }
}

function createJSON(){
  /*
    member_id,
    kondisi_ibu,
    tanggal_hpht,
    tanggal_kelahiran,
    tanggal_trimester_3,
    tanggal_input,
    minggu_kehamilan,
    is_trimester,
    questions,
    trimester_questions,
    result
  */

  var all_data = {
    'member_id': '',
    'kondisi_ibu': $('#kondisi-ibu').val(),
    'nama_mama': localStorage.getItem('nama-mama'),
    'no_telp': $('#no-telp').val(),
    'email': $('#email').val(),
    'nama_anak': $('#nama-anak').val(),
    'tanggal_anak': $('#tanggal-anak').val(),
    'tanggal_hpht': localStorage.getItem('tanggal_hpht'),
    'tanggal_kelahiran': localStorage.getItem('tanggal_kelahiran'),
    'tanggal_input': localStorage.getItem('tanggal_input'),
    'minggu_kehamilan': localStorage.getItem('minggu_kehamilan'),
    'is_trimester': localStorage.getItem('is_trimester'),
    'questions': JSON.parse(localStorage.getItem('questions')),
    'trimester_questions': JSON.parse(localStorage.getItem('trimester_questions')),
    'result': localStorage.getItem('result')
  }

  console.log('------------------------');
  console.log(all_data);
  console.log('------------------------');

  $.ajax({
    type: "POST",
    url: "/siap-lewati-caesar/submit",
    data: all_data,
    success: function (data) {
        console.log("SEND DATA TO BACKEND SUCCESS");
        console.log(data);
       
        if (data.StatusCode == "00") {
           console.log("00");
           window.location.href = '/siap-lewati-caesar/result';

        } else {
            console.log("else");
            window.location.href = '/siap-lewati-caesar/result';
        }
    },
    error: function(){
      alert('error!');
      window.location.href = '/siap-lewati-caesar/result';
    }
});
}

function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}

/* ------------------ */
/* RESULT CONTROLLER  */
/* ------------------ */
if(localStorage.getItem('result') == 'high'){
  $('.result-1').hide();
  $('.result-3').hide();
  $('.result-2').show();
}
else if(localStorage.getItem('result') == 'high, trimester 3'){
  $('.result-1').hide();
  $('.result-2').hide();
  $('.result-3').show();
}
else if(localStorage.getItem('result') == 'low'){
  $('.result-2').hide();
  $('.result-3').hide();
  $('.result-1').show();
}

$('.result-summary h1 span').text(localStorage.getItem('nama-mama'));

$('.result-list-item').each(function(){
  var this_id = $(this).attr('id');
  $(this).find('.result-list-answer p').text(localStorage.getItem(this_id));
});

breakdownQs();

function breakdownQs(){
  var lists = JSON.parse(localStorage.getItem('questions'));
  var i;

  for(i=0; i<lists.length; i++){
    console.log("this is: " + lists[i]);
    if(lists[i] == 0){
      $('.result-list-item:eq('+ i +')').hide();
    }
  }
}

if(localStorage.getItem('q3-1') == null){
  $('.result-list #q3-1').hide();
}

$('.btn-print').click(function(){
  window.print();
});