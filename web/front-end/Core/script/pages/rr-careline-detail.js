$(document).ready(function () {


});

function rebindTimeClick() {
	$(".time-list li:not(.disabled)").on("click",function(){console.log('jam'),$(".time-list li").each(function(){$(this).removeClass("selected")}),$(this).addClass("selected"),jam=$(this).text(),catalogueId=$(this).attr("data-catalogueId"),checkVal()});
} 

function setActiveTime(date) {
	$(".time-list li").each(function(){
		$(this).addClass("disabled");
	});
	
	$.ajax({
		type: 'GET',
		url: '/royal-rewards/careline/time/' + date,
		success: function(data){
			$.each(data, function(k, v){
				// console.log(v.Time);
			});
			
			$.each(data, function(k, v){
				$(".time-list li").each(function(){
//					console.log(v.Time);
					if(v.Time == $(this).attr('value')) {
						$(this).removeClass("disabled");
						$(this).attr('data-catalogueId', v.Id);
						rebindTimeClick();
					}
				});
			});

		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log('FAILED: Get time');
		}
	});
}

function carelinePostRedeem() {
	console.log("tanggal=" + tanggal + ",hari=" + hari + ",jam=" + jam + ",catalogueId=" + catalogueId);
	if(cId != catalogueId) {
			
		$.ajax({
			type: 'GET',
			url: '/royal-rewards/careline/redeem/' + cId + '/' + catalogueId,
			success: function(data){
				
				if(data.StatusCode == '00') {
					cId = catalogueId;
					carelinePostSuccess();
				} else {
					carelinePostFailed();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				carelinePostFailed();
			}
		});
	} else {
		console.log('catalogueId sama');
		carelinePostFailed();
	}
	
} 

function carelinePostSuccess() {
	$("#main-register").hide();
	$("#success-register").fadeIn();
	
}

function carelinePostFailed() {
	$("#main-register").hide();
	$("#fail-register").fadeIn();
}

function getTimeList(date) {
	$(".time-list-preloader").addClass("show");
	$(".time-list li").each(function(){
		$(this).remove();
	});
	$.ajax({
		type: 'GET',
		url: '/royal-rewards/careline/time/' + date,
		success: function(data){
			
			if(data.StatusCode == '00') {
				// let item = data.Value;
				$(".time-list-preloader").removeClass("show");
				$.each(data.Value, function(index, item){
					$('.time-list').append(
						`<li 
						value="${item.Time}" 
						class="${item.IsStock ? "" : "disabled"} ${item.IsUsed ? "selected" : ""}"
						data-catalogueId="${item.Id}">${item.Time.slice(0,-3)}</li>`
					);
				});
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$(".time-list-preloader").removeClass("show");
			console.error("Fail to get time list from API");
		}
	});
}