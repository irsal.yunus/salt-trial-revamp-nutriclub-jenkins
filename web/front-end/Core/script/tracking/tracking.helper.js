function gatherData(obj) {
    const DATA_ATTRS_OBJ = $(obj).data();

    const USER_REWARDS_ID_OBJ = typeof USER_REWARDS_ID !== "undefined" ? {
        'user_rewards_id' : USER_REWARDS_ID
    } : {};

    const CURRENT_PERSONA_OBJ = typeof CURRENT_PERSONA !== "undefined" ? {
        'current_persona' : CURRENT_PERSONA
    } : {};

    const DATA_OBJ = {
        ...DATA_ATTRS_OBJ,
        ...USER_REWARDS_ID_OBJ,
        ...CURRENT_PERSONA_OBJ
    };

    return DATA_OBJ instanceof Object ? DATA_OBJ : {};
}

function goTrackActiveAnalytics(eventName, dataObj) {
    trackCountlyEvent(eventName, dataObj);
    trackMoEngageEvent(eventName, dataObj);
}

function trackCountlyEvent(eventName, dataObj) {
    if (typeof Countly === "undefined") {
        return null;
    }

    Countly.add_event({
        key:eventName,
        segmentation: dataObj
    });
}

function trackMoEngageEvent(eventName, dataObj) {
    if (typeof Moengage === "undefined") {
        return null;
    }

    Moengage.track_event(eventName, dataObj);
}