$(document).ready(function() {
    $("a").on("click", function() {
        const EVENT_NAME = $(this).attr('event-name');

        if (EVENT_NAME === undefined || EVENT_NAME === "") {
            // @todo notify ?
            return;
        }

        const DATA_OBJ = gatherData($(this));
        goTrackActiveAnalytics(EVENT_NAME, DATA_OBJ);
    });
});