$('#freesampleCheck').click(function(){
  if($(this).is(':checked')){
      $('#freeSampleSubmit').attr('disabled', false);
  } else{
      $('#freeSampleSubmit').attr('disabled', true);
  }
});

$('select').change(function(){
  $(this).css('color', '#495057');
  $('.sampel option').css('color', '#b2b2b2');
});

$(document).ready(function() {
  $('#kodepos').bind('keypress', function (e) {
      var keyCode = e.which ? e.which : e.keyCode
           
      if (!(keyCode >= 48 && keyCode <= 57)) {
        return false;
      }
  });
});

$("#freesampleCheck").click(function(){
    if($(this).is(':checked')){
        $('#freeSampleSubmit').attr("disabled", false);
    } else{
        $('#freeSampleSubmit').attr("disabled", true);
    }
});

$("select").change(function(){
    $(this).css('color', '#495057');
    $(".sampel option").css('color', '#b2b2b2');
  });

$(document).ready(function() {
    $("#kodepos").bind("keypress", function (e) {
        var keyCode = e.which ? e.which : e.keyCode
             
        if (!(keyCode >= 48 && keyCode <= 57)) {
          return false;
        }
    });
  });
$("#freeSampleSubmit").click(function() {
    $("form").submit(function(e){
        e.preventDefault(e);
    });
    let error = 0;
    if ( $(".btn-province").attr("dataProvinsi") === "0") {
        $(".province-validator").show()
        $(".btn-province").css('border', '1px solid rgba(220,53,69,.9)');
        error++;
    }
    if ( $("#kodepos").val().trim() < 4 ) {
        $(".postalcode").show()
        $("#kodepos").css('border', '1px solid rgba(220,53,69,.9)');
        error++;
    }
    if ( $("#address").val().trim() < 4 ) {
        $(".address-validation").show();
        $("#address").css('border', '1px solid rgba(220,53,69,.9)');
        error++;
    }
    for (let i = 0; i < $(".sampel").length; i++){
        if($(".sampel").eq(i).val() === "Pilih Jawaban"){
            $(".sampel").eq(i).css('border', '1px solid rgba(220,53,69,.9)');
            $(`.alert-sampel`).eq(i).show();
            error++;
        }else{
            $(`.alert-sampel`).eq(i).hide();
            $(".sampel").eq(i).css('border', '1px solid #ced4da');
        }
        $(".sampel").eq(i).click(function(){
            $(`.alert-sampel`).eq(i).hide();
            $(".sampel").eq(i).css('border', '1px solid #ced4da');
        });
    }

    if(error === 0){
        $("form").submit(function(e){
            e.currentTarget.submit();
        });
    }
})
$(".btn-province").click(function() {
    $(".btn-province").css('border', '1px solid #ced4da');
    $(".province-validator").hide()
})
$("#kodepos").on("keyup", function() {
    $("#kodepos").css('border', '1px solid #ced4da');
    $(".postalcode").hide()
})
$("#address").on("keyup", function() {
    $("#address").css('border', '1px solid #ced4da');
    $(".address-validation").hide();
})