$(document).ready(function() {
    var img = $('.profile-photo'), 
        btn = $('.upload-wrapper .input span'),
        inputFile = $('.upload-wrapper .input #file'),
        inputHiddenProfilePicture = $('.upload-wrapper .input #ProfilePicture');
		
	btn.on('click', function(){
		inputFile.click();
	});

	inputFile.on('change', function(e){
		var i = 0;
		for(i; i < e.originalEvent.srcElement.files.length; i++) {
			var file = e.originalEvent.srcElement.files[i], 
				reader = new FileReader();

			reader.onloadend = function(){
			    console.log(reader.result);
                img.attr('src', reader.result);
                inputHiddenProfilePicture.attr('value', reader.result);
			}
            reader.readAsDataURL(file);
        }

        // console.log($(this).val());
	});
});