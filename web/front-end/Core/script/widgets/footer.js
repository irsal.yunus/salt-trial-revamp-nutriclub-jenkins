function hitCookieConsentApi(){
    const cookieConsentObject = IS_LOGGED_IN === "true" ? {"ID" : COOKIE_CONSENT, "MemberId" : USER_REWARDS_ID, "IsAcceptCookie": true} : {"ID" : COOKIE_CONSENT, "IsAcceptCookie": true}
    const ENDPOINT = "api/Consent/Save"
    $.post(`${REWARDS_API_BASE_URL}/${ENDPOINT}`, cookieConsentObject)
        .done(function(){
            console.log("success hit cookie");
        }).fail(function(error){
        console.log("error : ", error);
    })
}
$(".footer__notfication-btn").click(() => {
    localStorage.setItem("cookie_acceptance", 1);
    $(".footer__notification").removeClass("active");
    hitCookieConsentApi();
});

// check if there is value cookie acceptance
if(localStorage.getItem("cookie_acceptance") == null){
    $(".footer__notification").addClass("active");
}else{
    $(".footer__notification").removeClass("active");
}

