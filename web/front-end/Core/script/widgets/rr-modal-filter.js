$(document).ready(function () {
    // Construct URLSearchParams object instance from current URL querystring.
    var queryParams = new URLSearchParams(window.location.search);

    if (queryParams.get('type')) {
        var type = queryParams.get('type');
        $("button.history-filter-type__button").addClass("filtered");
        $(`#action-${type}`).attr('checked', true);
        $(`#action-${type}`).prop('checked', true);
        $(".apply-filter , .reset-filter").addClass("active").removeAttr("disabled");
    }
    if (queryParams.get('timeBy') == '1') {
        $("button.history-filter-newest__button").addClass("filtered");
    }
    
    $('input.form-check-input').change(function() {
        if(this.checked) {
            $('input.form-check-input').each(function() {
                $(this).prop( 'checked', false );
                $(this).attr( 'checked', false ); 
            });
            $(this).prop( 'checked', true );
            $(this).attr( 'checked', true );

            queryParams.set("type", $(this).val());

            $(".apply-filter , .reset-filter").addClass("active").removeAttr("disabled");
        }
    });

    $(".apply-filter").on("click" , function(){
        history.pushState(null, null, "?" + queryParams.toString());
        location.reload();
    });

    $(".reset-filter").on("click" , function(){
        $('input.form-check-input').each(function() {
            $(this).prop( 'checked', false );
            $(this).attr( 'checked', false ); 
        });
        $(".apply-filter , .reset-filter").removeClass("active").attr("disabled" , "disabled");
        $("button.history-filter__button").removeClass("filtered");
        $('#modalFilter').modal('hide');

        queryParams.delete('type');
        history.pushState(null, null, "?" + queryParams.toString());
        location.reload();
    });

    $(".history-filter-newest__button").on("click", function () {
        var timeBy = (queryParams.get('timeBy') == 1) ? 0 : 1;
        queryParams.set("timeBy", timeBy);

        history.pushState(null, null, "?" + queryParams.toString());
        location.reload();
    });

    $(".page-link").on("click", function () {
        d = $(this).attr("value");
        if (d != "0") {
            queryParams.set("page", d);
            history.pushState(null, null, "?" + queryParams.toString());
            location.reload();
        }
    });
});