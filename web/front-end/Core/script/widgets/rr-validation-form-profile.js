$(document).ready(function(){

    getDataProvince();
    getDataDistrict($("input[name='ProvinceID']").attr("value"));
    getDataSubDistrict($("input[name='DistrictID']").attr("value"));
    
    // button gender
    $(".gender button").on("click" , function(){
        $(".gender button").each(function(){
            $(this).removeClass("active");
        });
        $(this).addClass("active");
        if ( $(this).attr("id") == "papa"){
            $("#parent-status").siblings("label").html("Pilihan Status Istri Anda");
            $("#set-gender").attr("value","M");
        }
        else {
            $("#parent-status").siblings("label").html("Pilihan Status Anda");
            $("#set-gender").attr("value","F");
        }
    });

    // datepicker birthdate
    $( ".birthdate-picker" ).datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
    });
    
    // dropdown select parent status
    var default_status_value = $("input[name='StagesID']").attr("value");
    setStateParentStatus(default_status_value);
    $("#select-status li").click( function(){
        var changed_status_value = $(this).attr("value");

        $("#mother-info , #child-info, #parent-separator , .add-child").hide();
        
        setStateParentStatus(changed_status_value);
    });

    //Check OnChange checkbox subscription
    $('#check-receivement').change(function(){
        if ($(this).is(':checked')){
            $(this).attr("value","agree").attr("checked","checked");
            // console.log("true");
        }
        else{
            $(this).attr("value","disagree").removeAttr("checked");
            // console.log("false");
        }
    });
});

function setStateParentStatus(status_value){
    if (status_value === "10"){
        $("#child-info").fadeIn();
        $(".add-child").css("display","flex");
    }
    else if (status_value === "9"){
        $("#mother-info , #child-info, #parent-separator").fadeIn();
        $(".add-child").css("display","flex");
    }
    else if (status_value === "7"){
        $("#mother-info").fadeIn();
    }
    else{
        $("#mother-info , #child-info, #parent-separator, .add-child").hide();
    }
}

// datepicker dynamic child
$(document).on('focus', '.child-birth-input', function() {
    $(this).datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
    });
});

$(document).on('change', '.birthdate-picker , .child-birth-input', function() {
    $(this).attr("value", $(this).val());
});

// dropdown input
var dropdown_open = false;
$(document).on('click', '.select-input', function() {
    if ($(this).siblings(".dropdown-select").css('display') == 'none'){
        $(this).siblings(".dropdown-select").slideDown(50);
    }
    else if ($(this).siblings(".dropdown-select").css('display') == 'block'){
        $(this).siblings(".dropdown-select").slideUp(100);
    }
});

$("body").not(".dropdown-select").mouseup(function(){
    $(".dropdown-select").slideUp(100);
    dropdown_open = false;
});

$(document).on('click', '.dropdown-select li', function() {
    $(this).parent().siblings("input").val($(this).text());
    $(this).parent().slideUp(100);
    dropdown_open = false;
    
    var get_parent_id = $(this).parent().attr("id");

    // region status select condition
    if(get_parent_id == "select-province"){
        var id_province = $(this).attr("id");
        $("#city , #subdistrict").val("");
        $("#select-city li , #select-subdistrict li").each(function(){
            $(this).remove();
        });
        $(this).parent().siblings("input").attr("value",$(this).attr("id"));
        getDataDistrict(id_province);
        $('#select-subdistrict').append("<li id=''>Pilih Kota / Kabupaten Sebelumnya</li>");
    }
    else if(get_parent_id == "select-city"){
        var id_district = $(this).attr("id");
        $("#subdistrict").val("");
        $("#select-subdistrict li").each(function(){
            $(this).remove();
        });
        $(this).parent().siblings("input").attr("value",$(this).attr("id"));
        getDataSubDistrict(id_district);
    }
    else if(get_parent_id == "select-subdistrict"){
        $(this).parent().siblings("input").attr("value",$(this).attr("id"));
    }

    // parent status select condition
    else if ( get_parent_id == "select-status" || get_parent_id == "using-product-select" || get_parent_id == "gestational-age-select"){
        $(this).parent().siblings("input").attr("value",$(this).attr("value")).val($.trim($(this).text()));
        $(this).parent().siblings("input[type='hidden']").attr("value",$(this).attr("value"));
    }

    if ($(this).parent().siblings("input").hasClass("child-product-input")){
        $(this).parent().siblings("input").attr("value",$(this).attr("value")).val($.trim($(this).text()));
        $(this).parent().siblings("input[type='hidden']").attr("value",$(this).attr("value"));
    }
});

// function check input value
$(document).on('blur', '.input-wrapper input , .input-wrapper textarea', function() {
    checkVal();
});
$('#check-agreement').click(function(){
    checkVal();
});
$('.select-input').each(function(){
    $(this).change(function(){
        console.log($(this).attr("id"));
    });
});

var empty_count = 0;
function checkVal(){
    empty_count = 0;
    $(".input-wrapper input , .input-wrapper textarea").not(":hidden , #old-password , #new-password , #confirm-new-password").each(function(){
        if( $(this).val() === "" ){
            empty_count++;
        }
    });
    if(empty_count === 0 && $("#check-agreement").is(":checked")){
        $(".btn-update").removeClass("disabled").removeAttr("disabled");
    }
    else{
        $(".btn-update").addClass("disabled").attr("disabled","disabled");
    }
}


// get data province , city , and subdistrict
const getDataProvince = () => {
    fetch('https://nutriclub.staging.salt.id/RewardsApi/api/Account/ListProvince')
        .then(response => response.json())
        .then(data => {
            let item = data.Value;
            item.forEach((item) => {
                $('#select-province').append(`
                    ${item.ProvinceName != "-" ? `
                        <li id="${item.ID}">${item.ProvinceName}</li>
                    ` : ""}    
                `);
            });
        })
        .catch(err => {
            console.error('FAILED TO FETCH PROVINCE DATA');
            console.trace(err);
        });
}

const getDataDistrict = (id) => {
    fetch('https://nutriclub.staging.salt.id/RewardsApi/api/Account/ListDistrict?ProvinceID='+id)
        .then(response => response.json())
        .then(data => {
            let item = data.Value;
            item.forEach((item) => {
                $('#select-city').append(`
                    ${item.DistrictName != "-" ? `
                        <li id="${item.ID}">${item.DistrictName}</li>
                    ` : ""}    
                `);
            });
        })
        .catch(err => {
            console.error('FAILED TO FETCH DISTRICT DATA');
            console.trace(err);
        });
}

const getDataSubDistrict = (id) => {
    fetch('https://nutriclub.staging.salt.id/RewardsApi/api/Account/ListSubDistrict?DistrictID='+id)
        .then(response => response.json())
        .then(data => {
            let item = data.Value;
            item.forEach((item) => {
                $('#select-subdistrict').append(`
                    ${item.SubDistrictName != "-" ? `
                        <li id="${item.ID}">${item.SubDistrictName}</li>
                    ` : ""}    
                `);
            });
        })
        .catch(err => {
            console.error('FAILED TO FETCH SUBDISTRICT DATA');
            console.trace(err);
        });
}

// set default checked checkbox subscription
if ($("#check-receivement").attr("value")=="agree"){
    $("#check-receivement").attr("checked","checked");
}
else if ($("#check-receivement").attr("value")=="disagree"){
    $("#check-receivement").removeAttr("checked");
}

// button generate child form
var idxChild = $('.child-group .child-wrapper').length;
$(".add-child").on("click" , function(){
    idxChild++;

    var child = $($('.child-group .child-wrapper').first()).clone();
    
    child.find(".set-id-child").attr("name", "Childs["+(idxChild-1)+"][ID]").attr("id", "set-id-"+idxChild).val("");
    child.find(".set-id-parent").attr("name", "Childs["+(idxChild-1)+"][ParentID]").attr("id", "set-parentId-"+idxChild);
    child.find(".child-title").html("Data Anak "+idxChild);
    child.find(".child-name-label").attr("for", "child-name-"+idxChild);
    child.find(".child-name-input").attr("name", "Childs["+(idxChild-1)+"][Name]").attr("id", "child-name-"+idxChild).attr("value","");
    child.find(".child-birth-label").attr("for", "child-birth-date"+idxChild);
    child.find(".child-birth-input").attr("name", "Childs["+(idxChild-1)+"][Birthdate]").attr("id", "child-birth-date-"+idxChild).removeClass("hasDatepicker").attr("value","");
    child.find(".child-product-label").attr("for", "product-before-"+idxChild);
    child.find(".hidden-child-product").attr("name", "Childs["+(idxChild-1)+"][ProductBeforeID]").val("");
    child.find(".child-product-input").attr("id", "product-before-"+idxChild).attr("value","");

    
    child.appendTo(".child-group").hide().fadeIn();
    checkVal();
});