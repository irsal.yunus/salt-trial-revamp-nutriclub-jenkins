


$(document).ready(function(){
    if($(window).width() < 375 ){
        $(".video-lists__content-summary").slick()
    }
})

// initialize datepicker
$(function () {
    $(".date").datepicker({
        changeMonth: true,
        minDate: "-9m",
        maxDate: new Date(),
        dateFormat : "m/dd/yy"
    });
});


//
// hide second step
$(".second-step").hide();

// date value
let date;
$(".date").change(function () {
    date = $(this).val();
    return date;
});

for (let i = 20; i <= 28; i++) {
    if (i == 28) {
        $("#drop-down-trigger").append(`
        <option selected="28" value="28">28 Hari</option>
    `);
    } else {
        $("#drop-down-trigger").append(`
        <option value="${i}">${i} Hari</option>
    `);
    }
}

function countingPregnancy(number) {
    let dropDownValue = parseInt($("#drop-down-trigger").val());
    let formulaCounting = number + (dropDownValue - 28);
    let result = formulaCounting;
    return result;
}

function formatToIndonesia(date) {
    var year = date.getFullYear();
    var month = date.getMonth();
    var date = date.getDate();
    switch (month) {
        case 0:
            month = "Januari";
            break;
        case 1:
            month = "Februari";
            break;
        case 2:
            month = "Maret";
            break;
        case 3:
            month = "April";
            break;
        case 4:
            month = "Mei";
            break;
        case 5:
            month = "Juni";
            break;
        case 6:
            month = "Juli";
            break;
        case 7:
            month = "Agustus";
            break;
        case 8:
            month = "September";
            break;
        case 9:
            month = "Oktober";
            break;
        case 10:
            month = "November";
            break;
        case 11:
            month = "Desember";
            break;
    }
    let result = `${date} ${month} ${year}`
    console.log("resultnya adalah", result);
    return result;
}

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

function sumDays(number) {
    let result = formatToIndonesia(addDays(date, countingPregnancy(number)));
    return result;
}

function sumDaysToTimeStamp(number){
    let Result = addDays(date, countingPregnancy(number));
    let TimeStampResult = Date.parse(Result) / 1000;
    return TimeStampResult;
}

$(".next-step").click(function () {
    if($(".date").val() === ""){
        $(".date").addClass("is-invalid")
    }else{
        postDataLayer(
            "Hitung",
            "My Pregnancy Today",
            "my-pregnancy-today-calculator",
            "next-step-counting"
        )
        $(".first-step").hide();
        $(".second-step").fadeIn();
        // conception
        $(".conception").html(sumDays(14));
        // first trimester
        $(".trimester-one").html(sumDays(0));
        // second trimester
        $(".trimester-two").html(sumDays(95));
        // third trimester
        $(".trimester-three").html(sumDays(189));
        // birthdate
        $(".birthdate").html(sumDays(280));
    }
});

$(".count-again").click(function () {
    $(".second-step").hide();
    $(".date").removeClass("is-invalid")
    $(".first-step").fadeIn();
    postDataLayer(
        "Hitung Ulang",
        "My Pregnancy Today",
        "my-pregnancy-today-calculator",
        "Hitung Ulang"
    )
});

$(".go-to-homepage").click(function(){
    if(IS_LOGGED_IN === "true"){
        postDataLayer(
            "Kunjungi Profile",
            "My Pregnancy Today",
            "my-pregnancy-today-calculator",
            "Kunjungi Profile"
        )
        $.post("/my-pregnancy-today/haid/cycle", {
            id : USER_REWARDS_ID,
            name : NAME,
            email: EMAIL,
            conceptDate : sumDaysToTimeStamp(14),
            trimesterOne : sumDaysToTimeStamp(0),
            trimesterTwo : sumDaysToTimeStamp(95),
            trimesterThree : sumDaysToTimeStamp(189),
            birthDate : sumDaysToTimeStamp(280),
        }).done(function(data){
            console.log("data : ", data)
        });
    }
});

