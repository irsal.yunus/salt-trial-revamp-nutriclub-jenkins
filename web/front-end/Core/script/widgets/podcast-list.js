function getPodcastList(){
    let activePage = Number($(".pagination-container .active-page").val());
	let totalPages = Number($(".pagination-container .total-page").val());
	$(".podcast-preloader").show();
	$("#nextPage").val(activePage);

	// get form-id from anchor tag with class name card__articles-button-load-more
	let key = $("input#nextPage").attr("form-id");
	// url for geting the load more tag
	let url = `/podcast/load/more?${$(`form#${key}`).serialize()}`;
	// request with get 
	$.ajax({
		url : url,
		cache : true,
		typed : "GET",
		success : function(response) {
            // append In the row with Id card article
			if(response.success == true) {
				// parsing next page value to integer
				renderPodcast(response.data);
				initPagination();
			} else {
				// hide button if response success is false
				console.log(`error: ${response}`);
			}
		}
	});
}

function renderPodcast(dataVideo){
    // append element with new resprenonse
	$(".podcast__list").empty().append(dataVideo);
    $(".podcast-preloader").hide();
}

function disableSomeButton() {
    $(document).ready(function () {
      if (IS_LOGGED_IN === "false") {
        $(this).find(".page-link.btn-last")
          .attr("onclick", `alert("${message}")`)
          .unbind("onclick")
          .addClass("disabled");
        if ($("[page-target=1]").parents(".page-item").hasClass("active")) {
          $(this).find(".page-link.btn-last")
            .attr("onclick", `alert("${message}")`)
            .unbind("onclick");
          $("[page-target=3]").attr("onclick", `alert("${message}")`).unbind("onclick");
        } else if (($("[page-target=2]").parents(".page-item").hasClass("active"))) {
          $(this).find(".page-link.btn-next-arrow")
            .attr("onclick", `alert("${message}")`)
            .unbind("onclick");
          $(this).find(".page-link.btn-last")
            .attr("onclick", `alert("${message}")`)
            .unbind("onclick");
          $("[page-target=3]").attr("onclick", `alert("${message}")`).unbind("onclick");
        }
      }
    });
  }

function initPagination(){
    let totalPages = Number($(".pagination-container .total-page").val());
	let activePage = Number($(".pagination-container .active-page").val());
	let paginationSize = 3;
	let renderedPagination = renderPagination(totalPages, activePage, paginationSize, "getPodcastList");
	$(".pagination-container .pagination").html(renderedPagination);
	if($(".podcast__item").length >= 4){
		$(".podcast__list").addClass("d-flex justify-content-between");
	}else{
		$(".podcast__list").removeClass("d-flex justify-content-between");
	}
	disableSomeButton();
}

$(document).ready(function() {
	initPagination();
});