function getVideoList(){
    let activePage = Number($(".pagination-container .active-page").val());
	let totalPages = Number($(".pagination-container .total-page").val());
	$(".video-preloader").show();
	$("#nextPage").val(activePage);

	// get form-id from anchor tag with class name card__articles-button-load-more
	let key = $("input#nextPage").attr("form-id");
	// url for geting the load more tag
	let url = `/video/load/more?${$(`form#${key}`).serialize()}`;
	// request with get 
	$.ajax({
		url : url,
		cache : true,
		typed : "GET",
		success : function(response) {
            // append In the row with Id card article
			if(response.success == true) {
				// parsing next page value to integer
				renderVideo(response.data);
				initPagination();
			} else {
				// hide button if response success is false
				console.log(`error: ${response}`);
			}
		}
	});
}

function renderVideo(dataVideo){
    // append element with new response
	$(".card-video__list-items").empty().append(dataVideo);
    $(".video-preloader").hide();
}
function disableSomeButton() {
    $(document).ready(function () {
      if (IS_LOGGED_IN === "false") {
        $(this).find(".page-link.btn-last")
          .attr("onclick", `alert("${message}")`)
          .unbind("onclick")
          .addClass("disabled");
        if ($("[page-target=1]").parents(".page-item").hasClass("active")) {
          $(this).find(".page-link.btn-last")
            .attr("onclick", `alert("${message}")`)
            .unbind("onclick");
          $("[page-target=3]").attr("onclick", `alert("${message}")`).unbind("onclick");
        } else if (($("[page-target=2]").parents(".page-item").hasClass("active"))) {
          $(this).find(".page-link.btn-next-arrow")
            .attr("onclick", `alert("${message}")`)
            .unbind("onclick");
          $(this).find(".page-link.btn-last")
            .attr("onclick", `alert("${message}")`)
            .unbind("onclick");
          $("[page-target=3]").attr("onclick", `alert("${message}")`).unbind("onclick");
        }
      }
    });
  }
function initPagination(){
    let totalPages = Number($(".pagination-container .total-page").val());
	let activePage = Number($(".pagination-container .active-page").val());
	let paginationSize = 3;
	let renderedPagination = renderPagination(totalPages, activePage, paginationSize, "getVideoList");
	$(".pagination-container .pagination").html(renderedPagination);
	$(".card-video__link").last().addClass('border-none');
	disableSomeButton();
}

$(document).ready(function() {
	initPagination();
});