$(document).ready(function(){
    $(".date-list").slick({
        slidesToShow: 8,
        slidesToScroll: 1,
        infinite: false,
        arrows: true,
        dots: false,
        responsive: [{
            breakpoint: 750,
            settings: "unslick"
        }]
    });
});