$(".search__queries-text-keyword").html(localStorage.getItem("searchKey"));
// remove last border
function removeLastBorder(classname){
	$(classname).last().addClass("border-none");
}
$(".header-result").removeClass("d-none").addClass("d-block");
// disabled some pagination 

function disableSomeButton() {
	let message = "Register terlebih dahulu untuk melanjutkan";
	$(document).ready(function () {
		if (IS_LOGGED_IN === "false") {
			$("a.btn-last")
                .attr("onclick", `alert("${message}")`)
                .unbind("onclick")
				.addClass("disabled");
				$("[page-target=3]").attr("onclick", `alert("${message}")`).unbind("onclick");
				
			if ($("[page-target=1]").parents(".page-item").hasClass("active")) {
				$("a.btn-last")
					.attr("onclick", `alert("${message}")`)
					.unbind("onclick");
				$("[page-target=3]").attr("onclick", `alert("${message}")`).unbind("onclick");
			}
			
			if (($("[page-target=2]").parents(".page-item").hasClass("active"))) {
				$("a.btn-next-arrow")
					.attr("onclick", `alert("${message}")`)
					.unbind("onclick");
				$("a.btn-last")
					.attr("onclick", `alert("${message}")`)
					.unbind("onclick");
				$("[page-target=3]").attr("onclick", `alert("${message}")`).unbind("onclick");
			}

			
		}
	});
}

/* PAGINATION */
$(document).ready(function() {
	initPagination_article();
	initPagination_video();
	initPagination_podcast();
	initPagination_ebook();
	initPagination_faq();

	removeLastBorder(".search__articles-item");
	removeLastBorder(".search__articles-video-item");
	removeLastBorder(".search__articles-podcasts-item");
	removeLastBorder(".search__articles-ebook-border");
	removeLastBorder(".search__faq-item")
});

/* SEARCH - ARTICLE */
function getList_article(){
    let activePage = Number($(".search__articles-pagination .pagination-container .active-page").val());
	//$('.video-preloader').show();
	$("#nextPage-article").val(activePage);

	// get form-id from anchor tag with class name card__articles-button-load-more
	let key = $("#nextPage-article").attr("form-id");
	// url for geting the load more tag
	let url = `/search/load/more?${$(`form#${key}`).serialize()}`;
	// request with get 
	$.ajax({
		url : url,
		cache : true,
		typed : "GET",
		success : function(response) {
            // append In the row with Id card article
            console.log(response);
			if(response.success == true) {
				// parsing next page value to integer
				renderList_article(response.data);
				initPagination_article();
			} else {
				// hide button if response success is false
				console.log(`error: ${response}`);
			}
		}
	});
}

function renderList_article(data){
    // append element with new response
    console.log(data);
	$(".search__articles-lists").empty().html(data);
	//$('.video-preloader').hide();
	removeLastBorder(".search__articles-item");
}

function initPagination_article(){
    let totalPages = Number($(".search__articles-pagination .pagination-container .total-page").val());
	let activePage = Number($(".search__articles-pagination .pagination-container .active-page").val());
	let paginationSize = 3;
	disableSomeButton();
	if(totalPages === 1 || totalPages === 0){
		$(".search__articles-pagination").hide();
	}else{
		let renderedPagination = renderPagination(totalPages, activePage, paginationSize, "getList_article");
		$(".search__articles-pagination .pagination-container .pagination").html(renderedPagination);
	}
}
/* END SEARCH - ARTICLE */


/* SEARCH - VIDEO */
function getList_video(){
    let activePage = Number($(".search__video-pagination .pagination-container .active-page").val());
	//$('.video-preloader').show();
	$("#nextPage-video").val(activePage);

	// get form-id from anchor tag with class name card__articles-button-load-more
	let key = $("#nextPage-video").attr("form-id");
	// url for geting the load more tag
	let url = `/search/load/more?${$(`form#${key}`).serialize()}`;
	// request with get 
	$.ajax({
		url : url,
		cache : true,
		typed : "GET",
		success : function(response) {
            // append In the row with Id card article
            console.log(response);
			if(response.success == true) {
				// parsing next page value to integer
				renderList_video(response.data);
				initPagination_video();
			} else {
				// hide button if response success is false
				console.log(`error: ${response}`);
			}
		}
	});
}

function renderList_video(data){
    // append element with new response
    console.log(data.length);
	$(".search__articles-video-lists").empty().html(data).not(":first").remove();
	//$('.video-preloader').hide();
	removeLastBorder(".search__articles-video-item");
}

function initPagination_video(){
    let totalPages = Number($(".search__video-pagination .pagination-container .total-page").val());
	let activePage = Number($(".search__video-pagination .pagination-container .active-page").val());
	let paginationSize = 3;
	disableSomeButton();
	if(totalPages === 1 || totalPages === 0){
		$(".search__video-pagination").hide();
	}else{
		let renderedPagination = renderPagination(totalPages, activePage, paginationSize, "getList_video");
	$(".search__video-pagination .pagination-container .pagination").html(renderedPagination);
	}
	
}
/* END SEARCH - VIDEO */


/* SEARCH - PODCAST */
function getList_podcast(){
    let activePage = Number($(".search__podcast-pagination .pagination-container .active-page").val());
	//$('.video-preloader').show();
	$("#nextPage-podcast").val(activePage);

	// get form-id from anchor tag with class name card__articles-button-load-more
	let key = $("#nextPage-podcast").attr("form-id");
	// url for geting the load more tag
	let url = `/search/load/more?${$(`form#${key}`).serialize()}`;
	// request with get 
	$.ajax({
		url : url,
		cache : true,
		typed : "GET",
		success : function(response) {
            // append In the row with Id card article
            console.log(response);
			if(response.success == true) {
				// parsing next page value to integer
				renderList_podcast(response.data);
				initPagination_podcast();
			} else {
				// hide button if response success is false
				console.log(`error: ${response}`);
			}
		}
	});
}

function renderList_podcast(data){
    // append element with new response
    console.log(data);
	$(".search__podcast-video-lists").empty().html(data).not(":first").remove();
	//$('.video-preloader').hide();
	removeLastBorder(".search__articles-podcasts-item");
}

function initPagination_podcast(){
    let totalPages = Number($(".search__video-pagination .pagination-container .total-page").val());
	let activePage = Number($(".search__video-pagination .pagination-container .active-page").val());
	let paginationSize = 3;
	disableSomeButton();
	if(totalPages === 1 || totalPages === 0){
		$(".search__podcast-pagination").hide();
	}else{
		let renderedPagination = renderPagination(totalPages, activePage, paginationSize, "getList_podcast");
		$(".search__podcast-pagination .pagination-container .pagination").html(renderedPagination);
	}
}
/* END SEARCH - PODCAST */


/* SEARCH - EBOOK */
function getList_ebook(){
    let activePage = Number($(".search__ebook-pagination .pagination-container .active-page").val());
	//$('.video-preloader').show();
	$("#nextPage-ebook").val(activePage);

	// get form-id from anchor tag with class name card__articles-button-load-more
	let key = $("#nextPage-ebook").attr("form-id");
	// url for geting the load more tag
	let url = `/search/load/more?${$(`form#${key}`).serialize()}`;
	// request with get 
	$.ajax({
		url : url,
		cache : true,
		typed : "GET",
		success : function(response) {
            // append In the row with Id card article
            console.log(response);
			if(response.success == true) {
				// parsing next page value to integer
				renderList_ebook(response.data);
				initPagination_ebook();
			} else {
				// hide button if response success is false
				console.log(`error: ${response}`);
			}
		}
	});
}

function renderList_ebook(data){
    // append element with new response
    console.log(data);
	$(".search__ebook-video-lists").empty().html(data).not(":first").remove();
	//$('.video-preloader').hide();
	removeLastBorder(".search__articles-ebook-border");
}

function initPagination_ebook(){
    let totalPages = Number($(".search__ebook-pagination .pagination-container .total-page").val());
	let activePage = Number($(".search__ebook-pagination .pagination-container .active-page").val());
	let paginationSize = 3;
	disableSomeButton();
	if(totalPages === 1 || totalPages === 0){
		$(".search__ebook-pagination").hide();
	}else{
		let renderedPagination = renderPagination(totalPages, activePage, paginationSize, "getList_ebook");
		$(".search__ebook-pagination .pagination-container .pagination").html(renderedPagination);
	}
}
/* END SEARCH - PODCAST */


/* SEARCH - FAQ */
function getList_faq(){
    let activePage = Number($(".search_faq-pagination .pagination-container .active-page").val());
	//$('.video-preloader').show();
	$("#nextPage-faq").val(activePage);

	// get form-id from anchor tag with class name card__articles-button-load-more
	let key = $("#nextPage-faq").attr("form-id");
	// url for geting the load more tag
	let url = `/expert-advisor/faq/load/more?${$(`form#${key}`).serialize()}`;
	// request with get 
	$.ajax({
		url : url,
		cache : true,
		typed : "GET",
		success : function(response) {
            // append In the row with Id card article
            console.log(response);
			if(response.success == true) {
				// parsing next page value to integer
				renderList_faq(response.data);
				initPagination_faq();
			} else {
				// hide button if response success is false
				console.log(`error: ${response}`);
			}
		}
	});
}

function renderList_faq(data){
    // append element with new response
    console.log(data);
	$(".search__faq").empty().html(data);
	//$('.video-preloader').hide();
	removeLastBorder(".search__faq-item");
}

function initPagination_faq(){
    let totalPages = Number($(".search_faq-pagination .pagination-container .total-page").val());
	let activePage = Number($(".search_faq-pagination .pagination-container .active-page").val());
	let paginationSize = 3;
	disableSomeButton();
	if(totalPages === 1 || totalPages === 0){
		$(".search_faq-pagination").hide();
	}else{
		let renderedPagination = renderPagination(totalPages, activePage, paginationSize, "getList_faq");
		$(".search_faq-pagination .pagination-container .pagination").html(renderedPagination);
	}
}
/* END SEARCH - FAQ */