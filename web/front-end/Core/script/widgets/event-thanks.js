function goToDetail() {
    $('html, body').animate({
        scrollTop: $('#event-detail').offset().top - 100,
    }, 500, 'linear' )
}

$(function() {
    var formatDate = function(data) {
        var date = new Date(data)
        return date.toISOString().replace(/-|:|\.\d+/g, '');
    }

    $('#addToCalendar').click(function() {
        var self = $(this),
            startDate = formatDate(self.data('start')),
            endDate = formatDate(self.data('end'));

        var href = encodeURI([
            'https://www.google.com/calendar/render',
            '?action=TEMPLATE',
            '&text=' + (self.data('title') || ''),
            '&dates=' + (startDate || ''),
            '/' + (endDate || ''),
            '&details=' + (self.data('description') || ''),
            '&location=' + (self.data('location') || ''),
            '&sprop=&sprop=name:'
          ].join(''));

        window.open(href, '_blank');
    })
})