function loadJqueryUI(callback) {
    var dURL = location.origin;
    var a=document.createElement('link');
    a.rel='stylesheet';
    a.href=dURL + '/assets/vendor/jquery-ui/jquery-ui.min.css';
    document.getElementsByTagName("head")[0].appendChild(a);

    var b = document.createElement('script');
    b.src = dURL + '/assets/vendor/jquery-ui/jquery-ui.min.js';
    b.onload = () => callback(b);
    document.body.appendChild(b);
}

if(!Modernizr.inputtypes.date) {
    loadJqueryUI(function() {
        $('input[type=date]').datepicker({
            maxDate: new Date(),
            dateFormat: 'dd/mm/yy',
            altFormat  : "dd/mm/yy",
            altField   : '.jsDateFormat',
        });
    })
}