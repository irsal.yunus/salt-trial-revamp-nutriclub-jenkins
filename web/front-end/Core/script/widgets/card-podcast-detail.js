$(document).ready(function() {
    const screenWidth = window.innerWidth;
    const player = $("#soundcloud-player");

    let soundId = player.data("value");
    let playerSrc = `https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/${soundId}&color=%2326419e&auto_play=false&hide_related=true&show_comments=false&show_user=false&show_reposts=false&show_teaser=true`;

    
    initSoundcloud();
    
    screenWidth < 768 ? player.attr("src", playerSrc + "&visual=true") : player.attr("src", playerSrc);
});

function initSoundcloud() {
    // Soundcloud Widget API
    let iframeElement = document.querySelector("#soundcloud-player");
    let widget = SC.Widget(iframeElement);

    const playBtn = $(".play-btn");

    playBtn.on("click", function() {
        widget.toggle();
    });
}

if($(".podcast__content-btn-toggler").hasClass("collapsed")){
    $(".podcast__content-btn-toggler").removeClass("collapsed");
    $(".podcast__content-btn-toggler").parent("#podcast-description-mobile").removeClass("show-description");
  }else{
    $(".podcast__content-btn-toggler").addClass("collapsed");
  }