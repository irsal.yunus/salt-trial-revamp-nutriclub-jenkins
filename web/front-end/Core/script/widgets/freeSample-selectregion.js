var provinceData = [];
let dataToPush;
// Get Province
$(function() {
    $.ajax({
        url: "/province/",
        success: function(result) {
            if (result.StatusCode == '00') {
                var data = result.Value;
                dataToPush = [...provinceData, ...data];
                localStorage.setItem("provinceData", JSON.stringify(dataToPush)); 
            }else{
                dataToPush = []
            }
        }
    });
});

// Display District 
$(document).on('keyup', '.province__input', function (ev) {
    var self = $(this).val();
    if(self.length > 3) {
        let provinces = dataToPush.filter(province => province.ProvinceName.toLowerCase().includes(self.toLowerCase()));
        let provinceObjID;
        provinces.forEach(obj => 
            provinceObjID = obj.ID    
        )
        if (provinceObjID !== undefined ) {
            $.ajax({
                url: "/province/district/" + provinceObjID,    
                success: function(result) {
                    if (result.StatusCode == '00') {
                        $( ".dropdown-province-items" ).empty();
                        var data = result.Value;
                        for(var i=0; i < data.length; i++) {
                            $( ".dropdown-province-items" ).append( "<li DistrictID="+data[i].ID+" ProvinceID="+data[i].ProvinceID+">"+data[i].DistrictName+"</li>" );
                            $(".dropdown-province-items li").text(function(index, currentText) {
                                return currentText.toLowerCase();
                            }).css('text-transform', 'capitalize').css('cursor', 'pointer');
                        }
                    }
                }
            });
        }
    } else {
        $( ".dropdown-province-items" ).empty();
    }
});

$(document).on("click", ".dropdown-province-items li", function () {
    var DistrictID = $(this).attr('DistrictID');
    var ProvinceID = $(this).attr('ProvinceID');
    let provinces = dataToPush.filter(province => province.ID == ProvinceID);
    let provinceObjName;
    provinces.forEach(obj => 
        provinceObjName = obj.ProvinceName    
    )
    var district_name = $(this).text(); 
    $(".btn-province").text(district_name).css('text-transform', 'capitalize')
    .css('color', '#495057').attr('dataProvinsi', ProvinceID);
    $("#DistrictID").val(DistrictID);
    $("#ProvinceID").val(ProvinceID);
    $("#DistrictName").val(district_name);
    $("#ProvinceName").val(provinceObjName);
});






 