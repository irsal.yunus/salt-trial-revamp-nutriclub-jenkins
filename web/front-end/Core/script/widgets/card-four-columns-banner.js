$(".podcast__list").slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll : 1,
  // variableWidth: true,
  // adaptiveHeight : true,
  arrows : true,
  responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll : 1,
          arrows : false,
          variableWidth: false,
        }
      },
      {
        breakpoint: 280,
        settings: {
          slidesToShow: 1,
          slidesToScroll : 1,
          arrows : false,
          variableWidth: false,
        }
      }
    ]
});