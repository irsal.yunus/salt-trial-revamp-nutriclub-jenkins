$("a[is-loadmore='true']").click(function(event){
    event.preventDefault();
    // get form-id from anchor tag with class name card__articles-button-load-more
    let key = $(this).attr("form-id");
    // url for geting the load more tag
    let url = `/article/load/more?${$(`form#${key}`).serialize()}`;
    // declare button as this so it can be call in the ajax or another function
    let buttonLoadMore = $(this);
    // request with get 
    $.ajax({
        url : url,
        cache : true,
        typed : "GET",
        success : function(response){
            // append In the row with Id card article
            if(response.success == true){
                // parsing next page value to integer
                let nextPageValue = parseInt($("#nextPage").val());
                // take nextPage value and plus one 
                $($("#nextPage").val(nextPageValue += 1));
                // append element with new response
                $(`div#${key}`).append(response.data);
            }else{
                // hide button if response success is false
                buttonLoadMore.hide();
            }
        }
    });
});

$(document).ready(function(){
    let checkActive = 1;
    $(".pagination-number").each(function(){
        console.log($(this).attr("page"));
        let getPrevData = Number($("#prev-pagination").data("page"));
        let getNextData = Number($("#next-pagination").data("page"));
        if($(this).attr("page") >= getNextData || $(this).attr("page") <= getPrevData) {
            $(this).css("display", "none");
        } else{
            $(this).addClass("show-pagination");
        }
    });
    function onGetDataPagination(updateDataPage, updateDataPrev){
        $(".pagination-number").each(function(){
            if($(this).attr("page") >= updateDataPage || $(this).attr("page") <= updateDataPrev) {
                $(this).css("display", "none");
                $(this).removeClass("show-pagination");
            } else{
                $(this).css("display", "block");
                $(this).addClass("show-pagination");
            }
        });
    }
    $("#next-pagination").on("click",function(){
        let getActive = Number($(".pagination-number.active").attr("page")) + 1;
        let getMaxPage = getActive;
        let $parent = $(this).parent("li");
        let getParent = $parent.parent(".pagination");
        
        // e.preventDefault()
        let getDataPrev = Number($("#prev-pagination").attr("data-page"));
        let getDataPage = Number($(this).attr("data-page"));
        let updateDataPage = getDataPage + 1;
        let updateDataPrev = getDataPrev + 1;
        // console.log('getDataPage', updateDataPage)
        // $(this).attr("data-page", updateDataPage)
        // $("#prev-pagination").attr("data-page", updateDataPrev)
        if($(this).attr("data-page") >= (Number(getParent.attr("max-page")) + 1 )){
            updateDataPrev = (Number(getParent.attr("max-page")) - 3 );
            updateDataPage = (Number(getParent.attr("max-page"))+ 1 );
            $("#prev-pagination").attr("data-page", (Number(getParent.attr("max-page")) - 3 ));
            if(getActive === (Number(getParent.attr("max-page")))){
                $parent.siblings("li").find(".pagination-number").removeClass("active");
                $parent.siblings("li").find(".pagination-number[page="+Number(getParent.attr("max-page"))+"]").addClass("active"); 
            }

        } else{
            $(this).attr("data-page", updateDataPage);
            $("#prev-pagination").attr("data-page", updateDataPrev);
            $parent.siblings("li").find(".pagination-number").removeClass("active");
            $parent.siblings("li").find(".pagination-number[page="+getActive+"]").addClass("active");
        }
        console.log("udah sepuluh,", getMaxPage);
        onGetDataPagination(updateDataPage, updateDataPrev);
    });
    $("#prev-pagination").on("click",function(){
        let getActive = Number($(".pagination-number.active").attr("page")) - 1;
        let $parent = $(this).parent("li");
        let getParent = $parent.parent(".pagination");
        $parent.siblings("li").find(".pagination-number").removeClass("active");
        $parent.siblings("li").find(".pagination-number[page="+getActive+"]").addClass("active");
        let getDataPrev = Number($("#next-pagination").attr("data-page"));
            let getDataPage = Number($(this).attr("data-page"));
            let updateDataPage = getDataPage - 1;
            let updateDataPrev = getDataPrev - 1;
            console.log("getDataPage", updateDataPage);
            $(this).attr("data-page", updateDataPage);
            $("#next-pagination").attr("data-page", updateDataPrev);
            $("#next-pagination").attr("disabled", false);
            onGetDataPagination(updateDataPrev, updateDataPage);
        if($(this).attr("data-page") <= "0"){
            console.log("udah nol");
            $(this).attr("disabled", true);
        }
    });
    $(".pagination-number").click(function(){
        $(".pagination-number").removeClass("active");
        $(this).addClass("active");
        checkActive = Number($(this).attr("page"));
    });
});