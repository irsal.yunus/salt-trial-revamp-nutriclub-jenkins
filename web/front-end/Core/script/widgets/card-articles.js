function getArticleList() {
	let activePage = Number($(".pagination-container .active-page").val());
	let totalPages = Number($(".pagination-container .total-page").val());
	$(".article-preloader").addClass("show");
	$("#nextPage").val(activePage);

	// get form-id from anchor tag with class name card__articles-button-load-more
	let key = $("input#nextPage").attr("form-id");
	// url for geting the load more tag
	let url = `/article/load/more?${$(`form#${key}`).serialize()}`;
	// request with get 
	$.ajax({
		url: url,
		cache: true,
		typed: "GET",
		success: function (response) {
			// append In the row with Id card article
			if (response.success == true) {
				// parsing next page value to integer

				renderArticle(response.data);
				initPagination();
			} else {
				// hide button if response success is false
				console.log(`error: ${response}`);
			}
		}
	});
}

function renderArticle(dataArticle) {
	// append element with new response
	$(".article-list").empty().append(dataArticle);
	$(".article-preloader").removeClass("show");
}

function disableSomeButton() {
	$(document).ready(function () {
        let totalPages = Number($(".pagination-container .total-page").val());
        if(totalPages <= 3){
            $(this).find(".btn-last").hide()
        }else if(totalPages == 1){
            $(this).find('.pagination-container').hide();
        }else{
            $(this).find(".btn-last").show();
            $(this).find('.pagination-container').show();
        }
		if (IS_LOGGED_IN === "false") {
			$(this).find(".page-link.btn-last")
				.attr("onclick", `alert("${message}")`)
				.unbind("onclick")
				.addClass("disabled");
			if ($("[page-target=1]").parents(".page-item").hasClass("active")) {
				$(this).find(".page-link.btn-last")
					.attr("onclick", `alert("${message}")`)
					.unbind("onclick");
				$("[page-target=3]").attr("onclick", `alert("${message}")`).unbind("onclick");
			} else if (($("[page-target=2]").parents(".page-item").hasClass("active"))) {
				$(this).find(".page-link.btn-next-arrow")
					.attr("onclick", `alert("${message}")`)
					.unbind("onclick");
				$(this).find(".page-link.btn-last")
					.attr("onclick", `alert("${message}")`)
					.unbind("onclick");
				$("[page-target=3]").attr("onclick", `alert("${message}")`).unbind("onclick");
			}
		}
	});
}

function initPagination() {
	let totalPages = Number($(".pagination-container .total-page").val());
	let activePage = Number($(".pagination-container .active-page").val());
	let paginationSize = 3;
	let renderedPagination = renderPagination(totalPages, activePage, paginationSize, "getArticleList");

	$(".pagination-container .pagination").html(renderedPagination);
	disableSomeButton();
}

$(document).ready(function () {
	initPagination();
});