$(window).scroll(function() {
    let scrollTime = $(this).scrollTop();
    if (scrollTime >= ($("#side-description").height() - $("footer").height())){
        $("#side-card").removeClass("position-fixed-bottom").addClass("position-absolute-bottom")
    }else{
        $("#side-card").removeClass("position-absolute-bottom").addClass("position-fixed-bottom")
    }
});

$("a[href^='#']").on("click", function() {
    var hash = this.hash;
    if($(hash).length){
        $("html, #side-description").animate({
            scrollTop: $(hash).offset().top - 80
        }, 2000)
    }else{
        console.log("target id is not found")
    }
})