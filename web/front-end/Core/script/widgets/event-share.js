var currentURL = window.location.href;

function shareWhatsapp() {   
}

$(function() {
    $('#copyClipboard').tooltip({
        trigger: 'click',
        placement: 'top'
    });

    function destroyToolip() {
        setTimeout(function() {
            $('#copyClipboard').tooltip('hide');
        }, 1000);
    }

    $('#copyClipboard').click(function() {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(currentURL).select();
        document.execCommand("copy");
        $temp.remove();
        $(this).tooltip('hide').attr('data-original-title', "Copied To Clipboard").tooltip('show');
        destroyToolip()
    })

    $('#shareWhatsapp').click(function() {
        var _url = encodeURIComponent(currentURL);
        window.location.assign('whatsapp://send?text=' + _url);
    })
})