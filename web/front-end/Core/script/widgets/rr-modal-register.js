$(document).ready(function () {
    // Construct URLSearchParams object instance from current URL querystring.
    var queryParams = new URLSearchParams(window.location.search);
    if (queryParams.get("method") == "POST") {
        $("#modal-register").modal("show");
        $("#main-register").hide();
        $("#success-register").fadeIn();
    }
});

function postRedemption(service,id) {
    window.location = `/royal-rewards/${service}/${id}?method=POST`;
}

function hideRegister () {
    window.location = '/royal-rewards/dashboard';
}

function hideFailRegister() {
    $("#modal-register").modal("hide");
    $("#main-register").show();
    $("#success-register").hide();
    $("#fail-register").hide();
}
