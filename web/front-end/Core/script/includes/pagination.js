function renderPagination(totalData, currentPage, paginationSize, funcEcexute) {
	var pager = getPager(totalData, currentPage, paginationSize);
	var pageEl = "";
  // $('.total-page').val(pager.totalPages);
  let activePage = Number($(".pagination-container .active-page").val());
  let totalPages = Number($(".pagination-container .total-page").val());
  let disabledButton = {
    prev: (activePage == 1) ? "disabled" : null,
    next: (activePage == totalPages) ? "disabled" : null
  };

	for(var p = 0; p < pager.pages.length; p++) {
		if(pager.currentPage === pager.pages[p]) {
			pageEl += "<li class=\"page-item active\"><a class=\"page-link\" type=\"page-number\" href=\"javascript:void(0)\" page-target=\"" + pager.pages[p] + "\" onclick=\"changePage(this," + funcEcexute + ")\">"+ pager.pages[p] +"</a></li>";
		} else {
			pageEl += "<li class=\"page-item\"><a class=\"page-link\" type=\"page-number\" href=\"javascript:void(0)\" page-target=\"" + pager.pages[p] + "\" onclick=\"changePage(this," + funcEcexute + ")\">"+ pager.pages[p] +"</a></li>";				
		}
	}

	var paginationElement = "<li class=\"page-item " + disabledButton.prev + "\"><a class=\"page-link\" type=\"page-first\" href=\"javascript:void(0)\" page-target=\"1\" onclick=\"changePage(this," + funcEcexute + ")\">First</a></li>" +
			"<li class=\"page-item " + disabledButton.prev + "\"><a class=\"page-link\" type=\"page-prev\" href=\"javascript:void(0)\" page-target=\"prev\" onclick=\"changePage(this," + funcEcexute + ")\"><i class=\"fa fa-caret-left\"></i></a></li>" +
			pageEl +
			"<li class=\"page-item " + disabledButton.next + "\"><a class=\"page-link btn-next-arrow\" type=\"page-next\" href=\"javascript:void(0)\" page-target=\"next\" onclick=\"changePage(this," + funcEcexute + ")\"><i class=\"fa fa-caret-right\"></i></a></li>" +
			"<li class=\"page-item " + disabledButton.next + "\"><a class=\"page-link btn-last\" type=\"page-last\" href=\"javascript:void(0)\" page-target=\"" + pager.totalPages + "\" onclick=\"changePage(this," + funcEcexute + ")\">Last</a></li>";
	
	return paginationElement;
}

function getPager(totalPages, currentPage, pageSize) {
	if(totalPages == 0) {
		totalPages = 1;
	}

	let centerPagination = Math.round(pageSize / 2);
	let lastPagination = (pageSize >= 5) ? 2 : 1;
	// ensure current page isn't out of range
	if(currentPage < 1) {
		currentPage = 1;
	}

	if(currentPage > totalPages) {
		currentPage = totalPages;
	}

	// calculate first page and last page
	let startPage, endPage;
	if(totalPages <= pageSize) {
		startPage = 1;
		endPage = totalPages;
	} else if(currentPage <= centerPagination) {
			startPage = 1;
			endPage = pageSize;
		} else if(currentPage + lastPagination >= totalPages) {
			startPage = totalPages - (pageSize - 1);
			endPage = totalPages;
		} else {
			startPage = currentPage - (centerPagination - 1);
			endPage = currentPage + (centerPagination - 1);
		}

	// create an array of pages to ng-repeat in the pager control
	const pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

	// return object with all pager properties required by the view
	return {
		currentPage: currentPage,
		pageSize: pageSize,
		totalPages: totalPages,
		startPage: startPage,
		endPage: endPage,
		pages: pages
	};
}

function changePage(el, functionExecute) {
	var currentEl = $(el).parents().eq(2).find(".active-page");
	var totalEl = $(el).parents().eq(2).find(".total-page");
	var type = $(el).attr("type");
	var pageTarget = Number($(el).attr("page-target"));
	var currentPage = Number(currentEl.val());
	var totalPage = Number(totalEl.val());

	if(type == "page-number" || type == "page-first" || type == "page-last") {
		currentPage = pageTarget;
	}
	if(type == "page-prev") {
		currentPage -= 1;

		if(currentPage < 1) {
			currentPage = 1;
		}
	}
	if(type == "page-next") {
		currentPage += 1;

		if(currentPage > totalPage) {
			currentPage = totalPage;
		}
	}

	currentEl.val(currentPage);
	// function yg dipanggil pas button di click
	functionExecute();
}