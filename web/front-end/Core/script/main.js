$(function () {
    $(".accordion_trigger")
        .unbind("click")
        .click(function () {
            $(this).toggleClass("acordion_active");
            $(".panel").addClass("slideInDown");
            // $(this).sibling(".panel").slideToggle();
            $(this).parents(".accordion-section").find(".panel").slideToggle();
            var otherMenuItems = $(".accordion_trigger").not($(this));
            otherMenuItems
                .parents(".accordion-section")
                .find(".panel")
                .slideUp();
            $(otherMenuItems).removeClass("acordion_active");
        });
});

$(".accordion-section").last().find(".panel").addClass("pb-5");

function hideMenuList() {
    $("#hamburger-menu").removeClass("d-none").addClass("d-block");
    $("#close-menu").removeClass("d-block").addClass("d-none");
    $(".hamburger-menu__lists")
        .removeClass("d-block animated fadeInLeft")
        .addClass("animated fadeOutLeft");
    $(".hamburger-menu__lists").css("position", "absolute");
}

function showMenuList() {
    $(".hamburger-menu__lists")
        .removeClass("d-none animated fadeOutLeft")
        .addClass("d-block animated fadeInLeft");
    $("#hamburger-menu").removeClass("d-block").addClass("d-none");
    $("#close-menu").removeClass("d-none").addClass("d-block");
    $(".hamburger-menu__lists").css("position", "fixed");
}

$("#hamburger-menu").click(() => {
    showMenuList();
});

$("#close-menu").click(() => {
    hideMenuList();
});
$(".close-service").click(() => {
    hideMenuList();
    $("html, body").animate(
        {
            scrollTop: $("#service").offset().top - 500,
        },
        200
    );
});
$(".close-question").click(() => {
    hideMenuList();
    $("html, body").animate(
        {
            scrollTop: $("#question").offset().top - 150,
        },
        200
    );
});
$(".close-contact").click(() => {
    hideMenuList();
    $("html, body").animate(
        {
            scrollTop: $("#contact").offset().top - 150,
        },
        200
    );
});
$(".close-feature").click(() => {
    hideMenuList();
    $("html, body").animate(
        {
            scrollTop: $("#feature").offset().top - 150,
        },
        200
    );
});

// header__search-submit is clicked (loop logo)
$(".header__search-submit").click(() => {
    $(".header-form").hide();
    $(".header-result").removeClass("d-none").addClass("d-block");
    $(".search__results").removeClass("d-none").addClass("d-block");
    $(".search__results").addClass("animated fadeInRight");
    $("#close-menu").removeClass("d-block").addClass("d-none");
    $(".hamburger-menu__lists")
        .removeClass("d-block animated fadeInLeft")
        .addClass("animated fadeOutLeft");
    $(".hamburger-menu__lists").css("position", "absolute");
    $(".header__search-submit").hide();
});

// redirect back to home
$("#back-menu").click(() => {
    $(".header-result").removeClass("d-block").addClass("d-none");
    $(".header-form").show();
    $(".search__results").removeClass("d-block").addClass("d-none");
    $(".header__search-submit").show();
    window.close();
});

// check if path is search
if (window.location.pathname == "/search") {
    $(".header-form").hide();
    $(".header-result").show();
    $(".search__results").css("visibility", "hidden");
    $(".search-form").val(localStorage.getItem("searchKey"));
}

// search-form
$(".search-form").keypress((event) => {
    if (event.which == "13") {
        localStorage.setItem("searchKey", $(".search-form").val());
        window.location.href = "/search";
    }
});

$("img").on("error", function () {
    $(this).attr("src", "https://via.placeholder.com/500x300");
});

$(".banner-slider__trigger").on("click", function () {
    var _src =
        "https://www.youtube.com/embed/" +
        $(this).attr("data-video") +
        "?autoplay=1";
    $("#modal-video__iframe").attr("src", _src);
    $("#modal-video").modal("show");
});

$("#modal-video").on("hide.bs.modal", function () {
    $("#modal-video__iframe").attr("src", "");
});

function alert(msg, title) {
    $("#modal-error").find(".modal-title").text(title || "Info");
    $("#modal-error p").html(msg);
    $("#modal-error").modal("show");
}

// if all document is click then close result lists
$(document).click(function () {
    $(".search__results-lists").hide();
});

$(".filter__article .filter__item a").each(function () {
    if ($(this).attr("href") == location.pathname) {
        $(this).addClass("located");
        $(".filter__article").animate(
            { scrollLeft: $(".located").position().left - 20 },
            500
        );
    }
});

if (window.location.href.indexOf("#tools") > 0) {
    setTimeout(function () {
        $("html, body").animate(
            {
                scrollTop: $("#tools").offset().top - 100,
            },
            200
        );
    }, 300);
}

function scrollDownToBottom(classname, offsetLength, Length) {
    $(`${classname}`).click(function () {
        hideMenuList();
        if (
            location.pathname.replace(/^\//, "") ==
                this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length
                ? target
                : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html,body").animate(
                    {
                        scrollTop: target.offset().top - offsetLength,
                    },
                    Length
                );
                return false;
            }
        }
    });
}

function storeDatalayerEmailValue() {
    let email = $("#user_email_datalayer");
    if (email.val() === "" && IS_LOGGED_IN === "false") {
        dataLayer.push({ email: email.val("BELUM LOGIN") });
    } else {
        dataLayer.push({ email: email.val() });
    }
}

$(document).ready(function () {
    scrollDownToBottom('a.header__caption[href*="#tools', 125, 1000);
    scrollDownToBottom(
        'a#pimcore-navigation-renderer-menu-82[href*="#tools"]',
        125,
        1000
    );
    storeDatalayerEmailValue();
});

// if (localStorage.getItem("floating") == 1) {
//     $(window).on("load", function () {
//         $("#floating").removeClass("animated fadeIn showUp");
//     });
// } else {
//     $(window).on("load", function () {
//         $("#floating").addClass("animated fadeIn showUp");
//     });
// }

$(document).ready(function () {
    $("#btn-floating").click(function () {
        $("#floating").addClass("animated fadeIn show");
    });
    $(".close").click(function () {
        localStorage.setItem("floating", 1);

        $("#floating").removeClass("animated fadeIn show showUp");
    });
});

const postDataLayer = (tools_button_name, tools_name, tools_step, tools_detail) => {
    dataLayer.push({
        "event": "toolsengagement",
        "tools_button_name": tools_button_name,
        "tools_name": tools_name,
        "tools_step": tools_step,
        "tools_detail": tools_detail
    });
}
