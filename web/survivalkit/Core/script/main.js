$( document ).ready(function() {

  $(function() {
    $(".item-list").unbind('click').click(function() {
      $( this ).toggleClass( "godown" );
      $('.list-dropdown').addClass('slideInDown');
      $(`#${ $(this).data("toggle-content") }`).stop().slideToggle();

      var otherMenuItems = $(".item-list").not($(this));
      otherMenuItems.children(".list-dropdown").slideUp();
      $(otherMenuItems).removeClass("godown");
    });

  });

  // ====================================================
  $(function() {
    $(".list-of-content").unbind('click').click(function() {
      $( this ).toggleClass( "godown" );
      $('.list-of-desc').addClass('slideInDown');
      $(`#${ $(this).data("toggle-content") }`).stop().slideToggle();

      var otherMenuItems = $(".list-of-content").not($(this));
      otherMenuItems.children(".list-of-desc").slideUp();
      $(otherMenuItems).removeClass("godown");
    });

  });
  // ====================================================

  $('.btn-search').on('click', function(){
    $('.menu-item:last-child').toggleClass('inputbar');
  });

  // slide menu
  $('.login-navbar').click(function(){
    $('.navbar').addClass('open');
    $('.push-right').addClass('pushed');
    $('body').css({
      'overflow': 'hidden'
    });
  });

  $('.new-closed').click(function(){
    $('.navbar').removeClass('open');
    $('.push-right').removeClass('pushed');
    $('body').css({
      'overflow': 'auto'
    });
  });

  // =================================================TAB ARTICLE
    $('ul.tabs li').click(function() {
      var tab_id = $(this).attr('data-tab');

      $('ul.tabs li').removeClass('current');
      $('.tab-content').removeClass('current');

      $(this).addClass('current');
      $("#" + tab_id).addClass('current');
    })

    var href_ = ''
    // show Modal
    $(document).on('click', '.open-modal', function(){
      href_ = $(this).closest(".card").data('href') || $(this).data('href');
      console.log(href_);
      
      $('#myModal').modal('show');
    })
    $(document).on('click', '.clickPetugas', function(){
      window.location.href = href_;
    })

    // $(".clickPetugas").click(function(){
    //     var $this = $(this);
    //     if($this.data('clicked', true)) {
    //         alert(1)
    //         href_ = $(this).data('href');
    //         // window.location.href = href_;
    //     }
    // });

    //active class
    var href = window.location.href;
    // var baseUrl = 'http://localhost:3000/';
    var baseUrl = 'http://nutriclub.staging.salt.id/nutriexpert/';
    var artikel = baseUrl + 'artikel-list.html';
    var jurnal = baseUrl + 'jurnal.html';
    var _menu = $(".header_menu").find('div');

    if (href == baseUrl) {
      _menu.eq(0).addClass('active')
    } else if (href == jurnal) {
      _menu.eq(2).addClass('active');
    } else if (href == artikel) {
      _menu.eq(3).addClass('active');
    }

});