$('.navpills--list').click(function(event) {
    var id = $(this).attr('id');
    var toShow = '#show-' + id;

    $('.all-content').not(toShow).hide();
    $(toShow).fadeIn().removeAttr('hidden');
});


$('#artikel-list .navpills--list').click(function(){
    $('li').removeClass("active");
    $(this).addClass("active");
});