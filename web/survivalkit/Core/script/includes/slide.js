$('#artikel-list').slick({
    centerMode: false,
    infinite: false,
    centerPadding: '5px',
    slidesToShow: 7,
    arrows: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: true,
          infinite: false,
          
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: true,
          infinite: false,
          
          slidesToShow: 3
        }
      }
    ]
  });