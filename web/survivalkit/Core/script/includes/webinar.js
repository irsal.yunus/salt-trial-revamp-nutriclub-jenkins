$(function() {
  var webinarDate =  $('.counter').data('date');
  var countDownDate = new Date(webinarDate).getTime();

  var x = setInterval(function() {
    var now = new Date().getTime();
    var distance = countDownDate - now;
    if (distance < 0) {
      clearInterval(x);
      return false;
    }
  
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    $('.js-day').text(days);
    $('.js-hour').text(hours);
    $('.js-minute').text(minutes);
    $('.js-second').text(seconds);
   
  }, 1000);
  

  function updateIframe(e){switch(e.type){case"resizeIframe":this.style.height=e.height+"px";break;case"iframeDownloadFailed":this.hidden=!0;break;default:return}}window.addEventListener("message",function(e){var t=document.querySelectorAll(e.data.id);[].forEach.call(t,function(t){updateIframe.call(t,e.data)})});
})

