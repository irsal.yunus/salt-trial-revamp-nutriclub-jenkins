#!/usr/bin/env bash

Workspace_Dir="/var/lib/jenkins/workspace"
Package_Name="revamp"
Library_Dir="vendor"


# validasi workdir pipeline jenkins
if [ ! -d $Workspace_Dir/$Package_Name ] || [ -f $Workspace_Dir/$Package_Name ]; then
    echo "Dir $Package_Name Not Found"
    echo "Pulling"
    git branch --set-upstream-to=origin/master master
    git pull
    # copy parameter build
    echo "Copying Parameter"
    cp -rv app/constants.example.php app/constants.php
    cp -rv app/config/parameters.example.yml app/config/parameters.yml
    # permission console
    echo "Update permission bin/console"
    chmod +x bin/console   
    # permission var
    echo "Update permission var/"
    chmod 777 -R var/
    # validasi Dir vendor    
    if [ ! -d $Workspace_Dir/$Package_Name/$Library_Dir ] || [ -f $Workspace_Dir/$Package_Name/$Library_Dir ]; then
        echo "Dir $Library_Dir Already..."        
        exit 0    
    else
        echo "$Library_Dir Not Found"
        php -d memory_limit=-1 /usr/local/bin/composer install  --no-interaction --optimize-autoloader  
        exit 0   
    fi
else
    echo "Dir $Package_Name Already.."    
    exit 0    
fi