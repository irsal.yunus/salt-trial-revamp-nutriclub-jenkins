FROM dpfaffenbauer/pimcore:PHP7.2-apache
MAINTAINER allandhino.widyaputra <allandhino.widyaputra@salt.co.id>

WORKDIR /var/www/html

COPY . .

RUN composer dump-autoload
RUN COMPOSER_MEMORY_LIMIT=-1 composer install
