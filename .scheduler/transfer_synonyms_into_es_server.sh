#!/usr/bin/env bash

synonymsPath="/var/www/app-nutriclub/cms/var/bundles/pimcore-elasticsearchbundle"
synonymsFilename="synonyms.txt"

elasticsearchServerIp="192.168.0.115"
elasticsearchServerUser="customer"
elasticsearchServerPort="22"
elasticsearchServerPath="~"

echo "Transfer ES synonyms Into ES VM"

echo "Current Directory : "
pwd;

echo "Change Directory Into ${synonymsPath}"
cd ${synonymsPath};

echo "Current Directory : "
pwd;

scp -P ${elasticsearchServerPort} ./${synonymsFilename} ${elasticsearchServerUser}@${elasticsearchServerIp}:${elasticsearchServerPath}${synonymsFilename}.new


