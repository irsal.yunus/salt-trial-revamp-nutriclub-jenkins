#!/usr/bin/env bash

stopWordsPath="/var/www/app-nutriclub/cms/var/bundles/pimcore-elasticsearchbundle"
stopWordsFilename="stopwords.txt"

elasticsearchServerIp="192.168.0.115"
elasticsearchServerUser="customer"
elasticsearchServerPort="22"
elasticsearchServerPath="~"

echo "Transfer ES Bad Words Into ES VM"

echo "Current Directory : "
pwd;

echo "Change Directory Into ${stopWordsPath}"
cd ${stopWordsPath};

echo "Current Directory : "
pwd;

scp -P ${elasticsearchServerPort} ./${stopWordsFilename} ${elasticsearchServerUser}@${elasticsearchServerIp}:${elasticsearchServerPath}${stopWordsFilename}.new


