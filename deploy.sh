#!/usr/bin/env bash
Target_Dir="/var/www/app-nutri-jenkins"
Build_Dir="/var/lib/jenkins/workspace"
Package_Name="revamp"
php bin/console cache:clear -v

php bin/console pimcore:cache:clear

php bin/console pimcore:deployment:classes-rebuild -cv

php bin/console pimcore:migration:migrate -v

php bin/console assets:install --symlink web

chmod 777 -R var/

chmod 777 -R web/

# validasi copy package to directory web
if [ ! -d $Target_Dir/$Package_Name ] || [ -f $Target_Dir/$Package_Name ]; then
    echo "Dir $Package_Name not found"
    echo "Copying Package..."    
    sudo cp -rv $Build_Dir/$Package_Name $Target_Dir/
    sudo chown -R www-data:www-data $Target_Dir/$Package_Name
    echo "Package has been copy..."
    exit 0
else
    echo "Package Already..."
    exit 0
fi
