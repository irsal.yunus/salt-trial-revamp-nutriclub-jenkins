<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 17:51
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */


use AppBundle\Model\DataObject\{Article, ArticleSubCategory};
use Pimcore\Model\DataObject\Fieldcollection\Data\ArticleChild;
use Pimcore\Model\Document\Tag\Link;
use Pimcore\Tool;

$this->extend('layout.html.php');

/** @var Article $article */
$article = $this->article;

$inThisArticles = null;
$articleContents = null;

if ($article && $article->getChildContent()) {
    /** @var ArticleChild $childContent */
    foreach ($article->getChildContent() as $childContent) {
        $headingTwo = null;
        if ($title = $childContent->getTitle()) {
            $id = \Ramsey\Uuid\Uuid::uuid4();
            $inThisArticles .= '<li><a href="#'.$id.'">' . $title . '</a></li>';

            $headingTwo .= '<h2 id="'.$id.'">' . $title . '</h2>';
        }
        $findCurlyBraches = preg_match_all('/{{(\w+)}}/', $childContent->getContent(), $matches);
        $originalContent = $childContent->getContent();
        if ($findCurlyBraches) {
            foreach ($matches[0] as $match) {
                if ($match === '{{WIDGET_PARAGRAPH_ONE}}') {
                    $originalContent = str_replace($match, $this->inc('/snippets/paragraph-one', []), $childContent->getContent());
                }

                if ($match === '{{WIDGET_PARAGRAPH_TWO}}') {
                    $originalContent = str_replace($match, $this->inc('/snippets/paragraph-two', []), $originalContent);
                }
            }
        }

        $articleContents .= '<div class="article__body"><div class="container">' . $headingTwo . $originalContent . '</div></div>';
    }
}

$tagsHtml = null;
if ($article && $article->getSubCategory()) {
    /** @var ArticleSubCategory $articleSubCategory */
    foreach ($article->getSubCategory() as $articleSubCategory) {
        $articleSubCategory = ArticleSubCategory::getById($articleSubCategory->getId());
        $articleSubCategoryUrl = $articleSubCategory->getRouter();
        $tagsHtml .= '<a href="'. $articleSubCategoryUrl .'" class="tag__button btn">'.  $articleSubCategory->getName().'</a>';
    }
}

$host = Tool::getHostUrl('https');
$title = $article ? $article->getTitle() : null;
$url = $article ? $article->getRouter() : null;
/** @var Link $fb */
$fb = $this->facebookShareAbleLink;
/** @var Link $tw */
$tw = $this->twitterShareAbleLink;
?>
<?php
    $this->headLink()->offsetSetStylesheet(4, '/assets/css/vendor/slick.css');
    $this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/slick-theme.css');
?>
<?php $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/detail.css'); ?>

<!-- DATA LAYER -->
<input type="hidden" id="article_published_date" value="<?= \Carbon\Carbon::parse($article->getCreationDate())->format('Ymd') ?>" />
<!-- DATA LAYER -->

<div class="article__wrapper">
    <div class="banner">
        <div class="banner__detail">
            <div class="banner__medium-content d-block d-sm-none">
                <?= $article ? $article->getImgMobileHtml('default', [
                    'class' => 'img-fluid'
                ]) : ''?>
            </div>
            <div class="banner__medium-content d-none d-sm-block">
                <?= $article ? $article->getImgDekstopHtml('default', [
                    'class' => 'img-fluid'
                ]) : ''
                ?>
            </div>
        </div>
    </div>

    <div class="heading__article mt-2 ">
        <div class="container">
            <h4><?= $article ? $article->getCategory()->getName() : '' ?></h4>
            <h1><?= $title ?></h1>
            <?php if($article && $article->getCreator()) { ?>
                <p><?= $this->t('ARTICLE_BY') ?> : <?= $article->getCreator() ?></p>
            <?php } ?>
        </div>
    </div>

    <div class="socialmedia__sharing">
        <div class="container">
            <?php
                $fb->setText('<img src="/assets/images/icons/facebook.png" alt="facebook-logo">');
                echo htmlspecialchars_decode($fb->frontend());
            ?>
            <?php
                $tw->setText('<img src="/assets/images/icons/twitter.png" alt="twitter-logo">');
                echo htmlspecialchars_decode($tw->frontend());
            ?>
            <hr class="socialmedia__underline">
        </div>
    </div>
    <?php if ($inThisArticles) { ?>
    <div class="article__point">
        <div class="container">
            <p><?= $this->t('IN_THIS_ARTICLE') ?></p>
            <ul>
                <?= $inThisArticles ?>
            </ul>
        </div>
    </div>
    <?php } ?>

    <?php if ($article && $article->getContent()) { ?>
    <div class="article__body">
        <div class="container">
            <?php
            if ($originalContent = $article->getContent()) {
                $findCurlyBraches = preg_match_all('/{{(\w+)}}/', $article->getContent(), $matches);
                if ($findCurlyBraches) {
                    foreach ($matches[0] as $match) {
                        if ($match === '{{WIDGET_PARAGRAPH_ONE}}') {
                            $originalContent = str_replace($match, $this->inc('/snippets/paragraph-one', []), $originalContent);
                        }

                        if ($match === '{{WIDGET_PARAGRAPH_TWO}}') {
                            $originalContent = str_replace($match, $this->inc('/snippets/paragraph-two', []), $originalContent);
                        }
                    }
                }
                echo $originalContent;
            }
            ?>
        </div>
    </div>
    <?php } ?>

    <?= $articleContents ?>
</div>

<?= $this->placeholder('bannerTopArticles') ?
    $this->inc($this->placeholder('bannerTopArticles'), []) :
    null
?>

<div class="socialmedia__sharing">
    <div class="container">
        <?= htmlspecialchars_decode($fb->frontend()) ?>
        <?= htmlspecialchars_decode($tw->frontend()) ?>
    </div>
</div>

<?php
if ($article->getSource()) {
    ?>
    <div class="accordion__source" id="accordion__toggle">
        <div class="container">
            <div class="card">
                <h2 class="mb-0">
                    <button class="accordion__source-btn btn w-100 text-left" type="button" data-toggle="collapse"
                            data-target="#accordion__source" aria-expanded="true" aria-controls="accordion__source">
                        <?= $this->t('ARTICLE_SOURCE') ?>
                    </button>
                </h2>
                <div id="accordion__source" class="collapse hide" aria-labelledby="headingOne"
                     data-parent="#accordion__toggle">
                    <div class="card-body">
                        <?= $article->getSource() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php
}
?>

<?= $this->inc('/snippets/article-detail', []) ?>
<?php $this->headScript()->offsetSetFile(9, '/assets/js/vendor/slick.min.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(10, '/assets/js/widgets/card-three-columns-article-video.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(11, '/assets/js/pages/article-detail.js', 'text/javascript', []); ?>

<!-- RRR -->
<input type="hidden" id="article_token" value="<?= $article->getId() ?>">
