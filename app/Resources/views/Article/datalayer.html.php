<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource datalayer.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 24/09/20
 * @time 15.55
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

/** @var \AppBundle\Model\DataObject\Article $article */
$article = $this->data;
?>

var content_category = "<?= $article && $article->getCategory() ? $article->getCategory()->getName() : '' ?>";
var content_type = "Article";
<?php
$subCategory = null;
if ($article && $article->getSubCategory()) {
    $subCategory = $article->getSubCategory()[0]->getName();
}
?>
var content_subcategory = "<?= $subCategory ?>";
