<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 15:41
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

echo $this->areablock('article-landing-areablock', []);
?>
<!-- css -->
<?php
    $this->headLink()->offsetSetStylesheet(4, '/assets/css/vendor/slick.css');
    $this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/slick-theme.css');
?>
<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/article.css'); ?>


<!-- js -->
<?php $this->headScript()->offsetSetFile(9, '/assets/js/widgets/acordion.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(10, '/assets/js/vendor/slick.min.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(11, '/assets/js/widgets/card-four-columns-banner.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(12, '/assets/js/widgets/card-three-columns-article-video.js', 'text/javascript', []); ?>
<!-- <?php $this->headScript()->offsetSetFile(13, '/assets/js/widgets/load-more.js', 'text/javascript', []); ?> -->
<?php $this->headScript()->offsetSetFile(14, '/assets/js/includes/pagination.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(15, '/assets/js/widgets/card-articles.js', 'text/javascript', []); ?>

<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="article"/>
<input type="hidden" id="content_type" value="article"/>
<input type="hidden" id="content_subcategory" value="article_landing"/>
