<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/02/2020
 * Time: 14:01
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\Article;

/** @var Article\Listing $articleObject */
$articleObject = $this->articleObject;
?>

<?= $this->render('Widgets/CardArticle/list.html.php', [
    'data' => $articleObject
]) ?>
