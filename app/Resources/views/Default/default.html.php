<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\Customer;

$this->extend('layout.html.php');

$user = $app->getUser();
if ($user) {
    /** @var Customer $userProfile */
    $userProfile = $this->getRequest()->getSession()->get('UserProfile');
}

$this->headLink()->offsetSetStylesheet(4, '/assets/css/vendor/slick.css', 'screen', false);
$this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/slick-theme.css', 'screen', false);
$this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/home.css', 'screen', false);
echo $this->areablock('homepage-areablock', []);
?>

<?php $this->headScript()->offsetSetFile(3, '/assets/js/vendor/slick.min.js', 'text/javascript'); ?>
<?php $this->headScript()->offsetSetFile(4, '/assets/js/widgets/banner-slider.js', 'text/javascript'); ?>

<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="homepage"/>
<input type="hidden" id="content_type" value="landing_page"/>
<input type="hidden" id="content_subcategory" value=""/>
