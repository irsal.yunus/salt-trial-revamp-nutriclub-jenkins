 <?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/font-awesome.min.css');
$this->headLink()->offsetSetStylesheet(6, '/assets/css/vendor/slick.css');
$this->headLink()->offsetSetStylesheet(7, '/assets/css/vendor/slick-theme.css');
$this->headLink()->offsetSetStylesheet(8, '/assets/css/pages/homelogin.css');

?>

<div class="container mt-5">
    <div class="row">
        <div class="col-12 col-md-5">
            <?= $this->areablock('card-member-left', []) ?>
        </div>
        <div class="col-12 col-md-7 right-side">
            <?= $this->areablock('card-member-right', []) ?>
        </div>
    </div>
</div>

<?= $this->areablock('home-after-login', [])?>

<?php $this->headScript()->offsetSetFile(2, '/assets/js/vendor/bootstrap.bundle.min.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(3, '/assets/js/vendor/slick.min.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(4, '/assets/js/widgets/card-slider-big.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(6, '/assets/js/pages/homelogin.js', 'text/javascript', []); ?>

<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="homelogin"/>
<input type="hidden" id="content_type" value="landing_page"/>
<input type="hidden" id="content_subcategory" value=""/>