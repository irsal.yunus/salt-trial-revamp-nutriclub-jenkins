<?php
$isValid = $form->vars['valid'];
?>

<div class="form-group <?= $isValid ? '' : 'has-error' ?>">
    <?= $this->form()->label($form) ?>
    <?= $this->form()->widget($form, [
        'attr' => [
            'class' => 'form-control'
        ]
    ]) ?>
</div>

<div class="form-group content__group show-anak">
    <label for="" class="content__label">Usia Anak</label>
    <input id="" type="text" class="form-control" placeholder="Contoh: 6 bulan"/>
</div>
