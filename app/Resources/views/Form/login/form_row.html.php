<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/01/2020
 * Time: 14:03
 */

$isValid = $form->vars['valid'];
?>

<div class="col-md-8 offset-md-2 <?= $isValid ? '' : 'has-error' ?>">
    <div class="text-left">
        <?= $this->form()->label($form, null, [
            'label_attr' => [
                'class' => null
            ]
        ]) ?>
    </div>

    <?= $this->form()->widget($form, [
        'attr' => [
            'class' => 'form-control'
        ]
    ]) ?>

    <?= $this->form()->errors($form) ?>
</div>
