<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/01/2020
 * Time: 14:04
 */

/** @var \Symfony\Component\Form\FormErrorIterator $errors */
?>

<?php if (count($errors) > 0): ?>

    <ul class="help-block">
        <?php foreach ($errors as $error): ?>
            <li><?php echo $error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>

<?php endif ?>

