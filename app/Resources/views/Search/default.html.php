<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 06/02/2020
 * Time: 16:09
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
use AppBundle\Model\DataObject\Article;
use Ramsey\Uuid\Uuid;
use Zend\Paginator\Paginator;

$this->extend('layout.html.php');

$query = $this->searchQuery;
$articleTotalPage = $this->searchArticleTotalPage;
$articleTotalData = $this->searchArticleTotalData;

$videoTotalPage = $this->searchVideoTotalPage;
$videoTotalData = $this->searchVideoTotalData;

$podcastTotalPage = $this->searchPodcastTotalPage;
$podcastTotalData = $this->searchPodcastTotalData;

$ebookTotalPage = $this->searchEbookTotalPage;
$ebookTotalData = $this->searchEbookTotalData;

$faqTotalData = $this->searchFaqTotalData;

$formIdArticle = 'search_article-load-more-' . Uuid::uuid4();
$formIdVideo = 'search_video-load-more-' . Uuid::uuid4();
$formIdPodcast = 'search_podcast-load-more-' . Uuid::uuid4();
$formIdEbook = 'search_ebook-load-more-' . Uuid::uuid4();
$formIdFaq = 'search_faq-load-more-' . Uuid::uuid4();
?>
<?php $this->headLink()->offsetSetStylesheet(7, '/assets/css/pages/search.css');?>
<div class="container">
    <div class="search__queries">
    <ul class="nav nav-pills" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a
                 href=".pills-article"
                 class="nav-link active"
                 data-toggle="pill"
                 role="tab"
                 id="pills-article-tab"
                 aria-controls="pills-article"
                 aria-selected="true"
                 ><?= $this->t('ARTICLE') ?></a>
        </li>
        <li class="nav-item">
            <a
                href=".pills-video"
                class="nav-link"
                id="pills-video-tab"
                data-toggle="pill"
                role="tab"
                aria-controls="pills-video"
                aria-selected="false"><?= $this->t('VIDEO') ?>
            </a>
        </li>
        <li class="nav-item">
            <a
                href=".pills-podcasts"
                class="nav-link"
                id="pills-podcasts-tab"
                data-toggle="pill"
                role="tab"
                aria-controls="pills-podcasts"
                aria-selected="false"><?= $this->t('PODCAST') ?>
            </a>
        </li>
        <li class="nav-item">
            <a
                href=".pills-ebook"
                class="nav-link"
                id="pills-ebook-tab"
                data-toggle="pill"
                role="tab"
                aria-controls="pills-ebook"
                aria-selected="false"><?= $this->t('EBOOK') ?>
            </a>
        </li>
        <li class="nav-item">
            <a
                href=".pills-faq"
                class="nav-link"
                id="pills-faq-tab"
                data-toggle="pill"
                role="tab"
                aria-controls="pills-faq"
                aria-selected="false"><?= $this->t('FAQ') ?>
            </a>
        </li>
    </ul>

    </div>
</div>

<div class="search__articles <?= $this->searchTotalData > 0 ? 'd-none' : '' ?>">
    <div class="container">
        <div class="search__articles-notfound">
                <img src="/assets/images/icons/search-result-notfound.png" alt="search-result-notfound" >
                <h3>Tidak Ada Hasil</h3>
        </div>
    </div>
</div>

<div class="search__articles <?= $this->searchTotalData <= 0 ? 'd-none' : '' ?>">
    <div class="container">
    <div class="container">
            <div class="tab-content search__articles-results" >
            <div class="tab-pane fade show active pills-article" role="tabpanel">
                <p class="search__queries-text"><b><?= $articleTotalData ?? 0 ?></b> Hasil ditemukan pencarian kata <b class="font-weight-bold"><?= $query ?></b></p>
            </div>
            <div class="tab-pane fade pills-video" role="tabpanel">
                <p class="search__queries-text"><b><?= $videoTotalData ?? 0 ?></b> Hasil ditemukan pencarian kata <b class="font-weight-bold"><?= $query ?></b></p>
            </div>
            <div class="tab-pane fade pills-podcasts" role="tabpanel">
                <p class="search__queries-text"><b><?= $podcastTotalData ?? 0 ?></b> Hasil ditemukan pencarian kata <b class="font-weight-bold"><?= $query ?></b></p>
            </div>
            <div  class="tab-pane fade pills-ebook"  role="tabpanel">
                <p class="search__queries-text"><b><?= $ebookTotalData ?? 0 ?></b> Hasil ditemukan pencarian kata <b class="font-weight-bold"><?= $query ?></b></p>
            </div>
            <div  class="tab-pane fade pills-faq"  role="tabpanel">
                <p class="search__queries-text"><b><?= $faqTotalData ?? 0 ?></b> Hasil ditemukan pencarian kata <b class="font-weight-bold"><?= $query ?></b></p>
            </div>
            </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active pills-article" role="tabpanel" aria-labelledby="pills-article-tab">
                <div class="container">
                <div class="row">
                       <div class="search__articles-lists  row container">
                           <?php
                                 if ($this->searchArticle) {
                                    echo $this->render('Search/article.partial.html.php', [
                                        'data' => $this->searchArticle
                                    ]);
                                }
                           ?>
                       </div>
                       <?php
                           if (!$this->searchArticle) {
                           ?>
                               <div class="container">
                                    <div class="search__articles-notfound-result">
                                        <h2>Tidak Ada Hasil</h2>
                                    </div>
                               </div>
                           <?php
                           }
                           ?>
                    </div>
                </div>

                <form id="<?= $formIdArticle ?>">
                    <input type="hidden" id="nextPage-article" name="nextPage" value="2" form-id="<?= $formIdArticle ?>">
                    <input type="hidden" id="currentPageLimit-article" name="currentPageLimit" value="10">
                    <input type="hidden" id="q-article" name="q" value="<?= $query ?>">
                    <input type="hidden" id="type-article" name="type" value="article">
                </form>
                <div class="search__articles-pagination <?= ($articleTotalPage === null | ($articleTotalPage < 1)) ? 'd-none' : '' ?>">
                    <?= $this->render('Include/paging.html.php', ['last' => $articleTotalPage]) ?>
                </div>

            </div>
            <div class="tab-pane fade pills-video" role="tabpanel" aria-labelledby="pills-video-tab">
                <div class="row search__articles-video-lists">
                    <?php
                    if ($this->searchVideo) {
                        echo $this->render('Search/video.partial.html.php', [
                            'data' => $this->searchVideo
                        ]);
                    }
                    ?>
                </div>
                <?php
                if (!$this->searchVideo) {
                    ?>
                    <div class="search__articles-notfound-result">
                        <h2>Tidak Ada Hasil</h2>
                    </div>
                    <?php
                }
                ?>

                <form id="<?= $formIdVideo ?>">
                    <input type="hidden" id="nextPage-video" name="nextPage" value="2" form-id="<?= $formIdVideo ?>">
                    <input type="hidden" id="currentPageLimit-video" name="currentPageLimit" value="10">
                    <input type="hidden" id="q-video" name="q" value="<?= $query ?>">
                    <input type="hidden" id="type-video" name="type" value="video">
                </form>

                <div class="search__video-pagination <?= ($videoTotalPage === null | ($videoTotalPage < 1)) ? 'd-none' : '' ?>">
                    <?= $this->render('Include/paging.html.php', ['last' => $videoTotalPage]) ?>
                </div>

            </div>
            <div class="tab-pane fade pills-podcasts" role="tabpanel" aria-labelledby="pills-podcasts-tab">
                <div class="search__articles-podcast-lists container">
                    <div class="row">
                        <div class="row container">
                            <?php
                            if ($this->searchPodcast) {
                                echo $this->render('Search/podcast.partial.html.php', [
                                    'data' => $this->searchPodcast
                                ]);
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
                if (!$this->searchPodcast) {
                    ?>
                    <div class="search__articles-notfound-result">
                        <h2>Tidak Ada Hasil</h2>
                    </div>
                    <?php
                }
                ?>

                <form id="<?= $formIdPodcast ?>">
                    <input type="hidden" id="nextPage-podcast" name="nextPage" value="2" form-id="<?= $formIdPodcast ?>">
                    <input type="hidden" id="currentPageLimit-podcast" name="currentPageLimit" value="10">
                    <input type="hidden" id="q-podcast" name="q" value="<?= $query ?>">
                    <input type="hidden" id="type-podcast" name="type" value="podcast">
                </form>

                <div class="search__podcast-pagination <?= ($podcastTotalPage === null | ($podcastTotalPage < 1)) ? 'd-none' : '' ?>">
                    <?= $this->render('Include/paging.html.php', ['last' => $podcastTotalPage]) ?>
                </div>

            </div>
            <div class="tab-pane fade pills-ebook" role="tabpanel" aria-labelledby="pills-ebook-tab">
                <div class="row container search__articles-ebook-lists container">
                    <?php
                    if ($this->searchEbook) {
                        echo $this->render('Search/ebook.partial.html.php', [
                            'data' => $this->searchEbook
                        ]);
                    }
                    ?>
                </div>
                <?php
                if (!$this->searchEbook) {
                    ?>
                    <div class="search__articles-notfound-result">
                        <h2>Tidak Ada Hasil</h2>
                    </div>
                    <?php
                }
                ?>
                    <form id="<?= $formIdEbook ?>">
                        <input type="hidden" id="nextPage-ebook" name="nextPage" value="2" form-id="<?= $formIdEbook ?>">
                        <input type="hidden" id="currentPageLimit-ebook" name="currentPageLimit" value="10">
                        <input type="hidden" id="q-ebook" name="q" value="<?= $query ?>">
                        <input type="hidden" id="type-ebook" name="type" value="ebook">
                    </form>

                    <div class="search__ebook-pagination <?= ($ebookTotalPage === null | ($ebookTotalPage < 1)) ? 'd-none' : '' ?>">
                        <?= $this->render('Include/paging.html.php', ['last' => $ebookTotalPage]) ?>
                    </div>
            </div>
            <div class="tab-pane fade pills-faq" role="tabpanel" aria-labelledby="pills-faq-tab">
                <div class="container">
                <div class="row">
                       <div class="search__faq row container">
                           <?php
                                if ($this->searchFaq && $this->searchFaq['data']) {
                                    echo $this->render('Search/faq.partial.html.php', [
                                        'data' => $this->searchFaq['data']
                                    ]);
                                }
                           ?>
                       </div>
                       <?php
                           if (!$this->searchFaq) {
                           ?>
                               <div class="container">
                                    <div class="search__faq-notfound-result">
                                        <h2>Tidak Ada Hasil</h2>
                                    </div>
                               </div>
                           <?php
                           }
                           ?>
                    </div>
                </div>

                <form id="<?= $formIdFaq ?>">
                    <input type="hidden" id="nextPage-faq" name="nextPage" value="2" form-id="<?= $formIdFaq ?>">
                    <input type="hidden" id="currentPageLimit-faq" name="currentPageLimit" value="10">
                    <input type="hidden" id="q-faq" name="q" value="<?= $query ?>">
                    <input type="hidden" id="type-faq" name="type" value="article">
                </form>
                <div class="search_faq-pagination <?= ($this->searchFaq['totalPage'] === null | ($this->searchFaq['totalPage'] < 1)) ? 'd-none' : '' ?>">
                    <?= $this->render('Include/paging.html.php', ['last' => $this->searchFaq['totalPage']]) ?>
                </div>
            </div>
    </div>
</div>

<nav class="search__articles-navigation d-none" >
  <ul class="pagination justify-content-center d-flex mb-0">
    <li class="page-item">
      <a class="page-link" href="#" tabindex="-1" aria-disabled="true">First</a>
    </li>
        <li class="page-item">
            <a href="#" class="page-link">
                <img src="/assets/images/icons/backward.png" alt="backward-arrow">
            </a>
        </li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
            <a href="#" class="page-link">
                <img src="/assets/images/icons/forward.png" alt="forward-arrow">
            </a>
        </li>
    <li class="page-item">
      <a class="page-link" href="#">Last</a>
    </li>
  </ul>
</nav>
</div>
<?= $this->areablock('search-areablock', []) ?>
<?php $this->headScript()->offsetSetFile(3, '/assets/js/widgets/search-results.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(4, '/assets/js/includes/pagination.js', 'text/javascript', []); ?>


<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="search"/>
<input type="hidden" id="content_type" value="search_result"/>
<input type="hidden" id="content_subcategory" value="search_result"/>
