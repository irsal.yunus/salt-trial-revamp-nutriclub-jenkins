<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 14:32
 */

use AppBundle\Model\DataObject\ArticlePodcast;

$podcasts = $this->data;
if ($podcasts) {
    /** @var ArticlePodcast $podcast */
    foreach ($podcasts as $podcast) {
        if (!$podcast instanceof ArticlePodcast) {
            continue;
        }
        ?>
        <div class="col-md-6 col-12">
            <div class="search__articles-podcasts-item">
                <a href="<?= $podcast->getRouter() ?>">
                    <div class="row">
                        <div class="col-5 col-md-4 search__articles-podcasts-figure">
                            <?=
                            $podcast
                                ->getImageDesktop()
                                ->getThumbnail('podcast-partial-thumbnail-image-desktop')->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'img-fluid'
                                ])
                            ?>
                            <img src="/assets/images/contents/play-button.png" alt="play-button" class="play-button">
                        </div>
                        <div class="col-7 col-md-8 search__articles-podcasts-description">
                            <div class="search__articles-podcast-title">
                                <p class="search__articles-category">
                                    <?= ($podcast && $podcast->getCategory()) ? $podcast->getCategory()->getName() : '' ?>
                                </p>
                                <p class="search__articles-text">
                                    <?= $podcast->getTitle() ?? '' ?>
                                </p>
                            </div>
                            <div class="search__articles-timeplay">06:30</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <?php
    }
}
