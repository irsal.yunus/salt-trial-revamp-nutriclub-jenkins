<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 14:17
 */

use AppBundle\Model\DataObject\Article;

/** @var Article\Listing $data */
$data = $this->data;
if ($data) {
    foreach ($data as $article) {
        if (!$article instanceof Article) {
            continue;
        }
    ?>

    <div class="col-12 col-md-6 mt-2">
        <div class="search__articles-item">
            <div class="row">
                <div class="col-5 col-md-4">
                    <a href="<?= $article->getRouter() ?>">
                        <?= $article->getImgDesktop() ?
                            $article->getImgDesktop()->getThumbnail('default')->getHtml([
                                'disableWidthHeightAttributes' => true,
                                'class' => 'img-fluid'
                            ]) : '' ?>
                    </a>
                </div>
                <div class="col-7 col-md-8 p-0">
                    <a href="<?= $article->getCategory()->getRouter() ?>">
                        <p class="search__articles-category"><?= $article->getCategory() ? $article->getCategory()->getName() : '' ?></p>
                    </a>
                    <a href="<?= $article->getRouter() ?>">
                        <p class="search__articles-text"><?= $article->getTitle() ?></p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php
    }
}
