<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 14:31
 */

use AppBundle\Model\DataObject\ArticleVideo;

$videos = $this->data;
if ($videos) {
    /** @var ArticleVideo $video */
    foreach ($videos as $video) {
        if (!$video instanceof ArticleVideo) {
            continue;
        }
        ?>
        <div class="col-12 col-md-6">
        <a href="<?= $video->getRouter() ?>">
            <div class="row">
                <div class="container">
                    <div class="col-md-12 col-12">
                        <div class="search__articles-video-item">
                            <div class="row">
                                <div class="col-5 col-md-4 search__articles-video-figure">
                                    <img src="<?= $video->getImageMaxRes() ?? '' ?>" alt="<?= $video->getTitle() ?? '' ?>" class="img-fluid">
                                    <img src="/assets/images/contents/play-button.png" alt="play-button" class="play-button">
                                </div>
                                <div class="search__articles-video-wrapper col-7 col-md-8">
                                    <div class="search__articles-video-description">
                                        <p class="search__articles-category">
                                            <?= ($video && $video->getCategory()) ? $video->getCategory()->getName() : '' ?>
                                        </p>
                                        <p class="search__articles-text"><?= $video->getTitle() ?? '' ?></p>
                                    </div>
                                    <div class="search__articles-video-timeplay">
                                        <p class="search__articles-timeplay">
                                            <?= ($video && $video->getVideoDuration()) ? $video->getVideoDuration() : '00:00' ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>
        <?php
    }
}
