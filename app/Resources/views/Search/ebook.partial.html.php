<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 14:40
 */

use AppBundle\Model\DataObject\ArticleEbook;

$ebooks = $this->data;
if ($ebooks) {
    /** @var ArticleEbook $ebook */
    foreach ($ebooks as $ebook) {
        if (!$ebook instanceof ArticleEbook) {
            continue;
        }
        ?>
        <div class="col-md-6 search__articles-ebook-item">
           <div class="search__articles-ebook-border">
           <a href="<?= $ebook->getRouter() ?>" class="row">
               <div class="col-5 col-md-4">
                    <?= $ebook->getImageDesktop()
                    ->getThumbnail('ebook-partial-thumbnail-desktop')->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'img-fluid'
                    ]) ?>
               </div>
               <div class="col-7 col-md-8 pl-0">
                    <p><?= $ebook->getTitle() ?? '' ?></p>
               </div>
           </a>
           </div>
        </div>
        <?php
    }
}
