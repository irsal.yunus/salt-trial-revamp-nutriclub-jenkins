<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 14:40
 */

use AppBundle\Helper\GeneralHelper;
use CarelineBundle\Model\DataObject\CarelineFaq;

$searchFaqs = $this->data;

foreach ($searchFaqs as $key => $searchFaq) {
    /** @var CarelineFaq $customerFaq */
    $carelineFaq = $searchFaq['object'];
    $index = $searchFaq['index'] ?? 0;
?>
<div class="col-12 col-lg-6 col-md-12 mt-2">
    <div class="search__faq-item">
        <div class="d-flex faq-result">
            <div class="faq-result__num">
                <span class="number"><?= $searchFaq['keyHelper'] ?? $key+1 ?></span>
            </div>
            <div class="faq-result__rsult">
                <h2 class="search__faq-category">
                <?= $carelineFaq ? $carelineFaq->getQuestion() : null ?>
                </h2>
                <a class="search__faq-topic" href="<?= $carelineFaq ? $carelineFaq->getRouter() . '#' . $index  : null ?>">
                    <p><?= $searchFaq['question'] ?? null ?></p>
                </a>
                <a class="search__faq-text" href="<?= $carelineFaq ? $carelineFaq->getRouter() . '#' . $index : null ?>">
                    <?= GeneralHelper::trimText($searchFaq['answer'] ?? null, 160)  ?>
                </a>
            </div>
        </div>
    </div>
</div>
<?php
}
?>
