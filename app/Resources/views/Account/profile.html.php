<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/01/2020
 * Time: 18:53
 */

use AppBundle\Model\DataObject\Customer;

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$this->extend('layout.html.php');
?>
<?php

$user = $app->getUser();

if ($user) {
    /** @var Customer $userSso */
    $userSso = $this->getRequest()->getSession()->get('UserProfile');
    echo $userSso->getFullname();
}
