<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/02/2020
 * Time: 11:48
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>
<?php $this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/register.css'); ?>
<?php $this->headLink()->offsetSetStylesheet(5, '/assets/css/pages/forgot-password.css'); ?>
<form class="registration">
    <div class="content justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 d-none d-sm-block">
                    <div class="registration__image text-center">
                        <img src="/assets/images/register__step1/content/family.svg" />
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="registration__form">
                        <div class="registration__content registration__otp active" id="step-1"> <!-- step 1 -->
                        <!-- <div class="registration__content-btn text-left">
                            <a class="btn btn-controller blue-color" href="/account/login">
                                Kembali
                            </a>
                        </div> -->
                        <div class="registration__content-btn text-left">
                            <a id="btn-back-step" class="btn btn-controller blue-color btn-back" href="/account/login">
                                Kembali
                            </a>
                        </div>
                            <div class="content__card rounded">
                                    <div class="text-center">
                                        <h3 class="forgot__password-header">Forgot Password</h3>
                                    </div>
                                    <div class="row">
                                            <div class="col-md-8 offset-md-2">
                                                <div class="container">
                                                    <div class="text-left">
                                                        <label for="user-email" class="forgot__password-label">Enter your mobile number or email number</label>
                                                    </div>
                                                <input type="text" name="email" id="user-email" class="form-control mt-3" placeholder="Mobile Number or Email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-8 col-md-6 offset-2 offset-md-3">
                                            <button type="button" class="btn btn-controller btn-primary btn-block" id="btn-forgot-password" data-href="#step-2">
                                                    Continue
                                            </button>
                                        </div>
                                    </div>
                            </div>
                        </div> <!-- End of step 1 -->
                        <div class="registration__content registration__otp" id="step-2"> <!-- step 2 -->
                        <div class="registration__content-btn text-left">
                            <a id="btn-back-step" class="btn btn-controller blue-color btn-back" data-href="#step-1">
                                Kembali
                            </a>
                        </div>
                            <div class="content__card rounded">
                                    <div class="content__card-text text-center">
                                        <p>A verification code Has Been Sent to</p>
                                        <b id="email-forgot"></b>
                                    </div>
                                    <div class="row">
                                            <div class="col-md-8 offset-md-2">
                                                <div class="container">
                                                <input type="text" name="email" id="verification-code" class="form-control mt-3" placeholder="Verification Code">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-8 col-md-6 offset-2 offset-md-3">
                                            <button type="button" class="btn btn-primary btn-block" id="btn-submit-forgot">
                                                    Continue
                                            </button>
                                            <button class="content__card-retry text-center">Resend Code</button>
                                        </div>
                                    </div>
                            </div>
                        </div> <!-- End of step 2 -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal" id="modal-error" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Error Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<?= $this->areablock('forgot-password-areablock', []) ?>
<?php $this->headScript()->offsetSetFile(4, '/assets/js/pages/forgot-password.js', 'text/javascript', []); ?>
