<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/01/2020
 * Time: 18:36
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$this->extend('layout.html.php');

$form = $this->form;

$this->form()->setTheme($form, [':Form/register']);
?>

<?= $this->form()->start($form, [
    'action' => $this->path('account-registration'),
    'name' => 'registration_form',
    'attr' => [
        'class' => 'registration'
    ]
]); ?>
<?php $this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/register.css'); ?>
<?php $this->headLink()->offsetSetStylesheet(5, '/assets/css/pages/login.css'); ?>
<div class="content justify-content-center">
    <div class="container">
        <div class="row">
            <div class="col d-none d-sm-block">
                <div class="registration__image text-center">
                    <img src="/assets/images/register__step1/content/family.svg" />
                </div>
            </div>
            <div class="col">

                <div class="registration__form">
                    <div class="registration__content text-center active" id="step-1">
                        <div class="registration__content-btn text-left">
                            <a href="/account/login" class="visibility-hidden">Kembali</a>
                        </div>
                        <?php
                        if ($this->registernotif && isset($this->registernotif['StatusCode'])) {
                            ?>
                            <div class="alert <?= $this->registernotif['StatusCode'] === '00' ? 'alert-success' : 'alert-danger' ?>" role="alert">
                                <?= $this->registernotif['StatusMessage'] ?>
                            </div>
                            <?php
                        }
                        ?>

                        <div class="content__card">
                            <h4 class="pt-4 pb-4">
                                Untuk mengenal lebih dekat, <br class="d-none d-sm-block"/>siapa Anda?
                            </h4>
                            <div class="row text-center">
                                <div class="col-12 col-md-8 offset-md-2">

                                    <div class="row">
                                        <div class="col-6 text-center">
                                            <label class="custom-radio">
                                                <span class="custom-radio__overlay">&nbsp;</span>
                                                <img src="/assets/images/register__step1/content/dad.png" />
                                                <p class="content__family text-center">Papa</p>
                                                <input type="radio" value="M" name="Gender" class="custom-radio__input gender" />
                                            </label>
                                        </div>
                                        <div class="col-6 text-center">
                                            <label class="custom-radio">
                                                <span class="custom-radio__overlay">&nbsp;</span>
                                                <img src="/assets/images/register__step1/content/mom.png"/>
                                                <p class="content__family text-center">Mama</p>
                                                <input type="radio" value="F" name="Gender" class="custom-radio__input gender" />
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <button disabled type="button" class="btn btn-controller btn-primary btn-lg btn-disabled" data-href="#step-2">
                                Selanjutnya
                            </button>

                            <a href="<?= $this->path('account-login') ?>">
                                <p class="content__have-account text-center pt-4">
                                    <?= $this->t('HAVE_AN_ACCOUNT_LOGIN_HERE') ?>
                                </p>
                            </a>
                        </div>
                    </div>

                    <div class="registration__content text-center" id="step-2">
                        <div class="registration__content-btn text-left">
                            <a class="btn btn-controller blue-color btn-back" id="btn-back-step-one" data-href="#step-1">
                                Kembali
                            </a>
                        </div>
                        <div class="content__card">
                            <h4 class="pt-4">
                                Kondisi <span id="kondisi-gender"></span> saat ini
                            </h4>
                            <div class="mb-3">
                                <div class="row text-center">
                                    <div class="col-6 text-center">
                                        <label class="custom-radio bg-white">
                                            <span class="custom-radio__overlay">&nbsp;</span>
                                            <img src="/assets/images/register__step2/content/belum_hamil.svg" />
                                            <p class="content__family text-center">Belum Hamil</p>
                                            <input type="radio" value="8" name="StagesID" class="custom-radio__input kondisi-ibu" data-show=".show-belum" />
                                        </label>
                                    </div>
                                    <div class="col-6 text-center">
                                        <label class="custom-radio bg-white">
                                            <span class="custom-radio__overlay">&nbsp;</span>
                                            <img src="/assets/images/register__step2/content/hamill.svg"/>
                                            <p class="content__family text-center">Hamil</p>
                                            <input type="radio" value="7" name="StagesID" class="custom-radio__input kondisi-ibu" data-show=".show-hamil" />
                                        </label>
                                    </div>
                                    <div class="col-6 text-center">
                                        <label class="custom-radio bg-white">
                                            <span class="custom-radio__overlay">&nbsp;</span>
                                            <img src="/assets/images/register__step2/content/ada_bayi.svg"/>
                                            <p class="content__family text-center">Hamil dan Memiliki Anak</p>
                                            <input type="radio" value="9" name="StagesID" class="custom-radio__input kondisi-ibu" data-show=".show-hamil, .show-anak" />
                                        </label>
                                    </div>
                                    <div class="col-6 text-center">
                                        <label class="custom-radio bg-white">
                                            <span class="custom-radio__overlay">&nbsp;</span>
                                            <img src="/assets/images/register__step2/content/ada_anak.svg"/>
                                            <p class="content__family text-center">Tidak Hamil dan memiliki anak</p>
                                            <input type="radio" value="10" name="StagesID" class="custom-radio__input kondisi-ibu" data-show=".show-anak, .show-belum" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <button type="button" disabled class="btn btn-controller btn-primary btn-lg mb-5 btn-disabled" data-href="#step-3">
                                Selanjutnya
                            </button>
                        </div>
                    </div>

                    <div class="registration__content" id="step-3">
                        <div class="registration__content-btn text-left">
                            <a id="btn-back-step" class="btn btn-controller blue-color btn-back" data-href="#step-2">
                                Kembali
                            </a>
                        </div>
                        <div class="content__card">
                            <h4 class="content-hamil__caption2 pt-3">
                                Isi dan lengkapi data diri
                            </h4>
                            <div class="mb-3 pt-2 registration__scroll">
                                <div class="form-group content__group show-anak childbirth-wrapper">
                                    <label for="ChildBirthDate" class="content__label">Tanggal Lahir Anak</label>

                                    <span id="ChildBirthPlaceHolder">dd/mm/yyyy</span>
                                    <input id="ChildBirthDateX" name="ChildBirthDateX" type="date" class="form-control custom-date-range" data-placeholder="Contoh: 6 bulan" onchange="changeDate(event)" placeholder="Choose date"/>
                                    <input id="ChildBirthDate" class="jsDateFormat" name="ChildBirthDate" type="hidden">
                                    <div class="invalid-feedback"></div>
                                </div>

                                <div class="form-group content__group show-anak">
                                    <label for="ChildName" class="content__label">Nama Anak</label>
                                    <input id="ChildName" name="ChildName" type="text" class="form-control" placeholder="Nama Anak" onkeypress="return /[a-zA-Z \s]/i.test(event.key)"/>
                                    <div class="invalid-feedback"></div>
                                </div>

                                <div class="form-group content__group show-hamil">
                                    <label for="AgePregnant" class="content__label">Usia Kehamilan (Minggu) </label>
                                    <input id="AgePregnant" name="AgePregnant" type="text" class="form-control number-only" placeholder="Contoh: 6" maxlength="2"/>
                                    <div class="invalid-feedback"></div>
                                </div>

                                <div class="form-group">
                                    <label for="Fullname" class="content__label">Nama Lengkap</label>
                                    <input id="Fullname" name="Fullname" type="text" class="form-control" placeholder="Nama Lengkap" onkeypress="return /[a-zA-Z \s]/i.test(event.key)"/>
                                    <div class="invalid-feedback"></div>
                                </div>

                                <div class="form-group">
                                    <label for="Email" class="content__label">Alamat Email</label>
                                    <input id="Email" name="Email" type="email" class="form-control" placeholder="Alamat Email"/>
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="form-group">
                                    <label for="Phone" class="content__label">Nomor Handphone</label>
                                    <input id="Phone" name="Phone" type="text" class="form-control number-only" placeholder="Nomor Handphone" minlength="10" maxlength="13" size="13" required/>
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="form-group position-relative">
                                    <div class="position-relative">
                                        <label for="password" class="content__label">Password</label>
                                        <input id="Password" name="Password" type="password" class="form-control" placeholder="Password" autocomplete="off"/>
                                        <button class="btn bg-transparent btn-eye-password" type="button">
                                            <i class="fa fa-eye-slash pr-2"></i>
                                        </button>
                                    </div>
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="form-group position-relative">
                                    <div class="position-relative">
                                        <label for="password" class="content__label">Konfirmasi Password</label>
                                        <input id="ConfirmPassword" name="ConfirmPassword" type="password" class="form-control" placeholder="Konfirmasi Password" autocomplete="off"/>
                                        <button class="btn bg-transparent btn-eye-password" type="button">
                                            <i class="fa fa-eye-slash pr-2"></i>
                                        </button>
                                    </div>
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="from-group show-belum mt-3 mb-3">
                                    <label for="">Apakah Anda sedang merencanakan kehamilan?</label>
                                    <div class="form-control no-border" id="RencanaKehamilan">
                                        <label class="mr-3">
                                            <input type="radio" name="rencana" value="1" class="radio-inline" id="confirm-yes"/>
                                            <span>Iya</span>
                                        </label>
                                        <label>
                                            <input type="radio" name="rencana" value="0" class="radio-inline" id="confirm-no"/>
                                            <span>Tidak</span>
                                        </label>
                                    </div>
                                    <div class="invalid-feedback"></div>
                                </div>

                                <div class="form-check mb-3">
                                    <div class="form-control no-border">
                                        <input class="form-check-input" type="checkbox" id="IsAgree" value="option1" />
                                        <div>
                                            <label class="form-check-label" for="IsAgree"> Saya telah membaca dan setuju dengan</label>
                                            <?= $this->wysiwyg('tos-and-privacy', []) ?>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback"></div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <button type="button" class="btn btn-controller btn-primary btn-block btn-lg mb-5" data-href="#step-4">
                                            Selanjutnya
                                        </button>
                                    </div>
                                </div>

                                <a href="<?= $this->path('account-login') ?>" class="text-center">
                                    <p class="content__have-account pt-4">
                                        <?= $this->t('HAVE_AN_ACCOUNT_LOGIN_HERE') ?>
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="registration__content registration__otp" id="step-4">
                        <div class="registration__content-btn text-left">
                            <a class="btn btn-controller blue-color btn-back" data-href="#step-3">
                                Kembali
                            </a>
                        </div>
                        <div class="content__card rounded">
                            <p>Kami akan melakukan Miscall ke nomor HP anda untuk proses verifikasi. <span>Tidak perlu diangkat, pastikan nomor HP Anda aktif, kemudian catat 4 digit terakhir nomor tersebut.</span></p>
                            <div class="form-group content__group mt-5 d-block">
                                <label for="phone-verify" class="content__label">Nomor Handphone Anda</label>
                                <div class="d-flex justify-content-center">
                                    <input id="phone-verify" name="phone-verify" type="text" class="form-control phone-verify text-center w-50" disabled="">
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary mb-5 btn-miscall w-50 mt-0" id="triggerMisscall">
                                Miscall saya sekarang
                            </button>
                        </div>
                    </div>

                    <div class="registration__content registration__otp" id="step-5">
                        <div class="content__card rounded">
                            <p>Lengkapi 4 Digit Terakhir</p>
                            <div class="form-group mt-3 registration__otp-inline justify-content-center">
                                <input type="text" class="form-control otp-item" id="otp-1" maxlength="1" />
                                <input type="text" class="form-control otp-item" id="otp-2" maxlength="1" />
                                <input type="text" class="form-control otp-item" id="otp-3" maxlength="1" />
                                <input type="text" class="form-control otp-item" id="otp-4" maxlength="1" />
                            </div>

                            <p>Tidak dapat miscall? <button class="retry btn-re-miscall" disabled>Retry <i class="fas fa-redo"></i></button></p>
                            <p class="text-center font-weight-bold" id="countdown">02:00</p>

                            <button type="button" class="btn btn-primary btn-block mb-5 btn-verifikasi w-75 m-auto" disabled>
                                Verifikasi
                            </button>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="baseURL" value="<?= getenv('REWARDS_API', null) ?>" />
<?= $this->form()->widget($form['_csrf_token'], []) ?>

<div class="modal" id="modal-error" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Error Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<?= $this->areablock('register-areablock', []) ?>

<?= $this->form()->end($form, ['render_rest' => false]); ?>

<?php $this->headScript()->offsetSetFile(3, '/assets/js/pages/register.js', 'text/javascript', []); ?>
