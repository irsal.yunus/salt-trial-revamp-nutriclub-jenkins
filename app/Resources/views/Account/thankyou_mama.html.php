<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/02/2020
 * Time: 14:31
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>
<?php $this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/thankyou-mama.css'); ?>
<form class="registration">

    <div class="content justify-content-center registration__thank-you">
        <div class="container">
            <div class="row align-items-center">
                <div class="col text-center">
                    <img src="assets/images/mama-thank-you.svg" class="img-responsive" alt="">
                </div>
                <div class="col col-text">
                    <h2><?= $this->input('title') ?></h2>
                    <?= $this->wysiwyg('description') ?>
                    <?php
                    $moEngageOsmUtm = $this->placeholder('moEngageOsmUtm')->getValue();

                    if (is_array($moEngageOsmUtm)) {
                        foreach ($moEngageOsmUtm as $variable) {
                            echo $variable;
                        }
                    }

                    if (is_string($moEngageOsmUtm)) {
                        echo $moEngageOsmUtm;
                    }
                    ?>
                    <?= $this->link('link', [
                        'class' => 'btn btn-primary mt-4'
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</form>

<?= $this->areablock('thankyou-mama-areablock', []) ?>
