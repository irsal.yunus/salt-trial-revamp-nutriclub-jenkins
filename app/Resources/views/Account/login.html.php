<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/01/2020
 * Time: 18:36
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\WebsiteSetting;

$this->extend('layout.html.php');
$form = $this->form;

$this->form()->setTheme($form, [':Form/login']);
?>



<?= $this->form()->start($form, [
    'attr' => [
        'class' => 'registration'
    ]
])
?>
<?php $this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/register.css'); ?>
<?php $this->headLink()->offsetSetStylesheet(5, '/assets/css/pages/login.css'); ?>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="registration__imagewrapper col">
                    <div class="registration__image">
                        <img src="/assets/images/register__step1/content/family.svg" />
                    </div>
                </div>
                <div class="col">

                    <div class="registration__form">
                        <div class="registration__content registration__otp active" id="step-4">

                            <div class="row login__wrapper">
                                <div class="col-10 offset-1 col-lg-10 offset-lg-2">
                                <div class="registration__content-btn mb-0">
                                <a href="/" class="d-flex">
                                    <p><?= $this->t('BACK') ?></p>
                                </a>
                            </div>
                                <div class="content__card rounded">
                                <?php if ($this->error) { ?>
                                    <div class="alert alert-danger"><?php echo $this->error->getMessage() ?></div>
                                <?php } ?>
                                <div class="row">
                                    <?= $this->form()->row($form['username'], [
                                        'label' => $this->t('FORM_LOGIN_USERNAME_LABEL'),
                                        'attr' => [
                                            'placeholder' => $this->t('FORM_LOGIN_USERNAME_PLACEHOLDER')
                                        ]
                                    ]) ?>

                                     <div class="col-md-8 offset-md-2 ">
                                        <div class="text-left">
                                            <label class="required" for="password"><?= $this->t('PASSWORD') ?></label>
                                        </div>
                                        <div class="input-group">
                                            <?= $this->form()->widget($form['password'], [
                                                'attr' => [
                                                    'class' => 'password form-control',
                                                    'placeholder' => $this->t('FORM_LOGIN_PASSWORD_PLACEHOLDER'),
                                                ]
                                            ]) ?>
                                            <div class="showpassword input-group-append">
                                                <span class="input-group-text">
                                                    <i class="fa fa-eye-slash"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="registration__text text-right">
                                            <a href="<?= WebsiteSetting::getByName('FORGOT_PASSWORD_LINK') ? WebsiteSetting::getByName('FORGOT_PASSWORD_LINK')->getData() : null ?>" class="registration__text-forgot">
                                                <?= $this->t('FORGOT_PASSWORD') ?>
                                            </a>
                                        </div>
                                    </div>
                                    <?= $this->form()->widget($form['_submit'], [
                                        'label' => $this->t('LOGIN'),
                                        'attr' => [
                                            'class' => 'btn btn-secondary btn-block mb-4 mr-3 ml-3 mt-md-5 d-block d-sm-none',
                                            'disabled' => 'disabled'

                                        ]
                                    ]) ?>
                                </div>
                                <div class="row d-none d-sm-block">
                                    <div class="col-6 offset-md-3">
                                        <button type="submit" class="btn btn-secondary btn-block" disabled>
                                            <?= $this->t('LOGIN') ?>
                                        </button>
                                    </div>
                                </div>
                                <a href="<?= $this->path('account-register') ?>">
                                    <p class="content__have-account text-center pt-md-4">
                                        <?= $this->t('Don’t have an account? Sign up here!') ?>
                                    </p>
                                </a>
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= $this->form()->end($form) ?>

<?= $this->areablock('login-areablock', []) ?>

<?php $this->headScript()->offsetSetFile(9, '/assets/js/pages/login.js', 'text/javascript', []); ?>
