<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 19:08
 */

use Pimcore\Model\Document;
use Pimcore\Navigation\Page;

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

/** @var Page $page */
foreach ($this->pages as $page) {
    $page->setClass(null);
    $page->setClass('footer__title');

    $document = Document::getById($page->getId(), 1);

    if (!$document->hasProperty('NAV_FOOTER')) {
        continue;
    }

    $name = $page->getLabel();
    $page->setCustomHtmlAttribs([
        'data-name' => $name,
        'event-name' => 'CLICK_MENU_FOOTER'
    ]);

    echo $this->navigation()->menu()->htmlify($page);
}

?>
