<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/02/2020
 * Time: 22:05
 */

use Pimcore\Templating\GlobalVariables;
use Pimcore\Templating\Helper\Placeholder\Container;
use Pimcore\Templating\PhpEngine;

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

if (!$this->pages) {
    /** @var Container $pages */
    $containerPlaceholder = $this->placeholder('addBreadcrumb');
    $pages = $containerPlaceholder->getValue();

    if ($pages) {
        foreach ($pages as $page) {
        ?>
        <li class="breadcrumb-item">
            <a href="<?= $page->getUri() ?>"
               data-name="<?= $page ?>"
               event-name="CLICK_BREADCRUMBS"
               class="<?= $page->getClass() ?>">
                <?= $page ?>
            </a>
        </li>
        <?php
        }
    }
}
?>

<?php if ($this->pages) { ?>
<?php
    /** @var Pimcore\Navigation\Page\Document $page */
    foreach($this->pages as $page):
        $name = $page->getLabel();
        $page->setCustomHtmlAttribs([
            'data-name' => $name,
            'event-name' => 'CLICK_BREADCRUMBS'
        ])
?>
    <li class="breadcrumb-item">
        <?= $this->navigation()->menu()->htmlify($page) ?>
    </li>
<?php endforeach; ?>
<?php } ?>
