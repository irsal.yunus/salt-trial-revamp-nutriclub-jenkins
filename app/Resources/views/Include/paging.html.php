<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 04/03/2020
 * Time: 14:57
 */
?>
<div class="pagination-container mt-4">
    <ul class="pagination justify-content-end">
        <li class="page-item disabled">
            <a class="page-link" href="javascript:void(0)" type="page-first" page-target="1">First</a>
        </li>
        <li class="page-item disabled">
            <a class="page-link" href="javascript:void(0)" type="page-prev" page-target="prev"><i class="fa fa-caret-left"></i></a>
        </li>
        <li class="page-item active">
            <a class="page-link" href="javascript:void(0)" type="page-number" page-target="1">1</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="javascript:void(0)" type="page-next" page-target="next"><i class="fa fa-caret-right"></i></a>
        </li>
        <li class="page-item">
            <a class="page-link" href="javascript:void(0)" type="page-last" page-target="1">Last</a>
        </li>
    </ul>

    <input type="hidden" class="active-page" value="1">
    <input type="hidden" class="total-page" value="<?= $this->last ?>">
</div>