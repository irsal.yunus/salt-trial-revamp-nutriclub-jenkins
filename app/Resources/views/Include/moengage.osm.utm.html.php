<?php

use Pimcore\Model\DataObject\MoEngageOsmUtm;

/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource moengage.osm.utm.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 31/07/20
 * @time 23.08
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

/** @var MoEngageOsmUtm $moEngageOsmUtmObj */
$moEngageOsmUtmObj = $this->data;
?>
<?= $moEngageOsmUtmObj && $moEngageOsmUtmObj->getDescription() ? $moEngageOsmUtmObj->getDescription() : null ?>
<?= $moEngageOsmUtmObj && $moEngageOsmUtmObj->getTo() ? $moEngageOsmUtmObj->getTo()->getHtml() : null ?>
