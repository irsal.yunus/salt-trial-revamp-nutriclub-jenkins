<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/doctor-team.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>
<div class="doctor-team">
    <div class="doctor-team__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <?= $this->areablock('team-area-block', []) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->areablock('team-breadcrumbs-area-block', [
    'allowed' => 'breadcrumbs'
]) ?>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' )
    ->offsetSetFile(24, '/assets/js/pages/doctor-team.js', 'text/javascript' );
?>

