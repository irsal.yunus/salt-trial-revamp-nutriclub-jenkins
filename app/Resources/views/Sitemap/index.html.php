<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/sitemap.css", 'screen', false, [
        'defer' => 'defer'
    ]);

if ($this->editmode)
{ ?>
    <style type="text/css">
        .second-lists-block {
            margin-left: 20px;
        }
    </style>
<?php } ?>

<div class="sitemap">
    <div class="sitemap__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="sitemap__heading">
                        <h1><?= $this->input('sitemap-title') ?></h1>
                        <hr>
                    </div>
                    <div class="sitemap__description">
                        <?= $this->wysiwyg('sitemap_description') ?>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <?= $this->inc('/snippets/simple-page/side-card', []) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->areablock('sitemap-area-block', [
    'allowed' => 'breadcrumbs'
]) ?>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>
