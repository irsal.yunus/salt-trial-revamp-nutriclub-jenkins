<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Helper\SeoFriendlyHelper;

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/privacy.css", 'screen', false, [
        'defer' => 'defer'
    ]);

if ($this->editmode)
{ ?>
    <style type="text/css">
        .second-lists-block {
            margin-left: 20px;
        }
    </style>
<?php } ?>
<div class="privacy">
    <div class="privacy__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="privacy__heading">
                        <h1><?= $this->t('PRIVACY_QUESTIONS') ?></h1>
                        <hr>
                    </div>
                    <div class="privacy__description">
                        <p>
                            <?= $this->wysiwyg('privacy-description') ?>
                        </p>
                        <div class="alert alert-secondary" role="alert">
                            <?= $this->wysiwyg('privacy-alert') ?>
                        </div>
                    </div>
                    <div class="privacy__menu container">
                        <ul>
                            <?php while($this->block("privacy-lists-block")->loop()) {
                                $listText = $this->link('privacy-list-link')->getText();
                                $renderUrl = SeoFriendlyHelper::renderListFriendlyUrl($listText, 'list-style-none', '#');
                                $renderUrl = !$this->editmode ? $renderUrl : $this->link('privacy-list-link');
                                ?>
                                <li>
                                    <?= $renderUrl ?>
                                </li>
                                <?php while($this->block("privacy-second-lists-block", ['class' => 'second-lists-block'])->loop()) {
                                    $listText = $this->link('privacy-second-list-link')->getText();
                                    $renderUrl = SeoFriendlyHelper::renderListFriendlyUrl($listText, 'list-style-none', '#');
                                    $renderUrl = !$this->editmode ? $renderUrl : $this->link('privacy-second-list-link');
                                    ?>
                                    <ul>
                                        <li>
                                            <?= $renderUrl ?>
                                        </li>
                                    </ul>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- Privacy Heading Start -->
                    <?php while($this->block("privacy-subheading")->loop()) {
                        $subHeadingUniqueId = SeoFriendlyHelper::friendlyURL($this->input('privacy-subheading-title')->getData())
                        ?>
                        <div class="privacy__subheading">
                            <div class="privacy__subheading-header" id="<?= $subHeadingUniqueId ?>">
                                <h3><?= $this->input('privacy-subheading-title')?></h3>
                            </div>
                            <div class="privacy__subheading-description">
                                <?= $this->wysiwyg('privacy-subheading-content')?>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- Privacy Heading End -->
                </div>
                <div class="col-12 col-md-4">
                    <?= $this->inc('/snippets/simple-page/side-card', []) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->areablock('privacy-area-block', [
    'allowed' => 'breadcrumbs'
]) ?>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>
