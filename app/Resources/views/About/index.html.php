<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/about.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>

<div class="about">
    <div class="about__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="about__heading">
                        <h1><?= $this->t('ABOUT_NUTRICLUB') ?></h1>
                        <hr>
                    </div>
                    <div class="about__description">
                        <?= $this->wysiwyg('about-description')?>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <?= $this->inc('/snippets/simple-page/side-card', []) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->areablock('about-area-block', [
    'allowed' => 'breadcrumbs'
]) ?>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>
