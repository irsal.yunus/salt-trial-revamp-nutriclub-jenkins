<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Helper\SeoFriendlyHelper;

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/cookie.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>

<div class="cookie">
    <div class="cookie__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="cookie__heading">
                        <h1><?= $this->t('COOKIE_STATEMENT')?></h1>
                        <hr>
                    </div>
                    <div class="cookie__description">
                        <?= $this->wysiwyg('cookie-description') ?>
                    </div>

                    <div class="cookie__menu container">
                        <ul>
                            <?php while($this->block("cookie-menu-block")->loop()) {
                                $listText = $this->link('privacy-second-list-link')->getText();
                                $renderUrl = SeoFriendlyHelper::renderListFriendlyUrl($listText, '', '#');
                                $renderUrl = !$this->editmode ? $renderUrl : $this->link('privacy-second-list-link');
                                ?>
                                <li>
                                    <?= $renderUrl ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>

                    <?php while($this->block("cookie-subheading")->loop()) {
                        $subHeadingUniqueId = SeoFriendlyHelper::friendlyURL($this->input('cookie-subheading-title')->getData());
                        ?>
                        <div class="cookie__subheading">
                            <div class="cookie__subheading-header" id="<?= $subHeadingUniqueId ?>">
                                <h3><?= $this->input('cookie-subheading-title')?></h3>
                            </div>
                            <div class="cookie__subheading-description">
                                <?= $this->wysiwyg('cookie-subheading-content')?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-12 col-md-4">
                    <?= $this->inc('/snippets/simple-page/side-card', []) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->areablock('cookie-area-block', [
    'allowed' => 'breadcrumbs'
]) ?>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>


