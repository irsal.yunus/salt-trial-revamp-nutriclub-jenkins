<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/01/2020
 * Time: 18:39
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document\Page;
use Pimcore\Tool;

$baseUrl = Tool::getHostUrl(getenv('HTTP_PROTOCOL'));
$user = $app->getUser();

if ($user) {
    /** @var \AppBundle\Model\DataObject\Customer $userSso */
    $userSso = $user ? $this->getRequest()->getSession()->get('UserProfile') : null;
}
?>
<!DOCTYPE html>
<html lang="<?= $this->getLocale() ?>">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="shortcut icon" type="image/png" href="/assets/images/favicon.png">
    <link rel="preload" as="font" href="<?= $baseUrl ?>/assets/fonts/GothamRounded-Bold.woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= $baseUrl ?>/assets/fonts/GothamRounded-Medium.woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= $baseUrl ?>/assets/fonts/GothamRounded-MediumItalic.woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= $baseUrl ?>/assets/fonts/GothamRounded-BookItalic.woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= $baseUrl ?>/assets/fonts/GothamRounded-Book.woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= $baseUrl ?>/assets/fonts/GothamRounded-LightItalic.woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= $baseUrl ?>/assets/fonts/GothamRounded-Light.woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= $baseUrl ?>/assets/fonts/GothamRounded-BoldItalic.woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= $baseUrl ?>/assets/css/vendor/fonts/slick.woff" crossorigin="anonymous">
    <link rel=  "preload" as="font" href="<?= $baseUrl ?>/assets/fonts/fontawesome-webfont.woff2?v=4.7.0" crossorigin="anonymous">
    <link rel="preload" as="script" href="/assets/js/vendor/jquery.min.js">
    <link rel="preload" as="script" href="/assets/js/vendor/bootstrap.bundle.min.js">
    <?php
        $this->headLink()
            ->offsetSetStylesheet(0, '/assets/css/vendor/bootstrap.css', false)
            ->offsetSetStylesheet(1, '/assets/css/vendor/font-awesome.min.css', false)
            ->offsetSetStylesheet(2, '/assets/css/main.css', false)
            ->offsetSetStylesheet(3, '/assets/css/vendor/animate.min.css', false)->setCacheBuster(false);


        $this->headTitle($this->websiteTitlePostFix)->setSeparator($this->websiteTitleSeparator);

        $document = $this->document;
        if ($document instanceof Page) {
            $pageTitle = $document->getTitle() ? $this->headTitle()->prepend($document->getTitle()) : null;

            $request = $this->getRequest();
            $attributes = $request->attributes;

            if (!$attributes->has('pimcore_request_source')) {
                $this->headMeta()->setDescription($document->getDescription());
            }
        }
    ?>
    <?php
    if ($this->editmode) {
    ?>
        <style type="text/css">
            .x-btn.pimcore_block_button_plus.x-unselectable.x-btn-default-small.x-border-box {
                z-index: 10000 !important;
            }
            header,nav{display:block}h3{margin-top:0;margin-bottom:.5rem}h3{margin-bottom:.5rem;font-weight:500;line-height:1.2}.col-1,.col-11,.col-2,.col-8,.col-md-1,.col-md-10,.col-md-12{position:relative;width:100%;padding-right:15px;padding-left:15px}@font-face{font-family:GothamRounded-Medium;src:url(../../../../assets/fonts/GothamRounded-Medium.eot);src:url(../../../../assets/fonts/GothamRounded-Medium.eot) format("embedded-opentype"),url(../../../../assets/fonts/GothamRounded-Medium.woff2) format("woff2"),url(../../../../assets/fonts/GothamRounded-Medium.woff) format("woff"),url(../../../../assets/fonts/GothamRounded-Medium.ttf) format("truetype"),url(../../../../assets/fonts/GothamRounded-Medium.svg#FuturaStdLight) format("svg");font-display:swap}@font-face{font-family:GothamRounded-Book;src:url(../../../../assets/fonts/GothamRounded-Book.eot);src:url(../../../../assets/fonts/GothamRounded-Book.eot) format("embedded-opentype"),url(../../../../assets/fonts/GothamRounded-Book.woff2) format("woff2"),url(../../../../assets/fonts/GothamRounded-Book.woff) format("woff"),url(../../../../assets/fonts/GothamRounded-Book.ttf) format("truetype"),url(../../../../assets/fonts/GothamRounded-Book.svg#FuturaStdLight) format("svg");font-display:swap}*,:after,:before{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%}header,main,nav{display:block}body{margin:0;font-family:-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}h2,h3,h5{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}ul{margin-bottom:1rem}ul{margin-top:0}ul ul{margin-bottom:0}a{color:#007bff;text-decoration:none;background-color:rgba(0,0,0,0)}img{border-style:none}img{vertical-align:middle}button{border-radius:0}button,input{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}button{text-transform:none}[type=button],[type=submit],button{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{padding:0;border-style:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}h2,h3,h5{margin-bottom:.5rem;font-weight:500;line-height:1.2}h2{font-size:2rem}h3{font-size:1.75rem}h5{font-size:1.25rem}.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:576px){.container{max-width:540px}.form-inline .form-control{display:inline-block;width:auto;vertical-align:middle}}.row{display:flex;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.col-1,.col-11,.col-12,.col-2,.col-8,.col-md-1,.col-md-10,.col-md-12,.col-md-5{position:relative;width:100%;padding-right:15px;padding-left:15px}.col-1{flex:0 0 8.33333%;max-width:8.33333%}.col-2{flex:0 0 16.66667%;max-width:16.66667%}.col-8{flex:0 0 66.66667%;max-width:66.66667%}.col-11{flex:0 0 91.66667%;max-width:91.66667%}.col-12{flex:0 0 100%;max-width:100%}.form-control{display:block;width:100%;height:calc(1.5em + .75rem + 2px);padding:.375rem .75rem;font-size:1rem;font-weight:400;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem}.form-control::-ms-expand{background-color:rgba(0,0,0,0);border:0}.form-inline{display:flex;flex-flow:row wrap;align-items:center}.btn{display:inline-block;font-weight:400;color:#212529;text-align:center;vertical-align:middle;background-color:rgba(0,0,0,0);border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem}.btn-primary{color:#fff;background-color:#007bff;border-color:#007bff}.btn-secondary{color:#fff;background-color:#6c757d;border-color:#6c757d}.fade:not(.show){opacity:0}.nav-link{display:block;padding:.5rem 1rem}.navbar{position:relative;padding:.5rem 1rem}.navbar,.navbar>.container{display:flex;flex-wrap:wrap;align-items:center;justify-content:space-between}.navbar-brand{display:inline-block;padding-top:.3125rem;padding-bottom:.3125rem;margin-right:1rem;font-size:1.25rem;line-height:inherit;white-space:nowrap}.navbar-nav{display:flex;flex-direction:column;padding-left:0;margin-bottom:0;list-style:none}.navbar-nav .nav-link{padding-right:0;padding-left:0}@media (max-width:991.98px){.navbar-expand-lg>.container{padding-right:0;padding-left:0}}.close{float:right;font-size:1.5rem;font-weight:700;line-height:1;color:#000;text-shadow:0 1px 0 #fff;opacity:.5}button.close{padding:0;background-color:rgba(0,0,0,0);border:0;appearance:none}.modal{position:fixed;top:0;left:0;z-index:1050;display:none;width:100%;height:100%;overflow:hidden;outline:0}.modal-dialog{position:relative;width:auto;margin:.5rem}.modal.fade .modal-dialog{transform:translateY(-50px)}.modal-content{position:relative;display:flex;flex-direction:column;width:100%;background-color:#fff;background-clip:padding-box;border:1px solid rgba(0,0,0,.2);border-radius:.3rem;outline:0}.modal-header{display:flex;align-items:flex-start;justify-content:space-between;padding:1rem;border-bottom:1px solid #dee2e6;border-top-left-radius:.3rem;border-top-right-radius:.3rem}.modal-header .close{padding:1rem;margin:-1rem -1rem -1rem auto}.modal-title{margin-bottom:0;line-height:1.5}.modal-footer{display:flex;align-items:center;justify-content:flex-end;padding:1rem;border-top:1px solid #dee2e6;border-bottom-right-radius:.3rem;border-bottom-left-radius:.3rem}.modal-footer>:not(:first-child){margin-left:.25rem}.modal-footer>:not(:last-child){margin-right:.25rem}.d-none{display:none!important}.d-block{display:block!important}.d-flex{display:flex!important}@media (min-width:576px){.modal-dialog{max-width:500px;margin:1.75rem auto}.d-sm-none{display:none!important}.d-sm-block{display:block!important}}.justify-content-end{justify-content:flex-end!important}.position-relative{position:relative!important}.fixed-top{top:0}.fixed-top{position:fixed;right:0;left:0;z-index:1030}.shadow-none{box-shadow:none!important}.mt-0{margin-top:0!important}.mb-0{margin-bottom:0!important}.pt-0{padding-top:0!important}.p-3{padding:1rem!important}.text-center{text-align:center!important}body{background:#f7f9fa;font-family:GothamRounded-Medium;margin-top:60px}body,html{overflow-x:hidden}:focus{outline:0!important}main{min-height:calc(100vh - 430px)}#navbarNav{width:100%}@media (max-width:991px){.header .nav-item{display:none}.header .nav-item.header__logo{display:block;padding:0!important;width:auto}}.header .header__tool{color:#040303;font-size:12px;align-items:center;min-height:40px;justify-content:space-evenly}.header .header__caption{font-family:GothamRounded-Medium;font-size:15px;color:#040303;font-weight:300}.header .header__search{color:#000;display:block;align-items:center;flex-flow:row}.header .header__search .live-search{outline:0;border:1px solid #ced4da!important}.header .header__search .header__search-submit{background:url(../../../../assets/images/icons/loop-search.svg) no-repeat;width:30px;height:40px;background-size:cover;appearance:none;border:none;outline:0}.header-result>.row{margin:0;width:100%}.header-result .close-query-icon{position:absolute;right:10px;bottom:15px;display:none}#site-header{padding-bottom:0}#site-header>.header{z-index:3}#site-header:before{position:absolute;background:#fff;width:100%;height:55px;top:0;left:0;content:""}#site-header:after{position:absolute;background:#fff;border-radius:100%;top:30px;left:50%;content:"";width:104%;z-index:-1;height:50px;box-shadow:0 8px 6px -6px rgba(0,0,0,.2);margin-left:-52%}#site-header .header-result:after{position:absolute;background:#fff;border-radius:100%;top:23px;left:0;content:"";width:100%;z-index:-1;height:50px}.hamburger-menu__lists{background-color:#f7f9fa;width:100%;z-index:9999;height:100%;top:0}.hamburger-menu__lists .hamburger-menu__header{background-color:#f1f8fc}.hamburger-menu__lists .hamburger-menu__header h3{padding:15px 0 0;color:#00adee;font-family:GothamRounded-Medium}@media (max-width:767px){#navbarNav{margin:0}.header .nav-link{padding:0}#site-header{padding:8px 0 0}#site-header:after{top:20px;width:120%;left:50%;margin-left:-60%}#site-header .header-result:after{top:20px}.hamburger-menu__lists .hamburger-menu__header h3{text-align:center}}.hamburger-menu__lists-item{list-style:none;padding-left:0}.hamburger-menu__lists-item.bg-grey li{font-family:GothamRounded-Book}.hamburger-menu__lists-item.bg-grey li:first-child{font-family:GothamRounded-Medium}.hamburger-menu__lists-item a{color:#303030}.hamburger-menu__lists-item .hamburger-menu__item{padding:5px 0;text-align:center}.hamburger-menu__lists-item li{font-family:GothamRounded-Medium}.bg-grey{background:#f1f1f1}.search__results{height:100vh;position:fixed;top:0;left:0;width:100%;display:block;z-index:99;background:#fff;padding-top:100px}.search__results-text{color:#26419e;margin-bottom:0!important}.search__results-history{margin-bottom:25px}.search__results-text-btn-wrapper{display:flex;justify-content:space-between}.search__results-lists{list-style:none;padding-inline-start:0;background:#fff;padding:5px 15px 15px;border:1px solid #303030}.logo-nutriclub{width:120px;max-width:120px}.nav-link{min-width:145px}.delete-cookie{color:#00adee}.header__search #submit{visibility:hidden}.btn-primary{background:#26419e;color:#fff;font-family:GothamRounded-Medium;border:none;font-size:18px}.btn-blue{background:#00adee;color:#fff;font-family:GothamRounded-Medium;border:none;font-size:18px}
        </style>
    <?php
    }
    ?>

    <?php

        echo $this->headMeta();
        echo $this->headTitle();

        echo $this->headLink();
    ?>
    <script type="application/javascript">
        <?php
        /** @var \AppBundle\Model\DataObject\Customer $customer */
        if ($app->getUser() && $customer = $this->session()->get('UserProfile')) {
            //@todo please remove this if block, when you merge into phase2
        ?>
        var FIRST_NAME = "<?= $customer->getFirstname() ?>";
        var LAST_NAME = "<?= $customer->getLastname() ?>";
        var USER_REWARDS_ID = "<?= $customer->getId() ?>";
        var NAME = "<?= $customer->getFullname() ?>";
        var EMAIL = "<?= $customer->getEmail() ?>";
        var PHONE_NUMBER = "<?= $customer->getPhone() ?>";
        var GENDER = "<?= $customer->getGender() ?>";
        <?php
        }
        ?>

        var ES_URL = "<?= getenv('ES_URL') ?>";
        var IS_LOGGED_IN = "<?= $app->getUser() ? 'true' : 'false' ?>";
        var CURRENT_PERSONA = "<?= $this->placeholder('currentPersona') ?>";
        var IS_DESKTOP = "<?= $this->device()->isDesktop() ? 'true' : 'false' ?>";
        var REWARDS_API_BASE_URL = "<?= env('REWARDS_API') ?>"
        var COOKIE_CONSENT = "<?= session_id() ?>"
    </script>

    <script type="text/javascript">
        console.log("datalayer ready");
        function getValue(selector){
            let target = document.querySelector(selector);
            if(target !== null){
                return target.value;
            }else{
                return "";
            }
        }

        function getText(selector){
            let target = document.querySelector(selector);
            if(target !== null){
                return target.textContent;
            }else{
                return "";
            }
        }

        function getLength(selector){
            let target = document.querySelector(selector);
            if(target !== null){
                return target.length;
            }else{
                return ""
            }
        }

        function ClickEvent(selector, clickEvent){
            document.querySelector(selector).addEventListener("click", clickEvent());
        }
        var user_id = ''; //doneF
        var client_id = ''; //done
        var ad_blocker_status = ''; //done
        var video_present = ''; //done
        var reward_point = ''; //done
        var membership_status = ''; //done
        var page_ref = '';
        var article_length = '';

        <?php
        $dataLayer = $this->placeholder('dataLayer')->getValue();

        if (is_array($dataLayer)) {
            foreach ($dataLayer as $variable) {
                echo $variable;
            }
        }

        if (is_string($dataLayer)) {
            echo $dataLayer;
        }
        ?>

        if( window.canRunAds === undefined ){
            // adblocker detected, show fallback
            ad_blocker_status = 'true';
        }
        else {
            ad_blocker_status = 'false';
        }

        if(getLength(".hamburger-menu__profile")> 0){
            membership_status = 'yes';
        }
        else {
            membership_status = 'no'
        }
        if(getLength(".video-detail")> 0){
            video_present = 'true';
        }
        else {
            video_present = 'false';
        }

        user_id = getText(".hamburger-menu__profile h4 span");
        client_id = getText(".hamburger-menu__profile h4 span");
        reward_point = parseInt(getText(".point"));

        if(getLength("#article_published_date") > 0){
            content_category = getText(".heading__article h4");
            content_subcategory = getText(".heading__article h4");
        }

        dataLayer = [
            {
                "user_id" : user_id,
                "content_category" : content_category,
                "content_type" : content_type,
                "content_subcategory" : content_subcategory,
                "ad_blocker_status" : ad_blocker_status,
                "video_present" : video_present,
                "reward_point" : reward_point,
                "membership_status" : membership_status,
                "article_length" : article_length
            }
        ]
    </script>
    <?php
    if (!$this->editmode) {
        $prodGtm = $this->websiteConfig('GTM_PRODUCTION', 'GTM-WPBL2W5');
        $gtm = getenv('PIMCORE_ENVIRONMENT') === 'prod' ? $prodGtm : 'GTM-NKXG24D';
    ?>

    <!-- Google Tag Manager -->
    <script async>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?= $gtm ?>');</script>
    <!-- End Google Tag Manager -->
    <?php } ?>
</head>
<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= $gtm ?>"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <header>
        <?php
        $module = $this->getRequest()->get('contentDocument')->getModule() ?? 'AppBundle';
        ?>
        <?= $this->template('@' . $module . '/Resources/views/Include/nav.html.php', [
            'documentInitiator' => $this->document->getId()
        ])
        ?>
    </header>

    <main>
        <?= $this->slots()->get('_content') ?>
    </main>


    <?= $this->inc('/snippets/footer', []) ?>

<?php

        $this->headScript()
            ->offsetSetFile(0, '/assets/js/vendor/jquery.min.js', 'text/javascript' )
            ->offsetSetFile(1, '/assets/js/app.min.js', 'text/javascript')
            ->offsetSetFile(2, '/assets/js/vendor/bootstrap.bundle.min.js', 'text/javascript')
            ->offsetSetFile(5, '/assets/js/pages/search.js', 'text/javascript')
            ->offsetSetFile(7, '/assets/js/widgets/footer.js', 'text/javascript')
            ->offsetSetFile(16, '/assets/js/pages/ads.js', 'text/javascript', [
                "defer" => "defer"
            ])
            ->offsetSetFile(21, '/assets/vendor/modernizr/modernizr-custom.js', 'text/javascript', [
                "defer" => "defer"
            ])
            ->offsetSetFile(22, '/assets/js/widgets/datepicker.js', 'text/javascript', [
                "defer" => "defer"
            ])
            ->offsetSetFile(2000000, '/assets/js/tracking/tracking.helper.js', [
                'defer' => 'defer'
            ])
            ->offsetSetFile(2000001, '/assets/js/tracking/widgets/all.a.js', [
                'defer' => 'defer'
            ])
            ->setCacheBuster(false);

        if ($app->getUser()) {
            $this->headScript()->offsetSetFile(30, '/assets/js/pages/search.login.js', 'text/javascript', [
                'defer' => 'defer'
            ]);
        }

        echo $this->headScript();
?>

    <!-- email use hidden for datalayer -->
    <input type="hidden" id="user_email_datalayer" value="<?= $userSso ? $userSso->getEmail() : 'USER_NOT_LOGGEDIN' ?>">
    <div class="modal" id="modal-error" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer text-center">
                    <a id="btn-popup-register" href="<?= $this->path('account-register') ?>" type="button" class="btn btn-primary">Daftar</a>
                </div>
            </div>
        </div>
    </div>
    <div class="floating-icon">
        <button id="btn-floating" class="floating-icon__btn" aria-label="careline">
            <img src="/assets/images/home-careline/floating-icon.png" alt="">
        </button>
        <div class="floating-icon__parent">
            <div class="floating-icon__parent-comment">
                <img src="/assets/images/home-careline/comment.png" alt="">
            </div>
            <div id="floating" class="floating-icon__parent-after">
                <a href="javascript:void(0);" class="close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
                <div class="floating-icon__parent-after-phone">
                    <a href="https://wa.me/6282120000100?text=Hai%20Nutriclub%20Expert%20Advisor" class="floating-icon__parent-after-phone-item">
                        <img src="/assets/images/home-careline/wa.png" alt="">
                        <span>WhatsApp</span>
                    </a>
                    <a href="tel:08001360360" class="floating-icon__parent-after-phone-item" target="_blank">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <span>Call Us <i class="text-tol">(Toll Free)</i></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
