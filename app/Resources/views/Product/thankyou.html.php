<?php

use AppBundle\Model\DataObject\Customer;
use AppBundle\Model\DataObject\Product;
use Pimcore\Model\DataObject\Fieldcollection\Data\TermsConditions;
use Pimcore\Model\Document\Tag\Link;

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

echo $this->headLink()->offsetSetStylesheet(12344, '/assets/css/vendor/animate.min.css');
echo $this->headLink()->offsetSetStylesheet(12343, '/assets/css/pages/freesample-thankyou.css');

$user = $app->getUser();

if ($user) {
    /** @var \AppBundle\Model\DataObject\Customer $userSso */
    $userSso = $user ? $this->getRequest()->getSession()->get('UserProfile') : null;
}

/** @var Product $product */
$product = $this->product;

/** @var Link $facebook */
$facebook = $this->facebook;
/** @var Link $twitter */
$twitter = $this->twitter;
/** @var Link $whatsApp */
$whatsApp = $this->whatsApp;
?>

<div class="content free-sample free-sample__thank-you bg-white py-lg-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-4 text-center mb-3">
                <div class="free-sample__thank-you-avatar">
                     <img alt="" src="/assets/images/page_product/<?= $user ? $userSso->getGenderFriendlyName() : null ?>-thank-you.svg">
                </div>
            </div>
            <div class="col-12 col-lg-5 text-center d-lg-flex flex-column align-items-center">
                <h2>
                    <?= $this->t('THANKYOU_FREE_SAMPLE', [
                        '%genderFriendlyName' => $user ? ucfirst($userSso->getGenderFriendlyName()) : null,
                        '%name' => $user ? $userSso->getFullname() : null
                    ]) ?>
                </h2>
                <p class="mb-5">
                    <?= $this->t('DESCRIPTION_FREE_SAMPLE', [
                        '%genderFriendlyName' => $user ? ucfirst($userSso->getGenderFriendlyName()) : null,
                        '%name' => $user ? $userSso->getFullname() : null,
                        '%productName'  => $product->getName()
                    ]) ?>
                </p>
                <p class="flex-grow mb-5">
                    <?= $this->t('PRODUCT_SAMPLE_SHARE', [
                        '%genderFriendlyName' => $user ? ucfirst($userSso->getGenderFriendlyName()) : null,
                        '%name' => $user ? $userSso->getFullname() : null,
                        '%productName' => $product->getName()
                    ]) ?>
                </p>
                <div class="free-sample__share d-flex flex-wrap justify-content-center mb-5">
                    <a href="<?= $whatsApp->getHref() ?>">
                        <img src="/assets/images/icons/icon-whatsapp-large.png" class="d-block d-md-none" alt="">
                    </a>
                    <a href="<?= $facebook->getHref() ?>">
                        <img src="/assets/images/icons/facebook.png" alt="">
                    </a>
                    <a href="<?= $twitter->getHref() ?>">
                        <img src="/assets/images/icons/twitter.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-12">
                <ul class="list-unstyled free-sample__link-back text-center">
                    <li>
                        <a href="<?= $this->url('document_1') ?>">
                            <?= $this->t('PRODUCT_SAMPLE_BACK_TO_HOMEPAGE') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->url('PRODUCT', [
                            'groupSlug' => $product->getGroup()->getSlug(),
                            'productSlug' => $product->getSlug()
                        ]) ?>">
                            <?= $this->t('PRODUCT_SAMPLE_BACK_TO_PRODUCT_DESCRIPTION') ?>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- Accordion -->
            <div class="col-12 free-sample__accordion-wrapper py-3">
                <div class="accordion-info">
                    <?php
                    if (!$this->editmode && $product->getTermsConditions() !== null) {
                        $terms = $product->getTermsConditions()->getItems();

                        /** @var TermsConditions $term */
                        foreach ($terms as $term) {
                            if ($term->getHideOnThankYou()) {
                                continue;
                            }
                            ?>
                            <div class="accordion-section mb-3 border-0">
                                <div class="container px-0">
                                    <button class="accordion_trigger bg-blue rounded">
                                        <?= $term->getTitle() ?>
                                    </button>
                                </div>
                                <div class="panel slideInDown px-0" style="display: none;">
                                    <div class="container p-4 animated fadeIn bg-white border">
                                        <?= $term->getContent() ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->areablock('product-free-sample-thank-you', [
    'allowed' => 'breadcrumbs'
]) ?>
