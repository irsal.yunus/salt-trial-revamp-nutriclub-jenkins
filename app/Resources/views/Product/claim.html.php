<?php

/**
 * Created by PhpStorm.
 * User: Budi Perkasa (budi.perkasa@salt.co.id)
 */

use AppBundle\Model\DataObject\Customer;
use AppBundle\Model\DataObject\Product;
use Pimcore\Model\DataObject\Fieldcollection\Data\ProductQuestions;
use Pimcore\Model\DataObject\Fieldcollection\Data\TermsConditions;

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
echo $this->headLink()->offsetSetStylesheet(12346, '/assets/css/pages/freesample-form.css');

/** @var Product $product */
$product = $this->product;
$user = $app->getUser();
if($user) {
    /** @var Customer $userSso */
    $userSso = $user ? $this->getRequest()->getSession()->get('UserProfile') : null;
}
?>
<div class="content free-sample free-sample__form">
    <div class="container">
        <div class="row justify-content-center">
            <div class="free-sample__form-header col-12 d-flex justify-content-center text-center">
                <h2><?= $this->t('HELP_US_TO_GET_KNOW_YOU_FREE_SAMPLE') ?></h2>
            </div>
            <?php
            if ($flash = $this->session()->getFlash('claimNotif')) {
                ?>
            <div class="col-12 d-flex justify-content-center text-center mb-3">
                <div class="alert <?= $flash['StatusCode'] === '00' ? 'alert-success' : 'alert-danger' ?>" role="alert">
                    <?= $flash['StatusMessage'] ?>
                </div>
            </div>
                <?php
            }
            ?>
            <div class="col-12 col-md-4 mx-md-auto">
            <form method="post" novalidate>
                    <?php
                    if ((!$this->editmode && $product->getQuestions())) {
                        $questions = $product->getQuestions()->getItems();
                        /** @var ProductQuestions $question */
                        foreach ($questions as $key => $question) {
                            ?>
                            <label for="answer"><?= $question->getQuestion() ?></label>
                            <input type="hidden" name="questions[<?= $key  ?>][q]" value="<?= $question->getQuestion() ?>">
                            <div class="form-sample-wrapper position-relative">
                                <select id="answer" name="questions[<?= $key  ?>][a]" class="form-control custom-select custom-select-lg sampel">
                                    <option><?= $this->t('SELECT_ANSWERS_FREE_SAMPLE') ?></option>
                                    <?php
                                    $answers = $question->getAnswerBlock();
                                    foreach ($answers as $answer) {
                                        if (!$answer['answer']) {
                                            continue;
                                        }
                                        ?>
                                        <option value="<?= $answer['answer']->getData() ?>">
                                            <?= $answer['answer']->getData() ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <div class="invalid-tooltip alert-sampel">Please fill out this field</div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                        <input type="hidden" id="ProvinceID" name="provinceId">
                        <input type="hidden" id="DistrictID" name="districtId">
                        <input type="hidden" id="DistrictName" name="districtName">
                        <input type="hidden" id="ProvinceName" name="provinceName">
                        <div class="form-sample-wrapper border-top position-relative">
                        <label for="address"><?= $this->t('ADDRESS_FREE_SAMPLE') ?><sup>*</sup></label>
                        <textarea id="address" name="address" class="form-control address" rows="6" placeholder="Jalan Aries 3D No. 19, Kembangan, Jakarta Barat, Jakarta, 11620" required></textarea>
                        <div class="invalid-tooltip address-validation">Please fill out this field</div>
                    </div>
                    <div class="form-group mt-4">
                        <label class="mb-0"><?= $this->t('CITY_DISTRICT_FREE_SAMPLE') ?><sup>*</sup></label>
                        <label class="smalltext">Ketik Nama Provinsi untuk Menemukan Kota/Kab Anda</label>
                        <div class="province__wraper position-relative">
                            <div class="dropdown w-100">
                                <button class="w-100 btn-province btn btn-secondary dropdown-toggle d-flex justify-content-between align-items-center" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" dataProvinsi="0" aria-expanded="false">
                                    <?= $this->t('SELECT_REGION') ?>
                                    <img src="/assets/images/page_product/ionic-ios-arrow-down.svg">
                                </button>
                                <div class="dropdown-menu w-100 px-2" aria-labelledby="dropdownMenuButton">
                                        <input type="text" class="province__input form-control" placeholder="Ketik nama Provinsi" style="top:0;">
                                        <div class="dropdown-province__list p-2">
                                            <ul class="dropdown-province-items list-unstyled"></ul>
                                        </div>
                                </div>
                            </div>
                            <div class="invalid-tooltip province-validator">Please fill out this field</div>
                        </div>
                    <div class="input input-lg mt-4 form-sample-wrapper position-relative">
                        <label><?= $this->t('POSTAL_CODE_FREE_SAMPLE')?></label><sup>*</sup>
                        <input name="postalCode" type="number" id="kodepos" class="form-control no-arrow form-control-lg"
                        placeholder="11629" oninput="this.value=this.value.slice(0,this.maxLength)" maxlength="5" aria-label="Kode Pos" required>
                        <div class="invalid-tooltip postalcode">Please fill out this field</div>
                    </div>
                    <div class="d-flex align-items-start justify-content-between pt-3">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" value="" id="freesampleCheck">
                            <label class="custom-control-label kustom" for="freesampleCheck"></label>
                        </div>
                        <div class="chkbox-description">
                            <?=$this->t('CONTENT_TERMS_CONDITION_FREE_SAMPLE') ?>
                        </div>
                   </div>

                    <button type="submit" id="freeSampleSubmit" class="btn btn-primary btn-klaim w-100 my-3" disabled><?= $this->t('SUBMIT_CLAIM_FREE_SAMPLE')?></button>
                </form>
                <div class="d-flex legends">
                <p><span class="text-danger">*</span><?= $this->t('MANDATORY_TO_FILL') ?></p>
                </div>
            </div>

            <!-- Accordion -->
            <div class="d-lg-none col-12 free-sample__accordion-wrapper my-3">
                <?php
                if (!$this->editmode && $product->getTermsConditions()) {
                    $terms = $product->getTermsConditions()->getItems();
                    /** @var TermsConditions $term */
                    foreach ($terms as $term) {
                        ?>
                        <div class="accordion-info">
                            <div class="accordion-section border-0">
                                <div class="container px-0">
                                    <button class="accordion_trigger bg-blue rounded"><?= $term->getTitle() ?></button>
                                </div>
                                <div class="panel slideInDown px-0" style="display: none;">
                                    <div class="container p-4 bg-white animated fadeIn border">
                                        <?= $term->getContent() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } ?>
            </div>
        </div>
    </div>
</div>

<?= $this->areablock('claim-area-block', [
    'allowed' => 'breadcrumbs'
]) ?>


<?php $this->headScript()->offsetSetFile(199, '/assets/js/widgets/freeSample-selectregion.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(299, '/assets/js/widgets/freeSample-validation.js', 'text/javascript', []); ?>


