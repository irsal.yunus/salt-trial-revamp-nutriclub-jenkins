<?php

use AppBundle\Model\DataObject\ProductGroup;
use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/07/2020
 * Time: 16:51
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

echo $this->headLink()->offsetSetStylesheet(3, '/assets/css/pages/product-group.css');
$this->extend('layout.html.php');
/** @var ProductGroup $productGroup */
$productGroup = $this->productGroup;
?>
<div class="nutriclub-content-product-group">
    <?= $this->inc($productGroup && $productGroup->getSnippetPage() ?
        $productGroup->getSnippetPage()->getFullPath() : null
    )
    ?>
</div>

<?= $this->inc('/snippets/product/product-group') ?>
