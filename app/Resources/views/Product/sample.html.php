<?php
/**
 * phase1
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource claim.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi P
 * @since 28/07/20
 * @time 17.16
 *
 */

use AppBundle\Model\DataObject\Product;
use Pimcore\Model\DataObject\Fieldcollection\Data\TermsConditions;

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
echo $this->headLink()
          ->offsetSetStylesheet(12347, '/assets/css/pages/freesample-landing.css')
          ->offsetSetStylesheet(4, '/assets/css/vendor/slick.css', 'screen', false)
          ->offsetSetStylesheet(5, '/assets/css/vendor/slick-theme.css', 'screen', false);

/** @var Product $product */

/** @var Product $product */
$product = $this->product;

$user = $app->getUser();

$userSso = null;
if ($user) {
    /** @var \AppBundle\Model\DataObject\Customer $userSso */
    $userSso = $user ? $this->getRequest()->getSession()->get('UserProfile') : null;
}

?>

<div class="content free-sample free-sample__login py-2">
    <div class="free-sample__header">
    <!-- Background Mobile -->
    <img src="/assets/images/page_product/freesample-bg-mobile.png" class="free-sample__header-figure-mobile w-100 d-block d-md-none">
    <!-- Background Desktop -->
    <img src="/assets/images/page_product/bg-freesample-desktop.png" class="w-100 d-none d-md-block">
        <div class="overlay">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-6 pt-4 order-last order-md-first text-right">
                      <div class="d-flex justify-content-center justify-content-md-end">
                        <?= $product && $product->getImgFreeSample() ?
                            $product->getImgFreeSample()
                                ->getThumbnail('image-free-sample-thumbnail')
                                ->getHtml([
                                    'class' => 'product-image mt-4'
                                ]) : null
                        ?>
                      </div>
                    </div>
                    <div class="col-lg-6 px-lg-0">
                        <div class="free-sample__header-inside text-center pt-5 px-lg-0">
                            <span><?= $this->t('GET_IT_FREE_SAMPLE') ?></span>
                            <h1 class="text-uppercase">
                                <?= !$this->editmode && $product ? $product->getFreeSampleName() : '' ?>
                            </h1>
                            <span class="product-name">
                                <?= !$this->editmode && $product ? $product->getFreeSampleDescription() : '' ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="free-sample__login-alert bg-white text-center px-3 py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-10 mx-auto">
                <h2 class="py-2">
                    <?= $user && $userSso && !$product->getFreeSampleHi() ?
                        $this->t('HI_FREE_SAMPLE', [
                            '%genderFriendlyName' => ucfirst($userSso->getGenderFriendlyName()),
                            '%name' => $userSso->getFullname()
                        ]) : $this->t('HI_FREE_SAMPLE_BEFORE_LOGIN') ?>
                    <?= $user && $userSso && $product->getFreeSampleHi() ?
                         $product->getFreeSampleHiAfterLogin() : $product->getFreeSampleHiBeforeLogin()
                    ?>
                </h2>
                <?php if (!$product->getFreeSampleHiDescription()) { ?>
                <p>
                    <?= $user && $userSso ?
                        $this->t('CONTENT_WORDING_FREE_SAMPLE_AFTER_LOGIN', [
                            '%genderFriendlyName' => ucfirst($userSso->getGenderFriendlyName()),
                            '%productName' => $product->getName()
                        ]) : $this->t('CONTENT_WORDING_FREE_SAMPLE_BEFORE_LOGIN')
                    ?>
                </p>
                <?php } ?>
                <?= $user && $userSso && $product->getFreeSampleHiDescription() ?
                    $product->getFreeSampleHiDescriptionAfterLogin($userSso) : $product->getFreeSampleHiDescriptionBeforeLogin()
                ?>
                <?php if (!$product->getFreeSampleButton()) { ?>
                <a href="<?= $user && $userSso ? $this->url('PRODUCT_FREE_SAMPLE_CLAIM') : $this->url('account-register') ?>" class="btn btn-register-sampel my-3">
                    <?= $user ? $this->t('CLAIM_NOW_FREE_SAMPLE') : $this->t('REGISTER_FREE_SAMPLE') ?>
                </a>
                <?php } ?>
                <?= $user && $userSso && $product->getFreeSampleButton() ?
                    $product->getFreeSampleButtonAfterLogin() : $product->getFreeSampleButtonBeforeLogin()
                ?>
                <?php if(!$user) { ?>
                    <br>
                    <a href="<?= $this->url('account-login') ?>"><?= $this->t('LOGIN_FREE_SAMPLE')?></a>
                    <?= $this->t('IF_YOU_ARE_ALREADY_A_MEMBER_FREE_SAMPLE')?>
                <?php } ?>
        </div>
    </div>
      </div>
    </div>
    <?php if($freeSampleImage = $product->getFreeSampleImage()) { ?>
    <div class="free-sample__banner">
        <?php
            /** @var \Pimcore\Model\DataObject\Fieldcollection\Data\FreeSampleImage $image */
        foreach ($freeSampleImage as $image) {
        ?>
            <div class="free-sample__banner-item">
                <a href="<?= $image->getLink() ? $image->getLink()->getHref() : '#' ?>" class="free-sample__figure">
                    <?= $image->getMobile() ? $image->getMobile()
                        ->getThumbnail('free-sample-mobile-thumbnail')->getHtml([
                            'disableWidthHeightAttributes' => false,
                            'class' => 'img-fluid free-sample__figure-mobile'
                        ]) : null ?>
                    <?= $image->getDesktop() ? $image->getDesktop()
                        ->getThumbnail('free-sample-desktop-thumbnail')->getHtml([
                            'disableWidthHeightAttributes' => false,
                            'class' => 'img-fluid free-sample__figure-desktop'
                        ]) : null ?>
                </a>
            </div>
        <?php } ?>
    </div>
    <?php } ?>
    <div class="col-12 free-sample__accordion-wrapper py-3">
        <?php
        if (!$this->editmode && $product->getTermsConditions() !== null) {
            $terms = $product->getTermsConditions()->getItems();

            /** @var TermsConditions $term */
            foreach ($terms as $term) {
                ?>
                <div class="accordion-info">
                    <div class="accordion-section border-0">
                        <div class="container px-0">
                            <button class="accordion_trigger bg-blue rounded"><?= $term->getTitle() ?></button>
                        </div>
                        <div class="panel slideInDown px-0" style="display: none;">
                            <div class="container p-4 animated bg-white fadeIn border">
                                <?= $term->getContent() ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <div class="free-sample__login-alert bg-white text-center px-3 py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-10 mx-auto">
                    <?php if (!$product->getFreeSampleButton()) { ?>
                    <a href="<?= $user && $userSso ? $this->url('PRODUCT_FREE_SAMPLE_CLAIM') : $this->url('account-register') ?>" class="btn btn-register-sampel my-3">
                        <?= $user ? $this->t('CLAIM_NOW_FREE_SAMPLE') : $this->t('REGISTER_FREE_SAMPLE') ?>
                    </a>
                    <?php } ?>
                    <?= $user && $userSso && $product->getFreeSampleButton() ?
                        $product->getFreeSampleButtonAfterLogin() : $product->getFreeSampleButtonBeforeLogin()
                    ?>
                    <?php if(!$user) { ?>
                        <br>
                        <a href="<?= $this->url('account-login') ?>"><?= $this->t('LOGIN_FREE_SAMPLE')?></a>
                        <?= $this->t('IF_YOU_ARE_ALREADY_A_MEMBER_FREE_SAMPLE')?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->areablock('product-free-sample-area-block', [
    'allowed' => 'breadcrumbs'
]) ?>

<?php
    $this->headScript()
        ->offsetSetFile(3, '/assets/js/vendor/slick.min.js', 'text/javascript')
        ->offsetSetFile(4, '/assets/js/pages/free-sample.js', 'text/javascript');
?>
