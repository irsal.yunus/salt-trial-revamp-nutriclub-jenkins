<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/01/2020
 * Time: 13:21
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
echo $this->headLink()->offsetSetStylesheet(3, '/assets/css/pages/product.css');
?>
<div class="nutriclub-content-product">
<?php echo $this->areablock('product-areablock', []);

?>
</div>

<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="product"/>
<input type="hidden" id="content_type" value="landing_page"/>
<input type="hidden" id="content_subcategory" value="product_landing"/>

<?php
$this->headScript()->offsetSetFile(31, '/assets/js/pages/product.js', 'text/javascript', [])
?>
