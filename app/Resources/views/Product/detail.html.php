<?php

use AppBundle\Model\DataObject\Product;

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/01/2020
 * Time: 13:21
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */


echo $this->headLink()->offsetSetStylesheet(3, '/assets/css/pages/product-detail.css');
$this->headLink()->offsetSetStylesheet(4, '/assets/css/vendor/slick.css');
$this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/slick-theme.css');

$this->extend('layout.html.php');

/** @var Product $product */
$product = $this->product;

?>
<?= $this->inc($product && $product->getSnippetPage() ?
    $product->getSnippetPage()->getFullPath() : null
)
?>

<?php $this->headScript()->offsetSetFile(3, '/assets/js/vendor/slick.min.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(128, '/assets/js/widgets/product-testimoni.js', 'text/javascript', []); ?>
