<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 20:42
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

?>
<?php 
    $this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/tools.css   ', 'screen', false, [
        'defer' => 'defer'
    ])->setCacheBuster(false);
?>

<div class="tools">
<?php echo $this->areablock('tool-landing-areablock', []); ?>
</div>


<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="tools"/>
<input type="hidden" id="content_type" value="landing_page"/>
<input type="hidden" id="content_subcategory" value="tools_landing"/>