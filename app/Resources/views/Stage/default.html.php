<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 04/02/2020
 * Time: 14:29
 */
$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/font-awesome.min.css');
$this->headLink()->offsetSetStylesheet(6, '/assets/css/vendor/slick.css');
$this->headLink()->offsetSetStylesheet(7, '/assets/css/vendor/slick-theme.css');
$this->headLink()->offsetSetStylesheet(8, '/assets/css/pages/stage.css');
?>
<div class="stage-side">
    <?= $this->areablock('stage-landing-areablock', []); ?>
</div>
<?php $this->headScript()->offsetSetFile(2, '/assets/js/vendor/bootstrap.bundle.min.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(3, '/assets/js/vendor/slick.min.js', 'text/javascript', []); ?>

<?php $this->headScript()->offsetSetFile(4, '/assets/js/pages/stage.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(8, '/assets/js/widgets/card-slider-big.js', 'text/javascript', []); ?>

<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="stage"/>
<input type="hidden" id="content_type" value="landing_page"/>
<input type="hidden" id="content_subcategory" value="stage_landing"/>