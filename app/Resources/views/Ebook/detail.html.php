<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/03/2020
 * Time: 12:43
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

 use AppBundle\Model\DataObject\ArticleEbook;
use Pimcore\Model\Document\Tag\Link;
use Pimcore\Model\DataObject\Data\Link as DataLink;

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/ebook-detail.css   ', 'screen', false, [
    'defer' => 'defer'
])->setCacheBuster(false);

/** @var ArticleEbook $articleEbook */
$articleEbook = $this->article;
/** @var Link $fb */
$fb = $this->facebookShareAbleLink;
/** @var Link $twt */
$twt = $this->twitterShareAbleLink;
?>

<div class="ebook">
    <div class="container ebook__container">
        <!-- Desktop view -->
        <div class="ebook__card hidden-xs">
            <div class="ebook__cover">
                <?= $articleEbook->getImageDesktop()
                    ->getThumbnail('ebook-detail-thumbnail')->getHtml([
                        'disableWidthHeightAttributes' => true
                    ]) ?>
            </div>

            <div class="ebook__content">
                <div class="ebook__content--top">
                    <h3><?= $articleEbook->getTitle() ?? '' ?></h3>
                    <?= $articleEbook->getDescription() ?? '' ?>
                </div>
                <div class="ebook__content--bottom">
                    <?php
                    $assetsLink = ($articleEbook && $articleEbook->getAssetLink()) ?
                        $articleEbook->getAssetLink() : null;

                    /** @var DataLink */
                    if ($assetsLink) {
                        $assetsLink->setClass('ebook__content-btn');
                        $assetsLink->setText($this->t('EBOOK_DOWNLOAD'));

                        echo $assetsLink->getHtml();
                    }
                    ?>
                    <div class="ebook__content-action">
                        <?php
                         $fb->setText('<img src="/assets/images/icons/facebook.png" alt="facebook share" draggable="false">');
                         echo htmlspecialchars_decode($fb->frontend());
                        ?>
                        <?php
                        $twt->setText('<img src="/assets/images/icons/twitter.png" alt="twitter share" draggable="false">');
                        echo htmlspecialchars_decode($twt->frontend());
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Mobile view -->
        <div class="ebook__card visible-xs">
            <div class="ebook__cover">
                <?= $articleEbook->getImageDesktop()
                    ->getThumbnail('ebook-detail-mobile-thumbnail')->getHtml([
                        'disableWidthHeightAttributes' => true
                    ]) ?>
            </div>

            <div class="ebook__content">
                <div class="ebook__content--top">
                    <h3><?= $articleEbook->getTitle() ?? '' ?></h3>
                    <?php
                    /** @var DataLink */
                    if ($assetsLink) {
                        $assetsLink->setClass('ebook__content-btn');
                        $assetsLink->setText($this->t('EBOOK_DOWNLOAD'));

                        echo $assetsLink->getHtml();
                    }
                    ?>

                    <div id="descriptionToggle" class="collapse">
                        <p class="lable-desc">Deskripsi</p>
                        <?= $articleEbook->getDescription() ?? '' ?>

                        <div class="ebook__content-action">
                            <?php
                            $fb->setText('<img src="/assets/images/icons/facebook.png" alt="facebook share" draggable="false">');
                            echo htmlspecialchars_decode($fb->frontend());
                            ?>
                            <?php
                            $twt->setText('<img src="/assets/images/icons/twitter.png" alt="twitter share" draggable="false">');
                            echo htmlspecialchars_decode($twt->frontend());
                            ?>
                        </div>
                    </div>

                    <button data-toggle="collapse" href="#descriptionToggle" role="button" aria-expanded="false" aria-controls="collapseExample" class="ebook__content-btn ebook__content-btn--toggler"></button>

                </div>
            </div>
        </div>
    </div>

    <div class="container ebook__container">
    </div>
</div>

<?= $this->areablock('ebook-detail-areablock', []); ?>

<?= $this->inc('/snippets/ebook-detail', []) ?>

<?php $this->headScript()->offsetSetFile(14, '/assets/js/pages/ebook-detail.js', 'text/javascript', []); ?>
<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="ebook"/>
<input type="hidden" id="content_type" value="detail_page"/>
<input type="hidden" id="content_subcategory" value="ebook_detail"/>

<div class="modal" id="modal-error" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Modal body text goes here.</p>
            </div>
            <div class="modal-footer text-center">
                <a id="btn-popup-register" href="<?= $this->path('account-login') ?>" type="button" class="btn btn-primary">Login</a>
            </div>
        </div>
    </div>
</div>
