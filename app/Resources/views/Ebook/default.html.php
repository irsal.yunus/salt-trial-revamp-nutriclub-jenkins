<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/03/2020
 * Time: 12:43
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/ebook.css', 'screen', false, [
    'defer' => 'defer'
])->setCacheBuster(false);
?>
<div class="ebook">
     <?= $this->areablock('ebook-landing-areablock', []); ?>
</div>
<?php $this->headScript()->offsetSetFile(14, '/assets/js/includes/pagination.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(15, '/assets/js/widgets/ebook-list.js', 'text/javascript', []); ?>
<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="ebook"/>
<input type="hidden" id="content_type" value="ebook"/>
<input type="hidden" id="content_subcategory" value="ebook_landing"/>
