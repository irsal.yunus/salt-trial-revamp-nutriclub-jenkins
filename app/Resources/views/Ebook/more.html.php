<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/03/2020
 * Time: 22:44
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\ArticleEbook;

/** @var ArticleEbook\Listing $ebookObject */
$ebookObject = $this->ebookObject;

echo $this->render('Widgets/CardEbookList/list.html.php', [
    'data' => $ebookObject
]);
?>
