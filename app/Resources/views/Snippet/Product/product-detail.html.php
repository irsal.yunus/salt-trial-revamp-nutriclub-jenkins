<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/07/2020
 * Time: 19:35
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
echo $this->areablock('product-detail-area-block', [
    'globalParams' => [
        'product' => $this->product
    ]
    ]);
?>
