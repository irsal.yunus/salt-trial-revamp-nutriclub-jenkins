<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 18:55
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Navigation\Page;

// get root node if there is no document defined (for pages which are routed directly through static route)
$document = $this->document;
if(!$document instanceof Document\Page) {
    $document = Document\Page::getById(1);
}

// get the document which should be used to start in navigation | default home
$mainNavStartNode = $this->document->getProperty("navigationRoot");
if(!$mainNavStartNode instanceof Document\Page) {
    $mainNavStartNode = Document\Page::getById(1);
}

// this returns us the navigation container we can use to render the navigation
$mainNavigation = $this->navigation()->buildNavigation($document, $mainNavStartNode);
?>
<footer>
    <div class="footer__notification active">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <p>Website ini menggunakan <a href="https://old.nutriclub.co.id/cookies" target="_blank"><i>cookies</i></a> untuk memastikan Anda mendapat pengalaman terbaik di dalam website kami. <a href="/kebijakan-privasi" target="_blank">Pelajari lebih lanjut</a></p>
                </div>
                <div class="col-md-4 text-center align-self-center">
                    <button class="footer__notfication-btn btn btn-primary p-2">
                        Saya Setuju
                    </button>
                </div>
            </div>
        </div>
    </div>
    <?= $this->areablock('footer-areablock', []); ?>
    <div class="info__contacts">
        <div class="row">
            <div class="info__contacts-call col-md-6 col-6 text-center p-3 text-white">
                <div class="info__contacts-call-item ">
                    <?php
                    if ($this->editmode) {
                        echo $this->link('tel', []);
                        echo $this->input('telText', []);
                    }

                    if (!$this->editmode) {
                        $prefix = '<div class="info__contacts-figure">
                        <img src="/assets/images/contents/phone.png" alt="phone icon">
                        <p>'. (!$this->input('telText')->isEmpty() ? $this->input('telText')->getData() : null) .'</p>
                        </div>';
                        ?>
                        <?= $this->link('tel', [
                            'textPrefix' => $prefix
                        ])->frontend() ?>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="info__contacts-fullhour mt-1">
                <img src="/assets/images/contents/24hour.png" alt="img-24-hour" class="">
            </div>
            <div class="info__contacts-whatsapp col-md-6 col-6 text-center p-3 text-white">
                <div class="info__contacts-call-whatsapp">

                    <?php
                    if ($this->editmode) {
                        echo $this->link('wa', []);
                        echo $this->input('waText', []);
                    }

                    if (!$this->editmode) {
                        $prefix = '<div class="info__contacts-figure">
                        <img src="/assets/images/contents/whatsapp.png" alt="phone">
                        <p>'. (!$this->input('waText')->isEmpty() ? $this->input('waText')->getData() : null) .'</p>
                    </div>';
                        ?>
                        <?= $this->link('wa', [
                            'textPrefix' => $prefix
                        ])->frontend() ?>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row footer__social " style="align-items: center;">
            <div class="col col-md-12 ">
                <div class="col footer__social-media text-center ">
                    <?php
                    if ($this->editmode) {
                        echo $this->link('fb', []);
                        echo $this->link('ig', []);
                        echo $this->link('yt', []);
                    }
                    ?>
                    <div class="row justify-content-center d-none d-md-block mb-4 mt-4">
                        <?php if (!$this->editmode) { ?>
                        <?= $this->link('fb', [
                            'class' => 'pr-4 social-fb',
                            'textPrefix' => '<img src="/assets/images/register__step1/footer/fb.svg" alt="facebook logo"/>'
                        ]) ?>
                        <?= $this->link('ig', [
                            'class' => 'pr-4 social-ig',
                            'textPrefix' => '<img src="/assets/images/logo-ig.svg" alt="instagram logo" />'
                        ]) ?>
                        <?= $this->link('yt', [
                            'class' => 'social-yt',
                            'textPrefix' => '<img src="/assets/images/logo-yt.svg" alt="youtube Logo"/>'
                        ]) ?>
                        <?php } ?>

                    </div>
                    <div class="row justify-content-center d-block d-sm-none mb-4 mt-4">
                        <?php if (!$this->editmode) { ?>
                        <?= $this->link('fb', [
                            'class' => 'pr-3',
                            'textPrefix' => '<img src="/assets/images/register__step1/footer/fb.svg" class="social_media_icon"/>'
                        ]) ?>
                        <?= $this->link('ig', [
                            'class' => 'pr-3',
                            'textPrefix' => '<img src="/assets/images/logo-ig.svg" class="social_media_icon"/>'
                        ]) ?>
                        <?= $this->link('yt', [
                            'textPrefix' => '<img src="/assets/images/logo-yt.svg" class="social_media_icon"/>'
                        ]) ?>
                        <?php } ?>
                    </div>
                    <div class="text-center">
                        <?php
                             try {
                                 $footerNav = $this->navigation()
                                     ->menu()
                                     ->setPartial('Include/footer.partial.html.php')
                                     ->setMinDepth(null)
                                     ->render($mainNavigation);

                                 echo $footerNav;
                             } catch (Exception $e) {

                             }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__caption text-center">
        <p class="footer__caption"><?= $this->input('footer-copyright', []) ?></p>
    </div>
    </div>
</footer>
