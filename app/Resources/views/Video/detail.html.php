<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/03/2020
 * Time: 12:45
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\ArticleVideo;
use Pimcore\Model\Document\Tag\Link;

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/video-detail.css');

/** @var Link $fb */
$fb = $this->facebookShareAbleLink;
/** @var Link $twitter */
$twitter = $this->twitterShareAbleLink;

/** @var ArticleVideo $video */
$video = $this->video;
$others = $this->others;
?>

<!-- DATA LAYER -->
<input type="hidden" id="video_url" value="<?= $video->getYouTube()->getId() ?>" />
<input type="hidden" id="video_duration" value="<?= $video->getYouTube()->getDuration() ?>" />
<input type="hidden" id="video_published_date" value="<?= $video->getYouTube()->getPublishedAt()->format('Ymd') ?>" />
<input type="hidden" id="video_author_id" value="<?= $video->getYouTube()->getCreatorId() ?>" />
<!-- END DATA LAYER-->

<div class="video-detail">
    <div class="video-detail__wrapper">
        <div class="video-detail__wrapper-item container">
            <div class="video-detail__item">
                <iframe id="player" type="text/html" src="https://www.youtube.com/embed/<?= $video->getYouTube()->getId() ?>?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="video-detail__social-media">
                                    <?php
                                    $fb->setText('<img src="/assets/images/icons/icon-facebook.png" alt="facebook">');
                                    echo htmlspecialchars_decode($fb);

                                    $twitter->setText('<img src="/assets/images/icons/icon-twitter.png" alt="twitter">');
                                    echo htmlspecialchars_decode($twitter);
                                    ?>
                </div>
            <div class="video-detail__description">
                <h3 class="video-detail__description-title"><?= $video->getTitle() ?? ''?></h3>
                <div class="video-detail__description-text">
                    <?= $video->getContent() ?? '' ?>
                </div>
            </div>
            <div class="video-detail__description-mobile">
            <h3 class="video-detail__description-title"><?= $video->getTitle() ?? ''?>
                    <div class="accordion" id="accordion-toggle">
                        <div class="card">
                            <div id="collapse-description" class="hide collapse" aria-labelledby="video-description" data-parent="#accordion-toggle">
                                <div class="video-detail__description-mobile-text">
                                    <?= $video->getContent() ?? '' ?>
                                </div>
                                <div class="video-detail__social-media-mobile">
                                    <?php
                                    $fb->setText('<img src="/assets/images/icons/icon-facebook.png" alt="facebook">');
                                    echo htmlspecialchars_decode($fb);

                                    $twitter->setText('<img src="/assets/images/icons/icon-twitter.png" alt="twitter">');
                                    echo htmlspecialchars_decode($twitter);
                                    ?>
                                </div>
                            </div>
                            <h2 class="mb-0">
                                <button class="video-detail__description-btn btn btn-link" type="button" data-toggle="collapse" data-target="#collapse-description" aria-expanded="true" aria-controls="collapse-description">
                                </button>
                            </h2>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="video-detail__related">
        <div class="container">
            <h2 class="video-detail__related-header"><?= $this->t('VIDEO_MORE') ?></h2>
            <div class="row">
                <?php
                /** @var ArticleVideo $other */
                foreach ($others as $other) {
                    if (!$other) {
                        continue;
                    }
                    $title = $other->getTitle() ?? '';
                    $duration = $other->getVideoDuration() ?? '00:00';

                    $link = $other->getLink();

                    $link->setClass('video-detail__related-item col-12 col-md-3');
                    $desktopTags = '<img src="'. $other->getImageStandard() .'" srcset="'. $other->getImageMaxRes() .'" alt="' . $title . '" class="video-detail__figure">';
                    $desktopTags .= '<img src="/assets/images/contents/gold-play-button.png" alt="gold-play" class="video-detail__button">';
                    $desktopTags .= '<p class="video-detail__timeplay">'. $duration . '</p>';
                    $desktopTags .= '<p class="video-detail__title">' . $title . '</p>';
                    $link->setText($desktopTags);

                    // desktop
                    echo $link->getHtml(false);
                    $mobileTags = '<div class="col-12"><div class="video-detail__border"><div class="row"><div class="position-relative col-5 col-md-3">';
                    $mobileTags .= '<img class="video-detail__figure" src="'. $other->getImageStandard(). '" srcset="'. $other->getImageMaxRes() .'">';
                    $mobileTags .= '<img src="/assets/images/contents/gold-play-button.png" alt="gold-play" class="video-detail__mobile-btn">';
                    $mobileTags .= '</div>';
                    $mobileTags .= '<div class="video-detail__description-mobile col-7 col-md-8">';
                    $mobileTags .= '<p class="video-detail__description-mobile-title">'. $title .'</p>';
                    $mobileTags .= '<p class="video-detail__description-mobile-timeplay">' . $duration .'</p>';
                    $mobileTags .= '</div></div></div></div>';

                    $link->setClass('video-detail__related-item-mobile');
                    $link->setText($mobileTags);

                    // mobile
                    echo $link->getHtml(false);
                    ?>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="video"/>
<input type="hidden" id="content_type" value="detail_page"/>
<input type="hidden" id="content_subcategory" value="video_detail"/>

<?= $this->inc('/snippets/video-detail', []) ?>
<?php $this->headScript()->offsetSetFile(11, '/assets/js/pages/video-detail.js', 'text/javascript', []); ?>
