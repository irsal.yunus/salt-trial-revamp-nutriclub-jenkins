<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/03/2020
 * Time: 22:27
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\ArticleVideo;

$videoObject = $this->videoObject;
?>

<?php
if ($videoObject) {
    echo $this->render('Widgets/CardVideoList/list.html.php', [
        'data' => $videoObject
    ]);
}
?>
