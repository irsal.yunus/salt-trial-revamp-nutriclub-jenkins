<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 04/02/2020
 * Time: 14:28
 */
$this->extend('layout.html.php');
?>

<?php
    $this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/video.css', 'screen', false, [
        'defer' => 'defer'
    ])->setCacheBuster(false);
?>
<div class="videos">
    <?= $this->areablock('video-landing-areablock', []); ?>
</div>
<?php $this->headScript()->offsetSetFile(14, '/assets/js/includes/pagination.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(15, '/assets/js/widgets/video-list.js', 'text/javascript', []); ?>

<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="video"/>
<input type="hidden" id="content_type" value="video"/>
<input type="hidden" id="content_subcategory" value="video_landing"/>
