<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/03/2020
 * Time: 09:28
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Countly\Document\Areabrick\CardPodcastList\CountlyModel;
use AppBundle\Countly\LinkBuilder;
use AppBundle\Model\DataObject\ArticlePodcast;

/** @var ArticlePodcast\Listing $podcasts */
$podcasts = $this->data;

$customAttrs = $this->websiteConfig('ARTICLE_PODCAST_ATTRIBUTE_LINK', null);
?>
<?php
if ($podcasts) {
    /** @var ArticlePodcast $podcast */
    foreach ($podcasts as $podcast) {
        ?>
        <?= LinkBuilder::start($podcast, new CountlyModel(), 'podcast__item', $customAttrs) ?>
            <div class="podcast__item-cover">
                <?= $podcast->getImageDesktop() ?
                    $podcast->getImageDesktop()
                        ->getThumbnail('card-postcast-list-desktop-thumbnail')
                        ->getHtml([
                            'disableWidthHeightAttributes' => true
                        ]) : ''
                ?>
            </div>
            <div class="podcast__item-desc">
                <p><?= $podcast->getTitle() ?></p>
            </div>
        <?= LinkBuilder::stop() ?>
        <?php
    }
}
?>
