<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 08/04/2020
 * Time: 12:22
 */

use AppBundle\Model\DataObject\ArticleEbook;

$ebookObject = $this->data;
?>
<?php
if ($ebookObject) {
    /** @var ArticleEbook $ebook */
    foreach ($ebookObject as $ebook) {
        ?>
        <div class="ebook__item">
            <div class="ebook__item-cover">
                <?= $ebook->getImageDesktop()->getThumbnail('ebook-page')->getHtml([

                ]) ?>
                <div class="ebook__item-hover">
                    <?php
                    $link = $ebook->getGeneratedLink();
                    if ($link) {
                        $link->setClass('ebook__btn');
                        $link->setText($this->t('EBOOK_DOWNLOAD'));

                        echo $link->getHtml();
                    }
                    ?>
                </div>
            </div>
            <div class="ebook__item-desc">
                <a href="<?= $link->getPath() ?>">
                    <p><?= $ebook->getTitle() ?></p>
                </a>
            </div>
        </div>
        <?php
    }
}
