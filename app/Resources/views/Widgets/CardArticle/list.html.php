<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 25/03/2020
 * Time: 14:50
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Countly\Document\Areabrick\CardArticle\CountlyModel;
use AppBundle\Countly\LinkBuilder;
use AppBundle\Model\DataObject\Article;

/** @var Article\Listing $articleObject */
$articleObject = $this->data;
?>

<?php
/** @var Article $object */
$splitter = 0;
if ($articleObject) {
    foreach ($articleObject as $object) {
        $categoryName = $object->getCategory() ? $object->getCategory()->getName() : null;
        $title = $object->getTitle();
        $img = $object->getImgDekstopHtml('default', [
            'class' => 'img-fluid'
        ]);

        $customAttrs = $this->websiteConfig('ARTICLE_ATTRIBUTE_LINK', null);

        $splitter++;
        //<div class="row is-video">
        ?>
        <div class="card__articles-item col-12 col-md-6 ">
            <?= LinkBuilder::start($object, new CountlyModel(), null, $customAttrs) ?>
                <div class="row">
                    <div class="card__articles-item-figure col-4 col-md-3">
                        <?= $img ?>
                        <img src="/assets/images/contents/play-button.png" alt="play-button" class="play-button">
                    </div>
                    <div class="col-8 col-md-8">
                        <p class="card__articles-category"><?= $categoryName ?></p>
                        <p class="card__articles-text"><?= $title ?></p>
                        <div class="card__articles-timeplay">06:30</div>
                    </div>
                </div>
            <?= LinkBuilder::stop() ?>
        </div>

        <?php
        if ($splitter === 2) {
            $splitter = 0;
            ?>
            <hr class="card__articles-underline ">
            <?php
        }
        ?>
        <?php
    }
}
?>
