<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 17:39
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */


use AppBundle\Countly\Document\Areabrick\CardVideoList\CountlyModel;
use AppBundle\Countly\LinkBuilder;
use AppBundle\Model\DataObject\ArticleVideo;

/** @var ArticleVideo\Listing $videos */
$videos = $this->data;

$customAttrs = $this->websiteConfig('ARTICLE_VIDEO_ATTRIBUTE_LINK', null);
?>
<?php
if ($videos) {
    /** @var ArticleVideo $video */
    foreach ($videos as $video) {
        $customAttrs .= ' youtube-id=' . $video->getYouTube()->getId();
?>
        <?= LinkBuilder::start($video, new CountlyModel(), 'card-video__list-item col-12 col-md-3', $customAttrs) ?>
            <img srcset="<?= $video->getYouTube()->getThumbnails()['maxres']['url'] ?>" alt="<?= $video->getTitle() ?>" class="card-video__figure">
            <img src="/assets/images/contents/gold-play-button.png" alt="gold-play" class="card-video__button">
            <p class="card-video__list-timeplay"><?= $video->getYouTube()->getDuration() ?></p>
            <p class="card-video__list-title"><?= $video->getTitle() ?? '' ?></p>
        <?= LinkBuilder::stop() ?>
        <div class="card-video__list-item-mobile col-12">
            <?= LinkBuilder::start($video, new CountlyModel(), 'card-video__link', $customAttrs) ?>
                <div class="row">
                        <div class="position-relative col-5 col-md-3">
                            <img class="card-video__figure" srcset="<?= $video->getYouTube()->getThumbnails()['maxres']['url'] ?>">
                            <img src="/assets/images/contents/gold-play-button.png" alt="gold-play" class="card-video__mobile-btn">
                        </div>
                        <div class="card-video__description col-7 col-md-8">
                            <p class="card-video__description-title"><?= $video->getTitle() ?? '' ?></p>
                            <p class="card-video__description-timeplay"><?= $video->getYouTube()->getDuration() ?></p>
                        </div>
                    </div>
            <?= LinkBuilder::stop() ?>
        </div>
<?php
    }
}
?>
