<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/06/2020
 * Time: 14:32
 */

use AppBundle\Model\DataObject\Event;

$events = $this->data;

setlocale(LC_ALL, 'id_ID', 'id', 'ID');

if ($events) {
    foreach ($events as $event) {
        if (!$event instanceof Event) {
            continue;
        }
        ?>
        <div class="col-12 col-md-6 col-lg-4">
            <div class="card-event">
                <a href="<?= $event->getRouter() ?>">
                    <div class="card-event__thumbnail">
                        <?=  $event->getImageDesktop() ? $event->getImageDesktop()->getThumbnail('event-image-desktop-thumbnail')->getHtml([
                            'disableWidthHeightAttributes' => true
                        ]) : null ?>
                    </div>
                    <div class="card-event__content">
                        <div>
                            <p class="card-event__tag"><?= $event->getClassType() ?? '' ?></p>
                            <h3 class="card-event__title"><?= $event->getTitle() ?? '' ?></h3>
                        </div>

                        <div>
                            <ul class="card-event__list">
                                <li>
                                    <div><img src="/assets/images/icons/calendar.svg" alt="calendar icon"></div>
                                    <span><?= $event->getStartDate()->formatLocalized('%d %B %Y') ?></span>
                                </li>
                                <li>
                                    <div><img src="/assets/images/icons/clock.svg" alt="clock icon"></div>
                                    <span><?= $event->getStartTime() ?? '00:00' ?> - <?= $event->getEndTime() ?? '00:00' ?> <?= $this->t('INDONESIA_TIME_UNIT') ?></span>
                                </li>
                                <li>
                                    <div><img src="/assets/images/icons/maps-and-flags.svg" alt="maps icon"></div>
                                    <span><?= $event->getTag() ?? ''  ?></span>
                                </li>
                            </ul>
                            <div class="card-event__btn"><?= $this->t('EVENT_DETAIL_BUTTON') ?></div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
<?php
    }
}
?>

