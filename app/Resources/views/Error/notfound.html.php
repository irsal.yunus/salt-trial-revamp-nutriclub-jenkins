<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 18:08
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>
<!-- caling styling for pages  -->
<?php $this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/notfound.css'); ?>
<div class="error">  <!-- error -->
    <div class="error__wrapper"> <!-- error wrapper -->
        <div class="container"> <!-- container -->
            <div class="error__notfound"> <!-- error__notfound -->
                <div class="error__internalserver-figure text-center"> <!-- image code 404 -->
                    <img src="/assets/images/errors/404.png" alt="status-code-404" class="img-fluid">
                </div> <!-- End of image code 404 -->
                <div class="error__notfound-description text-center"> <!-- error description -->
                    <h4><?= $this->input('title') ?></h4>
                    <?= $this->wysiwyg('description') ?>
                    <?php if ($this->editmode) {   ?>
                        <?= $this->link('link', []) ?>
                    <?php } ?>
                    <?php if (!$this->editmode) {   ?>
                        <?= $this->link('link', [
                            'class' => 'btn'
                        ])->frontend() ?>
                    <?php } ?>
                    <br>
                    <small>Error Code: 404</small>
                </div> <!-- End of error description -->
            </div> <!-- end of error__notfound -->
        </div> <!-- end of container -->
    </div> <!-- end of error wrapper -->
</div> <!-- end of error -->

