<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 13:10
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>
<?php 
    $this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/category.css   ', 'screen', false, [
        'defer' => 'defer'
    ])->setCacheBuster(false);
?>
<div class="category">
    <div class="category__wrapper">
        <div class="container">
            <?php echo $this->areablock('category-landing-areablock', []); ?>
        </div>
    </div>
</div>