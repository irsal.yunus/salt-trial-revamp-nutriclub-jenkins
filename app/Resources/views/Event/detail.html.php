<?php

use AppBundle\Model\DataObject\Event;
use Pimcore\Templating\{GlobalVariables, PhpEngine};
use Pimcore\Model\Document\Tag\Link;

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/06/2020
 * Time: 16:37
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/vendor/slick.css', 'screen', false);
$this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/slick-theme.css', 'screen', false);
$this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/event-thanks.css', 'screen', false, [
    'defer' => 'defer'
])->setCacheBuster(false);

/** @var Event $event */
$event = $this->event;
/** @var Link $fb */
$fb = $this->facebookShareAbleLink;
/** @var Link $tw */
$tw = $this->twitterShareAbleLink;

setlocale(LC_ALL, 'id_ID', 'id', 'ID');
?>

<div class="event-body">
    <div class="container">
        <div class="mb-3">
            <a href="<?= $baseUrl ?>/event" class="event-body__back">
                <i class="fa fa-chevron-left"></i><?= $this->t('EVENT_BACK_BUTTON') ?>
            </a>
        </div>
        <?=  $event->getImageDesktop() ? $event->getImageDesktop()->getThumbnail('event-image-desktop-thumbnail')->getHtml([
            'disableWidthHeightAttributes' => true,
            'class' => 'event-body__cover'
        ]) : null ?>
        <p class="event-body__tag"><?= $event->getClassType() ?? ''?></p>
        <h1 class="event-body__title"><?= $event->getTitle() ?? '' ?></h1>

        <?php
        // retrieve messages
        foreach ($this->getRequest()->getSession()->getFlashBag()->get('notice', []) as $message) {
            echo '<div class="alert alert-danger">'.$message.'</div>';
        }
        ?>

        <div class="row mb-3">
            <?php
            if ($eventSpeaker = $event->getSpeaker()) {
            ?>
            <div class="col-12 col-sm-12 col-md-4">
                <div class="event-body__author">
                    <?= $eventSpeaker->getProfilePicture() ? $eventSpeaker->getProfilePicture()
                        ->getThumbnail('event-speaker-profile-picture-thumbnail')
                        ->getHtml([
                            'disableWidthHeightAttributes' => true,
                            'class' => 'event-body__author-avatar'
                        ]) : null
                    ?>
                    <div class="event-body__author-content">
                        <p class="event-body__author-name"><?= $eventSpeaker->getName() ?? '' ?></p>
                        <p class="event-body__author-title"><?= $eventSpeaker->getDescription() ?? '' ?></p>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
            <div class="col-12 col-sm-12 col-md-4">
                <div class="event-body__icon">
                    <div><img src="/assets/images/icons/calendar.svg" alt="calendar icon"></div>
                    <span><?= $event->getStartDate()->formatLocalized('%d %B %Y') ?></span>
                </div>
                <div class="event-body__icon">
                    <div><img src="/assets/images/icons/clock.svg" alt="clock icon"></div>
                    <span><?= $event->getStartTime() ?? '00:00' ?> - <?= $event->getEndTime() ?? '00:00' ?> <?= $this->t('INDONESIA_TIME_UNIT') ?></span>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4">
                <div class="event-body__icon">
                    <div><img src="/assets/images/icons/maps-and-flags.svg" alt="maps icon"></div>
                    <span><?= $event->getTag() ?? ''  ?></span>
                </div>
                <div class="event-body__share">
                    <span><?= $this->t('EVENT_SHARE_TO_MEDIA_SOCIAL')  ?></span>
                    <div>
                        <a href="javascript:void(0)" id="shareWhatsapp"><img src="/assets/images/icons/icon-whatsapp-large.png" alt="Whatsapp icon"></a>
                        <?php
                        $tw->setText('<img src="/assets/images/icons/twitter.png" alt="twitter-logo">');
                        echo htmlspecialchars_decode($tw->frontend());

                        $fb->setText('<img src="/assets/images/icons/facebook.png" alt="facebook-logo">');
                        echo htmlspecialchars_decode($fb->frontend());
                        ?>
                        <a href="javascript:void(0)" id="copyClipboard"><img src="/assets/images/icons/icon-copy-large.png" alt="Copy icon"></a>
                    </div>
                </div>
            </div>
        </div>

        <?= $this->inc('/snippets/event-register-top', [
            'isJoined' => $this->isJoined,
            'eventObj' => $event
        ]) ?>

        <hr class="event-body__sparator" />

        <h3><?= $this->t('EVENT_DESCRIPTION') ?></h3>
        <?= $event->getDescription() ?? '' ?>
        <hr class="event-body__sparator" />

        <button class="event-body__collapse-btn" type="button" data-toggle="collapse" data-target="#collapse-syarat" aria-expanded="false" aria-controls="collapse-syarat">
            <?= $this->t('EVENT_TERM_AND_CONDITION') ?> <i class="fa fa-chevron-left"></i>
        </button>
        <div class="collapse event-body__collapse-content show" id="collapse-syarat">
            <?= $event->getTerm() ?? '' ?>
        </div>

        <hr class="event-body__sparator" />

        <?= $this->inc('/snippets/event-register-bottom', [
            'isJoined' => $this->isJoined,
            'eventObj' => $event
        ]) ?>

    </div>
</div>

<?= $this->inc('/snippets/event-detail', []) ?>

<?php $this->headScript()->offsetSetFile(20, '/assets/js/widgets/event-share.js', 'text/javascript', []); ?>
