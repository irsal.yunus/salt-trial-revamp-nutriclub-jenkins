<?php

use AppBundle\Model\DataObject\Customer;
use AppBundle\Model\DataObject\Event;
use Pimcore\Templating\{GlobalVariables, PhpEngine};
use Pimcore\Model\Document\Tag\Link;

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 15/06/2020
 * Time: 09:57
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/vendor/slick.css', 'screen', false);
$this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/slick-theme.css', 'screen', false);
$this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/event-thanks.css', 'screen', false, [
    'defer' => 'defer'
])->setCacheBuster(false);

/** @var Event $event */
$event = $this->event;
/** @var Link $fb */
$fb = $this->facebookShareAbleLink;
/** @var Link $tw */
$tw = $this->twitterShareAbleLink;
/** @var Customer $user */
$user = $this->user;
?>

<div class="event-thanks">
    <div class="container">
        <div class="row align-items-center">
            <div class="col text-center event-thanks__img">
                <img src="/assets/images/<?= $user->getGenderFriendlyName() ?>-thank-you.svg" class="img-responsive">
            </div>
            <div class="col">
                <h2>Selamat Bergabung <?= ucfirst($user->getGenderFriendlyName()) ?>!</h2>
                <p>Terima kasih telah melakukan registrasi untuk event Nutriclub. Tim Careline Nutriclub siap 24/7 jika <?= ucfirst($user->getGenderFriendlyName()) ?> memiliki pertanyaan lebih lanjut mengenai event ini, nutrisi atau info tumbuh kembang si Kecil. Hubungi via WhatsApp : <a href="https://api.whatsapp.com/send?phone=+628123456789" class="txt-blue">082120000100</a></p>
                <a href="<?= $baseUrl ?>/event" class="event-thanks__btn">Lihat Event Lainnya</a>
            </div>
        </div>
        <button type="button" class="event-thanks__btn-detail" onclick="goToDetail()">
            <span><?= $this->t('EVENT_DETAIL') ?></span>
            <i class="fa fa-chevron-down"></i>
        </button>
    </div>
</div>

<div class="event-body" id="event-detail">
    <div class="container">
        <?=  $event->getImageDesktop() ? $event->getImageDesktop()->getThumbnail('event-image-desktop-thumbnail')->getHtml([
            'disableWidthHeightAttributes' => true,
            'class' => 'event-body__cover'
        ]) : null ?>
        <p class="event-body__tag"><?= $event->getClassType() ?? ''?></p>
        <h1 class="event-body__title"><?= $event->getTitle() ?? '' ?></h1>

        <div class="row mb-3">
            <?php
            if ($eventSpeaker = $event->getSpeaker()) {
                ?>
                <div class="col-12 col-sm-12 col-md-4">
                    <div class="event-body__author">
                        <?= $eventSpeaker->getProfilePicture() ?
                            $eventSpeaker->getProfilePicture()
                                ->getThumbnail('event-speaker-profile-picture-thumbnail')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'event-body__author-avatar'
                                ]) : null;
                        ?>
                        <div class="event-body__author-content">
                            <p class="event-body__author-name"><?= $eventSpeaker->getName() ?? '' ?></p>
                            <p class="event-body__author-title"><?= $eventSpeaker->getDescription() ?? '' ?></p>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="col-12 col-sm-12 col-md-4">
                <div class="event-body__icon">
                    <div><img src="/assets/images/icons/calendar.svg" alt="calendar icon"></div>
                    <span><?= $event->getStartDate()->format('d F Y') ?></span>
                </div>
                <div class="event-body__icon">
                    <div><img src="/assets/images/icons/clock.svg" alt="clock icon"></div>
                    <span><?= $event->getStartTime() ?? '00:00' ?> - <?= $event->getEndTime() ?? '00:00' ?> <?= $this->t('INDONESIA_TIME_UNIT') ?></span>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4">
                <div class="event-body__icon">
                    <div><img src="/assets/images/icons/maps-and-flags.svg" alt="maps icon"></div>
                    <span><?= $event->getTag() ?? ''  ?></span>
                </div>
                <div class="event-body__share">
                    <span><?= $this->t('EVENT_SHARE_TO_MEDIA_SOCIAL')  ?></span>
                    <div>
                        <a href="javascript:void(0)" id="shareWhatsapp"><img src="/assets/images/icons/icon-whatsapp-large.png" alt="Whatsapp icon"></a>
                        <?php
                        $tw->setText('<img src="/assets/images/icons/twitter.png" alt="twitter-logo">');
                        echo htmlspecialchars_decode($tw->frontend());

                        $fb->setText('<img src="/assets/images/icons/facebook.png" alt="facebook-logo">');
                        echo htmlspecialchars_decode($fb->frontend());
                        ?>
                        <a href="javascript:void(0)" id="copyClipboard"><img src="/assets/images/icons/icon-copy-large.png" alt="Copy icon"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-12 col-md-5">
                <button
                    type="button"
                    id="addToCalendar"
                    class="event-body__btn"
                    data-title="<?= $event->getTitle() ?? '' ?>"
                    data-start="<?= $event ? $event->getStartDate()->format('m-d-Y') : '' ?> <?= $event ? $event->getStartTime() . ':00' : '' ?>"
                    data-end="<?= $event ? $event->getStartDate()->format('m-d-Y') : '' ?> <?= $event ? $event->getEndTime() . ':00' : '' ?>"
                    data-description="<?= strip_tags($event->getDescription()) ?? '' ?>"
                    data-location="<?= $event->getTag() ?? ''   ?>"
                >
                    <?= $this->t('EVENT_ADD_TO_CALENDAR')  ?>
                </button>
            </div>
        </div>

        <hr class="event-body__sparator" />

        <button class="event-body__collapse-btn --center collapsed" type="button" data-toggle="collapse" data-target="#collapse-syarat" aria-expanded="false" aria-controls="collapse-syarat">
            <?= $this->t('EVENT_TERM_AND_CONDITION') ?> <i class="fa fa-chevron-left"></i>
        </button>
        <div class="collapse event-body__collapse-content" id="collapse-syarat">
            <?= $event->getTerm() ?? '' ?>
        </div>

        <hr class="event-body__sparator" />
    </div>
</div>

<?php $this->headScript()->offsetSetFile(9, '/assets/js/widgets/event-thanks.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(20, '/assets/js/widgets/event-share.js', 'text/javascript', []); ?>
