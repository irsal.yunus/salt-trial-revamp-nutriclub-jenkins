<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/06/2020
 * Time: 11:23
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/vendor/slick.css', 'screen', false);
$this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/slick-theme.css', 'screen', false);
$this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/event.css', 'screen', false, [
    'defer' => 'defer'
])->setCacheBuster(false);

echo $this->areablock('event-landing-areablock', []);
?>
<?php $this->headScript()->offsetSetFile(3, '/assets/js/vendor/slick.min.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(8, '/assets/js/widgets/card-slider-big.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(14, '/assets/js/includes/pagination.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(15, '/assets/js/widgets/event-list.js', 'text/javascript', []); ?>
