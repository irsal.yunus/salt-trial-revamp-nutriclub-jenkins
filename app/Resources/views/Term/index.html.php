<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Helper\SeoFriendlyHelper;

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/tac.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>
<div class="tac">
    <div class="tac__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="tac__heading">
                        <h1><?= $this->t('TERM_AND_CONDITION_NUTRICLUB_INDONESIA')?></h1>
                        <hr>
                    </div>
                    <div class="tac__description">
                        <p><?= $this->wysiwyg('tac-description')?></p>
                    </div>
                    <div class="tac__menu container">
                        <ol>
                            <?php while($this->block("tac-lists-block")->loop()) {
                                $listText = $this->link('tac-list-link')->getText();
                                $renderUrl = SeoFriendlyHelper::renderListFriendlyUrl($listText, '', '#');
                                $renderUrl = !$this->editmode ? $renderUrl : $this->link('tac-list-link');
                                ?>
                                <li>
                                    <?= $renderUrl ?>
                                </li>
                            <?php } ?>
                        </ol>
                    </div>
                    <!-- TAC SUBHEADING START -->
                    <?php while($this->block("tac-subheading")->loop()) { ?>
                        <div class="tac__subheading">
                            <?php
                            $subHeadingUniqueId = SeoFriendlyHelper::friendlyURL($this->input('tac-subheading-title')->getData())
                            ?>
                            <div class="tac__subheading-header" id="<?= $subHeadingUniqueId ?>">
                                <h3><?= $this->input('tac-subheading-title')?></h3>
                            </div>
                            <div class="tac__subheading-description">
                                <?= $this->wysiwyg('tac-subheading-content')?>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- TAC SUBHEADING END -->
                </div>
                <div class="col-12 col-md-4">
                    <?= $this->inc('/snippets/simple-page/side-card', []) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->areablock('term-area-block', [
    'allowed' => 'breadcrumbs'
]) ?>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>

