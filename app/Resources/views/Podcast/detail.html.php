<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/03/2020
 * Time: 12:44
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Countly\Document\Areabrick\CardPodcastList\CountlyModel;
use AppBundle\Countly\LinkBuilder;
use AppBundle\Model\DataObject\ArticlePodcast;
use Pimcore\Model\Document\Tag\Link;

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/podcast-detail.css   ', 'screen', false, [
    'defer' => 'defer'
])->setCacheBuster(false);
$this->headScript()->offsetSetFile(4, '/assets/js/widgets/card-podcast-detail.js', 'text/javascript', []);

/** @var Link $fb */
$fb = $this->facebookShareAbleLink;
/** @var Link $twitter */
$twitter = $this->twitterShareAbleLink;

/** @var ArticlePodcast $podcast */
$podcast = $this->podcast;
$others = $this->others;

$customAttrs = $this->websiteConfig('ARTICLE_PODCAST_ATTRIBUTE_LINK', null);
?>

<div class="podcast">
    <div class="container podcast__container">
        <div class="podcast__card position-relative">
            <div class="podcast__player">
                <iframe id="soundcloud-player" width="100%" scrolling="no" frameborder="no" allow="autoplay" src="" data-value="<?= $podcast->getSoundCloudId() ?? '' ?>">
                </iframe>
            </div>

            <div class="podcast__content hidden-xs">
                <div class="podcast__content-action">
                    <?php
                    $fb->setText('<img src="/assets/images/icons/icon-facebook.png" alt="facebook">');
                    echo htmlspecialchars_decode($fb);

                    $twitter->setText('<img src="/assets/images/icons/icon-twitter.png" alt="twitter">');
                    echo htmlspecialchars_decode($twitter);
                    ?>
                </div>

                <div class="podcast__content-description">
                    <h3><?= $podcast->getTitle() ?? '' ?></h3>
                    <?= $podcast->getContent() ?? '' ?>
                </div>
            </div>

            <div class="podcast__content pl-4 pr-4 visible-xs trigger-absolute" id="podcast-description-mobile">
                <div class="podcast__content-description-top">
                    <h3><?= $podcast->getTitle() ?? '' ?></h3>
                    <button class="podcast__content-description-top-btn play-btn">
                        <img src="/assets/images/icons/play.png" alt="">
                    </button>
                </div>

                <div id="descriptionToggle" class="podcast__content-description collapse">
                    <p class="lable-desc">Deskripsi</p>
                    <?= $podcast->getContent() ?>

                    <div class="podcast__content-action">
                        <?php
                        $fb->setText('<img src="/assets/images/icons/icon-facebook.png" alt="facebook">');
                        echo htmlspecialchars_decode($fb);

                        $twitter->setText('<img src="/assets/images/icons/icon-twitter.png" alt="twitter">');
                        echo htmlspecialchars_decode($twitter);
                        ?>
                    </div>
                </div>

                <button data-toggle="collapse" href="#descriptionToggle" role="button" aria-expanded="false" aria-controls="collapseExample" class="podcast__content-btn-toggler"></button>

            </div>
        </div>

    </div>

    <div class="container podcast__container-related">
        <div class="podcast__content">
            <div class="podcast__content-title">
                <p><?= $this->t('OTHER_PODCAST') ?></p>
            </div>

            <div class="podcast__content-list">
                <?php
                /** @var ArticlePodcast $other */
                foreach ($others as $other) {
                    ?>
                    <?= LinkBuilder::start($other, new CountlyModel(), 'podcast__content-item', $customAttrs) ?>
                        <div class="podcast__content-cover">
                            <?= $other->getImageDesktop()
                                ->getThumbnail('podcast-detail-desktop-thumbnail')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true
                                ])
                            ?>
                            <div class="podcast__content-cover-overlay">
                                <button class="podcast__content-cover-btn">
                                    <img src="/assets/images/icons/play.png" alt="">
                                </button>
                            </div>
                        </div>
                        <div class="podcast__content-item-desc">
                            <p>
                                <?= $other->getTitle() ?? '' ?>
                            </p>
                        </div>
                    <?= LinkBuilder::stop() ?>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?= $this->inc('/snippets/podcast-detail', []) ?>
<?php $this->headScript()->offsetSetFile(10, '/assets/js/widgets/soundcloud-player.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(11, '/assets/js/widgets/card-podcast-detail.js', 'text/javascript', []); ?>


<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="podcast"/>
<input type="hidden" id="content_type" value="detail_page"/>
<input type="hidden" id="content_subcategory" value="podcast_detail"/>
