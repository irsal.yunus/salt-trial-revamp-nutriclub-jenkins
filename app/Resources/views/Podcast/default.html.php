<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 04/02/2020
 * Time: 14:35
 */
$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/podcast.css   ', 'screen', false, [
    'defer' => 'defer'
])->setCacheBuster(false);
?>


<?php $this->headScript()->offsetSetFile(14, '/assets/js/includes/pagination.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(15, '/assets/js/widgets/podcast-list.js', 'text/javascript', []); ?>
<div class="podcast">
    <?= $this->areablock('podcast-landing-areablock', []); ?>
</div>

<!-- INPUT HIDDEN FOR DATA LAYER -->
<input type="hidden" id="content_category" value="podcast"/>
<input type="hidden" id="content_type" value="podcast"/>
<input type="hidden" id="content_subcategory" value="podcast_landing"/>
