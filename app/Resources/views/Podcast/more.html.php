<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 09:25
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\ArticlePodcast;

/** @var ArticlePodcast\Listing $podcastObject */
$podcastObject = $this->data;
?>

<?php
if ($podcastObject) {
    echo $this->render('Widgets/CardPodcastList/list.html.php', [
        'data' => $podcastObject
    ]);
}
?>
