<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/expert.css", 'screen', false, [
        'defer' => 'defer'
    ]);

if ($this->editmode) {
?>
    <style type="text/css">
        div#pimcore_editable_expert-card {
            display: flex;
            width: 100% !important;
        }
        div.pimcore_block_entry {
            width: 33.33333%;
        }
    </style>
<?php
}
?>

<div class="expert-team">
    <div class="expert-team__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-10 offset-md-1">
                    <div class="row">
                        <div class="col-12 col-md-8 offset-md-2">
                            <?php
                            $this->headScript()
                                ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
                            ?>
                            <div class="expert-team__heading">
                                <h1><?= $this->input('expert-title') ?></h1>
                                <hr>
                            </div>
                            <div class="expert-team__description">
                                <?= $this->wysiwyg('team-content') ?>
                            </div>
                        </div>
                    </div>
                    <div class="expert-team__lists">
                        <div class="row">
                            <?php
                            $maxWidth = $this->editmode ?'style="max-width: 100% !important"' : '';
                            while ($this->block('expert-card')->loop()) { ?>
                                <div class="expert-team__lists-item col-12 col-md-4" <?= $maxWidth ?>>
                                    <div class="card">
                                        <div class="card-header">
                                            <h3>
                                                <?= $this->input('expert-card-title', [
                                                    'placeholder'   => 'Input your title here'
                                                ]) ?>
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <p class="card-text">
                                                <?= $this->textarea('expert-card-content', [
                                                'placeholder' => 'Input your text here'
                                            ]) ?>
                                            </p>
                                            <?= $this->link('expert-card-link', [
                                                'class' => 'card-text__link text-blue'
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->areablock('expert-area-block', [
    'allowed' => 'breadcrumbs'
]) ?>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>
