#!/usr/bin/env bash
Working_Dir="/home/irsal/git"
Package_Name="revamp"
Lib_Dir="vendor"
DIr_Unit="phpunit"

if [ ! -d $Working_Dir/$Package_Name/$Lib_Dir/bin/$Dir_Unit ] || [ -f $Working_Dir/$Package_Name/$Lib_Dir/$Dir_Unit ]; then
   echo "Dir $Dir_Unit Not Found"
   exit 0
else
   echo "Dir $DIr_Unit Already..."
   find ${Library_Dir} ~ -type d -name "Tests" -print | xargs ./vendor/bin/phpunit
   echo "Php Unit Process.."
   exit 0
fi
