<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 01/04/2020
 * Time: 13:26
 */

namespace AppBundle\Traits;

trait ImagePlaceholder
{
    /**
     * @param string $width
     * @param string $height
     * @return string
     */
    public function generateImagePlaceholder($width = '250', $height = '250')
    {
        $placeholder = 'https://via.placeholder.com/' . $width . 'x' . $height;

        return '<img src='. $placeholder .' alt="Image Placeholder">';
    }
}
