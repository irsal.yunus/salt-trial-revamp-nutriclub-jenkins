<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 09/03/2020
 * Time: 11:04
 */

namespace AppBundle\Traits;

use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

trait Authenticate
{
    /** @var TokenStorageInterface $tokenStorage */
    private $tokenStorage;

    /** @var AuthenticationManagerInterface $authenticationManager */
    private $authenticationManager;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthenticationManagerInterface $authenticationManager
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }

    private function authenticate()
    {
        $usernameDummy = getenv('USERNAME_VALID_DUMMY', null);
        $passwordDummy = getenv('PASSWORD_VALID_DUMMY', null);

        $token = new UsernamePasswordToken($usernameDummy, $passwordDummy, 'cmf');

        try {
            $authenticatedToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authenticatedToken);

        } catch (AuthenticationException $e) {
            // clear token on auth failure
            $this->unAuthenticate();
        }
    }

    private function unAuthenticate()
    {
        $storedToken = $this->tokenStorage->getToken();
        if ($storedToken instanceof UsernamePasswordToken && $storedToken->getProviderKey() === 'cmf') {
            $this->tokenStorage->setToken(null);
        }
    }
}
