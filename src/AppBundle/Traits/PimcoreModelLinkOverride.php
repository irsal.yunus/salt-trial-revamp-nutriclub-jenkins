<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 31/03/2020
 * Time: 17:17
 */

namespace AppBundle\Traits;


use Pimcore\Model\DataObject\Data\Link;
use AppBundle\Model\Data\Link as LinkOverrided;

trait PimcoreModelLinkOverride
{
    public function setValueFromObject($link)
    {
        $newObjectOverrideModel = new LinkOverrided();

        if ($link instanceof  Link) {
            $values = [
                'text' => $link->getText(),
                'internalType'=>  $link->getInternalType(),
                'internal' => $link->getInternal(),
                'direct' => $link->getDirect(),
                'linktype' => $link->getLinktype(),
                'target' => $link->getTarget(),
                'parameters' => $link->getParameters(),
                'anchor' => $link->getAnchor(),
                'title' => $link->getTitle(),
                'accesskey' => $link->getAccesskey(),
                'rel' => $link->getRel(),
                'tabindex' => $link->getTabindex(),
                'class' => $link->getClass(),
                'attributes' => $link->getAttributes(),
            ];
            $newObjectOverrideModel->setValues($values);
        }

        return $newObjectOverrideModel;
    }
}
