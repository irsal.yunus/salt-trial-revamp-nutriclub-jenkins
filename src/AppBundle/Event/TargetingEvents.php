<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/03/2020
 * Time: 23:09
 */

namespace AppBundle\Event;

final class TargetingEvents
{
    /**
     * Fired after a condition was used which depends on the count of visited
     * pages. Will be used by VisitedPagesCountListener to update the page count
     * if there are conditions depending on it.
     *
     * @Event("Symfony\Component\EventDispatcher\Event")
     *
     * @var string
     */
    const VISITED_SAME_PAGES_COUNT_MATCH = 'pimcore.targeting.visited_same_pages_count_match';
}
