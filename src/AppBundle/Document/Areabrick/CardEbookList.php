<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/03/2020
 * Time: 11:09
 */

namespace AppBundle\Document\Areabrick;

use AppBundle\Helper\CardEbookListWidgetHelper;
use Pimcore\Model\Document\Tag\Area\Info;

class CardEbookList extends AbstractAreabrick
{
    public function action(Info $info)
    {
        $info->getView()->ebook = CardEbookListWidgetHelper::getObjectWithPagination(0, 4);
    }
}
