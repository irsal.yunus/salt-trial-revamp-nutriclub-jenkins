<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 17:43
 */

namespace AppBundle\Document\Areabrick;

use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Pimcore\Model\Document\Tag\Area\Info;
use Symfony\Component\HttpFoundation\Request;

class Breadcrumbs extends AbstractAreabrick
{
    /** @var BreadcrumbHelperService $breadcrumbHelperService */
    private $breadcrumbHelperService;

    public function __construct(BreadcrumbHelperService $breadcrumbHelperService)
    {
        $this->breadcrumbHelperService = $breadcrumbHelperService;
    }

    public function action(Info $info)
    {
        $info->getView()->breadcrumbHelperService = $this->breadcrumbHelperService;
    }
}
