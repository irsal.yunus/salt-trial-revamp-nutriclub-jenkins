<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 12:54
 */

namespace AppBundle\Document\Areabrick;

use AppBundle\Services\VisitorInfoService;
use AppBundle\Targeting\DataProvider\CardSliderOneLabel;
use Pimcore\Model\Document\Tag\Area\Info;

class CardColumnSmall extends AbstractAreabrick
{
    /** @var VisitorInfoService $visitorInfoService */
    private $visitorInfoService;

    public function __construct(VisitorInfoService $visitorInfoService)
    {
        $this->visitorInfoService = $visitorInfoService;
    }

    public function action(Info $info)
    {
        $visitorInfo = $this->visitorInfoService->getVisitorInfo();
        if (!$visitorInfo) {
            return;
        }
    }
}
