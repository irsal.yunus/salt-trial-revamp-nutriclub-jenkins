<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 13:19
 */

namespace AppBundle\Document\Areabrick;

use AppBundle\Model\DataObject\Customer;
use AppBundle\Services\AgeConverterService;
use AppBundle\Services\VisitorInfoService;
use Pimcore\Model\DataObject\ArticleCalendar;
use Pimcore\Model\DataObject\QuantityValue\UnitConversionService;
use Pimcore\Model\DataObject\StageAge;
use Pimcore\Model\Document\Tag\Area\Info;
use Pimcore\Targeting\Model\VisitorInfo;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CardColumnOne extends AbstractAreabrick
{
    /** @var VisitorInfoService $visitorInfo */
    private $visitorInfo;

    /** @var SessionInterface $session */
    private $session;

    /** @var AgeConverterService $ageConverterService */
    private $ageConverterService;

    public function __construct(
        AgeConverterService $ageConverterService,
        VisitorInfoService $visitorInfo,
        SessionInterface $session
    )
    {
        $this->visitorInfo = $visitorInfo;
        $this->session = $session;
        $this->ageConverterService = $ageConverterService;
    }

    public function action(Info $info)
    {
        /** @var Customer $user */
        $user = $this->session->get('UserProfile');
        $agePregnant = $user->getAgePregnant();

        /** @var StageAge\Listing $stageAge */
        $stageAge = $this->ageConverterService->convert($agePregnant);

        if (!$stageAge) {
            return;
        }

        $stageAgeObject = null;

        if ($stageAge->getCount() > 0) {
            /** @var StageAge $stageAgeObject */
            $stageAgeObject = $stageAge->getObjects()[0];
        }

        $articleCalendar = new ArticleCalendar\Listing();
        $articleCalendar->setCondition('stageAge__id = ?', [$stageAgeObject->getId()]);

        $info->getView()->articleCalendar = $articleCalendar->getObjects();
    }
}
