<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/02/2020
 * Time: 19:54
 */

namespace AppBundle\Document\Areabrick;

use AppBundle\Helper\CardArticleRelatedWidgetHelper;
use AppBundle\Helper\CardArticleWidgetHelper;
use AppBundle\Model\DataObject\ArticleCategory;
use AppBundle\Model\DataObject\Stage;
use Pimcore\Model\Document\Tag\Area\Info;
use Symfony\Component\HttpFoundation\RequestStack;

class CardArticleRelated extends AbstractAreabrick
{
    /** @var RequestStack $requestStack */
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function action(Info $info)
    {
        $masterRequest = $this->requestStack->getMasterRequest()->attributes->all();

        $stageSlug = $masterRequest['stageSlug'] ?? null;
        $articleCategorySlug = $masterRequest['articleCategorySlug'] ?? null;
        $articleSlug = $masterRequest['articleSlug'] ?? null;

        $relatedArticle = CardArticleRelatedWidgetHelper::getRelatedObject(
            Stage::getBySlug($stageSlug, 1),
            ArticleCategory::getBySlug($articleCategorySlug, 1),
            $articleSlug
        );

        $info->getView()->relatedArticleObjects = $relatedArticle->getObjects();
    }
}
