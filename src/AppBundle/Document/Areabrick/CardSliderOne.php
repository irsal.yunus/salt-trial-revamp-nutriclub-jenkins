<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:23
 */

namespace AppBundle\Document\Areabrick;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Targeting\DataProvider\CardSliderOneLabel;
use Pimcore\Model\Document\Tag\Area\Info;
use AppBundle\Services\Document\Areabrick\CardSliderOne as CardSliderOneService;

class CardSliderOne extends AbstractAreabrick
{
    /** @var SessionInterface $session */
    private $session;

    private $service;

    public function __construct(SessionInterface $session, CardSliderOneService $cardSliderOneService)
    {
        $this->session = $session;
        $this->service = $cardSliderOneService;
    }

    public function action(Info $info)
    {
        $document = $info->getDocument();
        $this->service->setDocument($document);
        $info->getView()->editModeModel = $this->service->getEditModeModel();
        $info->getView()->viewModeModel = $this->service->getViewModeModel();

        if (!$this->session->has(CardSliderOneLabel::PROVIDER_KEY)) {
            return;
        }

        $myCardSliderOneSessionData = $this->session->get(CardSliderOneLabel::PROVIDER_KEY);

        $info->getView()->myCardSliderOneSessionData = $myCardSliderOneSessionData;
    }
}
