<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/07/2020
 * Time: 17:01
 */

namespace AppBundle\Model;

interface SchemaOrgInterface
{
    public function getSchemaOrg();
}
