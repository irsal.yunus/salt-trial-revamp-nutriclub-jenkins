<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 13:10
 */

namespace AppBundle\Model\Widget;

class CardColumnSmallModel
{
    /** @var string $img */
    private $img;

    /** @var string $lastUsed */
    private $lastUsed;

    /** @var string $name */
    private $name;

    /** @var string $url */
    private $url;

    /**
     * @return string
     */
    public function getImg(): string
    {
        return $this->img;
    }

    /**
     * @param string $img
     */
    public function setImg($img): void
    {
        $this->img = $img;
    }

    /**
     * @return string
     */
    public function getLastUsed(): string
    {
        return $this->lastUsed;
    }

    /**
     * @param string $lastUsed
     */
    public function setLastUsed($lastUsed): void
    {
        $this->lastUsed = $lastUsed;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }
}
