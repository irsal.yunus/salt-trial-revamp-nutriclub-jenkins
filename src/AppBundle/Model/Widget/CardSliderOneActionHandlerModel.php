<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/02/2020
 * Time: 23:52
 */

namespace AppBundle\Model\Widget;

class CardSliderOneActionHandlerModel
{
    /** @var string $labelTitle */
    private $labelTitle;

    /** @var string $stageSlug */
    private $stageSlug;

    /**
     * @return mixed
     */
    public function getLabelTitle()
    {
        return $this->labelTitle;
    }

    /**
     * @param mixed $labelTitle
     */
    public function setLabelTitle($labelTitle): void
    {
        $this->labelTitle = $labelTitle;
    }

    /**
     * @return mixed
     */
    public function getStageSlug()
    {
        return $this->stageSlug;
    }

    /**
     * @param mixed $stageSlug
     */
    public function setStageSlug($stageSlug): void
    {
        $this->stageSlug = $stageSlug;
    }

    public function setValues($values = [])
    {
        if (!$values) {
            return;
        }

        foreach ($values as $key => $value) {
            $setter = 'set' . ucfirst($key);
            if (!method_exists($this, $setter)) {
                return;
            }
            $this->$setter($value);
        }
    }
}
