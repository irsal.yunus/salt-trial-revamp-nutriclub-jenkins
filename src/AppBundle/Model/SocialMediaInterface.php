<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 02/03/2020
 * Time: 12:04
 */

namespace AppBundle\Model;

interface SocialMediaInterface
{
    public function getTitle();

    public function getRouter();
}
