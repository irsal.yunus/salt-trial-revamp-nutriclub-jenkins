<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 10:26
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Model\Data\Link;
use AppBundle\Model\RouterInterface;
use AppBundle\Model\SocialMediaInterface;
use AppBundle\Traits\PimcoreModelLinkOverride;
use Pimcore\Model\DataObject\ArticleEbook as BaseArticleEbook;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Tool;

class ArticleEbook extends BaseArticleEbook implements RouterInterface, SocialMediaInterface
{
    use PimcoreModelLinkOverride;

    public function getRouterParams(): array
    {
        return [
            'stageSlug' => $this->getStage()->getSlug(),
            'articleCategorySlug' => $this->getCategory()->getSlug(),
            'articleSubCategorySlug' => $this->getSubCategory() ? $this->getSubCategory()[0]->getSlug() : null,
            'articleSlug' => $this->getSlug()
        ];
    }

    public function getRouter(): string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $protocol = getenv('HTTP_PROTOCOL', 'https');
            $url = Tool::getHostUrl($protocol) . $linkGenerator;
        } catch (\Exception $e) {

        } finally {
            return $url;
        }
    }

    public function getGeneratedLink()
    {
        /** @var Link $generatedLink */
        $generatedLink = parent::getGeneratedLink();
        $newGeneratedLink = $this->setValueFromObject($generatedLink);
        $newGeneratedLink->setDirect($this->getRouter());

        return $newGeneratedLink;
    }

    public function getImageWithAbsoluteUrl()
    {
        $imgDesktop = $this->getImageDesktop() ?: '';
        $protocol = getenv('HTTP_PROTOCOL', 'https');
        return Tool::getHostUrl($protocol) . $imgDesktop;
    }
}
