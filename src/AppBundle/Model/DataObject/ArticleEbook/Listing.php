<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 10:47
 */

namespace AppBundle\Model\DataObject\ArticleEbook;

use AppBundle\Model\RouterInterfaceListing;
use Pimcore\Model\DataObject\ {ArticleCategory, ArticleSubCategory, Stage};
use Pimcore\Model\DataObject\ArticleEbook\Listing as BaseArticleEbookListing;

class Listing extends BaseArticleEbookListing implements RouterInterfaceListing
{
    public function getObjectForStaticRoute($routeVariables = []): array
    {
        $data = [];
        if (!$routeVariables) {
            return $data;
        }

        /** @var Stage $stage */
        $stage = Stage::getBySlug($routeVariables['stageSlug'] ?? null, 1);
        /** @var ArticleCategory $articleCategorySlug */
        $articleCategorySlug = ArticleCategory::getBySlug($routeVariables['articleCategorySlug'] ?? null, 1);
        /** @var ArticleSubCategory $articleSubCategorySlug */
        $articleSubCategorySlug = ArticleSubCategory::getBySlug($routeVariables['articleSubCategorySlug'] ?? null, 1);

        if (!$stage || !$articleCategorySlug || !$articleSubCategorySlug) {
            return $data;
        }

        $this->addConditionParam('stage__id = ?', $stage->getId(), 'AND');
        $this->addConditionParam('category__id = ?', $articleCategorySlug->getId(), 'AND');
        $this->addConditionParam('subCategory LIKE ?', '%' . $articleSubCategorySlug->getId() . '%', 'AND');
        $this->addConditionParam('slug = ?', $routeVariables['articleSlug'], 'AND');

        return $data[] = $this->load();
    }
}
