<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 15/03/2020
 * Time: 00:04
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Model\Data\Link;
use AppBundle\Model\RouterInterface;
use AppBundle\Traits\PimcoreModelLinkOverride;
use Pimcore\Model\Asset\Image;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\Tool as BaseTool;

class Tool extends BaseTool implements RouterInterface
{
    use PimcoreModelLinkOverride;

    public function getRouterParams(): array
    {
        return [
            'slug' => $this->getSlug()
        ];
    }

    public function getRouter(): string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());
            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';


            $url = $linkGenerator;
        } catch (\Exception $e) {

        } finally {
            return $url;
        }
    }

    public function getIconThumbnail(string $thumbnailname = 'default', array $options = [], array $removeAttrs = [])
    {
        $default = 'https://via.placeholder.com/130';

        $checkIcon = $this->hasProperty('IMAGE_ICON')
            ? $this->getProperty('IMAGE_ICON') : null;

        if (!$checkIcon) {
            return '<img src="'. $default .'" alt="icon stage default">';
        }

        /** @var Image $icon */
        $icon = $checkIcon;

        return $icon->getThumbnail($thumbnailname)->getHtml($options, $removeAttrs);
    }

    public function getCurrentLink()
    {
        /** @var Link $currentLink */
        $currentLink = parent::getCurrentLink();
        $newGeneratedLink = $this->setValueFromObject($currentLink);
        //$newGeneratedLink->setDirect($this->getRouter());

        return $newGeneratedLink;
    }
}
