<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/02/2020
 * Time: 19:14
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Model\Data\Link;
use AppBundle\Model\RouterInterface;
use AppBundle\Traits\PimcoreModelLinkOverride;
use Pimcore\Model\Asset\Image;
use Pimcore\Model\DataObject\ArticleCategory as BaseArticleCategory;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\WebsiteSetting;

class ArticleCategory extends BaseArticleCategory implements RouterInterface
{
    use PimcoreModelLinkOverride;

    public function getRouterParams(): array
    {
        return [
            'slug' => $this->getSlug()
        ];
    }

    public function getRouter(): string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $url = $linkGenerator;
        } catch (\Exception $e) {

        } finally {
            return $url;
        }
    }

    public function getIconThumbnail(string $thumbnailname = 'default', array $options = [], array $removeAttrs = [])
    {
        $default = 'https://via.placeholder.com/130';

        $checkIcon = $this->hasProperty('IMAGE_ICON')
            ? $this->getProperty('IMAGE_ICON') : null;

        if (!$checkIcon) {
            return '<img src="'. $default .'" alt="icon stage default">';
        }

        /** @var Image $icon */
        $icon = $checkIcon;

        return $icon->getThumbnail($thumbnailname)->getHtml($options, $removeAttrs);
    }

    public function getGeneratedLink()
    {
        /** @var Link $generatedLink */
        $generatedLink = parent::getGeneratedLink();
        $newGeneratedLink = $this->setValueFromObject($generatedLink);
        $newGeneratedLink->setDirect($this->getRouter());

        $attributes = WebsiteSetting::getByName('ARTICLE_CATEGORY_ATTRIBUTE_LINK') ?
            WebsiteSetting::getByName('ARTICLE_CATEGORY_ATTRIBUTE_LINK')->getData() : null;

        $newGeneratedLink->setAttributes($attributes);

        return $newGeneratedLink;
    }
}
