<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/02/2020
 * Time: 19:02
 */

namespace AppBundle\Model\DataObject\Stage;

use AppBundle\Model\RouterInterfaceListing;
use AppBundle\Model\RouterInterface;
use Pimcore\Model\DataObject\Stage\Listing as BaseStageListing;

class Listing extends BaseStageListing implements RouterInterfaceListing
{
    public function getObjectForStaticRoute($routeVariables = []): array
    {
        // TODO: Implement getObjectForStaticRoute() method.
    }
}
