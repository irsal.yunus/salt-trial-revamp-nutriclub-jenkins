<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 13/02/2020
 * Time: 17:33
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Model\RouterInterface;
use Pimcore\Model\DataObject\ArticleSubCategory as BaseArticleSubCategory;

class ArticleSubCategory extends BaseArticleSubCategory implements RouterInterface
{
    public function getRouterParams(): array
    {
        // TODO: Implement getRouterParams() method.
    }

    public function getRouter() : string
    {
        return '';
    }
}
