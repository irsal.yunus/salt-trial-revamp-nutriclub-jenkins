<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/06/2020
 * Time: 16:15
 */

namespace AppBundle\Model\DataObject\Event;

use AppBundle\Model\RouterInterfaceListing;
use Pimcore\Model\DataObject\Event\Listing as BaseEventListing;

class Listing extends BaseEventListing implements RouterInterfaceListing
{
    public function getObjectForStaticRoute($routeVariables = []): array
    {
        $data = [];
        if (!$routeVariables) {
            return $data;
        }

        $this->addConditionParam('slug = ?', $routeVariables['slug'], 'AND');

        return $data[] = $this->load();
    }
}
