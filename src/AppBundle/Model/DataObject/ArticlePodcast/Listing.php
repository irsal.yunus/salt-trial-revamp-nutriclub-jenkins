<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 09:28
 */

namespace AppBundle\Model\DataObject\ArticlePodcast;

use AppBundle\Model\RouterInterfaceListing;
use Pimcore\Model\DataObject\ArticlePodcast\Listing as BaseArticlePodcastListing;

class Listing extends BaseArticlePodcastListing implements RouterInterfaceListing
{
    public function getObjectForStaticRoute($routeVariables = []): array
    {
        $data = [];
        if (!$routeVariables) {
            return $data;
        }

        $this->addConditionParam('slug = ?', $routeVariables['articleSlug'], 'AND');

        return $data[] = $this->load();
    }

    public function getOtherPodcasts(array $ids = [], int $limit = 4, int $offet = 0)
    {
        $others = [];
        if (!$ids) {
            return $others;
        }

        $this->addConditionParam('oo_id NOT IN('. implode(',', $ids) .')', null, 'AND');
        $this->setLimit($limit);
        $this->setOffset($offet);

        $this->setOrder('DESC');
        $this->setOrderKey('oo_id');

        return $others[] = $this->load();
    }
}
