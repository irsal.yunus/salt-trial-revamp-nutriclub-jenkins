<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/06/2020
 * Time: 16:09
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Model\RouterInterface;
use AppBundle\Model\SchemaOrgInterface;
use AppBundle\Model\SocialMediaInterface;
use Carbon\Carbon;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\Event as BaseEvent;
use Pimcore\Tool;
use Spatie\SchemaOrg\Schema;

class Event extends BaseEvent implements RouterInterface, SocialMediaInterface, SchemaOrgInterface
{
    public function getImageWithAbsoluteUrl()
    {
        $imgDesktop = $this->getImageDesktop() ?: '';
        $protocol = getenv('HTTP_PROTOCOL', 'https');
        return Tool::getHostUrl($protocol) . $imgDesktop;
    }


    public function getRouterParams(): array
    {
        return [
            'slug' => $this->getSlug()
        ];
    }

    public function getRouter(): string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $protocol = getenv('HTTP_PROTOCOL', 'https');
            $url = Tool::getHostUrl($protocol) . $linkGenerator;
        } catch (\Exception $e) {

        } finally {
            return $url;
        }
    }

    public function getSchemaOrg()
    {
        $startDate = $this->getStartDate();
        $endDate = Carbon::createFromTimestamp($startDate->timestamp);

        $endTime = $this->getEndTime();
        $hours = (int) substr($endTime, 0, 2);
        $minutes = (int) substr($endTime, 3, 4);

        $duration = ($hours - (int) $startDate->format('H')) * 60 + $minutes;

        return Schema::event()
            ->name($this->getTitle())
            ->performer([
                'name' => $this->getSpeaker() ? $this->getSpeaker()->getName() : 'Nutriclub'
            ])
            ->image($this->getImageWithAbsoluteUrl())
            ->location([
                'name' => $this->getTag(),
                'address' => $this->getTag()
            ])
            ->startDate($startDate)
            ->endDate($endDate->addMinutes($duration))
            ->description(strip_tags($this->getDescription()))
            ;
    }
}
