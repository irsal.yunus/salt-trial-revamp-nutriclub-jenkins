<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 11:38
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Helper\GeneralHelper;
use AppBundle\Model\Data\Link;
use AppBundle\Model\RouterInterface;
use AppBundle\Model\SchemaOrgInterface;
use AppBundle\Model\SocialMediaInterface;
use AppBundle\Traits\PimcoreModelLinkOverride;
use Pimcore\Model\DataObject\Article as BaseArticle;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\Fieldcollection;
use Pimcore\Model\User;
use Pimcore\Model\WebsiteSetting;
use Pimcore\Tool;
use Spatie\SchemaOrg\Schema;

class Article extends BaseArticle implements RouterInterface, SocialMediaInterface, SchemaOrgInterface
{
    use PimcoreModelLinkOverride;

    public function getTitleTrim($trim = true, $length = 40, $ellipses = true)
    {
        $title = parent::getTitle();

        if ($trim) {
            return GeneralHelper::trimText($title, $length, $ellipses);
        }

        return $title;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        $categories = $this->getCategory();
        $tags = [];

        if (empty($categories)) {
            return $tags;
        }

        foreach ($categories as $category) {
            $tags[$category->getId()] = $category->getSlug();
        }

        return $tags;
    }

    public function getUserOwnerName()
    {
        $user = User::getById($this->getUserOwner());
        return ucwords($user->getName());
    }

    public function getImgDekstopHtml($thumbnailConfigName = 'default', $options = [])
    {
        return $this->getImgDesktop() ?
            $this->getImgDesktop()->getThumbnail($thumbnailConfigName)->getHtml($options) :
            GeneralHelper::getImgPlaceholder();
    }

    public function getImgMobileHtml($thumbnailConfigName = 'default', $options = [])
    {
        return $this->getImgDesktop() ?
            $this->getImgDesktop()->getThumbnail($thumbnailConfigName)->getHtml($options) :
            GeneralHelper::getImgPlaceholder();
    }

    public function getChildContent()
    {
        $data = [];
        if ($this->getChild() != null && ($this->getChild()->getCount() <= 0)) {
            return $data;
        }

        return $this->getChild() ? $this->getChild()->getItems() : null;
    }

    /**
     * @return array
     */
    public function getRouterParams() : array
    {
        return [
            'stageSlug' => $this->getStage()->getSlug(),
            'articleCategorySlug' => $this->getCategory()->getSlug(),
            'articleSubCategorySlug' => $this->getSubCategory() ? $this->getSubCategory()[0]->getSlug() : null,
            'articleSlug' => $this->getSlug()
        ];
    }

    public function getImageWithAbsoluteUrl()
    {
        $imgDesktop = $this->getImgDesktop() ?: '';
        $protocol = getenv('HTTP_PROTOCOL', 'https');
        return Tool::getHostUrl($protocol) . $imgDesktop;
    }

    public function getRouter() : string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $protocol = getenv('HTTP_PROTOCOL', 'https');
            $url = Tool::getHostUrl($protocol) . $linkGenerator;
        } catch (\Exception $e) {

        } finally {
            return $url;
        }
    }

    public function getGeneratedLink()
    {
        /** @var Link $generatedLink */
        $generatedLink = parent::getGeneratedLink();
        $newGeneratedLink = $this->setValueFromObject($generatedLink);
        $newGeneratedLink->setDirect($this->getRouter());

        $attributes = WebsiteSetting::getByName('ARTICLE_ATTRIBUTE_LINK') ?
            WebsiteSetting::getByName('ARTICLE_ATTRIBUTE_LINK')->getData() : null;

        $newGeneratedLink->setAttributes($attributes);

        return $newGeneratedLink;
    }

    public function getSchemaOrg()
    {
        return Schema::article()
            ->articleBody($this->getSeoDescription())
            ->articleSection($this->getCategory()->getName())
            ->name($this->getTitle());
    }
}
