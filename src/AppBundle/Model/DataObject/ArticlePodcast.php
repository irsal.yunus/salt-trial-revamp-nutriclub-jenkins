<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 13/01/2020
 * Time: 14:24
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Model\Data\Link;
use AppBundle\Model\RouterInterface;
use AppBundle\Model\SocialMediaInterface;
use AppBundle\Traits\PimcoreModelLinkOverride;
use Pimcore\Model\DataObject\ArticlePodcast as BaseArticlePodcast;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\WebsiteSetting;
use Pimcore\Tool;

class ArticlePodcast extends BaseArticlePodcast implements RouterInterface, SocialMediaInterface
{
    use PimcoreModelLinkOverride;

    /**
     * @return array
     */
    public function getRouterParams() : array
    {
        return [
            'stageSlug' => $this->getStage()->getSlug(),
            'articleCategorySlug' => $this->getCategory()->getSlug(),
            'articleSubCategorySlug' => $this->getSubCategory() ? $this->getSubCategory()[0]->getSlug() : null,
            'articleSlug' => $this->getSlug()
        ];
    }

    public function getRouter() : string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $protocol = getenv('HTTP_PROTOCOL', 'https');
            $url = Tool::getHostUrl($protocol) . $linkGenerator;
        } catch (\Exception $e) {

        } finally {
            return $url;
        }
    }

    public function getGeneratedLink()
    {
        /** @var Link $generatedLink */
        $generatedLink = parent::getGeneratedLink();
        $newGeneratedLink = $this->setValueFromObject($generatedLink);
        $newGeneratedLink->setDirect($this->getRouter());

        $attributes = WebsiteSetting::getByName('ARTICLE_PODCAST_ATTRIBUTE_LINK') ?
            WebsiteSetting::getByName('ARTICLE_PODCAST_ATTRIBUTE_LINK')->getData() : null;

        $newGeneratedLink->setAttributes($attributes);

        return $newGeneratedLink;
    }
    
    public function getImageWithAbsoluteUrl()
    {
        $imgDesktop = $this->getImageDesktop() ?: '';
        $protocol = getenv('HTTP_PROTOCOL', 'https');
        return Tool::getHostUrl($protocol) . $imgDesktop;
    }
}
