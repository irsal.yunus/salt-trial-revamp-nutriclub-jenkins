<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/07/2020
 * Time: 10:53
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Model\RouterInterface;
use AppBundle\Model\SocialMediaInterface;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\ProductGroup as BaseProductGroup;
use Pimcore\Tool;

class ProductGroup extends BaseProductGroup implements RouterInterface, SocialMediaInterface
{
    public function getRouterParams(): array
    {
        return [
            'groupSlug' => $this->getSlug(),
        ];
    }

    public function getRouter(): string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $protocol = getenv('HTTP_PROTOCOL', 'https');
            $url = Tool::getHostUrl($protocol) . $linkGenerator;
        } catch (\Exception $e) {

        } finally {
            return $url;
        }
    }
}
