<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 15/03/2020
 * Time: 00:08
 */

namespace AppBundle\Model\DataObject\Tool;

use AppBundle\Model\RouterInterfaceListing;
use Pimcore\Model\DataObject\Tool\Listing as BaseToolListing;

class Listing extends BaseToolListing implements RouterInterfaceListing
{
    public function getObjectForStaticRoute($routeVariables = []): array
    {
        // TODO: Implement getObjectForStaticRoute() method.
    }
}
