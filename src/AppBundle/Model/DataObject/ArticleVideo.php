<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 13/01/2020
 * Time: 14:24
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Model\Data\Link;
use AppBundle\Model\RouterInterface;
use AppBundle\Model\SchemaOrgInterface;
use AppBundle\Model\SocialMediaInterface;
use AppBundle\Model\Video\YouTube;
use AppBundle\Traits\PimcoreModelLinkOverride;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\ArticleVideo as BaseArticleVideo;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\Data\ExternalImage;
use Pimcore\Tool;
use Spatie\SchemaOrg\Schema;

class ArticleVideo extends BaseArticleVideo implements RouterInterface, SocialMediaInterface, SchemaOrgInterface
{
    use PimcoreModelLinkOverride;

    public function getRouterParams() : array
    {
        return [
            'stageSlug' => $this->getStage()->getSlug(),
            'articleCategorySlug' => $this->getCategory()->getSlug(),
            'articleSubCategorySlug' => $this->getSubCategory() ? $this->getSubCategory()[0]->getSlug() : null,
            'articleSlug' => $this->getSlug()
        ];
    }

    public function getRouter() : string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $protocol = getenv('HTTP_PROTOCOL', 'https');
            $url = Tool::getHostUrl($protocol) . $linkGenerator;
        } catch (\Exception $e) {

        } finally {
            return $url;
        }
    }

    public function getYouTube()
    {
        if (!$this->getVideo()) {
            return null;
        }

        if ($this->getVideo()->getType() !== 'youtube') {
            return null;
        }

        if ($this->getVideo()->getData() === $this->getVideoId()) {
            $newYouTubeObjectBasedOnExistingData = new YouTube();
            $newYouTubeObjectBasedOnExistingData->setId($this->getVideoId());
            $newYouTubeObjectBasedOnExistingData->setDuration($this->getVideoDuration());
            $newYouTubeObjectBasedOnExistingData->setPublishedAt($this->getVideoPublishedAt());
            $newYouTubeObjectBasedOnExistingData->setCreatorId($this->getVideoCreatorId());

            $thumbnails = [
                'maxres' => [
                    'url' => $this->getImageMaxRes()->getUrl()
                ],
                'standard' => [
                    'url' => $this->getImageStandard()->getUrl()
                ],
                'high' => [
                    'url' => $this->getImageHigh()->getUrl()
                ],
                'medium' => [
                    'url' => $this->getImageMedium()->getUrl()
                ],
                'default' => [
                    'url' => $this->getImageDefault()->getUrl()
                ],
            ];

            $newYouTubeObjectBasedOnExistingData->setThumbnails($thumbnails);

            return $newYouTubeObjectBasedOnExistingData;
        }

        $fetchFromYouTube = YouTube::getObject($this->getVideo()->getData());

        try {
            $this->setVideoId($fetchFromYouTube->getId());
            $this->setVideoDuration($fetchFromYouTube->getDuration());
            $this->setVideoPublishedAt($fetchFromYouTube->getPublishedAt());
            $this->setVideoCreatorId($fetchFromYouTube->getCreatorId());

            $this->setImageMaxRes(
                $this->externalImage($fetchFromYouTube->getThumbnails()['maxres']['url'] ?? '')
            );

            $this->setImageStandard(
                $this->externalImage($fetchFromYouTube->getThumbnails()['standard']['url'] ?? '')
            );

            $this->setImageHigh(
                $this->externalImage($fetchFromYouTube->getThumbnails()['high']['url'] ?? '')
            );

            $this->setImageMedium(
                $this->externalImage($fetchFromYouTube->getThumbnails()['medium']['url'] ?? '')
            );

            $this->setImageDefault(
                $this->externalImage($fetchFromYouTube->getThumbnails()['default']['url'] ?? '')
            );

            $this->save(
                [
                    'versionNote' => 'Automatically assign by ArticleVideoListener.'
                ]
            );
        } catch (\Exception $exception) {
            Simple::log('ARTICLE_VIDEO_OBJECT', $exception->getMessage());
        }

        return $fetchFromYouTube;
    }

    protected function externalImage($url = '')
    {
        $externalImageAsset = new ExternalImage();
        $externalImageAsset->setUrl($url);

        return $externalImageAsset;
    }

    public function getLink()
    {
        /** @var Link $generatedLink */
        $generatedLink = parent::getLink();
        $newGeneratedLink = $this->setValueFromObject($generatedLink);
        //$newGeneratedLink->setDirect($this->getRouter());

        return $newGeneratedLink;
    }

    public function getSchemaOrg()
    {
        return Schema::videoObject()
            ->name($this->getTitle())
            ->description(strip_tags($this->getContent()))
            ->uploadDate($this->getYouTube()->getPublishedAt())
            ->thumbnailUrl(array_column($this->getYouTube()->getThumbnails() ?? [], 'url'));
            ;
    }
}
