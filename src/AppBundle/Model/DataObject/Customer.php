<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/01/2020
 * Time: 14:20
 */

namespace AppBundle\Model\DataObject;

use Pimcore\Model\DataObject\Customer as BaseCustomer;
use AppBundle\Model\CustomerManagementFramework\PasswordRecoveryInterface;
use CustomerManagementFrameworkBundle\Model\SsoAwareCustomerInterface;

class Customer extends BaseCustomer implements PasswordRecoveryInterface, SsoAwareCustomerInterface
{
    public function getProfilingConsent()
    {
    }

    public function loadUserByUsername($username)
    {
    }

    public function refreshUser(UserInterface $user)
    {
        // TODO: Implement refreshUser() method.
    }

    public function supportsClass($class)
    {
        // TODO: Implement supportsClass() method.
    }

    public function getFullname()
    {
        return $this->getFirstname() !== $this->getLastname() ?
            $this->getFirstname() . ' ' . $this->getLastname() : $this->getFirstname();
    }

    public function getProfilePicture()
    {
        return ($this->getGender() === 'M') ?
            '/assets/images/register__step1/content/dad.png' : '/assets/images/register__step1/content/mom.png';
    }

    public function getProfilePictureSvg()
    {
        return ($this->getGender() === 'M') ?
            '/assets/images/papa-thank-you.svg' : '/assets/images/mama-thank-you.svg';
    }

    public function getGenderFriendlyName()
    {
        return ($this->getGender() === 'M') ? 'papa' : 'mama';
    }

    /**
     * @todo save to sso/update/anything.
     */
    public function saveToSso()
    {

    }
}
