<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 19/02/2020
 * Time: 12:10
 */

namespace AppBundle\Model\DataObject\ArticleCalendar;

use AppBundle\Model\RouterInterfaceListing;
use Facebook\WebDriver\Exception\NullPointerException;
use Pimcore\Model\DataObject\ArticleCalendar\Listing as BaseArticleCalendarListing;
use Pimcore\Model\DataObject\Data\QuantityValue;
use Pimcore\Model\DataObject\QuantityValue\Unit;
use Pimcore\Model\DataObject\QuantityValue\UnitConversionService;

class Listing extends BaseArticleCalendarListing implements RouterInterfaceListing
{
    /** @var UnitConversionService $unitConversionService */
    private $unitConversionService;

    public function __construct(UnitConversionService $unitConversionService)
    {
        parent::__construct();

        $this->unitConversionService = $unitConversionService;
    }

    public function getObjectListingByStageAge(int $weekAge)
    {
        try {
            $originalValue = new QuantityValue($weekAge, Unit::getByAbbreviation('week')->getId());

            $unitListing = new Unit\Listing();

            if (!$unitListing) {
                return;
            }

            /** @var Unit $unit */
            foreach ($unitListing->load() as $unit) {
                $convertedValue = $this->unitConversionService
                    ->convert($originalValue, $unit);

            }
        } catch (\Exception $exception) {
            return;
        }
    }

    public function getObjectForStaticRoute($routeVariables = []): array
    {
        // TODO: Implement getObjectForStaticRoute() method.
    }
}
