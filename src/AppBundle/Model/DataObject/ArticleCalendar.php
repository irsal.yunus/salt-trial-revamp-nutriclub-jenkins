<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 19/02/2020
 * Time: 12:08
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Model\RouterInterface;
use Pimcore\Model\DataObject\ArticleCalendar as BaseArticleCalendar;

class ArticleCalendar extends BaseArticleCalendar implements RouterInterface
{
    public function getRouterParams(): array
    {
        // TODO: Implement getRouterParams() method.
    }

    public function getRouter(): string
    {
        // TODO: Implement getRouter() method.
    }
}
