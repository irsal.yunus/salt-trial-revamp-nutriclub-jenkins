<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/02/2020
 * Time: 19:21
 */

namespace AppBundle\Model\DataObject\ArticleCategory;

use Pimcore\Model\DataObject\ArticleCategory\Listing as BaseArticleCategoryListing;

class Listing extends BaseArticleCategoryListing
{

}
