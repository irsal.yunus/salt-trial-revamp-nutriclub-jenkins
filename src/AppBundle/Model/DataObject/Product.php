<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/07/2020
 * Time: 11:26
 */

namespace AppBundle\Model\DataObject;

use AppBundle\Model\RouterInterface;
use AppBundle\Model\SchemaOrgInterface;
use AppBundle\Model\SocialMediaInterface;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\Product as BaseProduct;
use Pimcore\Tool;
use Spatie\SchemaOrg\Schema;

class Product extends BaseProduct implements RouterInterface, SocialMediaInterface, SchemaOrgInterface
{
    public function getRouterParams(): array
    {
        return [
            'groupSlug' => $this->getGroup()->getSlug(),
            'productSlug' => $this->getSlug()
        ];
    }

    public function getRouter(): string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $protocol = getenv('HTTP_PROTOCOL', 'https');
            $url = Tool::getHostUrl($protocol) . $linkGenerator;
        } catch (\Exception $e) {

        } finally {
            return $url;
        }
    }

    public function getTitle()
    {
        return $this->getName();
    }

    public function getSchemaOrg()
    {
        $protocol = getenv('HTTP_PROTOCOL', 'https');
        $url = Tool::getHostUrl($protocol);

        return Schema::product()
            ->name($this->getName())
            ->image($url . $this->getImgIcon())
            ->brand([
                'name' => $this->getGroup()->getName()
            ])
            ->description(strip_tags($this->getProductShortDescription()))
            ->review([
                'author' => 'Danone'
            ])
            ->offers([])
            ->aggregateRating([])
        ;
    }

    public function getImgFreeSampleAbsoluteUrl()
    {
        $imgDesktop = $this->getImgFreeSample() ?: '';
        $protocol = getenv('HTTP_PROTOCOL', 'https');

        return Tool::getHostUrl($protocol) . $imgDesktop;
    }

    public function getFreeSampleHiDescriptionAfterLogin($user = null)
    {
        $parent = parent::getFreeSampleHiDescriptionAfterLogin();

        return $this->userStringReplacer($user, $parent);
    }

    private function userStringReplacer($user, $string)
    {
        if (!$user || !$user instanceof Customer) {
            return $string;
        }

        return str_replace(
            ['%genderFriendlyName', '%name', '%productName'],
            [ucfirst($user->getGenderFriendlyName()), $user->getFullname(), $this->getName()],
            $string
        );
    }
}
