<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 03/04/2020
 * Time: 15:05
 */

namespace AppBundle\Model\Document\Areabrick\CardSliderOne\Data;

use AppBundle\Model\Data\Link;

class Stage
{
    /** @var int $id */
    private $id;

    /** @var string $name */
    private $name;

    /** @var string $icon */
    private $icon;

    /** @var Link $link */
    private $link;

    /** @var string $router */
    private $router;

    /** @var string $slug */
    private $slug;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return Link
     */
    public function getLink(): Link
    {
        return $this->link;
    }

    /**
     * @param Link $link
     */
    public function setLink(Link $link): void
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getRouter(): string
    {
        return $this->router;
    }

    /**
     * @param string $router
     */
    public function setRouter(string $router): void
    {
        $this->router = $router;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }
}
