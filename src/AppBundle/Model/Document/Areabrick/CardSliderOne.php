<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 03/04/2020
 * Time: 13:10
 */

namespace AppBundle\Model\Document\Areabrick;

use AppBundle\Model\Document\AbstractModelDocumentAreabrick;
use Pimcore\Model\Document\Tag\ {Input};

class CardSliderOne extends AbstractModelDocumentAreabrick
{
    /** @var Input $title */
    private $title;

    /** @var array $stages */
    private $stages;

    /**
     * @return Input
     */
    public function getTitle(): Input
    {
        return $this->title;
    }

    /**
     * @param Input $title
     */
    public function setTitle(Input $title): void
    {
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function getStages(): array
    {
        return $this->stages;
    }

    /**
     * @param array $stages
     */
    public function setStages(array $stages): void
    {
        $this->stages = $stages;
    }
}
