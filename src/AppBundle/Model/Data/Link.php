<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 31/03/2020
 * Time: 15:50
 */

namespace AppBundle\Model\Data;

use Pimcore\Model\DataObject\Data\Link as PimcoreDataLink;

class Link extends PimcoreDataLink
{
    /**
     * @inheritDoc
     */
    public function getHtml($textToHtmlSpecialChars = true)
    {
        $attributes = ['rel', 'tabindex', 'accesskey', 'title', 'target', 'class'];
        $attribs = [];
        foreach ($attributes as $a) {
            if ($this->$a) {
                $attribs[] = $a . '="' . $this->$a . '"';
            }
        }

        if ($this->getAttributes()) {
            $attribs[] = $this->getAttributes();
        }

        if (empty($this->text)) {
            return '';
        }

        if ($textToHtmlSpecialChars) {
            return '<a href="' . $this->getHref() . '" ' . implode(' ', $attribs) . '>' . htmlspecialchars($this->getText()) . '</a>';
        }
        if (!$textToHtmlSpecialChars) {
            return '<a href="' . $this->getHref() . '" ' . implode(' ', $attribs) . '>' . htmlspecialchars_decode($this->getText()) . '</a>';
        }
    }
}
