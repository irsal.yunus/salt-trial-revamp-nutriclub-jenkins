<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 13:07
 */

namespace AppBundle\Model\ElasticSearch;

class Article
{
    /** @var int $totalData */
    public $totalData;

    /** @var array $article */
    public $article;

    /** @var int $articleTotalPage */
    public $articleTotalPage;

    public $articleTotalData;

    /** @var array $video */
    public $video;

    /** @var int $videoTotalPage */
    public $videoTotalPage;

    public $videoTotalData;

    /** @var array $podcast */
    public $podcast;

    /** @var int $podcastTotalPage */
    public $podcastTotalPage;

    public $podcastTotalData;

    /** @var array $ebook */
    public $ebook;

    /** @var int $ebookTotalPage */
    public $ebookTotalPage;

    public $ebookTotalData;

    /**
     * @return int
     */
    public function getTotalData()
    {
        return $this->totalData;
    }

    /**
     * @param int $totalData
     */
    public function setTotalData(int $totalData): void
    {
        $this->totalData = $totalData;
    }

    /**
     * @return array
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param $article
     */
    public function setArticle($article): void
    {
        $this->article = $article;
    }

    /**
     * @return int
     */
    public function getArticleTotalPage()
    {
        return $this->articleTotalPage;
    }

    /**
     * @param int $articleTotalPage
     */
    public function setArticleTotalPage($articleTotalPage): void
    {
        $this->articleTotalPage = $articleTotalPage;
    }

    /**
     * @return mixed
     */
    public function getArticleTotalData()
    {
        return $this->articleTotalData;
    }

    /**
     * @param mixed $articleTotalData
     */
    public function setArticleTotalData($articleTotalData): void
    {
        $this->articleTotalData = $articleTotalData;
    }

    /**
     * @return array
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * @param $video
     */
    public function setVideo($video): void
    {
        $this->video = $video;
    }

    /**
     * @return int
     */
    public function getVideoTotalPage()
    {
        return $this->videoTotalPage;
    }

    /**
     * @param int $videoTotalPage
     */
    public function setVideoTotalPage($videoTotalPage): void
    {
        $this->videoTotalPage = $videoTotalPage;
    }

    /**
     * @return mixed
     */
    public function getVideoTotalData()
    {
        return $this->videoTotalData;
    }

    /**
     * @param mixed $videoTotalData
     */
    public function setVideoTotalData($videoTotalData): void
    {
        $this->videoTotalData = $videoTotalData;
    }

    /**
     * @return array
     */
    public function getPodcast()
    {
        return $this->podcast;
    }

    /**
     * @param $podcast
     */
    public function setPodcast($podcast): void
    {
        $this->podcast = $podcast;
    }

    /**
     * @return int
     */
    public function getPodcastTotalPage()
    {
        return $this->podcastTotalPage;
    }

    /**
     * @param int $podcastTotalPage
     */
    public function setPodcastTotalPage($podcastTotalPage): void
    {
        $this->podcastTotalPage = $podcastTotalPage;
    }

    /**
     * @return mixed
     */
    public function getPodcastTotalData()
    {
        return $this->podcastTotalData;
    }

    /**
     * @param mixed $podcastTotalData
     */
    public function setPodcastTotalData($podcastTotalData): void
    {
        $this->podcastTotalData = $podcastTotalData;
    }

    /**
     * @return array
     */
    public function getEbook()
    {
        return $this->ebook;
    }

    /**
     * @param $ebook
     */
    public function setEbook($ebook): void
    {
        $this->ebook = $ebook;
    }

    /**
     * @return int
     */
    public function getEbookTotalPage()
    {
        return $this->ebookTotalPage;
    }

    /**
     * @param int $ebookTotalPage
     */
    public function setEbookTotalPage($ebookTotalPage): void
    {
        $this->ebookTotalPage = $ebookTotalPage;
    }

    /**
     * @return mixed
     */
    public function getEbookTotalData()
    {
        return $this->ebookTotalData;
    }

    /**
     * @param mixed $ebookTotalData
     */
    public function setEbookTotalData($ebookTotalData): void
    {
        $this->ebookTotalData = $ebookTotalData;
    }
}
