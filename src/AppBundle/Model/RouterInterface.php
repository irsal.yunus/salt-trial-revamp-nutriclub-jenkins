<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/01/2020
 * Time: 11:27
 */

namespace AppBundle\Model;

interface RouterInterface
{
    /**
     * @return array
     */
    public function getRouterParams() : array;

    public function getRouter() : string;
}
