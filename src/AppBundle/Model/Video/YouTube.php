<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 19:35
 */

namespace AppBundle\Model\Video;

use AppBundle\Api\v1\YouTube\YouTube as YouTubeApi;
use Carbon\Carbon;

class YouTube
{
    /** @var string $id */
    public $id;

    /** @var string $title */
    public $title;

    /** @var string $duration */
    public $duration;

    /** @var Carbon $publishedAt */
    public $publishedAt;

    /** @var string $creatorId */
    public $creatorId;

    /** @var array $thumbnails */
    public $thumbnails;

    public static function getObject($id = null)
    {
        if (!$id) {
            return null;
        }

        $object = new self();
        $object->setId($id);

        $youtubeApi = new YouTubeApi();
        $videoDetail = $youtubeApi->getVideoDetail($object->getId());

        if (!$videoDetail) {
            return null;
        }

        $videoTitle = $videoDetail['snippet']['title'] ?? '';
        $videoThumbnails = $videoDetail['snippet']['thumbnails'] ?? null;
        $videoDuration = $videoDetail['contentDetails']['duration'] ?? null;
        $videoPublishedAt = Carbon::parse($videoDetail['snippet']['publishedAt'] ?? null);
        $videoChannelId = $videoDetail['snippet']['channelId'] ?? '';

        $converter = new \DateTime('@0'); // Unix epoch
        try {
            $converter->add(new \DateInterval($videoDuration));
            $convertedVideoDuration = $converter->format('i:s');
        } catch (\Exception $e) {
            $convertedVideoDuration = '00:00';
        }

        $object->setTitle($videoTitle);
        $object->setThumbnails($videoThumbnails);
        $object->setDuration($convertedVideoDuration);
        $object->setPublishedAt($videoPublishedAt);
        $object->setCreatorId($videoChannelId);

        return $object;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDuration(): string
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration(string $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return Carbon
     */
    public function getPublishedAt(): Carbon
    {
        return $this->publishedAt;
    }

    /**
     * @param Carbon $publishedAt
     */
    public function setPublishedAt(Carbon $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }

    /**
     * @return string
     */
    public function getCreatorId(): string
    {
        return $this->creatorId;
    }

    /**
     * @param string $creatorId
     */
    public function setCreatorId(string $creatorId): void
    {
        $this->creatorId = $creatorId;
    }

    /**
     * @return array
     */
    public function getThumbnails(): array
    {
        return $this->thumbnails;
    }

    /**
     * @param array $thumbnails
     */
    public function setThumbnails(array $thumbnails): void
    {
        $this->thumbnails = $thumbnails;
    }
}
