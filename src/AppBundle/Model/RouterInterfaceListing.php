<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 05/02/2020
 * Time: 16:47
 */

namespace AppBundle\Model;

interface RouterInterfaceListing
{
    public function getObjectForStaticRoute($routeVariables = []) : array;
}
