<?php

namespace AppBundle\Controller;

use AppBundle\Services\EventJoinService;
use AppBundle\Traits\Authenticate;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends AbstractController
{
    use Authenticate;

    public function defaultAction(Request $request)
    {
    }

    /**
     * @param Request $request
     * @param EventJoinService $eventJoinService
     * @return RedirectResponse
     * @Security("has_role('ROLE_USER')")
     */
    public function loginAction(
        Request $request,
        EventJoinService $eventJoinService
    )
    {
        if ($redirectTo = $eventJoinService->getRedirectTo()) {
            $eventJoinService->clearSession();
            return $this->redirect($redirectTo);
        }

        if ($request->cookies->has('refererFreeSample')) {
            return $this->redirect($request->cookies->get('refererFreeSample'));
        }

        if ($request->cookies->has('refererMyPregnancyToday')) {
            return $this->redirect($request->cookies->get('refererMyPregnancyToday'));
        }

        if ($request->cookies->has('refererRPC')) {
            return $this->redirect($request->cookies->get('refererRPC'));
        }
    }
}
