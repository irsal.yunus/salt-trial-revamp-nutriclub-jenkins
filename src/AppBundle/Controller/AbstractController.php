<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 09/01/2020
 * Time: 10:54
 */

namespace AppBundle\Controller;

use AppBundle\Api\v1\Rewards\Account;
use AppBundle\Form\SearchFormType;
use AppBundle\Model\DataObject\Customer;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

abstract class AbstractController extends FrontendController
{
    /** @var FormInterface $formSearch */
    public $formSearch;

    public function onKernelController(FilterControllerEvent $event)
    {
        parent::onKernelController($event);

        $this->view->websiteTitlePostFix = WebsiteSetting::getByName('WEBSITE_TITLE_POSTFIX') ?
            WebsiteSetting::getByName('WEBSITE_TITLE_POSTFIX')->getData() : '';

        $this->view->websiteTitleSeparator = WebsiteSetting::getByName('WEBSITE_TITLE_SEPARATOR') ?
            WebsiteSetting::getByName('WEBSITE_TITLE_SEPARATOR')->getData() : ' - ';

        $this->createFormSearch();

        $this->synchronizeAutoLogin($event->getRequest());
    }

    private function createFormSearch()
    {
        $formData['q'] = null;

        $this->formSearch = $this->createForm(SearchFormType::class, $formData, [
            'action' => '/search'
        ]);

        $this->view->formSearch = $this->formSearch->createView();
    }

    private function synchronizeAutoLogin(Request $request)
    {
        $user = $this->getUser();

        if (!$user) {
            return;
        }

        $cookieName = getenv('COOKIE_NAME_DOT_NET_AUTOLOGIN', null);

        if (!$cookieName) {
            return;
        }

        $cookies = $request->cookies;

        $hasCookieAutoLogin = $cookies->has($cookieName);

        if (!$hasCookieAutoLogin) {
            return;
        }

        $accessToken = $cookies->get($cookieName);

        $account = new Account();
        $profile = $account->profile($accessToken);

        if ($profile['StatusCode'] !== '00') {
            return null;
        }

        $customer = new Customer();
        $customer->setValues($profile['Value']);

        $sessions = $request->getSession();

        $sessions->set('AccessToken', $accessToken);
        $sessions->set('UserProfile', $customer);
    }
}
