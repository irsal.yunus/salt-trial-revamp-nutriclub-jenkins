<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/06/2020
 * Time: 11:20
 */

namespace AppBundle\Controller;

use AppBundle\Helper\CardEventListHelper;
use AppBundle\Helper\CustomSmsHelper;
use AppBundle\Helper\StaticRouteHelper;
use AppBundle\Model\DataObject\Customer;
use AppBundle\Model\DataObject\Event;
use AppBundle\Services\EventJoinService;
use AppBundle\Services\EventUtmLogService;
use AppBundle\Services\LoginRefererService;
use AppBundle\Services\SchemaOrgService;
use AppBundle\Services\SocialMediaService;
use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Pimcore\Model\Document\Snippet;
use Pimcore\Model\WebsiteSetting;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Zend\Paginator\Paginator;

/**
 * Class EventController
 * @package AppBundle\Controller
 *
 * @Route("/event")
 */
class EventController extends AbstractController
{
    public function defaultAction(Request $request)
    {

    }

    /**
     * @param Request $request
     * @param SocialMediaService $socialMediaService
     * @param BreadcrumbHelperService $breadcrumbHelperService
     * @param EventJoinService $eventJoinService
     * @param EventUtmLogService $eventUtmLogService
     * @param SchemaOrgService $schemaOrgService
     * @return RedirectResponse
     * @throws \Exception
     */
    public function detailAction(
        Request $request,
        SocialMediaService $socialMediaService,
        BreadcrumbHelperService $breadcrumbHelperService,
        EventJoinService $eventJoinService,
        EventUtmLogService $eventUtmLogService,
        SchemaOrgService $schemaOrgService
    ) {
        $routeVariables = StaticRouteHelper::removeUnusedRouteParams($request->get('_route_params'));

        $events = new Event\Listing();
        $getEventObject = $events->getObjectForStaticRoute($routeVariables);
        if (!$getEventObject && !$this->editmode && !$request->get('contentDocument') instanceof Snippet) {
            throw new NotFoundHttpException('Event tidak ditemukan');
        }
        if ($this->editmode) {
            $events = new Event\Listing();
            $events->setLimit(1);
            $events->load();

            /** @var Event $getEventObject */
            $getEventObject = $events->getObjects();
        }

        /** @var Event $eventObj */
        $eventObj = $getEventObject[0] ?? null;

        if ($request->getMethod() === 'POST') {
            $userRewards = $request->getSession()->get('UserProfile');

            if ($userRewards instanceof Customer) {
                $eventJoinService->setEventObj($eventObj);
                $eventJoinService->checkAndCreateParticipant($userRewards);
                $eventUtmLogService->createUtmLog($eventObj);

                $redirectTo = $eventJoinService->getRedirectTo();
                $eventJoinService->clearSession();

                return $this->redirect($redirectTo);
            }
        }

        $events->resetConditionParams();

        if ($request->get('_route') === 'EVENT') {
            $eventPageDetail = WebsiteSetting::getByName('EVENT_PAGE_DETAIL') ?
                WebsiteSetting::getByName('EVENT_PAGE_DETAIL')->getData() : null;

            $schemaOrgService->getSchemaOrg($eventObj);

            $breadcrumbHelperService->enrichEventPage($eventObj, $eventPageDetail);
        }

        $fb = $socialMediaService->getFacebookShareAble($eventObj);
        $twitter = $socialMediaService->getTwitterShareAble($eventObj);

        $this->view->facebookShareAbleLink = $fb;
        $this->view->twitterShareAbleLink = $twitter;
        $this->view->event = $eventObj;
        if (!$this->editmode && !$request->get('contentDocument') instanceof Snippet) {
            $this->view->isJoined = $eventJoinService->isJoined($eventObj, $request->getSession()->get('UserProfile'));
        }
    }

    /**
     * @param Request $request
     * @param SocialMediaService $socialMediaService
     * @param BreadcrumbHelperService $breadcrumbHelperService
     * @param EventJoinService $eventJoinService
     *
     * @Security("has_role('ROLE_USER')")
     * @throws \Exception
     */
    public function thankyouAction(
        Request $request,
        SocialMediaService $socialMediaService,
        BreadcrumbHelperService $breadcrumbHelperService,
        EventJoinService $eventJoinService
    ) {
        $routeVariables = StaticRouteHelper::removeUnusedRouteParams($request->get('_route_params'));

        $events = new Event\Listing();
        $getEventObject = $events->getObjectForStaticRoute($routeVariables);
        if (!$getEventObject && !$this->editmode && !$request->get('contentDocument') instanceof Snippet) {
            throw new NotFoundHttpException('Event tidak ditemukan');
        }
        if ($this->editmode) {
            $events = new Event\Listing();
            $events->setLimit(1);
            $events->load();

            /** @var Event $getEventObject */
            $getEventObject = $events->getObjects();
        }

        /** @var Event $eventObj */
        $eventObj = $getEventObject[0] ?? null;

        if (!$eventObj) {
            throw new NotFoundHttpException('Event tidak ditemukan');
        }

        $isJoined = $eventJoinService->isJoined($eventObj, $user = $request->getSession()->get('UserProfile'));

        if (!$isJoined) {
            throw new UnauthorizedHttpException('Kamu belum join event', 'Kamu belum join event');
        }

        $events->resetConditionParams();

        if ($request->get('_route') === 'EVENT') {
            $eventPageDetail = WebsiteSetting::getByName('EVENT_PAGE_DETAIL') ?
                WebsiteSetting::getByName('EVENT_PAGE_DETAIL')->getData() : null;
            $breadcrumbHelperService->enrichEventPage($eventObj, $eventPageDetail);
        }

        $fb = $socialMediaService->getFacebookShareAble($eventObj);
        $twitter = $socialMediaService->getTwitterShareAble($eventObj);

        $this->view->facebookShareAbleLink = $fb;
        $this->view->twitterShareAbleLink = $twitter;
        $this->view->event = $eventObj;
        $this->view->user = $user;
        if (!$this->editmode && !$request->get('contentDocument') instanceof Snippet) {
            $this->view->isJoined = $isJoined;
        }
    }

    /**
     *
     * @Route("/load/more")
     * @param Request $request
     * @return JsonResponse
     */
    public function moreAction(Request $request)
    {
        $queryRequest = $request->query->all();
        $nextPage = (int) ($queryRequest['nextPage'] ?? 1);
        $limit = (int) ($queryRequest['currentPageLimit'] ?? 6);
        $offset = ((($nextPage > 1)  ? $nextPage - 1 : 0)) * $limit;

        /** @var Paginator $eventObject */
        $eventObject = CardEventListHelper::getObjectWithFilter(
            $offset,
            $limit,
            $orderKey = 'startDate',
            $order = 'asc'
        );

        $html = $this->render('Widgets/CardEventList/list.html.php', [
            'data' => $eventObject
        ]);

        return $this->json([
            'success' => true,
            'data' => $html->getContent()
        ]);
    }
}
