<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/01/2020
 * Time: 13:16
 */

namespace AppBundle\Controller;

use AppBundle\Model\DataObject\Customer;
use AppBundle\Services\SocialMediaService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Model\DataObject\Tool;
use AppBundle\Services\ProductGroupService;
use AppBundle\Services\ProductService;
use Pimcore\Model\Document\Snippet;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    public function defaultAction(Request $request)
    {

    }

    public function detailAction(Request $request, ProductService $productService)
    {
        $this->view->product = $productService->getProduct();
    }

    public function groupAction(Request $request, ProductGroupService $productGroupService)
    {
        if (!$request->get('contentDocument') instanceof Snippet) {
            $this->view->productGroup = $productGroupService->getProductGroup();
        }
    }

    public function sampleAction(Request $request, ProductService $productService)
    {
        if (!$this->editmode && $productService->isActiveFreeSample()) {
            $this->view->product = $productService->getProduct();
        }
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @param ProductService $productService
     */
    public function claimAction(ProductService $productService)
    {
        $this->view->product = $productService->getProduct();

        if ($redirectTo = $productService->getRedirectTo()) {
            return $this->redirect($redirectTo);
        }
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @param ProductService $productService
     * @param SocialMediaService $socialMediaService
     */
    public function thankyouAction(ProductService $productService, SocialMediaService $socialMediaService)
    {
        if($productService->isActiveFreeSample()) {
            $product = $productService->getProduct();
            $this->view->product = $product;
            $this->view->facebook = $socialMediaService->getFacebookShareAble($product);
            $this->view->twitter = $socialMediaService->getTwitterShareAble($product);
            $this->view->whatsApp = $socialMediaService->getWhatsAppShareAble($product);
        }
    }
}
