<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/05/2020
 * Time: 11:33
 */

namespace AppBundle\Controller;

use AppBundle\Traits\Authenticate;
use Pimcore\Http\ResponseStack;
use Pimcore\Model\Document;
use Pimcore\Navigation\Page;
use Pimcore\Templating\Helper\Navigation;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MembershipController extends AbstractController
{
    use Authenticate;

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/membership/header", methods={"GET"})
     */
    public function headerHtmlMenu(Request $request)
    {
        //$request->getSession()->set('fromApi', true);
        //$this->authenticate();

        $data['navigation'] = $this->render('Include/nav.html.php', [
            'document' => Document::getById(1, 1),
            'formSearch' => $this->formSearch->createView()
        ])->getContent();
        //$request->getSession()->remove('fromApi');

        return $this->json($data, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/membership/footer", methods={"GET"})
     */
    public function footerHtmlMenu(Request $request)
    {
        $data['navigation'] = $this->renderTemplate('Include/footer.html.php')->getContent();

        return $this->json($data, 200);
    }

    /**
     * @param Request $request
     *
     * @param Navigation $navigation
     * @return JsonResponse
     *
     * @Route("/membership/menu", methods={"GET"})
     */
    public function menu(Request $request, Navigation $navigation)
    {
        $menus = [];

        $menus['FooterContent'] = $this->buildFooterContent();
        $menus['FooterNavigation'] = $this->buildFooterNavigation($navigation);
        $menus['MainNavigation'] = $this->buildMainNavigation($navigation);

        return $this->json($menus, 200);
    }

    private function buildFooterContent()
    {
        return null;
    }

    private function buildFooterNavigation(Navigation $navigation)
    {
        $mainNavigation = $this->mainNav($navigation);

        $footerNav = [];
        foreach ($mainNavigation->getPages() as $page) {
            $documentPage = Document::getById($page->getId(), 1);
            if (!$documentPage->hasProperty('NAV_FOOTER')) {
                continue;
            }

            $footerNav[] = $this->menuParentStructure($page);
        }

        return $footerNav;
    }

    private function buildMainNavigation(Navigation $navigation)
    {
        $mainNavigation = $this->mainNav($navigation);

        $mainNav = [];
        foreach ($mainNavigation->getPages() as $page) {
            $documentPage = Document::getById($page->getId(), 1);
            if (!$documentPage->hasProperty('NAV_TOP')) {
                continue;
            }

            $mainNav[] = $this->menuParentStructure($page);
        }

        return $mainNav;
    }

    private function mainNav(Navigation $navigation)
    {
        $document = Document\Page::getById(1);
        $mainNavStartNode = Document\Page::getById(1);

        return $navigation->buildNavigation($document, $mainNavStartNode);
    }

    private function menuParentStructure(Page $page)
    {
        $pagesChild = $page->getPages();

        $parent = [
            'Url' => $page->getHref(),
            'Name' => $page->getLabel(),
            'SubNavigation' => count($pagesChild) > 0 ?  $this->menuChildStructure($pagesChild) : null
        ];

        return $parent;
    }

    private function menuChildStructure(array $pagesChild)
    {
        return [];
    }
}
