<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 18:03
 */

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class ErrorController extends AbstractController
{
    public function notfoundAction(Request $request)
    {

    }

    public function internalservererrorAction(Request $request)
    {

    }
}
