<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 17:27
 */

namespace AppBundle\Controller;

use AppBundle\Services\ArticleService;
use AppBundle\Services\BannerTopArticleService;
use AppBundle\Services\DataLayerVariableInjectorService;
use AppBundle\Services\SchemaOrgService;
use AppBundle\Helper\{StaticRouteHelper};
use AppBundle\Model\DataObject\{Article, ArticleCategory};
use Symfony\Component\HttpFoundation\{JsonResponse, Request};
use AppBundle\Services\SocialMediaService;
use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Pimcore\Model\Document\Snippet;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @package AppBundle\Controller
 *
 * @Route("/article")
 */
class ArticleController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @Route("/")
     */
    public function defaultAction(Request $request)
    {

    }

    public function detailAction(
        Request $request,
        SocialMediaService $socialMediaService,
        BreadcrumbHelperService $breadcrumbHelperService,
        BannerTopArticleService $bannerTopArticleService,
        SchemaOrgService $schemaOrgService,
        DataLayerVariableInjectorService $dataLayerVariableInjectorService
    ) {
        $routeVariables = StaticRouteHelper::removeUnusedRouteParams($request->get('_route_params'));

        $article = new Article\Listing();
        $getArticleObject = $article->getObjectForStaticRoute($routeVariables);
        if (!$getArticleObject && !$this->editmode && !$request->get('contentDocument') instanceof Snippet) {
            throw new NotFoundHttpException('Artikel tidak ditemukan');
        }
        if ($this->editmode) {
            $article->setLimit(1);
            $article->load();

            /** @var Article $getArticleObject */
            $getArticleObject = $article->getObjects();
        }

        /** @var Article $articleObj */
        $articleObj = $getArticleObject[0] ?? null;
        if ($request->get('_route') === 'ARTICLE') {
            $articlePageDetail = WebsiteSetting::getByName('ARTICLE_PAGE_DETAIL') ?
                WebsiteSetting::getByName('ARTICLE_PAGE_DETAIL')->getData() : null;

            $schemaOrgService->getSchemaOrg($articleObj);
            $breadcrumbHelperService->enrichArticlePage($articleObj, $articlePageDetail);
        }

        $fb = $socialMediaService->getFacebookShareAble($articleObj);
        $twitter = $socialMediaService->getTwitterShareAble($articleObj);

        $bannerTopArticleService->getBanner();

        $dataLayerVariableInjectorService->injectData($articleObj);

        $this->view->facebookShareAbleLink = $fb;
        $this->view->twitterShareAbleLink = $twitter;
        $this->view->article = $articleObj;
    }

    public function categoryAction(Request $request)
    {
        $routeVariables = StaticRouteHelper::removeUnusedRouteParams($request->get('_route_params'));
        if ($request->get('_route') === 'ARTICLE_CATEGORY') {
            $articleCategorySlug = $request->get('slug');
            $articleCategory = ArticleCategory::getBySlug($articleCategorySlug, 1);
            if (!$articleCategory) {
                throw new NotFoundHttpException('Not Found');
            }
        }
    }

    /**
     * @param Request $request
     *
     * @param ArticleService $articleService
     * @return JsonResponse
     * @Route("/load/more")
     */
    public function moreAction(Request $request, ArticleService $articleService)
    {
        $articleObject = $articleService->getObjects();

        $html = $this->render('Article/more.html.php', [
            'articleObject' => $articleObject
        ]);

        return $this->json([
            'success' => true,
            'data' => $html->getContent()
        ]);
    }
}
