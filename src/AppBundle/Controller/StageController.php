<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 04/02/2020
 * Time: 14:26
 */

namespace AppBundle\Controller;

use AppBundle\Helper\StaticRouteHelper;
use AppBundle\Model\DataObject\Stage;
use Symfony\Component\HttpFoundation\{JsonResponse, Request};
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class StageController extends AbstractController
{
    public function defaultAction(Request $request)
    {
        $routeVariables = StaticRouteHelper::removeUnusedRouteParams($request->get('_route_params'));
        if ($request->get('_route') === 'STAGE') {
            $stageSlug = $request->get('slug');
            $stage = Stage::getBySlug($stageSlug, 1);
            if (!$stage) {
                throw new NotFoundHttpException('Stage Not Found');
            }
        }
    }

    public function detailAction(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/rewards/stages")
     */
    public function getStages(Request $request)
    {
        $stage = new \AppBundle\Api\v1\Loyalty\Stage();

        return $this->json($stage->getStages(), 200);
    }
}
