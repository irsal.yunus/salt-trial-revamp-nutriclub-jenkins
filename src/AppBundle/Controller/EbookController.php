<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/03/2020
 * Time: 12:42
 */

namespace AppBundle\Controller;

use AppBundle\Helper\{CardEbookListWidgetHelper, StaticRouteHelper};
use AppBundle\Model\DataObject\ArticleEbook;
use AppBundle\Services\SocialMediaService;
use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Symfony\Component\HttpFoundation\{JsonResponse, Request};
use Pimcore\Model\Document\Snippet;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Zend\Paginator\Paginator;

/**
 * Class EbookController
 * @package AppBundle\Controller
 *
 * @Route("/ebook")
 */
class EbookController extends AbstractController
{
    public function defaultAction(Request $request)
    {

    }

    public function detailAction(
        Request $request,
        SocialMediaService $socialMediaService,
        BreadcrumbHelperService $breadcrumbHelperService
    ) {
        $routeVariables = StaticRouteHelper::removeUnusedRouteParams($request->get('_route_params'));
        $articleEbooks = new ArticleEbook\Listing();
        $getArticleObject = $articleEbooks->getObjectForStaticRoute($routeVariables);
        if (!$getArticleObject && !$this->editmode && !$request->get('contentDocument') instanceof Snippet) {
            throw new NotFoundHttpException('Artikel tidak ditemukan');
        }
        if ($this->editmode) {
            $articleEbooks->setLimit(1);
            $articleEbooks->load();

            /** @var ArticleEbook $getArticleObject */
            $getArticleObject = $articleEbooks->getObjects();
        }
        /** @var ArticleEbook $articleObj */
        $articleObj = $getArticleObject[0] ?? null;
        if ($request->get('_route') === 'ARTICLE_EBOOK') {
            $ebookPageDetail = WebsiteSetting::getByName('EBOOK_PAGE_DETAIL') ?
                WebsiteSetting::getByName('EBOOK_PAGE_DETAIL')->getData() : null;
            $breadcrumbHelperService->enrichArticleEbookPage($articleObj, $ebookPageDetail);
        }

        $fb = $socialMediaService->getFacebookShareAble($articleObj);
        $twitter = $socialMediaService->getTwitterShareAble($articleObj);

        $this->view->facebookShareAbleLink = $fb;
        $this->view->twitterShareAbleLink = $twitter;
        $this->view->article = $articleObj;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/load/more")
     */
    public function moreAction(Request $request)
    {
        $queryRequest = $request->query->all();
        $nextPage = (int) ($queryRequest['nextPage'] ?? 1);
        $limit = (int) ($queryRequest['limit'] ?? 4);
        $offset = ((($nextPage > 1)  ? $nextPage - 1 : 0)) * $limit;

        /** @var ArticleEbook\Listing $ebookObject */
        $ebookObject = CardEbookListWidgetHelper::getObject(
            $offset,
            $limit
        );

        $html = $this->render('Ebook/more.html.php', [
            'ebookObject' => $ebookObject
        ]);

        return $this->json([
            'success' => $ebookObject->getCount() > 0,
            'data' => $html->getContent()
        ]);
    }
}
