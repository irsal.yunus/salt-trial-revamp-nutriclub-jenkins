<?php

namespace AppBundle\Controller;

use AppBundle\Api\v1\Rewards\Account;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProvinceController
 * @package AppBundle\Controller
 *
 * @Route("province")
 */
class ProvinceController extends AbstractController
{

    public function defaultAction(Request $request)
    {


    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/", methods={"GET"}, name="get-province")
     */
    public function getProvince(Request $request)
    {
        $account = new Account;
        $provinces = $account->getProvince([]);

        return $this->json($provinces, 200);
    }

    /**
     * @param Request $request
     * @param $provinceId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/district/{provinceId}", methods={"GET"}, name="get-district")
     */
    public function getDisctrict($provinceId, Request $request)
    {
        $account = new Account;
        $districts = $account->getDistrict($provinceId);

        return $this->json($districts, 200);
    }
}
