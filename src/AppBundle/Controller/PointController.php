<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource PointController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/08/20
 * @time 01.45
 *
 */

namespace AppBundle\Controller;

use AppBundle\Services\PointService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PointController
 * @package AppBundle\Controller
 *
 * @Route("/point")
 */
class PointController extends AbstractController
{
    /**
     * @param PointService $pointService
     *
     * @Route("/article", methods={"POST"}, name="point-article")
     *
     * @return JsonResponse
     */
    public function article(PointService $pointService)
    {
        return $this->json($pointService->article());
    }

    /**
     * @param PointService $pointService
     *
     * @Route("/tools", methods={"POST"}, name="point-tool")
     *
     * @return JsonResponse
     */
    public function tools(PointService $pointService)
    {
        return $this->json($pointService->tools());
    }
}
