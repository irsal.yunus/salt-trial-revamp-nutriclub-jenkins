<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource OtpController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/09/20
 * @time 13.20
 *
 */

namespace AppBundle\Controller;

use AppBundle\Services\OtpService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OtpController
 * @package AppBundle\Controller
 *
 * @Route("/otp")
 */
class OtpController extends AbstractController
{
    /**
     * @param OtpService $otpService
     *
     * @return JsonResponse
     * @Route("/", methods={"POST"})
     *
     */
    public function index(OtpService $otpService)
    {
        return $this->json($otpService->requestOtp());
    }

    /**
     * @param OtpService $otpService
     *
     * @return JsonResponse
     *
     * @Route("/verify", methods={"POST"})
     *
     */
    public function verify(OtpService $otpService)
    {
        return $this->json($otpService->requestOtpValidation());
    }
}
