<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/01/2020
 * Time: 13:17
 */

namespace AppBundle\Controller;

use AppBundle\Helper\{CardPodcastPlayerHelper, StaticRouteHelper};
use AppBundle\Model\DataObject\ArticlePodcast;
use AppBundle\Services\SocialMediaService;
use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Symfony\Component\HttpFoundation\{JsonResponse, Request};
use Pimcore\Model\Document\Snippet;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Zend\Paginator\Paginator;

/**
 * Class PodcastController
 * @package AppBundle\Controller
 *
 * @Route("/podcast")
 */
class PodcastController extends AbstractController
{
    public function defaultAction(Request $request)
    {

    }

    public function detailAction(
        Request $request,
        SocialMediaService $socialMediaService,
        BreadcrumbHelperService $breadcrumbHelperService
    ) {
        $routeVariables = StaticRouteHelper::removeUnusedRouteParams($request->get('_route_params'));

        $articlePodcast = new ArticlePodcast\Listing();
        $getArticlePodcastObject = $articlePodcast->getObjectForStaticRoute($routeVariables);
        if (!$getArticlePodcastObject && !$this->editmode && !$request->get('contentDocument') instanceof Snippet) {
            throw new NotFoundHttpException('Podcast tidak ditemukan');
        }
        if ($this->editmode) {
            $articlePodcast = new ArticlePodcast\Listing();
            $articlePodcast->setLimit(1);
            $articlePodcast->load();

            /** @var ArticlePodcast $getArticlePodcastObject */
            $getArticlePodcastObject = $articlePodcast->getObjects();
        }

        /** @var ArticlePodcast $articlePodcastObj */
        $articlePodcastObj = $getArticlePodcastObject[0] ?? null;

        $articlePodcast->resetConditionParams();
        $otherPodcastsObj = $articlePodcast->getOtherPodcasts([$articlePodcastObj ? $articlePodcastObj->getId() : 0], 6);

        if ($request->get('_route') === 'ARTICLE_PODCAST') {
            $podcastPageDetail = WebsiteSetting::getByName('PODCAST_PAGE_DETAIL') ?
                WebsiteSetting::getByName('PODCAST_PAGE_DETAIL')->getData() : null;
            $breadcrumbHelperService->enrichArticlePodcastPage($articlePodcastObj, $podcastPageDetail);
        }

        $fb = $socialMediaService->getFacebookShareAble($articlePodcastObj);
        $twitter = $socialMediaService->getTwitterShareAble($articlePodcastObj);

        $this->view->facebookShareAbleLink = $fb;
        $this->view->twitterShareAbleLink = $twitter;
        $this->view->podcast = $articlePodcastObj;
        $this->view->others = $otherPodcastsObj;
    }

    /**
     *
     * @Route("/load/more")
     * @param Request $request
     * @return JsonResponse
     */
    public function moreAction(Request $request)
    {
        $queryRequest = $request->query->all();
        $nextPage = (int) ($queryRequest['nextPage'] ?? 1);
        $limit = (int) ($queryRequest['currentPageLimit'] ?? 4);
        $offset = ((($nextPage > 1)  ? $nextPage - 1 : 0)) * $limit;

        /** @var Paginator $podcastObject */
        $podcastObject = CardPodcastPlayerHelper::getObjectWithFilter(
            $offset,
            $limit
        );

        $html = $this->render('Podcast/more.html.php', [
            'data' => $podcastObject
        ]);

        return $this->json([
            'success' => true,
            'data' => $html->getContent()
        ]);
    }
}
