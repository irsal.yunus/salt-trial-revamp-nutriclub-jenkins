<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/01/2020
 * Time: 13:17
 */

namespace AppBundle\Controller;

use AppBundle\Services\SchemaOrgService;
use AppBundle\Helper\{CardVideoListWidgetHelper, StaticRouteHelper};
use AppBundle\Model\DataObject\ArticleVideo;
use AppBundle\Services\SocialMediaService;
use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\{JsonResponse, Request};
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Pimcore\Model\Document\Snippet;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VideoController
 * @package AppBundle\Controller
 *
 * @Route("/video")
 */
class VideoController extends AbstractController
{
    public function defaultAction(Request $request)
    {

    }

    public function detailAction(
        Request $request,
        SocialMediaService $socialMediaService,
        BreadcrumbHelperService $breadcrumbHelperService,
        SchemaOrgService $schemaOrgService
    ) {
        $routeVariables = StaticRouteHelper::removeUnusedRouteParams($request->get('_route_params'));
        $articleVideo = new ArticleVideo\Listing();
        $getArticleVideoObject = $articleVideo->getObjectForStaticRoute($routeVariables);
        if (!$getArticleVideoObject && !$this->editmode && !$request->get('contentDocument') instanceof Snippet) {
            throw new NotFoundHttpException('Video tidak ditemukan');
        }
        if ($this->editmode) {
            $articleVideo->resetConditionParams();
            $articleVideo->setLimit(1);
            $articleVideo->load();

            /** @var ArticleVideo $getArticleVideoObject */
            $getArticleVideoObject = $articleVideo->getObjects();
        }

        /** @var ArticleVideo $articleVideoObj */
        $articleVideoObj = $getArticleVideoObject[0] ?? null;

        $articleVideo->resetConditionParams();
        $otherVideosObj = $articleVideo->getOtherVideos([$articleVideoObj ? $articleVideoObj->getId() : 0], 6);

        if ($request->get('_route') === 'ARTICLE_VIDEO') {
            $videoPageDetail = WebsiteSetting::getByName('VIDEO_PAGE_DETAIL') ?
                WebsiteSetting::getByName('VIDEO_PAGE_DETAIL')->getData() : null;

            $schemaOrgService->getSchemaOrg($articleVideoObj);

            $breadcrumbHelperService->enrichArticleVideoPage($articleVideoObj, $videoPageDetail);
        }

        $fb = $socialMediaService->getFacebookShareAble($articleVideoObj);
        $twitter = $socialMediaService->getTwitterShareAble($articleVideoObj);

        $this->view->facebookShareAbleLink = $fb;
        $this->view->twitterShareAbleLink = $twitter;
        $this->view->video = $articleVideoObj;
        $this->view->others = $otherVideosObj;
    }

    /**
     * @Route("/load/more")
     * @param Request $request
     * @return JsonResponse
     */
    public function moreAction(Request $request)
    {
        $queryRequest = $request->query->all();
        $nextPage = (int) ($queryRequest['nextPage'] ?? 1);
        $limit = (int) ($queryRequest['currentPageLimit'] ?? 4);
        $offset = ((($nextPage > 1)  ? $nextPage - 1 : 0)) * $limit;

        /** @var ArticleVideo\Listing $videoObject */
        $videoObject = CardVideoListWidgetHelper::getObjectWithFilter(
            $offset,
            $limit
        );

        $html = $this->render('Video/more.html.php', [
            'videoObject' => $videoObject
        ]);

        return $this->json([
            'success' => true,
            'data' => $html->getContent()
        ]);
    }
}
