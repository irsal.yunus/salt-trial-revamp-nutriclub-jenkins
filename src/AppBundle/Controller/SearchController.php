<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 06/02/2020
 * Time: 16:08
 */

namespace AppBundle\Controller;

use AppBundle\Api\v1\ElasticSearch\Popular\Popular;
use AppBundle\Api\v1\ElasticSearch\Search\Search;
use AppBundle\Helper\GeneralHelper;
use AppBundle\Model\DataObject\Customer;
use AppBundle\Model\ElasticSearch\Article;
use Symfony\Component\HttpFoundation\{JsonResponse, Request, Response};
use AppBundle\Services\{ElasticSearchQueryService, FaqSearchService, SearchHistoryService};
use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchController
 * @package AppBundle\Controller
 *
 * @Route("/search")
 */
class SearchController extends AbstractController
{
    public function defaultAction(
        Request $request,
        ElasticSearchQueryService $elasticSearchQueryService,
        SearchHistoryService $searchHistoryService,
        FaqSearchService $faqSearchService
    )
    {
        if ($request->getMethod() === 'POST') {
            if (!$this->isCsrfTokenValid('search', $request->get('_csrf_token'))) {
                throw new AccessDeniedHttpException('Invalid csrf token');
            }
            $query = $request->get('q');
            $type = $request->get('type') ?? [];

            /** @var Article $articles */
            $articles = $elasticSearchQueryService->getArticleFromElasticSearch($query, $type,8);

            $dotNetToken = $request->getSession()->get('AccessToken') ?? '1';
            /** @var Customer $userId */
            $userId = $request->getSession()->get('UserProfile');

            $params = [
                'headers' => [
                    'X-DOT-NET-TOKEN' => $dotNetToken,
                    'X-DOT-NET-UID' => $userId ? $userId->getId() : '1'
                ],
                'json' => [
                    'word' => $query
                ]
            ];

            $searchHistoryService->addMySearchHistory($params);
            $searchFaq = $faqSearchService->getSubFaqs($query);
            $countFaq = $searchFaq['totalData'];

            $this->view->searchQuery = htmlspecialchars($query);

            $this->view->searchTotalData = ($articles ? $articles->getTotalData() : 0) + $countFaq;

            $this->view->searchArticle = $articles ? $articles->getArticle() : null;
            $this->view->searchArticleTotalPage = $articles ? $articles->getArticleTotalPage() : null;
            $this->view->searchArticleTotalData = $articles ? $articles->getArticleTotalData() : null;

            $this->view->searchVideo = $articles ? $articles->getVideo() : null;
            $this->view->searchVideoTotalPage = $articles ? $articles->getVideoTotalPage() : null;
            $this->view->searchVideoTotalData = $articles ? $articles->getVideoTotalData() : null;

            $this->view->searchPodcast = $articles ? $articles->getPodcast() : null;
            $this->view->searchPodcastTotalPage = $articles ? $articles->getPodcastTotalPage() : null;
            $this->view->searchPodcastTotalData = $articles ? $articles->getPodcastTotalData() : null;

            $this->view->searchEbook = $articles ? $articles->getEbook() : null;
            $this->view->searchEbookTotalPage = $articles ? $articles->getEbookTotalPage() : null;
            $this->view->searchEbookTotalData = $articles ? $articles->getEbookTotalData() : null;

            $this->view->searchFaq = $searchFaq ?? [];
            $this->view->searchFaqTotalPage = null;
            $this->view->searchFaqTotalData = $countFaq;
        }
    }

    /**
     * @param Request $request
     * @Route("/history", methods={"DELETE"}, name="search-history-delete")
     * @return JsonResponse
     */
    public function history(Request $request)
    {
        $guzzleClient = new Client([
            'verify' => false,
            'debug' => false,
        ]);

        $baseUrl = getenv('ES_URL_V1', null);
        $dotNetToken = $request->getSession()->get('AccessToken');

        $requestSearchHistory = $guzzleClient->request('DELETE', $baseUrl . '/search', [
            'headers' => [
                'X-DOT-NET-TOKEN' => $dotNetToken
            ]
        ]);

        $response = [];

        if ($requestSearchHistory->getStatusCode() === Response::HTTP_OK) {
            $body = json_decode($requestSearchHistory->getBody()->getContents(), true);
            $response = $body;
        }

        return $this->json($response, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/history", methods={"GET"}, name="search-history-get")
     */
    public function historyList(Request $request)
    {
        $search = new Search();

        $token = $request->getSession()->get('AccessToken') ?? '';
        $params = [
            'headers' => [
                'X-DOT-NET-TOKEN' => $token
            ]
        ];

        $history = $search->history($params);
        $response = $history;

        return $this->json($response, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/popular", methods={"GET"}, name="search-popular-get")
     */
    public function popular(Request $request)
    {
        $popular = new Popular();
        $data = $popular->index([]);
        $response = $data;

        return $this->json($response, 200);
    }

    /**
     * @param Request $request
     * @param ElasticSearchQueryService $elasticSearchQueryService
     * @return JsonResponse
     * @Route("/load/more")
     */
    public function moreAction(Request $request, ElasticSearchQueryService $elasticSearchQueryService)
    {
        $query = $request->get('q');
        $nextPage = (int) ($request->get('nextPage') ?? 0);
        $type = $request->get('type') ?? '';
        $size= 8;
        $from = ((($nextPage > 1)  ? $nextPage - 1 : 0)) * $size;

        $typeSwitcher = GeneralHelper::articleTypeSwitcherMatchElasticSearchMapping($type);

        /** @var Article $articles */
        $articles = $elasticSearchQueryService->getArticleFromElasticSearch($query, [$typeSwitcher], 8, $from);

        $getterFromType = 'get' . ucfirst($type);
        $partialName = strtolower($type);

        if (!method_exists($articles, $getterFromType)) {
            return $this->json([
                'success' => false,
                'message' => 'Type Not Found',
                'data' => ''
            ]);
        }

        $objects = $articles->$getterFromType();

        $html = $this->render('Search/' . $partialName . '.partial.html.php', [
            'data' => $objects
        ]);

        return $this->json([
            'success' => true,
            'data' => $html->getContent()
        ]);
    }
}
