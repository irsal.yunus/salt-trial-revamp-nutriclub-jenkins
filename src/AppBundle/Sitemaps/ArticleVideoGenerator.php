<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 18:41
 */

namespace AppBundle\Sitemaps;

use Pimcore\Model\DataObject\ArticleVideo;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Sitemap\Element\{AbstractElementGenerator, GeneratorContext};
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ArticleVideoGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'article.video') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'article.video';

        $articleVideos = new ArticleVideo\Listing();
        $articleVideos->setOrderKey('oo_id');
        $articleVideos->setOrder('DESC');

        foreach ($articleVideos as $article) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($article);

            // run URL through registered processors
            $url = $this->process($url, $article, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(ArticleVideo $article)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $article->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for Article Video class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($article, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
