<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/07/2020
 * Time: 20:12
 */

namespace AppBundle\Sitemaps;

use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Model\DataObject\Product;
use Pimcore\Sitemap\Element\AbstractElementGenerator;
use Pimcore\Sitemap\Element\GeneratorContext;
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProductGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'product') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'product';

        $products = new Product\Listing();
        $products->setOrderKey('oo_id');
        $products->setOrder('DESC');

        foreach ($products as $product) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($product);

            // run URL through registered processors
            $url = $this->process($url, $product, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(Product $product)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $product->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for Product class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($product, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
