<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/02/2020
 * Time: 19:19
 */

namespace AppBundle\Sitemaps;

use AppBundle\Model\DataObject\ArticleCategory;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Sitemap\Element\{AbstractElementGenerator, GeneratorContext};
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ArticleCategoryGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'article.category') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'article.category';

        $articleCategories = new ArticleCategory\Listing();
        $articleCategories->setOrderKey('oo_id');
        $articleCategories->setOrder('DESC');

        foreach ($articleCategories as $articleCategory) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($articleCategory);

            // run URL through registered processors
            $url = $this->process($url, $articleCategory, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(ArticleCategory $article)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $article->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for ArticleCategory class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($article, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
