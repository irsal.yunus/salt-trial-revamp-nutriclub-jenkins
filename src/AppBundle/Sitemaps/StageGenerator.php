<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 17:30
 */

namespace AppBundle\Sitemaps;

use Pimcore\Model\DataObject\Stage;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Sitemap\Element\{AbstractElementGenerator, GeneratorContext};
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class StageGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'stage') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'stage';

        $stages = new Stage\Listing();
        $stages->setOrderKey('oo_id');
        $stages->setOrder('DESC');

        foreach ($stages as $stage) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($stage);

            // run URL through registered processors
            $url = $this->process($url, $stage, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(Stage $stage)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $stage->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for Stage class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($stage, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
