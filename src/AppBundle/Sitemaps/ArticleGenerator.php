<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 17:30
 */

namespace AppBundle\Sitemaps;

use Pimcore\Model\DataObject\Article;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Sitemap\Element\{AbstractElementGenerator, GeneratorContext};
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ArticleGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'article') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'article';

        $articles = new Article\Listing();
        $articles->setOrderKey('oo_id');
        $articles->setOrder('DESC');

        foreach ($articles as $article) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($article);

            // run URL through registered processors
            $url = $this->process($url, $article, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(Article $article)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $article->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for Article class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($article, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
