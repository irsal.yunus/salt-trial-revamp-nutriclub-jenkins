<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/03/2020
 * Time: 14:26
 */

namespace AppBundle\Sitemaps;

use AppBundle\Model\DataObject\Tool;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Sitemap\Element\{AbstractElementGenerator, GeneratorContext};
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ToolGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'tool') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'tool';

        $tools = new Tool\Listing();
        $tools->setOrderKey('oo_id');
        $tools->setOrder('DESC');

        foreach ($tools as $tool) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($tool);

            // run URL through registered processors
            $url = $this->process($url, $tool, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(Tool $tool)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $tool->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for Tool class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($tool, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
