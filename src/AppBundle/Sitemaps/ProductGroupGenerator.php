<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/07/2020
 * Time: 20:12
 */

namespace AppBundle\Sitemaps;

use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Model\DataObject\ProductGroup;
use Pimcore\Sitemap\Element\AbstractElementGenerator;
use Pimcore\Sitemap\Element\GeneratorContext;
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProductGroupGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'product.group') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'product.group';

        $productGroups = new ProductGroup\Listing();
        $productGroups->setOrderKey('oo_id');
        $productGroups->setOrder('DESC');

        foreach ($productGroups as $productGroup) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($productGroup);

            // run URL through registered processors
            $url = $this->process($url, $productGroup, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(ProductGroup $productGroup)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $productGroup->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for Product Group class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($productGroup, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
