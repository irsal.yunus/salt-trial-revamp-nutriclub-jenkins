<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/06/2020
 * Time: 16:20
 */

namespace AppBundle\Sitemaps;

use Pimcore\Model\DataObject\Event;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Sitemap\Element\AbstractElementGenerator;
use Pimcore\Sitemap\Element\GeneratorContext;
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EventGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'event') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'event';

        $events = new Event\Listing();
        $events->setOrderKey('oo_id');
        $events->setOrder('DESC');

        foreach ($events as $event) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($event);

            // run URL through registered processors
            $url = $this->process($url, $event, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(Event $event)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $event->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for Event class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($event, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
