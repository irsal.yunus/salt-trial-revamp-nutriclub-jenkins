<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource MoEngageOsmUtmService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 31/07/20
 * @time 23.02
 *
 */

namespace AppBundle\Services;

use Pimcore\Model\DataObject\MoEngageOsmUtm;
use Pimcore\Templating\Helper\Placeholder;
use Symfony\Component\Templating\EngineInterface;

class MoEngageOsmUtmService
{
    private $placeholder;

    private $engineInterface;

    private $utmService;

    public function __construct(Placeholder $placeholder, EngineInterface $engineInterface, UtmService $utmService)
    {
        $this->placeholder = $placeholder;
        $this->engineInterface = $engineInterface;
        $this->utmService = $utmService;
    }

    public function getValidUtmContent()
    {
        $utms = $this->utmService->getUtms();

        $utmContent = $utms['utm_content'] ?? null;

        if (!$utmContent) {
            return;
        }

        $moEngageOsmUtmObj = MoEngageOsmUtm::getByUtmContent($utmContent, 1);

        if (!$moEngageOsmUtmObj) {
            return;
        }

        $rendererDocument = $this->engineInterface->render(
            'Include/moengage.osm.utm.html.php', [
                'data' => $moEngageOsmUtmObj
            ]
        );

        $this->placeholder->__invoke('moEngageOsmUtm')->set($rendererDocument);
    }
}
