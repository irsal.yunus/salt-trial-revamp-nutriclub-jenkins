<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource DataLayerVariableInjectorService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 24/09/20
 * @time 15.53
 *
 */

namespace AppBundle\Services;

use AppBundle\Model\DataObject\Article;
use Pimcore\Templating\Helper\Placeholder;
use Symfony\Component\Templating\EngineInterface;

class DataLayerVariableInjectorService
{
    /** @var Placeholder $placeholder */
    private $placeholder;

    /** @var EngineInterface $engineInterface */
    private $engineInterface;

    public function __construct(Placeholder $placeholder, EngineInterface $engineInterface)
    {
        $this->placeholder = $placeholder;
        $this->engineInterface = $engineInterface;
    }

    public function injectData($article)
    {
        if (!$article instanceof Article) {
            return;
        }

        $variables = $this->engineInterface->render('Article/datalayer.html.php', [
            'data' => $article
        ]);

        $this->placeholder->__invoke('dataLayer')->set($variables);
    }
}
