<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 17:24
 */

namespace AppBundle\Services;

use AppBundle\EventListener\MoEngageListener;
use AppBundle\Model\DataObject\Customer;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class MoEngageService
{
    private $session;

    public function __construct(SessionInterface  $session)
    {
        $this->session = $session;
    }

    public function register(Customer $customer)
    {
        $this->session->getFlashBag()->set(MoEngageListener::ON_REGISTER_SUCCESS, true);
    }
}
