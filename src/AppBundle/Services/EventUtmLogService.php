<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 06/07/2020
 * Time: 14:39
 */

namespace AppBundle\Services;

use AppBundle\Model\DataObject\Customer;
use AppBundle\Model\DataObject\Event;
use AppBundle\Tool\Folder;
use Carbon\Carbon;
use Pimcore\File;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\EventParticipant;
use Pimcore\Model\DataObject\EventUtmLog;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class EventUtmLogService
{
    public const ACTION_LOGIN = 'LOGIN';

    public const ACTION_REGISTER = 'REGISTER';

    public const ACTION_JOIN_EVENT = 'JOIN_EVENT';

    /** @var Request $request */
    private $request;

    /** @var SessionInterface $session */
    private $session;

    /** @var UtmService $utmService */
    private $utmService;

    public function __construct(RequestStack $requestStack, SessionInterface $session, UtmService $utmService)
    {
        $this->utmService = $utmService;
        $this->session = $session;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function createUtmLog(Event $event)
    {
        $eventParticipant = $this->getEventParticipant();

        if (!$eventParticipant) {
            Simple::log('EVENT_UTM_LOG', 'GET EVENT PARTICIPANT NULL FAILED TO CONTINUE');
            return null;
        }

        $utms = $this->utmService->getUtms();
        $utmsFilter = count(array_filter($utms));

        if ($utmsFilter < 1) {
            Simple::log('EVENT_UTM_LOG', 'EMPTY UTM AT LEAST HAS ONE UTM FAILED TO CONTINUE');
            return null;
        }

        $currentDate = Carbon::now();
        $folderEventUtmLog = Folder::checkAndCreate('EventUtmLog');
        $folderEventUtmLogYear = Folder::checkAndCreate($currentDate->format('Y'), $folderEventUtmLog);
        $folderEventUtmLogMonth = Folder::checkAndCreate($currentDate->format('m'), $folderEventUtmLogYear);
        $folderEventUtmLogDay = Folder::checkAndCreate($currentDate->format('d'), $folderEventUtmLogMonth);

        $eventUtmLog = new EventUtmLog();

        $eventUtmLog->setParent($folderEventUtmLogDay);
        $objName = $currentDate->timestamp . '_' . $eventParticipant->getUserId();
        $eventUtmLog->setKey(File::getValidFilename($objName));

        $eventUtmLog->setUtmSource($utms['utm_source'] ?? null);
        $eventUtmLog->setUtmMedium($utms['utm_medium'] ?? null);
        $eventUtmLog->setUtmCampaign($utms['utm_campaign'] ?? null);
        $eventUtmLog->setUtmTerm($utms['utm_term'] ?? null);
        $eventUtmLog->setUtmContent($utms['utm_content'] ?? null);

        $eventUtmLog->setParticipant($eventParticipant);

        $eventUtmLog->setEvent($event);

        $eventUtmLog->setAction($this->getAction());

        $eventUtmLog->setPublished(true);

        try {
            $eventUtmLog->save([
                'versionNote' => 'CREATE_FROM_SERVICE'
            ]);

            return $eventUtmLog;
        } catch (\Exception $exception) {
            Simple::log('EVENT_UTM_LOG_SERVICE', $exception->getMessage());
            return null;
        }
    }

    /**
     * @return EventParticipant|null
     */
    private function getEventParticipant()
    {
        /** @var Customer $userProfile */
        $userProfile = $this->session->get('UserProfile');

        if (!$userProfile instanceof Customer) {
            return null;
        }

        $userId = $userProfile->getId();

        return EventParticipant::getByUserId($userId, 1);
    }

    private function getAction()
    {
        $action = $this->request->get('_route') === 'account-login' ? self::ACTION_LOGIN : null;

        if ($action) {
            return $action;
        }

        $action = $this->request->get('_route') === 'account-registration' ? self::ACTION_REGISTER : null;

        if ($action) {
            return $action;
        }

        $action = $this->request->get('_route') === 'EVENT' ? self::ACTION_JOIN_EVENT : null;

        return $action;
    }
}
