<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/03/2020
 * Time: 13:06
 */

namespace AppBundle\Services;

use AppBundle\Api\v1\ElasticSearch\Search\Search;

class SearchHistoryService
{
    private $search;

    public function __construct()
    {
        $this->search = new Search();
    }

    public function getMySearchHistory(array $params)
    {
        $data = $this->search->history($params);

        return $data;
    }

    public function addMySearchHistory(array $params)
    {
        $data = $this->search->add($params);

        return $data;
    }
}
