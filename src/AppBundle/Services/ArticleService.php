<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 25/03/2020
 * Time: 16:56
 */

namespace AppBundle\Services;

use AppBundle\Model\DataObject\Article;
use Symfony\Component\HttpFoundation\Request;

class ArticleService
{
    /** @var array $params */
    private $params;

    public function __construct()
    {
        $request = Request::createFromGlobals();

        $params = $request->query->all();

        $this->params = $this->conditionParamBuilder($params);
    }

    public function getObjects() : Article\Listing
    {
        $articleListing = new Article\Listing();
        $params = $this->params;

        $queryAbleParams = $params['queryable'];
        $callAbleParams = $params['callable'];

        foreach ($queryAbleParams as $queryAbleParam) {
            $articleListing
                ->addConditionParam($queryAbleParam[0], $queryAbleParam[1], $queryAbleParam[2]);
        }

        foreach ($callAbleParams as $callAbleParam) {
            if (!method_exists($articleListing, $callAbleParam[0])) {
                continue;
            }

            $setter = $callAbleParam[0];
            $value = $callAbleParam[1];

            $articleListing->$setter($value);
        }

        $articleListing->load();

        return $articleListing;
    }

    private function variableMapping() : array
    {
        $mapping = [
            'queryable' => [
                'roleFocusId' => [
                    'isLikeable' => false
                ],
                'categoryId' => [
                    'isLikeable' => false
                ],
                'subCategoryId' => [
                    'isLikeable' => true
                ],
                'stageId' => [
                    'isLikeable' => false
                ],
                'stageGroupId' => [
                    'isLikeable' => false
                ],
                'subStageId' => [
                    'isLikeable' => true
                ],
                'stageAgeId' => [
                    'isLikeable' => false
                ]
            ],
            'callable' => [
                'orderKey' => [
                    'type' => 'string',
                    'isSetAble' => true
                ],
                'order' => [
                    'type' => 'string',
                    'isSetAble' => true
                ],
                'nextPage' => [
                    'type' => 'int',
                    'isSetAble' => false
                ],
                'limit' => [
                    'type' => 'int',
                    'isSetAble' => true
                ],
                'offset' => [
                    'type' => 'int',
                    'isSetAble' => true
                ]
            ]
        ];

        return $mapping;
    }

    private function conditionParamBuilder(array $params) : array
    {
        $filteredParams = array_filter($params);

        // @todo validation ?

        $queryAbleParams = [];
        $callAbleParams = [];
        $conditionParams = [];

        $variableMapping = $this->variableMapping();
        $variableMappingQueryAble = $variableMapping['queryable'];
        $variableMappingCallAble = $variableMapping['callable'];

        foreach ($filteredParams as $key => $filteredParam) {
            if (!array_key_exists($key, $variableMappingQueryAble)) {
                $tmpCallAbleParams[$key] = $filteredParam;
                continue;
            }

            $regexPattern = '/(Id)/';
            $findSuffix = preg_match($regexPattern, $key);
            if (!$findSuffix) {
                continue;
            }

            $configFromMapping = $variableMappingQueryAble[$key];

            $keyQuery = null;
            $valueQuery = null;

            if ($configFromMapping['isLikeable']) {
                $replace = preg_replace($regexPattern, '', $key);
                $keyQuery = sprintf('%s LIKE ?', $replace);
                $valueQuery = '%' . $filteredParam . '%';
            }

            if (!$configFromMapping['isLikeable']) {
                $replace = preg_replace($regexPattern, '__id', $key);
                $keyQuery = sprintf('%s = ?', $replace);
                $valueQuery = $filteredParam;
            }

            $queryAbleParams[] = [$keyQuery, $valueQuery, 'AND'];
        }

        $nextPage = (int) ($tmpCallAbleParams['nextPage'] ?? 1);
        $limit = (int) ($tmpCallAbleParams['limit'] ?? 4);
        $offset = ((($nextPage > 1)  ? $nextPage - 1 : 0)) * $limit;

        $tmpCallAbleParams['offset'] = $offset;

        if (!array_key_exists('orderKey', $tmpCallAbleParams)) {
            $tmpCallAbleParams['orderKey'] = 'oo_id';
        }

        if (!array_key_exists('order', $tmpCallAbleParams)) {
            $tmpCallAbleParams['order'] = 'DESC';
        }

        foreach ($tmpCallAbleParams as $key => $callAbleParam) {
            if (!array_key_exists($key, $variableMappingCallAble)) {
                continue;
            }

            $configFromMapping = $variableMappingCallAble[$key];

            if ($configFromMapping['type'] === 'int') {
                $callAbleParam = (int) $callAbleParam;
            }

            if ($configFromMapping['isSetAble']) {
                $setter = 'set' . ucfirst($key);
                $callAbleParams[] = [$setter, $callAbleParam];
            }
        }

        $conditionParams['queryable'] = $queryAbleParams;
        $conditionParams['callable'] = $callAbleParams;

        return $conditionParams;
    }
}
