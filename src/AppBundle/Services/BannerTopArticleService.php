<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 30/06/2020
 * Time: 11:14
 */

namespace AppBundle\Services;

use AppBundle\Model\DataObject\Article;
use Pimcore\Model\DataObject\BannerTopArticle;
use Pimcore\Templating\Helper\Placeholder;
use Symfony\Component\HttpFoundation\{Request, RequestStack};
use Symfony\Component\Templating\EngineInterface;

class BannerTopArticleService
{
    /** @var Request  */
    private $request;

    /** @var EngineInterface */
    private $engineInterface;

    private $placeholder;

    public function __construct(
        Placeholder $placeholder,
        RequestStack $requestStack,
        EngineInterface $engine
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->placeholder = $placeholder;
        $this->engineInterface = $engine;
    }

    public function getBanner()
    {
        $request = $this->request;
        $articleSlug = $request->get('articleSlug');

        /** @var Article $article */
        $article = Article::getBySlug($articleSlug, 1);

        if (!$article) {
            return;
        }

        $bannerTopArticles = new BannerTopArticle\Listing();
        $bannerTopArticles->setCondition('topArticles LIKE ?', [
            '%' . $article->getId() . '%'
        ]);

        if ($bannerTopArticles->getCount() < 1) {
            return;
        }

        /** @var BannerTopArticle $bannerTopArticle */
        $bannerTopArticle = $bannerTopArticles->getObjects()[0] ?? null;

        if (!$bannerTopArticle) {
            return;
        }

        $template = $bannerTopArticle && $bannerTopArticle->getBanner() ?
            $bannerTopArticle->getBanner()->getFullPath() : null;

        $this->placeholder->__invoke('bannerTopArticles')->set($template);
    }
}
