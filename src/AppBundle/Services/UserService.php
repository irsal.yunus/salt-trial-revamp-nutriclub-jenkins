<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource UserService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 11/08/20
 * @time 12.01
 *
 */

namespace AppBundle\Services;

use AppBundle\Model\DataObject\Customer;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class UserService
{
    /** @var SessionInterface $session */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @return Customer|null
     */
    public function getUser()
    {
        return $this->session->get('UserProfile', null);
    }

    /**
     * @return array|null
     */
    public function getUserRaw()
    {
        return $this->session->get('UserProfileRaw', []);
    }

    /**
     * @param array $userProfileRaw
     *
     * @return array|null
     */
    public function setUserRaw(array $userProfileRaw)
    {
        return $this->session->set('UserProfileRaw', $userProfileRaw);
    }
}
