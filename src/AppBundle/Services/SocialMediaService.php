<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 02/03/2020
 * Time: 11:34
 */

namespace AppBundle\Services;

use AppBundle\Model\RouterInterface;
use AppBundle\Model\SocialMediaInterface;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\Document\Tag\Link;

class SocialMediaService
{
    public function getFacebookShareAble($abstractObject)
    {
        $link = new Link();
        if (!$abstractObject instanceof AbstractObject) {
            return $link;
        }

        if (!$abstractObject instanceof RouterInterface) {
            return $link;
        }

        if (!$abstractObject instanceof SocialMediaInterface) {
            return $link;
        }

        $title = $abstractObject->getTitle();
        $url = $abstractObject->getRouter();

        $facebookSharer = 'https://www.facebook.com/sharer.php?t='. $title .'&u=' . $url .'';

        $link->setDataFromResource([
            'target' => '_blank',
            'path' => $facebookSharer,
        ]);

        return $link;
    }

    public function getTwitterShareAble($abstractObject)
    {
        $link = new Link();
        if (!$abstractObject instanceof AbstractObject) {
            return $link;
        }

        if (!$abstractObject instanceof RouterInterface) {
            return $link;
        }

        if (!$abstractObject instanceof SocialMediaInterface) {
            return $link;
        }

        $title = $abstractObject->getTitle();
        $url = $abstractObject->getRouter();

        $twitterTweet = 'https://twitter.com/intent/tweet?text=' . $title . '&url='. $url .' ';

        $link->setDataFromResource([
            'target' => '_blank',
            'path' => $twitterTweet,
        ]);

        return $link;
    }

    public function getWhatsAppShareAble($abstractObject)
    {
        $link = new Link();
        if (!$abstractObject instanceof AbstractObject) {
            return $link;
        }

        if (!$abstractObject instanceof RouterInterface) {
            return $link;
        }

        if (!$abstractObject instanceof SocialMediaInterface) {
            return $link;
        }

        $title = $abstractObject->getTitle();
        $url = $abstractObject->getRouter();

        $whatsApp = 'whatsapp://send?text='.$url;

        $link->setDataFromResource([
            'target' => '_blank',
            'path' => $whatsApp,
        ]);

        return $link;
    }
}
