<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource FreeSampleService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 07/08/20
 * @time 12.35
 *
 */

namespace AppBundle\Services;

use AppBundle\Model\DataObject\ProductGroup;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\RequestStack;

class FreeSampleService
{
    private $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function channelProductGroup($productGroup = null)
    {
        $productGroupSlug = $productGroup ?? $this->request->get('groupSlug');

        if (!$productGroupSlug) {
            return;
        }

        /** @var ProductGroup $productGroupObj */
        $productGroupObj = $productGroupSlug instanceof ProductGroup ? $productGroupSlug :
            ProductGroup::getBySlug($productGroupSlug, 1);

        if(!$productGroupObj) {
            return;
        }

        $productGroupObj->getChannel() ?
            $this->cookieCounter('channel', $productGroupObj->getChannel()) :
            null;
    }

    private function cookieCounter($cookieName, $value, $expired = false)
    {
        $dotNetDomain = env('WILDCARD_DOMAIN_FOR_DOT_NET', null);

        $expiredCookie = $expired ? 1 : Carbon::now()->addMinutes(15)->timestamp;

        setrawcookie(
            $cookieName,
            $value,
            $expiredCookie,
            '/',
            $dotNetDomain
        );
    }
}
