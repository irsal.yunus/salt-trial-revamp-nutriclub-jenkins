<?php

declare(strict_types=1);

namespace AppBundle\Services;

use Pimcore\Targeting\Model\VisitorInfo;
use Pimcore\Targeting\Storage\TargetingStorageInterface;

class VisitedSamePagesCounter
{
    /** @var string $storageKey */
    private $storageKey = 'vspc_';

    /**
     * @var TargetingStorageInterface
     */
    private $targetingStorage;

    /**
     * @var bool
     */
    private $incremented = false;

    public function __construct(TargetingStorageInterface $targetingStorage)
    {
        $this->targetingStorage = $targetingStorage;
    }

    /**
     * @return string
     */
    public function getStorageKey(): string
    {
        return $this->storageKey;
    }

    /**
     * @param string $storageKey
     */
    public function setStorageKey(string $storageKey): void
    {
        $this->storageKey = $storageKey;
    }

    public function getCount(VisitorInfo $visitorInfo, string $scope = TargetingStorageInterface::SCOPE_VISITOR): int
    {
        $visitorStorageKey = (string) $visitorInfo->get('vspf_storagekey');
        $this->setStorageKey($visitorStorageKey);

        return $this->targetingStorage->get($visitorInfo, $scope, $this->getStorageKey(), 0);
    }

    public function increment(VisitorInfo $visitorInfo, string $scope = TargetingStorageInterface::SCOPE_VISITOR, bool $force = false)
    {
        if ($this->incremented && !$force) {
            return;
        }

        // TODO to make sure this works in concurrent request we probably need
        // to support some kind of transactional updates on the storage
        $count = $this->getCount($visitorInfo, $scope);
        $count++;

        $visitorStorageKey = (string) $visitorInfo->get('vspf_storagekey');
        $this->setStorageKey($visitorStorageKey);

        $this->targetingStorage->set($visitorInfo, $scope, $this->getStorageKey(), $count);

        $this->incremented = true;
    }
}
