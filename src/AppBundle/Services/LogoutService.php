<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/04/2020
 * Time: 15:22
 */

namespace AppBundle\Services;

use AppBundle\Api\v1\Loyalty\Access;
use AppBundle\Api\v1\Rewards\Account;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class LogoutService
{
    /** @var SessionInterface $session */
    private $session;

    /** @var ContainerInterface $container */
    private $container;

    /** @var Access $access */
    private $access;

    /** @var Account */
    private $rewardsAccountApi;

    public function __construct(SessionInterface $session, ContainerInterface $container)
    {
        $this->session = $session;
        $this->container = $container;
        $this->access = new Access();
        $this->rewardsAccountApi = new Account();
    }

    public function doLogout()
    {
        $accessToken = $this->session->get('AccessToken');

        /** @var UsageTrackingTokenStorage $tokenStorage */
        $tokenStorage = $this->container->get('security.token_storage');
        /** @var AuthorizationChecker $authChecker */
        $authChecker = $this->container->get('security.authorization_checker');

        $tokenStorage->setToken(null);

        // logout api.
        if (env('RRR_LIVE')) {
            $this->access->logout($accessToken);
        }

        if (!env('RRR_LIVE')) {
            $this->rewardsAccountApi->logout($accessToken);
        }

        // clear UserProfile.
        $this->session->remove('UserProfile');
        // clear dotNetAccessToken
        $this->session->remove('AccessToken');

        $request = Request::createFromGlobals();
        $cookies = $request->cookies;

        $dotNetCookies = explode(',', env('DOTNET_COOKIES_FOR_AUTH_SEPERATED_BY_COMMA', null));
        $dotNetDomain = env('WILDCARD_DOMAIN_FOR_DOT_NET', null);

        if (!$dotNetCookies) {
            return;
        }

        foreach ($cookies as $key => $cookie) {
            if (!in_array($key, $dotNetCookies, true)) {
                continue;
            }

            setrawcookie(
                $key,
                null,
                1,
                '/',
                $dotNetDomain
            );
        }
    }
}
