<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/07/2020
 * Time: 17:25
 */

namespace AppBundle\Services;

use AppBundle\Model\SchemaOrgInterface;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Templating\Helper\HeadScript;

class SchemaOrgService
{
    private $headScript;

    public function __construct(HeadScript $headScript)
    {
        $this->headScript = $headScript;
    }

    public function getSchemaOrg(Concrete $object)
    {
        if (!$object instanceof SchemaOrgInterface) {
            return;
        }

        $this->headScript
            ->offsetSetScript(1010000, json_encode($object->getSchemaOrg()), 'application/ld+json');
    }
}
