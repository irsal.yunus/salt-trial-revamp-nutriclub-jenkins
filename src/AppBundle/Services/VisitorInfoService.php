<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/02/2020
 * Time: 17:41
 */

namespace AppBundle\Services;

use Pimcore\Model\Tool\Targeting\TargetGroup;
use Pimcore\Targeting\VisitorInfoStorageInterface;

class VisitorInfoService
{
    /**
     * @var VisitorInfoStorageInterface
     */
    private $visitorInfoStorage;

    public function __construct(VisitorInfoStorageInterface $visitorInfoStorage)
    {
        $this->visitorInfoStorage = $visitorInfoStorage;
    }

    public function getVisitorInfo()
    {
        // always check if there is a visitor info before trying to fetch it
        if (!$this->visitorInfoStorage->hasVisitorInfo()) {
            return null;
        }

        return $this->visitorInfoStorage->getVisitorInfo();
    }

    public function getCurrentTargeting()
    {
        $visitorInfo = $this->getVisitorInfo();

        if (!$visitorInfo) {
            return $visitorInfo;
        }

        $sortedTargetGroupAssignments = $visitorInfo->getAssignedTargetGroups();

        /** @var TargetGroup $currentTargeting */
        $currentTargeting = $sortedTargetGroupAssignments[0] ?? null;

        return $currentTargeting !== null ? $currentTargeting->getDescription() : null;
    }
}
