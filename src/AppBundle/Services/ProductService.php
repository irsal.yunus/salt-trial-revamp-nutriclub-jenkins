<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/07/2020
 * Time: 11:20
 */

namespace AppBundle\Services;

use AppBundle\Api\v1\Rewards\Account;
use AppBundle\Model\DataObject\Product;
use AppBundle\Tool\Folder;
use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Carbon\Carbon;
use Pimcore\File;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\Fieldcollection;
use Pimcore\Model\DataObject\Fieldcollection\Data\ProductAnswers;
use Pimcore\Model\DataObject\FreeSampleClaim;
use Pimcore\Model\WebsiteSetting;
use Pimcore\Templating\Helper\HeadScript;
use Pimcore\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Model\DataObject\Customer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProductService
{
    private $request;

    private $breadcrumbHelperService;

    private $productGroupService;

    private $productSlug;

    private $schemaOrgService;

    private $isBreadcrumbs = false;

    /** @var SessionInterface $session */
    private $session;

    private $redirectTo;

    /** @var ContainerInterface $container */
    private $container;

    /** @var Product $product */
    private $product;

    /** @var FlashBag $flashBag*/
    private $flashBag;

    /** @var Translator $translator */
    private $translator;

    public function __construct(
        RequestStack $requestStack,
        BreadcrumbHelperService $breadcrumbHelperService,
        ProductGroupService $productGroupService,
        SchemaOrgService $schemaOrgService,
        SessionInterface  $session,
        Translator $translator,
        ContainerInterface $container
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->breadcrumbHelperService = $breadcrumbHelperService;
        $this->productGroupService = $productGroupService;
        $this->schemaOrgService = $schemaOrgService;
        $this->session = $session;
        $this->flashBag = $this->session->getFlashBag();
        $this->translator = $translator;
        $this->container = $container;
    }

    public function getProduct()
    {
        $productGroup = $this->productGroupService->getProductGroup();

        $product = $this->isProductExist();
        $isEditMode = $this->request->get('_editmode');

        if (!$product && !$isEditMode) {
            throw new NotFoundHttpException('Product not found');
        }

        if ($isEditMode) {
            $productLists = new Product\Listing();
            $productLists->setLimit(1);
            $productLists->load();

            $product = $productLists->getCount() > 0 ? $productLists->getObjects()[0] : null;
        }

        if ($product->getGroup() !== $productGroup) {
            throw new NotFoundHttpException('Product group does not belongs for this product');
        }

        $this->product = $product;

        // claim.
        $this->claimSubmit();

        $route = $this->request->get('_route');
        if ($route === 'PRODUCT' && !$this->isBreadcrumbs) {
            $productGroupPage = WebsiteSetting::getByName('PRODUCT_GROUP_PAGE') ?
                WebsiteSetting::getByName('PRODUCT_GROUP_PAGE')->getData() : null;

            $this->breadcrumbHelperService->enrichProductGroupPage($productGroup, $productGroupPage);
            $this->breadcrumbHelperService->enrichProductPage($product, null);
            $this->isBreadcrumbs = true;
        }

        $this->schemaOrgService->getSchemaOrg($product);

        return $product;
    }

    public function isProductExist()
    {
        $this->productSlug = $this->request->get('productSlug');

        return Product::getBySlug($this->productSlug, 1);
    }

    public function isActiveFreeSample()
    {
        $product = $this->isProductExist();

        if (!$product->getIsActiveFreeSample()) {
            throw new NotFoundHttpException('Free Sample Not Found');
        }

        return $this->getProduct();
    }

    /**
     * @return mixed
     */
    public function getRedirectTo()
    {
        $this->redirectTo =  $this->session->get('redirectTo');
        return $this->redirectTo;
    }

    /**
     * @param mixed $redirectTo
     */
    public function setRedirectTo($redirectTo): void
    {
        $redirectTo = $this->session->set('redirectTo', $redirectTo);
        $this->redirectTo = $redirectTo;
    }

    public function claimSubmit()
    {
        if ($this->request->getMethod() !== 'POST') {
            return [];
        }

        $account = new Account();

        /** @var Customer $customer */
        $customer = $this->session->get('UserProfile');

        $checkExistingFreeSampleParticipant = new FreeSampleClaim\Listing();
        $checkExistingFreeSampleParticipant->setCondition('memberId = ? AND product__id = ?', [
            $customer->getId(),
            $this->product->getId()
        ]);

        // user already claimed.
        if ($checkExistingFreeSampleParticipant->getCount() > 0) {
            $this->flashBag->set('claimNotif', [
                'StatusCode' => null,
                'StatusMessage' => $this->translator->trans('PRODUCT_FREE_SAMPLE_USER_ALREADY_CLAIM')
            ]);

            // @todo already claim return to something.
            return [];
        }

        $claim = $this->createFreeSampleClaimer($customer);

        if (!$claim) {
            $this->flashBag->set('claimNotif', [
                'StatusCode' => null,
                'StatusMessage' => $this->translator->trans('PRODUCT_FREE_SAMPLE_SOMETHING_WENT_WRONG')
            ]);

            return [];
        }

        $user = $this->session->get('UserProfileRaw');
        $token = $this->session->get('AccessToken');

        $user['ProvinceID'] = (int) $this->request->get('provinceId');
        $user['DistrictID'] = (int) $this->request->get('districtId');
        $user['Address'] = $this->request->get('address');
        $user['ZipCode'] = $this->request->get('postalCode');

        // update profile information
        $updateProfile = $account->updateProfile($user, $token);
        Simple::log('UPDATE_PROFILE_' . Carbon::now()->format('d-m-Y'), json_encode($updateProfile));

        $redirectToThankYouPage = $this->container
            ->get('router')
            ->generate('PRODUCT_FREE_SAMPLE_THANKYOU', [
                'groupSlug' => $this->product->getGroup()->getSlug(),
                'productSlug' => $this->product->getSlug()
            ], UrlGeneratorInterface::ABSOLUTE_URL);

        $this->setRedirectTo($redirectToThankYouPage);

        return true;
    }

    public function createFreeSampleClaimer(Customer $customer)
    {
        $folderFreeSampleParticipant = Folder::checkAndCreate('FreeSampleParticipant');
        $folderFreeSampleParticipantId = Folder::checkAndCreate($customer->getId(), $folderFreeSampleParticipant);

        $freeSampleParticipant = new FreeSampleClaim();

        $freeSampleParticipant->setParent($folderFreeSampleParticipantId);
        $freeSampleParticipant->setKey(File::getValidFilename($customer->getId() . '_' . $this->product->getId()));
        $freeSampleParticipant->setMemberId($customer->getId());
        $freeSampleParticipant->setEmail($customer->getEmail());
        $freeSampleParticipant->setPhone($customer->getPhone());
        $freeSampleParticipant->setProduct($this->product);

        $freeSampleParticipant->setAddress($this->request->get('address'));
        $freeSampleParticipant->setProvince($this->request->get('provinceName'));
        $freeSampleParticipant->setDistrict($this->request->get('districtName'));
        $freeSampleParticipant->setPostalCode($this->request->get('postalCode'));

        $freeSampleParticipant->setName($customer->getFullname());

        $freeSampleParticipant->setAnswers($this->createAnswerCollections());

        $freeSampleParticipant->setPublished(true);

        try {
            $freeSampleParticipant->save([
                'versionNote' => 'AUTO_CREATED_FREE_SAMPLE_PRODUCT_SERVICE'
            ]);
        } catch (\Exception $exception) {
            $freeSampleParticipant = null;
            Simple::log('FREE_SAMPLE_PRODUCT_SERVICE_CREATE_USER_FAILED', $exception->getMessage());
        } finally {
            return $freeSampleParticipant;
        }
    }

    public function createAnswerCollections()
    {
        $questions = $this->request->get('questions', []);

        if (count($questions) < 1) {
            return null;
        }

        $fieldCollection = new Fieldcollection();

        foreach ($questions as $question) {
            $q = $question['q'];
            $a = $question['a'];

            $productAnswerCollection = new ProductAnswers();
            $productAnswerCollection->setQuestion($q);
            $productAnswerCollection->setAnswer($a);

            $fieldCollection->add($productAnswerCollection);
        }

        return $fieldCollection;
    }
}
