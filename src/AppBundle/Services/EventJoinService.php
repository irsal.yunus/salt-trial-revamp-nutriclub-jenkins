<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/06/2020
 * Time: 18:22
 */

namespace AppBundle\Services;

use AppBundle\Api\v1\Lixus\Lixus;
use AppBundle\Helper\CustomSmsHelper;
use AppBundle\Model\DataObject\Customer;
use AppBundle\Model\DataObject\Event;
use AppBundle\Tool\Folder;
use Pimcore\File;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\EventParticipant;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EventJoinService
{
    /** @var SessionInterface $session */
    private $session;

    private $lixus;

    /** @var Event $eventObj */
    private $eventObj;

    private $redirectTo;

    /** @var int $changePageCounter */
    private $changePageCounter = 0;

    /** @var ContainerInterface $container */
    private $container;

    public function __construct(SessionInterface $session, ContainerInterface $container)
    {
        $this->session = $session;
        $this->lixus = new Lixus();

        $this->container = $container;
    }

    /**
     * @return Event|null
     */
    public function getEventObj()
    {
        $this->eventObj =  $this->session->get('eventObj');
        return $this->eventObj;
    }

    /**
     * @param Event $eventObj
     */
    public function setEventObj(Event $eventObj): void
    {
        $eventObj = $this->session->set('eventObj', $eventObj);
        $this->eventObj = $eventObj;
    }

    /**
     * @return mixed
     */
    public function getRedirectTo()
    {
        $this->redirectTo =  $this->session->get('redirectTo');
        return $this->redirectTo;
    }

    /**
     * @param mixed $redirectTo
     */
    public function setRedirectTo($redirectTo): void
    {
        $redirectTo = $this->session->set('redirectTo', $redirectTo);
        $this->redirectTo = $redirectTo;
    }

    public function checkAndCreateParticipant(Customer $userRewards)
    {
        $folderEventParticipant = Folder::checkAndCreate('EventParticipant');
        $folderEventParticipantId = Folder::checkAndCreate($userRewards->getId(), $folderEventParticipant);

        /** @var EventParticipant $checkExistingEventParticipant */
        $checkExistingEventParticipant = EventParticipant::getByUserId($userRewards->getId(), 1);

        if (!$checkExistingEventParticipant) {
            $eventParticipant = new EventParticipant();

            $eventParticipant->setParent($folderEventParticipantId);
            $eventParticipant->setKey(File::getValidFilename($userRewards->getId()));

            $eventParticipant->setUserId($userRewards->getId());
            $eventParticipant->setFullName($userRewards->getFullname());
            $eventParticipant->setEmail($userRewards->getEmail());
            $eventParticipant->setPhone($userRewards->getPhone());
            $eventParticipant->setGender(strtolower($userRewards->getGender()) === 'm' ? 'male' : 'female');

            $eventParticipant->setPublished(true);

            try {
                $eventParticipant->save([
                    'versionNote' => 'AUTO_CREATED_EVENT_JOIN_SERVICE'
                ]);
            } catch (\Exception $exception) {
                $eventParticipant = null;
                Simple::log('EVENT_JOIN_SERVICE_CREATE_USER_FAILED', $exception->getMessage());
            }

            $checkExistingEventParticipant = $eventParticipant;
        }

        if (!$checkExistingEventParticipant) {
            $this->clearSession();
            return;
        }

        $eventObj = $this->getEventObj();

        if (!$eventObj) {
            return;
        }

        $eventObjCurrentParticipants = $eventObj ? $eventObj->getParticipants() : [];

        if ($this->isJoined($eventObj, $userRewards)) {
            $redirectTo = $this->container->get('router')
                ->generate('EVENT', $eventObj->getRouterParams(), UrlGeneratorInterface::ABSOLUTE_URL);

            $this->session->getFlashBag()->add('notice', 'Failed Join Event Because You are already joined.');

            $this->setRedirectTo($redirectTo);
            return;
        }

        $quota = $eventObj ? $eventObj->getQuota() : 0;

        $existingParticipants = count($eventObjCurrentParticipants);

        $isReachedQuota = $quota - $existingParticipants;

        if ($isReachedQuota === 0) {
            $redirectTo = $this->container->get('router')
                ->generate('EVENT', $eventObj->getRouterParams(), UrlGeneratorInterface::ABSOLUTE_URL);

            $this->session->getFlashBag()->add('notice', 'Failed Join Event, quota exceed .');

            $this->setRedirectTo($redirectTo);
            return;
        }

        // @todo set eventObj eventParticipant.
        $eventObjCurrentParticipants[] = $checkExistingEventParticipant;
        $eventObj->setParticipants($eventObjCurrentParticipants);
        try {
            $eventObj->save([
                'versionNote' => sprintf('USER %d JOIN EVENT', $userRewards->getId())
            ]);

            $message = ($eventObj && $customSms = $eventObj->getCustomSms()) ?
                CustomSmsHelper::reformat($customSms, $eventObj, $userRewards) :
                sprintf('Terimakasih %s sudah berpartisipasi mengikuti event %s',
                    $userRewards->getGenderFriendlyName(),
                    $eventObj->getTitle()
                );

            $this->lixus->sendSms($userRewards->getPhone(), $message);

            $redirectTo = $this->container->get('router')
                ->generate('EVENT_THANKYOU', $eventObj->getRouterParams(), UrlGeneratorInterface::ABSOLUTE_URL);

            $this->setRedirectTo($redirectTo);
            return;

        } catch (\Exception $e) {

        }
    }

    public function isJoined($eventObj, $userRewards)
    {
        if (!$eventObj instanceof Event) {
            return false;
        }

        if (!$userRewards instanceof Customer) {
            return false;
        }

        /** @var EventParticipant $checkExistingEventParticipant */
        $checkExistingEventParticipant = EventParticipant::getByUserId($userRewards->getId(), 1);
        $eventObjCurrentParticipants = $eventObj ? $eventObj->getParticipants() : [];
        return in_array($checkExistingEventParticipant, $eventObjCurrentParticipants, false);
    }

    /**
     * @return int
     */
    public function getChangePageCounter(): int
    {
        $this->changePageCounter =  $this->session->get('changePageCounter', 0);
        return $this->changePageCounter;
    }

    /**
     * @param int $changePageCounter
     */
    public function setChangePageCounter(int $changePageCounter): void
    {
        $changePageCounter = $this->session->set('changePageCounter', $changePageCounter);
        $this->changePageCounter = $changePageCounter;
    }

    public function clearSession()
    {
        $this->session->remove('changePageCounter');
        $this->session->remove('redirectTo');
        $this->session->remove('eventObj');
    }
}
