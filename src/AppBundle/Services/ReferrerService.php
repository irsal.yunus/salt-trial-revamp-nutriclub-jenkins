<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ReferrerService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 06/08/20
 * @time 16.31
 *
 */

namespace AppBundle\Services;

use AppBundle\Helper\GeneralHelper;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

class ReferrerService
{
    /** @var RequestStack */
    private $requestStack;

    /** @var RouterInterface */
    private $router;

    /** @var SessionInterface $session */
    private $session;

    public function __construct(RequestStack $requestStack, RouterInterface $router, SessionInterface $session)
    {
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->session = $session;
    }

    public function getReferrer() : string
    {
        $request = $this->requestStack->getMasterRequest();

        if (null === $request)
        {
            return '/';
        }

        $uri = (string)$request->headers->get('referer');

        $referrer = GeneralHelper::isValidRefererUrl($uri) ? $uri : $this->router->generate('document_1');
        $this->session->set('globalReferrer', $referrer);

        return $referrer;
    }
}
