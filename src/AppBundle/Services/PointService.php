<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource PointService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 01/09/20
 * @time 15.44
 *
 */

namespace AppBundle\Services;

use AppBundle\Api\v1\Loyalty\Point;
use AppBundle\Model\DataObject\Article;
use AppBundle\Model\DataObject\Customer;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PointService
{
    public const ACTION_CODE_READ_ARTICLE = 'ReadArticle';

    public $actionCodeTools = [
        'FinishOSA1' => 'OSA 1',
        'FinishOSA2' => 'OSA 2',
        'FinishMitosKehamilan' => 'Mitos Kehamilan',
        'FinishARS' => 'Allergy Risk Screener',
        'FinishASC' => 'Allergy Symptom Checker',
        'FinishPRS' => 'Preterm Risk Screener',
        'FinishMPT' => 'My Pregnancy Today',
        'FinishBCC' => 'Baby Cost Calculator',
    ];

    /** @var Point $point */
    private $point;

    /** @var SessionInterface $session */
    private $session;

    private $request;

    /** @var ElasticSearchPointService $elasticSearchPointService */
    private $elasticSearchPointService;

    public function __construct(
        SessionInterface $session,
        RequestStack $requestStack,
        ElasticSearchPointService $elasticSearchPointService
    ) {
        $this->point = new Point();
        $this->session = $session;
        $this->request = $requestStack->getCurrentRequest();
        $this->elasticSearchPointService = $elasticSearchPointService;
    }

    public function article()
    {
        $accessToken = $this->session->get('AccessToken');
        if (!$accessToken) {
            return [
                'StatusCode' => '01',
                'StatusMessage' => 'Token Not Found'
            ];
        }

        /** @var Customer $userProfile */
        $userProfile = $this->session->get('UserProfile', null);

        $idAsToken = $this->request->get('token');

        $article = Article::getById($idAsToken, 1);
        if (!$article) {
            return [
                'StatusCode' => '09',
                'StatusMessage' => 'Article Not Found'
            ];
        }

        $checkAndCreateEs = $this->elasticSearchPointService->checkAndCreate($userProfile->getId(), $article->getId());

        if (!$checkAndCreateEs) {
            return [
                'StatusCode' => '10',
                'StatusMessage' => 'Already added'
            ];
        }

        return $this->point->addPointByActionCode(
            $accessToken,
            self::ACTION_CODE_READ_ARTICLE,
            $article->getTitle()
        );
    }

    public function tools()
    {
        $accessToken = $this->session->get('AccessToken') ?: $this->request->get('token');
        if (!$accessToken) {
            return [
                'StatusCode' => '01',
                'StatusMessage' => 'Token Not Found'
            ];
        }

        $actionCode = $this->request->get('code');

        if (!$actionCode) {
            return [
                'StatusCode' => '02',
                'StatusMessage' => 'Action code not found.'
            ];
        }

        if (!$this->actionCodeTools[$actionCode]) {
            return [
                'StatusCode' => '03',
                'StatusMessage' => 'Action code invalid.'
            ];
        }

        return $this->point->addPointByActionCode(
            $accessToken,
            $actionCode,
            $this->actionCodeTools[$actionCode]
        );
    }
}
