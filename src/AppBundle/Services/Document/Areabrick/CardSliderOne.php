<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 03/04/2020
 * Time: 13:01
 */

namespace AppBundle\Services\Document\Areabrick;

use AppBundle\Services\Document\AbstractServiceDocumentAreabrick;
use AppBundle\Model\DataObject\Stage;
use AppBundle\Model\Document\Areabrick\CardSliderOne as CardSliderOneModel;
use Pimcore\Model\Document\Tag\{Block, Relation};

class CardSliderOne extends AbstractServiceDocumentAreabrick
{
    private $documentTags = [];

    private $reArrange = [];

    private $reArrangeAsExcept = [];

    private $reArrangeWithoutExcept = true;

    private $exceptIds = [];

    private $onlyIds = [];

    public function getEditModeModel(): array
    {
        $this->documentTags = [];
        $this->documentTags[0] = $this->getDocumentTag('input', 'card-slider-one-title', []);


        return $this->documentTags;
    }

    public function getViewModeModel()
    {
        $model = new CardSliderOneModel();
        $model->setTitle($this->documentTags[0]);

        return $model;
    }

    private function getFilteredStage()
    {
        $stages = new Stage\Listing();

        $this->renderFirst();
        $this->except();
        if (count($this->exceptIds) > 0) {
            $stages
                ->addConditionParam(
                    'oo_id NOT IN('. implode(',', $this->exceptIds) .')',
                    null,
                    'AND'
                );
            $this->reArrangeWithoutExcept = false;
        }

        if ($this->reArrangeWithoutExcept && $this->reArrangeAsExcept) {
            $stages->addConditionParam(
                'oo_id NOT IN('. implode(',', $this->reArrangeAsExcept) .')',
                null,
                'AND'
            );
        }
        $this->only();

        if (count($this->onlyIds) > 0) {
            $stages->
            addConditionParam(
                'oo_id IN('. implode(',', $this->onlyIds) .')',
                null,
                'AND'
            );
        }

        $stages->setOrderKey('orderIndex');
        $stages->setOrder('asc');
        $stages->load();

        $stages = array_merge($this->reArrange, $stages->getObjects());

        return $stages;
    }

    private function renderFirst()
    {
        $counter = 0;
        /** @var Block $renderFirstBlock */
        $renderFirstBlock = $this->getDocumentTag('block', 'tool-list-render-first-block', []);

        if ($renderFirstBlock->isEmpty()) {
            return;
        }

        while ($renderFirstBlock->loop()) {
            /** @var Relation $relationObject */
            $relationObject = $this
                ->getDocumentTag('relation', 'tools-list-render-first-' . $counter);

            if ($relationObject->isEmpty()) {
                continue;
            }

            /** @var Stage $stageObject */
            $stageObject = $relationObject->getElement();
            $this->reArrange[] = $stageObject;
            $this->reArrangeAsExcept[] = $stageObject->getId();

            $counter++;
        }
    }

    private function except()
    {
        $counter = 0;
        /** @var Block $exceptBlock */
        $exceptBlock = $this->getDocumentTag('block', 'card-slider-one-block', []);
        $this->documentTags[2] = $exceptBlock;

        if ($exceptBlock->isEmpty()) {
            return;
        }

        while ($exceptBlock->loop()) {
            /** @var Relation $relationObject */
            $relationObject = $this
                ->getDocumentTag('relation', 'card-slider-one-stage-to-except-' . $counter, []);
            $this->documentTags[2][] = $relationObject;

            if ($relationObject->isEmpty()) {
                continue;
            }

            /** @var Stage $stageObject */
            $stageObject = $relationObject->getElement();
            $this->exceptIds[] = $stageObject->getId();

            $counter++;
        }

        $this->exceptIds = array_unique(array_merge($this->exceptIds, $this->reArrangeAsExcept));
    }

    private function only()
    {
        $counter = 0;
        /** @var Block $onlyBlock */
        $onlyBlock = $this->getDocumentTag('block', 'card-slider-one-block-only', []);
        $this->documentTags[3] = $onlyBlock;

        if ($onlyBlock->isEmpty()) {
            return;
        }

        while ($onlyBlock->loop()) {
            /** @var Relation $relationObject */
            $relationObject = $this
                ->getDocumentTag('relation', 'card-slider-one-stage-to-only-' . $counter, []);
            $this->documentTags[3][] = $relationObject;

            if ($relationObject->isEmpty()) {
                continue;
            }

            /** @var Stage $stageObject */
            $stageObject = $relationObject->getElement();
            $this->onlyIds[] = $stageObject->getId();

            $counter++;
        }
    }
}
