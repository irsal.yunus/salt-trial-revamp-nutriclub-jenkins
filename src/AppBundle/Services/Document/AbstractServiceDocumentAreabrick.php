<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 01/04/2020
 * Time: 10:37
 */

namespace AppBundle\Services\Document;

use Pimcore\Model\Document\ {PageSnippet, Tag};
use Symfony\Component\DependencyInjection\ {ContainerInterface};
use Pimcore\Templating\Renderer\TagRenderer;

abstract class AbstractServiceDocumentAreabrick implements ServiceDocumentAreabrickInterface
{
    /** @var ContainerInterface $container */
    private $container;

    /** @var PageSnippet $document */
    protected $document;

    /** @var string $prefixName */
    private $prefixName;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param PageSnippet $document
     */
    public function setDocument(PageSnippet $document) : void
    {
        $this->document = $document;
    }

    /**
     * @return PageSnippet
     */
    public function getDocument(): PageSnippet
    {
        return $this->document;
    }

    /**
     * @return string
     */
    public function getPrefixName(): string
    {
        return $this->prefixName;
    }

    /**
     * @param string $prefixName
     */
    public function setPrefixName(string $prefixName): void
    {
        $this->prefixName = $prefixName;
    }

    /**
     * @param string $type
     * @param string $inputName
     * @param array $options
     *
     * @return Tag|null
     */
    protected function getDocumentTag($type, $inputName, array $options = [])
    {
        /** @var TagRenderer $tagRenderer */
        $tagRenderer = $this->container->get('pimcore.templating.tag_renderer');

        $prefixName = $this->prefixName ?? '';

        return $tagRenderer->getTag($this->getDocument(), $type, $prefixName . $inputName, $options);
    }
}
