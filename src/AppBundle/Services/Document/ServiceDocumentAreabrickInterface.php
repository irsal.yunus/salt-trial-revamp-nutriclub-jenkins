<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 01/04/2020
 * Time: 10:45
 */

namespace AppBundle\Services\Document;

use Pimcore\Model\Document\PageSnippet;

interface ServiceDocumentAreabrickInterface
{
    public function setDocument(PageSnippet $document): void;

    public function getDocument(): PageSnippet;

    public function getEditModeModel(): array;

    public function getViewModeModel();
}
