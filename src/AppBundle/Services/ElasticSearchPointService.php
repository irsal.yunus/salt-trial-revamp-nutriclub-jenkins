<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ElasticSearchPointService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 02/09/20
 * @time 20.30
 *
 */

namespace AppBundle\Services;

use Carbon\Carbon;
use Elasticsearch\ClientBuilder;
use GuzzleHttp\Client;
use Pimcore\Log\Simple;

class ElasticSearchPointService
{
    public const ES_INDEX = 'point';

    public const ES_INDEX_TYPE_ARTICLE = 'article';

    /** @var \Elasticsearch\Client $elasticsearchClient */
    private $elasticsearchClient;

    public function __construct()
    {
        $elasticsearchClient = ClientBuilder::create();

        $elasticsearchClient->setHosts(
            [
                'host' => env('ES_URL_INTERNAL')
            ]
        );

        $this->elasticsearchClient = $elasticsearchClient->build();
    }

    public function setPointIndex()
    {
        $params = [
            'index' => self::ES_INDEX
        ];

        try {
            return $this->elasticsearchClient->indices()->create($params);
        } catch (\Exception $exception) {
            Simple::log(
                'ES_POINT_SERVICE_CREATE_INDEX' . Carbon::now()->format('dmY'),
                $exception->getMessage()
            );
        }
    }

    public function createDocumentArticle($bodyData)
    {
        $params = [
            'index' => self::ES_INDEX,
            'type' => self::ES_INDEX_TYPE_ARTICLE,
            'body' => $bodyData
        ];

        try {
            return $this->elasticsearchClient->index($params);
        } catch (\Exception $exception) {
            Simple::log(
                'ES_POINT_SERVICE_CREATE_INDEX_DOCUMENT' . Carbon::now()->format('dmY'),
                $exception->getMessage()
            );
        }
    }

    public function updateDocumentArticle($id, $bodyData)
    {
        $params = [
            'index' => self::ES_INDEX,
            'type' => self::ES_INDEX_TYPE_ARTICLE,
            'id' => $id,
            'body' => $bodyData
        ];

        try {
            return $this->elasticsearchClient->index($params);
        } catch (\Exception $exception) {
            Simple::log(
                'ES_POINT_SERVICE_UPDATE_INDEX_DOCUMENT' . Carbon::now()->format('dmY'),
                $exception->getMessage()
            );
        }
    }

    public function searchDocument($params)
    {
        try {
            return $this->elasticsearchClient->search($params);
        } catch (\Exception $exception) {
            Simple::log(
                'ES_POINT_SERVICE_SEARCH_DOCUMENT' . Carbon::now()->format('dmY'),
                $exception->getMessage()
            );
        }
    }

    public function checkAndCreate($memberId, $articleId)
    {
        $currentTime = Carbon::now();

        $paramDocument = [
            'memberId' => $memberId,
            'articleId' => $articleId,
            'read_at' => $currentTime->timestamp,
            'allowed_to_read_at' => $currentTime->addHours(24)->timestamp,
            'closed' => false
        ];

        $paramSearch = [
            'index' => 'point',
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'match' => [
                                    'memberId' => $memberId
                                ]
                            ],
                            [
                                'match' => [
                                    'articleId' => $articleId
                                ]
                            ],
                            [
                                'match' => [
                                    'closed' => false
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $isExist = $this->searchDocument($paramSearch);
        $hits = $isExist['hits']['hits'] ?? null;
        if (!$hits) {
            $this->createDocumentArticle($paramDocument);

            return true;
        }

        $source = $hits[0]['_source'] ?? null;
        if (!$source) {
            return false;
        }

        $sourceId = $hits[0]['_id'];

        $allowedToReadAt = $source['allowed_to_read_at'] ?? null;
        if (!$allowedToReadAt) {
            return false;
        }

        if ($currentTime->addHours(-24)->timestamp >= $allowedToReadAt) {
            $source['closed'] = true;

            $updateDocument = $this->updateDocumentArticle($sourceId, $source);

            $this->createDocumentArticle($paramDocument);

            return true;
        }

        return false;
    }
}
