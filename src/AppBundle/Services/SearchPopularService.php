<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/03/2020
 * Time: 19:27
 */

namespace AppBundle\Services;

use AppBundle\Api\v1\ElasticSearch\Popular\Popular;

class SearchPopularService
{
    private $popular;

    public function __construct()
    {
        $this->popular = new Popular();
    }

    public function getPopularSearch(array $params)
    {
        return $this->popular->index($params);
    }
}
