<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 05/03/2020
 * Time: 19:59
 */

namespace AppBundle\Services;

use Pimcore\Log\Simple;
use Pimcore\Model\DataObject;
use AppBundle\Model\ElasticSearch\Article as ElasticSearchModelArticle;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

class ElasticSearchQueryService
{
    /** @var Client $client */
    private $client;

    private $baseUrl;

    public function __construct()
    {
        $this->client = new Client([
            'verify' => false,
            'debug' => false,
        ]);

        $this->baseUrl = getenv('ES_URL_V2', null);
    }

    public function getArticleFromElasticSearch($query, $type = [], $size = 100, $from = 0)
    {
        $articles = null;
        try {
            $requestSearch = $this->client->request('GET', $this->baseUrl . '/find', [
                'query' => [
                    'word' => $query,
                    'size' => $size,
                    'from' => $from,
                    'type' => $type
                ]
            ]);

            if ($requestSearch->getStatusCode() === Response::HTTP_OK) {
                $body = $requestSearch->getBody()->getContents();

                if (!is_json($body)) {
                    return $articles;
                }

                $decode = json_decode($body, true);
                $statusCode = $decode['status']['code'] ?? null;
                if ($statusCode !== '00') {
                    return [];
                }

                $datas = $decode['data'] ?? [];

                $totalData = $datas['total']['data'] ?? 0;
                $dataArticles = $datas['data'] ?? [];

                $countDataArticles = count($dataArticles);

                $newElasticSearchArticleModel = new ElasticSearchModelArticle();

                $regexFindPrefix = '/(article)/';
                foreach ($dataArticles as $dataArticle) {
                    $firstKey = array_key_first($dataArticle);
                    $article = $dataArticle[$firstKey];

                    $objects = $this->objectCollector($article['data'] ?? []);

                    $replacePrefix = preg_replace($regexFindPrefix, '', $firstKey);
                    if (!$replacePrefix) {
                        $setter = 'set' . ucfirst($firstKey);

                        $setterTotalPage = $setter . 'TotalPage';
                        $objectTotalPage = $article['total']['page'] ?? 0;

                        $setterTotalData = $setter . 'TotalData';
                        $objectTotalData = $article['total']['data'] ?? 0;

                        $setterObject = method_exists($newElasticSearchArticleModel, $setter) ?
                            $newElasticSearchArticleModel->$setter($objects) : null;

                        $setterObjectTotalPage = method_exists($newElasticSearchArticleModel, $setterTotalPage) ?
                            $newElasticSearchArticleModel->$setterTotalPage($objectTotalPage) : null;

                        $setterObjectTotalData = method_exists($newElasticSearchArticleModel, $setterTotalData) ?
                            $newElasticSearchArticleModel->$setterTotalData($objectTotalData) : null;
                    }

                    if ($replacePrefix) {
                        $setter = 'set' . ucfirst($replacePrefix);

                        $setterTotalPage = $setter . 'TotalPage';
                        $objectTotalPage = $article['total']['page'] ?? 0;

                        $setterTotalData = $setter . 'TotalData';
                        $objectTotalData = $article['total']['data'] ?? 0;

                        $setterObject = method_exists($newElasticSearchArticleModel, $setter) ?
                            $newElasticSearchArticleModel->$setter($objects) : null;

                        $setterObjectTotalPage = method_exists($newElasticSearchArticleModel, $setterTotalPage) ?
                            $newElasticSearchArticleModel->$setterTotalPage($objectTotalPage) : null;

                        $setterObjectTotalData = method_exists($newElasticSearchArticleModel, $setterTotalData) ?
                            $newElasticSearchArticleModel->$setterTotalData($objectTotalData) : null;
                    }
                }
                $newElasticSearchArticleModel->setTotalData($totalData);

                return $newElasticSearchArticleModel;
            }
        } catch (\Exception $exception) {
            Simple::log('ELASTICSEARCH_QUERY_SERVICE', $exception->getMessage());
            return new ElasticSearchModelArticle();
        }
    }

    private function objectCollector(array $datas)
    {
        if (!$datas) {
            return [];
        }

        $ids = [];
        foreach ($datas as $data) {
            $id = (int) $data['id'];

            $ids[] = $id;
        }

        $dataObjects = new DataObject\Listing();
        $dataObjects->setCondition('o_id IN('. implode(',', $ids) .')');

        // rearrange.
        foreach ($dataObjects->getObjects() as $key => $object) {
            $id = $object->getId();

            $searchIndex = array_search($id, $ids, false);

            $ids[$searchIndex] = $object;
        }

        // override objects. after rearrange.
        $dataObjects->setObjects($ids);

        return $dataObjects ? $dataObjects->getObjects() : [];
    }
}
