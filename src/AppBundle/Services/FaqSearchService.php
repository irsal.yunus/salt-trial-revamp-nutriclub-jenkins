<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource FaqSearchService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 20.03
 *
 */

namespace AppBundle\Services;

use CarelineBundle\Model\DataObject\CarelineFaq;
use Pimcore\Db;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\{Request, RequestStack};

class FaqSearchService
{
    /** @var Request $request */
    private $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getFaqs($query)
    {
        $carelineFaqs = new CarelineFaq\Listing();
        $carelineFaqs->setCondition('question LIKE ?', ['%' . $query . '%']);

        $carelineFaqs->setOrderKey('oo_id');
        $carelineFaqs->setOrder('desc');

        return $carelineFaqs->getObjects();
    }

    public function getSubFaqs($query = null)
    {
        $query = $query ?? $this->request->get('q');
        $nextPage = (int)$this->request->get('nextPage', 1);

        $query = htmlspecialchars($query, ENT_QUOTES);

        $carelineSubFaqs = Db::getConnection()
            ->select()
            ->from('object_collection_CarelineSubFaq_30')
            ->where("question LIKE '%". $query ."%'")
            ->execute()
            ->fetchAll();

        $countSubFaqs = count($carelineSubFaqs);
        $offset = 0;
        $limit = 8;
        $totalPage = (int)ceil($countSubFaqs / $limit);

        $newCarelineSubFaqs = [];
        $objectIds = [];

        if ($nextPage > 0) {
            if ($nextPage > 1) {
                $offset = ($nextPage - 1) * $limit;
            }
            if ($nextPage === 1) {
                $offset = 0;
            }
            $carelineSubFaqs = Db::getConnection()
                ->select()
                ->from('object_collection_CarelineSubFaq_30')
                ->where("question LIKE '%". $query ."%'")
                ->limit($limit, $offset)
                ->execute()
                ->fetchAll();
        }

        foreach ($carelineSubFaqs as $key => $carelineSubFaq) {
            $key++;
            $object = DataObject::getById($carelineSubFaq['o_id'] ?? 0, 1);
            $carelineSubFaq['object'] = $object ?? null;
            $carelineSubFaq['keyHelper'] = $offset > 0 ? ($offset + $key) : null;

            $newCarelineSubFaqs[] = $carelineSubFaq;
        }

        $data = [
            'offset' => $offset,
            'limit' => $limit,
            'currentPage' => $nextPage,
            'totalPage' => $totalPage,
            'totalData' => $countSubFaqs,
            'data' => $newCarelineSubFaqs,
        ];

        return $data;
    }
}
