<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource OtpService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/09/20
 * @time 13.27
 *
 */

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;
use AppBundle\Api\v1\Loyalty\OtpMember as OtpMemberLoyalty;
use AppBundle\Api\v1\Rewards\OtpMember as OtpMemberRewards;

class OtpService
{
    private $request;

    /** @var OtpMemberLoyalty $otpMemberLoyalty */
    private $otpMemberLoyalty;

    /** @var OtpMemberRewards $otpMemberRewards */
    private $otpMemberRewards;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function requestOtp()
    {
        $phone = $this->request->query->get('phone');
        $oldPhone = $this->request->query->get('old_phone');

        if (env('RRR_LIVE')) {
            $otpMember = new OtpMemberLoyalty();
        }

        if (!env('RRR_LIVE')) {
            $otpMember = new OtpMemberRewards();
        }

        return $otpMember->requestMiscallMemberRegister($phone, $oldPhone);
    }

    public function requestOtpValidation()
    {
        $phone = $this->request->query->get('phone');
        $token = $this->request->query->get('token');

        if (env('RRR_LIVE')) {
            $otpMember = new OtpMemberLoyalty();
        }

        if (!env('RRR_LIVE')) {
            $otpMember = new OtpMemberRewards();
        }

        return $otpMember->verifyMisscallMemberRegister($phone, $token);
    }
}
