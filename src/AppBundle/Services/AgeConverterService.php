<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/03/2020
 * Time: 17:29
 */

namespace AppBundle\Services;

use Pimcore\Model\DataObject\Data\QuantityValue;
use Pimcore\Model\DataObject\QuantityValue\Unit;
use Pimcore\Model\DataObject\QuantityValue\UnitConversionService;
use Pimcore\Model\DataObject\StageAge;

class AgeConverterService
{
    /** @var UnitConversionService $unitConversionService */
    private $unitConversionService;

    public function __construct(UnitConversionService $unitConversionService)
    {
        $this->unitConversionService = $unitConversionService;
    }

    public function convert($age)
    {
        $converter = $this->unitConversionService;
        $ageValue = [];
        $ageUnit = [];

        $originalValue = new QuantityValue($age, Unit::getByAbbreviation('week')->getId());
        $stageAgeWeekListing = new StageAge\Listing();
        $stageAgeWeekListing->setCondition('age__value = ? AND age__unit = ?', [
            $originalValue->getValue(),
            $originalValue->getUnitId()
        ]);
        $stageAgeWeek = $stageAgeWeekListing->getObjects();
        if ($stageAgeWeek) { return $stageAgeWeekListing; }

        $convertedValueMonth = $converter->convert($originalValue, Unit::getByAbbreviation('month'));
        $stageAgeMonthListing = new StageAge\Listing();
        $stageAgeMonthListing->setCondition('age__value = ? AND age__unit = ?', [
            (int) ceil($convertedValueMonth->getValue()),
            $convertedValueMonth->getUnitId()
        ]);
        $stageAgeMonth = $stageAgeMonthListing->getObjects();
        if ($stageAgeMonth) { return $stageAgeMonthListing; }

        $convertedValueYear = $converter->convert($originalValue, Unit::getByAbbreviation('year'));
        $stageAgeYearListing = new StageAge\Listing();
        $stageAgeYearListing->setCondition('age__value = ? AND age__unit = ?', [
            (int) ceil($convertedValueYear->getValue()),
            $convertedValueYear->getUnitId()
        ]);
        $stageAgeYear = $stageAgeYearListing->getObjects();
        if ($stageAgeYear) { return $stageAgeYearListing; }

        return null;
    }
}
