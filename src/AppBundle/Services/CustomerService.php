<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/01/2020
 * Time: 21:45
 */

namespace AppBundle\Services;

use AppBundle\Model\DataObject\Customer;
use Symfony\Component\Security\Core\User\UserInterface;

class CustomerService
{
    /** @var Customer $customCustomer */
    private $customCustomer;

    public function __construct()
    {
        $this->customCustomer = new Customer();
    }

    public function setCustomCustomer(UserInterface $customer)
    {
        $this->customCustomer = $customer;
    }

    public function getCustomCustomer()
    {
        return $this->customCustomer ?? null;
    }
}
