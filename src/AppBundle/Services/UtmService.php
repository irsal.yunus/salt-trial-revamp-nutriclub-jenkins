<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 25/05/2020
 * Time: 23:33
 */

namespace AppBundle\Services;

use AppBundle\EventListener\UtmListener;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class UtmService
{
    /** @var Request */
    private $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getUtms()
    {
        $utms = [];
        $cookies = $this->request->cookies->all();

        if (!$cookies) {
            return $utms;
        }

        $currentKeys = UtmListener::UTMS;

        if (!$currentKeys) {
            return $utms;
        }

        foreach ($currentKeys as $currentKey) {
            $utms[$currentKey] = $cookies[$currentKey] ?? null;
        }

        return $utms;
    }
}
