<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 08/06/2020
 * Time: 10:01
 */

namespace AppBundle\Services;

use AppBundle\EventListener\ChannelListener;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ChannelService
{
    private $request;

    private $cookies;

    private $redirect;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->cookies = $this->request->cookies;
    }

    public function getChannel()
    {
        $channel = [];
        $cookies = $this->request->cookies;

        if (!$cookies->has(ChannelListener::COOKIE_NAME)) {
            return $channel;
        }

        $channel['ChannelCode'] = $cookies->get(ChannelListener::COOKIE_NAME);
        return $channel;
    }

    /**
     * @return mixed
     */
    public function getRedirect()
    {
        return $this->cookies->get('refererMedia');
    }

    /**
     * @param mixed $redirect
     */
    public function setRedirect($redirect): void
    {
        $dotNetDomain = env('WILDCARD_DOMAIN_FOR_DOT_NET', null);
        $expiredCookie = Carbon::now()->addMinutes(15)->timestamp;

        setrawcookie(
            'refererMedia',
            $redirect,
            $expiredCookie,
            '/',
            $dotNetDomain
        );
    }
}
