<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 08/06/2020
 * Time: 15:07
 */

namespace AppBundle\Services;

use AppBundle\EventListener\LoginRefererListener;
use AppBundle\Helper\GeneralHelper;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LoginRefererService
{
    private $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack;
    }

    public function redirectToReferer()
    {
        $loginRefererSessionName = LoginRefererListener::LOGIN_REFERER_SESSION;

        $cookies = $this->request->getCurrentRequest()->cookies;
        $loginRefererSession = GeneralHelper::isValidRefererUrl($cookies->get($loginRefererSessionName));

        return  $loginRefererSession;
    }
}
