<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/07/2020
 * Time: 10:48
 */

namespace AppBundle\Services;

use AppBundle\Model\DataObject\ProductGroup;
use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Pimcore\Model\Document\Snippet;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductGroupService
{
    private $request;

    private $productGroupSlug;

    private $breadcrumbHelperService;

    private $freeSampleService;

    public function __construct(
        RequestStack $requestStack,
        BreadcrumbHelperService $breadcrumbHelperService,
        FreeSampleService $freeSampleService
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->breadcrumbHelperService = $breadcrumbHelperService;
        $this->freeSampleService = $freeSampleService;
    }

    public function getProductGroup()
    {
        $productGroup = $this->isProductGroupExist();
        $isEditMode = $this->request->get('_editmode');

        if (!$productGroup && !$isEditMode) {
            throw new NotFoundHttpException('Product Group not found');
        }

        if ($isEditMode) {
            $productGroupLists = new ProductGroup\Listing();
            $productGroupLists->setLimit(1);
            $productGroupLists->load();

            $productGroup = $productGroupLists->getCount() > 0 ? $productGroupLists->getObjects()[0] : null;
        }

        $route = $this->request->get('_route');
        if ($route === 'PRODUCT_GROUP') {
            $productGroupPage = WebsiteSetting::getByName('PRODUCT_GROUP_PAGE') ?
                WebsiteSetting::getByName('PRODUCT_GROUP_PAGE')->getData() : null;

            $this->breadcrumbHelperService->enrichProductGroupPage($productGroup, $productGroupPage);
        }

        $this->freeSampleService->channelProductGroup($productGroup);

        return $productGroup;
    }

    public function isProductGroupExist()
    {
        $this->productGroupSlug = $this->request->get('groupSlug');

        return ProductGroup::getBySlug($this->productGroupSlug, 1);
    }
}
