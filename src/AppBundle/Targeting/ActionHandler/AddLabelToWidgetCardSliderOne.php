<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/02/2020
 * Time: 17:19
 */

namespace AppBundle\Targeting\ActionHandler;

use AppBundle\Model\Widget\CardSliderOneActionHandlerModel;
use AppBundle\Targeting\DataProvider\CardSliderOneLabel;
use Pimcore\Model\Tool\Targeting\Rule;
use Pimcore\Targeting\ActionHandler\ActionHandlerInterface;
use Pimcore\Targeting\DataProviderDependentInterface;
use Pimcore\Targeting\Model\VisitorInfo;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AddLabelToWidgetCardSliderOne implements ActionHandlerInterface, DataProviderDependentInterface
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function apply(VisitorInfo $visitorInfo, array $action, Rule $rule = null)
    {
        $labelTitle = $action['labelTitle'] ?? null;
        $stageSlug = $action['stageSlug'] ?? null;

        $model = new CardSliderOneActionHandlerModel();
        $data = [
            'labelTitle' => $labelTitle,
            'stageSlug' => $stageSlug
        ];
        $model->setValues($data);

        $this->session->set(CardSliderOneLabel::PROVIDER_KEY, $model);
        $visitorInfo->set(CardSliderOneLabel::PROVIDER_KEY, $model);
    }

    public function getDataProviderKeys(): array
    {
        return [CardSliderOneLabel::PROVIDER_KEY];
    }
}
