<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ForcePersona.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 03/09/20
 * @time 12.30
 *
 */

namespace AppBundle\Targeting\ActionHandler;

use Pimcore\Model\Tool\Targeting\Rule;
use Pimcore\Model\Tool\Targeting\TargetGroup;
use Pimcore\Targeting\ActionHandler\AssignTargetGroup;
use Pimcore\Targeting\ConditionMatcherInterface;
use Pimcore\Targeting\Model\VisitorInfo;
use Pimcore\Targeting\Storage\TargetingStorageInterface;

class ForceAssignTargetGroup extends AssignTargetGroup
{
    /**
     * @var ConditionMatcherInterface
     */
    private $conditionMatcher;

    /**
     * @var TargetingStorageInterface
     */
    private $storage;

    public function __construct(
        ConditionMatcherInterface $conditionMatcher,
        TargetingStorageInterface $storage
    ) {
        parent::__construct($conditionMatcher, $storage);

        $this->conditionMatcher = $conditionMatcher;
        $this->storage = $storage;
    }

    public function apply(VisitorInfo $visitorInfo, array $action, Rule $rule = null)
    {
        $targetGroupId = $action['targetGroup'] ?? null;
        if (!$targetGroupId) {
            return;
        }

        if ($targetGroupId instanceof TargetGroup) {
            $targetGroup = $targetGroupId;
        } else {
            $targetGroup = TargetGroup::getById($targetGroupId);
        }

        if (!$targetGroup || !$targetGroup->getActive()) {
            return;
        }

        $visitorInfo->assignTargetGroup($targetGroup, 9999999, true);
    }
}
