<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/02/2020
 * Time: 20:25
 */

namespace AppBundle\Targeting\ActionHandler;

use AppBundle\Helper\GeneralHelper;
use AppBundle\Model\DataObject\Customer;
use AppBundle\Targeting\DataProvider\UserRewardSession;
use Pimcore\Model\DataObject\StageAge;
use Pimcore\Model\Tool\Targeting\Rule;
use Pimcore\Targeting\ActionHandler\ActionHandlerInterface;
use Pimcore\Targeting\DataProviderDependentInterface;
use Pimcore\Targeting\Model\VisitorInfo;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AddContentToWidgetCardColumnOne implements ActionHandlerInterface, DataProviderDependentInterface
{
    /** @var SessionInterface $session */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function apply(VisitorInfo $visitorInfo, array $action, Rule $rule = null)
    {
        $sessionName = $visitorInfo->get(UserRewardSession::PROVIDER_KEY);
        /** @var Customer $customer */
        $customer = $this->session->get($sessionName);

        if (!$action['assignArticleCalendar']) {
            return;
        }

        $agePregnant = $customer->getAgePregnant();

        $articleCalendar = GeneralHelper::getArticleCalanderByAgePregnant($agePregnant);
        $visitorInfo->set('articleCalendar', $articleCalendar);
    }

    public function getDataProviderKeys(): array
    {
        return [UserRewardSession::PROVIDER_KEY];
    }
}
