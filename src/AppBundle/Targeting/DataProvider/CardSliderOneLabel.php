<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/02/2020
 * Time: 17:20
 */

namespace AppBundle\Targeting\DataProvider;

use Pimcore\Targeting\DataProvider\DataProviderInterface;
use Pimcore\Targeting\Model\VisitorInfo;

class CardSliderOneLabel implements DataProviderInterface
{
    const PROVIDER_KEY = 'card_slider_one_label';

    public function load(VisitorInfo $visitorInfo)
    {
        if ($visitorInfo->has(self::PROVIDER_KEY)) {
            return;
        }

        $visitorInfo->set(self::PROVIDER_KEY, "somethinghere");
    }
}
