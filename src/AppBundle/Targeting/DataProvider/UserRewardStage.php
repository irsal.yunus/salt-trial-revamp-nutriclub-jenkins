<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/02/2020
 * Time: 17:12
 */

namespace AppBundle\Targeting\DataProvider;

use Pimcore\Targeting\DataProvider\DataProviderInterface;
use Pimcore\Targeting\Model\VisitorInfo;

class UserRewardStage implements DataProviderInterface
{
    const PROVIDER_KEY = 'user_reward_stage';

    public function load(VisitorInfo $visitorInfo)
    {
        if ($visitorInfo->has(self::PROVIDER_KEY)) {
            return;
        }
    }
}
