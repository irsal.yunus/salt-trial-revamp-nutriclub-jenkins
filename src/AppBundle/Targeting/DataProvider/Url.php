<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/03/2020
 * Time: 22:07
 */

namespace AppBundle\Targeting\DataProvider;

use phpDocumentor\Reflection\Types\Self_;
use Pimcore\Targeting\DataProvider\DataProviderInterface;
use Pimcore\Targeting\Model\VisitorInfo;

class Url implements DataProviderInterface
{
    const PROVIDER_KEY = 'url_with_data_provider';

    public function load(VisitorInfo $visitorInfo)
    {
        if ($visitorInfo->has(self::PROVIDER_KEY)) {
            return;
        }
    }
}
