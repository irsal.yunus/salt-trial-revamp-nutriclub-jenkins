<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/03/2020
 * Time: 21:05
 */

namespace AppBundle\Targeting\DataProvider;

use AppBundle\Services\VisitedSamePagesCounter;
use Pimcore\Targeting\DataProvider\DataProviderInterface;
use Pimcore\Targeting\Model\VisitorInfo;


class VisitSamePagesFor implements DataProviderInterface
{
    const PROVIDER_KEY = 'visit_same_pages_for';

    /**
     * @var VisitedSamePagesCounter
     */
    private $service;

    public function __construct(VisitedSamePagesCounter $service)
    {
        $this->service = $service;
    }

    public function load(VisitorInfo $visitorInfo)
    {
        $visitorInfo->set(self::PROVIDER_KEY, $this->service);
    }
}
