<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 25/02/2020
 * Time: 22:48
 */

namespace AppBundle\Targeting\Condition;

use Pimcore\Model\Tool\Targeting\TargetGroup;
use Pimcore\Targeting\Condition\AbstractVariableCondition;
use Pimcore\Targeting\DataProviderDependentInterface;
use Pimcore\Targeting\Model\VisitorInfo;
use AppBundle\Targeting\DataProvider\TargetGroupCurrent as TargetGroupCurrentDataProvider;

class TargetGroupCurrent extends AbstractVariableCondition implements DataProviderDependentInterface
{
    /**
     * @var array $configs
     */
    private $configs;

    public function __construct(array $configs = [])
    {
        $this->configs = $configs;
    }

    public static function fromConfig(array $config)
    {
        $configs['targetGroup'] = $config['targetGroup'] ?? null;

        return new self($configs);
    }

    public function canMatch(): bool
    {
        return null !== $this->configs;
    }

    public function match(VisitorInfo $visitorInfo): bool
    {
        $sortedTargetGroupAssignments = $visitorInfo->getAssignedTargetGroups();
        $countAssignedTargetGroup = count($sortedTargetGroupAssignments);

        if ($countAssignedTargetGroup === 0) {
            return false;
        }

        /** @var TargetGroup $firstLevelOfAssigned */
        $firstLevelOfAssigned = $sortedTargetGroupAssignments[0] ?? null;

        if (!$firstLevelOfAssigned) {
            return false;
        }

        $configTargetGroup = $this->configs['targetGroup'] ?? null;

        if (!$configTargetGroup) {
            return false;
        }

        return $configTargetGroup === $firstLevelOfAssigned->getId();
    }

    public function getDataProviderKeys(): array
    {
        return [TargetGroupCurrentDataProvider::PROVIDER_KEY];
    }
}
