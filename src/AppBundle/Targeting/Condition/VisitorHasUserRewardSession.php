<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/02/2020
 * Time: 19:15
 */

namespace AppBundle\Targeting\Condition;

use AppBundle\Model\DataObject\Customer;
use AppBundle\Targeting\DataProvider\UserRewardSession;
use Pimcore\Targeting\Condition\AbstractVariableCondition;
use Pimcore\Targeting\DataProviderDependentInterface;
use Pimcore\Targeting\Model\VisitorInfo;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class VisitorHasUserRewardSession extends AbstractVariableCondition implements DataProviderDependentInterface
{
    /**
     * @var array $configs
     */
    private $configs;

    public function __construct(array $configs = [])
    {
        $this->configs = $configs;
    }

    public static function fromConfig(array $config)
    {
        $configs['userRewardSessionModel'] = $config['userRewardSessionModel'] ?? null;

        return new self($configs);
    }

    public function canMatch(): bool
    {
        return null !== $this->configs;
    }

    public function match(VisitorInfo $visitorInfo): bool
    {
        $session = new Session();
        $sessionName = $this->configs['userRewardSessionModel'];

        if (!$session->has($sessionName)) {
            return false;
        }

        if (($customer = $session->get($sessionName)) && !$customer instanceof Customer ) {
            return false;
        }

        $visitorInfo->set(UserRewardSession::PROVIDER_KEY, $sessionName);

        return true;
    }

    public function getDataProviderKeys(): array
    {
        return [
            UserRewardSession::PROVIDER_KEY
        ];
    }
}
