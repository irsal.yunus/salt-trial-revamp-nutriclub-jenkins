<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/02/2020
 * Time: 14:01
 */

namespace AppBundle\Targeting\Condition;

use AppBundle\Model\DataObject\Customer;
use AppBundle\Targeting\DataProvider\UserRewardSession;
use AppBundle\Targeting\DataProvider\UserRewardStage;
use Pimcore\Targeting\Condition\AbstractVariableCondition;
use Pimcore\Targeting\DataProviderDependentInterface;
use Pimcore\Targeting\Model\VisitorInfo;
use Symfony\Component\HttpFoundation\Session\Session;

class UserRewardSessionHasStage extends AbstractVariableCondition implements DataProviderDependentInterface
{
    /**
     * @var array $configs
     */
    private $configs;

    public function __construct(array $configs = [])
    {
        $this->configs = $configs;
    }

    public static function fromConfig(array $config)
    {
        $configs['stage'] = $config['stage'] ?? null;

        return new self($configs);
    }

    public function canMatch(): bool
    {
        return null !== $this->configs;
    }

    public function match(VisitorInfo $visitorInfo): bool
    {
        $session = new Session();
        $sessionName = $visitorInfo->get(UserRewardSession::PROVIDER_KEY);

        $stage = $this->configs['stage'];

        if (!$sessionName) {
            return false;
        }

        if (($customer = $session->get($sessionName)) && !$customer instanceof Customer) {
            return false;
        }

        return $stage === $customer->getStagesID();
    }

    public function getDataProviderKeys(): array
    {
        return [
            UserRewardStage::PROVIDER_KEY
        ];
    }
}
