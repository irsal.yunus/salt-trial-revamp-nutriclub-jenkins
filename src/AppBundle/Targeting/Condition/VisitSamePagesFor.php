<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/03/2020
 * Time: 21:02
 */

namespace AppBundle\Targeting\Condition;

use AppBundle\Event\TargetingEvents;
use AppBundle\Services\VisitedSamePagesCounter as VisitedSamePagesCounterService;
use AppBundle\Targeting\DataProvider\Url;
use Pimcore\Targeting\Condition\AbstractVariableCondition;
use Pimcore\Targeting\Condition\EventDispatchingConditionInterface;
use Pimcore\Targeting\DataProviderDependentInterface;
use Pimcore\Targeting\Model\VisitorInfo;
use AppBundle\Targeting\DataProvider\VisitSamePagesFor as VisitSamePagesForDataProvider;
use Ramsey\Uuid\Uuid;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class VisitSamePagesFor extends AbstractVariableCondition implements DataProviderDependentInterface, EventDispatchingConditionInterface
{
    /**
     * @var array $configs
     */
    private $configs;

    public function __construct(array $configs = [])
    {
        $this->configs = $configs;
    }

    public static function fromConfig(array $config)
    {
        $configs['uniqueid'] = $config['uniqueid'] ?? null;
        $configs['number'] = $config['number'] ?? null;

        return new self($configs);
    }

    public function canMatch(): bool
    {
        return null !== $this->configs;
    }

    public function match(VisitorInfo $visitorInfo): bool
    {
        if (!$visitorInfo->has(Url::PROVIDER_KEY)) {
            return false;
        }

        $urlWithProvider = $visitorInfo->get(Url::PROVIDER_KEY);
        if (!$urlWithProvider) {
            return false;
        }

        $storageKey = 'vspc_';
        $storageKey .= (string) $this->configs['uniqueid'];

        if (!$visitorInfo->has('vspf_storagekey') || !$visitorInfo->get('vspf_storagekey', null)) {
            $visitorInfo->set('vspf_storagekey', $storageKey);
        }

        /** @var VisitedSamePagesCounterService $counter */
        $counter = $visitorInfo->get(VisitSamePagesForDataProvider::PROVIDER_KEY);
        $count = $counter->getCount($visitorInfo);

        $numberVisit = (int) $this->configs['number'];
        $minimalVisitSamePages = $numberVisit > 0 ? ($numberVisit - 1) : $numberVisit;

        if ($count >= $minimalVisitSamePages) {
            return true;
        }

        return false;
    }

    public function getDataProviderKeys(): array
    {
        return [
            VisitSamePagesForDataProvider::PROVIDER_KEY
        ];
    }

    public function preMatch(VisitorInfo $visitorInfo, EventDispatcherInterface $eventDispatcher)
    {
        // nothing.
    }

    public function postMatch(VisitorInfo $visitorInfo, EventDispatcherInterface $eventDispatcher)
    {
        $eventDispatcher->dispatch(TargetingEvents::VISITED_SAME_PAGES_COUNT_MATCH);
    }
}
