<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 01/06/2020
 * Time: 14:50
 */

namespace AppBundle\Targeting\Condition;

use AppBundle\Model\DataObject\Customer;
use AppBundle\Targeting\DataProvider\UserRewardSession;
use Carbon\Carbon;
use Pimcore\Targeting\Condition\AbstractVariableCondition;
use Pimcore\Targeting\DataProviderDependentInterface;
use Pimcore\Targeting\Model\VisitorInfo;

class ChildBirthDay extends AbstractVariableCondition implements DataProviderDependentInterface
{
    /**
     * @var array $configs
     */
    private $configs;

    public function __construct(array $configs = [])
    {
        $this->configs = $configs;
    }

    public static function fromConfig(array $config)
    {
        $configs['birthday'] = $config['birthday'] ?? null;
        $configs['child'] = $config['child'] ?? null;

        return new self($configs);
    }

    public function canMatch(): bool
    {
        return null !== $this->configs;
    }

    public function match(VisitorInfo $visitorInfo): bool
    {
        $hasUserRewardSession = $visitorInfo->get(UserRewardSession::PROVIDER_KEY);

        if (!$hasUserRewardSession) {
            return false;
        }

        /** @var Customer $userRewards */
        $userRewards = $hasUserRewardSession;

        $session = $visitorInfo->getRequest()->getSession();
        $userProfileRaw = $session->get('UserProfileRaw');

        $childs = $userProfileRaw['Childs'];

        return $this->isChildBirthDay($childs);
    }

    public function getDataProviderKeys(): array
    {
        return [
            \AppBundle\Targeting\DataProvider\ChildBirthDay::PROVIDER_KEY
        ];
    }

    private function isChildBirthDay($childs)
    {
        if (!$childs) {
            return false;
        }

        $currentDateWithoutYear = Carbon::now()->format('d-m');
        $currentYear = (int) Carbon::now()->format('Y');

        $birthday = (int) $this->configs['birthday'];

        foreach ($childs as $child) {
            $birthdate = $child['Birthdate'] ?? null;

            try {
                $birthdateWithoutYear = Carbon::create($birthdate)->format('d-m');
                $birthdateYear = (int) Carbon::create($birthdate)->format('Y');
            } catch (\Exception $e) {
                $birthdateWithoutYear = null;
                $birthdateYear = null;
            }

            if (!$birthdateWithoutYear) {
                continue;
            }

            if (!$birthdateYear) {
                continue;
            }

            if ($currentDateWithoutYear === $birthdateWithoutYear) {
                return (($currentYear - $birthdateYear) === $birthday);
            }
        }

        return false;
    }
}
