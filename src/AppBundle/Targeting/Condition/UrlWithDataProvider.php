<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/03/2020
 * Time: 22:03
 */

namespace AppBundle\Targeting\Condition;

use Pimcore\Targeting\Condition\AbstractVariableCondition;
use Pimcore\Targeting\DataProviderDependentInterface;
use Pimcore\Targeting\Model\VisitorInfo;
use AppBundle\Targeting\DataProvider\Url as UrlDataProvider;

class UrlWithDataProvider extends AbstractVariableCondition implements DataProviderDependentInterface
{
    /**
     * @var string|null
     */
    private $pattern;

    /**
     * Url constructor.
     * @param string|null $pattern
     */
    public function __construct(string $pattern = null)
    {
        $this->pattern = $pattern;
    }

    /**
     * @inheritDoc
     */
    public static function fromConfig(array $config)
    {
        return new static($config['url'] ?? null);
    }

    /**
     * @inheritDoc
     */
    public function canMatch(): bool
    {
        return !empty($this->pattern);
    }

    /**
     * @inheritDoc
     */
    public function match(VisitorInfo $visitorInfo): bool
    {
        $request = $visitorInfo->getRequest();

        $uri = $request->getUri();
        $result = preg_match($this->pattern, $uri);

        if ($result) {
            $visitorInfo->set(UrlDataProvider::PROVIDER_KEY, true);

            return true;
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getDataProviderKeys(): array
    {
        return [
            UrlDataProvider::PROVIDER_KEY
        ];
    }
}
