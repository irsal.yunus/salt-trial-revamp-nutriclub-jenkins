<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/01/2020
 * Time: 15:22
 */

namespace AppBundle\Api\v1;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

class BaseApi
{
    public $client;

    public $baseUrl;

    public $clientId;

    public $clientSecret;

    public function __construct()
    {
        $this->client = new Client([
            'verify' => false,
            'debug' => false
        ]);

        $this->baseUrl = getenv('REWARDS_API_BASE_URL', null);
        $this->clientId = getenv('REWARDS_API_CLIENT_ID', null);
        $this->clientSecret = getenv('REWARDS_API_CLIENT_SECRET', null);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $params
     * @return array
     */
    public function theExecutor(string $method, string $uri, array $params)
    {
        try {
            $response = [];

            $requestExecutor = $this->client->request($method, $uri, $params);

            if ($requestExecutor->getStatusCode() === Response::HTTP_OK) {
                $responseBody = $requestExecutor->getBody()->getContents();

                $response = is_json($responseBody) ? json_decode($responseBody, true) : [
                    'success' => false,
                    'message' => 'Response type is not json.'
                ];

                return $response;
            }

            $responseBody = $requestExecutor->getBody()->getContents();

            $response = is_json($responseBody) ? json_decode($responseBody, true) : [
                'StatusCode' => '010',
                'StatusMessage' => 'Response type is not json.'
            ];

            return $response;
        } catch (GuzzleException $e) {
            $response = [
                'StatusCode' => '500',
                'StatusMessage' => $e->getMessage(),
            ];

            return $response;
        }
    }
}
