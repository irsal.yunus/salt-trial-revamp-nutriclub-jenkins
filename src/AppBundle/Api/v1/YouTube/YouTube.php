<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 20:09
 */

namespace AppBundle\Api\v1\YouTube;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

class YouTube
{
    /** @var Client $guzzleClient */
    private $guzzleClient;

    /** @var string $baseUrl */
    private $baseUrl;

    /** @var string|null $googleDeveloperKey */
    protected $googleDeveloperKey;

    public function __construct()
    {
        $this->guzzleClient = new Client([
            'verify' => false,
            'debug' => false,
        ]);

        $this->googleDeveloperKey = env('GOOGLE_DEVELOPER_KEY', null);

        $this->baseUrl = env('GOOGLEAPIS_YOUTUBE_URL', 'https://www.googleapis.com/youtube/v3/videos');
    }

    public function getVideoDetail($videoId = null)
    {
        if (!$videoId) {
            return null;
        }

        $request = $this->guzzleClient->request('GET', $this->baseUrl, [
            'query' => [
                'part' => 'snippet,contentDetails',
                'id' => $videoId,
                'key' => $this->googleDeveloperKey
            ]
        ]);

        if ($request->getStatusCode() === Response::HTTP_OK) {
            $body = $request->getBody()->getContents();

            if (!is_json($body)) {
                return null;
            }

            $decodeBody = json_decode($body, true);

            return $decodeBody['items'][0] ?? null;
        }
        return null;
    }
}
