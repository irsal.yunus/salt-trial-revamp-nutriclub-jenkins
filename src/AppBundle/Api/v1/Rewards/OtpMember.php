<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/01/2020
 * Time: 14:54
 */

namespace AppBundle\Api\v1\Rewards;

use AppBundle\Api\v1\BaseApi;

class OtpMember extends BaseApi
{
    public function requestMiscallMemberRegister($phone, $oldPhone = null)
    {
        $params = [
            'query' => [
                'Phone' => $phone,
                'ClientID' => $this->clientId,
                'OldPhone' => $oldPhone
            ]
        ];

        return $this->theExecutor(
            'POST',
            $this->baseUrl . '/OTPMember/RequestMisscallRegister',
            $params
        );
    }

    public function verifyMisscallMemberRegister($phone, $token)
    {
        $params = [
            'query' => [
                'Phone' => $phone,
                'Token' => $token,
                'ClientID' => $this->clientId
            ]
        ];

        return $this->theExecutor(
            'POST',
            $this->baseUrl . '/OTPMember/VerifyMisscallRegister',
            $params
        );
    }
}
