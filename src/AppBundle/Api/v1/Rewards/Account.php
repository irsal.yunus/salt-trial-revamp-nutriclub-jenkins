<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/01/2020
 * Time: 14:53
 */

namespace AppBundle\Api\v1\Rewards;

use AppBundle\Api\v1\BaseApi;

class Account extends BaseApi
{
    public function __construct()
    {
        parent::__construct();

    }

    public function login(string $username, string $password, bool $rememberMe = false)
    {
        $params = [
            'json' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'username' => $username,
                'password' => $password,
                'RememberMe' => $rememberMe,
                'type' => null,
                'DeviceType' => null,
                'DeviceID' => null
            ]
        ];

        return $this->theExecutor('POST', $this->baseUrl . '/Account/Login', $params);
    }

    public function register($data)
    {
        // @todo validation for negative case.
        $auth = [
            'json' => [
                'ClientID' => $this->clientId,
                'ClientSecret' => $this->clientSecret
            ]
        ];

        $params = [
            'json' => $data
        ];

        $params = array_merge_recursive($auth, $params);

        $execute = $this->theExecutor('POST', $this->baseUrl . '/Account/Register', $params);
        return $execute;
    }

    public function profile($accessToken)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ];
        return $this->theExecutor('GET', $this->baseUrl . '/Account/Profile', $params);
    }

    public function logout($accessToken)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ];
        return $this->theExecutor('POST', $this->baseUrl . '/Account/Logout', $params);
    }

    public function getProvince($data)
    {
        $params = $data;

        return $this->theExecutor('GET', $this->baseUrl . '/Account/ListProvince', $params);
    }

    public function getDistrict($provinceId)
    {
        $params = [
            'query' => [
                'ProvinceID' => $provinceId
            ]
        ];

        return $this->theExecutor('GET', $this->baseUrl . '/Account/ListDistrict', $params);
    }

    public function updateProfile($data, $accessToken)
    {
        // @todo validation for negative case.
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            'json' => $data
        ];

        return $this->theExecutor('POST', $this->baseUrl . '/Account/UpdateProfile', $params);
    }
}
