<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/02/2020
 * Time: 14:18
 */

namespace AppBundle\Api\v1\Rewards;

use AppBundle\Api\v1\BaseApi;
use Symfony\Component\HttpFoundation\Request;

class Stage extends BaseApi
{
    private $accessToken;

    public function __construct()
    {
        parent::__construct();

        $this->accessToken = $this->getAccessToken();
    }

    public function getStages()
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->accessToken
            ],
            'json' => [
                'TotalRowPerPage' => 10,
                'Page' => 1,
                'OrderByColumnName' => 'id',
                'OrderByDirection' => 'asc'
            ]
        ];

        return $this->theExecutor('POST', $this->baseUrl . '/Account/GetStages', $params);
    }

    private function getAccessToken()
    {
        $account = new \AppBundle\Api\v1\Membership\Account();
        $login = $account->loginRevamp(env('DOT_NET_USER_EMAIL_DUMMY'), env('DOT_NET_USER_PASSWORD_DUMMY'));

        $token = $login['Value']['AccessToken'] ?? null;

        return $token;
    }
}
