<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Register.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 01/09/20
 * @time 13.30
 *
 */

namespace AppBundle\Api\v1\Loyalty;

class Register extends BaseApi
{
    public function sortRegister($data)
    {
        $auth = [
            'json' => [
                'ClientID' => $this->clientId,
                'ClientSecret' => $this->clientSecret
            ]
        ];

        $params = [
            'json' => $data
        ];

        $params = array_merge_recursive($auth, $params);

        $execute = $this->theExecutor('POST', $this->baseUrl . '/api/member/AddSortRegister', $params);
        return $execute;
    }
}
