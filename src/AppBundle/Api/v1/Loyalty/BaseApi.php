<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource BaseApi.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 31/08/20
 * @time 16.05
 *
 */

namespace AppBundle\Api\v1\Loyalty;

use AppBundle\Api\v1\BaseApi as MainBaseApi;

class BaseApi extends MainBaseApi
{
    public function __construct()
    {
        parent::__construct();

        $this->baseUrl = env('NEWNUTRILOYAL_API_BASE_URL', 'https://newnutriloyalapi.staging.salt.id');
        $this->clientId = env('NEWNUTRILOYAL_API_CLIENT_ID', 'bebelac');
        $this->clientSecret = env('NEWNUTRILOYAL_API_CLIENT_SECRET', 'm8omcKtXOrrskrd9qPLIddPGRjU=');
    }
}
