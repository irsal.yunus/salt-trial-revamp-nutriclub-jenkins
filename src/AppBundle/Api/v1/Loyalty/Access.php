<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Access.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 31/08/20
 * @time 15.42
 *
 */

namespace AppBundle\Api\v1\Loyalty;

use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;

class Access extends BaseApi
{
    public function login(string $username, string $password, bool $rememberMe = false)
    {
        $cookies = new CookieJar();
        $params = [
            'debug' => false,
            'json' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'username' => $username,
                'password' => $password,
                'RememberMe' => $rememberMe,
                'OTP' => "sample string 5",
                'DeviceID' => "sample string 6",
                'DeviceType' => "sample string 7",
            ]
        ];

        $exec = $this->theExecutor('GET', $this->baseUrl . '/api/Access/Token', $params);

        $domain = getenv('WILDCARD_DOMAIN_FOR_DOT_NET', null);

        /** @var SetCookie $cookie */
        foreach ($cookies as $cookie) {
            setrawcookie(
                $cookie->getName(),
                $cookie->getValue(),
                $cookie->getExpires(),
                $cookie->getPath(),
                $domain
            );
        }

        return $exec;
    }

    public function logout($accessToken)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ];
        return $this->theExecutor('POST', $this->baseUrl . '/api/Access/LogOut', $params);
    }
}
