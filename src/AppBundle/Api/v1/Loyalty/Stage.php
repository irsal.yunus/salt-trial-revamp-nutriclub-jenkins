<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Stage.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 03/09/20
 * @time 14.36
 *
 */

namespace AppBundle\Api\v1\Loyalty;

class Stage extends BaseApi
{
    public function getStages()
    {
        $params = [];

        return $this->theExecutor('GET', $this->baseUrl . '/api/stages/getall', $params);
    }
}
