<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Point.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 01/09/20
 * @time 15.49
 *
 */

namespace AppBundle\Api\v1\Loyalty;

class Point extends BaseApi
{
    public function addPointByActionCode($accessToken, $actionCode, $description = '')
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            'json' => [
                'ActionCode' => $actionCode,
                'Description' => $description
            ]
        ];

        return $this->theExecutor('POST', $this->baseUrl . '/api/Point/AddPointByActionCode', $params);
    }
}
