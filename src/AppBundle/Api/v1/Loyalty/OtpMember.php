<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource OtpMember.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/09/20
 * @time 13.41
 *
 */

namespace AppBundle\Api\v1\Loyalty;

class OtpMember extends BaseApi
{
    public function requestMiscallMemberRegister($phone, $oldPhone = null)
    {
        $params = [
            'query' => [
                'Phone' => $phone,
                'ClientID' => $this->clientId,
                'OldPhone' => $oldPhone
            ]
        ];

        return $this->theExecutor(
            'POST',
            $this->baseUrl . '/api/otpmember/RequestMisscallMemberRegist',
            $params
        );
    }

    public function verifyMisscallMemberRegister($phone, $token)
    {
        $params = [
            'query' => [
                'Phone' => $phone,
                'Token' => $token,
                'ClientID' => $this->clientId
            ]
        ];

        return $this->theExecutor(
            'POST',
            $this->baseUrl . '/api/otpmember/VerifyMisscallMemberRegister',
            $params
        );
    }
}
