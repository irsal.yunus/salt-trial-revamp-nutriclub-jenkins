<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Member.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 31/08/20
 * @time 16.05
 *
 */

namespace AppBundle\Api\v1\Loyalty;

class Member extends BaseApi
{
    public function profile($accessToken)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ];

        return $this->theExecutor('GET', $this->baseUrl . '/api/member/get', $params);
    }
}
