<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/04/2020
 * Time: 21:54
 */

namespace AppBundle\Api\v1\Membership;

use AppBundle\Api\v1\BaseApi;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Account extends BaseApi
{
    public function __construct()
    {
        parent::__construct();

        // override baseUrl from construct.
        $this->baseUrl = getenv('MEMBERSHIP_API_BASE_URL', null);
    }

    public function loginRevamp(string $username, string $password, bool $rememberMe = false)
    {
        $cookies = new CookieJar();
        $params = [
            'debug' => false,
            'json' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'username' => $username,
                'password' => $password,
                'RememberMe' => $rememberMe,
            ],
            'cookies' => $cookies
        ];

        $exec = $this->theExecutor('POST', $this->baseUrl . '/Account/LoginRevamp', $params);

        // do some stuff with $cookies;

        $domain = getenv('WILDCARD_DOMAIN_FOR_DOT_NET', null);

        /** @var SetCookie $cookie */
        foreach ($cookies as $cookie) {
            setrawcookie(
                $cookie->getName(),
                $cookie->getValue(),
                $cookie->getExpires(),
                $cookie->getPath(),
                $domain
            );
        }

        return $exec;
    }
}
