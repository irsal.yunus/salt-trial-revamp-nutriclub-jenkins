<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 16:29
 */

namespace AppBundle\Api\v1\MoEngage\Event;

use AppBundle\Api\v1\MoEngage\MoEngage;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Pimcore\Log\Simple;
use Symfony\Component\HttpFoundation\Response;

class Event extends MoEngage
{
    public function event($customerId, $deviceId, $actions)
    {
        try {

            $logName = 'MOENGAGE_EVENT_LOG_' . Carbon::now()->format('d-m-Y');

            $requestEvent = $this->client->request('POST', $this->apiUrl . 'event/' .$this->appId, [
                'auth' => [
                    $this->apiId, $this->apiKey
                ],
                'json' => [
                    'type' => 'event',
                    'customer_id' => $customerId,
                    'device_id' => $deviceId,
                    'actions' => $actions
                ]
            ]);

            if ($requestEvent->getStatusCode() === Response::HTTP_OK) {
                $body = $requestEvent->getBody()->getContents();

                Simple::log($logName, $body);

                if (!is_json($body)) {
                    return [];
                }

                $decodeBody = json_decode($body, true);

                if (!$decodeBody['status'] || ($decodeBody['status'] !== 'success')) {
                    return [];
                }

                return $decodeBody;
            }

            Simple::log($logName, $requestEvent->getBody()->getContents());

            return [];

        } catch (GuzzleException $e) {
            Simple::log($logName, $e->getMessage());
        }
    }
}
