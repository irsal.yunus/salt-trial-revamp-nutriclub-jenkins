<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 16:22
 */

namespace AppBundle\Api\v1\MoEngage;

use GuzzleHttp\Client;

class MoEngage
{
    protected $appId;

    protected $apiId;

    protected $apiKey;

    protected $apiUrl;

    protected $client;

    private $appVersion;

    public function __construct()
    {
        $this->appId = env('MOENGAGE_APP_ID', null);

        $this->apiId = env('MOENGAGE_API_ID', null);
        $this->apiKey = env('MOENGAGE_API_KEY', null);

        $this->apiUrl = env('MOENGAGE_API_URL', null);

        $this->appVersion = env('MOENGAGE_APP_VERSION', '1');

        $this->client = new Client([
            'debug' => false,
            'verify' => false,
        ]);
    }

    public function getAppVersion()
    {
        return $this->appVersion;
    }
}
