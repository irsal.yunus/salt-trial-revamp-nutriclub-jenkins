<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/03/2020
 * Time: 18:04
 */

namespace AppBundle\Api\v1\ElasticSearch\Popular;

use AppBundle\Api\v1\ElasticSearch\AbstractElasticSearchApi;

class Popular extends AbstractElasticSearchApi
{
    public function index(array $params)
    {
        $getPopular = $this->theExecutor('GET', '/popular', $params);

        return $getPopular['status']['code'] === '00' && $getPopular['data'] ? $getPopular['data']['popular'] : [];
    }
}
