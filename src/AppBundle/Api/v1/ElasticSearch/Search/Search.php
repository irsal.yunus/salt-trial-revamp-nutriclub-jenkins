<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/03/2020
 * Time: 18:04
 */

namespace AppBundle\Api\v1\ElasticSearch\Search;

use AppBundle\Api\v1\ElasticSearch\AbstractElasticSearchApi;

class Search extends AbstractElasticSearchApi
{
    public function history(array $params)
    {
        $getHistory = $this->theExecutor('GET', '/search', $params);

        return $getHistory['status']['code'] === '00' && $getHistory['data'] ? $getHistory['data']['history'] : [];
    }

    public function add(array $params)
    {
        $add = $this->theExecutor('PUT', '/search', $params);

        return $add['statusCode'] === '00';
    }
}
