<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/03/2020
 * Time: 18:07
 */

namespace AppBundle\Api\v1\ElasticSearch;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractElasticSearchApi
{
    public $client;

    public $baseUrl;

    public function __construct()
    {
        $this->client = new Client([
            'verify' => false,
            'debug' => false,
        ]);

        $this->baseUrl = env('ES_URL_V1', null);
    }

    public function theExecutor(string $method, string $endpoint, array $params)
    {
        try {
            $response = [];

            $requestExecutor = $this->client->request($method, $this->baseUrl . $endpoint, $params);

            if ($requestExecutor->getStatusCode() === Response::HTTP_OK) {
                $responseBody = $requestExecutor->getBody()->getContents();

                $response = is_json($responseBody) ? json_decode($responseBody, true) : [
                    'statusCode' => '010',
                    'statusMessage' => 'Response type is not json.',
                    'data' => null
                ];

                return $response;
            }

            $responseBody = $requestExecutor->getBody()->getContents();

            $response = is_json($responseBody) ? json_decode($responseBody, true) : [
                'statusCode' => '010',
                'statusMessage' => 'Response type is not json.',
                'data' => null
            ];

            return $response;
        } catch (GuzzleException $e) {
            $response = [
                'statusCode' => '500',
                'statusMessage' => $e->getMessage(),
                'data' => null
            ];

            return $response;
        }
    }
}
