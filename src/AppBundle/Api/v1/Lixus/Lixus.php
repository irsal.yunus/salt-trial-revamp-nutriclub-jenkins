<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/01/2020
 * Time: 18:28
 */

namespace AppBundle\Api\v1\Lixus;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Pimcore\Log\Simple;
use Symfony\Component\HttpFoundation\Response;

class Lixus
{
    private $client;

    private $baseUrl;

    protected $apiId;

    protected $accessToken;

    public function __construct()
    {
        $this->client = new Client([
            'verify' => false
        ]);

        $this->baseUrl = env('LIXUS_BASE_URL', null);
        $this->apiId = env('LIXUS_API_ID', null);
        $this->accessToken = env('LIXUS_ACCESS_TOKEN', null);
    }

    public function sendSms($phoneNumber, $messages = 'Test Lixus')
    {
        try {
            $request = $this->client->request('POST', $this->baseUrl . '/new_api/v1/sms', [
                'query' => [
                    'api_id' => $this->apiId,
                    'access_token' => $this->accessToken,
                    'phone' => $phoneNumber,
                    'message' => $messages
                ]
            ]);

            if ($request->getStatusCode() === Response::HTTP_OK) {
                $body = $request->getBody()->getContents();
                if (!is_json($body)) {
                    return false;
                }

                $decode = json_decode($body, true);

                $dateLog = Carbon::now()->format('d-m-Y');
                Simple::log('LIXUS_LOG_' . $dateLog, $body);

                $code = $decode['code'];

                if (!$code) {
                    return false;
                }

                if ($code === 7702) {
                    return true;
                }
            }

        } catch (GuzzleException $e) {
            Simple::log('LIXUS_LOG_ERROR', $e->getMessage());
        }
        return false;
    }
}
