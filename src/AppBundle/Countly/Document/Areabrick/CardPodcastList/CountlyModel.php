<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 11/05/2020
 * Time: 23:16
 */

namespace AppBundle\Countly\Document\Areabrick\CardPodcastList;

use AppBundle\Countly\Document\Areabrick\CountlyModelInterface;
use AppBundle\Countly\Model\DataObject\ArticlePodcast;

class CountlyModel extends ArticlePodcast implements CountlyModelInterface
{
    public CONST EVENT_NAME = 'CLICK_ARTICLE_PODCAST';
}
