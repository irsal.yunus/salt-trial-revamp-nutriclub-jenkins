<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 11/05/2020
 * Time: 15:18
 */

namespace AppBundle\Countly\Document\Areabrick\CardSliderOne;

use AppBundle\Countly\Document\Areabrick\CountlyModelInterface;
use AppBundle\Countly\Model\DataObject\Stage;

class CountlyModel extends Stage implements CountlyModelInterface
{
    public CONST EVENT_NAME = 'CLICK_STAGE';
}
