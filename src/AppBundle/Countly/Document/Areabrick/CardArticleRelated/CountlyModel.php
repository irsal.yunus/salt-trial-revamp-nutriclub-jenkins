<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 11/05/2020
 * Time: 15:18
 */

namespace AppBundle\Countly\Document\Areabrick\CardArticleRelated;

use AppBundle\Countly\Document\Areabrick\CountlyModelInterface;

class CountlyModel implements CountlyModelInterface
{
    public CONST EVENT_NAME = 'CLICK_ARTICLE_RELATED';

    public $id;

    public $slug;

    public $title;
}
