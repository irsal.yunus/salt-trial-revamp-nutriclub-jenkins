<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 11/05/2020
 * Time: 18:09
 */

namespace AppBundle\Countly\Document\Areabrick\CardSliderThree;

use AppBundle\Countly\Document\Areabrick\CountlyModelInterface;
use AppBundle\Countly\Model\DataObject\ArticleCategory;

class CountlyModel extends ArticleCategory implements CountlyModelInterface
{
    public CONST EVENT_NAME = 'CLICK_ARTICLE_CATEGORY';
}
