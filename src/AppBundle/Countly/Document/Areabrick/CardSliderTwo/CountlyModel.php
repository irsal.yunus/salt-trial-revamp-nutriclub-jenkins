<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 11/05/2020
 * Time: 18:05
 */

namespace AppBundle\Countly\Document\Areabrick\CardSliderTwo;

use AppBundle\Countly\Document\Areabrick\CountlyModelInterface;
use AppBundle\Countly\Model\DataObject\Article;

class CountlyModel extends Article implements CountlyModelInterface
{
    public CONST EVENT_NAME = 'CLICK_ARTICLE';
}
