<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 11/05/2020
 * Time: 18:30
 */

namespace AppBundle\Countly\Document\Areabrick\CardArticle;

use AppBundle\Countly\Document\Areabrick\CountlyModelInterface;

class CountlyModel implements CountlyModelInterface
{
    public CONST EVENT_NAME = 'CLICK_ARTICLE';

    public $id;

    public $slug;

    public $title;
}
