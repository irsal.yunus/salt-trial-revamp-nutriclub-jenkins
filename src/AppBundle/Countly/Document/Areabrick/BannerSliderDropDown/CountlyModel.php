<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 11/05/2020
 * Time: 18:36
 */

namespace AppBundle\Countly\Document\Areabrick\BannerSliderDropDown;

use AppBundle\Countly\Document\Areabrick\CountlyModelInterface;

class CountlyModel implements CountlyModelInterface
{
    public $id;

    public $slug;

    public $name;
}
