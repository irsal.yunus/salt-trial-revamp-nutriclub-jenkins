<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 11/05/2020
 * Time: 15:20
 */

namespace AppBundle\Countly;

use AppBundle\Countly\Document\Areabrick\CountlyModelInterface;
use AppBundle\Model\RouterInterface;
use Pimcore\Model\DataObject\AbstractObject;

class LinkBuilder
{
    public static function start(
        AbstractObject $abstractObject,
        CountlyModelInterface $countlyModel,
        string $classAttr = null,
        string $customAttr = null
    ) {
        $emptyTag = '<a href="#">';
        $methods = get_object_vars($countlyModel);

        if (!$methods) {
            return $emptyTag;
        }

        $attrs = 'href="#"';
        if ($abstractObject instanceof RouterInterface) {
            $attrs = 'href="' . $abstractObject->getRouter() . '" ';
        }

        if ($classAttr) {
            $attrs .= 'class="' . $classAttr . '" ';
        }

        foreach ($methods as $key => $method) {
            $keyAsGetter = 'get' . ucfirst($key);

            if (!method_exists($abstractObject, $keyAsGetter)) {
                continue;
            }

            $attrs .= 'data-' . $key . '="' . $abstractObject->$keyAsGetter() . '" ';
        }

        if ($customAttr) {
            $attrs .= $customAttr;
        }

        $class_name = get_class($countlyModel);

        try {
            $constantReflex = new \ReflectionClassConstant($class_name, 'EVENT_NAME');
            $constantValue = $constantReflex->getValue();
        } catch (\ReflectionException $e) {
            $constantValue = null;
        }

        if ($constantValue) {
            $attrs .= ' event-name="' . $constantValue . '" ';
        }

        return '<a '. $attrs .'>';
    }

    public static function stop()
    {
        return '</a>';
    }
}
