<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/05/2020
 * Time: 00:24
 */

namespace AppBundle\Countly\Model\DataObject;

class ArticleVideo
{
    public $slug;

    public $title;
}
