<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 11/05/2020
 * Time: 22:37
 */

namespace AppBundle\Countly\Model\DataObject;

class Stage
{
    public $slug;

    public $name;
}
