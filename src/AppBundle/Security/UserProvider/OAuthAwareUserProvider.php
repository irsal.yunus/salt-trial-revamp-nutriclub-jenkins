<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/01/2020
 * Time: 21:01
 */

namespace AppBundle\Security\UserProvider;

use AppBundle\Api\v1\Rewards\Account;
use AppBundle\Model\DataObject\Customer;
use AppBundle\Services\CustomerService;
use CustomerManagementFrameworkBundle\Security\SsoIdentity\SsoIdentityServiceInterface;
use CustomerManagementFrameworkBundle\Security\UserProvider\OAuthAwareUserProvider as BaseOAuthAwareUserProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class OAuthAwareUserProvider extends BaseOAuthAwareUserProvider
{
    private $customerService;

    public function __construct(
        UserProviderInterface $userProvider,
        SsoIdentityServiceInterface $ssoIdentityService,
        CustomerService $customerService
    )
    {
        parent::__construct($userProvider, $ssoIdentityService);

        $this->customerService = $customerService;
    }

    public function loadUserByUsername($username)
    {
        return parent::loadUserByUsername($username);
    }
}
