<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/03/2020
 * Time: 22:20
 */

namespace AppBundle\Helper;

use AppBundle\Model\DataObject\ArticleVideo;

class CardVideoListWidgetHelper
{
    public static function getObjectWithFilter(
        $offset = 0,
        $limit = 6,
        $orderKey = 'oo_id',
        $order = 'desc'
    ) {
        $articleListing = new ArticleVideo\Listing();

        $articleListing->setOrderKey($orderKey);
        $articleListing->setOrder($order);

        $articleListing->setLimit($limit);
        $articleListing->setOffset($offset);

        return $articleListing;
    }
}
