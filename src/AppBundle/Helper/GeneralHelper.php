<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/01/2020
 * Time: 11:46
 */

namespace AppBundle\Helper;

use Pimcore\Model\DataObject\{ArticleCalendar, StageAge};
use Carbon\Carbon;

class GeneralHelper
{
    /**
     * convert leading zero into 62
     * convert +62 into 62
     * keep 62 into 62
     *
     * @param $msisdn
     * @return bool|string|string[]|null
     */
    public static function convertMsisdnFormat($msisdn)
    {
        $reformat = false;
        if (preg_match('/^62/', $msisdn)) {
            $reformat = $msisdn;
        } elseif (preg_match('/^[+]62/', $msisdn)) {
            $reformat = preg_replace('/^[+]62/', '62', $msisdn);
        } elseif (preg_match('/^0/', $msisdn)) {
            $reformat = preg_replace('/^0/', '62', $msisdn);
        }
        return $reformat;
    }

    public static function getFirstNameFromFullName($fullname)
    {
        $firstname = explode(' ', preg_replace('/\s+/', ' ', $fullname));
        return $firstname[0] ?? $fullname;
    }

    public static function getLastNameFromFullName($fullname)
    {
        $lastname = explode(' ', preg_replace('/\s+/', ' ', $fullname));
        return $lastname[1] ?? $fullname;
    }

    public static function getImgPlaceholder(int $width = 150, int $height = 150)
    {
        $placeholder = 'https://via.placeholder.com/' . $width . 'x' . $height ;
        return '<img src="'.$placeholder.'">';
    }

    public static function getArticleCalanderByAgePregnant($agePregnant)
    {
        $stageAge = new StageAge\Listing();
        $stageAge->setCondition('age__value = ? AND age__unit = ?', [$agePregnant, 1]);

        if ($stageAge->getCount() <= 0) {
            return null;
        }

        foreach ($stageAge as $age) {
            $ageId = $age->getId();

            $articleCalendar = new ArticleCalendar\Listing();
            $articleCalendar->setCondition('stageAge__id = ?', [$ageId]);

            if ($articleCalendar->getCount() <= 0) {
                return null;
            }

            return $articleCalendar->load();
        }
    }

    public static function trimText($input, $length, $ellipses = true, $strip_html = true) {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }

        return $trimmed_text;
    }

    public static function getObjectCountByRelation(
        $targetObjectName,
        $objectRelationSqlQueryField,
        $objectRelationSqlQueryValue
    )
    {
        $namespace = '\\Pimcore\Model\\DataObject\\' . $targetObjectName . '\\Listing';
        $object = new $namespace();
        $object->setCondition($objectRelationSqlQueryField . ' = ?', [$objectRelationSqlQueryValue]);
        $object->load();


        return $object->getCount() ?? 0;
    }

    public static function articleTypeSwitcherMatchElasticSearchMapping(string $articleType)
    {
        switch ($articleType) :
            case 'artice' :
                return 'article';
            break;
            case 'video' :
                return 'articlevideo';
            case 'podcast' :
                return 'articlepodcast';
            case 'ebook' :
                return 'articleebook';
        endswitch;

        return $articleType;
    }

    public static function rearrangeChildBasedOnAge(array $childs)
    {
        $rearrange = [];

        if (!$childs) {
            return $rearrange;
        }

        $childsHolder = [];
        $rearrangeChilds = [];

        foreach ($childs as $child) {
            $birthdate = $child['Birthdate'] ?? null;

            if (!$birthdate) {
                continue;
            }

            $dateFormat = env('RRR_LIVE') ? 'd/m/Y' : 'd-m-Y';
            $birthdateToTimeStamp = Carbon::createFromFormat($dateFormat, $birthdate)->timestamp;

            $rearrange[$birthdateToTimeStamp] = $birthdateToTimeStamp;
            $childsHolder[$birthdateToTimeStamp] = $child;
        }

        krsort($rearrange);

        foreach ($rearrange as $arrange) {
            $rearrangeChilds[] = $childsHolder[$arrange];
        }

        return $rearrangeChilds;
    }

    public static function getYoungestChildId(array $childs)
    {
        if (!$childs) {
            return 0;
        }

        $childBirthDate = [];
        $childIdHolder = [];
        foreach ($childs as $child) {
            $birthdate = $child['Birthdate'] ?? null;
            $childId = $child['ID'] ?? null;

            if (!$birthdate) {
                continue;
            }

            $dateFormat = env('RRR_LIVE') ? 'd/m/Y' : 'd-m-Y';
            $birthdateToTimeStamp = Carbon::createFromFormat($dateFormat, $birthdate)->timestamp;
            $childBirthDate[] = $birthdateToTimeStamp;
            $childIdHolder[$birthdateToTimeStamp] = $childId;
        }

        return $childIdHolder[min($childBirthDate)] ?? 0;
    }

    public static function isValidRefererUrl($referer)
    {
        $regex = null;

        if (env('PIMCORE_ENVIRONMENT') === 'prod') {
            $regex = '/https?:\/\/([a-z0-9]+[.])*nutriclub[.]co[.]id/m';
        }

        if (env('PIMCORE_ENVIRONMENT') === 'dev') {
            $regex = '/https?:\/\/([\w-]+[.])*salt[.]id/m';

//            $regex = '/https?:\/\/([a-z0-9]+[.])*localhost/m';
        }

        preg_match_all($regex, $referer, $matches, PREG_SET_ORDER, 0);

        return $matches;
    }
}
