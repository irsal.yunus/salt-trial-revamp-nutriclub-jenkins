<?php
/**
 * Created by PhpStorm.
 */
namespace AppBundle\Helper;

class SeoFriendlyHelper
{
    public static function friendlyURL($inputString){
        $url = strtolower($inputString);
        $patterns = $replacements = array();
        $patterns[0] = '/(&amp;|&)/i';
        $replacements[0] = '-and-';
        $patterns[1] = '/[^a-zA-Z01-9]/i';
        $replacements[1] = '-';
        $patterns[2] = '/(-+)/i';
        $replacements[2] = '-';
        $patterns[3] = '/(-$|^-)/i';
        $replacements[3] = '';
        $patterns[4] = '/[0-9]+/';
        $replacements[4] = '';
        $url = preg_replace($patterns, $replacements, $url);
        $url = self::beautifyUrl($url);
        return $url;
    }

    public static function beautifyUrl($inputString)
    {
      return ltrim($inputString, '-');
    }

    public static function renderFriendlyUrl($inputString, $class)
    {
        $friendlyUrl = self::friendlyURL($inputString);
        $html = '<a href="' . $friendlyUrl . '"';
        $html .= 'class="' . $class . '">';
        $html .= $inputString . '</a>';

        return $html;
    }
    public static function renderListFriendlyUrl($mainUrl, $class = '', $prefixUrl)
    {
        $friendlyUrl = self::friendlyURL($mainUrl);
        $html = '<a href="' . $prefixUrl . $friendlyUrl . '"';
        $html .= 'class="' . $class . '">';
        $html .= $mainUrl . '</a>';

        return $html;
    }
}
