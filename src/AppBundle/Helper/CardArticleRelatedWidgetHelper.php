<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/02/2020
 * Time: 20:23
 */

namespace AppBundle\Helper;

use AppBundle\Model\DataObject\{Article, Stage};
use Pimcore\Model\DataObject\{AbstractObject, ArticleCategory, StageSwitcher};

class CardArticleRelatedWidgetHelper
{
    public static function getRelatedObject(
        $articleStage,
        $articleCategory,
        $exceptThisSlug,
        $limit = 6,
        $orderKey = 'oo_id',
        $order = 'desc'
    )
    {
        // same stage, same category.
        $articleHolder[0] = self::getArticle(
            $articleStage,
            $articleCategory,
            $exceptThisSlug,
            $limit = 1,
            0
        );
        $notInId = null;
        if ($articleHolder[0] && $objects = $articleHolder[0]->getObjects()) {
            /** @var Article $firstObject */
            $firstObject = $objects[0];
            $notInId[0] = $firstObject->getId();
        }

        // randomize stage, randomize category.
        $articleHolder[1] = self::getArticle(
            self::stageSwitcher($articleStage),
            self::randomize($articleCategory),
            $exceptThisSlug,
            $limit = 1,
            0,
            $notInId
        );
        if ($articleHolder[1] && $objects = $articleHolder[1]->getObjects()) {
            /** @var Article $firstObject */
            $firstObject = $objects[0];
            $notInId[1] = $firstObject->getId();
        }

        // same stage, same category.
        $articleHolder[2] = self::getArticle(
            $articleStage,
            $articleCategory,
            $exceptThisSlug,
            $limit = 1,
            1,
            $notInId
        );
        if ($articleHolder[2] && $objects = $articleHolder[2]->getObjects()) {
            /** @var Article $firstObject */
            $firstObject = $objects[0];
            $notInId[2] = $firstObject->getId();
        }

        // alergi stage, same category.
        $articleHolder[3] = self::getArticle(
            Stage::getBySlug('alergi', 1) ?? $articleStage,
            $articleCategory,
            $exceptThisSlug,
            $limit = 1,
            0,
            $notInId
        );
        if ($articleHolder[3] && $objects = $articleHolder[3]->getObjects()) {
            /** @var Article $firstObject */
            $firstObject = $objects[0];
            $notInId[3] = $firstObject->getId();
        }

        // same stage, same category.
        $articleHolder[4] = self::getArticle(
            $articleStage,
            $articleCategory,
            $exceptThisSlug,
            $limit = 1,
            2,
            $notInId
        );
        if ($articleHolder[4] && $objects = $articleHolder[4]->getObjects()) {
            /** @var Article $firstObject */
            $firstObject = $objects[0];
            $notInId[4] = $firstObject->getId();
        }

        // randomize stage, same category.
        $articleHolder[5] = self::getArticle(
            self::stageSwitcher($articleStage),
            $articleCategory,
            $exceptThisSlug,
            $limit = 1,
            0,
            $notInId
        );
        if ($articleHolder[5] && $objects = $articleHolder[5]->getObjects()) {
            /** @var Article $firstObject */
            $firstObject = $objects[0];
            $notInId[5] = $firstObject->getId();
        }

        $objects = [];
        $tmp = [];
        foreach ($articleHolder as $article) {
            $tmp[] = $article->getObjects();
        }
        $objects = array_merge($objects, ...$tmp);

        $customeArticleListing = new Article\Listing();
        $customeArticleListing->setObjects($objects);

        return $customeArticleListing;
    }

    public static function getArticle(
        $articleStage,
        $articleCategory,
        $exceptThisSlug,
        $limit = 6,
        $offset = 0,
        $notInId = null,
        $orderKey = 'oo_id',
        $order = 'desc'
    )
    {
        $articleListing = new Article\Listing();

        /** @var Stage $articleStage */
        if ($articleStage) {
            $articleListing
                ->addConditionParam('stage__id = ?', $articleStage->getId(), 'AND');
        }

        /** @var ArticleCategory $articleCategory */
        if ($articleCategory) {
            $articleListing
                ->addConditionParam('category__id = ?', $articleCategory->getId(), 'AND');
        }

        if ($exceptThisSlug) {
            $articleListing
                ->addConditionParam('slug != ?', $exceptThisSlug, 'AND');
        }

        if ($notInId) {
            $articleListing
                ->addConditionParam('oo_id NOT IN('. implode(',',$notInId) .')');
        }

        $articleListing->setLimit($limit);
        $articleListing->setOffset($offset);
        $articleListing->setOrderKey($orderKey);
        $articleListing->setOrder($order);

        return $articleListing;
    }

    public static function stageSwitcher(?Stage $articleStage)
    {
        if (!$articleStage) {
            return null;
        }
        $stageSwitchers = new StageSwitcher\Listing();
        $stageSwitchers->setCondition('fromStage__id = ?', $articleStage->getId());
        $stageSwitchers->setLimit(1);

        $stageAfterSwitch = $articleStage;

        if ($stageSwitchers->getCount() > 0) {
            /** @var StageSwitcher $stageSwitcher */
            $stageSwitcher = $stageSwitchers->getObjects()[0];

            $stageAfterSwitch = $stageSwitcher->getToStage();
        }

        return $stageAfterSwitch;
    }

    public static function randomize(?AbstractObject $object)
    {
        if (!$object) {
            return null;
        }
        $randomize = $object;

        $reflectionClass = new \ReflectionClass($object);
        $namespace = $reflectionClass->getName();

        $obj = $namespace . '\Listing';

        $objects = new $obj();
        $objects->setCondition('oo_id != ?', $object->getId());
        $objects->load();

        try {
            $randomIndex = random_int(0, ($objects->getCount() - 1));

            $randomize = $objects->getObjects()[$randomIndex];
        } catch (\Exception $exception) {

        }

        return $randomize;
    }
}
