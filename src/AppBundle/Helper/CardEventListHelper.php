<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 13/06/2020
 * Time: 22:55
 */

namespace AppBundle\Helper;

use Pimcore\Model\DataObject\Event;

class CardEventListHelper
{
    public static function getObjectWithFilter(
        $offset = 0,
        $limit = 6,
        $orderKey = 'oo_id',
        $order = 'desc'
    ) {
        $articleListing = new Event\Listing();

        $articleListing->setOrderKey($orderKey);
        $articleListing->setOrder($order);

        $articleListing->setLimit($limit);
        $articleListing->setOffset($offset);

        return $articleListing;
    }
}
