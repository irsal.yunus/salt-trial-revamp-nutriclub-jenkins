<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 06/02/2020
 * Time: 11:09
 */

namespace AppBundle\Helper;

use AppBundle\Model\DataObject\Article;
use Pimcore\Model\DataObject\ {
    ArticleCategory,
    ArticleRoleFocus,
    ArticleSubCategory,
    Stage,
    StageAge,
    StageGroup,
    SubStage
};
use Zend\Paginator\Paginator;

class CardArticleWidgetHelper
{
    public static function getObjectWithfilter(
        $articleRoleFocus,
        $articleCategory,
        $articleSubCategory,
        $stage,
        $stageGroup,
        $subStage,
        $stageAge,
        $offset = 0,
        $limit = 6,
        $orderKey = 'oo_id',
        $order = 'desc'
    ) {
        $articleListing = new Article\Listing();
        /** @var ArticleRoleFocus $articleRoleFocus */
        if ($articleRoleFocus) {
            $articleListing
                ->addConditionParam('roleFocus__id = ?', $articleRoleFocus->getId(), 'AND');
        }

        /** @var ArticleCategory $articleCategory */
        if ($articleCategory) {
            $articleListing
                ->addConditionParam('category__id = ?', $articleCategory->getId(), 'AND');
        }

        /** @var ArticleSubCategory $articleSubCategory */
        if ($articleSubCategory) {
            $articleListing
                ->addConditionParam('subCategory LIKE ?', '%' . $articleSubCategory->getId() . '%', 'AND');
        }

        /** @var Stage $stage */
        if ($stage) {
            $articleListing
                ->addConditionParam('stage__id = ?', $stage->getId(), 'AND');
        }

        /** @var SubStage $subStage */
        if ($subStage) {
            $articleListing
                ->addConditionParam('subStage LIKE ?', '%' . $subStage->getId() . '%', 'AND');
        }

        /** @var StageAge $stageAge */
        if ($stageAge) {
            $articleListing
                ->addConditionParam('stageAge__id = ?', $stage->getId(), 'AND');
        }

        /** @var StageGroup $stageGroup */
        if ($stageGroup) {
            if ($stagesFromStageGroups = $stageGroup->getStages()) {
                $collector = [];
                $countStagesFromStageGroups = 0;
                if (is_countable($stagesFromStageGroups)) {
                    $countStagesFromStageGroups = count($stagesFromStageGroups);
                }
                foreach ($stagesFromStageGroups as $stagesFromStageGroup) {
                    $articleListing
                        ->addConditionParam('stage__id = ?', $stagesFromStageGroup->getId(), 'AND');


                    $articleListing->setOffset($offset);
                    $articleListing->setLimit($limit / $countStagesFromStageGroups);
                    $articleListing->setOrder($order);

                    $collector[] = $articleListing->load();
                }

                return $collector;
            }
        }

        $articleListing->setOrderKey($orderKey);
        $articleListing->setOrder($order);

        $paginator = new Paginator($articleListing);
        $paginator->setCurrentPageNumber($offset);
        $paginator->setItemCountPerPage($limit);

        return $paginator;
    }
}
