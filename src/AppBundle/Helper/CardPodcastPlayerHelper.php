<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/03/2020
 * Time: 09:27
 */

namespace AppBundle\Helper;

use AppBundle\Model\DataObject\ArticlePodcast;

class CardPodcastPlayerHelper
{
    public static function getObjectWithFilter(
        $offset = 0,
        $limit = 6,
        $orderKey = 'oo_id',
        $order = 'desc'
    ) {
        $articleListing = new ArticlePodcast\Listing();

        $articleListing->setOrderKey($orderKey);
        $articleListing->setOrder($order);

        $articleListing->setLimit($limit);
        $articleListing->setOffset($offset);

        return $articleListing;
    }
}
