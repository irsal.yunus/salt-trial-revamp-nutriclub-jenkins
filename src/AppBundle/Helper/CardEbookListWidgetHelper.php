<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/03/2020
 * Time: 22:20
 */

namespace AppBundle\Helper;

use AppBundle\Model\DataObject\ArticleEbook;
use Zend\Paginator\Paginator;

class CardEbookListWidgetHelper
{
    public static function getObjectWithPagination(
        $offset = 0,
        $limit = 6,
        $orderKey = 'oo_id',
        $order = 'desc'
    ) {
        $articleListing = new ArticleEbook\Listing();

        $articleListing->setOrderKey($orderKey);
        $articleListing->setOrder($order);

        $paginator = new Paginator($articleListing);
        $paginator->setCurrentPageNumber($offset);
        $paginator->setItemCountPerPage($limit);

        return $paginator;
    }

    public static function getObject(
        $offset = 0,
        $limit = 6,
        $orderKey = 'oo_id',
        $order = 'desc'
    )
    {
        $articleListing = new ArticleEbook\Listing();

        $articleListing->setOrderKey($orderKey);
        $articleListing->setOrder($order);
        $articleListing->setLimit($limit);
        $articleListing->setOffset($offset);

        return $articleListing;
    }
}
