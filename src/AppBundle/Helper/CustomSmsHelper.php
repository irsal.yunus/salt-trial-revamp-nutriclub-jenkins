<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/06/2020
 * Time: 13:49
 */

namespace AppBundle\Helper;

use AppBundle\Model\DataObject\Customer;
use Carbon\Carbon;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\Data\Link;

class CustomSmsHelper
{
    public const VARIABLES_TO_BE_REPLACE = [
        '::LinkWebinar',
        '::GenderFriendlyName',
        '::FullName',
        '::Title',
        '::StartDate',
        '::StartTime',
        '::EndTime'
    ];

    public static function reformat(string $text, AbstractObject $event, $customer)
    {
        if (!$customer instanceof Customer) {
            return $text;
        }

        $variableToBeReplace = self::VARIABLES_TO_BE_REPLACE;

        foreach ($variableToBeReplace as $variable) {

            $pattern = '/'. $variable .'/m';

            preg_match_all($pattern, $text, $matches, PREG_SET_ORDER, 0);

            if (!$matches || !$matches[0]) {
                continue;
            }

            $variableWithOutSelector = str_replace('::', '', $variable);
            $getter = 'get' . ucfirst($variableWithOutSelector);

            if (method_exists($event, $getter)) {
                $replaceByEvent = self::eventObjectGetter($event, $getter, $variable);
                $text = str_replace($variable, $replaceByEvent, $text);
            }

            if (method_exists($customer, $getter)) {
                $replaceByCustomer = ucfirst(self::customerObjectGetter($customer, $getter, $variable));
                $text = str_replace($variable, $replaceByCustomer, $text);
            }
        }

        return $text;
    }

    private static function eventObjectGetter($event, $getter, $variable)
    {
        /** @var Link $link */
        if (($link = $event->$getter()) instanceof Link) {
            return $link->getHref() ?? $variable;
        }

        if (is_string($event->$getter())) {
            return $event->$getter();
        }

        if ($event->$getter() instanceof Carbon) {
            setlocale(LC_ALL, 'id_ID', 'id', 'ID');

            /** @var Carbon $date */
            $date = $event->$getter();

            return $date->formatLocalized('%d %B %Y');
        }

        return $event->$getter();
    }

    private static function customerObjectGetter(Customer $customer, $getter, $variable)
    {
        return $customer->$getter() ?? $variable;
    }
}
