<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 05/02/2020
 * Time: 16:31
 */

namespace AppBundle\Helper;

use Pimcore\Model\DataObject\Article;

class StaticRouteHelper
{
    public static function removeUnusedRouteParams(
        $routeParams,
        $exclude = [
            'bundle', 'controller', 'action', 'module', 'pimcore_request_source'
        ])
    {
        $newRouteParams = [];
        if (!$routeParams) {
            return $newRouteParams;
        }

        if (in_array('*', $exclude, false)) {
            return $newRouteParams;
        }


        foreach ($routeParams as $key => $routeParam) {
            if (in_array($key, $exclude)) {
                continue;
            }
            $newRouteParams[$key] = $routeParam;
        }
        return $newRouteParams;
    }
}
