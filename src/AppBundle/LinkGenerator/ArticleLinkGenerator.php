<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 17:29
 */

namespace AppBundle\LinkGenerator;

use AppBundle\Model\DataObject\Article;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Model\DataObject\Concrete;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RouterInterface;

class ArticleLinkGenerator implements LinkGeneratorInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function generate(Concrete $object, array $params = []): string
    {
        if (!$object instanceof Article) {
            throw new \InvalidArgumentException(sprintf('Object must be an instance of %s', Article::class));
        }

        $routeParams = $object->getRouterParams();

        $referenceType = $params['referenceType'] ?? UrlGenerator::ABSOLUTE_PATH;

        return $this->router->generate('ARTICLE', $routeParams, $referenceType);
    }
}
