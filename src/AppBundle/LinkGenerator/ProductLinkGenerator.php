<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/07/2020
 * Time: 20:13
 */

namespace AppBundle\LinkGenerator;

use AppBundle\Model\DataObject\Product;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Model\DataObject\Concrete;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RouterInterface;

class ProductLinkGenerator implements LinkGeneratorInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function generate(Concrete $object, array $params = []): string
    {
        if (!$object instanceof Product) {
            throw new \InvalidArgumentException(sprintf('Object must be an instance of %s', Product::class));
        }

        $routeParams = $object->getRouterParams();

        $referenceType = $params['referenceType'] ?? UrlGenerator::ABSOLUTE_PATH;

        return $this->router->generate('PRODUCT', $routeParams, $referenceType);
    }
}
