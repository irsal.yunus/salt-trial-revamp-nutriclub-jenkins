<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

declare(strict_types=1);

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\{
    CheckboxType, ChoiceType, EmailType, HiddenType, NumberType, PasswordType, SubmitType, TextType
};
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFormType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Gender', ChoiceType::class, [
                'label' => 'Untuk mengenal lebih dekat, siapa Anda ?',
                'choices' => [
                    'mama' => 'female',
                    'papa' => 'male'
                ],
                'expanded' => true
            ])
            ->add('Fullname', TextType::class, [
                'label' => 'Nama Lengkap'
            ])
            ->add('Email', EmailType::class, [
                'label' => 'Alamat Email',
            ])
            ->add('Phone', TextType::class, [
                'label' => 'Nomor Handphone'
            ])
            ->add('pregnancyPlanning', ChoiceType::class, [

            ])
            ->add('AgePregnant', NumberType::class, [
                'label' => 'Usia Kehamilan'
            ]);
        if (!$options['hidePassword']) {
            $builder->add('password', PasswordType::class, [
                'attr' => [
                    'autocomplete' => 'off'
                ],
                'label' => 'general.password'
            ]);
        }

        $builder
            ->add('newsletter', CheckboxType::class, [
                'label' => 'general.newsletter',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-custom'
                ]
            ])
            ->add('profiling', CheckboxType::class, [
                'label' => 'general.profiling',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-custom'
                ]
            ]);

        $builder
            ->add('oAuthKey', HiddenType::class)
            ->add('_submit', SubmitType::class, [
                'label' => 'Register',
                'attr' => [
                    'class' => 'btn btn-lg btn-block btn-success'
                ]
            ]);
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        // we need to set this to an empty string as we want _username as input name
        // instead of login_form[_username] to work with the form authenticator out
        // of the box
        return '';
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'csrf_protection' => true,
                'csrf_field_name' => '_csrf_token',
                'csrf_token_id'   => 'register',
            ])
            ->setDefined('hidePassword');
    }
}
