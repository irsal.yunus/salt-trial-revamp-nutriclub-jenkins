<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 13/03/2020
 * Time: 21:46
 */

namespace AppBundle\Command;

use AppBundle\Model\DataObject\Article;
use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ArticleIndexer extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('article:es:index')
            ->setDescription('Article Index To ElasticSearch');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $newArticleListing = new Article\Listing();
        $newArticleListing->load();

        foreach ($newArticleListing as $key => $article) {
            $index = $key;
            $message = 'Success';
            try {

                $article->save([
                    'versionNote' => 'AUTO INDEX FOR ELASTICSEARCH'
                ]);
            } catch (\Exception $exception) {
                $message = $exception->getMessage();
            } finally {
                $output->writeln($index . $message);
            }
        }
    }
}
