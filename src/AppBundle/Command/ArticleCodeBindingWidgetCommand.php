<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/02/2020
 * Time: 18:43
 */

namespace AppBundle\Command;

use AppBundle\Model\DataObject\Article;
use Pimcore\Console\AbstractCommand;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Fieldcollection\Data\ArticleChild;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ArticleCodeBindingWidgetCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('article:codebinding:widget')
            ->setDescription('Code binding widget in wysiwyg at detail article');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $articles = new Article\Listing();
        $articles->load();

        if ($articles->getObjects()) {
            /** @var Article $object */
            foreach ($articles->getObjects() as $key => $object) {
                $parentContent = $object->getContent();

                if (!$parentContent) {
                    $this->addCodeBindOnChild($object);
                    continue;
                }

                $findCurlyBraches = preg_match_all('/{{(\w+)}}/', $parentContent, $matches);

                if ($findCurlyBraches) {
                    continue;
                }

                $findParagraph = preg_match_all("(<p>.+?</p>)", $parentContent, $matches);

                if (!$findParagraph) {
                    continue;
                }

                $countMatches = count($matches[0]);

                if ($countMatches >= 1 || $countMatches <= 2) {
                    if (isset($matches[0][0])) {
                        $addWidgetParagraphOne = $matches[0][0] . '<p>{{WIDGET_PARAGRAPH_ONE}}</p>';
                        $parentContent = str_replace($matches[0][0], $addWidgetParagraphOne, $parentContent);
                    }
                    if (isset($matches[0][1])) {
                        $addWidgetParagraphOne = $matches[0][1] . '<p>{{WIDGET_PARAGRAPH_TWO}}</p>';
                        $parentContent = str_replace($matches[0][1], $addWidgetParagraphOne, $parentContent);
                    }
                    try {
                        $object->setContent($parentContent);
                        $object->save();
                    } catch (\Exception $exception) {
                        Simple::log('CODE_BINDING_ERROR', $exception->getMessage() . ' ON OBJECT ID : ' . $object->getId());
                    }
                }
            }
        }
    }

    private function addCodeBindOnChild(Article $object)
    {
        $childContent = $object->getChild();

        if (!$childContent) {
            return;
        }

        // Pimcore\Model\DataObject\Fieldcollection\Data\ArticleChild
        $countChild = count($childContent->getItems());

        if (!$countChild) {
            return;
        }

        $articleChilds = new DataObject\Fieldcollection();
        $shouldStop = false;
        /** @var ArticleChild $item */
        foreach ($childContent->getItems() as $key => $item) {
            if ($shouldStop) {
                return;
            }
            $originalContent = $item->getContent();

            $findParagraph = preg_match_all("(<p>.+?</p>)", $originalContent, $matches);

            if (!$findParagraph) {
                return;
            }

            $countMatches = count($matches[0]);

            $contentHolder = $item->getContent();
            if ($countMatches >= 1 || $countMatches <= 2) {
                if (isset($matches[0][0])) {
                    $addWidgetParagraphOne = $matches[0][0] . '<p>{{WIDGET_PARAGRAPH_ONE}}</p>';
                    $contentHolder = str_replace($matches[0][0], $addWidgetParagraphOne, $contentHolder);
                }
                if (isset($matches[0][1])) {
                    $addWidgetParagraphTwo = $matches[0][1] . '<p>{{WIDGET_PARAGRAPH_TWO}}</p>';
                    $contentHolder = str_replace($matches[0][1], $addWidgetParagraphTwo, $contentHolder);
                }
                $shouldStop = true;
            }

            $articleChild = new ArticleChild();
            $articleChild->setTitle($item->getTitle());
            $articleChild->setContent($contentHolder);

            $articleChilds->add($articleChild);

            try {
                echo $object->getSlug();
                echo PHP_EOL;
                $object->setChild($articleChilds);
                $object->save();
            } catch (\Exception $exception) {
                Simple::log('CODE_BINDING_ERROR', $exception->getMessage() . ' ON OBJECT ID : ' . $object->getId());
            }
        }



        if ($countChild >= 1) {
            if (isset($childContent->getItems()[0])) {
                /** @var ArticleChild $child */
                $child = $childContent->getItems()[0];
                $contentChild = $child->getContent();

            }
        }
    }
}
