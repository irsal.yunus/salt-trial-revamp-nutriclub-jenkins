<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 15/01/2020
 * Time: 10:53
 */

namespace AppBundle\Command;

use AppBundle\Model\DataObject\Article;
use AppBundle\Tool\Folder;
use Pimcore\Model\Asset;
use Pimcore\Model\DataObject\ArticleCategory;
use Pimcore\Model\DataObject\Fieldcollection;
use Pimcore\Model\DataObject\Stage;
use Ramsey\Uuid\Uuid;
use AppBundle\Traits\{Slugable};
use Pimcore\Console\AbstractCommand;
use Pimcore\Log\Simple;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Pimcore\Model\DataObject\QuantityValue\Unit;
use Pimcore\Model\DataObject\StageAge;
use Symfony\Component\Console\Input\{InputDefinition, InputInterface, InputOption};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Response;

class ArticleImportCommand extends AbstractCommand
{
    use Slugable;

    private $folderPath;

    private $baseUrl;

    private $client;

    public function __construct()
    {
        parent::__construct();

        $this->folderPath = 'umbraco-articles';

        $this->baseUrl = 'https://nutriclub.co.id';

        $this->client = new Client([
            'verify' => false,
        ]);
    }

    protected function configure()
    {
        $this
            ->setName('article:import')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('filepath', 'p', InputOption::VALUE_REQUIRED)
                ])
            )
            ->setDescription('Import Article from json')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $input->getOption('filepath');

        if (!$filePath) {
            $output->write('Nothing to do file not found, check --help for more information');
        }

        $filePath = PIMCORE_PROJECT_ROOT . $filePath;

        if (!$isExist = file_exists($filePath)) {
            $output->writeln('File : "' . $filePath . '" does not exist');
        }

        $fileContent = file_get_contents($filePath);

        if (!is_json($fileContent)) {
            return;
        }

        $decodeContent = json_decode($fileContent, true);

        $articleDatas = $decodeContent['Data'] ?? [];

        foreach ($articleDatas as $articleData) {
            $this->checkAndImport($articleData);
        }
    }

    private function checkAndImport($articleData)
    {
        $folder = Folder::checkAndCreate($this->folderPath);

        $title = $articleData['mainTitle'] ?? '';
        $assetName = $this->slugify($title);

        $stageName = $articleData['kategori'] ?? null;

        $category = $articleData['subKategori'] ?? null;

        $stageWeek = $articleData['stageWeek'] ?? null;
        $stageMonth = $articleData['stageMonth'] ?? null;
        $stageYear = $articleData['stageYear'] ?? null;

        $ageUnit = $stageWeek ? 'week' : ($stageMonth ? 'month' : ($stageYear ? 'year' : 'week'));

        $ageValue = $articleData['stage'. ucfirst($ageUnit)];

        $largeImage = $articleData['largeImg'] ?? '';
        $mediumImage = $articleData['mediumImg'] ?? '';
        $smallImage = $articleData['smallImg'] ?? '';

        $largeAssetName = $assetName . '_large' . '.jpg';
        $mediumAssetName = $assetName . '_medium' . '.jpg';
        $smallAssetName = $assetName . '_small' . '.jpg';

        /** @var Unit $unit */
        $unit = Unit::getByAbbreviation($ageUnit);
        if (!$unit) {
            return;
        }

        $stageAgeLists = new StageAge\Listing();
        $stageAgeLists->setCondition('age__value = ? AND age__unit = ?', [
            $ageValue,
            $unit->getId()
        ]);
        $stageAgeLists->setLimit(1);

        $stageAge = $stageAgeLists->getCount() > 0 ? $stageAgeLists->getObjects()[0] : null;

        /** @var Stage $stage */
        $stage = Stage::getBySlug(strtolower($stageName), 1);

        /** @var ArticleCategory $category */
        $category = ArticleCategory::getBySlug(strtolower($category), 1);

        $childs = $articleData['ChildArticle'];

        $content = $articleData['caption'];

        try {
            $article = new Article();

            $article->setParent($folder);
            $article->setKey($this->slugify($title));
            $article->setSlug($this->slugify($title));
            $article->setTitle($title);

            $article->setContent($content);

            $article->setChild($this->getChildCollection($childs));

            $article->setImgDesktop($this->getImageContentFromUrl($largeImage, $largeAssetName));
            $article->setImgTablet($this->getImageContentFromUrl($mediumImage, $mediumAssetName));
            $article->setImgMobile($this->getImageContentFromUrl($smallImage, $smallAssetName));

            $article->setCategory($category);
            $article->setStage($stage);
            $article->setStageAge($stageAge);

            $article->setPublished(false);
            $article->save(['versionNote' => 'From Article Import Command']);
        } catch (\Exception $exception) {
            Simple::log('ARTICLE_IMPORT_FAILED', $exception->getMessage());
        }
    }

    /**
     * @param String $url
     * @return Asset|Asset\Image|null
     */
    private function getImageContentFromUrl(string $url, string $filename)
    {
        $articlesPath = Folder::checkAndCreateAssets('articles');
        $imagePath = Folder::checkAndCreateAssets('image', $articlesPath);

        try {
            $getImageContent = $this->client->request('GET', $this->baseUrl . $url, [

            ]);

            if ($getImageContent->getStatusCode() === Response::HTTP_OK) {
                $imageContent = $getImageContent->getBody()->getContents();

                $newAsset = new Asset();
                $newAsset->setParent($imagePath);
                $newAsset->setFilename($filename);
                $newAsset->setData($imageContent);
                $newAsset->save(['versionNote' => 'Import JSON']);

                return $newAsset;
            }

            return null;
        } catch (RequestException $requestException) {
            return null;
        } catch (GuzzleException $guzzleException) {
            return null;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param $childs
     * @return Fieldcollection|null
     *
     */
    private function getChildCollection($childs)
    {
        if (!$childs) {
            return null;
        }

        $items = new Fieldcollection();

        foreach ($childs as $child) {
            $content = $child['expertContent'] ?? null;

            if (!$content) {
                continue;
            }

            $item = new Fieldcollection\Data\ArticleChild();
            $item->setContent($content);

            $items->add($item);
        }

        return $items;
    }
}
