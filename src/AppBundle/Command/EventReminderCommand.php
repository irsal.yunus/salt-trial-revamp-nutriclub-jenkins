<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/06/2020
 * Time: 15:29
 */

namespace AppBundle\Command;

use AppBundle\Api\v1\Lixus\Lixus;
use AppBundle\Model\DataObject\Event;
use Carbon\Carbon;
use Pimcore\Console\AbstractCommand;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\EventParticipant;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EventReminderCommand extends AbstractCommand
{
    /** @var Lixus $lixus */
    protected $lixus;

    protected $event;

    public function __construct(string $name = null)
    {
        $this->lixus = new Lixus();

        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('event:reminder')
            ->setDescription('Event Reminder');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currentTime = Carbon::now()->timestamp;

        $today = Carbon::today()->timestamp;
        $tomorrow = Carbon::tomorrow()->timestamp;

        $eventLists = new Event\Listing();

        $eventLists->setCondition('startDate BETWEEN ? AND ?', [
            $today,
            $tomorrow
        ]);

        $eventLists->load();

        if ($eventLists->getCount() < 1) {
            $output->writeln('Empty data');

            return;
        }

        /** @var Event $object */
        foreach ($eventLists->getObjects() as $object) {
            $startDateTime = $object->getStartDate()->addHours(-2)->timestamp;

            if ($object->getIsReminder()) {
                continue;
            }

            if ($currentTime < $startDateTime) {
                continue;
            }

            $this->event = $object;

            $participants = $object->getParticipants();

            Simple::log($object->getSlug(), 'Total Participants : ' . count($participants));

            $this->goReminderParticipants($participants);

            $object->setIsReminder(true);

            try {
                $object->save([
                    'versionNote' => 'EVENT_REMINDER_COMMAND'
                ]);
            } catch (\Exception $exception) {
                Simple::log('EVENT_REMINDER_SAVE_FAILED', $exception);
            }
        }
    }

    private function goReminderParticipants(array $getParticipants)
    {
        if (!$getParticipants) {
            return null;
        }

        /** @var Event $event */
        $event = $this->event;
        $eventSlug = $event->getSlug();

        $eventTitle = $event->getTitle() ?? '';

        $linkWebinar = $event->getLinkWebinar() ? $event->getLinkWebinar()->getHref() : null;

        foreach ($getParticipants as $participant) {
            if (!$participant instanceof EventParticipant) {
                continue;
            }

            $friendlyNameShort = $participant->getGender() === 'female' ? 'Ma' : 'Pa';

            $messages = sprintf(
                '%s mulai 2 jam lagi! Ditunggu ya %s',
                $eventTitle, $friendlyNameShort
            );

            $messages = $linkWebinar ? $messages . ' : ' . $linkWebinar : $messages;

            $status = $this->lixus->sendSms($participant->getPhone(), $messages) ? 'Success' : 'Failed';
            $messages = $status . ' SEND SMS To : ' . $participant->getPhone();
            Simple::log($eventSlug, $messages);
        }
    }
}
