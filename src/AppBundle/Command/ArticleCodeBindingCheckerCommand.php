<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 26/02/2020
 * Time: 13:44
 */

namespace AppBundle\Command;

use AppBundle\Model\DataObject\Article;
use Pimcore\Console\AbstractCommand;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\Fieldcollection\Data\ArticleChild;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ArticleCodeBindingCheckerCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('article:codebinding:checker')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('filepath', 'p', InputOption::VALUE_REQUIRED)
                ])
            )
            ->setDescription('Code binding checker in wysiwyg at detail article parent content or child');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $input->getOption('filepath');
        if (!$filePath) {
            $output->write('Nothing to do file not found, check --help for more information');
        }

        $filePath = PIMCORE_PROJECT_ROOT . $filePath;

        if (!$isExist = file_exists($filePath)) {
            $output->writeln('File : "' . $filePath . '" does not exist');
        }

        $fileContent = file_get_contents($filePath);

        if (!is_json($fileContent)) {
            return;
        }

        $decodeContent = json_decode($fileContent, true);

        $count = count($decodeContent);

        if (!$count) {
            return;
        }

        foreach ($decodeContent as $item) {
            /** @var Article $article */
            $article = Article::getBySlug($item['slug'], 1);
            if (!$article) { return; }

            if ($item['countWidgetOnContentChild'] === 0 &&
                $item['countChildContent'] > 0 &&
                $item['countWidgetOnContentParent'] > 0)
            {
                if ($item['countParagraphOnChildOne'] > 0) {
                    $this->addWidget($article, 0);
                }
            }
        }
    }

    private function addWidget(Article $article, $indexAddToChild)
    {
        $contentChild = $article->getChild() ? $article->getChild()->getItems() : [];

        /** @var ArticleChild $item */
        foreach ($contentChild as $key => $item) {

            $findParagraph = preg_match_all("(<p>.+?</p>)", $item->getContent(), $matches);

            if (!$findParagraph) { return; }

            if ($indexAddToChild === $key) {
                $addWidgetParagraphTwo = $matches[0][1] . '<p>{{WIDGET_PARAGRAPH_TWO}}</p>';
                $item->setContent(str_replace($matches[0][1], $addWidgetParagraphTwo, $item->getContent()));
            }
        }
        try {
            $article->save();
        } catch (\Exception $e) {
            Simple::log('ERROR_CODE_BINDING_CHECKER_COMMAND', $e->getMessage() . ' On Object ID : ' . $article->getId());
        }
    }
}
