<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/07/2020
 * Time: 01:13
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="product__detail-footer">
    <div class="container pb-5 text-center">
        <div class="row">
            <div class="col-12 col-lg-10 mx-lg-auto">
        <?= $this->wysiwyg('footer-text')->frontend() ?>
        </div>
        </div>
    </div>
</div>
