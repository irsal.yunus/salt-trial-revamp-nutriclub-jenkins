<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/07/2020
 * Time: 19:55
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Tabs : </h4>
<?php $no = 1; ?>
<?php while($this->block('tab-product-detail')->loop()) { ?>
    <h4>Tab Name <?= $no ?>:</h4>
    <?= $this->input('tab-name', []) ?>
    <?php $no++; ?>

    <h4>Tab Name <?= $no ?>:</h4>
    <?= $this->input('tab-name', []) ?>
    <?php $no++; ?>
<?php } ?>
