<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/07/2020
 * Time: 19:54
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$temp = '';
?>

<div class="nutriclub-content-detail-product">
    <!-- Tab Panel -->
    <div class="container">
        <nav class="nav justify-content-center">
            <div class="nav nav-tabs" role="tablist">
                <?php while($this->block('tab-product-detail')->loop()) { ?>
                    <?php $tabName = $this->input('tab-name')->frontend() ?>
                    <?php $tabNameId = strtolower(str_replace(' ', '-', $tabName)) ?>
                    <a class="nav-item nav-link" id="nav-<?= $tabNameId ?>-tab" data-toggle="tab" href="#nav-<?= $tabNameId ?>" role="tab" aria-controls="nav-<?= $tabNameId ?>" aria-selected="true">
                        <?= $tabName ?>
                    </a>
                <?php } ?>
            </div>
        </nav>
    </div>
    <div class="tab-content tab-detail-product">
        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <div class="container-fluid">
                <div class="row justify-content-center bg-panel py-5">
                    <div class="col-12 mx-auto text-center">
                        <div class="img-iner-tab">
                            <img src="/assets/images/page_product/Actiduobio.png" class="img-fluid" />
                            <p class="my-3">Keuntungan Ganda dari Lactamil ACTIDuobio+</p>
                        </div>
                    </div>
                    <div class="col-lg-3 text-center">
                        <div class="content-pertab">
                            <img src="/assets/images/page_product/Asset12.png" class="img-fluid" />
                            <p>Ketahanan Tubuh <span>Asam Folat, DHA & Omega 6</span></p>
                        </div>
                    </div>
                    <div class="col-lg-3 text-center">
                        <div class="content-pertab">
                            <img src="/assets/images/page_product/Asset11.png" class="img-fluid" />
                            <p>Untuk Ketahanan Tubuh <span>Vit C, Vit D3, Vit E & Sumber Protein</span</p>
                        </div>
                    </div>
                    <div class="col-lg-3 text-center">
                        <div class="content-pertab">
                            <img src="/assets/images/page_product/Asset10.png" class="img-fluid" />
                            <p>Nutrisi Ganda untuk Mama dan si Kecil <span>Nutrisi Makro & Mikro</span></p>
                        </div>
                    </div>
                </div>
                <div class="row p-5">
                    <!-- if mobile -->
                    <div class="col-lg-6 mx-auto d-flex flex-wrap justify-content-center bg-white rounded-lg mx-4 py-5">
                        <div class="detail-product__box text-center">
                            <img src="/assets/images/page_product/Inisis_3D_Coklat_small.png">
                            <h5 class="my-2 product__box-title">Lactamil Inisis</h5>
                            <span>200g</span>
                        </div>
                        <div class="detail-product__box text-center">
                            <img src="/assets/images/page_product/Inisis_3D_Coklat_small.png">
                            <h5 class="my-2 product__box-title">Lactamil Inisis</h5>
                            <span>200g</span>
                        </div>
                        <div class="detail-product__box text-center mt-5 w-100">
                            <h5 class="my-2 product__box-title">Varian Rasa</h5>
                            <div class="flavor d-flex justify-content-center align-items-center">
                                <div class="flavor__box">&nbsp;</div>
                                <span class="ml-3">Coklat</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            <div class="container-fluid py-5 text-center bg-panel">
                <img src="/assets/images/page_product/lactamil-ingredients.png" class="img-fluid m-auto" />
            </div>
        </div>
        <div class="tab-pane fade serving" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 text-center bg-serving">
                        <img src="/assets/images/page_product/serving-suggestion_subpage-up.png" class="img-fluid m-auto" />
                    </div>
                    <div class="col-12 text-center bg-serving-dark order-first order-lg-last">
                        <img src="/assets/images/page_product/update-serving-suggestion_-subpage.png" class="img-fluid m-auto" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

