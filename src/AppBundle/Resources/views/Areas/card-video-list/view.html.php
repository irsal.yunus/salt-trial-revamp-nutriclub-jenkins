<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/03/2020
 * Time: 11:10
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Ramsey\Uuid\Uuid;
use AppBundle\Model\DataObject\ArticleVideo;
use Zend\Paginator\Paginator;

$formId = 'card-video__list-load-more-' . Uuid::uuid4();
$limit = 8;

$articleVideos = new ArticleVideo\Listing();
$reArrange = [];
$reArrangeAsExcept = [];
$reArrangeWithoutExcept = true;
if (!$this->block($this->prefixName . '-render-first-block')->isEmpty()) {
    $noRf = 0;
    while ($this->block($this->prefixName . '-render-first-block')->loop()) {
        if (!$this->relation($this->prefixName . '-render-first-' . $noRf)->isEmpty()) {
            /** @var ArticleVideo $articleVideos */
            $articleVideos = $this->relation($this->prefixName . '-render-first-' . $noRf)->getElement();
            $reArrange[] = $articleVideos;
            $reArrangeAsExcept[] = $articleVideos->getId();
        }
        $noRf++;
    }
}
if (!$this->block($this->prefixName . '-block')->isEmpty()) {
    $listId = [];
    $no = 0;
    while($this->block($this->prefixName . '-block')->loop()) {
        if (!$this->relation($this->prefixName . '-to-except-' . $no)->isEmpty()) {
            /** @var ArticleVideo $articleVideos */
            $articleVideos = $this->relation($this->prefixName . '-to-except-' . $no)->getElement();
            $listId[] =  $articleVideos->getId();
        }
        $no++;
    }
    $listId = array_unique(array_merge($listId, $reArrangeAsExcept));
    if (count($listId) > 0) {
        $articleVideos->addConditionParam('oo_id NOT IN('. implode(',', $listId) .')', null, 'AND');
        $reArrangeWithoutExcept = false;
    }
}
if ($reArrangeWithoutExcept && $reArrangeAsExcept) {
    $articleVideos->addConditionParam('oo_id NOT IN('. implode(',', $reArrangeAsExcept) .')', null, 'AND');
}
if (!$this->block($this->prefixName . '-block-only')->isEmpty()) {
    $listIdOnly = [];
    $no = 0;
    while($this->block($this->prefixName . '-block-only')->loop()) {
        if (!$this->relation($this->prefixName . '-to-only-' . $no)->isEmpty()) {
            /** @var ArticleVideo $articleVideos */
            $articleVideos = $this->relation($this->prefixName . '-to-only-' . $no)->getElement();
            $listIdOnly[] =  $articleVideos->getId();
        }
        $no++;
    }
    if (count($listIdOnly) > 0) {
        $articleVideos->addConditionParam('oo_id IN('. implode(',', $listIdOnly) .')', null, 'AND');
    }
}
$articleVideos->setOrderKey('oo_id');
$articleVideos->setOrder('desc');
$articleVideos->setLimit($limit);
$articleVideos->load();

$paginator = new Paginator($articleVideos);
$paginator->setCurrentPageNumber(0);
$paginator->setItemCountPerPage($limit);

$videos = array_merge($reArrange, $articleVideos->getObjects());
?>

<div class="card-video">
    <div class="card-video__list">
        <div class="container">
            <form id="<?= $formId ?>">
                <input type="hidden" id="nextPage" name="nextPage" value="2" form-id="<?= $formId ?>">
                <input type="hidden" id="currentPageLimit" name="currentPageLimit" value="<?= $limit ?>">
            </form>
            <div class="video-preloader">
                <?= $this->render('Include/preloader.html.php') ?>
            </div>
           <div class="card-video__list-items row">
               <?= $this->render('Widgets/CardVideoList/list.html.php', [
                   'data' => $videos
               ]) ?>
           </div>
            <!-- pagination start -->
            <?= $this->render('Include/paging.html.php', get_object_vars($paginator->getPages())) ?>
            <!-- pagination end -->
        </div>
    </div>
</div>
