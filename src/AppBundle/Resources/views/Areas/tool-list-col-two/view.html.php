<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 20:56
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\Tool;

$tools = new Tool\Listing();

$name = $this->prefixName;
$reArrange = [];
$reArrangeAsExcept = [];
$reArrangeWithoutExcept = true;
if (!$this->block($name .'-render-first-block')->isEmpty()) {
    $noRf = 0;
    while ($this->block($name .'-render-first-block')->loop()) {
        if (!$this->relation($name . '-render-first-' . $noRf)->isEmpty()) {
            /** @var Tool $tool */
            $tool = $this->relation($name . '-render-first-' . $noRf)->getElement();
            $reArrange[] = $tool;
            $reArrangeAsExcept[] = $tool->getId();
        }
        $noRf++;
    }
}
if (!$this->block($name . '-block')->isEmpty()) {
    $listId = [];
    $no = 0;
    while($this->block($name . '-block')->loop()) {
        if (!$this->relation($name . '-stage-to-except-' . $no)->isEmpty()) {
            /** @var Tool $tool */
            $tool = $this->relation($name . '-stage-to-except-' . $no)->getElement();
            $listId[] =  $tool->getId();
        }
        $no++;
    }
    $listId = array_unique(array_merge($listId, $reArrangeAsExcept));
    if (count($listId) > 0) {
        $tools->addConditionParam('oo_id NOT IN('. implode(',', $listId) .')', null, 'AND');
        $reArrangeWithoutExcept = false;
    }
}
if ($reArrangeWithoutExcept && $reArrangeAsExcept) {
    $tools->addConditionParam('oo_id NOT IN('. implode(',', $reArrangeAsExcept) .')', null, 'AND');
}
if (!$this->block($name . '--block-only')->isEmpty()) {
    $listIdOnly = [];
    $no = 0;
    while($this->block($name . '--block-only')->loop()) {
        if (!$this->relation($name . '-stage-to-only-' . $no)->isEmpty()) {
            /** @var Tool $tool */
            $tool = $this->relation($name . '-stage-to-only-' . $no)->getElement();
            $listIdOnly[] =  $tool->getId();
        }
        $no++;
    }
    if (count($listIdOnly) > 0) {
        $tools->addConditionParam('oo_id IN('. implode(',', $listIdOnly) .')', null, 'AND');
    }
}
$tools->load();

$toolObjects = array_merge($reArrange, $tools->getObjects());
?>
<?php if ($toolObjects) { ?>
    <div class="tool-lists">
        <div class="tool-lists__wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12">
                        <div class="container">
                            <div class="row tool-lists__items">
                                <?php
                                    /** @var Tool $toolObject */
                                    foreach ($toolObjects as $toolObject) {
                                        $currentLink = $toolObject->getCurrentLink();
                                        $currentLink->setClass('tool-lists__card col-12 col-md-5');

                                        $text = '';
                                        $text .= '<div class="tool-lists__card-item row"><div class="col-3 col-md-3">';
                                        $text .= $toolObject->getIconThumbnail('default', [
                                            'disableWidthHeightAttributes' => true,
                                            'class' => 'icon-relisensi'
                                            ]);
                                        $text.= '</div>';
                                        $text .= '<div class="col-7 col-md-8 align-self-center pr-0">';
                                        $text .= $toolObject->getDescription() ?? '';
                                        $text .= '</div>';
                                        $text .= '<div class="col-2 col-md-1 align-self-center">';
                                        $text .= '<img src="/assets/images/icons/arrow-forward.png" alt="arrow-forward">';
                                        $text .= '</div>';
                                        $text .= '</div>';

                                        $currentLink->setText($text);

                                        echo $currentLink->getHtml(false);

                                        $text = null;
                                } ?>
                                <div class="tool-lists__card col-12 col-md-5"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
