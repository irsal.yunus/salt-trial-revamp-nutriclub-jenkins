<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 20:55
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$name = $this->prefixName;
?>

<h4>Render First :</h4>
<?php $noRf = 0; ?>
<?php while ($this->block($name .'-render-first-block')->loop()) { ?>

    <h4>Tool to Render First <?= $noRf ?></h4>
    <?= $this->relation($name . '-render-first-' . $noRf, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Tool'],
    ]) ?>

    <?php $noRf++; ?>
<?php } ?>

<h4>Except :</h4>
<?php $no = 0; ?>
<?php while($this->block($name . '-block')->loop()) { ?>

    <h4>Tool to Except <?= $no ?>:</h4>
    <?= $this->relation($name . '-stage-to-except-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Tool'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>

<h4>Only :</h4>
<?php $no = 0; ?>
<?php while($this->block($name . '--block-only')->loop()) { ?>

    <h4>Tool to Only <?= $no ?>:</h4>
    <?= $this->relation($name . '-stage-to-only-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Tool'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>
