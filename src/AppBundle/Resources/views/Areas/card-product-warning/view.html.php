<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/07/2020
 * Time: 01:47
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="content-detail_list">
    <div class="container-child">
        <div class="warning-desc">
            <?= $this->wysiwyg('message')->frontend()  ?>
            <?= $this->link('link')->frontend()  ?>
        </div>
    </div>
</div>
