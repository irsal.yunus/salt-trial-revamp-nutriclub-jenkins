<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Title :</h4>
<?= $this->input('title', []) ?>

<h4>Step :</h4>
<?php while ($this->block('step-cook')->loop()) { ?>
    <h4>Image :</h4>
    <?= $this->image('step-image') ?>

    <h4>Step N :</h4>
    <?= $this->input('step-n') ?>

    <h4>Description :</h4>
    <?= $this->wysiwyg('description') ?>

    <h4>Link :</h4>
    <?= $this->link('link') ?>
<?php } ?>
