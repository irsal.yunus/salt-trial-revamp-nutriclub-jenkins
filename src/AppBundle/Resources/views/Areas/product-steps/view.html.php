<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="content__tahapan py-3">
    <div class="container">
        <div class="row">
            <div class="col text-center py-4">
                <h3><?= $this->input('title')->frontend() ?></h3>
            </div>
        </div>
        <div class="row d-block d-lg-none">
            <?php while ($this->block('step-cook')->loop()) { ?>
                <div class="col-12">
                    <div class="tahapan__image position-relative">
                        <?= !$this->image('step-image')->isEmpty() ?
                            $this->image('step-image')
                                ->getThumbnail('step-image-thumbnail')
                                ->getHtml([
                                    'class' => 'w-100'
                                ]) : null
                        ?>
                        <div class="image-overlay position-absolute">
                            <div class="d-flex h-100 justify-content-center flex-column ml-50">
                                <h4><?= $this->input('step-n')->frontend() ?></h4>
                                <?= $this->wysiwyg('description', [
                                    'class' => 'mb-0'
                                ])->frontend() ?>
                                <?= $this->link('link')->frontend() ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row d-none d-lg-block">
            <?php while ($this->block('step-cook')->loop()) { ?>
                <div class="col-lg-10 mx-auto">
                    <div class="rounded tahapan__box image-overlay d-flex">
                        <div class="tahapan__image mr-lg-5">
                            <?= !$this->image('step-image')->isEmpty() ?
                                $this->image('step-image')
                                    ->getThumbnail('step-image-thumbnail')
                                    ->getHtml([
                                        'class' => 'w-100'
                                    ]) : null
                            ?>
                        </div>
                        <div class="tahapan__description align-self-center ml-lg-5">
                            <h4><?= $this->input('step-n')->frontend() ?></h4>
                            <?= $this->wysiwyg('description', [
                                'class' => 'mb-0'
                            ])->frontend() ?>
                            <?= $this->link('link')->frontend() ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
