<?php

/**
 * Created by PhpStorm.
 * User: Budi P (budi.perkasa@salt.co.id)
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$bannerDesktop = !$this->image('desktop-banner-free-sample')->isEmpty() ?
    $this->image('desktop-banner-free-sample')->getThumbnail('desktop-banner')->getHtml([
        'class' => 'd-none d-lg-block w-100'
    ]) : null;

$bannerMobile = !$this->image('mobile-banner-free-sample')->isEmpty() ?
    $this->image('mobile-banner-free-sample')->getThumbnail('mobile-banner')->getHtml([
        'class' => 'd-block d-lg-none w-100'
    ]) : null;
$freeSampleActivated = $this->product->getIsActiveFreeSample();
$url = $this->url('PRODUCT_FREE_SAMPLE', [
    'groupSlug' => $this->product->getGroup()->getSlug(),
    'productSlug' => $this->product->getSlug()
]);
?>
<div class="banner-free-sample__wrapper pt-5 pt-lg-0">
    <div class="container">
        <?php if($freeSampleActivated) { ?>
        <a href="<?= $url ?>">
            <?= $bannerMobile ?></a>
        <a href="<?= $url ?>"><?= $bannerDesktop ?></a>
        <?php } ?>
    </div>
</div>
