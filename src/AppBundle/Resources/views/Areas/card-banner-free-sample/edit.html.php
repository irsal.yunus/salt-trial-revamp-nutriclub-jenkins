<?php

/**
 * Created by PhpStorm.
 * User: Budi P (budi.perkasa@salt.co.id)
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

?>

<h4>Mobile Image :</h4>
<?= $this->image('mobile-banner-free-sample', [
    'uploadPath'    => '/banner-free-sample/'
]) ?>

<h4>Desktop Image :</h4>
<?= $this->image('desktop-banner-free-sample', [
    'uploadPath'    => '/banner-free-sample/'
])?>
