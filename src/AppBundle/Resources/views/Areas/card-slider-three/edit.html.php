<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:59
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title : </h4>
<?= $this->input('card-slider-three-title', [])?>

<h4>Link : </h4>
<?= $this->link('card-slider-three-link', [])?>

<h4>Render First :</h4>
<?php $noRf = 0; ?>
<?php while ($this->block('card-slider-three-render-first-block')->loop()) { ?>

    <h4>ArticleCategory to Render First <?= $noRf ?></h4>
    <?= $this->relation('card-slider-three-render-first-' . $noRf, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['ArticleCategory'],
    ]) ?>

    <?php $noRf++; ?>
<?php } ?>

<h4>Except :</h4>
<?php $no = 0; ?>
<?php while($this->block('card-slider-three-block')->loop()) { ?>

    <h4>ArticleCategory to Except <?= $no ?>:</h4>
    <?= $this->relation('card-slider-three-category-to-except-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['ArticleCategory'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>

<h4>Only :</h4>
<?php $no = 0; ?>
<?php while($this->block('card-slider-three-block-only')->loop()) { ?>

    <h4>ArticleCategory to Only <?= $no ?>:</h4>
    <?= $this->relation('card-slider-three-category-to-only-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['ArticleCategory'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>
