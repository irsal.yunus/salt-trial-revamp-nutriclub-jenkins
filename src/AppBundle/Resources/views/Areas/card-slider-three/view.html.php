<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:59
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Countly\Document\Areabrick\CardSliderThree\CountlyModel;
use AppBundle\Countly\LinkBuilder;
use AppBundle\Model\DataObject\ArticleCategory;

$articleCategories = new ArticleCategory\Listing();
$reArrange = [];
$reArrangeAsExcept = [];
$reArrangeWithoutExcept = true;
if (!$this->block('card-slider-three-render-first-block')->isEmpty()) {
    $noRf = 0;
    while ($this->block('card-slider-three-render-first-block')->loop()) {
        if (!$this->relation('card-slider-three-render-first-' . $noRf)->isEmpty()) {
            /** @var ArticleCategory $articleCategory */
            $articleCategory = $this->relation('card-slider-three-render-first-' . $noRf)->getElement();
            $reArrange[] = $articleCategory;
            $reArrangeAsExcept[] = $articleCategory->getId();
        }
        $noRf++;
    }
}
if (!$this->block('card-slider-three-block')->isEmpty()) {
    $listId = [];
    $no = 0;
    while($this->block('card-slider-three-block')->loop()) {
        if (!$this->relation('card-slider-three-category-to-except-' . $no)->isEmpty()) {
            /** @var ArticleCategory $articleCategory */
            $articleCategory = $this->relation('card-slider-three-category-to-except-' . $no)->getElement();
            $listId[] =  $articleCategory->getId();
        }
        $no++;
    }
    $listId = array_unique(array_merge($listId, $reArrangeAsExcept));
    if (count($listId) > 0) {
        $articleCategories->addConditionParam('oo_id NOT IN('. implode(',', $listId) .')', null, 'AND');
        $reArrangeWithoutExcept = false;
    }
}
if ($reArrangeWithoutExcept && $reArrangeAsExcept) {
    $articleCategories->addConditionParam('oo_id NOT IN('. implode(',', $reArrangeAsExcept) .')', null, 'AND');
}
if (!$this->block('card-slider-three-block-only')->isEmpty()) {
    $listIdOnly = [];
    $no = 0;
    while($this->block('card-slider-three-block-only')->loop()) {
        if (!$this->relation('card-slider-three-category-to-only-' . $no)->isEmpty()) {
            /** @var ArticleCategory $articleCategory */
            $articleCategory = $this->relation('card-slider-three-category-to-only-' . $no)->getElement();
            $listIdOnly[] =  $articleCategory->getId();
        }
        $no++;
    }
    if (count($listIdOnly) > 0) {
        $articleCategories->addConditionParam('oo_id IN('. implode(',', $listIdOnly) .')', null, 'AND');
    }
}
$articleCategories->setLimit(6);
$articleCategories->load();

$stages = array_merge($reArrange, $articleCategories->getObjects());
?>
<?php if ($articleCategories) { ?>
<div class="card-slider-three mt-5">
    <div class="container">
        <div class="row mb-2">
            <div class="col-7 col-md-6 align-self-center">
                <h2 class="heading-blue m-0"><?= $this->input('card-slider-three-title')->getData() ?></h2>
            </div>
            <div class="col-5 col-md-6 text-right">
                <?= $this->link('card-slider-three-link', [
                    'class' => 'see-more'
                ])->frontend() ?>
            </div>
        </div>

        <div class="card-slider-three__wrapper">
            <?php
                /** @var ArticleCategory $object */
            foreach ($articleCategories as $object) {
                $url = $object->getRouter();

                $imgIcon = $object->getIconThumbnail('card-slider-three-icon');

                $name = $object->getName() ?? '';
                $customAttrs = $this->websiteConfig('ARTICLE_CATEGORY_ATTRIBUTE_LINK', null);
                ?>
                <?= LinkBuilder::start($object, new CountlyModel(), 'card-slider-three__item', $customAttrs) ?>
                    <?= $imgIcon ?>
                    <h5><?= $name ?></h5>
                    <span class="card-slider-three__gradient"></span>
                <?= LinkBuilder::stop() ?>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<?php } ?>
