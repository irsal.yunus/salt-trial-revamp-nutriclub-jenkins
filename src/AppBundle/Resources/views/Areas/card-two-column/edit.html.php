<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 14:13
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title :</h4>
<?= $this->input('card-two-columns-title', []) ?>

<h4>Content Title 1 :</h4>
<?= $this->input('card-two-columns-content-title-1', []) ?>

<h4>Content Image 1 :</h4>
<?= $this->image('card-two-columns-content-image-1', []) ?>

<h4>Content Link 1 :</h4>
<?= $this->link('card-two-columns-content-link-1', []) ?>

<h4>Content Title 2 :</h4>
<?= $this->input('card-two-columns-content-title-2', []) ?>

    <h4>Content Image 2 :</h4>
<?= $this->image('card-two-columns-content-image-2', []) ?>

    <h4>Content Link 2 :</h4>
<?= $this->link('card-two-columns-content-link-2', []) ?>

<h4>Content Title 3 :</h4>
<?= $this->input('card-two-columns-content-title-3', []) ?>

<h4>Content Image 3 :</h4>
<?= $this->image('card-two-columns-content-image-3', []) ?>

<h4>Content Link 3 :</h4>
<?= $this->link('card-two-columns-content-link-3', []) ?>
