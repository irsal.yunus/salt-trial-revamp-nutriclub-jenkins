<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 14:13
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="card-two-column mt-5">
    <div class="container">
        <h2 class="heading-blue"><?= $this->input('card-two-columns-title')->getData() ?></h2>
        <div class="row">
            <?php
                if (
                    !$this->input('card-two-columns-content-title-1')->isEmpty() &&
                    !$this->image('card-two-columns-content-image-1')->isEmpty() &&
                    !$this->link('card-two-columns-content-link-1')->isEmpty()
                ) {

                    $img = $this->image('card-two-columns-content-image-1')
                        ->getThumbnail('default')
                        ->getHtml();

                    $imgDiv = '<div class="image-wrapper text-center">' . $img .'</div>';

                    $title = '<h5>'. $this->input('card-two-columns-content-title-1')->getData() .'</h5>';

                    $contentOne = $this->link('card-two-columns-content-link-1', [
                        'textPrefix' => $imgDiv . $title
                    ]);
                    ?>
                    <div class="col-4">
                        <?= $contentOne ?>
                    </div>
                    <?php
                }
            ?>
            <?php
            if (
                !$this->input('card-two-columns-content-title-2')->isEmpty() &&
                !$this->image('card-two-columns-content-image-2')->isEmpty() &&
                !$this->link('card-two-columns-content-link-2')->isEmpty()
            ) {

                $img = $this->image('card-two-columns-content-image-2')
                    ->getThumbnail('default')
                    ->getHtml();

                $imgDiv = '<div class="image-wrapper text-center">' .$img . '</div>';

                $title = '<h5>'. $this->input('card-two-columns-content-title-2')->getData() .'</h5>';

                $contentTwo = $this->link('card-two-columns-content-link-2', [
                    'textPrefix' => $imgDiv . $title
                ]);
                ?>
                <div class="col-4">
                    <?= $contentTwo ?>
                </div>
                <?php
            }
            ?>
            <?php
            if (
                !$this->input('card-two-columns-content-title-3')->isEmpty() &&
                !$this->image('card-two-columns-content-image-3')->isEmpty() &&
                !$this->link('card-two-columns-content-link-3')->isEmpty()
            ) {

                $img = $this->image('card-two-columns-content-image-3')
                    ->getThumbnail('default')
                    ->getHtml();

                $imgDiv = '<div class="image-wrapper text-center">' . $img . '</div>';

                $title = '<h5>'. $this->input('card-two-columns-content-title-3')->getData() .'</h5>';

                $contentThree = $this->link('card-two-columns-content-link-3', [
                    'textPrefix' => $imgDiv . $title
                ]);
                ?>
                <div class="col-4">
                    <?= $contentThree ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
