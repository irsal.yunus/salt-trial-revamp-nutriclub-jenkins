<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 12:12
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\Customer;

$user = $app->getUser();

if ($user) {
    /** @var Customer $userSso */
    $userSso = $user ? $this->getRequest()->getSession()->get('UserProfile') : null;
}

$gender = ($userSso && $userSso->getGender() === 'M') ? 'Papa' : 'Mama';

$profilePicture = $userSso ? $userSso->getProfilePicture() : null;
?>

<div class="card-member">
    <div class="card-member__wrapper text-center">
        <img class="profile-pic" src="<?= $profilePicture ?>">
        <img class="bg-layout d-none d-md-block" src="/assets/images/bg-profile-desktop.png">
        <img class="bg-layout d-block d-sm-none" src="/assets/images/bg-profile-mobile.png">
        <div class="content-wrapper">
            <h5>Hi <?= $gender ?> <?= $userSso ? $userSso->getFullname() : '' ?>,</h5>
            <p style="display: none">Selamat <?= $gender ?> adalah <span class="color-gold">Gold Member</span> dari <span class="color-black">My Nutriclub</span> program</p>
            <div class="row justify-content-center point-wrapper">
                <div class="col-5 col-md-4">
                    <p>Poin <?= $gender ?></p>
                </div>
                <div class="col-5 col-md-4">
                    <p class="point"><span><img src="/assets/images/point.png" alt=""></span> <?= number_format($userSso ? $userSso->getPoint() : 0) ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
