<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 12:37
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title :</h4>
<?= $this->input($this->prefixName . '-title', [

]) ?>

<h4>Sub Title :</h4>
<?= $this->wysiwyg($this->prefixName . '-sub-title', [

]) ?>

<h4>Link :</h4>
<?= $this->link($this->prefixName . '-link', [

]) ?>

<h4>Banner Desktop :</h4>
<?= $this->image($this->prefixName . '-banner-desktop', [

]) ?>

<h4>Banner Mobile :</h4>
<?= $this->image($this->prefixName . '-banner-mobile', [

]) ?>

<h4>Banner Tablet :</h4>
<?= $this->image($this->prefixName . '-banner-table', [

]) ?>
