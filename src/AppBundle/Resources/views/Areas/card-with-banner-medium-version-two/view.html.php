<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 12:37
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="banner-medium">
    <?php
    if (!$this->image($this->prefixName . '-banner-desktop')->isEmpty()) {
        echo $this->image($this->prefixName . '-banner-desktop')
            ->getThumbnail($this->prefixName . '-banner-desktop')
            ->getHtml([
                'class' => 'banner-medium__two',
                'disableWidthHeightAttributes' => true
            ]);
    }
    ?>
    <div class="banner-medium__description">
        <h2><?= $this->input($this->prefixName . '-title')->getData() ?? '' ?></h2>
        <?= $this->wysiwyg($this->prefixName . '-sub-title')->getData() ?? '' ?>
        <?= $this->link($this->prefixName . '-link', [
            'class' => 'btn btn-blue'
        ])->frontend() ?>
    </div>
</div>
