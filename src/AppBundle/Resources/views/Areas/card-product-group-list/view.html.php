<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:32
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="content-porduct_section_2 bg-acean">
    <div class="container">
        <div class="d-sm-flex">
            <?php while ($this->block('product-group-block')->loop()) { ?>
                <?php
                if ($this->relation('product-group')->isEmpty()) {
                    continue;
                }

                /** @var AppBundle\Model\DataObject\ProductGroup $productGroup */
                $productGroup = $this->relation('product-group')->getElement();
                ?>
                <div class="col-md-4">
                    <a href="<?= $productGroup->getRouter() ?>">
                        <div class="card-product">
                            <div class="img-div">
                                <?= $productGroup->getImgIcon() ?
                                    $productGroup->getImgIcon()
                                        ->getThumbnail('product-group-image-icon')
                                        ->getHtml([
                                            'disableWidthHeightAttributes' => true,
                                            'class' => 'img-fluid'
                                        ]) : null
                                ?>
                            </div>
                            <div class="desc-div">
                                <h2 class="name-product">
                                    <?= $productGroup->getTitle() ?>
                                </h2>
                                <?= $productGroup->getDescription() ?>
                                <span class="text-blue">
                                    <?= $this->t('PRODUCT_GROUP_MORE_DETAIL') ?>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
