<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:32
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<?php $no = 1; ?>
<?php while ($this->block('product-group-block')->loop()) { ?>
    <h4>Product Group : <?= $no ?></h4>
    <?= $this->relation('product-group', [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['ProductGroup'],
    ]) ?>
    <?php $no++; ?>
<?php } ?>
