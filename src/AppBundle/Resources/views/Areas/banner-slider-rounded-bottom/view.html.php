<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/02/2020
 * Time: 17:58
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>
<div class="rounded-tools">
    <?php
        if (!$this->image('banner-slider-rounded-bottom-banner-desktop')->isEmpty()) {
            echo $this->image('banner-slider-rounded-bottom-banner-desktop')
                ->getThumbnail('banner-slider-rounded-bottom-banner-desktop-thumbnail')
                ->getHtml([
                    'disableWidthHeightAttributes' => true,
                    'class' => 'rounded-tools__content-figure'
                ]);
        }
    ?>
    <div class="rounded-tools__wrapper">
        <div class="rounded-tools__content">
            <h3><?= $this->input('banner-slider-rounded-bottom-title')->frontend() ?></h3>
            <?= $this->wysiwyg('banner-slider-rounded-bottom-description') ?>
        </div>
    </div>
</div>
