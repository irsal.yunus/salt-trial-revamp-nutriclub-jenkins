<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/02/2020
 * Time: 17:58
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<h4>Banner Desktop :</h4>
<?= $this->image('banner-slider-rounded-bottom-banner-desktop', []) ?>

<h4>Banner Mobile :</h4>
<?= $this->image('banner-slider-rounded-bottom-banner-mobile', []) ?>

<h4>Banner Tablet :</h4>
<?= $this->image('banner-slider-rounded-bottom-banner-tablet', []) ?>

<h4>Title :</h4>
<?= $this->input('banner-slider-rounded-bottom-title', []) ?>

<h4>Description :</h4>
<?= $this->wysiwyg('banner-slider-rounded-bottom-description', []) ?>
