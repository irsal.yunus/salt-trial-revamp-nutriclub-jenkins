<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/03/2020
 * Time: 11:10
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
use Ramsey\Uuid\Uuid;
use AppBundle\Model\DataObject\ArticlePodcast;
use Zend\Paginator\Paginator;

$formId = 'card-podcast__list-load-more-' . Uuid::uuid4();
$limit = 4;

$articlePodcast = new ArticlePodcast\Listing();
$reArrange = [];
$reArrangeAsExcept = [];
$reArrangeWithoutExcept = true;
if (!$this->block($this->prefixName . '-render-first-block')->isEmpty()) {
    $noRf = 0;
    while ($this->block($this->prefixName . '-render-first-block')->loop()) {
        if (!$this->relation($this->prefixName . '-render-first-' . $noRf)->isEmpty()) {
            /** @var ArticlePodcast $articlePodcast */
            $articlePodcast = $this->relation($this->prefixName . '-render-first-' . $noRf)->getElement();
            $reArrange[] = $articlePodcast;
            $reArrangeAsExcept[] = $articlePodcast->getId();
        }
        $noRf++;
    }
}
if (!$this->block($this->prefixName . '-block')->isEmpty()) {
    $listId = [];
    $no = 0;
    while($this->block($this->prefixName . '-block')->loop()) {
        if (!$this->relation($this->prefixName . '-to-except-' . $no)->isEmpty()) {
            /** @var ArticlePodcast $articlePodcast */
            $articlePodcast = $this->relation($this->prefixName . '-to-except-' . $no)->getElement();
            $listId[] =  $articlePodcast->getId();
        }
        $no++;
    }
    $listId = array_unique(array_merge($listId, $reArrangeAsExcept));
    if (count($listId) > 0) {
        $articlePodcast->addConditionParam('oo_id NOT IN('. implode(',', $listId) .')', null, 'AND');
        $reArrangeWithoutExcept = false;
    }
}
if ($reArrangeWithoutExcept && $reArrangeAsExcept) {
    $articlePodcast->addConditionParam('oo_id NOT IN('. implode(',', $reArrangeAsExcept) .')', null, 'AND');
}
if (!$this->block($this->prefixName . '-block-only')->isEmpty()) {
    $listIdOnly = [];
    $no = 0;
    while($this->block($this->prefixName . '-block-only')->loop()) {
        if (!$this->relation($this->prefixName . '-to-only-' . $no)->isEmpty()) {
            /** @var ArticlePodcast $articlePodcast */
            $articlePodcast = $this->relation($this->prefixName . '-to-only-' . $no)->getElement();
            $listIdOnly[] =  $articlePodcast->getId();
        }
        $no++;
    }
    if (count($listIdOnly) > 0) {
        $articlePodcast->addConditionParam('oo_id IN('. implode(',', $listIdOnly) .')', null, 'AND');
    }
}
$articlePodcast->setOrderKey('oo_id');
$articlePodcast->setOrder('desc');
$articlePodcast->setLimit($limit);
$articlePodcast->load();

$paginator = new Paginator($articlePodcast);
$paginator->setCurrentPageNumber(0);
$paginator->setItemCountPerPage($limit);

$podcast = array_merge($reArrange, $articlePodcast->getObjects());
?>
<div class="podcast">
    <div class="container podcast__container pr-md-0">
        <form id="<?= $formId ?>">
            <input type="hidden" id="nextPage" name="nextPage" value="2" form-id="<?= $formId ?>">
            <input type="hidden" id="currentPageLimit" name="currentPageLimit" value="<?= $limit ?>">
        </form>
        <div class="podcast-preloader">
            <?= $this->render('Include/preloader.html.php') ?>
        </div>
        <div class="podcast__list">
            <?php
            echo $this->render('Widgets/CardPodcastList/list.html.php', [
                'data' => $podcast
            ])
            ?>
        </div>
        <?= $this->render('Include/paging.html.php', get_object_vars($paginator->getPages())) ?>
    </div>
</div>
