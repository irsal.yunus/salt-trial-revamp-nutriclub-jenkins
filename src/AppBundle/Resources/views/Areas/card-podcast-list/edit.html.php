<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/03/2020
 * Time: 11:10
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Render First :</h4>
<?php $noRf = 0; ?>
<?php while ($this->block($this->prefixName . '-render-first-block')->loop()) { ?>

    <h4>ArticlePodcast to Render First <?= $noRf ?></h4>
    <?= $this->relation($this->prefixName . '-render-first-' . $noRf, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['ArticlePodcast'],
    ]) ?>

    <?php $noRf++; ?>
<?php } ?>

<h4>Except :</h4>
<?php $no = 0; ?>
<?php while($this->block($this->prefixName . '-block')->loop()) { ?>

    <h4>ArticlePodcast to Except <?= $no ?>:</h4>
    <?= $this->relation($this->prefixName . '-to-except-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['ArticlePodcast'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>

<h4>Only :</h4>
<?php $no = 0; ?>
<?php while($this->block($this->prefixName . '-block-only')->loop()) { ?>

    <h4>ArticlePodcast to Only <?= $no ?>:</h4>
    <?= $this->relation($this->prefixName . '-to-only-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['ArticlePodcast'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>
