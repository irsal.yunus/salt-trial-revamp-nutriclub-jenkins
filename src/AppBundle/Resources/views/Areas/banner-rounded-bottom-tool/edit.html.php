<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 20:51
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$name = $this->prefixName;
$no = 1;
?>

<h4>Title </h4>
<?= $this->input($name . '-title-' . $no, [

]) ?>
<h4>Description</h4>
<?= $this->wysiwyg($name . '-description-' . $no, [

]) ?>

<h4>Dekstop Banner </h4>
<?= $this->image($name . '-desktop-banner-' . $no, [
    "title" => "Drag your image here",
    'thumbnail'   => $name . '-desktop-banner-thumbnail',
    'uploadPath'  => '/banner-slider/desktop/',
    'reload'      => false,
    'hidetext'    => true
])
?>
<h4>Tablet Banner </h4>
<?= $this->image($name . '-tablet-banner-' . $no, [
    "title" => "Drag your image here",
    'thumbnail'   => $name . '-tablet-banner-thumbnail',
    'uploadPath'  => '/banner-slider/tablet/',
    'reload'      => false,
    'hidetext'    => true
])
?>
<h4>Mobile Banner </h4>
<?= $this->image($name . '-mobile-banner-' . $no, [
    "title" => "Drag your image here",
    'thumbnail'   => $name . '-mobile-banner-thumbnail',
    'uploadPath'  => '/banner-slider/mobile/',
    'reload'      => false,
    'hidetext'    => true
])
?>
