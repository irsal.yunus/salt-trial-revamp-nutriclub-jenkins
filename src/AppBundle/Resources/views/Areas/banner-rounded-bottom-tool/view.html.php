<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 20:51
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$name = $this->prefixName;
$no = 1;
?>

<div class="rounded-tools">
    <?php
    // example image for background rounded bottom tool
    $imgDekstop = '<img src="/assets/images/banner/banner-ebook.png" alt="banner-rounded-tool" class="rounded-tools__content-figure">';
    if (!$this->image($name . '-desktop-banner-' . $no)->isEmpty()) {
        $imgDekstop = $this->image($name . '-desktop-banner-' . $no)
            ->getThumbnail($this->prefixName . '-desktop')
            ->getHtml([
                'class' => 'rounded-tools__content-figure',
                'disableWidthHeightAttributes' => true
            ]);
    }
    echo $imgDekstop;
    ?>
    <div class="rounded-tools__wrapper">
        <div class="rounded-tools__content">
            <h3><?= $this->input($name . '-title-' . $no)->getData() ?? '' ?></h3>
            <?= $this->wysiwyg($name . '-description-' . $no)->getData() ?? '' ?>
        </div>
    </div>
</div>
