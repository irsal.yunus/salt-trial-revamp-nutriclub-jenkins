<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/07/2020
 * Time: 01:55
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="content-detail_list">
    <div class="container-child pb-3">
        <div class="short-desc">
            <?= $this->wysiwyg('short-description-quality')->frontend() ?>
        </div>
    </div>
</div>
