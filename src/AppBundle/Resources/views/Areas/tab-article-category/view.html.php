<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 17:38
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Countly\Document\Areabrick\TabArticleCategory\CountlyModel;
use AppBundle\Countly\LinkBuilder;
use AppBundle\Model\DataObject\ArticleCategory;

$articleCategories = new ArticleCategory\Listing();
$articleCategories->load();

$default = null;
$defaultUrl = $this->path('ARTICLE_CATEGORY', [
    'slug' => null
]);
if (!$this->relation('tab-article-category-default')->isEmpty()) {
    /** @var ArticleCategory $default */
    $default = $this->relation('tab-article-category-default')->getElement();
}

$customAttrs = $this->websiteConfig('ARTICLE_CATEGORY_ATTRIBUTE_LINK', null);
?>


<?php if ($articleCategories->getCount() > 0) { ?>
<div class="filter">
    <div class="container">
    <div class="filter__article">
        <div class="filter__item">
            <a href="<?= $defaultUrl ?>" class="<?= !$default ? 'item-active' : '' ?>" data-name="Semua" <?= $customAttrs ?>>
                Semua
            </a>
        </div>
        <?php
            /** @var ArticleCategory $object */
            foreach ($articleCategories->getObjects() as $object) {
                $url = $object->getRouter();

                $name = $object->getName();
        ?>
                <div class="filter__item">
                    <?= LinkBuilder::start($object, new CountlyModel(), $default === $object ? 'item-active' : '', $customAttrs) ?>
                        <?= $name ?>
                    <?= LinkBuilder::stop() ?>
                </div>
        <?php
            }
        ?>
    </div>
    </div>
</div>
<?php } ?>
