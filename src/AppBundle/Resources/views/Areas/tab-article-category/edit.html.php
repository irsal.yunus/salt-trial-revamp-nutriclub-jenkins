<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 17:38
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Default :</h4>
<?= $this->relation('tab-article-category-default', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['ArticleCategory'],
]) ?>
