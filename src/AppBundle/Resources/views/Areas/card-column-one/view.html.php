<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 13:27
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\ArticleCalendar;

if (($articleCalendar = $this->articleCalendar) && (count($articleCalendar) > 0)) { ?>
    <?php
    /** @var ArticleCalendar $articleCalendar */
    $articleCalendar = $articleCalendar[0];
    ?>
<div class="card-column-one">
    <div class="liner"></div>
    <div class="card-column-one__wrapper">
        <div class="row align-items-center">
            <div class="col left-content">
                <h5 class="color-blue"><?= $articleCalendar->getTitle() ?></h5>
                <?= $articleCalendar->getContent() ?>
                <a href="<?= $articleCalendar->getLink() ? $articleCalendar->getLink()->getHref() : '#' ?>">
                    <span><?= $this->t('READ_MORE') ?></span>
                </a>
            </div>
            <div class="col right-content">
                <?= ($articleCalendar && $articleCalendar->getImageDesktop()) ? $articleCalendar->getImageDesktop()->getThumbnail('default')->getHtml() : null ?>
            </div>
        </div>
    </a>
</div>
<?php } ?>
