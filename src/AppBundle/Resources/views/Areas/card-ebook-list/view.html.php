<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/03/2020
 * Time: 11:10
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\ArticleEbook;
use Ramsey\Uuid\Uuid;
use Zend\Paginator\Paginator;

$formId = 'card-video__list-load-more-' . Uuid::uuid4();
$limit = 4;

/** @var Paginator $ebook */
$ebook = $this->ebook;
?>

<div class="ebook">
    <div class="container ebook__container">
        <form id="<?= $formId ?>">
            <input type="hidden" id="nextPage" name="nextPage" value="2" form-id="<?= $formId ?>">
            <input type="hidden" id="currentPageLimit" name="limit" value="<?= $limit ?>">
        </form>
        <div class="ebook-preloader">
                    <?= $this->render('Include/preloader.html.php') ?>
        </div>
        <div class="ebook__list">
            <?php

            echo $this->render('Widgets/CardEbookList/list.html.php', [
                'data' => $ebook
            ])
            ?>
        </div>
         <!-- pagination start -->
         <?= $this->render('Include/paging.html.php', get_object_vars($ebook->getPages())) ?>
            <!-- pagination end -->
    </div>
</div>
