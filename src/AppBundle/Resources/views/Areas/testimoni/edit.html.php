<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Testimoni</h4>
<h5>Title</h5>
<?= $this->input('title-testimoni', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>

<h5>Content Testimoni</h5>
<?php $no = 1; ?>
<?php while($this->block('content-testimoni')->loop()) { ?>
<h6>Avatar</h6>
<?= $this->image('testimoni-avatar', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'testimoni-avatar-thumbnail',
    'uploadPath'    => '/testimoni/',
    'reload'        => false,
    'hidetext'      => true
]) ?>
<h6>Name / Alias</h6>
<?= $this->input('testimoni-name', [
    'htmlspecialchars'  => false,
    'placeholder'   => 'Input your text here'
]) ?>
<h6>Months of Pregnancy</h6>
<?= $this->input('testimoni-months-pregnancy', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>
<h6>Testimoni Description</h6>
<?= $this->wysiwyg('testimoni-description', []) ?>
<h6>Star :</h6>
<?= $this->numeric('star', []) ?>
<?php $no++; ?>
<?php } ?>
