<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="testimoni-wrapper py-3">
    <div class="container mb-5">
        <div class="row justify-content-center py-4">
            <h3><?= $this->input('title-testimoni')->frontend() ?></h3>
        </div>
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <div class="testimoni">
                    <?php while($this->block('content-testimoni')->loop()) { ?>
                        <div class="mx-3 bg-white rounded-lg p-3 p-lg-4">
                            <div class="content_testimoni">
                                <div class="content_testimoni__header d-flex">
                                    <div class="content_testimoni__header-image mr-3">
                                        <?= !$this->image('testimoni-avatar')->isEmpty() ?
                                            $this->image('testimoni-avatar')
                                                ->getThumbnail('testimoni-avatar-thumbnail')
                                                ->getHtml([
                                                    'disableWidthHeightAttributes' => true,
                                                    'class' => 'w-100'
                                                ]) : null
                                        ?>
                                    </div>
                                    <div class="content_testimoni__title">
                                        <h2 class="my-0"><?= $this->input('testimoni-name')->frontend() ?></h2>
                                        <span><?= $this->input('testimoni-months-pregnancy')->frontend() ?></span>
                                        <div class="rating d-flex">
                                            <?php
                                            $start = $this->numeric('star')->getData() ?? 0;
                                            for ($i = 0; $i < $start; $i++) {
                                                ?>
                                                <img src="/assets/images/page_product/star.svg" alt="Product Star" />
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="content_testimoni__description pt-3">
                                    <?= $this->wysiwyg('testimoni-description', [
                                        'class' => 'trimmed'
                                    ])->frontend() ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
