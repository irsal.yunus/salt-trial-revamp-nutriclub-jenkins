<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:53
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Helper\CardArticleWidgetHelper;
use AppBundle\Model\DataObject\Article;
use Pimcore\Model\DataObject\{ArticleCategory,
    ArticleRoleFocus,
    ArticleSubCategory,
    Stage,
    StageAge,
    StageGroup,
    SubStage};
use Ramsey\Uuid\Uuid;

/** @var ArticleRoleFocus $roleFocus */
$roleFocus = !$this->relation('relation-role-focus')->isEmpty() ?
    $this->relation('relation-role-focus')->getElement() : null;

/** @var ArticleCategory $articleCategory */
$articleCategory = !$this->relation('relation-article-category')->isEmpty() ?
    $this->relation('relation-article-category')->getElement() : null;

/** @var ArticleSubCategory $articleSubCategory */
$articleSubCategory = !$this->relation('relation-article-sub-category')->isEmpty() ?
    $this->relation('relation-article-sub-category')->getElement() : null;

/** @var Stage $stage */
$stage = !$this->relation('relation-stage')->isEmpty() ?
    $this->relation('relation-stage')->getElement() : null;

/** @var StageGroup $stageGroup */
$stageGroup = !$this->relation('relation-stage-group')->isEmpty() ?
    $this->relation('relation-stage-group')->getElement() : null;

/** @var SubStage $subStage */
$subStage = !$this->relation('relation-sub-stage')->isEmpty() ?
    $this->relation('relation-sub-stage')->getElement() : null;

/** @var StageAge $stageAge */
$stageAge = !$this->relation('relation-stage-age')->isEmpty() ?
    $this->relation('relation-stage-age')->getElement() : null;

/** @var Article\Listing $articleObject */
$articleObject = CardArticleWidgetHelper::getObjectWithfilter(
    $roleFocus,
    $articleCategory,
    $articleSubCategory,
    $stage,
    $stageGroup,
    $subStage,
    $stageAge,
    1,
    4
);
if ($stageGroup) {
    $articleObjectn = [];
    foreach ($articleObject as $obj) {
        $articleObjectn[] = $obj;
    }

    $articleObject = array_merge([], ...$articleObjectn);
}
if (!$stageGroup) {
    $articleObject;
}

$disableTrim = !$this->checkbox('card-slider-two-disable-trim-title')->getData();
?>
<?php if ($articleObject) { ?>
<div class="card-slider-two mt-5">
    <div class="container">
        <div class="row mb-4">
            <div class="col-6 col-md-6 align-self-center">
                <h2 class="card-slider-two__text"><?= $this->input('card-slider-two-title')->getData() ?? '' ?></h2>
            </div>
            <div class="col-6 col-md-6 text-right">
                <?= $this->link('card-slider-two-link', [
                    'class' => 'see-more'
                ])->frontend() ?>
            </div>
        </div>

        <div class="card-slider-two__wrapper">
            <?php
            /** @var Article $object */
            foreach ($articleObject as $object) {
                $category = $object->getCategory();
                $url = $object->getRouter();
                $title = $object->getTitleTrim($disableTrim);
                $checkImg = $object->getImgDesktop() ?
                    $object->getImgDesktop()->getThumbnail('default')->getHtml() : null;
                $img = $checkImg ?? '<img src="https://via.placeholder.com/150"></img>';

                $generatedLink = $object->getGeneratedLink();

                $text = '<div class="card-slider-two__copy">';
                $text .= '<div class="card-slider-two__text">';
                $text .= '<p>'. $title .'</p>';
                $text .= '</div>';
                $text .= '<div class="card-slider-two__author">';
                $text .= '<p>'. 'By Lactamil'. '</p>';
                $text .= '</div>';
                $text .= '</div>';

                $linkClass = $generatedLink->getClass();
                $generatedLink->setClass( 'card-slider-two__item' . $linkClass);
                $generatedLink->setText($img . $text);

                echo $generatedLink->getHtml(false);
            }
            ?>
        </div>
    </div>
</div>
<?php } ?>
