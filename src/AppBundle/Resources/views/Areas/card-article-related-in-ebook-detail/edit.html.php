<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:53
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title :</h4>
<?= $this->input('card-slider-two-title', []) ?>

<h4>Disable Trim Title : </h4>
<?= $this->checkbox('card-slider-two-disable-trim-title') ?>

<h4>Filter Article By RoleFocus : </h4>
<?= $this->relation('relation-role-focus', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['ArticleRoleFocus'],
]) ?>

<h4>Filter Article By ArticleCategory : </h4>
<?= $this->relation('relation-article-category', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['ArticleCategory'],
]) ?>

<h4>Filter Article By ArticleSubCategory : </h4>
<?= $this->relation('relation-article-sub-category', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['ArticleSubCategory'],
]) ?>

<h4>Filter Article By Stage : </h4>
<?= $this->relation('relation-stage', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['Stage'],
]) ?>

<h4>Filter Article By StageGroup : </h4>
<?= $this->relation('relation-stage-group', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['StageGroup'],
]) ?>

<h4>Filter Article By SubStage : </h4>
<?= $this->relation('relation-sub-stage', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['SubStage'],
]) ?>

<h4>Filter Article By StageAge : </h4>
<?= $this->relation('relation-stage-age', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['StageAge'],
]) ?>

<h4>Link :</h4>
<?= $this->link('card-slider-two-link', []) ?>


