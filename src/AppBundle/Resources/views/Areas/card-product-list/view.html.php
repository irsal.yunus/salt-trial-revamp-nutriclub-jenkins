<?php

use AppBundle\Model\DataObject\Product;
use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:32
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="content-detail_list">
    <div class="container-child pb-3">
        <div class="short-desc py-5">
            <?= $this->wysiwyg('product-description')->frontend() ?>
        </div>
        <div class="d-sm-flex justify-content-sm-around">
            <?php while ($this->block('product-list-block')->loop()) { ?>
                <?php
                if ($this->relation('product')->isEmpty()) {
                    continue;
                }

                /** @var Product $product */
                $product = $this->relation('product')->getElement();
                ?>
                <a href="<?= $product->getRouter() ?>">
                    <div class="card-product">
                        <div class="text-center">
                            <?= $product->getImgIcon() ?
                                $product->getImgIcon()
                                ->getThumbnail('product-list-thumbnail')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'img-fluid'
                                ]) : null
                            ?>
                        </div>
                        <div class="name"><?= $product->getName() ?></div>
                        <?= $product->getProductShortDescription() ?>
                        <a href="<?= $product->getRouter() ?>" ><?= $this->t('PRODUCT_LEARN_MORE') ?></a>
                    </div>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
