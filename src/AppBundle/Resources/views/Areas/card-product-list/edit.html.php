<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:32
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<h4>Product Description : </h4>
<?= $this->wysiwyg('product-description', []) ?>

<h4>Product List : </h4>
<?php $no = 1; ?>
<?php while ($this->block('product-list-block')->loop()) { ?>
    <h4>Product <?= $no ?></h4>
    <?= $this->relation('product', [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Product'],
    ]) ?>
    <?php $no++; ?>
<?php } ?>
