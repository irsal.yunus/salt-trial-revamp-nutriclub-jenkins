<?php 

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * 
 * 
 */

?>

<div class="short-desc">
    <?php while($this->block('content-paragraph-area')->loop()) { ?>
        <p><?= $this->input('content-paragraph') ?></p>
        <?php }?>
</div>