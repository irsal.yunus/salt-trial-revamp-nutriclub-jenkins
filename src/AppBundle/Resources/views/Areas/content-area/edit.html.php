<?php 

use Pimcore\Templating\{GlobalVariables, PhpEngine}

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

?>

<h3>Content Area</h3>

<?php $no = 1; ?>
<?php while($this->block('content-paragraph-area')->loop()) { ?>
<h5>Paragaraph <?= $no; ?></h5>
<?= $this->input('content-paragraph', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>
<?php $no++; ?>
<?php } ?>