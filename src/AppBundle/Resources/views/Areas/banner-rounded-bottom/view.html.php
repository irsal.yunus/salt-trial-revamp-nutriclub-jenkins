<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 02/03/2020
 * Time: 14:39
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$this->headLink()->offsetSetStylesheet(3, '/assets/css/pages/product.css');
?>

<div class="banner-product">
    <div class="jumbotron">
        <div class="w-100">
            <h3 class="text-center">Best Protection for the best future</h3>
        </div>
    </div>
</div>
