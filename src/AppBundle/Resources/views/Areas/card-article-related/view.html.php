<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/02/2020
 * Time: 19:54
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Countly\Document\Areabrick\CardArticleRelated\CountlyModel;
use AppBundle\Countly\LinkBuilder;
use AppBundle\Model\DataObject\Article;

$relatedArticleObjects = $this->relatedArticleObjects;
?>

    <div class="card__articles-related">
        <div class="container">
        <p class="card__articles-related-header">
            <?= !$this->input('card-article-related-title')->isEmpty()
                ? $this->input('card-article-related-title')->getData() : null ?>
        </p>
        <div class="row">
            <?php
            if (count($relatedArticleObjects) > 0) {
                /** @var Article $relatedArticleObject */
                foreach ($relatedArticleObjects as $relatedArticleObject) {
                    $customAttr = $this->websiteConfig('ARTICLE_ATTRIBUTE_LINK', null);
            ?>
                    <div class="card__articles-related-item col-12 col-md-6 ">
                        <?= LinkBuilder::start($relatedArticleObject, new CountlyModel(), null, $customAttr) ?>
                            <div class="row">
                                <div class="card__articles-related-figure col-5 col-md-3">
                                    <?= $relatedArticleObject->getImgMobileHtml('default', [
                                        'class' => 'img-fluid',
                                        'disableWidthHeightAttributes' => true
                                    ]) ?>
                                </div>
                                <div class="card__articles-related-description col-7 col-md-8">
                                    <p class="card__articles-related-text"><?= $relatedArticleObject->getTitleTrim() ?></p>
                                    <p class="card__articles-related-sponsor <?= $relatedArticleObject && $relatedArticleObject->getCreator() ? '' : 'invisible' ?>
                                    ">Oleh <?= $relatedArticleObject->getCreator() ?></p>
                                </div>
                            </div>
                        <?= LinkBuilder::stop() ?>
                    </div>
            <?php
                }
            }
            ?>
        </div>
        </div>
        <div class="card__articles-related-button">
            <?= $this->link('card-article-related-link', [
                'class' => 'card__articles-related-button-load-more btn color-blue'
            ]) ?>
        </div>
    </div>
