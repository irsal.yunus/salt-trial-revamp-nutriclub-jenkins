<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<h4>Image :</h4>
<?= $this->image('card-image', [])?>

<h4>Title :</h4>
<?= $this->input('card-title', [])?>

<h4>Content :</h4>
<?= $this->wysiwyg('card-content', [])?>

<h4>Text & Link :</h4>
<?= $this->link('card-link', [])?>
