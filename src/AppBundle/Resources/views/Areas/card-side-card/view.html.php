<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>
<div class="card" id="side-card">
    <div class="row no-gutter">
        <div class="col-12 card-figure">
            <img src="<?= $this->image('card-image')->getThumbnail('default') ?>" alt="three doctors" class="img-fluid">
        </div>
        <div class="col-12">
            <div class="card-body">
                <div class="card-title">
                    <h3><?= $this->input('card-title') ?></h3>
                </div>
                <div class="card-text">
                    <?= $this->wysiwyg('card-content')->getData()?>
                </div>
                <?= $this->link('card-link', [
                    'class' => 'btn'
                ])?>
            </div>
        </div>
    </div>
</div>
