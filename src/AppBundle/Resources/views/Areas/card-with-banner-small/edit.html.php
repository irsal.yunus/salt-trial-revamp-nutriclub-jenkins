<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 14:16
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title :</h4>
<?= $this->input('card-with-banner-small-title', [
]) ?>

<h4>Banner Desktop : </h4>
<?= $this->image('card-with-banner-small-desktop', [
    'title' => 'Card with banner small desktop',
]) ?>

<h4>Banner Mobile : </h4>
<?= $this->image('card-with-banner-small-mobile', [
    'title' => 'Card with banner small mobile'
]) ?>

<h4>Link 1 :</h4>
<?= $this->link('card-with-banner-small-link-one', []) ?>

<h4>Image Link 1 :</h4>
<?= $this->image('card-with-banner-small-image-link-one', [
    'title' => 'Image for link 1'
]) ?>

<h4>Link 2 :</h4>
<?= $this->link('card-with-banner-small-link-two', []) ?>

<h4>Image Link 1 :</h4>
<?= $this->image('card-with-banner-small-image-link-two', [
    'title' => 'Image for link 2'
]) ?>
