<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 14:16
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<?php if (
    !$this->image('card-with-banner-small-desktop')->isEmpty() &&
    !$this->image('card-with-banner-small-mobile')->isEmpty()
) { ?>
<div class="card-banner-small mt-5">
    <div class="container">
        <div class="card-banner-small__wrapper color-blue">
            <?= $this->image('card-with-banner-small-desktop', [
            ])->getThumbnail('card-with-banner-small-desktop')->getHtml([
                'class' => 'image-wrapper d-none d-md-block'
            ]) ?>
            <?= $this->image('card-with-banner-small-mobile', [
            ])->getThumbnail('card-with-banner-small-mobile')->getHtml([
                'class' => 'image-wrapper d-block d-sm-none'
            ]) ?>
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-7 offset-5 col-md-5">
                        <h5><?= $this->input('card-with-banner-small-title')->frontend() ?></h5>
                        <div class="app-image-wrapper">
                            <?php
                            if (!$this->link('card-with-banner-small-link-one')->isEmpty()) {

                                $textPrefixLinkOne =
                                    !$this->image('card-with-banner-small-image-link-one')->isEmpty() ?
                                        $this->image('card-with-banner-small-image-link-one')
                                            ->getThumbnail('card-with-banner-small-image')
                                            ->getHtml([])
                                        : '';

                                $linkOne = $this->link('card-with-banner-small-link-one', [
                                    'textPrefix' => $textPrefixLinkOne
                                ]);

                                echo $linkOne;
                            }
                            ?>

                            <?php
                            if (!$this->link('card-with-banner-small-link-two')->isEmpty()) {

                                $textPrefixLinkTwo =
                                    !$this->image('card-with-banner-small-image-link-two')->isEmpty() ?
                                        $this->image('card-with-banner-small-image-link-two')
                                            ->getThumbnail('card-with-banner-small-image')
                                            ->getHtml([])
                                        : '';

                                $linkTwo = $this->link('card-with-banner-small-link-two', [
                                    'textPrefix' => $textPrefixLinkTwo
                                ]);

                                echo $linkTwo;
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
