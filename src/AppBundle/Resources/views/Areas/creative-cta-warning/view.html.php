<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="warning-desc">
    <p><?= $this->input('warning-text') ?></p>
    <button type="button" class=""><?= $this->link('warning-link') ?></button>
</div>