<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

?>

<h3>Creative CTA Warning</h3>

<h5>Description</h5>
<?= $this->input('warning-text', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here',
])?>

<h5>Anchor Path & Text</h5>
<?= $this->link('warning-link', [
    // 'textSuffix' => 'class="text-white"'
]) ?>