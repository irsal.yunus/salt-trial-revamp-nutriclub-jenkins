<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

?>
<div class="content-detail_sort_desc">
    <div class="container-child">
        <div class="d-flex">
            <div class="desc">
                <h2>
                    <?= $this->input('title')->frontend() ?>
                </h2>
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <?php while ($this->block('content-list-block')->loop()) { ?>
                            <div class="d-flex">
                                <span class="pr-3">
                                <?= !$this->image('content-list-icon')->isEmpty() ?
                                    $this->image('content-list-icon')
                                        ->getThumbnail('content-list-icon-thumbnail')
                                        ->getHtml([
                                            'disableWidthHeightAttributes' => true,
                                            'class' => 'img-fluid'
                                        ]) : null
                                ?>
                                </span>
                                <?= $this->wysiwyg('content-list-description')->frontend() ?>
                            </div>

                        <?php } ?>
                    </div>
                    <div class="col-sm-6 text-center">
                        <?= !$this->image('card-product-group-detail-image')->isEmpty() ?
                            $this->image('card-product-group-detail-image')
                            ->getThumbnail('card-product-group-detail-thumbnail')
                            ->getHtml([
                                'disableWidthHeightAttributes' => true,
                                'class' => 'img-fluid img-product'
                            ]) : null
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
