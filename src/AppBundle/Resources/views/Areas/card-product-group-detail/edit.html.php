<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>
<h3>Card Product Group Detail</h3>

<h4>Title :</h4>
<?= $this->input('title', [
    'placeholder' => 'Input your title here'
]) ?>

<h4>Block List : </h4>
<?php $no = 1; ?>
<?php while ($this->block('content-list-block')->loop()) { ?>
    <h4>Icon <?= $no ?></h4>
    <?= $this->image('content-list-icon', [
        'thumbnail'     => 'content-list-icon-thumbnail',
    ]) ?>

    <h4>Description <?= $no ?></h4>
    <?= $this->wysiwyg('content-list-description', []) ?>

    <?php $no++; ?>
<?php } ?>

<h4>Product Image</h4>
<?= $this->image('card-product-group-detail-image', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'card-product-group-detail-thumbnail',
    'uploadPath'    => '/card-product-group-detail/',
    'reload'        => false,
    'hideText'      => true
]) ?>

