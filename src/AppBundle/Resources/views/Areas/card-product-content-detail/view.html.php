<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/07/2020
 * Time: 19:49
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>
<div class="content-product_short_description py-lg-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 mx-auto">
                <div class="text-center py-5">
                    <?= $this->wysiwyg('description')->frontend() ?>
                </div>
            </div>
        </div>
    </div>
</div>
