<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/07/2020
 * Time: 01:04
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<div class="product__qna pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 mx-lg-auto">
                <div class="qna__image-wraper position-relative">
                    <?= !$this->image('product-question-and-ask-image')->isEmpty() ?
                        $this->image('product-question-and-ask-image')
                            ->getThumbnail('')
                            ->getHtml([
                                'class' => 'w-100'
                            ]) : null
                    ?>
                    <div class="qna__description position-absolute d-flex flex-column justify-content-center">
                        <h3 class="mb-0">
                            <?= $this->input('product-question-and-ask-title')->frontend() ?>
                        </h3>
                        <p class="mb-0 mb-lg-3">with Experts</p>
                        <?= $this->link('product-question-and-ask-link')->frontend() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
