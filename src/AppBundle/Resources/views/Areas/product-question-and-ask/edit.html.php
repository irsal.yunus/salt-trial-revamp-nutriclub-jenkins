<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/07/2020
 * Time: 01:04
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Title :</h4>
<?= $this->input('product-question-and-ask-title') ?>

<h4>Link :</h4>
<?= $this->link('product-question-and-ask-link') ?>

<h4>Image :</h4>
<?= $this->image('product-question-and-ask-image') ?>
