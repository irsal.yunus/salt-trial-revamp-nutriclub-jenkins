<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 07/02/2020
 * Time: 12:58
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Background Desktop :</h4>
<?= $this->image('card-with-banner-one-desktop', [

]) ?>
<h4>Background Mobile :</h4>
<?= $this->image('card-with-banner-one-mobile', [
]) ?>

<h4>Title : </h4>
<?= $this->input('card-with-banner-one-title') ?>

<h4>Sub Title</h4>
<?= $this->input('card-with-banner-one-sub-title') ?>

<h4>Link :</h4>
<?= $this->link('card-with-banner-one-link', []) ?>

