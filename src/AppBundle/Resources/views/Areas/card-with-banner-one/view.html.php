<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 07/02/2020
 * Time: 12:58
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<?php
if (!$this->image('card-with-banner-one-desktop')->isEmpty() &&
    !$this->image('card-with-banner-one-mobile')->isEmpty() &&
    !$this->input('card-with-banner-one-title')->isEmpty() &&
    !$this->input('card-with-banner-one-sub-title')->isEmpty() &&
    !$this->link('card-with-banner-one-link')->isEmpty()) {
    ?>
    <div class="card-with-banner-one mt-5">
        <div class="container">
            <div class="card-with-banner-one__wrapper">
                <?= $this->image('card-with-banner-one-desktop', [
                    'class' => 'image-wrapper d-none d-md-block'
                ])->frontend() ?>
                <?= $this->image('card-with-banner-one-mobile', [
                    'class' => 'image-wrapper d-block d-sm-none'
                ])->frontend() ?>
                <div class="card-with-banner-one__content color-blue">
                    <h4><?= $this->input('card-with-banner-one-title')->getData() ?></h4>
                    <h6><?= $this->input('card-with-banner-one-title')->getData() ?></h6>
                    <div class="btn-more">
                        <?= $this->link('card-with-banner-one-link')->frontend() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
