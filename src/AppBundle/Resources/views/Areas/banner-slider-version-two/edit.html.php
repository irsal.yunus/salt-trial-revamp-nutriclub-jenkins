<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:17
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Banner Slider</h4>
<?php $no = 1; ?>
<?php while($this->block('banner-slider-version-two-version-two-block')->loop()) { ?>
    <div class="accordion" id="accordion-<?= $no ?>">
        <h4>Title Desktop <?= $no ?></h4>
        <?= $this->input('banner-slider-version-two-title-default', [

        ]) ?>
        <h4>Title Mobile <?= $no ?></h4>
        <?= $this->input('banner-slider-version-two-title-default-mobile', [

        ]) ?>
        <h4>Description Desktop <?= $no ?></h4>
        <?= $this->input('banner-slider-version-two-description-default-desktop', [

        ]) ?>
        <h4>Description Mobile <?= $no ?></h4>
        <?= $this->input('banner-slider-version-two-description-default-mobile', [

        ]) ?>
        <h4>Link Banner <?= $no ?></h4>
        <?= $this->link('banner-slider-version-two-link-image', [

        ])?>
        <h4>Link Text <?= $no ?></h4>
        <?= $this->input('banner-slider-version-two-link-text', []) ?>
        <div class="card mt-2">
            <div class="card-header" id="heading-<?= $no ?>-one">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse-<?= $no ?>-one" aria-expanded="true" aria-controls="collapse-<?= $no ?>-one">
                        Banner Image <?= $no ?>
                    </button>
                </h2>
            </div>

            <div id="collapse-<?= $no ?>-one" class="collapse show" aria-labelledby="heading-<?= $no ?>-one" data-parent="#accordion-<?= $no ?>">
                <div class="card-body">
                    <h4>Dekstop Banner <?= $no ?></h4>
                    <?= $this->image('banner-slider-version-two-desktop', [
                        'title' => 'Drag your image here',
                        'thumbnail'   => 'banner-slider-version-two-desktop-thumbnail',
                        'uploadPath'  => '/banner-slider/desktop/',
                        'reload'      => false,
                        'hidetext'    => true
                    ])
                    ?>
                    <h4>Tablet Banner <?= $no ?></h4>
                    <?= $this->image('banner-slider-version-two-tablet', [
                        'title' => 'Drag your image here',
                        'thumbnail'   => 'banner-slider-version-two-tablet-thumbnail',
                        'uploadPath'  => '/banner-slider/tablet/',
                        'reload'      => false,
                        'hidetext'    => true
                    ])
                    ?>
                    <h4>Mobile Banner <?= $no ?></h4>
                    <?= $this->image('banner-slider-version-two-mobile', [
                        'title' => 'Drag your image here',
                        'thumbnail'   => 'banner-slider-version-two-mobile-thumbnail',
                        'uploadPath'  => '/banner-slider/mobile/',
                        'reload'      => false,
                        'hidetext'    => true
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="heading-<?= $no ?>-two">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse-<?= $no ?>-two" aria-expanded="false" aria-controls="collapse-<?= $no ?>-two">
                        Banner Video <?= $no ?>
                    </button>
                </h2>
            </div>
            <div id="collapse-<?= $no ?>-two" class="collapse" aria-labelledby="heading-<?= $no ?>-two" data-parent="#accordion-<?= $no ?>">
                <div class="card-body">
                    <h4>Video <?= $no ?></h4>
                    <?= $this->video('banner-slider-version-two-video', []) ?>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="heading-<?= $no ?>-three">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse-<?= $no ?>-three" aria-expanded="true" aria-controls="collapse-<?= $no ?>-three">
                        Banner GIF <?= $no ?>
                    </button>
                </h2>
            </div>

            <div id="collapse-<?= $no ?>-three" class="collapse show" aria-labelledby="heading-<?= $no ?>-three" data-parent="#accordion-<?= $no ?>">
                <div class="card-body">
                    <h4>Dekstop Banner GIF <?= $no ?></h4>
                    <?= $this->image('banner-slider-version-two-desktop-gif', [
                        'title' => 'Drag your image here',
                        'uploadPath'  => '/banner-slider/desktop/',
                        'reload'      => false,
                        'hidetext'    => true
                    ])
                    ?>
                    <h4>Dekstop Banner mp4 <?= $no ?></h4>
                    <?= $this->video('banner-slider-version-two-desktop-mp4', [
                        'title' => 'Drag your image here',
                        'uploadPath'  => '/banner-slider/desktop/',
                        'reload'      => false,
                        'hidetext'    => true
                    ])
                    ?>
                    <h4>Dekstop Banner webm <?= $no ?></h4>
                    <?= $this->video('banner-slider-version-two-desktop-webm', [
                        'title' => 'Drag your image here',
                        'uploadPath'  => '/banner-slider/desktop/',
                        'reload'      => false,
                        'hidetext'    => true
                    ])
                    ?>
                    <h4>Tablet Banner GIF <?= $no ?></h4>
                    <?= $this->image('banner-slider-version-two-tablet-gif', [
                        'title' => 'Drag your image here',
                        'uploadPath'  => '/banner-slider/tablet/',
                        'reload'      => false,
                        'hidetext'    => true
                    ])
                    ?>
                    <h4>Mobile Banner GIF <?= $no ?></h4>
                    <?= $this->image('banner-slider-version-two-mobile-gif', [
                        'title' => 'Drag your image here',
                        'uploadPath'  => '/banner-slider/mobile/',
                        'reload'      => false,
                        'hidetext'    => true
                    ])
                    ?>
                    <h4>Mobile Banner mp4 <?= $no ?></h4>
                    <?= $this->video('banner-slider-version-two-mobile-mp4', [
                        'title' => 'Drag your image here',
                        'uploadPath'  => '/banner-slider/desktop/',
                        'reload'      => false,
                        'hidetext'    => true
                    ])
                    ?>
                    <h4>Mobile Banner webm <?= $no ?></h4>
                    <?= $this->video('banner-slider-version-two-mobile-webm', [
                        'title' => 'Drag your image here',
                        'uploadPath'  => '/banner-slider/desktop/',
                        'reload'      => false,
                        'hidetext'    => true
                    ])
                    ?>

                </div>
            </div>
        </div>
    </div>
    <?php $no++; ?>
<?php } ?>

