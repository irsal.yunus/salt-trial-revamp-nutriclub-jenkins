<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:17
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<div class="banner-slider">
    <?php while ($this->block('banner-slider-version-two-version-two-block')->loop()) { ?>

        <?php
        $title = !$this->input('banner-slider-version-two-title-default')->isEmpty() ?
            $this->input('banner-slider-version-two-title-default')->getData() : null;

        $titleMobile = !$this->input('banner-slider-version-two-title-default-mobile')->isEmpty() ?
            $this->input('banner-slider-version-two-title-default-mobile')->getData() : null;

        $descDesktop = !$this->input('banner-slider-version-two-description-default-desktop')->isEmpty() ?
            $this->input('banner-slider-version-two-description-default-desktop')->getData() : null;

        $descMobile = !$this->input('banner-slider-version-two-description-default-mobile')->isEmpty() ?
            $this->input('banner-slider-version-two-description-default-mobile')->getData() : null;
        ?>

        <?php
        if (
            !$this->image('banner-slider-version-two-desktop')->isEmpty() &&
            !$this->image('banner-slider-version-two-mobile')->isEmpty() &&
            !$this->link('banner-slider-version-two-link-image')->isEmpty() &&
            !$this->input('banner-slider-version-two-link-text')->isEmpty()) {

            $titleHtmlTag = '<div class="banner-slider__header"><h2 class="banner-slider__header-desktop">' . $title . '</h2>' . '<h2 class="banner-slider__header-mobile">' . $titleMobile . '</h2>';

            $subTitleTag = '<p class="banner-slider__subtext">' . $descDesktop . '</p>
                        <p class="banner-slider__subtext-mobile">' . $descMobile . '</p>
                        </div>';
            $linkText = '<div class="banner-slider__button">' . '<span class="btn-now-more btn btn-blue">' . $this->input('banner-slider-version-two-link-text')->getData() . '</span>' . '</div>';

            $contentTag = '<div class="container"><div class="row"><div class="banner-slider__description-wrapper col-12 col-md-5 offset-md-2">' . $titleHtmlTag . $subTitleTag . $linkText . '</div></div></div><span class="banner-slider__gradient"></span>';

            $imgDesktop = $this->image('banner-slider-version-two-desktop', [
            ])->getThumbnail('banner-slider-version-two-desktop-thumbnail')->getHtml([
                'class' => 'banner-slider__image is-background d-none d-sm-block'
            ]);

            $imgMobile = $this->image('banner-slider-version-two-mobile', [
            ])->getThumbnail('banner-slider-version-two-mobile-thumbnail')->getHtml([
                'class' => 'banner-slider__image is-background d-block d-sm-none'
            ]);

            $link = $this->link('banner-slider-version-two-link-image', [
                'textPrefix' => $imgDesktop . $imgMobile . $contentTag
            ]);

            ?>
            <div class="banner-slider__item">
                <?= $link ?>
            </div>
            <?php
            continue;
        }
        ?>

        <?php
        if (
        !$this->video('banner-slider-version-two-video')->isEmpty()
        ) {

            $titleVideo = '';

            $description = '';

            $video = $this->video('banner-slider-version-two-video');

            $youtubeVideoId = $video->id;

            $youtubeThumbnail = $youtubeVideoId ? 'https://img.youtube.com/vi/' . $youtubeVideoId . '/0.jpg'
                : '/assets/images/video-thumbnail.png';

            ?>
            <div class="banner-slider__item is-video">
                <a href="#" data-video="<?= $youtubeVideoId ?>" class="banner-slider__trigger">
                    <div class="container">
                        <div class="row">
                            <div class="banner-slider__description-wrapper col-12 col-md-5 offset-md-2">
                                <div class="banner-slider__header">
                                    <h2 class="banner-slider__header-desktop">
                                        <?= $titleVideo = !$this->input('banner-slider-version-two-title-default')->isEmpty() ?
                                            $this->input('banner-slider-version-two-title-default')->getData() : '' ?>
                                    </h2>
                                    <h2 class="banner-slider__header-mobile">
                                        <?= $titleVideo = !$this->input('banner-slider-version-two-title-default-mobile')->isEmpty() ?
                                            $this->input('banner-slider-version-two-title-default-mobile')->getData() : '' ?>
                                    </h2>
                                    <?= $description = !$this->input('banner-slider-version-two-description-default-desktop')->isEmpty() ?
                                        $this->input('banner-slider-version-two-description-default-desktop', [
                                            'class' => 'banner-slider__subtext'
                                        ])->getData() : '' ?>
                                    <?= $description = !$this->input('banner-slider-version-two-description-default-mobile')->isEmpty() ?
                                        $this->input('banner-slider-version-two-description-default-mobile', [
                                            'class' => 'banner-slider__subtext-mobile'
                                        ])->getData() : '' ?>
                                </div>
                                <div class="banner-slider__button">
                                    <span class="btn-now-more btn btn-blue">Lihat Selengkapnya</span>
                                </div>
                            </div>
                            <div class="col-4">
                            <span class="banner-slider__video">
                                <img src="<?= $youtubeThumbnail ?>" alt="" class="banner-slider__image"/>
                                <img src="/assets/images/icon-play.svg" alt="" class="icon-play"/>
                            </span>
                            </div>
                        </div>
                    </div>
                    <span class="banner-slider__gradient"></span>
                </a>
            </div>
            <?php
        }
        ?>

        <?php
        if (
            !$this->image('banner-slider-version-two-desktop-gif')->isEmpty() &&
            !$this->image('banner-slider-version-two-mobile-gif')->isEmpty() &&
            !$this->link('banner-slider-version-two-link-image')->isEmpty() &&
            !$this->input('banner-slider-version-two-link-text')->isEmpty()) {

            $titleHtmlTag = '<div class="banner-slider__header"><h2 class="banner-slider__header-desktop">' . $title . '</h2>' . '<h2 class="banner-slider__header-mobile">' . $titleMobile . '</h2>';

            $subTitleTag = '<p class="banner-slider__subtext">' . $descDesktop . '</p>
                        <p class="banner-slider__subtext-mobile">' . $descMobile . '</p>
                        </div>';
            $linkText = '<div class="banner-slider__button">' . '<span class="btn-now-more btn btn-blue">' . $this->input('banner-slider-version-two-link-text')->getData() . '</span>' . '</div>';

            $contentTag = '<div class="container"><div class="row"><div class="banner-slider__description-wrapper col-12 col-md-5 offset-md-2">' . $titleHtmlTag . $subTitleTag . $linkText . '</div></div></div><span class="banner-slider__gradient"></span>';

            $videoDesktopWebm = !$this->video('banner-slider-version-two-desktop-webm')->isEmpty() ?
                $this->video('banner-slider-version-two-desktop-webm')->getData() : null;

            $videoDesktopMp4 = !$this->video('banner-slider-version-two-desktop-mp4')->isEmpty() ?
                $this->video('banner-slider-version-two-desktop-mp4')->getData() : null;

            $videoMobileWebm = !$this->video('banner-slider-version-two-mobile-webm')->isEmpty() ?
                $this->video('banner-slider-version-two-mobile-webm')->getData() : null;

            $videoMobileMp4 = !$this->video('banner-slider-version-two-mobile-mp4')->isEmpty() ?
                $this->video('banner-slider-version-two-mobile-mp4')->getData() : null;

            $imgDesktop = '<video autoplay loop muted playsinline class="banner-slider__image is-background d-none d-sm-block">
                      <source src="' . $videoDesktopWebm['path'] . '" type="video/webm">
                      <source src="' . $videoDesktopMp4['path'] . '" type="video/mp4">
                      </video>';

            $imgMobile = '<video autoplay loop muted playsinline class="banner-slider__image is-background d-block d-sm-none">
                      <source src="' . $videoMobileWebm['path'] . '" type="video/webm">
                      <source src="' . $videoMobileMp4['path'] . '" type="video/mp4">
                      </video>';

            $link = $this->link('banner-slider-version-two-link-image', [
                'textPrefix' => $imgDesktop . $imgMobile . $contentTag
            ]);

            ?>
            <div class="banner-slider__item">
                <?= $link ?>
            </div>
            <?php
            continue;
        }
        ?>

    <?php } ?>
</div>

<!-- POPUP VIDEO -->
<!-- Modal -->
<div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <iframe id="modal-video__iframe" width="100%" height="500" src="" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
