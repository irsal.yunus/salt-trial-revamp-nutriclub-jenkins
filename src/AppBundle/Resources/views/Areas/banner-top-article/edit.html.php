<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 30/06/2020
 * Time: 12:36
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<h4>Image desktop :</h4>
<?= $this->image('banner-top-article-desktop', []) ?>

<h4>Image mobile :</h4>
<?= $this->image('banner-top-article-mobile', []) ?>

<h4>Link : </h4>
<?= $this->link('banner-top-article-link', []) ?>
