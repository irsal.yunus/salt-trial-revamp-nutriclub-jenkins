<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 30/06/2020
 * Time: 12:36
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="banner-top">
    <div class="banner-top__wrapper container">
        <?php $imageDesktop = !$this->image('banner-top-article-desktop')->isEmpty() ?
            $this->image('banner-top-article-desktop')->getThumbnail('banner-top-article-desktop')->getHtml([
                'class' => 'img-fluid',
                'disableWidthHeightAttributes' => true
            ]) : null;
        ?>

        <?php $imageMobile =  !$this->image('banner-top-article-mobile')->isEmpty() ?
            $this->image('banner-top-article-mobile')->getThumbnail('banner-top-article-mobile')->getHtml([
                'class' => 'img-fluid',
                'disableWidthHeightAttributes' => true
            ]) : null;
        ?>

        <?= !$this->link('banner-top-article-link')->isEmpty() ?$this->link('banner-top-article-link', [
            'class' => 'banner-top__figure-desktop',
            'textSuffix' => $imageDesktop
        ])->frontend() : null ?>


        <?= $this->link('banner-top-article-link', [
            'class' => 'banner-top__figure-mobile',
            'textSuffix' => $imageMobile
        ])->frontend() ?>

    </div>
</div>
