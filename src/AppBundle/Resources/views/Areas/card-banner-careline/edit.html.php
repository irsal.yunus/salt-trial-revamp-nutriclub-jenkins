<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<h4>Title :</h4>
<?= $this->input('card-banner-careline-title', []) ?>

<h4>Short Desc :</h4>
<?= $this->wysiwyg('card-banner-careline-short-desc', []) ?>

<h4>Image WA :</h4>
<?= $this->image('card-banner-careline-image-wa', []) ?>

<h4>Link WA :</h4>
<?= $this->link('card-banner-careline-link-wa', []) ?>

<h4>Image Phone :</h4>
<?= $this->image('card-banner-careline-image-phone', []) ?>

<h4>Link Phone :</h4>
<?= $this->link('card-banner-careline-link-phone', [])?>

<h4>Image Cirlce</h4>
<?= $this->image('card-banner-careline-image-circle', []) ?>

<h4>Link Button</h4>
<?= $this->link('card-banner-careline-link-button', []) ?>





