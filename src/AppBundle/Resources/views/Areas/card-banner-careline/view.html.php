<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

?>

<div class="card-banner-careline">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card-banner-careline__content">
                    <div class="card-banner-careline__content-title">
                        <h2>
                        <?= $this->input('card-banner-careline-title')->frontend() ?>
                        </h2>
                    </div>
                    <div class="card-banner-careline__content-desc">
                        <?= $this->wysiwyg('card-banner-careline-short-desc')->frontend() ?>
                    </div>
                    <div class="card-banner-careline__content-btn">
                        <?= $this->link('card-banner-careline-link-button', [
                            'class' => 'btn btn-primary'
                        ])->frontend() ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-banner-careline__img">
                    <div class="img-lg-banner">
                        <?= !$this->image('card-banner-careline-image-circle')->isEmpty() ?
                            $this->image('card-banner-careline-image-circle')
                                ->getThumbnail('card-banner-careline-image-circle-thumbnail')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true
                                ])
                            : null
                        ?>
                    </div>
                    <?= str_replace('</a>', null, $this->link('card-banner-careline-link-wa', [
                        'class' => 'img-wa'
                    ])->frontend()) ?>
                        <?= !$this->image('card-banner-careline-image-wa')->isEmpty() ?
                            $this->image('card-banner-careline-image-wa')
                                ->getThumbnail('card-banner-careline-image-wa-thumbnail')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true
                                ])
                            : null
                        ?>
                    </a>
                    <?= str_replace('</a>', null, $this->link('card-banner-careline-link-phone', [
                        'class' => 'img-phone'
                    ])->frontend()) ?>
                        <?= !$this->image('card-banner-careline-image-phone')->isEmpty() ?
                            $this->image('card-banner-careline-image-phone')
                                ->getThumbnail('card-banner-careline-image-phone-thumbnail')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true
                                ])
                            : null
                        ?>
                    </a>
                </div>
                <div class="card-banner-careline__img-btn">
                    <?= $this->link('card-banner-careline-link-button', [
                        'class' => 'btn btn-primary'
                    ])->frontend() ?>
                </div>
            </div>
        </div>
    </div>
</div>
