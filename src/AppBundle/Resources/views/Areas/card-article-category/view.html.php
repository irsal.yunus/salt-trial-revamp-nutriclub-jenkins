<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 12:39
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Helper\GeneralHelper;
use AppBundle\Model\DataObject\ArticleCategory;

$articleCategories = new ArticleCategory\Listing();
$reArrange = [];
$reArrangeAsExcept = [];
$reArrangeWithoutExcept = true;
if (!$this->block($this->prefixName . '-render-first-block')->isEmpty()) {
    $noRf = 0;
    while ($this->block($this->prefixName . '-render-first-block')->loop()) {
        if (!$this->relation($this->prefixName . '-render-first-' . $noRf)->isEmpty()) {
            /** @var ArticleCategory $articleCategory */
            $articleCategory = $this->relation($this->prefixName . '-render-first-' . $noRf)->getElement();
            $reArrange[] = $articleCategory;
            $reArrangeAsExcept[] = $articleCategory->getId();
        }
        $noRf++;
    }
}
if (!$this->block($this->prefixName . '-block')->isEmpty()) {
    $listId = [];
    $no = 0;
    while($this->block($this->prefixName . '-block')->loop()) {
        if (!$this->relation($this->prefixName . '-to-except-' . $no)->isEmpty()) {
            /** @var ArticleCategory $articleCategory */
            $articleCategory = $this->relation($this->prefixName . '-to-except-' . $no)->getElement();
            $listId[] =  $articleCategory->getId();
        }
        $no++;
    }
    $listId = array_unique(array_merge($listId, $reArrangeAsExcept));
    if (count($listId) > 0) {
        $articleCategories->addConditionParam('oo_id NOT IN('. implode(',', $listId) .')', null, 'AND');
        $reArrangeWithoutExcept = false;
    }
}
if ($reArrangeWithoutExcept && $reArrangeAsExcept) {
    $articleCategories->addConditionParam('oo_id NOT IN('. implode(',', $reArrangeAsExcept) .')', null, 'AND');
}
if (!$this->block($this->prefixName . '-block-only')->isEmpty()) {
    $listIdOnly = [];
    $no = 0;
    while($this->block($this->prefixName . '-block-only')->loop()) {
        if (!$this->relation($this->prefixName . '-to-only-' . $no)->isEmpty()) {
            /** @var ArticleCategory $articleCategory */
            $articleCategory = $this->relation($this->prefixName . '-to-only-' . $no)->getElement();
            $listIdOnly[] =  $articleCategory->getId();
        }
        $no++;
    }
    if (count($listIdOnly) > 0) {
        $articleCategories->addConditionParam('oo_id IN('. implode(',', $listIdOnly) .')', null, 'AND');
    }
}
$articleCategories->setLimit(8);
$articleCategories->load();

$articleCategories = array_merge($reArrange, $articleCategories->getObjects());
?>

<div class="artilce-category pr-md-3">
    <div class="article-category__wrapper">
        <h3 class="article-category__header">
            <?= $this->input($this->prefixName . '-title')->getData() ?? '' ?>
        </h3>
        <div class="row">
            <?php
            /** @var ArticleCategory $articleCategory */
            foreach ($articleCategories as $articleCategory) {

                $articleCategoryLink = $this->path('ARTICLE_CATEGORY', [
                    'slug' => $articleCategory->getSlug()
                ]);

                $articleCount = GeneralHelper::getObjectCountByRelation(
                    'Article',
                    'category__id',
                    $articleCategory->getId()
                );

                $podcastCount = GeneralHelper::getObjectCountByRelation(
                    'ArticlePodcast',
                    'category__id',
                    $articleCategory->getId()
                );

                $videoCount = GeneralHelper::getObjectCountByRelation(
                    'ArticleVideo',
                    'category__id',
                    $articleCategory->getId()
                );
            ?>
                <div class="article-category__item col-12 col-md-4 pr-md-0">
                    <div class="article-category__item-card">
                        <?= $articleCategory->getIconThumbnail($this->prefixName . '-icon', [
                            'class' => 'img-fluid',
                            'disableWidthHeightAttributes' => true
                        ]) ?>
                        <div class="article-category__item-description">
                            <h5><?= $articleCategory->getName() ?></h5>
                            <?= $articleCategory->getDescription() ?? '' ?>
                            <div class="article-category__total">
                                <a href="<?= $articleCategoryLink ?>">
                                    <p class="article-category__total-list"> <b><?= $articleCount ?></b> Artikel</p>
                                </a>
                                <a href="/podcast">
                                    <p class="article-category__total-list"> <b><?= $podcastCount ?></b> Podcast</p>
                                </a>
                                <a href="/video">
                                    <p class="article-category__total-list"> <b><?= $videoCount ?></b> Video</p>
                                </a>
                            </div>
                        </div>
                        <span class="article-category__item-background gradient"></span>
                    </div>
                </div>
            <?php
            } ?>
        </div>
    </div>
</div>
