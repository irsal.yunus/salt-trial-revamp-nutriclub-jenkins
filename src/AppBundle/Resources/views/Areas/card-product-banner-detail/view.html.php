<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/07/2020
 * Time: 19:40
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="content-product_header">
    <div class="content-product_header_bg">
        <div class="bg-header-detail position-relative">
            <?= !$this->image('image-desktop')->isEmpty() ?
                $this->image('image-desktop')
                    ->getThumbnail('card-product-banner-detail-desktop-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'img-fluid d-none d-lg-block'
                    ]) : null
            ?>
            <?= !$this->image('image-mobile')->isEmpty() ?
                $this->image('image-mobile')
                    ->getThumbnail('card-product-banner-detail-mobile-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'w-100 d-block d-lg-none'
                    ]) : null
            ?>
            <?php if($this->product->getIsActiveFreeSample()) { ?>
                <?php
                $freeSampleLink = $this->url('PRODUCT_FREE_SAMPLE', [
                    'groupSlug'     => $this->product->getGroup()->getSlug(),
                    'productSlug'   => $this->product->getSlug()
                ]);
                $link = $this->link('free-sample-link', [
                    'href' => $freeSampleLink
                ]);
                ?>
            <div class="bg-header-detail__button">
                <a href="<?= $freeSampleLink ?>" class="btn btn-klaim">Coba Gratis</a>
            </div>
                        <?php } ?>
        </div>
    </div>
</div>
