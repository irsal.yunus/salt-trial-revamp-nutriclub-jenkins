<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 21/07/2020
 * Time: 19:40
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>
<h4>Image Desktop : </h4>
<?= $this->image('image-desktop', []) ?>

<h4>Image Mobile : </h4>
<?= $this->image('image-mobile', []) ?>

<h4>Free Sample Link: </h4>
<?= $this->link('free-sample-link') ?>
