<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 05/02/2020
 * Time: 12:59
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Countly\Document\Areabrick\BannerSliderDropDown\CountlyModel;
use AppBundle\Countly\LinkBuilder;
use AppBundle\Model\DataObject\Stage;
use Pimcore\Model\WebsiteSetting;

?>
<?php

if (
    !$this->input('banner-slider-drop-down-title')->isEmpty() &&
    !$this->image('banner-slider-drop-down-image')->isEmpty()) {

    $bgCss = $this->image('banner-slider-drop-down-image')->getThumbnail('default')->getPath();
    $title = $this->input('banner-slider-drop-down-title')->getData();

    $stages = new Stage\Listing();
    if (!$this->block('card-slider-one-block')->isEmpty()) {
        $listId = [];
        $no = 0;
        while($this->block('card-slider-one-block')->loop()) {
            if (!$this->relation('card-slider-one-stage-to-except-' . $no)->isEmpty()) {
                /** @var Stage $stage */
                $stage = $this->relation('card-slider-one-stage-to-except-' . $no)->getElement();
                $listId[] =  $stage->getId();
            }
            $no++;
        }
        if (count($listId) > 0) {
            $stages->addConditionParam('oo_id NOT IN('. implode(',', $listId) .')', null, 'AND');
        }
    }
    if (!$this->block('card-slider-one-block-only')->isEmpty()) {
        $listIdOnly = [];
        $no = 0;
        while($this->block('card-slider-one-block-only')->loop()) {
            if (!$this->relation('card-slider-one-stage-to-only-' . $no)->isEmpty()) {
                /** @var Stage $stage */
                $stage = $this->relation('card-slider-one-stage-to-only-' . $no)->getElement();
                $listIdOnly[] =  $stage->getId();
            }
            $no++;
        }
        if (count($listIdOnly) > 0) {
            $stages->addConditionParam('oo_id IN('. implode(',', $listIdOnly) .')', null, 'AND');
        }
    }
    $stages->setOrderKey('orderIndex');
    $stages->setOrder('asc');
    $stages->load();

    /** @var Stage $default */
    $default = !$this->relation('banner-slider-drop-down-default')->isEmpty() ?
        $this->relation('banner-slider-drop-down-default')->getElement() : null;
    ?>
    <div class="banner-bg" style="background-image: url(<?= $bgCss ?>);">
        <img src="<?= $bgCss ?>" class="d-sm-none banner-bg-mobile"/>
        <div class="overlay"></div>
        <div class="container banner-bg-container">
            <div class="dropdown-stage">
                <div class="dropdown-stage__wrapper text-center">
                    <h6><?= $title ?? '' ?></h6>
                    <div class="dropdown">

                        <a class="dropdown-stage__trigger" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"><?= $default ? $default->getName() : $this->t('ALL_STAGE') ?> <i class="fa fa-angle-down"></i></a>

                        <div class="relative-dropdown">
                            <div class="dropdown-menu dropdown-stage__menu" aria-labelledby="dropdownMenuButton">
                                <?php
                                if ($stages->getCount() > 0) {
                                    /** @var Stage $stage */
                                    foreach ($stages as $stage) {
                                        $classAttr = 'dropdown-item ' . ($stage === $default ? 'd-none' : '');
                                        $customAttr = $this->websiteConfig('STAGE_ATTRIBUTE_LINK', null);
                                ?>
                                    <?= LinkBuilder::start($stage, new CountlyModel(), $classAttr, $customAttr)?>
                                        <img  alt="<?= $stage->getName() ?? '' ?>" src="<?= $stage->hasProperty('IMAGE_ICON') ? $stage->getProperty('IMAGE_ICON') : '' ?>"><?= $stage->getName() ?>
                                    <?= LinkBuilder::stop() ?>
                                <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
