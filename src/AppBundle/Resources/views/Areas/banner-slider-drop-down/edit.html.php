<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 05/02/2020
 * Time: 12:59
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title : </h4>
<?= $this->input('banner-slider-drop-down-title', []) ?>

<h4>Image :</h4>
<?= $this->image('banner-slider-drop-down-image', []) ?>

<h4>Default </h4>
<?= $this->relation('banner-slider-drop-down-default', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['Stage'],
]) ?>

<h4>Except :</h4>
<?php $no = 0; ?>
<?php while($this->block('card-slider-one-block')->loop()) { ?>

    <h4>Stage to Except <?= $no ?>:</h4>
    <?= $this->relation('card-slider-one-stage-to-except-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Stage'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>

<h4>Only :</h4>
<?php $no = 0; ?>
<?php while($this->block('card-slider-one-block-only')->loop()) { ?>

    <h4>Stage to Only <?= $no ?>:</h4>
    <?= $this->relation('card-slider-one-stage-to-only-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Stage'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>
