<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 17:43
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<!-- Remove me. -->
<?php if ($this->block('card-four-columns-banner-block')->getCount() > 0) { ?>
<div class="podcasts mt-5">
    <div class="container">
    <div class="row">
            <div class="col-12 col-md-12 text-right">
                <div class="see-more__container container">
                    <?= $this->link('card-four-columns-banner-link', [
                        'class' => 'see-more container'
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="podcast__list">
            <?php $no = 1 ?>
            <?php while($this->block('card-four-columns-banner-block')->loop()) { ?>
                <?php
                if (
                    !$this->input('card-four-columns-banner-title-' . $no)->isEmpty() &&
                    !$this->link('card-four-columns-banner-link-' . $no)->isEmpty() &&
                    !$this->image('card-four-columns-banner-image-' . $no)->isEmpty()
                ) {

                    $contentHtml = null;
                ?>
                    <?php
                        $contentHtml .= '<div class="podcast__item text-center">';
                        $contentHtml .= $this
                            ->image('card-four-columns-banner-image-' . $no)
                            ->getThumbnail('default')
                            ->getHtml();
                        $contentHtml .=
                            '<p>' . $this->input('card-four-columns-banner-title-' . $no)->getData() ?? '' . '</p>';
                        $contentHtml .= '</div>';
                    ?>


                    <?= $this->link('card-four-columns-banner-link-' . $no, [
                        'textPrefix' => $contentHtml
                    ]) ?>

                <?php } ?>

                <?php $no++ ?>
            <?php } ?>

        </div>
    </div>
</div>
<?php } ?>
