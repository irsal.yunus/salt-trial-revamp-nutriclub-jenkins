<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 17:43
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Link :</h4>
<?= $this->link('card-four-columns-banner-link', []) ?>

<?php $no = 1;?>
<?php while($this->block('card-four-columns-banner-block')->loop()) { ?>

    <h4>Title <?= $no ?></h4>
    <?= $this->input('card-four-columns-banner-title-' . $no, [
    ]) ?>

    <h4>Link <?= $no ?> </h4>
    <?= $this->link('card-four-columns-banner-link-' . $no , [
    ]) ?>

    <h4>Image <?= $no ?></h4>
    <?= $this->image('card-four-columns-banner-image-' . $no, [

    ]) ?>

    <?php $no++ ?>
<?php } ?>
