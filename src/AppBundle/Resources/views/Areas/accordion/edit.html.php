<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:19
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<?php $no = 1;?>
<?php while($this->block('accordion-content-block')->loop()) { ?>

    <h4>Title <?= $no ?></h4>
    <?= $this->input('accordion-title-' . $no, [
    ]) ?>

    <h4>Content <?= $no ?> </h4>
    <?= $this->wysiwyg('accordion-content-' .$no , [
    ])?>

    <?php $no++ ?>
<?php } ?>
