<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:19
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<!-- Remove me. -->
<div class="accordion-info">
    <?php $no = 1;?>
       <?php while($this->block('accordion-content-block')->loop()) { ?>
           <?php if (
               !$this->input('accordion-title-' . $no)->isEmpty() &&
               !$this->wysiwyg('accordion-content-' .$no)->isEmpty()
           ) { ?>
               <div class="accordion-section">
                    <div class="container pl-0">
                        <button class="accordion_trigger"><?= $this->input('accordion-title-' . $no) ?></button>
                    </div>
                <div class="panel">
                    <div class="container animated fadeIn">
                        <?= $this->wysiwyg('accordion-content-' .$no) ?>
                    </div>
                </div>
               </div>
           <?php $no++ ?>
           <?php } ?>
       <?php } ?>
</div>
