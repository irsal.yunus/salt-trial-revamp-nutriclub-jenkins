<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:32
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="content-detail_list">
    <div class="container-child pb-3">
        <div class="banner">
            <div>
                <?= !$this->image('product-group-small-banner')->isEmpty() ?
                    $this->image('product-group-small-banner')
                        ->getThumbnail('product-group-small-banner-thumbnail')
                        ->getHtml([
                            'disableWidthHeightAttributes' => true,
                            'class' => 'img-fluid d-none d-sm-block'
                        ]) : null
                ?>
                <?= !$this->image('product-group-small-banner-mobile')->isEmpty() ?
                    $this->image('product-group-small-banner-mobile')
                        ->getThumbnail('product-group-small-banner-mobile-thumbnail')
                        ->getHtml([
                            'disableWidthHeightAttributes' => true,
                            'class' => 'img-fluid  d-block d-sm-none'
                        ]) : null
                ?>
                <div class="w-50">
                    <div>
                        <?= $this->wysiwyg('product-group-small-banner-caption')->frontend() ?>
                        <?= $this->link('small-banner-link')->frontend() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
