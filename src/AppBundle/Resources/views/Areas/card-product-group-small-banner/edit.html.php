<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:32
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<h3>Product Group Small Banner</h3>

<h5>Small Banner Image Desktop</h5>
<?= $this->image('product-group-small-banner', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'product-group-small-banner-thumbnail',
    'uploadPath'    => '/product-group-small-banner/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h5>Small Banner Image Mobile</h5>
<?= $this->image('product-group-small-banner-mobile', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'product-group-small-banner-mobile-thumbnail',
    'uploadPath'    => '/product-group-small-banner/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h5>Small Banner Caption</h5>
<?= $this->wysiwyg('product-group-small-banner-caption', []) ?>

<h5>Small Banner Link</h5>
<?= $this->link('small-banner-link', [
    'reload'    => false,
]) ?>
