<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 05/02/2020
 * Time: 13:01
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Content </h4>
<?php $no = 1;?>
<?php while($this->block('card-slider-big-block')->loop()) { ?>

    <h4>Image desktop :</h4>
    <?= $this->image('card-slider-big-image-desktop-' . $no, []) ?>

    <h4>Image mobile :</h4>
    <?= $this->image('card-slider-big-image-mobile-' . $no, []) ?>

    <h4>Caption :</h4>
    <?= $this->input('card-slider-big-caption-' . $no, []) ?>

    <h4>Link :</h4>
    <?= $this->link('card-slider-big-link-' . $no, []) ?>

    <?php $no++; ?>
<?php } ?>
