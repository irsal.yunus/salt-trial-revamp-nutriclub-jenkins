<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 05/02/2020
 * Time: 13:01
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="container">
    <div class="card-slider-big mt-5">
<?php $no = 1;?>
<?php while($this->block('card-slider-big-block')->loop()) { ?>
<?php
if (
    !$this->image('card-slider-big-image-desktop-' . $no)->isEmpty() &&
    !$this->image('card-slider-big-image-mobile-' . $no)->isEmpty() &&
    !$this->input('card-slider-big-caption-' . $no)->isEmpty() &&
    !$this->link('card-slider-big-link-' . $no)->isEmpty()) {
    ?>
    <div class="card-slider-big__wrapper">
        <div class="card-slider-big__content">
            <?= $this->image('card-slider-big-image-desktop-' . $no, [
                'class' => 'd-none d-md-block'
            ])->frontend() ?>
            <?= $this->image('card-slider-big-image-mobile-' . $no, [
                'class' => 'card-slider-big__image-mobile d-block d-sm-none'
            ])->frontend() ?>
            <div class="overlay-area">
                <div class="overlay-area__content text-center">
                    <p><?= $this->input('card-slider-big-caption-' . $no)->getData() ?? '' ?></p>
                    <div class="btn-join-now">
                        <?= $this->link('card-slider-big-link-' . $no)->frontend() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $no++; ?>
    <?php
    }
}
?>
</div>
</div>
