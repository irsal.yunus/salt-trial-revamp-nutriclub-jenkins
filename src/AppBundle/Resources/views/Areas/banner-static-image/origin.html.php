<div class="content-product_header">
        <div class="content-product_header_bg">
            <div class="bg-header-detail">
                <img src="/assets/images/page_product/lactamil-detail-product.png" class="img-fluid d-none d-lg-block" />
                <!-- if mobile -->
                <img src="/assets/images/page_product/lactamil-detail-product-mobile.png" class="w-100 d-block d-lg-none" />
            </div>
        </div>
    </div>