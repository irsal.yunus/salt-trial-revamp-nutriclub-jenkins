<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>
<div class="content-product_header">
        <div class="content-product_header_bg">
            <div class="bg-header-detail">
                
                <img src="<?= $this->image('banner-static-image')->getThumbnail('banner-static-image-thumbnail') ?>" class="img-fluid d-none d-lg-block" />
                <!-- if mobile -->
                <img src="<?= $this->image('banner-static-image-mobile')->getThumbnail('banner-static-image-mobile') ?>" class="w-100 d-block d-lg-none"/>
            </div>
        </div>
    </div>