<?php 
/**
 * 
 * 
 */
?>

<h3>Banner Static Image</h3>

<h5>Banner Image Desktop</h5>
<?= $this->image('banner-static-image', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'banner-static-image',
    'uploadPath'    => '/banner-static-image/',
    'reload'        => false,
    'hidetext'      => true 
]) ?>

<h5>Banner Image Mobile</h5>
<?= $this->image('banner-static-image-mobile', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'banner-static-image-mobile',
    'uploadPath'    => '/banner-static-image/',
    'reload'        => false,
    'hidetext'      => true

])?>
