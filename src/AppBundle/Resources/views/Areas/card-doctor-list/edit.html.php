<?php

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title :</h4>
<?= $this->input('doctor-team-heading', []) ?>

<h4>Short Description:</h4>
<?= $this->wysiwyg('doctor-team-desc', []) ?>
