<?php
use Pimcore\Model\DataObject\TeamDoctor;
use Pimcore\Templating\GlobalVariables;
use Pimcore\Templating\PhpEngine;

/**
 *
 */

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
$limit = 10;

$doctorsListing = new TeamDoctor\Listing();
$doctorsListing->setLimit($limit);
$doctorsListing->load();

$doctors = $doctorsListing->getObjects();
?>

<div class="row">
    <div class="col-12 col-md-6 offset-md-3">
        <div class="doctor-team__heading">
            <h1><?= $this->input('doctor-team-heading') ?></h1>
            <hr>
        </div>
        <div class="doctor-team__description">
            <?= $this->wysiwyg('doctor-team-desc') ?>
        </div>
    </div>
</div>

<div class="doctor-team__lists">
    <div class="row">

        <?php
        if ($doctors)
        {
            $count = 0;
            foreach ($doctors as $doctor)
            {
                $count++;
                $class = $count % 2 !== 0 ? 'offset-md-2' : '';
                ?>
                <div class="doctor-team__lists-item col-12 col-md-4 <?= $class ?>">
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="col-5 ">
                                <?= $doctor->getProfilePicture()->getThumbnail('default')->getHtml([
                                    'class' => 'img-fluid',
                                    'alt'   => 'doctor-team-picture'
                                ]) ?>
                            </div>
                            <div class="col-7">
                                <div class="card-body">
                                    <h5 class="card-title"><?= $doctor->getName() ?></h5>
                                    <p class="card-text"><?= $doctor->getDescription()  ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
