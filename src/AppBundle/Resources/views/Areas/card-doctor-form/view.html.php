<?php

use Pimcore\Templating\GlobalVariables;
use Pimcore\Templating\PhpEngine;

/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>
<!-- card form -->
<div class="doctor-team__form" id="send_question">
    <div class="row">
        <div class="col-12 col-md-8 offset-md-2">
            <div class="card">
                <div class="card-header">
                    <div class="doctor-team__form-heading">
                        <h1><?= $this->input('doctor-team-form-heading') ?></h1>
                        <hr/>
                    </div>
                    <div class="doctor-team__form-description">
                        <?= $this->wysiwyg('doctor-team-form-description') ?>
                    </div>
                </div>
                <div class="card-body">
                    <!-- id="submitQuestion" -->
                    <form action="//www.tanyadok.com/tanyadokwidgetsapi/v1/nutriclub/create" method="post" >
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="name"><?= $this->t('NAME') ?> <span>*</span></label>
                                    <input type="text" id="name" class="form-control" aria-describedby="user name">
                                 </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="email"><?= $this->t('EMAIL')?><span>*</span></label>
                                    <input type="email" id="email" class="form-control" aria-describedby="user email">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="phonenumber"><?= $this->t('PHONE_NUMBER') ?></label>
                                    <input type="tel" id="phonenumber" class="form-control" aria-describedby="user phonenumber" placeholder="+62">
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="topicquestion"><?= $this->t('SELECT_QUESTION_TOPIC')?></label>
                                    <select class="form-control" id="topicquestion" aria-describedby="topic of question">
                                        <option selected disabled value="0">Pilih Topik Umum</option>
                                        <option value="1">Nutrisi Seimbang</option>
                                        <option value="2">Kondisi Medis Anak</option>
                                        <option value="3">Produk Nutricia</option>
                                        <option value="4">Kehamilan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="issue"><?= $this->t('MAIN_COMPLAINT')?> <span>*</span></label>
                                    <input type="text" id="issue" class="form-control" aria-describedby="user phonenumber" maxlength="80">
                                    <div class="d-flex justify-content-end">
                                        <p class="maxCharacter">
                                            0
                                        </p>
                                        <p>
                                            /80
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="issue-description"><?= $this->t('EXPLAIN_MORE_COMPLETE')?> <span>**</span></label>
                                    <textarea name="issue-description" id="issue-description" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-2 pr-0 col-md-1">
                                        <span>*</span>
                                    </div>
                                    <div class="col-10 pl-0 col-md-10 mandatory">
                                        <p><?= $this->t('MANDATORY')?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-2 pr-0 col-md-1">
                                        <span>**</span>
                                    </div>
                                    <div class="col-10 pl-0 col-md-10 notes">
                                        <?= $this->wysiwyg('doctor-team-form-notes', [])?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 offset-md-3">
                                <button class="doctor-team__button-submit btn btn-primary btn-block p-2" type="submit"><?= $this->t('ASK_DOCTOR')?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of card -->
