<div class="d-sm-flex justify-content-sm-around">
    <a href="#">
        <div class="card-product">
            <div class="text-center"><img src="/assets/images/page_product/LactamilInisis.png" class="img-fluid" /></div>
            <div class="name">Lactamil Inisis</div>
            <p>Penuhi nutrisi Mama di masa Persiapan Kehamilan</p>
            <a href="#">Pelajari Lebih Lanjut</a>
        </div>
    </a>
    <a href="#">
        <div class="card-product">
            <div class="text-center"><img src="/assets/images/page_product/LactamilPregnasis.png" class="img-fluid" /></div>
            <div class="name">Lactamil Pregnasis</div>
            <p>Penuhi nutrisi Mama di masa Kehamilan</p>
            <a href="#">Pelajari Lebih Lanjut</a>
        </div>
    </a>
    <a href="#">
        <div class="card-product">
            <div class="text-center"><img src="/assets/images/page_product/LactamilLactasis.png" class="img-fluid" /></div>
            <div class="name">Lactamil Lactasis</div>
            <p>Penuhi nutrisi Mama di masa Menyusui</p>
            <a href="#">Pelajari Lebih Lanjut</a>
        </div>
    </a>
</div>