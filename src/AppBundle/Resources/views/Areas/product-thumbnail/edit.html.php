<?php
/**
 * 
 * 
 */
?>

<h3>Product Thumbnail Widget</h3>

<?php $no = 1; ?>
<h5>Product Image <?= $no; ?></h5>
<?php while($this->block('content-product-thumbnail-block')->loop()) { ?>
<?= $this->image('product_thumbnail_image', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'content-product-image-thumbnail',
    'uploadPath'    => '/content-product-image/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h5>Product Title</h5>
<?= $this->input('product_title', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>
<h5>Short Description Product <?= $no; ?></h5>
<?= $this->input('short_description_product', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>

 <h5>Anchor link and text <?= $no; ?></h5>
 <?= $this->link('product_thumbnail_link') ?>
 <?php $no++; ?>
<?php } ?>