<div class="d-sm-flex justify-content-sm-around"
<?php while($this->block('content-product-thumbnail-block')->loop()) { ?>
    <a href="#">
        <div class="card-product">
            <div class="text-center"><img src="<?= $this->image('product_thumbnail_image')->getThumbnail('default') ?>" class="img-fluid" /></div>
            <div class="name"><?= $this->input('product_title') ?></div>
            <p><?= $this->input('short_description_product') ?></p>
            <a href="#">Pelajari Lebih Lanjut</a>
        </div>
    </a>
<?php } ?>
</div>