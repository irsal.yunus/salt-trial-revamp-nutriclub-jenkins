<?php
/**
 *
 */

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

?>

<?php while ($this->block('expert-card')->loop()) { ?>
    <div class="expert-team__lists-item col-12 col-md-4">
        <div class="card">
            <div class="card-header">
                <h3>
                    <?= $this->input('expert-card-title') ?>
                </h3>
            </div>
            <div class="card-body">
                <?= $this->wysiwyg('expert-card-content') ?>
            </div>
        </div>
    </div>
<?php } ?>
