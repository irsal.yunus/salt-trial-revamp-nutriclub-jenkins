<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="nutriclub-content-detail-product">
	<div class="container">
		<div class="row">
        <div class="col-lg-9 px-0 mx-lg-auto">
	    <nav class="nav flex-fill">
	        <div class="nav nav-tabs" role="tablist">
	            <a class="nav-item nav-link pt-3 pt-lg-2 active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><?= $this->input('tab-nav-text') ?></a>
	            <a class="nav-item nav-link pt-3 pt-lg-2" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><?= $this->input('tab-nav-text-2') ?></a>
	            <a class="nav-item nav-link pt-md-3 pt-lg-2" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><?= $this->input('tab-nav-text-3') ?></a>
	        </div>
        </nav>
       </div>
	</div>
	</div>
	<div class="tab-content tab-detail-product">
	    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
	        <div class="container-fluid">
	            <div class="row justify-content-center bg-panel">
	                <div class="col-12 px-0 text-center">
                        <?= !$this->image('img-inner-tab')->isEmpty() ?
                            $this->image('img-inner-tab')
                                ->getThumbnail('img-inner-tab-thumbnail')->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'd-none d-md-block img-fluid m-auto'
                                ]) : null
                        ?>
                        <?= !$this->image('img-inner-tab-mobile')->isEmpty() ?
                            $this->image('img-inner-tab-mobile')
                                ->getThumbnail('img-inner-tab-mobile-thumbnail')->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'd-block d-md-none img-fluid m-auto'
                                ]) : null
                        ?>
	                </div>
	            </div>
	            <div class="row detail-product__bg-panel-mobile--alt p-3">
	                <div class="col-lg-5 d-flex flex-wrap justify-content-center bg-white rounded-lg m-auto my-lg-3 py-5 px-1 p-lg-5">
	                    <?php while ($this->block('product-box-tab-1')->loop()) { ?>
	                        <div class="detail-product__box mx-3 mx-lg-5 mb-4 text-center">
                                <?= !$this->image('product-box-image')->isEmpty() ?
                                    $this->image('product-box-image')
                                        ->getThumbnail('product-box-image-thumb')
                                        ->getHtml([
                                            'disableWidthHeightAttributes' => true,
                                        ]) : null
                                ?>
	                            <h5 class="product__box-title"><?= $this->input('product-box-title') ?></h5>
	                            <span class="detail-product__box-weight"><?= $this->input('product-box-weight') ?></span>
	                        </div>
	                    <?php } ?>

	                    <div class="detail-product__box text-center w-100">
	                        <h6 class="my-2"><?= $this->input('product-flavor-main-title') ?></h6>
	                        <div class="flavor text-center mt-3">
								<?= $this->input('product-flavor-content') ?>
								<div class="container">
									<div class="row">
                                        <?php while ($this->block('product-flavour')->loop()) { ?>
                                            <div class="col-6 text-right mb-2">
                                                <?= !$this->image('product-flavor-image')->isEmpty() ?
                                                    $this->image('product-flavor-image')
                                                        ->getThumbnail('product-flavor-image-thumbnail')
                                                        ->getHtml([
                                                            'disableWidthHeightAttributes' => true
                                                        ]) : null
                                                ?>
                                            </div>
                                            <div class="col-6 pl-0 py-2 text-left mb-2">
                                                <span>
                                                    <?= $this->input('product-flavor-title')->frontend() ?>
                                                </span>
                                            </div>
                                        <?php } ?>
									</div>
								</div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
	        <div class="container-fluid py-3 text-center bg-panel">
                <?= !$this->image('img-inner-tab-2')->isEmpty() ?
                    $this->image('img-inner-tab-2')
                        ->getThumbnail('img-thumb')
                        ->getHtml([
                            'disableWidthHeightAttributes' => true,
                            'class' => 'd-none d-lg-block img-fluid m-auto'
                        ]) : null ?>
                <?= !$this->image('img-inner-tab-2-mobile')->isEmpty() ?
                    $this->image('img-inner-tab-2-mobile')
                        ->getThumbnail('img-inner-tab-mobile')
                        ->getHtml([
                            'disableWidthHeightAttributes' => true,
                            'class' => 'w-100 d-block d-lg-none'
                        ]) : null ?>
	        </div>
	    </div>
	    <div class="tab-pane fade serving" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
	        <div class="container-fluid">
	            <div class="row justify-content-center">
	                <div class="col-12 text-center px-0 bg-serving">
                        <?= !$this->image('img-inner-tab-3-1th')->isEmpty() ?
                            $this->image('img-inner-tab-3-1th')
                                ->getThumbnail('img-inner-tab-3-1th-thumb')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'd-none d-md-block img-fluid m-auto'
                                ]) : null
                        ?>
                        <?= !$this->image('img-inner-tab-3-1th-1')->isEmpty() ?
                            $this->image('img-inner-tab-3-1th-1')
                                ->getThumbnail('img-inner-tab-3-1th-1-thumb')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'img-fluid m-auto d-block d-md-none'
                                ]) : null
                        ?>
	                </div>
	                <div class="col-12 px-0 text-center bg-serving-dark">
                        <?= !$this->image('img-inner-tab-3-2th')->isEmpty() ?
                            $this->image('img-inner-tab-3-2th')
                                ->getThumbnail('img-inner-tab-3-1th-thumb')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'd-none d-md-block img-fluid m-auto'
                                ]) : null
                        ?>
                        <?= !$this->image('img-inner-tab-3-2th-1')->isEmpty() ?
                            $this->image('img-inner-tab-3-2th-1')
                                ->getThumbnail('img-inner-tab-3-2th-1-thumb')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'img-fluid m-auto d-block d-md-none'
                                ]) : null
                        ?>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
