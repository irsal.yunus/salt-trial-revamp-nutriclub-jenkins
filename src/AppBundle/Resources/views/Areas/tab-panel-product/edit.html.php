<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h3>Product Tab Panel</h3>

<h4>Tab Panel 1</h4>
<h6>Navigation Text Tab 1</h6>
<?= $this->input('tab-nav-text', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>
<h6>Navigation Text Tab 2</h6>
<?= $this->input('tab-nav-text-2', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>
<h6>Navigation Text Tab 3</h6>
<?= $this->input('tab-nav-text-3', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>

<h6>Image Inner Tab 1</h6>
<?= $this->image('img-inner-tab', [
    'title'         => 'Drag your image here',
    'uploadPath'    => '/img-inner-tab/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h6>Image Inner Tab 1</h6>
<?= $this->image('img-inner-tab-mobile', [
    'title'         => 'Drag your image here',
    'uploadPath'    => '/img-inner-tab/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h6>Text Inner Tab 1</h6>
<?= $this->input('text-inner-tab', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>

<?php $no = 1; ?>
<?php while ($this->block('content-block')->loop()) { ?>
    <h5>Content Block <?= $no ?></h5>
    <h6>Image Content Block <?= $no ?></h6>
    <?= $this->image('img-content-block-1', [
        'title'         => 'Drop your image here',
        'thumbnail'     => 'img-inner-tab',
        'uploadPath'    => '/img-inner-tab/',
        'reload'        => false,
        'hidetext'      => true
    ]) ?>
    <h6>Title Content Block <?= $no ?> </h6>
    <?= $this->input('title-content-block-1', [
        'htmlspecialchars'  => false,
        'placeholder'       => 'Input your text here'
    ]) ?>
    <h6>Text Content Block <?= $no ?></h6>
    <?= $this->input('text-content-block-1', [
        'htmlspecialchars'  => false,
        'placeholder'       => 'Input text here'
    ]) ?>
    <?php $no++; ?>
<?php } ?>

<h5>Product Box Tab 1</h5>
<?php $no = 1; ?>
<?php while ($this->block('product-box-tab-1')->loop()) { ?>
    <h6>Product Box Image <?= $no ?></h6>
    <?= $this->image('product-box-image', [
        'title'         => 'Drop your image here',
        'thumbnail'     => 'content-product-image',
        'uploadPath'    => '/content-product-image/',
        'reload'        => false,
        'hidetext'      => true
    ]) ?>
    <h6>Product Box Title <?= $no ?></h6>
    <?= $this->input('product-box-title', [
        'htmlspecialchars'  => false,
        'placeholder'       => 'Input your text here'
    ]) ?>
    <h6>Product Box Weight <?= $no ?></h6>
    <?= $this->input('product-box-weight', [
        'htmlspecialchars'  => false,
        'placeholder'       => 'Input your text here'
    ]) ?>
    <?php $no++; ?>
<?php } ?>

<h6>Product Flavor Box</h6>
<?= $this->input('product-flavor-main-title', []) ?>

<?php while ($this->block('product-flavour')->loop()) { ?>
<h5>Product Flavor Box Title</h5>
<?= $this->input('product-flavor-title', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>

<h5>Product Flavor Image</h5>
<?= $this->image('product-flavor-image', []) ?>
<?php } ?>

<h5>Product Flavor Box Content</h5>
<?= $this->input('product-flavor-content', [
    'htmlspecialchars'  => false,
    'placeholder'       => 'Input your text here'
]) ?>

<h4>Tab Panel 2</h4>
<h6>Image Inner Tab 2</h6>
<?= $this->image('img-inner-tab-2', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'img-inner-tab',
    'uploadPath'    => '/img-inner-tab/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h6>Image Inner Tab 2 Mobile</h6>
<?= $this->image('img-inner-tab-2-mobile', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'img-inner-tab-mobile',
    'uploadPath'    => '/img-inner-tab/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h4>Tab Panel 3</h4>
<h6>Image Inner Tab 3 first</h6>
<?= $this->image('img-inner-tab-3-1th', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'img-inner-tab',
    'uploadPath'    => '/img-inner-tab/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h6>Image Inner Tab 3.1 first</h6>
<?= $this->image('img-inner-tab-3-1th-1', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'img-inner-tab',
    'uploadPath'    => '/img-inner-tab/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h6>Image Inner Tab 3 Second</h6>
<?= $this->image('img-inner-tab-3-2th', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'img-inner-tab',
    'uploadPath'    => '/img-inner-tab/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h6>Image Inner Tab 3 Second</h6>
<?= $this->image('img-inner-tab-3-2th-1', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'img-inner-tab',
    'uploadPath'    => '/img-inner-tab/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

