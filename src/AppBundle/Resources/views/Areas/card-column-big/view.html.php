<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 13:28
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Helper\CardArticleWidgetHelper;
use AppBundle\Model\DataObject\Article;
use Pimcore\Model\DataObject\{ArticleCategory,
    ArticleRoleFocus,
    ArticleSubCategory,
    Stage,
    StageAge,
    StageGroup,
    SubStage};
use Ramsey\Uuid\Uuid;
use Zend\Paginator\Paginator;

/** @var ArticleRoleFocus $roleFocus */
$roleFocus = !$this->relation('relation-role-focus')->isEmpty() ?
    $this->relation('relation-role-focus')->getElement() : null;

/** @var ArticleCategory $articleCategory */
$articleCategory = !$this->relation('relation-article-category')->isEmpty() ?
    $this->relation('relation-article-category')->getElement() : null;

/** @var ArticleSubCategory $articleSubCategory */
$articleSubCategory = !$this->relation('relation-article-sub-category')->isEmpty() ?
    $this->relation('relation-article-sub-category')->getElement() : null;

/** @var Stage $stage */
$stage = !$this->relation('relation-stage')->isEmpty() ?
    $this->relation('relation-stage')->getElement() : null;

/** @var StageGroup $stageGroup */
$stageGroup = !$this->relation('relation-stage-group')->isEmpty() ?
    $this->relation('relation-stage-group')->getElement() : null;

/** @var SubStage $subStage */
$subStage = !$this->relation('relation-sub-stage')->isEmpty() ?
    $this->relation('relation-sub-stage')->getElement() : null;

/** @var StageAge $stageAge */
$stageAge = !$this->relation('relation-stage-age')->isEmpty() ?
    $this->relation('relation-stage-age')->getElement() : null;

/** @var Paginator $articleObject */
$articleObject = CardArticleWidgetHelper::getObjectWithfilter(
    $roleFocus,
    $articleCategory,
    $articleSubCategory,
    $stage,
    $stageGroup,
    $subStage,
    $stageAge
);
if ($stageGroup) {
    $articleObjectn = [];
    foreach ($articleObject as $obj) {
        $articleObjectn[] = $obj;
    }

    $articleObject = array_merge([], ...$articleObjectn);
}
if (!$stageGroup) {
    $articleObject;
}

if ($articleObject) {
?>
<div class="card-column-big">
    <div class="card-column-big__wrapper">

        <?php
        foreach ($articleObject as $article) {
        ?>
            <div class="card-column-big__content">
                <a href="<?= $article->getRouter() ?>">
                    <div class="row">
                        <div class="card-column-big__figure col-4">
                            <?= $article->getImgDekstopHtml() ?>
                        </div>
                        <div class="col-8">
                            <h6><?= $article->getCategory()->getName() ?></h6>
                            <p><?= $article->getTitle() ?></p>
                        </div>
                    </div>
                </a>
            </div>
        <?php
        }
        ?>

        <?php if (!$this->link('card-article-link')->isEmpty()) {  ?>
        <div class="btn-see-article text-center">
            <?= $this->link('card-article-link')->frontend() ?>
        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
