<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 13:28
 */
?>

<h4>Filter Article By RoleFocus : </h4>
<?= $this->relation('relation-role-focus', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['ArticleRoleFocus'],
]) ?>

<h4>Filter Article By ArticleCategory : </h4>
<?= $this->relation('relation-article-category', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['ArticleCategory'],
]) ?>

<h4>Filter Article By ArticleSubCategory : </h4>
<?= $this->relation('relation-article-sub-category', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['ArticleSubCategory'],
]) ?>

<h4>Filter Article By Stage : </h4>
<?= $this->relation('relation-stage', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['Stage'],
]) ?>

<h4>Filter Article By StageGroup : </h4>
<?= $this->relation('relation-stage-group', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['StageGroup'],
]) ?>

<h4>Filter Article By SubStage : </h4>
<?= $this->relation('relation-sub-stage', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['SubStage'],
]) ?>

<h4>Filter Article By StageAge : </h4>
<?= $this->relation('relation-stage-age', [
    'width' => '500px',
    'types' => ['object'],
    'subtypes' => [
        'object' => ['object']
    ],
    'classes' => ['StageAge'],
]) ?>

<h4>Link :</h4>
<?= $this->link('card-article-link', []) ?>
