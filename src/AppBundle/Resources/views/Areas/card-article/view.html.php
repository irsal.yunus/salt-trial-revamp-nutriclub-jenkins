<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 17:39
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Helper\CardArticleWidgetHelper;
use AppBundle\Model\DataObject\Article;
use Pimcore\Model\DataObject\{ArticleCategory,
    ArticleRoleFocus,
    ArticleSubCategory,
    Stage,
    StageAge,
    StageGroup,
    SubStage};
use Ramsey\Uuid\Uuid;
use Zend\Paginator\Paginator;

/** @var ArticleRoleFocus $roleFocus */
$roleFocus = !$this->relation('relation-role-focus')->isEmpty() ?
    $this->relation('relation-role-focus')->getElement() : null;

/** @var ArticleCategory $articleCategory */
$articleCategory = !$this->relation('relation-article-category')->isEmpty() ?
    $this->relation('relation-article-category')->getElement() : null;

/** @var ArticleSubCategory $articleSubCategory */
$articleSubCategory = !$this->relation('relation-article-sub-category')->isEmpty() ?
    $this->relation('relation-article-sub-category')->getElement() : null;

/** @var Stage $stage */
$stage = !$this->relation('relation-stage')->isEmpty() ?
    $this->relation('relation-stage')->getElement() : null;

/** @var StageGroup $stageGroup */
$stageGroup = !$this->relation('relation-stage-group')->isEmpty() ?
    $this->relation('relation-stage-group')->getElement() : null;

/** @var SubStage $subStage */
$subStage = !$this->relation('relation-sub-stage')->isEmpty() ?
    $this->relation('relation-sub-stage')->getElement() : null;

/** @var StageAge $stageAge */
$stageAge = !$this->relation('relation-stage-age')->isEmpty() ?
    $this->relation('relation-stage-age')->getElement() : null;

$limit = $this->numeric('article-limit')->isEmpty() ? 6 : $this->numeric('article-limit')->getData();
/** @var Paginator $articleObject */
$articleObject = CardArticleWidgetHelper::getObjectWithfilter(
    $roleFocus,
    $articleCategory,
    $articleSubCategory,
    $stage,
    $stageGroup,
    $subStage,
    $stageAge,
    0,
    $limit
);
if ($stageGroup) {
    $articleObjectn = [];
    foreach ($articleObject as $obj) {
        $articleObjectn[] = $obj;
    }

    $newListing = new Article\Listing();
    $newListing->setObjects(array_merge([], ...$articleObjectn));
    $articleObject = new Paginator($newListing);
    $articleObject->setCurrentPageNumber(0);
    $articleObject->setItemCountPerPage($limit);
}

$formId = 'card__articles-load-more-' . Uuid::uuid4();
?>

<?php $this->headLink()->offsetSetStylesheet(3, '/assets/css/pages/article.css'); ?>
<?php if($articleObject) { ?>
<div class="card__articles">
        <form id="<?= $formId ?>">
            <input type="hidden" name="roleFocusId" value="<?= $roleFocus ? $roleFocus->getId() : null ?>">
            <input type="hidden" name="categoryId" value="<?= $articleCategory ? $articleCategory->getId() : null ?>">
            <input type="hidden" name="subCategoryId" value="<?= $articleSubCategory ? $articleSubCategory->getId() : null ?>">
            <input type="hidden" name="stageId" value="<?= $stage ? $stage->getId() : null ?>">
            <input type="hidden" name="stageGroupId" value="<?= $stageGroup ? $stageGroup->getId() : null ?>" >
            <input type="hidden" name="subStageId" value="<?= $subStage ? $subStage->getId() : null ?>">
            <input type="hidden" name="stageAgeId" value="<?= $stageAge ? $stageAge->getId() : null ?>">
            <input type="hidden" id="nextPage" name="nextPage" value="2" form-id="<?= $formId ?>">
            <input type="hidden" id="currentPageLimit" name="limit" value="<?= $limit ?>">
        </form>
        <div class="container">
            <h2 class="heading-blue"><?= $this->input('card-article-title')->getData() ?></h2>
            <div class="article-container" name="data-article" id="<?= $formId ?>">
                <div class="article-preloader">
                    <?= $this->render('Include/preloader.html.php') ?>
                </div>
                <div class="row article-list">
                    <?= $this->render('Widgets/CardArticle/list.html.php', [
                        'data' => $articleObject
                    ]) ?>
                </div>
            </div>
            <?php if(!$this->checkbox('isPagination')->isChecked()): ?>
                <div class="card__articles-button-wrapper text-center">
                    <?= $this->link('card-article-link', [
                        'class' => 'card__articles-button btn'
                    ]) ?>
                </div>
            <?php endif; ?>
            <?php if($this->checkbox('isPagination')->isChecked()): ?>
                <?= $this->render('Include/paging.html.php', get_object_vars($articleObject->getPages())) ?>
            <?php endif; ?>
        </div>
    </div>
<?php } ?>
