<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 02/03/2020
 * Time: 14:43
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>
<div class="content-porduct_section bg-acean2">
            <div class="container-child content-porduct_sort_desc">
                <div class="content-porduct_sort_desc-filter text-center">
                    <h2><?= $this->input('card-product-filter-title')->frontend() ?></h2>
                    <?= $this->wysiwyg('card-product-filter-content')->frontend() ?>
                </div>
                <div class="d-sm-flex">
                    <div class="col-md-6">
                        <div class="dropdown-product">
                            <div class="d-flex justify-content-between label">
                                <div class="">
                                    <?= $this->input('pre-select-title')->frontend() ?>
                                </div>
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </div>
                            <ul class="dropdown-product-menu first">
                                <?php while ($this->block('stage-block')->loop()) { ?>
                                    <li class="<?= $this->checkbox('isActive')->isChecked() ? 'active' : '' ?>" onclick="openSub(<?= $this->block('stage-block')->getCurrent() ?>)">
                                        <div class="d-lfex">
                                            <?= !$this->image('stage-image')->isEmpty() ? $this->image('stage-image')
                                                ->getThumbnail('')
                                                ->getHtml([]) : null
                                            ?>
                                            <?= $this->input('stage-name')->frontend() ?>
                                        </div>
                                    </li>
                                    <?php $isActive = false ?>
                                <?php } ?>
                            </ul>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="dropdown-product" id="condition">
                            <div class="d-flex justify-content-between label no">
                                <div class=""><?= $this->input('pre-select-title-second')->frontend() ?></div>
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </div>
                            <?php while ($this->block('stage-block')->loop()) { ?>
                                <ul class="dropdown-product-menu <?= $this->input('select-class')->frontend() ?>">
                                    <!-- <li><span><?= $this->input('stage-name')->frontend() ?></span></li> -->

                                    <?php while ($this->block('sub-stage-block')->loop()) { ?>
                                        <?php
                                        if (!$this->link('sub-stage-link')->isEmpty()) {
                                        ?>
                                            <li><?= $this->link('sub-stage-link')->frontend() ?></li>
                                        <?php
                                        }
                                        ?>
                                    <?php } ?>

                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
