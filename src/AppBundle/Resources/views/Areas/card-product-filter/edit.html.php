<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 02/03/2020
 * Time: 14:44
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>
<h3>Card Product Filter</h3>

<h4>Title</h4>
<?= $this->input('card-product-filter-title', [
    'htmlspecialchars'      => false,
    'placeholder'           => 'Input your title here'
]) ?>

<h4>Content</h4>
<?= $this->wysiwyg('card-product-filter-content', []) ?>

<h4>Pre Select Title</h4>
<?= $this->input('pre-select-title', []) ?>

<h4>Pre Select Title Second</h4>
<?= $this->input('pre-select-title-second', []) ?>

<h4>Stage Block :</h4>
<?php $no = 1; ?>
<?php while ($this->block('stage-block')->loop()) { ?>
    <h4>Stage Name <?= $no?> :</h4>
    <?= $this->input('stage-name') ?>
    <h4>Stage Image <?= $no?> :</h4>
    <?= $this->image('stage-image') ?>

    <h4>Select Class <?= $no?> :</h4>
    <?= $this->input('select-class') ?>

    <h4>Active :</h4>
    <?= $this->checkbox('isActive', []) ?>

    <h4>Sub Stage : </h4>
    <?php $noSubStage = 1; ?>
    <?php while ($this->block('sub-stage-block')->loop()) { ?>
        <h4>Sub Stage Link <?= $noSubStage ?> : </h4>
        <?= $this->link('sub-stage-link') ?>
        <?php $noSubStage++; ?>
    <?php } ?>

    <?php $no++; ?>
<?php } ?>
<hr>
