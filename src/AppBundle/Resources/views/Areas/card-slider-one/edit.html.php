<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:53
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document\Tag;

$editModeModels = $this->editModeModel;

/** @var Tag $editModeModel */
foreach ($editModeModels as $editModeModel) {
    $realName = ucwords(str_replace('-', ' ', $editModeModel->getRealName()));
    echo '<h4>'. $realName .' : </h4>';
    echo $editModeModel;
}
?>

<h4>Render First :</h4>
<?php $noRf = 0; ?>
<?php while ($this->block('tool-list-render-first-block')->loop()) { ?>

    <h4>Tool to Render First <?= $noRf ?></h4>
    <?= $this->relation('tools-list-render-first-' . $noRf, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Stage'],
    ]) ?>

    <?php $noRf++; ?>
<?php } ?>

<h4>Except :</h4>
<?php $no = 0; ?>
<?php while($this->block('card-slider-one-block')->loop()) { ?>

    <h4>Stage to Except <?= $no ?>:</h4>
    <?= $this->relation('card-slider-one-stage-to-except-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Stage'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>

<h4>Only :</h4>
<?php $no = 0; ?>
<?php while($this->block('card-slider-one-block-only')->loop()) { ?>

    <h4>Stage to Only <?= $no ?>:</h4>
    <?= $this->relation('card-slider-one-stage-to-only-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Stage'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>
