<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:52
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Countly\Document\Areabrick\CardSliderOne\CountlyModel;
use AppBundle\Countly\LinkBuilder;
use AppBundle\Model\Document\Areabrick\CardSliderOne;
use AppBundle\Model\Widget\CardSliderOneActionHandlerModel;
use AppBundle\Model\DataObject\Stage;

$stages = new Stage\Listing();
$reArrange = [];
$reArrangeAsExcept = [];
$reArrangeWithoutExcept = true;
if (!$this->block('tool-list-render-first-block')->isEmpty()) {
    $noRf = 0;
    while ($this->block('tool-list-render-first-block')->loop()) {
        if (!$this->relation('tools-list-render-first-' . $noRf)->isEmpty()) {
            /** @var Stage $stage */
            $stage = $this->relation('tools-list-render-first-' . $noRf)->getElement();
            $reArrange[] = $stage;
            $reArrangeAsExcept[] = $stage->getId();
        }
        $noRf++;
    }
}
if (!$this->block('card-slider-one-block')->isEmpty()) {
    $listId = [];
    $no = 0;
    while($this->block('card-slider-one-block')->loop()) {
        if (!$this->relation('card-slider-one-stage-to-except-' . $no)->isEmpty()) {
            /** @var Stage $stage */
            $stage = $this->relation('card-slider-one-stage-to-except-' . $no)->getElement();
            $listId[] =  $stage->getId();
        }
        $no++;
    }
    $listId = array_unique(array_merge($listId, $reArrangeAsExcept));
    if (count($listId) > 0) {
        $stages->addConditionParam('oo_id NOT IN('. implode(',', $listId) .')', null, 'AND');
        $reArrangeWithoutExcept = false;
    }
}
if ($reArrangeWithoutExcept && $reArrangeAsExcept) {
    $stages->addConditionParam('oo_id NOT IN('. implode(',', $reArrangeAsExcept) .')', null, 'AND');
}
if (!$this->block('card-slider-one-block-only')->isEmpty()) {
    $listIdOnly = [];
    $no = 0;
    while($this->block('card-slider-one-block-only')->loop()) {
        if (!$this->relation('card-slider-one-stage-to-only-' . $no)->isEmpty()) {
            /** @var Stage $stage */
            $stage = $this->relation('card-slider-one-stage-to-only-' . $no)->getElement();
            $listIdOnly[] =  $stage->getId();
        }
        $no++;
    }
    if (count($listIdOnly) > 0) {
        $stages->addConditionParam('oo_id IN('. implode(',', $listIdOnly) .')', null, 'AND');
    }
}
$stages->setOrderKey('orderIndex');
$stages->setOrder('asc');
$stages->load();

$stages = array_merge($reArrange, $stages->getObjects());

/** @var CardSliderOne $viewModeModel */
$viewModeModel = $this->viewModeModel;
?>
<?php if ($stages) { ?>
<div class="card-slider-one">
    <div class="container">
        <h2 class="heading-blue"><?= $viewModeModel->getTitle()->frontend() ?></h2>
        <div class="card-slider-one__wrapper">
            <?php
            /** @var Stage $object */
            foreach ($stages as $object) {
                $slug = $object->getSlug();

                $url = $object->getRouter();

                $imgIcon = $object->getIconThumbnail('card-slider-one-icon');

                $name = $object->getName();
                $customAttr = $this->websiteConfig('STAGE_ATTRIBUTE_LINK', null);
                ?>
                <?= LinkBuilder::start($object, new CountlyModel, 'card-slider-one__item', $customAttr) ?>
                    <?php
                    /** @var CardSliderOneActionHandlerModel $model */
                    if (($model = $this->myCardSliderOneSessionData) && $model->getStageSlug() === $slug) {
                        ?>
                        <span class="last-visit"><?= $model->getLabelTitle() ?></span>
                        <?php
                    }
                    ?>
                    <?= $imgIcon ?>
                    <h5><?= $name ?></h5>
                <?= LinkBuilder::stop() ?>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>
