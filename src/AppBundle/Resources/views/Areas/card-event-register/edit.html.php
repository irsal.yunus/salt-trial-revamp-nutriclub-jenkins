<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/06/2020
 * Time: 19:48
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Link Before Login :</h4>
<?= $this->link('card-event-register-link-before-login', []) ?>

<h4>Description Before Login :</h4>
<?= $this->wysiwyg('card-event-register-description-before-login', []) ?>

<hr>

<h4>Link After Login :</h4>
<?= $this->link('card-event-register-link-after-login', []) ?>

<h4>Link Hide Form :</h4>
<?= $this->checkbox('card-event-register-checkbox-hide-after-login', []) ?>

<h4>Description After Login :</h4>
<?= $this->wysiwyg('card-event-register-description-after-login', []) ?>

<hr>

<h4>Link Login & Joined :</h4>
<?= $this->link('card-event-register-link-login-and-joined', []) ?>

<h4>Description Login & Joined :</h4>
<?= $this->wysiwyg('card-event-register-description-login-and-joined', []) ?>

<hr>
