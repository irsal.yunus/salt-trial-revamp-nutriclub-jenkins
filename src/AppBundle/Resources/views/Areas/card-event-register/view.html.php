<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/06/2020
 * Time: 19:48
 */

use AppBundle\Model\DataObject\Event;

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$user = $app->getUser();
$userRewards = $app->getSession()->get('UserProfile', null);

$isJoined = $this->request()->getParameter('isJoined');
/** @var Event $eventObj */
$eventObj = $this->request()->getParameter('eventObj');
?>

<?php
// not logged in.
if (!$user && !$userRewards) {
    ?>
    <div class="row justify-content-center">
        <div class="col-12 col-md-4">
            <?php
            if (
                ($eventObj instanceof Event) &&
                !$eventObj->getButtonBeforeLogin()
            ) {
                ?>
                <?= $this->link('card-event-register-link-before-login', [
                    'class' => 'event-body__btn'
                ])->frontend() ?>
                <?= $this->wysiwyg('card-event-register-description-before-login', [
                    'class' => 'event-body__info'
                ])->frontend() ?>
                <?php
            }
            ?>
            <?php
            if (($eventObj instanceof Event) &&
                $buttonBeforeLogin = $eventObj->getButtonBeforeLogin()
            ) {
                if (!$eventObj->getHideBeforeLogin()) {
                    echo $buttonBeforeLogin->getHtml();

                    echo  "<div class='card-body__info'>".$eventObj->getDescriptionBeforeLogin()."</div>";
                }
            }
            ?>
        </div>
    </div>
    <?php
}
?>

<?php
// logged in & not joined
if ($user && $userRewards && !$isJoined) {
    ?>
    <div class="row justify-content-center">
        <div class="col-12 col-md-4">
            <?php
            if (!$hideForm = $eventObj->getHideFormLoggedInNotJoined()) {
                ?>
                <form method="post">
                    <button type="submit" class="event-body__btn"><?= $this->t('EVENT_BUTTON_JOIN') ?></button>
                </form>
                <?php
            }
            ?>

            <?php
            if (
                ($eventObj instanceof Event) &&
                !$eventObj->getButtonLoggedInNotJoined()
            ) {
                ?>
                <?= $this->link('card-event-register-link-after-login', [
                    'class' => 'event-body__btn'
                ])->frontend() ?>
                <?= $this->wysiwyg('card-event-register-description-after-login', [
                    'class' => 'event-body__info'
                ])->frontend() ?>
            <?php
            }
            ?>

            <?php
            if (
                ($eventObj instanceof Event) &&
                $eventObj->getButtonLoggedInNotJoined()
            ) {
                if (!$eventObj->getHideLoggedInNotJoined()) {
                    echo $eventObj->getButtonLoggedInNotJoined()->getHtml();
                    echo  "<div class='card-body__info'>".$eventObj->getDescriptionLoggedInNotJoined()."</div>";
                }
            }
            ?>
        </div>
    </div>
    <?php
}
?>

<?php
// logged in & joined
if ($user && $userRewards && $isJoined) {
    ?>
    <div class="row justify-content-center">
        <div class="col-12 col-md-4">
            <?php
            if (
                ($eventObj instanceof Event) &&
                !$eventObj->getButtonLoggedInAndJoin()
            ) {
                ?>
                <?= $this->link('card-event-register-link-login-and-joined', [
                    'class' => 'event-body__btn'
                ])->frontend() ?>
                <?= $this->wysiwyg('card-event-register-description-login-and-joined', [
                    'class' => 'event-body__info'
                ])->frontend() ?>
                <?php
            }
            ?>

            <?php
            if (
                ($eventObj instanceof Event) &&
                $eventObj->getButtonLoggedInAndJoin()
            ) {
                if (!$eventObj->getHideLoggedInAndJoin()) {
                    echo $eventObj->getButtonLoggedInAndJoin()->getHtml();

                    echo "<div class='card-body__info'>".$eventObj->getDescriptionLoggedInAndJoin()."</div>";
                }
            }
            ?>
        </div>
    </div>
    <?php
}
?>
