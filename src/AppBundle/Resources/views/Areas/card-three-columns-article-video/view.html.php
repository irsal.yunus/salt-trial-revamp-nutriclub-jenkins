<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 17:40
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */


use AppBundle\Model\DataObject\ArticleVideo;

$latestArticleVideo = new ArticleVideo\Listing();
$latestArticleVideo->setLimit(5);
$latestArticleVideo->load();
?>
<?php if ($latestArticleVideo->getCount() > 0) { ?>
<div class="video pt-5">
    <div class="container">
        <div class="row">
            <div class="container">
                <div class="col-12 col-md-12 text-right mr-4">
                    <?= $this->link('card-three-columns-article-video-link', [
                        'class' => 'see-more container'
                    ]) ?>
                </div>  
            </div>
        </div>
        <div class="video__list">
            <?php
            foreach ($latestArticleVideo as $articleVideo) {
                $video = $articleVideo->getVideo() ?
                    $articleVideo->getVideo()->getData() : null;

                $title = $articleVideo->getTitle();

                $link = $articleVideo->getLink();

                $videoSrc = $video ? 'https://img.youtube.com/vi/'.$video.'/mqdefault.jpg' : 'https://via.placeholder.com/250';

                $text = '<div class="video__list-item-figure is-video">';
                $text .= '<img class="img-video img-fluid" src="'. $videoSrc .'">';
                $text .= '<img src="/assets/images/icon-play.svg" alt="play-button" class="play-button">';
                $text .= '</div>';
                $text .= '<p class="text-left px-md-1">' . $title . '</p>';

                $link->setText($text);
            ?>

                    <div class="video__list-item">
                        <?php echo $link->getHtml(false); ?>
                    </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<?php } ?>
