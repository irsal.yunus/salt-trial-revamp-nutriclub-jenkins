<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 07/02/2020
 * Time: 12:55
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<?php
if (
    !$this->input('card-three-image-title')->isEmpty() &&
    !$this->block('card-three-image-block')->isEmpty()
) {
    ?>
    <div class="card-three-image">
        <div class="bg-aqua">
            <div class="container">
                <div class="card-three-image__wrapper">
                    <h2 class="heading-blue"><?= $this->input('card-three-image-title')->getData() ?? '' ?></h2>
                    <div class="row mt-4 text-center color-blue">
                        <?php $no = 1; ?>
                        <?php while ($this->block('card-three-image-block')->loop()) { ?>

                            <?php
                            if (!$this->image('card-three-image-' . $no)->isEmpty() &&
                                !$this->link('card-three-image-link-' . $no)->isEmpty() &&
                                !$this->input('card-three-image-title-' . $no)->isEmpty()) {

                                $img = '<div class="image-wrapper">'
                                    . $this->image('card-three-image-' . $no)->frontend()
                                    . '</div>';

                                $title = '<p>' . $this->input('card-three-image-title-' . $no) . '</p>';

                                $link = $this->link('card-three-image-link-' . $no, [
                                    'textPrefix' => $img . $title
                                ])->frontend();
                            ?>

                                <div class="col">
                                    <?= $link ?>
                                </div>

                            <?php $no++; ?>
                        <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
