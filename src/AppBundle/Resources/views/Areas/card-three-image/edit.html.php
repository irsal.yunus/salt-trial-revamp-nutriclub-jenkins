<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 07/02/2020
 * Time: 12:55
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title : </h4>
<?= $this->input('card-three-image-title', []) ?>

<?php $no = 1;?>
<?php while($this->block('card-three-image-block')->loop()) { ?>

    <h4>Image <?= $no ?></h4>
    <?= $this->image('card-three-image-' . $no, []) ?>

    <h4>Link <?= $no ?></h4>
    <?= $this->link('card-three-image-link-' . $no, []) ?>

    <h4>Title <?= $no ?></h4>
    <?= $this->input('card-three-image-title-' . $no, []) ?>

    <?php $no++; ?>
<?php } ?>
