<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:59
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\Tool;

$tools = new Tool\Listing();

$reArrange = [];
$reArrangeAsExcept = [];
$reArrangeWithoutExcept = true;
if (!$this->block('tool-list-render-first-block')->isEmpty()) {
    $noRf = 0;
    while ($this->block('tool-list-render-first-block')->loop()) {
        if (!$this->relation('tools-list-render-first-' . $noRf)->isEmpty()) {
            /** @var Tool $tool */
            $tool = $this->relation('tools-list-render-first-' . $noRf)->getElement();
            $reArrange[] = $tool;
            $reArrangeAsExcept[] = $tool->getId();
        }
        $noRf++;
    }
}
if (!$this->block('tool-list-block')->isEmpty()) {
    $listId = [];
    $no = 0;
    while($this->block('tool-list-block')->loop()) {
        if (!$this->relation('tool-list-stage-to-except-' . $no)->isEmpty()) {
            /** @var Tool $tool */
            $tool = $this->relation('tool-list-stage-to-except-' . $no)->getElement();
            $listId[] =  $tool->getId();
        }
        $no++;
    }
    $listId = array_unique(array_merge($listId, $reArrangeAsExcept));
    if (count($listId) > 0) {
        $tools->addConditionParam('oo_id NOT IN('. implode(',', $listId) .')', null, 'AND');
        $reArrangeWithoutExcept = false;
    }
}
if ($reArrangeWithoutExcept && $reArrangeAsExcept) {
    $tools->addConditionParam('oo_id NOT IN('. implode(',', $reArrangeAsExcept) .')', null, 'AND');
}
if (!$this->block('tool-list-block-only')->isEmpty()) {
    $listIdOnly = [];
    $no = 0;
    while($this->block('tool-list-block-only')->loop()) {
        if (!$this->relation('tool-list-stage-to-only-' . $no)->isEmpty()) {
            /** @var Tool $tool */
            $tool = $this->relation('tool-list-stage-to-only-' . $no)->getElement();
            $listIdOnly[] =  $tool->getId();
        }
        $no++;
    }
    if (count($listIdOnly) > 0) {
        $tools->addConditionParam('oo_id IN('. implode(',', $listIdOnly) .')', null, 'AND');
    }
}
$tools->load();

$toolObjects = array_merge($reArrange, $tools->getObjects());
$link = !$this->link('tool-list-link')->isEmpty() ? $this->link('tool-list-link', [
    'class' => 'see-more'
])->frontend() : null;
?>
<?php if ($toolObjects) { ?>
<div class="tool-list mt-5 mb-5" id="tools">
    <div class="container">
        <div class="row mb-2">
            <div class="tool-list__header col-7 col-md-6">
                <h2 class="heading-blue m-0 pb-1"><?= $this->input('tool-list-title') ?></h2>
            </div>
            <div class="col-5 col-md-6 text-right">
                <?= $link ?>
            </div>
        </div>
        <div class="tool-list__wrapper">
            <?php
            /** @var Tool $object */
            foreach ($toolObjects as $object) {
                $url = $object->getCurrentLink();

                $img = $object->getIconThumbnail('tool-list-icon', [
                    'class' => 'img-fluid'
                ]);

                $generatedLink = $object->getCurrentLink();

                $text = '<h5>'. $object->getName() .'</h5>';

                $linkClass = $generatedLink->getClass();
                $generatedLink->setClass( 'tool-list__item text-center' . $linkClass);
                $generatedLink->setText($img . $text);

                echo $generatedLink->getHtml(false);
            }
            ?>
        </div>
    </div>
</div>
<?php } ?>
