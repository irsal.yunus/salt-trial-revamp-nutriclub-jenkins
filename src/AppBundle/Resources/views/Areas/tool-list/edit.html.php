<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:59
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title : </h4>
<?= $this->input('tool-list-title', []) ?>

<h4>Link : </h4>
<?= $this->link('tool-list-link', []) ?>

<h4>Render First :</h4>
<?php $noRf = 0; ?>
<?php while ($this->block('tool-list-render-first-block')->loop()) { ?>

    <h4>Tool to Render First <?= $noRf ?></h4>
    <?= $this->relation('tools-list-render-first-' . $noRf, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Tool'],
    ]) ?>

    <?php $noRf++; ?>
<?php } ?>

<h4>Except :</h4>
<?php $no = 0; ?>
<?php while($this->block('tool-list-block')->loop()) { ?>

    <h4>Tool to Except <?= $no ?>:</h4>
    <?= $this->relation('tool-list-stage-to-except-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Tool'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>

<h4>Only :</h4>
<?php $no = 0; ?>
<?php while($this->block('tool-list-block-only')->loop()) { ?>

    <h4>Tool to Only <?= $no ?>:</h4>
    <?= $this->relation('tool-list-stage-to-only-' . $no, [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Tool'],
    ]) ?>

    <?php $no++; ?>
<?php } ?>
