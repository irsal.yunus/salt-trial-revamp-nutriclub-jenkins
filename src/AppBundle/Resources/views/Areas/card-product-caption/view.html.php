<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:31
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="content-porduct_section  bg-acean">
    <div class="container content-porduct_sort_desc">
        <div class="text-center">
            <h2><?= $this->input('title')->frontend() ?></h2>
            <?= $this->wysiwyg('description')->frontend() ?>
        </div>
    </div>
</div>
