<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:17
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Banner Slider</h4>
<?php $no = 1;?>
<?php while($this->block('banner-content-block')->loop()) { ?>

    <h4>Type <?= $no ?> </h4>
    <?= $this->select('banner-type-' . $no, [
        'store' => [
            ['default', 'default'],
            ['video', 'video'],
        ],
        'reload' => true
    ]) ?>

    <!-- default -->
    <?php if (
        !$this->select('banner-type-' . $no)->isEmpty() &&
        $this->select('banner-type-' . $no)->getData() === 'default') {

    ?>
        <h4>Title <?= $no ?></h4>
        <?= $this->input('title-default' . $no, [

        ]) ?>
        <h4>Dekstop Banner <?= $no ?></h4>
        <?= $this->image('banner-slider-desktop-' . $no, [
            "title" => "Drag your image here",
            'thumbnail'   => 'banner-slider-desktop-thumbnail',
            'uploadPath'  => '/banner-slider/desktop/',
            'reload'      => false,
            'hidetext'    => true
        ])
        ?>
        <h4>Tablet Banner <?= $no ?></h4>
        <?= $this->image('banner-slider-tablet-' . $no, [
            "title" => "Drag your image here",
            'thumbnail'   => 'banner-slider-tablet-thumbnail',
            'uploadPath'  => '/banner-slider/tablet/',
            'reload'      => false,
            'hidetext'    => true
        ])
        ?>
        <h4>Mobile Banner <?= $no ?></h4>
        <?= $this->image('banner-slider-mobile-' . $no, [
            "title" => "Drag your image here",
            'thumbnail'   => 'banner-slider-mobile-thumbnail',
            'uploadPath'  => '/banner-slider/mobile/',
            'reload'      => false,
            'hidetext'    => true
        ])
        ?>
        <h4>Link Banner <?= $no ?></h4>
        <?= $this->link('banner-slider-link-image-' . $no, [

        ])?>

        <h4>Link Text <?= $no ?></h4>
        <?= $this->input('banner-slider-link-text-' . $no, []) ?>
    <?php
    }
    ?>

    <!-- video -->
    <?php if (
        !$this->select('banner-type-' . $no)->isEmpty() &&
        $this->select('banner-type-' . $no)->getData() === 'video') {
    ?>
        <h4>Title <?= $no ?></h4>
        <?= $this->input('title-video-' . $no, []) ?>
        <h4>Description <?= $no ?> </h4>
        <?= $this->input('description-video-' . $no, []) ?>
        <h4>Video <?= $no ?></h4>
        <?= $this->video('video-' . $no, []) ?>
    <?php
    }
    ?>

    <?php $no++; ?>
<?php } ?>

