<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 10/01/2020
 * Time: 14:17
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<div class="banner-slider">
    <?php $no = 1;?>
    <?php while($this->block('banner-content-block')->loop()) { ?>

        <?php if (
            !$this->select('banner-type-' . $no)->isEmpty() &&
            $this->select('banner-type-' . $no)->getData() === 'default') {

            if (
                !$this->input('title-default' . $no)->isEmpty() &&
                !$this->image('banner-slider-desktop-' . $no)->isEmpty() &&
                !$this->image('banner-slider-mobile-' . $no)->isEmpty() &&
                !$this->link('banner-slider-link-image-' . $no)->isEmpty() &&
                !$this->input('banner-slider-link-text-' . $no)->isEmpty()) {

            $title = !$this->input('title-default' . $no)->isEmpty() ?
                $this->input('title-default' . $no)->getData() : '';

            $titleHtmlTag = '<h2>' . $title .  '</h2>';

            $subTitleTag = '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, voluptas?</p>';

            $linkText =  '<span class="btn-now-more btn btn-blue">'. $this->input('banner-slider-link-text-' . $no)->getData() . '</span>';

            $contentTag = '<div class="container"><div class="row"><div class="col-12 col-md-5 offset-md-2">' . $titleHtmlTag . $subTitleTag. $linkText . '</div></div></div>';

            $imgDesktop = $this->image('banner-slider-desktop-' . $no, [
            ])->getThumbnail('default')->getHtml([
                'class' => 'banner-slider__image is-background d-none d-sm-block'
            ]);

            $imgMobile = $this->image('banner-slider-mobile-' . $no, [
            ])->getThumbnail('default')->getHtml([
                'class' => 'banner-slider__image is-background d-block d-sm-none'
            ]);

            $link = $this->link('banner-slider-link-image-' . $no, [
                'textPrefix' => $imgDesktop . $imgMobile . $contentTag
            ]);

            ?>
            <div class="banner-slider__item">
                <?= $link ?>
            </div>
            <?php
            }
        }
        ?>

        <?php if (
            !$this->select('banner-type-' . $no)->isEmpty() &&
            $this->select('banner-type-' . $no)->getData() === 'video') {

            if (
                !$this->input('title-video-' . $no)->isEmpty() &&
                !$this->input('description-video-' . $no)->isEmpty() &&
                !$this->video('video-' . $no)->isEmpty()
            ) {

                $titleVideo = !$this->input('title-video-' . $no)->isEmpty() ?
                    $this->input('title-video-' . $no)->getData() : '';

                $description = !$this->input('description-video-' . $no)->isEmpty() ?
                    $this->input('description-video-' . $no)->getData() : '';

                $video = $this->video('video-' . $no);

                $youtubeVideoId = $video->id;

                $youtubeThumbnail = $youtubeVideoId ? 'http://img.youtube.com/vi/' . $youtubeVideoId . '/0.jpg'
                    :  '/assets/images/video-thumbnail.png';

                ?>
                <div class="banner-slider__item is-video">
                    <a href="#" data-video="<?= $youtubeVideoId ?>" class="banner-slider__trigger">
                        <div class="container">
                            <div class="row">
                                <div class="col-4 offset-1">
                        <span class="banner-slider__video">
                            <img src="<?= $youtubeThumbnail ?>" alt="" class="banner-slider__image"/>
                            <img src="/assets/images/icon-play.svg" alt="" class="icon-play"/>
                        </span>
                                </div>
                                <div class="col-6">
                        <span class="banner-slider__copy">
                            <h2><?= $titleVideo ?></h2>
                            <?php if($description != null) { ?>
                                <span><?= $description ?></span>
                            <?php } ?>
                        </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php
            }
        }
        ?>
        <?php $no++; ?>
    <?php } ?>
</div>

<!-- POPUP VIDEO -->
<!-- Modal -->
<div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <iframe id="modal-video__iframe" width="100%" height="500" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>
