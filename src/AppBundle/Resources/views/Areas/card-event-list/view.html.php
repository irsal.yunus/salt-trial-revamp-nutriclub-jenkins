<?php

use Pimcore\Model\DataObject\Event;
use Pimcore\Templating\GlobalVariables;
use Pimcore\Templating\PhpEngine;
use Zend\Paginator\Paginator;
use Ramsey\Uuid\Uuid;

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/06/2020
 * Time: 11:30
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

$limit = 6;

$eventsListing = new Event\Listing();
$eventsListing->setOrderKey('startDate');
$eventsListing->setOrder('asc');
$eventsListing->setLimit($limit);
$eventsListing->load();

$paginator = new Paginator($eventsListing);
$paginator->setCurrentPageNumber(0);
$paginator->setItemCountPerPage($limit);

$events = $eventsListing->getObjects();

$formId = 'card__event-list-load-more-' . Uuid::uuid4();

?>

<header class="heading-event">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <h1 class="heading-event__title"><?= $this->input('card-event-list-title') ?></h1>
                <p class="heading-event__text">
                    <?= $this->input('card-event-list-short-desc') ?>
                </p>
            </div>
        </div>
    </div>
</header>

<section class="card-event-wrapper py-4">
    <div class="container position-relative">
        <form id="<?= $formId ?>">
            <input type="hidden" id="nextPage" name="nextPage" value="2" form-id="<?= $formId ?>">
            <input type="hidden" id="currentPageLimit" name="limit" value="<?= $limit ?>">
        </form>
        <div class="event-preloader">
            <?= $this->render('Include/preloader.html.php') ?>
        </div>
        <div class="event__lists row">
            <?php
            echo $this->render('Widgets/CardEventList/list.html.php', [
                'data' => $events
            ])
            ?>

        </div>

        <?= $this->render('Include/paging.html.php', get_object_vars($paginator->getPages())) ?>
    </div>
</section>
