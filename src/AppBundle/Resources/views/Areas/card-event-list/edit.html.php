<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/06/2020
 * Time: 11:31
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title :</h4>
<?= $this->input('card-event-list-title', []) ?>

<h4>Short Description :</h4>
<?= $this->input('card-event-list-short-desc', []) ?>
