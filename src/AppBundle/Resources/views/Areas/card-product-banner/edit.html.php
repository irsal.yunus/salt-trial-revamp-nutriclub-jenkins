<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:29
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */

?>

<h3>Card Product Banner</h3>

<h4>Banner Caption</h4>
<?= $this->input('banner-caption', [
    'htmlspecialchars'      => false,
    'placeholder'           => 'input your text here'
])?>

<h4>Banner Caption Image</h4>
<?= $this->image('banner-caption-image', [
    'thumbnail'     => 'banner-caption-image-thumbnail',
]) ?>

<h4>Banner Image Desktop</h4>
<?= $this->image('card-product-banner-image', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'card-product-banner-thumbnail',
    'uploadPath'    => '/card-product-banner/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h4>Banner Image Mobile</h4>
<?= $this->image('card-product-banner-image-mobile', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'card-product-banner-thumbnail-mobile',
    'uploadPath'    => '/card-product-banner/',
    'reload'        => false,
    'hidetext'      => true
]) ?>
