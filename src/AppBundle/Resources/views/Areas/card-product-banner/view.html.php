<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:29
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="content-porduct_header">
    <div class="content-porduct_header_bg">
        <div class="container-child">
            <h1 class="center">
                <?= !$this->image('banner-caption-image')->isEmpty() ?
                    $this->image('banner-caption-image')
                        ->getThumbnail('banner-caption-image-thumbnail')
                        ->getHtml([
                            'disableWidthHeightAttributes' => true,
                            'class' => 'img-fluid d-block d-sm-none'
                        ]) : null
                ?>
                <div><?= $this->input('banner-caption') ?></div>
            </h1>
        </div>
        <div class="bg-header-detail">
            <?= !$this->image('card-product-banner-image')->isEmpty() ?
                $this->image('card-product-banner-image')
                    ->getThumbnail('card-product-banner-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'img-fluid  d-none d-sm-block'
                    ]) : null
            ?>

            <?= !$this->image('card-product-banner-image-mobile')->isEmpty() ?
                $this->image('card-product-banner-image-mobile')
                    ->getThumbnail('card-product-banner-thumbnail-mobile')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'img-fluid  d-block d-sm-none'
                    ]) : null
            ?>
        </div>
    </div>
</div>
