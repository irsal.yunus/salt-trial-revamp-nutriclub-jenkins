<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:31
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>

<div class="content-porduct_header">
    <div class="content-porduct_header_bg">
        <div class="container-child">
            <h1 class="<?= !$this->select('caption-position')->isEmpty() ?
                $this->select('caption-position')->frontend() : '' ?>">
                <?= $this->input('card-product-group-banner-caption')->frontend() ?>
            </h1>
        </div>
        <div class="bg-header-detail">
            <?= !$this->image('card-product-group-banner-image')->isEmpty() ?
                $this->image('card-product-group-banner-image')
                    ->getThumbnail('card-product-group-banner-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'img-fluid d-none d-sm-block'
                    ]) : null
            ?>
            <?= !$this->image('card-product-group-banner-mobile-image')->isEmpty() ?
                $this->image('card-product-group-banner-mobile-image')
                    ->getThumbnail('card-product-group-banner-mobile-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'img-fluid d-block d-sm-none'
                    ]) : null
            ?>
        </div>
    </div>
</div>
