<?php

use Pimcore\Templating\{GlobalVariables, PhpEngine};

/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 12/07/2020
 * Time: 12:32
 */
/**
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 */
?>
<h3>Card Product Group Banner</h3>

<h4>Banner Caption</h4>
<?= $this->input('card-product-group-banner-caption', [
    'htmlspecialchars'      => false,
    'placeholder'           => 'Input your text here'
]) ?>

<h4>Banner Caption Position</h4>
<?= $this->select('caption-position', [
    'store' => [
        ['', 'right'],
        ['left', 'left'],
    ],
    'defaultValue' => 'left'
]) ?>

<h4>Banner Image Desktop</h4>
<?= $this->image('card-product-group-banner-image', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'card-product-group-banner-thumbnail',
    'uploadPath'    => '/card-product-group-banner/',
    'reload'        => false,
    'hidetext'      => true
]) ?>

<h4>Banner Image Mobile</h4>
<?= $this->image('card-product-group-banner-mobile-image', [
    'title'         => 'Drag your image here',
    'thumbnail'     => 'card-product-group-banner-mobile-thumbnail',
    'uploadPath'    => '/card-product-group-banner/',
    'reload'        => false,
    'hidetext'      => true
]) ?>
