<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 14:14
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<?php if (
     !$this->image('card-with-banner-medium-desktop')->isEmpty() &&
     !$this->image('card-with-banner-medium-mobile')->isEmpty()
) { ?>
<div class="card-banner-medium mt-5">
    <div class="container">
        <div class="card-banner-medium__wrapper">
            <?= $this->image('card-with-banner-medium-desktop', [
            ])->getThumbnail('card-with-banner-medium-desktop')->getHtml([
                'class' => 'image-wrapper d-none d-md-block'
            ]) ?>

            <?= $this->image('card-with-banner-medium-mobile', [
            ])->getThumbnail('card-with-banner-medium-mobile')->getHtml([
                'class' => 'image-wrapper d-block d-sm-none'
            ]) ?>

            <div class="content-area color-blue text-center">
                <img src="/assets/images/logo-mynutriclub.png" alt="">
                <h4><?= $this->input('card-with-banner-medium-title')->getData() ?></h4>
                <h6><?= $this->input('card-with-banner-medium-sub-title')->getData() ?></h6>
                <?= $this->wysiwyg('card-with-banner-medium-description')->frontend() ?>
                <div class="button-register-now">
                    <?= $this->link('card-with-banner-medium-link')->frontend() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
