<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 14:14
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title : </h4>
<?= $this->input('card-with-banner-medium-title', []) ?>

<h4>Sub Title :</h4>
<?= $this->input('card-with-banner-medium-sub-title', []) ?>

<h4>Description :</h4>
<?= $this->wysiwyg('card-with-banner-medium-description', []) ?>

<h4>Banner Desktop : </h4>
<?= $this->image('card-with-banner-medium-desktop', [
    'title' => 'Card with banner medium desktop',
]) ?>

<h4>Banner Mobile : </h4>
<?= $this->image('card-with-banner-medium-mobile', [
    'title' => 'Card with banner medium mobile'
]) ?>

<h4>Link : </h4>
<?= $this->link('card-with-banner-medium-link', [
]) ?>
