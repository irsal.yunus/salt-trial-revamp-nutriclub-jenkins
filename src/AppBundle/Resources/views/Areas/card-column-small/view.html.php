<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 12:55
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$latestTools = [];
?>
<?php if (count($latestTools) > 0 && !$this->editmode) { ?>
<div class="card-column-small">
    <h2 class="heading-blue">
        <?= !$this->input('card-column-small-title')->isEmpty() ?
            $this->input('card-column-small-title')->getData() : null ?>
    </h2>
    <div class="card-column-small__wrapper">
        <?php
            foreach ($latestTools as $latestTool) {
        ?>
                <div class="card-column-small__content">
                        <img src="/assets/images/icons/icon-resiliensi.png">
                        <div class="text-area">
                            <h5>Tes Resiliensi</h5>
                            <p>last test: 12 Jan 2019</p>
                        </div>
                        <div class="btn-see">
                            <a href="">Lihat</a>
                        </div>
                    </div>
        <?php
            }
        ?>
    </div>
</div>
<?php } ?>
