<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/01/2020
 * Time: 17:44
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

/** @var BreadcrumbHelperService $breadcrumbHelperService */
$breadcrumbHelperService = $this->breadcrumbHelperService;

// get root node if there is no document defined (for pages which are routed directly through static route)
$document = $this->document;
if(!$document instanceof Page) {
    $document = Page::getById(1);
}

// get the document which should be used to start in navigation | default home
$mainNavStartNode = $this->document->getProperty('navigationRoot');
if(!$mainNavStartNode instanceof Page) {
    $mainNavStartNode = Page::getById(1);
}

// this returns us the navigation container we can use to render the navigation
try {
    $mainNavigation = $this->navigation()->build(['active' => $document, 'root' => $mainNavStartNode]);

    if ($document->getId() !== 1) {
        $homepage = Document::getById(1);
        $mainNavigation->addPage([
            'order' => -1, // put it in front of all the others
            'uri' => '/', //path to homepage
            'label' => $homepage->getProperty('navigation_name'), //visible label
            'title' => $homepage->getProperty('navigation_title'), //tooltip text,
            'active' => $this->document->getId() === $homepage->getId()
        ]);
    }
} catch (Exception $e) {
    \Pimcore\Log\Simple::log('BREADCRUMBS_ERROR', $e->getMessage());
}
?>
<div class="breadcrumbs mt-3">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/" class="active" data-name="Home" event-name="CLICK_BREADCRUMBS">
                    <?= $document->getId() === 1 ? $document->getProperty('navigation_name') :
                        $homepage->getProperty('navigation_name') ?>
                </a>
            </li>
            <?php try {
                $navBread = $this->navigation()
                    ->breadcrumbs()
                    ->setPartial('Include/breadcrumbs.partial.html.php')
                    ->setMinDepth(null)
                    ->render($mainNavigation);

                echo $navBread;
            } catch (Exception $e) {

            }
            ?>
        </ol>
    </div>
</div>
