(function() {
    'use strict';

    pimcore.settings.targeting.actions.register(
        'add_label_to_widget_card_slider_one',
        Class.create(pimcore.settings.targeting.action.abstract, {
            getName: function () {
                return 'Add Label To Widget Card Slider One';
            },

            getPanel: function (panel, data) {
                var id = Ext.id();

                return new Ext.form.FormPanel({
                    id: id,
                    forceLayout: true,
                    style: 'margin: 10px 0 0 0',
                    bodyStyle: 'padding: 10px 30px 10px 30px; min-height:40px;',
                    tbar: pimcore.settings.targeting.actions.getTopBar(this, id, panel),
                    items: [
                        {
                            name: 'labelTitle',
                            fieldLabel: 'Label Title',
                            xtype: 'textareafield',
                            width: 500,
                            value: ('undefined' !== typeof data.labelTitle) ? data.labelTitle : null,
                        },
                        {
                            name: 'stageSlug',
                            fieldLabel: 'Stage Slug',
                            xtype: 'textareafield',
                            width: 500,
                            value: ('undefined' !== typeof data.stageSlug) ? data.stageSlug : null,
                        },
                        {
                            xtype: 'hidden',
                            name: 'type',
                            value: 'add_label_to_widget_card_slider_one'
                        }
                    ]
                });
            }
        })
    );

    pimcore.settings.targeting.actions.register(
        'add_content_to_widget_card_column_one',
        Class.create(pimcore.settings.targeting.action.abstract, {
            getName: function () {
                return 'Add Content To Widget Card Column One';
            },

            getPanel: function (panel, data) {
                var id = Ext.id();

                return new Ext.form.FormPanel({
                    id: id,
                    forceLayout: true,
                    style: 'margin: 10px 0 0 0',
                    bodyStyle: 'padding: 10px 30px 10px 30px; min-height:40px;',
                    tbar: pimcore.settings.targeting.actions.getTopBar(this, id, panel),
                    items: [
                        {
                            name: 'assignArticleCalendar',
                            fieldLabel: 'Assign Article Calendar By User Reward Session Age Pregnant or Child',
                            labelWidth: 500,
                            xtype: 'checkbox',
                            width: 500,
                            value: ('undefined' !== typeof data.assignArticleCalendar) ? data.assignArticleCalendar : null,
                        },
                        {
                            xtype: 'hidden',
                            name: 'type',
                            value: 'add_content_to_widget_card_column_one'
                        }
                    ]
                });
            }
        })
    );

    pimcore.settings.targeting.actions.register(
        'force_assign_target_group',
        Class.create(pimcore.settings.targeting.action.abstract, {
            getName: function () {
                return 'Force Assign Target Group';
            },

            getPanel: function (panel, data) {
                var id = Ext.id();

                return new Ext.form.FormPanel({
                    id: id,
                    forceLayout: true,
                    style: 'margin: 10px 0 0 0',
                    bodyStyle: 'padding: 10px 30px 10px 30px; min-height:40px;',
                    tbar: pimcore.settings.targeting.actions.getTopBar(this, id, panel),
                    items: [
                        {
                            xtype: "combo",
                            fieldLabel: t('target_group'),
                            name: "targetGroup",
                            displayField: 'text',
                            valueField: "id",
                            store: pimcore.globalmanager.get("target_group_store"),
                            editable: false,
                            width: 400,
                            triggerAction: 'all',
                            listWidth: 200,
                            mode: "local",
                            value: data.targetGroup,
                            emptyText: t("select_a_target_group")
                        },
                        {
                            xtype: 'hidden',
                            name: 'type',
                            value: 'force_assign_target_group'
                        }
                    ]
                });
            }
        })
    );

    pimcore.settings.targeting.conditions.register(
        'visitor_has_user_reward_session',
        Class.create(pimcore.settings.targeting.condition.abstract, {
            getName: function () {
                return 'Visitor Has User Reward Session';
            },

            getPanel: function (panel, data) {
                var id = Ext.id();

                this.formVisitorHasUserRewardSession = new Ext.form.FormPanel({
                    id: id,
                    forceLayout: true,
                    style: 'margin: 10px 0 0 0',
                    bodyStyle: 'padding: 10px 30px 10px 30px; min-height:40px;',
                    tbar: pimcore.settings.targeting.conditions.getTopBar(this, id, panel, data),
                    items: [
                        {
                            name: 'userRewardSessionModel',
                            fieldLabel: 'User Reward Session Model',
                            xtype: 'textfield',
                            width: 500,
                            value: ('undefined' !== typeof data.userRewardSessionModel) ? data.userRewardSessionModel : null,
                            listeners: {
                                afterrender: function() {
                                    console.log(data);
                                }
                            }
                        },
                        {
                            xtype: 'hidden',
                            name: 'type',
                            value: 'visitor_has_user_reward_session' // the identifier chosen before when registering the PHP class
                        }
                    ]
                });

                return this.formVisitorHasUserRewardSession;
            },

        })
    );

    pimcore.settings.targeting.conditions.register(
        'user_reward_session_has_stage',
        Class.create(pimcore.settings.targeting.condition.abstract, {
            getName: function () {
                return 'User Reward Session Has Stage';
            },

            getPanel: function (panel, data) {
                var id = Ext.id();

                this.formUserRewardSessionHasStage = new Ext.form.FormPanel({
                    id: id,
                    forceLayout: true,
                    style: 'margin: 10px 0 0 0',
                    bodyStyle: 'padding: 10px 30px 10px 30px; min-height:40px;',
                    tbar: pimcore.settings.targeting.conditions.getTopBar(this, id, panel, data),
                    items: [
                        {
                            xtype: 'combo',
                            fieldLabel: 'Stage',
                            displayField: 'Name',
                            valueField: 'ID',
                            name: "stage",
                            store: new Ext.data.JsonStore({
                                autoDestroy: true,
                                proxy: {
                                    type: 'ajax',
                                    url: "/rewards/stages",
                                    reader: {
                                        type: 'json',
                                        rootProperty: 'Value'
                                    }
                                },
                                fields: ["ID", "Name"]
                            }),
                            triggerAction: "all",
                            mode: "local",
                            forceSelection: true,
                            queryMode: 'local',
                            autoComplete: false,
                            width: 350,
                            value: ('undefined' !== typeof data.stage) ? data.stage : null,
                            listeners: {
                                afterrender: function (el) {
                                    el.getStore().load();
                                },
                                change: function (field, value) {

                                }.bind(this)
                            }
                        },
                        {
                            xtype: 'hidden',
                            name: 'type',
                            value: 'user_reward_session_has_stage' // the identifier chosen before when registering the PHP class
                        }
                    ]
                });

                return this.formUserRewardSessionHasStage;
            },

        })
    );

    pimcore.settings.targeting.conditions.register(
        'target_group_current',
        Class.create(pimcore.settings.targeting.condition.abstract, {
            getName: function () {
                return 'Target Group Current';
            },

            getPanel: function (panel, data) {
                var id = Ext.id();

                this.formTargetGroupCurrent = new Ext.form.FormPanel({
                    id: id,
                    forceLayout: true,
                    style: 'margin: 10px 0 0 0',
                    bodyStyle: 'padding: 10px 30px 10px 30px; min-height:40px;',
                    tbar: pimcore.settings.targeting.conditions.getTopBar(this, id, panel, data),
                    items: [
                        {
                            xtype: "combo",
                            name: "targetGroup",
                            displayField: 'text',
                            valueField: "id",
                            store: pimcore.globalmanager.get("target_group_store"),
                            width: 400,
                            triggerAction: 'all',
                            listWidth: 200,
                            mode: "local",
                            forceSelection: true,
                            queryMode: 'local',
                            autoComplete: false,
                            value: ('undefined' !== typeof data.targetGroup) ? data.targetGroup : null,
                            emptyText: t("select_a_target_group"),
                            fieldLabel: "Using Current Target Group"
                        },
                        {
                            xtype: 'hidden',
                            name: 'type',
                            value: 'target_group_current' // the identifier chosen before when registering the PHP class
                        }
                    ]
                });

                return this.formTargetGroupCurrent;
            },

        })
    );

    pimcore.settings.targeting.conditions.register(
        'visit_same_pages_for',
        Class.create(pimcore.settings.targeting.condition.abstract, {
            getName: function () {
                return 'Visit Same Pages For';
            },

            getPanel: function (panel, data) {
                var id = Ext.id();

                this.formVisitSamePagesFor = new Ext.form.FormPanel({
                    id: id,
                    forceLayout: true,
                    style: 'margin: 10px 0 0 0',
                    bodyStyle: 'padding: 10px 30px 10px 30px; min-height:40px;',
                    tbar: pimcore.settings.targeting.conditions.getTopBar(this, id, panel, data),
                    items: [
                        {
                            xtype: 'hidden',
                            itemId: "uniqueid",
                            name: "uniqueid",
                            value: data.uniqueid,
                            listeners: {
                                afterrender: function () {
                                    let currentValue = this.formVisitSamePagesFor.getComponent("uniqueid").getValue();
                                    let currentTimestamp = new Date().getTime();
                                    if (!currentValue) {
                                        this.formVisitSamePagesFor.getComponent("uniqueid").setValue(currentTimestamp);
                                    }
                                }.bind(this)
                            }
                        },
                        {
                            xtype: 'numberfield',
                            fieldLabel: t('number_minimal_visit'),
                            labelWidth: 100,
                            itemId: "number",
                            name: "number",
                            value: data.number,
                            width: 200,
                        },
                        {
                            xtype: 'hidden',
                            name: 'type',
                            value: 'visit_same_pages_for' // the identifier chosen before when registering the PHP class
                        }
                    ]
                });

                return this.formVisitSamePagesFor;
            },

        })
    );

    pimcore.settings.targeting.conditions.register(
        'url_with_data_provider',
        Class.create(pimcore.settings.targeting.condition.abstract, {
            getName: function () {
                return t('targeting_condition_url_pattern') + " With Data Provider";
            },

            getPanel: function (panel, data) {
                var id = Ext.id();

                this.formTargetGroupCurrent = new Ext.form.FormPanel({
                    id: id,
                    forceLayout: true,
                    style: 'margin: 10px 0 0 0',
                    bodyStyle: 'padding: 10px 30px 10px 30px; min-height:40px;',
                    tbar: pimcore.settings.targeting.conditions.getTopBar(this, id, panel, data),
                    items: [
                        {
                            xtype: "textfield",
                            fieldLabel: t('targeting_condition_url_pattern'),
                            name: "url",
                            value: data.url,
                            width: 500
                        },
                        {
                            xtype: "hidden",
                            name: "type",
                            value: "url_with_data_provider"
                        }
                    ]
                });

                return this.formTargetGroupCurrent;
            },

        })
    );

}());