<?php

namespace AppBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/app/js/pimcore/targeting/conditions.js'
        ];
    }

}
