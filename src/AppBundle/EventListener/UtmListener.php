<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 25/05/2020
 * Time: 23:11
 */

namespace AppBundle\EventListener;

use Carbon\Carbon;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UtmListener implements EventSubscriberInterface
{
    public const UTMS = [
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_term',
        'utm_content'
    ];

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                'onKernelRequest'
            ]
        ];
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $request = $requestEvent->getRequest();

        $queryStrings = $request->query->all();

        if (!$queryStrings) {
            return;
        }

        $dotNetDomain = env('WILDCARD_DOMAIN_FOR_DOT_NET', null);
        $expiredCookie = Carbon::now()->addHours(24)->timestamp;

        foreach ($queryStrings as $key => $queryString) {
            $keyLowered = strtolower($key);

            if (!in_array($keyLowered, self::UTMS, false)) {
                continue;
            }

            setrawcookie(
                $keyLowered,
                $queryString,
                $expiredCookie,
                '/',
                $dotNetDomain
            );
        }
    }
}
