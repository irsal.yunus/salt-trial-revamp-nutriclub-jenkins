<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 22/02/2020
 * Time: 19:37
 */

namespace AppBundle\EventListener;

use Pimcore\Document\Renderer\DocumentRenderer;
use Pimcore\Model\{Document, WebsiteSetting};
use Pimcore\Templating\Renderer\ActionRenderer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    /**
     * @var ActionRenderer
     */
    protected $documentRenderer;

    public function __construct(DocumentRenderer $documentRenderer)
    {
        $this->documentRenderer = $documentRenderer;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getThrowable();

        // check if request has made by pimcore front end if so, return do not throw error page
        if (
            ($event->getRequest()->attributes->has('_pimcore_context') &&
                $event->getRequest()->attributes->get('_pimcore_context') === 'admin')
        ) {
            return;
        }

        if (env('PIMCORE_ENVIRONMENT') === 'dev') {
            return;
        }

        $statusCode = 500;
        $headers = [];
        if ($exception instanceof HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
            $headers = $exception->getHeaders();

            $prefixPage = 'ERROR_PAGE_' . $statusCode;
            $document = WebsiteSetting::getByName($prefixPage)
                ? WebsiteSetting::getByName($prefixPage)->getData() : null;

            if (!$document instanceof Document\Page) {
                // default is home
                $document = Document::getById(1);
            }

            // Customize your response object to display the exception details
            $response = $this->documentRenderer->render($document, [
                'exception' => $exception
            ]);

            // sends the modified response object to the event
            $event->setResponse(new Response($response, $statusCode, $headers));
        }

        if (!$exception instanceof HttpExceptionInterface) {
            $prefixPage = 'ERROR_PAGE_' . $statusCode;
            $document = WebsiteSetting::getByName($prefixPage)
                ? WebsiteSetting::getByName($prefixPage)->getData() : null;

            if (!$document instanceof Document\Page) {
                // default is home
                $document = Document::getById(1);
            }

            // Customize your response object to display the exception details
            $response = $this->documentRenderer->render($document, [
                'exception' => $exception
            ]);

            // sends the modified response object to the event
            $event->setResponse(new Response($response, $statusCode, $headers));
        }
    }
}
