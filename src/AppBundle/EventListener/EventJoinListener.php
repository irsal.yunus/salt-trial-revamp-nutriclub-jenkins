<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 14/06/2020
 * Time: 18:54
 */

namespace AppBundle\EventListener;

use AppBundle\Model\DataObject\Event;
use AppBundle\Services\EventJoinService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class EventJoinListener implements EventSubscriberInterface
{
    /** @var EventJoinService $eventJoinService */
    private $eventJoinService;

    public function __construct(EventJoinService $eventJoinService)
    {
        $this->eventJoinService = $eventJoinService;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                'onKernelRequest'
            ]
        ];
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        if (!$requestEvent->isMasterRequest()) {
            return;
        }

        $request = $requestEvent->getRequest();


        $currentCounter = $this->eventJoinService->getChangePageCounter();
        if ($currentCounter > 0) {
            $this->eventJoinService->setChangePageCounter(++$currentCounter);
            if ($this->eventJoinService->getChangePageCounter() > 10) {
                $this->eventJoinService->clearSession();
            }
        }

        if (
            $request->getMethod() === 'GET' &&
            (
                $request->get('_route') === 'account-login' ||
                $request->get('_route') === 'account-register'
            )
        ) {
            $referer = $request->headers->get('referer', null);
            $regex = '/\/event\/([^\/?]*)(?=[^\/]*$)/';

            preg_match_all($regex, $referer, $matches, PREG_SET_ORDER, 0);

            if (!$matches) {
                return;
            }

            if (!$matches[0] && (count($matches[0]) < 2)) {
                return;
            }
            $slug = $matches[0][1];

            /** @var Event $checkObjectEvent */
            $checkObjectEvent = Event::getBySlug($slug, 1);

            if (!$checkObjectEvent) {
                return;
            }

            $this->eventJoinService->setChangePageCounter(1);
            $this->eventJoinService->setEventObj($checkObjectEvent);
        }
    }
}
