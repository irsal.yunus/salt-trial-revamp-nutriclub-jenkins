<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource FreeSampleListener.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 07/08/20
 * @time 01.39
 *
 */

namespace AppBundle\EventListener;

use AppBundle\Model\DataObject\Product;
use Carbon\Carbon;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class FreeSampleListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                'onKernelRequest'
            ]
        ];
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $request = $requestEvent->getRequest();

        if (
            $request->getMethod() === 'GET' &&
            (
                $request->get('_route') === 'account-login' ||
                $request->get('_route') === 'account-register'
            )
        ) {
            $referer = $request->headers->get('referer', null);
            $regex = '/\/product\/([^\/?]*)\/([^\/?]*)\/(free-sample)?\/?(?=[^\/]*$)/';

            preg_match_all($regex, $referer, $matches, PREG_SET_ORDER, 0);

            if (!$matches) {
                return;
            }

            if (!$matches[0] && (count($matches[0]) < 2)) {
                return;
            }
            $slug = $matches[0][2];

            /** @var Product $checkProduct */
            $checkProduct = Product::getBySlug($slug, 1);

            if (!$checkProduct) {
                return;
            }

            $this->cookieCounter('refererFreeSample', $referer);
        }
    }

    private function cookieCounter($cookieName, $value, $expired = false)
    {
        $dotNetDomain = env('WILDCARD_DOMAIN_FOR_DOT_NET', null);

        $expiredCookie = $expired ? 1 : Carbon::now()->addMinutes(5)->timestamp;

        setrawcookie(
            $cookieName,
            $value,
            $expiredCookie,
            '/',
            $dotNetDomain
        );
    }
}
