<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/07/2020
 * Time: 16:33
 */

namespace AppBundle\EventListener;

use Pimcore\Templating\Helper\HeadScript;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class MoEngageListener implements EventSubscriberInterface
{
    public const ON_REGISTER_SUCCESS = 'onRegisterSuccess';

    public const ON_LOGOUT_SUCCESS = 'onLogoutSuccess';

    public const ON_LOGIN_SUCCESS = 'onLoginSuccess';

    public const ON_UPDATE_SUCCESS = 'onUpdateSuccess';

    private $session;

    private $headScript;

    public function __construct(SessionInterface $session, HeadScript $headScript)
    {
        $this->session = $session;
        $this->headScript = $headScript;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                'onKernelRequest'
            ]
        ];
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $flashBag = $this->session->getFlashBag();

        if (in_array(true, $flashBag->get(self::ON_REGISTER_SUCCESS, []), false)) {
            $this->headScript->offsetSetFile(2000002, '/assets/js/tracking/register.moengage.js');
        }

        // tracking user logout into MoEngage.
        if (in_array(true, $flashBag->get(self::ON_LOGOUT_SUCCESS, []), false)) {
            $this->headScript->offsetSetFile(2000003, '/assets/js/tracking/logout.moengage.js');
        }

        // tracking user login into MoEngage.
        if (in_array(true, $flashBag->get(self::ON_LOGIN_SUCCESS, []), false)) {
            $this->headScript->offsetSetFile(2000004, '/assets/js/tracking/login.moengage.js');
        }

        // tracking user update profile into MoEngage.
        if (in_array(true, $flashBag->get(self::ON_UPDATE_SUCCESS, []), false)) {
            $this->headScript->offsetSetFile(2000005, '/assets/js/tracking/update.moengage.js');
        }
    }
}
