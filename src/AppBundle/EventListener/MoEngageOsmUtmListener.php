<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource MoEngageOsmUtmListener.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 31/07/20
 * @time 22.59
 *
 */

namespace AppBundle\EventListener;

use AppBundle\Services\MoEngageOsmUtmService;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class MoEngageOsmUtmListener
{
    private $moEngageOsmUtmService;

    public function __construct(MoEngageOsmUtmService $moEngageOsmUtmService)
    {
        $this->moEngageOsmUtmService = $moEngageOsmUtmService;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                'onKernelRequest'
            ]
        ];
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $this->moEngageOsmUtmService->getValidUtmContent();
    }
}
