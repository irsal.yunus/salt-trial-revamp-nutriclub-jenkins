<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 19/05/2020
 * Time: 16:44
 */

namespace AppBundle\EventListener;

use AppBundle\Services\VisitorInfoService;
use Pimcore\Templating\Helper\Placeholder;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CurrentTargetingListener implements EventSubscriberInterface
{
    /** @var VisitorInfoService $visitorInfoService */
    private $visitorInfoService;

    /** @var Placeholder $placeholder */
    private $placeholder;

    public function __construct(VisitorInfoService $visitorInfoService, Placeholder $placeholder)
    {
        $this->visitorInfoService = $visitorInfoService;
        $this->placeholder = $placeholder;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                'onKernelRequest'
            ]
        ];
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $currentTargeting = $this->visitorInfoService->getCurrentTargeting();
        $this->placeholder->__invoke('currentPersona')->set($currentTargeting);
    }
}
