<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 13/01/2020
 * Time: 13:19
 */

namespace AppBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject;
use AppBundle\Model\DataObject\Article;
use Symfony\Component\EventDispatcher\GenericEvent;

class AdminListener
{
    public function onObjectListBeforeListLoad(GenericEvent $genericEvent)
    {
        $allParams = $genericEvent->getArgument('context');

        if (
            isset($allParams['context']) &&
            json_decode($allParams['context'])->scope === 'objectEditor' &&
            (isset($allParams['class']) && $allParams['class'] === 'SubStage')
        ) {
            $objectId = json_decode($allParams['context'])->objectId;
            $dataObject = DataObject::getById($objectId, 1);

            if (!$dataObject instanceof Article) {
                return;
            }

            $stage = $dataObject->getStage()->getId();

            $subStages = new DataObject\SubStage\Listing();
            //$genericEvent->setArgument('list', $subStages);
        }
    }
}
