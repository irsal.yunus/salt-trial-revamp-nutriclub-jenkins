<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 24/03/2020
 * Time: 23:11
 */

namespace AppBundle\EventListener;

use Pimcore\Event\Targeting\TargetingEvent;
use AppBundle\Event\TargetingEvents;
use Pimcore\Event\TargetingEvents as PimcoreTargetingEvents;
use AppBundle\Services\VisitedSamePagesCounter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class VisitedSamePagesCountListener implements EventSubscriberInterface
{
    /**
     * @var VisitedSamePagesCounter
     */
    private $visitedSamePagesCounter;

    /**
     * @var bool
     */
    private $recordPageCount = false;

    public function __construct(VisitedSamePagesCounter $visitedSamePagesCounter)
    {
        $this->visitedSamePagesCounter = $visitedSamePagesCounter;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            TargetingEvents::VISITED_SAME_PAGES_COUNT_MATCH => 'onVisitedSamePagesCountMatch', // triggered from conditions depending on page count
            PimcoreTargetingEvents::POST_RESOLVE => 'onPostResolveVisitorInfo'
        ];
    }

    public function onVisitedSamePagesCountMatch()
    {
        // increment page count after matching proceeded
        $this->recordPageCount = true;
    }

    public function onPostResolveVisitorInfo(TargetingEvent $event)
    {
        // TODO currently the pages count is only recorded if there's a condition depending on
        // the count. This is good for minimizing storage data and writes, but implies that the
        // page count is not recorded if there's no rule with a condition depending on the page
        // count. Alternatively this could be done blindly after resolving the visitor info, but
        // that would trigger a write/increment on every request without actually needing the data.
        if (!$this->recordPageCount) {
            return;
        }

        $this->visitedSamePagesCounter->increment($event->getVisitorInfo());
    }
}
