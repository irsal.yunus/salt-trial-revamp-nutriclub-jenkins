<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 08/06/2020
 * Time: 14:11
 */

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LoginRefererListener implements EventSubscriberInterface
{
    const LOGIN_REFERER_SESSION = 'redirectLoginReferer';

    /** @var SessionInterface $session */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                'onKernelRequest'
            ]
        ];
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $request = $requestEvent->getRequest();

        if (
            $request->getMethod() === 'GET' &&
            $request->attributes->get('_pimcore_context', null) === 'admin'
        ) {
            //dump($this->session->all());
        }

        if ($request->getMethod() === 'GET' &&
            $request->get('_route') === 'account-login'
        ) {
            $referer = $request->headers->has('referer') ?
                $request->headers->get('referer') : '';

            $regex = null;

            if (env('PIMCORE_ENVIRONMENT') === 'prod') {
                $regex = '/https?:\/\/([a-z0-9]+[.])*nutriclub[.]co[.]id/m';
            }

            if (env('PIMCORE_ENVIRONMENT') === 'dev') {
                $regex = '/https?:\/\/([a-z0-9]+[.])*staging[.]salt[.]id/m';
            }

            preg_match_all($regex, $referer, $matches, PREG_SET_ORDER, 0);

            if (!$matches) {
                return;
            }

            $this->session->set(self::LOGIN_REFERER_SESSION, $referer);
        }
    }
}
