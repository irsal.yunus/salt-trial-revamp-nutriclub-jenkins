<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 13/01/2020
 * Time: 11:44
 */

namespace AppBundle\EventListener;

use AppBundle\Traits\Slugable;
use Pimcore\Event\Model\{ElementEventInterface, DataObjectEvent};
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\{AbstractObject, ArticleCategory, Stage};
use AppBundle\Model\DataObject\{Article, ArticlePodcast, ArticleVideo};

class ObjectListener
{
    use Slugable;

    const VERSION_NOTE = 'Automatically assign by ObjectListener.';

    public function onPostAdd(ElementEventInterface $e)
    {
        if ($e instanceof DataObjectEvent) {
            $obj = $e->getObject();

            //$this->isArticle($obj);
        }
    }

    private function isArticle(AbstractObject $abstractObject)
    {
        if (
            !$isArticle = (
                ($abstractObject instanceof Article) ||
                ($abstractObject instanceof ArticlePodcast) ||
                ($abstractObject instanceof ArticleVideo)
            )
        ) {
            return;
        }

        /** @var Article|ArticlePodcast $article */
        $article = $abstractObject;

        try {
            $objKey = $article->getKey();
            $slugify = $this->slugify($objKey);

            $article->setSlug($slugify);
            $article->setKey($slugify);

            $categorySlug = $article->getParent()->getKey();
            /** @var ArticleCategory $category */
            $category = ArticleCategory::getBySlug($categorySlug,1 );

            $stageSlug = $article->getParent()->getParent()->getKey();
            /** @var Stage $stage */
            $stage = Stage::getBySlug($stageSlug, 1);

            $article->setCategory($category);
            $article->setStage($stage);
            $article->save([
                'versionNote' => self::VERSION_NOTE
            ]);
        } catch (\Exception $e) {
            Simple::log('OBJECT_LISTENER_FAILED_SAVE', $e->getMessage());
        }
    }
}
