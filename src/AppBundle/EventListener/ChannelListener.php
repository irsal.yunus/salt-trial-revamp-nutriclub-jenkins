<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 08/06/2020
 * Time: 10:04
 */

namespace AppBundle\EventListener;

use Carbon\Carbon;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ChannelListener implements EventSubscriberInterface
{
    const COOKIE_NAME = 'channel';

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                'onKernelRequest'
            ]
        ];
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $request = $requestEvent->getRequest();
        $query = $request->query;

        $cookieName = self::COOKIE_NAME;

        if (!$query->has($cookieName)) {
            return;
        }

        $cookieValue = $query->get($cookieName) ?? '';

        // @todo need to be filter input value ? only allow alpha num ?

        $dotNetDomain = env('WILDCARD_DOMAIN_FOR_DOT_NET', null);
        $expiredCookie = Carbon::now()->addHours(24)->timestamp;

        setrawcookie(
            $cookieName,
            $cookieValue,
            $expiredCookie,
            '/',
            $dotNetDomain
        );
    }
}
