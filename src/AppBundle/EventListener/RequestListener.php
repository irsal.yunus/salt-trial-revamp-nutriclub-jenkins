<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 17/02/2020
 * Time: 21:50
 */

namespace AppBundle\EventListener;

use AppBundle\Api\v1\Rewards\Account;
use AppBundle\Model\DataObject\Customer;
use AppBundle\Traits\Authenticate;
use Carbon\Carbon;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RequestListener implements EventSubscriberInterface
{
    use Authenticate;

    public static function getSubscribedEvents()
    {
        // set priority into 31, after Symfony\Component\HttpKernel\EventListener\RouterListener::onKernelRequest()
        // so we can get _route, for more detail php bin/console debug:event-dispatcher
        // reference https://symfony.com/doc/current/event_dispatcher.html
        return [
            KernelEvents::REQUEST => [
                'onKernelRequest', 31
            ]
        ];
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $request = $requestEvent->getRequest();
        $session = $request->getSession();

        $expiredDate = $session ? $session->get('ExpiredDate') : null;
        if ($expiredDate && (Carbon::now()->timestamp >= $expiredDate)) {
            $this->cookieChecker($requestEvent);
        }

        if ($request->getMethod() === 'POST' &&
            $request->get('_route') === 'account-login'
        ) {
            $usernameDummy = getenv('USERNAME_VALID_DUMMY', null);
            $passwordDummy = getenv('PASSWORD_VALID_DUMMY', null);

            $request->request->set('_username', $usernameDummy);
            $request->request->set('_password', $passwordDummy);
        }
    }

    /**
     * For autologin when user logged in from .NET side
     *
     * @param RequestEvent $requestEvent
     */
    private function cookieChecker(RequestEvent $requestEvent)
    {
        $request = $requestEvent->getRequest();

        $sessions = $request->getSession();

        $accessToken = $sessions->has('AccessToken');
        $userProfile = $sessions->has('UserProfile');

        if ($accessToken || $userProfile) {
            if (!$this->isCookieValid($requestEvent)) {
                return;
            }
        }

        $cookieName = getenv('COOKIE_NAME_DOT_NET_AUTOLOGIN', null);

        if (!$cookieName) {
            $this->unAuthenticate();
            return;
        }

        $cookies = $request->cookies;

        $hasCookieAutoLogin = $cookies->has($cookieName);

        if (!$hasCookieAutoLogin) {
            $this->unAuthenticate();
            return;
        }

        $cookieAutoLogin = $cookies->get($cookieName);

        $accountRewards = new Account();
        $checkGetProfile = $accountRewards->profile($cookieAutoLogin);

        if ($checkGetProfile['StatusCode'] !== '00') {
            $this->unAuthenticate();
            return;
        }

        $sessions->set('fromDotNET', true);
        $this->authenticate();
    }

    private function isCookieValid(RequestEvent $requestEvent)
    {
        $request = $requestEvent->getRequest();

        $sessions = $request->getSession();

        $accessToken = $sessions->get('AccessToken');

        $accountRewards = new Account();
        $checkGetProfile = $accountRewards->profile($accessToken);

        if ($checkGetProfile['StatusCode'] === '00') {
            $customer = new Customer();
            $customer->setValues($checkGetProfile['Value']);

            $userProfileRawCurrent = $sessions->get('UserProfileRaw', []);
            $mergedUserProfileRaw = array_merge($userProfileRawCurrent, $checkGetProfile['Value']);
            $sessions->set('UserProfileRaw', $mergedUserProfileRaw);
            $sessions->set('UserProfile', $customer);
            return true;
        }

        if (!isset($checkGetProfile['StatusCode']) || $checkGetProfile['StatusCode'] !== '00') {
            $this->unAuthenticate();
            return false;
        }

        return false;
    }
}
