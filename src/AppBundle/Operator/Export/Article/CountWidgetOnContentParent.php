<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 26/02/2020
 * Time: 15:08
 */

namespace AppBundle\Operator\Export\Article;

use AppBundle\Model\DataObject\Article;
use Pimcore\DataObject\GridColumnConfig\Operator\AbstractOperator;
use Pimcore\DataObject\GridColumnConfig\ResultContainer;
use Pimcore\Model\Element\ElementInterface;

class CountWidgetOnContentParent extends AbstractOperator
{
    private $additionalData;

    public function __construct(\stdClass $config, $context = null)
    {
        parent::__construct($config, $context);

        $this->additionalData = $config->additionalData;
    }

    public function getLabeledValue($element)
    {
        /** @var Article $obj */
        $obj = $element;
        $result = new ResultContainer();

        $parentContent = $obj->getContent();

        // find curly braches.
        $findCurlyBraches = preg_match_all('/{{(\w+)}}/', $parentContent, $matches);

        $count = $findCurlyBraches ? count($matches[0]) : 0;

        $result->setValue($count);
        return $result;
    }
}
