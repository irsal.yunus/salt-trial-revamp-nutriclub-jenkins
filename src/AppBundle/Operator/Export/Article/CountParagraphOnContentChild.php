<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/02/2020
 * Time: 12:58
 */

namespace AppBundle\Operator\Export\Article;

use AppBundle\Model\DataObject\Article;
use Pimcore\DataObject\GridColumnConfig\Operator\AbstractOperator;
use Pimcore\DataObject\GridColumnConfig\ResultContainer;
use Pimcore\Model\DataObject\Fieldcollection\Data\ArticleChild;

class CountParagraphOnContentChild extends AbstractOperator
{
    private $additionalData;

    public function __construct(\stdClass $config, $context = null)
    {
        parent::__construct($config, $context);

        $this->additionalData = $config->additionalData;
    }

    public function getLabeledValue($element)
    {
        /** @var Article $obj */
        $obj = $element;
        $result = new ResultContainer();

        $childs = $obj->getChild() ? $obj->getChild()->getItems() : [];
        $count = 0;

        if ($childs) {
            $index = $this->additionalData;

            /** @var ArticleChild $child */
            if ($child = $childs[$index]) {
                $findParagraph = preg_match_all("(<p>.+?</p>)", $child->getContent() ?? null, $matches);
                $matched = $findParagraph ? $matches[0] : [];
                $count = count($matched);
            }
        }

        $result->setValue($count);
        return $result;
    }
}
