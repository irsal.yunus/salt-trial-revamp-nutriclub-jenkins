<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 26/02/2020
 * Time: 15:23
 */

namespace AppBundle\Operator\Export\Article;

use AppBundle\Model\DataObject\Article;
use Pimcore\DataObject\GridColumnConfig\Operator\AbstractOperator;
use Pimcore\DataObject\GridColumnConfig\ResultContainer;
use Pimcore\Model\DataObject\Fieldcollection\Data\ArticleChild;

class CountWidgetOnContentChild extends AbstractOperator
{
    private $additionalData;

    public function __construct(\stdClass $config, $context = null)
    {
        parent::__construct($config, $context);

        $this->additionalData = $config->additionalData;
    }

    public function getLabeledValue($element)
    {
        /** @var Article $obj */
        $obj = $element;
        $result = new ResultContainer();

        $count = 0;
        $contentChild = $obj->getChild();
        if ($contentChild) {
            $contentChildItems = $contentChild->getItems();
            /** @var ArticleChild $contentChildItem */
            $matchedChild = 0;
            $shouldBreak = false;
            foreach ($contentChildItems as $contentChildItem) {
                $childContent = $contentChildItem->getContent();
                $findCurlyBraches = preg_match_all('/{{(\w+)}}/', $childContent, $matches);

                if (!$findCurlyBraches) {
                    continue;
                }
                $matchedChild += count($matches[0]);
                $count = $matchedChild;
                if ($matchedChild >= 2) {
                    $shouldBreak = true;
                }
                if ($shouldBreak) { break; }
            }
        }

        $result->setValue($count);
        return $result;
    }
}
