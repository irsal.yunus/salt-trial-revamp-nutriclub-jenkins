<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 28/02/2020
 * Time: 12:51
 */

namespace AppBundle\Operator\Export\Article;

use AppBundle\Model\DataObject\Article;
use Pimcore\DataObject\GridColumnConfig\Operator\AbstractOperator;
use Pimcore\DataObject\GridColumnConfig\ResultContainer;

class CountContentChild extends AbstractOperator
{
    private $additionalData;

    public function __construct(\stdClass $config, $context = null)
    {
        parent::__construct($config, $context);

        $this->additionalData = $config->additionalData;
    }

    public function getLabeledValue($element)
    {
        /** @var Article $obj */
        $obj = $element;
        $result = new ResultContainer();

        $childs = $obj->getChild() ? $obj->getChild()->getItems() : [];
        $count = count($childs);

        $result->setValue($count);
        return $result;
    }
}
