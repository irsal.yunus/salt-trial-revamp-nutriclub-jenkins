<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource QuestionAnswerOperator.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 19/08/20
 * @time 19.44
 *
 */

namespace AppBundle\Operator\Export\FreeSample;

use Pimcore\DataObject\GridColumnConfig\Operator\AbstractOperator;
use Pimcore\DataObject\GridColumnConfig\ResultContainer;
use Pimcore\Model\DataObject\Fieldcollection\Data\ProductAnswers;
use Pimcore\Model\DataObject\FreeSampleClaim;
use Pimcore\Model\Element\ElementInterface;

class QuestionAnswerOperator extends AbstractOperator
{
    private $additionalData;

    public function __construct(\stdClass $config, $context = null)
    {
        parent::__construct($config, $context);

        $this->additionalData = $config->additionalData;
    }

    public function getLabeledValue($element)
    {
        /** @var FreeSampleClaim $obj */
        $obj = $element;

        $items = $obj->getAnswers()->getItems();

        $data = $this->gatherDataFromFieldCollection($items);

        $result = new ResultContainer();

        $result->setValue($data);

        return $result;
    }

    private function gatherDataFromFieldCollection(array $items)
    {
        $data = null;
        if (!$items) {
            return $data;
        }

        /**
         * @var ProductAnswers $item
         */
        foreach ($items as $key => $item) {
            $data .= 'Question : ' . $key . ' ' . $item->getQuestion();
            $data .= 'Answer : ' . $key . ' ' . $item->getAnswer();
        }

        return $data;
    }
}
