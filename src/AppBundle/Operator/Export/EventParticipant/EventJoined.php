<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource EventJoined.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 07/09/20
 * @time 11.16
 *
 */

namespace AppBundle\Operator\Export\EventParticipant;

use AppBundle\Model\DataObject\Event;
use Pimcore\DataObject\GridColumnConfig\Operator\AbstractOperator;
use Pimcore\DataObject\GridColumnConfig\ResultContainer;
use Pimcore\Model\DataObject\EventParticipant;
use Pimcore\Model\DataObject\FreeSampleClaim;

class EventJoined extends AbstractOperator
{
    private $additionalData;

    public function __construct(\stdClass $config, $context = null)
    {
        parent::__construct($config, $context);

        $this->additionalData = $config->additionalData;
    }

    public function getLabeledValue($element)
    {
        /** @var EventParticipant $obj */
        $obj = $element;

        $fieldDefinition = $obj->getClass()->getFieldDefinition('eventJoined');
        $refKey = $fieldDefinition->getOwnerFieldName();
        $refId = $fieldDefinition->getOwnerClassId();

        $nonOwnerRelations = $obj->getRelationData($refKey, false, $refId);

        $value = null;

        if ($nonOwnerRelations) {
            $totalNonOwnerRelations = count($nonOwnerRelations);
            foreach ($nonOwnerRelations as $key => $nonOwnerRelation) {
                $objId = $nonOwnerRelation['id'];
                $eventObj = Event::getById($objId, 1);

                $value .= $eventObj ? $eventObj->getTitle() : null;
                $value .= ($totalNonOwnerRelations === ($key+1)) ? null : ', ';
            }
        }

        $result = new ResultContainer();

        $result->setValue($value);

        return $result;
    }
}
