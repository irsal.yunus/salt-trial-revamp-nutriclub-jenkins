<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 03/02/2020
 * Time: 14:21
 */

namespace AppBundle\Website\Navigation;

use AppBundle\Model\DataObject\{Article, ArticleEbook, ArticlePodcast, ArticleVideo, Event, Product, ProductGroup};
use CarelineBundle\Model\DataObject\CarelineFaq;
use CarelineBundle\Model\DataObject\CarelineServiceExpert;
use CarelineBundle\Model\DataObject\CarelineServiceType;
use Pimcore\Model\Document;
use Pimcore\Templating\Helper\Placeholder;
use Pimcore\Translation\Translator;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BreadcrumbHelperService
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var Placeholder
     */
    protected $placeholderHelper;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    /**
     * BreadcrumbHelperService constructor.
     *
     * @param RequestStack $requestStack
     * @param Placeholder $placeholderHelper
     * @param Translator $translator
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(RequestStack $requestStack, Placeholder $placeholderHelper, Translator $translator, UrlGeneratorInterface $urlGenerator)
    {
        $this->requestStack = $requestStack;
        $this->placeholderHelper = $placeholderHelper;
        $this->translator = $translator;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @return Document
     */
    public function getCurrentDocument(): Document
    {
        return $this->requestStack->getCurrentRequest()->attributes->get('contentDocument');
    }

    public function enrichPage($page)
    {
        if (!$page instanceof Document\Page) {
            return;
        }
        $document = $this->getCurrentDocument();
        //breadcrumbs
        $currentRequest = $this->requestStack->getCurrentRequest();

        if ($currentRequest && $fromStaticRoute = $currentRequest->get('pimcore_request_source')) {
            $document = $page;

            while($document->getParentId() !== 0) {
                $document = $document->getParent();
                if ($document->getId() === 1) {
                    continue;
                }
                $this->addBreadcrumbPage($document);
            }
        }
    }

    public function enrichArticlePage(Article $article, $documentPage)
    {
        $document = $this->getCurrentDocument();
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($article->getRouter());
            $page->setLabel($article->getTitle());
            $page->setId(-1);
        } catch (\Exception $e) {

        }

        $this->enrichPage($documentPage);
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }

    public function enrichArticleEbookPage(ArticleEbook $article, $documentPage)
    {
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($article->getRouter());
            $page->setLabel($article->getTitle());
            $page->setId(-1);
        } catch (\Exception $e) {

        }

        $this->enrichPage($documentPage);
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }

    public function enrichArticleVideoPage(ArticleVideo $article, $documentPage)
    {
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($article->getRouter());
            $page->setLabel($article->getTitle());
            $page->setId(-1);
        } catch (\Exception $e) {

        }

        $this->enrichPage($documentPage);
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }

    public function enrichArticlePodcastPage(ArticlePodcast $article, $documentPage)
    {
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($article->getRouter());
            $page->setLabel($article->getTitle());
            $page->setId(-1);
        } catch (\Exception $e) {

        }

        $this->enrichPage($documentPage);
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }

    public function enrichEventPage(Event $event, $documentPage)
    {
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($event->getRouter());
            $page->setLabel($event->getTitle());
            $page->setId(-1);
        } catch (\Exception $e) {

        }

        $this->enrichPage($documentPage);
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }

    public function enrichProductGroupPage(ProductGroup $productGroup, $documentPage)
    {
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($productGroup->getRouter());
            $page->setLabel($productGroup->getName());
            $page->setId(-1);
        } catch (\Exception $e) {

        }

        $this->enrichPage($documentPage);
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }

    public function enrichProductPage(Product $product, $documentPage)
    {
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($product->getRouter());
            $page->setLabel($product->getName());
            $page->setId(-1);
        } catch (\Exception $e) {

        }

        $this->enrichPage($documentPage);
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }

    public function enrichCarelineFaqPage(CarelineFaq $carelineFaq, $documentPage)
    {
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($carelineFaq->getRouter());
            $page->setLabel('FAQ ' . $carelineFaq->getQuestion());
            $page->setId(-1);
        } catch (\Exception $e) {

        }

        $this->enrichPage($documentPage);
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }

    public function enrichCarelineServiceTypePage(CarelineServiceType $carelineServiceType, $documentPage)
    {
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($carelineServiceType->getRouter());
            $page->setLabel($carelineServiceType->getName());
            $page->setId(-1);
        } catch (\Exception $e) {

        }

        $this->enrichPage($documentPage);
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }

    public function enrichCarelineServiceExpertPage(CarelineServiceExpert $carelineServiceExpert, $documentPage)
    {
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($carelineServiceExpert->getRouter());
            $page->setLabel($carelineServiceExpert->getName());
            $page->setId(-1);
        } catch (\Exception $e) {

        }

        $this->enrichPage($documentPage);
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }

    private function addBreadcrumbPage(Document $document)
    {
        $page = new \Pimcore\Navigation\Page\Document();
        try {
            $page->setClass('main active');
            $page->setUri($document->getFullPath());
            $page->setLabel($document->getProperty('navigation_name'));
            $page->setId(-1);
        } catch (\Exception $e) {

        }
        $this->placeholderHelper->__invoke('addBreadcrumb')->append($page);
    }
}
