<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 03/07/2020
 * Time: 13:33
 */

namespace AppBundle\Exception\Auth;

use AppBundle\Exception\AbstractException;

class UserInactiveException extends AbstractException
{

}
