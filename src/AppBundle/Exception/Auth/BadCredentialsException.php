<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 18:02
 */

namespace AppBundle\Exception\Auth;

use AppBundle\Exception\AbstractException;

class BadCredentialsException extends AbstractException
{

}
