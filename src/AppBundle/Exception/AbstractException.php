<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 16/03/2020
 * Time: 18:00
 */

namespace AppBundle\Exception;

abstract class AbstractException extends \Exception
{

}
