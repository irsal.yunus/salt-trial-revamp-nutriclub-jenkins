<?php
/** 
 * PT. Ako Media Asia (https://salt.co.id)
 * Copyright 2020
 *
 * Lisenced Under MIT Lisence
 * Redistributions of files must retain the above copyright notice.
 *
 * @ Author: Tommy Priambodo
 * @ Website: http://tommypriambodo.com
 * @ Create Time: 2020-09-03 10:40:58
 * @ Modified By: undefined
 * @ Modified Time: 2020-09-03 10:41:00
 * @ Descriptions:
 */

namespace CaesarBundle\Controller;

use AppBundle\Services\UserService;
use CaesarBundle\Helper\FileHelper;
use CaesarBundle\Service\CSectionService;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\CSection;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ApiController extends FrontendController
{
    /**
     * @Route("/submit", name="submit")
     * @Method({"POST"})
     */
    public function defaultAction(Request $request, UserService $userService)
    {
        if ($request->getMethod() == 'POST') {
            $service = new CSectionService;
            if ($service->submit($request, $userService)) {
                return $this->json(['status' => 'success'], 200);
            }
        }  
            
        return $this->json(['status' => 'failed'], 400);
    }

    
}