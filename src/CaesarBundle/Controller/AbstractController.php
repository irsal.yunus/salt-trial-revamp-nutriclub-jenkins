<?php
/** 
 * PT. Ako Media Asia (https://salt.co.id)
 * Copyright 2020
 *
 * Lisenced Under MIT Lisence
 * Redistributions of files must retain the above copyright notice.
 *
 * @ Author: Tommy Priambodo
 * @ Website: http://tommypriambodo.com
 * @ Create Time: 2020-09-09 18:04:25
 * @ Modified By: undefined
 * @ Modified Time: 2020-09-09 18:04:35
 * @ Descriptions:
 */

namespace CaesarBundle\Controller;

use AppBundle\Controller\AbstractController as AppBundleAbstractController;

abstract class AbstractController extends AppBundleAbstractController
{

}