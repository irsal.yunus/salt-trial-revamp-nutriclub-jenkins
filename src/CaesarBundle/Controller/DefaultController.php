<?php
/** 
 * PT. Ako Media Asia (https://salt.co.id)
 * Copyright 2020
 *
 * Lisenced Under MIT Lisence
 * Redistributions of files must retain the above copyright notice.
 *
 * @ Author: Tommy Priambodo
 * @ Website: http://tommypriambodo.com
 * @ Create Time: 2020-09-02 12:58:59
 * @ Modified By: undefined
 * @ Modified Time: 2020-09-09 17:41:09
 * @ Descriptions:
 */

namespace CaesarBundle\Controller;

use CaesarBundle\Service\CSectionService;
use CaesarBundle\Tool\CookieHelper;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ {
    Route,
    Method
};

class DefaultController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function defaultAction(Request $request)
    {
        return $this->redirect('/siap-lewati-caesar/tes-potensi-caesar');
    }

    /**
     * @Route("/slide")
     */
    public function slideAction(Request $request)
    {
        
    }
    
    /**
     * @Route("/tes-potensi-caesar")
     */
    public function csectionAction(Request $request, CSectionService $cSectionService)
    {
        $cSectionService->setTransactionId();
    }

    /**
     * @Route("/preassesment")
     */
    public function preassesmentAction(Request $request)
    {
        
    }

    /**
     * @Route("/questions")
     */
    public function questionsAction(Request $request)
    {
        
    }

    /**
     * @Route("/form", name="caesar-form")
     */
    public function formAction(Request $request)
    {
        
    }

    /**
     * @Route("/result", name="caesar-result")
     */
    public function resultAction(Request $request)
    {
    }
}
