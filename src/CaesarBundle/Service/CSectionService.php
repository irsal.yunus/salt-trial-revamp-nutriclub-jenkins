<?php

/** 
 * PT. Ako Media Asia (https://salt.co.id)
 * Copyright 2020
 *
 * Lisenced Under MIT Lisence
 * Redistributions of files must retain the above copyright notice.
 *
 * @ Author: Tommy Priambodo
 * @ Website: http://tommypriambodo.com
 * @ Create Time: 2020-09-04 11:23:54
 * @ Modified By: undefined
 * @ Modified Time: 2020-09-04 11:24:00
 * @ Descriptions:
 */

namespace CaesarBundle\Service;

use AppBundle\Services\UserService;
use CaesarBundle\Tool\FileHelper;
use Carbon\Carbon;
use DateTime;
use Exception;
use Pimcore\Model\DataObject;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CSectionService
{
    const COOKIE_KEY_TRANID = 'cs_transaction_id';

    public function setTransactionId()
    {
        $id = Uuid::uuid4();

        $dotNetDomain = env('WILDCARD_DOMAIN_FOR_DOT_NET', null);

        $expiredCookie = Carbon::now()->addMinutes(30)->timestamp;

        setrawcookie(
            self::COOKIE_KEY_TRANID,
            $id,
            $expiredCookie,
            '/',
            $dotNetDomain
        );
    }

    /**
     * @param object $data
     */
    public function submit(Request $request, UserService $userService)
    {
        // $data = json_decode($request->getContent());
        $data = (object) $request->request->all();

        // if (
        //     $data->transaction_id != null
        //     && $data->tanggal_input != null
        //     && $data->questions != null
        //     && $data->result != null
        // ) {
        //     try {
        //         $testId = $this->generateTestId();
        //         $folder = FileHelper::folder('CSection');
    
        //         $memberId = $userService->getUser() != null ? $userService->getUser()->getId() : null;
    
        //         $model = new DataObject\CSection();
        //         $model->setKey(\Pimcore\File::getValidFilename($testId));
        //         $model->setParent($folder);
        //         $model->setTransactionId($data->transaction_id);
        //         $model->setTestId($testId);
        //         $model->setMemberId($memberId);
        //         $model->setTanggalHpht(new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $data->tanggal_hpht)))));
        //         $model->setTanggalKelahiran(new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $data->tanggal_kelahiran)))));
        //         if ($data->tanggal_trimester_3)
        //             $model->setTanggalTrimesterTiga(new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $data->tanggal_trimester_3)))));
                
        //         $model->setTanggalInput(new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $data->tanggal_input)))));
        //         $model->setIsTrimester($data->is_trimester);
        //         $model->setResult($data->result);
    
        //         if ($data->questions) {
        //             $items = new DataObject\Fieldcollection();
        //             foreach ($data->questions as $key => $value) {
        //                 $item = new DataObject\Fieldcollection\Data\CSectionQuestions();
        //                 $item->setNomor($key + 1);
        //                 $item->setNilai($value);
        //                 $items->add($item);
        //             }
        //             $model->setQuestions($items);
        //         }
    
        //         if ($data->trimester_questions) {
        //             $modelTrimesterQuestions = new DataObject\Fieldcollection();
        //             foreach ($data->trimester_questions as $key => $value) {
        //                 $item = new DataObject\Fieldcollection\Data\CSectionTrimesterQuestions();
        //                 $item->setNomor($key + 1);
        //                 $item->setNilai($value);
        //                 $modelTrimesterQuestions->add($item);
        //             }
        //             $model->setTrimesterQuestions($modelTrimesterQuestions);
        //         }
    
        //         $model->setPublished(true);
        //         $model->save();

        //         return 201;

        //     } catch (\Exception $e) {
        //         return 500;
        //     }
        // }
        // return 400;

        if (
            $data->nama_mama != null
            && $data->no_telp != null
            && $data->email != null
            // && $data->nama_anak != null
            // && $data->tanggal_anak != null
            && $data->kondisi_ibu != null
            && $data->tanggal_hpht != null
            && $data->tanggal_kelahiran != null
            // && $data->tanggal_trimester_3 != null
            && $data->tanggal_input != null
            // && $data->minggu_kehamilan != null
            && $data->questions != null
            // && $data->trimester_questions != null
            && $data->result != null
        ) {
            try {
                $testId = $this->generateTestId();
                $folder = FileHelper::folder('CSection');
    
                $memberId = $userService->getUser() != null ? $userService->getUser()->getId() : null;
    
                $model = new DataObject\CSection();
                $model->setKey(\Pimcore\File::getValidFilename($testId));
                $model->setParent($folder);
                // $model->setTransactionId($data->transaction_id);
                $model->setTestId($testId);
                $model->setMemberId($memberId);
                $model->setNamaMama($data->nama_mama);
                $model->setTelepon($data->no_telp);
                $model->setEmail($data->email);
                if ($data->nama_anak)
                    $model->setNamaAnak($data->nama_anak);
                if ($data->tanggal_anak)
                    $model->setTanggalAnak(new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $data->tanggal_anak)))));

                $model->setKondisiIbu($data->kondisi_ibu);
                $model->setTanggalHpht(new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $data->tanggal_hpht)))));
                $model->setTanggalKelahiran(new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $data->tanggal_kelahiran)))));
                if ($data->tanggal_trimester_3)
                    $model->setTanggalTrimesterTiga(new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $data->tanggal_trimester_3)))));
                
                $model->setTanggalInput(new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $data->tanggal_input)))));
                $model->setMingguKehamilan($data->minggu_kehamilan);
                $model->setIsTrimester($data->is_trimester);
                $model->setResult($data->result);
    
                if ($data->questions) {
                    $items = new DataObject\Fieldcollection();
                    foreach ($data->questions as $key => $value) {
                        $item = new DataObject\Fieldcollection\Data\CSectionQuestions();
                        $item->setNomor($key + 1);
                        $item->setNilai($value);
                        $items->add($item);
                    }
                    $model->setQuestions($items);
                }
    
                if ($data->trimester_questions) {
                    $modelTrimesterQuestions = new DataObject\Fieldcollection();
                    foreach ($data->trimester_questions as $key => $value) {
                        $item = new DataObject\Fieldcollection\Data\CSectionTrimesterQuestions();
                        $item->setNomor($key + 1);
                        $item->setNilai($value);
                        $modelTrimesterQuestions->add($item);
                    }
                    $model->setTrimesterQuestions($modelTrimesterQuestions);
                }
    
                $model->setPublished(true);
                $model->save();

                return true;
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    private function generateTestId()
    {
        // format : PPYYMMSSSS;
        // set prefix
        $prefix = 'CS';
        // set date
        $date = date('ym');
        // get latest test id
        $model = new DataObject\CSection\Listing();
        $model->setCondition("substring(testId, 3, 4) = date_format(current_date(), '%y%m')");
        $model->setOrderKey('oo_id');
        $model->setOrder('desc');
        $model->setLimit(1);
        $model->load();
        
        $no = 1;
        if ($model->getTotalCount() > 0) 
            $no = (int)substr($model->getData()[0]->getTestId(), 6, 4) + 1;

        return $prefix . $date . str_pad($no, 4, 0, STR_PAD_LEFT);
    }
}
