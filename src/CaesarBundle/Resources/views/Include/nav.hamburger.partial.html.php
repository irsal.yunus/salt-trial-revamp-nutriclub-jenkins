<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 16:08
 */

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\DataObject\Customer;
use Pimcore\Model\Document;
use Pimcore\Navigation\Page;

$user = $app->getUser();

if ($user) {
    /** @var Customer $userSso */
    $userSso = $this->getRequest()->getSession()->get('UserProfile');
}
?>

<?php
/** @var Page $page */
foreach ($this->pages as $page):
?>

<?php endforeach; ?>
