<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.43
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/csection.css', 'screen', false);

$this->headScript()->offsetSetFile(10, '/assets/js/pages/csection.js', 'text/javascript'); ?>

<div class="csection --preass">
  <div class="container">

    <div class="row justify-content-center">
      <div class="col-md-8">

        <!-- QUESTION 1 -->
        <div class="question active" id="question-1">
          <div class="question-heading">
            <h2>1 of 4</h2>
            <p>Tahukah Mama bahwa ada beberapa faktor yang akan mempengaruhi metode melahirkan Mama?
              <br/>
              Yuk ketahui dengan menjawab pertanyaan berikut Ma!</p>
          </div>
          <div class="question-choices">

            <div class="question-choice" id="q1-1" data-index="0">
              <div class="question-image">
                <img src="/assets/images/c-section/q1-1.png" alt="">
              </div>
              <p>Apakah usia Mama lebih<br/>dari 35 tahun?</p>
              <div class="question-action">
                <button class="btn" data-answer="1" type="button">Ya</button>
                <button class="btn" data-answer="0" type="button">Tidak</button>
              </div>
              <div class="question-did">
                <span>
                  <img src="/assets/images/c-section/icon-did.png" alt="">
                  <strong>Did you know?</strong>
                </span>
                <div class="question-popup">
                  <p>Potensi Caesar meningkat 2x jika melahirkan pada usia di atas 35 tahun</p>
                  <div class="question-tutup">Tutup</div>
                </div>
              </div>
            </div>

            <div class="question-choice" id="q1-2" data-index="1">
              <div class="question-image">
                <img src="/assets/images/c-section/q1-2.png" alt="">
              </div>
              <p>Apakah ini merupakan momen melahirkan pertama Mama?</p>
              <div class="question-action">
                <button class="btn" data-answer="1" type="button">Ya</button>
                <button class="btn" data-answer="0" type="button">Tidak</button>
              </div>
              <div class="question-did">
                <span>
                  <img src="/assets/images/c-section/icon-did.png" alt="">
                  <strong>Did you know?</strong>
                </span>
                <div class="question-popup">
                  <p>Potensi dan angka Caesar meningkat pada kelahiran pertama </p>
                  <div class="question-tutup">Tutup</div>
                </div>
              </div>
            </div>

          </div>
          <div class="question-nav">
            <a href="pre-assesment.html" class="btn question-prev">
              <i class="fa fa-chevron-left" aria-hidden="true"></i> Sebelumnya
            </a>
            <a href="javascript:void(0)" data-href="#question-2" class="btn btn-primary question-next disabled">
              Selanjutnya
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
          </div>
        </div>
        <!-- /QUESTION 1-->

        <!-- QUESTION 2 -->
        <div class="question" id="question-2">
          <div class="question-heading">
            <h2>2 of 4</h2>
          </div>
          <div class="question-choices">

            <div class="question-choice" id="q2-1" data-index="2">
              <div class="question-image">
                <img src="/assets/images/c-section/q2-1.png" alt="">
              </div>
              <p>Apakah tinggi badan Mama di atas 150cm?</p>
              <div class="question-action">
                <button class="btn" data-answer="0" type="button">Ya</button>
                <button class="btn" data-answer="1" type="button">Tidak</button>
              </div>
              <div class="question-did">
                <span>
                  <img src="/assets/images/c-section/icon-did.png" alt="">
                  <strong>Did you know?</strong>
                </span>
                <div class="question-popup">
                  <p>Potensi Caesar meningkat pada Mama dengan tinggi badan di bawah 150 cm</p>
                  <div class="question-tutup">Tutup</div>
                </div>
              </div>
            </div>

            <div class="question-choice" id="q2-2" data-index="3">
              <div class="question-image">
                <img src="/assets/images/c-section/q2-2.png" alt="">
              </div>
              <p>Bagaimana status Gizi (Indeks Massa Tubuh / IMT) Mama pada saat sebelum hamil / trimester 1?</p>
              <p class="small">IMT = [berat badan (kg)] / [tinggi badan (m) x tinggi badan (m)]</p>
              <div class="question-action --multiple">
                <button class="btn" data-answer="0" type="button">Underweight (di bawah 18,5)</button>
                <button class="btn" data-answer="0" type="button">Normal (18,5 sampai 24,9)</button>
                <button class="btn" data-answer="1" type="button">Overweight (25 sampai 29,9)</button>
                <button class="btn" data-answer="1" type="button">Obese (di atas 30)</button>
              </div>
              <div class="question-did">
                <span>
                  <img src="/assets/images/c-section/icon-did.png" alt="">
                  <strong>Did you know?</strong>
                </span>
                <div class="question-popup">
                  <p>Potensi Caesar meningkat pada Mama dengan berat badan berlebih dan obesitas</p>
                  <div class="question-tutup">Tutup</div>
                </div>
              </div>
            </div>

          </div>
          <div class="question-nav">
            <a href="javascript:void(0)" data-href="#question-1" class="btn question-prev">
              <i class="fa fa-chevron-left" aria-hidden="true"></i> Sebelumnya
            </a>
            <a href="javascript:void(0)" data-href="#question-3" class="btn btn-primary question-next disabled">
              Selanjutnya
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
          </div>
        </div>
        <!-- /QUESTION 2-->

        <!-- QUESTION 3 -->
        <div class="question" id="question-3">
          <div class="question-heading">
            <h2>3 of 4</h2>
          </div>
          <div class="question-choices">

            <div class="question-choice" id="q3-1" data-index="4">
              <div class="question-image">
                <img src="/assets/images/c-section/q3-1.png" alt="">
              </div>
              <p>Apakah sebelumnya Mama melahirkan secara spontan
                (per vaginam / tanpa menggunakan alat bantu)?</p>
                <div class="question-action">
                  <button class="btn" data-answer="0" type="button">Ya</button>
                  <button class="btn" data-answer="1" type="button">Tidak</button>
                </div>
                <div class="question-did">
                  <span>
                    <img src="/assets/images/c-section/icon-did.png" alt="">
                    <strong>Did you know?</strong>
                  </span>
                  <div class="question-popup">
                    <p>Potensi Caesar meningkat jika terdapat riwayat Caesar pada kehamilan sebelumnya</p>
                    <div class="question-tutup">Tutup</div>
                  </div>
                </div>
            </div>

            <div class="question-choice" id="q3-2" data-index="5">
              <div class="question-image">
                <img src="/assets/images/c-section/q3-2.png" alt="">
              </div>
              <p>Apakah Mama pernah melakukan operasi yang berhubungan
                dengan rahim?</p>
                <div class="question-action">
                  <button class="btn" data-answer="1" type="button">Ya</button>
                  <button class="btn" data-answer="0" type="button">Tidak</button>
                </div>
                <div class="question-did">
                  <span>
                    <img src="/assets/images/c-section/icon-did.png" alt="">
                    <strong>Did you know?</strong>
                  </span>
                  <div class="question-popup">
                    <p>Riwayat operasi rahim (seperti pengangkatan mioma atau adenomiosis) merupakan salah satu indikasi Caesar pada kehamilan berikutnya </p>
                    <div class="question-tutup">Tutup</div>
                  </div>
                </div>
            </div>

          </div>
          <div class="question-nav">
            <a href="javascript:void(0)" data-href="#question-2" class="btn question-prev">
              <i class="fa fa-chevron-left" aria-hidden="true"></i> Sebelumnya
            </a>
            <a href="javascript:void(0)" data-href="#question-4" class="btn btn-primary question-next disabled">
              Selanjutnya
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
          </div>
        </div>
        <!-- /QUESTION 3-->

        <!-- QUESTION 4 -->
        <div class="question" id="question-4">
          <div class="question-heading">
            <h2>4 of 4</h2>
          </div>
          <div class="question-choices">

            <div class="question-choice" id="q4-1" data-index="6">
              <div class="question-image">
                <img src="/assets/images/c-section/q4-1.png" alt="">
              </div>
              <p>Apakah selama ini Mama mengikuti program kehamilan (Assisted Reproductive Technology / bayi tabung)?</p>
              <div class="question-action">
                <button class="btn" data-answer="1" type="button">Ya</button>
                <button class="btn" data-answer="0" type="button">Tidak</button>
              </div>
              <div class="question-did">
                <span>
                  <img src="/assets/images/c-section/icon-did.png" alt="">
                  <strong>Did you know?</strong>
                </span>
                <div class="question-popup">
                  <p>Potensi Caesar meningkat apabila mengikuti program kehamilan seperti IVF (in vitro fertilization / bayi tabung)</p>
                  <div class="question-tutup">Tutup</div>
                </div>
              </div>
            </div>

            <div class="question-choice" id="q4-2" data-index="7">
              <div class="question-image">
                <img src="/assets/images/c-section/q4-2.png" alt="">
              </div>
              <p>Apakah Mama mengandung bayi/janin kembar?</p>
                <div class="question-action">
                  <button class="btn" data-answer="1" type="button">Ya</button>
                  <button class="btn" data-answer="0" type="button">Tidak</button>
                </div>
                <div class="question-did">
                  <span>
                    <img src="/assets/images/c-section/icon-did.png" alt="">
                    <strong>Did you know?</strong>
                  </span>
                  <div class="question-popup">
                    <p>Potensi Caesar meningkat pada kehamilan kembar</p>
                    <div class="question-tutup">Tutup</div>
                  </div>
                </div>
            </div>

          </div>
          <div class="question-nav">
            <a href="javascript:void(0)" data-href="#question-3" class="btn question-prev">
              <i class="fa fa-chevron-left" aria-hidden="true"></i> Sebelumnya
            </a>
            <a id="next-trimester" href="javascript:void(0)" data-href="#question-6a" class="btn btn-primary question-next disabled">
              Selanjutnya
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
            <a id="next-form" href="/siap-lewati-caesar/form" class="btn btn-primary question-next disabled">
              Selanjutnya
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
          </div>
        </div>
        <!-- /QUESTION 5 -->

        <!-- LANDING TRIMESTER -->
        <div class="question trimester-landing" id="question-6a">
          <img src="/assets/images/c-section/img-trimester.png" alt="">
          <p>Selamat Mama telah memasuki trimester 3, kondisi tubuh Mama beserta janin akan  berpengaruh dalam menentukan metode melahirkan.</p>
          <div class="question-nav">
            <a href="javascript:void(0)" data-href="#question-6" class="btn btn-primary question-next">
              Kenali kondisi Mama disini
            </a>
            <a href="javascript:void(0)" data-href="#question-5" class="btn question-prev">
              Sebelumnya
            </a>
          </div>
        </div>
        <!-- /LANDING TRIMESTER -->

        <!-- QUESTION TRIMESTER / QUESTION 6 -->
        <div class="question" id="question-6">
          <!-- <div class="question-heading">
            <h2>5 of 5</h2>
          </div> -->
          <div class="question-choices --trimester">

            <div class="question-choice" id="q6-1" data-index="0">
              <p>Apakah Mama memeriksakan kehamilan minimal 4x selama masa kehamilan?</p>
              <div class="question-action">
                <button class="btn" data-answer="0" type="button">Ya</button>
                <button class="btn" data-answer="1" type="button">Tidak</button>
                <button class="btn" data-answer="0" type="button">Tidak Tahu</button>
              </div>
            </div>

            <div class="question-choice" id="q6-2" data-index="1">
              <p>Apakah kenaikan berat badan Mama lebih dari 11.5kg bila status gizi (IMT) Mama di awal kehamilan 25-29.9 atau lebih dari 9kg bila status gizi (IMT) Mama di awal kehamilan >30?</p>
              <div class="question-action">
                <button class="btn" data-answer="1" type="button">Ya</button>
                <button class="btn" data-answer="0" type="button">Tidak</button>
                <button class="btn" data-answer="0" type="button">Tidak Tahu</button>
              </div>
            </div>

            <div class="question-choice" id="q6-3" data-index="2">
              <p>Apakah sebelum hamil Mama pernah terdiagnosis salah satu kondisi ini:</p>
              <ul>
                <li>Gula darah tinggi/Diabetes Melitus</li>
                <li>Jantung</li>
                <li>Tekanan darah tinggi/Hipertensi</li>
                <li>Kelainan ginjal</li>
                <li>Penyakit autoimun</li>
                <li>Penyakit infeksi menular seperti HIV, sifilis</li>
                <li>Penyakit lain yang membutuhkan pengobatan berkala</li>
              </ul>
              
              <div class="question-action">
                <button class="btn" data-answer="1" type="button">Ya</button>
                <button class="btn" data-answer="0" type="button">Tidak</button>
                <button class="btn" data-answer="0" type="button">Tidak Tahu</button>
              </div>
            </div>

            <div class="question-choice" id="q6-4" data-index="3">
              <p>Selama kehamilan saat ini apakah Mama mengalami salah satu kondisi ini:</p>
              <ul>
                <li>Tekanan darah tinggi/hipertensi/preeklamsia.</li>
                <li>Gula darah tinggi/diabetes melitus gestasional.</li>
                <li>Gangguan pertumbuhan janin:
                  <ul>
                    <li>Pertumbuhan janin terhambat.</li>
                    <li>Janin lebih besar usia kehamilan.</li>
                  </ul>
                </li>
                <li>Letak plasenta menutupi jalan lahir.</li>
              </ul>
              <div class="question-action">
                <button class="btn" data-answer="1" type="button">Ya</button>
                <button class="btn" data-answer="0" type="button">Tidak</button>
                <button class="btn" data-answer="0" type="button">Tidak Tahu</button>
              </div>
            </div>

            <div class="question-choice" id="q6-5" data-index="4">
              <p>Apakah kelahiran anak sebelumnya secara Caesar, apakah jarak kelahiran dengan saat ini lebih 19 bulan?</p>
              <div class="question-action">
                <button class="btn" data-answer="0" type="button">Ya</button>
                <button class="btn" data-answer="1" type="button">Tidak</button>
                <button class="btn" data-answer="0" type="button">Tidak Tahu</button>
              </div>
            </div>

            <div class="question-choice" id="q6-6" data-index="5">
              <p>Apakah usia kehamilan Mama kurang dari 37 minggu?</p>
              <div class="question-action">
                <button class="btn" data-answer="1" type="button">Ya</button>
                <button class="btn" data-answer="0" type="button">Tidak</button>
                <button class="btn" data-answer="0" type="button">Tidak Tahu</button>
              </div>
            </div>

            <div class="question-choice" id="q6-7" data-index="6">
              <p>Apakah taksiran berat janin menjelang persalinan lebih dari atau sama dengan 3500 gram?</p>
              <div class="question-action">
                <button class="btn" data-answer="1" type="button">Ya</button>
                <button class="btn" data-answer="0" type="button">Tidak</button>
                <button class="btn" data-answer="0" type="button">Tidak Tahu</button>
              </div>
            </div>

            <div class="question-choice" id="q6-8" data-index="7">
              <p>Apakah presentasi (bagian terbawah) janin adalah kepala?</p>
              <div class="question-action">
                <button class="btn" data-answer="0" type="button">Ya</button>
                <button class="btn" data-answer="1" type="button">Tidak</button>
                <button class="btn" data-answer="0" type="button">Tidak Tahu</button>
              </div>
            </div>

           

          </div>
          <div class="question-nav">
            <a href="javascript:void(0)" data-href="#question-6a" class="btn question-prev">
              <i class="fa fa-chevron-left" aria-hidden="true"></i> Sebelumnya
            </a>
            <a href="/siap-lewati-caesar/form" class="btn btn-primary disabled">
              Selanjutnya
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
          </div>
        </div>
        <!-- /QUESTION TRIMESTER / QUESTION 6 -->

      </div>
    </div>

  </div>
</div>