<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.43
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/slick.css');
$this->headLink()->offsetSetStylesheet(6, '/assets/css/vendor/slick-theme.css');
$this->headLink()->offsetSetStylesheet(7, '/assets/css/pages/csection-landing.css', 'screen', false);


$this->headScript()->offsetSetFile(100, 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', 'text/javascript');
$this->headScript()->offsetSetFile(101, '/assets/js/pages/csection-landing-slides.js', 'text/javascript'); 
$this->headScript()->offsetSetFile(102, '/assets/js/pages/csection-landing.js', 'text/javascript'); ?>


<div class="csection-landing">
    <div id="first-page" class="slides first-page d-flex justify-content-center">
            <a id="first-btn-mob" class="position-absolute btn btn-csection-gold btn-csection-gold--1st d-inline-block d-md-none" href="javascript:void(0)">
                Temukan jawabannya disini
                <img src="/assets/images/csection-landing/arrow-right.svg">
            </a>
        <div class="col">
        <!-- <img src="/assets/images/csection-landing/group5.svg" 
            class="banner"
            alt="Seiring perjalanan kehamilan Mama, pasti banyak pertanyaan yang muncul seputar metode 
            kelahiran dan pasca melahirkan."> -->
        </div>
        <div class="col">
            <img src="/assets/images/csection-landing/group5.svg" 
            class="banner d-none d-md-block"
            alt="Seiring perjalanan kehamilan Mama, pasti banyak pertanyaan yang muncul seputar metode 
            kelahiran dan pasca melahirkan."> 
            <!-- <div id="vid-yt" class="banner my-4 ml-3 pt-5">
                    <iframe src="https://www.youtube.com/embed/ckQzMtptw4w" 
                    frameborder="0" allowfullscreen>
                    </iframe>
            </div>
            <div id="vid-thumb" class="banner vid-thumb my-4 ml-3 pt-5">
                <img class="img-yt w-100" src="https://img.youtube.com/vi/ckQzMtptw4w/maxresdefault.jpg">
                <div class="playbutton pt-5">
                    <img src="/assets/images/csection-landing/music-and-multimedia.png">
                </div>
            </div> -->
            <a id="first-btn" class="btn btn-csection-gold btn-csection-gold--1st d-none d-md-inline-block" href="javascript:void(0)">
                Temukan jawabannya disini
                <img src="/assets/images/csection-landing/arrow-right.svg">
            </a>
        </div>
    </div>
    <div id="second-page" class="slides second-page">
        <div class="d-none d-md-block slides__image">
            &nbsp;
        </div>
        <div class="slides__description">
            <h3>Hi Mama</h3>
            <p>Tentu Mama sudah tidak sabar menanti kehadiran si Kecil. Apapun metode kelahiran yang akan dijalani, 
                pastikan Mama telah mempersiapkan yang terbaik untuk Mama dan si Kecil</p>
            <div class="slides__cta">
                <p class="d-block d-md-none legends">1 of 9</p>
                <a id="btn-1" class="btn btn-csection-gold" href="javascript:void(0)">
                    Yuk, persiapkan sekarang!
                    <img src="/assets/images/csection-landing/arrow-right.svg">
                </a>
            </div>
            <img src="/assets/images/csection-landing/shape2.svg" class="my-5 d-none d-md-block">
            <div class="slides__cursor">
                <p>1 of 9</p>
                <span class="next">
                    <a class="next-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/next.svg">
                    </a>
                </span>
                <span class="prev-1">
                    <a class="prev-slide-1" href="/siap-lewati-caesar">
                        <img src="/assets/images/csection-landing/prev.svg" class="d-none d-md-block">
                        <img src="/assets/images/csection-landing/arrow-left-white.svg" class="d-block d-md-none">
                    </a>
                </span>
            </div>
        </div>
    </div>
    <div id="third-page" class="slides third-page">
    <img src="/assets/images/csection-landing/slides-03-mob.png" class="slides-mob-banner d-block d-md-none">
        <div class="d-none d-md-flex slides__image">
            <img src="/assets/images/csection-landing/slide-03.png" class="w-100">
        </div>
        <div class="slides__description">
            <h4>Apa yang perlu dipersiapkan untuk proses kelahiran</h4>
            <p>Wajar jika Mama gugup menyambut kelahiran si Kecil, namun 
            dengan persiapan & pemilihan proses kelahiran yang tepat Mama akan merasa lebih tenang</p>
            <div class="slides__cta my-md-5">
            <p class="d-block d-md-none legends">2 of 9</p>
                <a class="btn btn-csection-gold" href="https://www.nutriclub.co.id/article/?_ga=2.10340469.1506377902.1599456068-1557687739.1583312913">
                Cari tahu A-Z Persiapan Melahirkan
                    <img src="/assets/images/csection-landing/arrow-right.svg">
                </a>
            </div>
            <a class="toggle-article" href="#">Video & Artikel</a>
            <div class="slides__cursor">
                <p>2 of 9</p>
                <span class="next">
                    <a class="next-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/next.svg">
                    </a>
                </span>
                <span class="prev">
                    <a class="prev-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/prev.svg" class="d-none d-md-block">
                        <img src="/assets/images/csection-landing/arrow-left-white.svg" class="d-block d-md-none">
                    </a>
                </span>
            </div>
        </div>
        <div class="popup-article">
            <div class="popup-article__wrapper">
                <div class="d-none d-md-flex justify-content-between">
                    <h3 class="text-blue">Video & Artikel</h3>
                    <a class="toggle-close" href="javascript:void(0)">
                      <img src="/assets/images/csection-landing/close-btn.svg" width="35" height="35">
                    </a>
                </div>
                <div class="article-items py-5">
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/_default_upload_bucket/image-thumb__2708__default/3-700x278_1.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Keistimewaan Bayi Prematur yang Perlu Mama Ketahui
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-kehamilan/gaya-hidup/keuangan/checklist-finansial-menuju-persalinan">
                                <img src="/_default_upload_bucket/image-thumb__2705__default/1%20700x278@2x.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Checklist Finansial Menuju Persalinan
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/articles/image/image-thumb__795__default/5-masalah-kesehatan-si-kecil-prematur_large.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                5 Masalah Kesehatan Si Kecil Prematur
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                    <a class="btn btn-csection-gold d-inline-block d-md-none" href="#">
                        Video & Artikel lainnya
                    </a>
                    <a class="toggle-close text-center mt-4" href="javascript:void(0)">
                      <img class="d-inline-block d-md-none" src="/assets/images/csection-landing/close-btn-purple.svg" width="35" height="35">
                    </a>
            </div>
        </div>
    </div>
    <div id="fourth-page" class="slides fourth-page">
     <img src="/assets/images/csection-landing/slides-04-mob.png" class="slides-mob-banner d-block d-md-none">
        <div class="d-none d-md-flex slides__image">
            <img src="/assets/images/csection-landing/slide-04.png" class="w-100">
        </div>
        <div class="slides__description">
            <h4>Apakah Mama berpotensi melahirkan Caesar</h4>
            <p>Ada beberapa kondisi yang akan berpengaruh pada metode kelahiran si Kecil.
            <br>Untuk memahaminya, Mama bisa cari tahu dengan mengikuti tes di bawah ini!</p>
            <div class="slides__cta my-md-5">
                <a href="/siap-lewati-caesar/tes-potensi-caesar">
                    <img src="/assets/images/csection-landing/btn-slide-04.png">
                </a>
            </div>
            <a class="toggle-article" href="#">Video & Artikel</a>
            <div class="slides__cursor">
                <p>3 of 9</p>
                <span class="next">
                    <a class="next-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/next.svg">
                    </a>
                </span>
                <span class="prev">
                    <a class="prev-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/prev.svg" class="d-none d-md-block">
                        <img src="/assets/images/csection-landing/arrow-left-white.svg" class="d-block d-md-none">
                    </a>
                </span>
            </div>
        </div>
        <div class="popup-article">
            <div class="popup-article__wrapper">
                <div class="d-none d-md-flex justify-content-between">
                    <h3 class="text-blue">Video & Artikel</h3>
                    <a class="toggle-close" href="javascript:void(0)">
                      <img src="/assets/images/csection-landing/close-btn.svg" width="35" height="35">
                    </a>
                </div>
                <div class="article-items py-5">
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/_default_upload_bucket/image-thumb__2708__default/3-700x278_1.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Keistimewaan Bayi Prematur yang Perlu Mama Ketahui
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-kehamilan/gaya-hidup/keuangan/checklist-finansial-menuju-persalinan">
                                <img src="/_default_upload_bucket/image-thumb__2705__default/1%20700x278@2x.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Checklist Finansial Menuju Persalinan
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/articles/image/image-thumb__795__default/5-masalah-kesehatan-si-kecil-prematur_large.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                5 Masalah Kesehatan Si Kecil Prematur
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                    <a class="btn btn-csection-gold d-inline-block d-md-none" href="#">
                        Video & Artikel lainnya
                    </a>
                    <a class="toggle-close text-center mt-4" href="javascript:void(0)">
                      <img class="d-inline-block d-md-none" src="/assets/images/csection-landing/close-btn-purple.svg" width="35" height="35">
                    </a>
            </div>
        </div>
    </div>
    <div id="fifth-page" class="slides fifth-page">
      <img src="/assets/images/csection-landing/slides-05-mob.png" class="slides-mob-banner d-block d-md-none">
        <div class="d-none d-md-flex slides__image">
            <img src="/assets/images/csection-landing/slide-05.png" class="w-100">
        </div>
        <div class="slides__description">
            <h4>Bagaimana proses kelahiran Caesar?</h4>
            <p>Apa saja sih yang perlu Mama siapkan untuk proses kelahiran caesar, dan apa saja dampaknya?</p>
            <div class="slides__cta my-md-5">
            <p class="d-block d-md-none legends">3 of 9</p>
            <a class="btn btn-csection-gold" href="https://www.nutriclub.co.id/article/?_ga=2.10340469.1506377902.1599456068-1557687739.1583312913">
            Pengetahuan seputar metode Caesarian
                    <img src="/assets/images/csection-landing/arrow-right.svg">
                </a>
            </div>
            <a class="toggle-article" href="#">Video & Artikel</a>
            <div class="slides__cursor">
                <p>4 of 9</p>
                <span class="next">
                    <a class="next-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/next.svg">
                    </a>
                </span>
                <span class="prev">
                    <a class="prev-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/prev.svg" class="d-none d-md-block">
                        <img src="/assets/images/csection-landing/arrow-left-white.svg" class="d-block d-md-none">
                    </a>
                </span>
            </div>
        </div>
        <div class="popup-article">
            <div class="popup-article__wrapper">
                <div class="d-none d-md-flex justify-content-between">
                    <h3 class="text-blue">Video & Artikel</h3>
                    <a class="toggle-close" href="javascript:void(0)">
                      <img src="/assets/images/csection-landing/close-btn.svg" width="35" height="35">
                    </a>
                </div>
                <div class="article-items py-5">
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/_default_upload_bucket/image-thumb__2708__default/3-700x278_1.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Keistimewaan Bayi Prematur yang Perlu Mama Ketahui
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-kehamilan/gaya-hidup/keuangan/checklist-finansial-menuju-persalinan">
                                <img src="/_default_upload_bucket/image-thumb__2705__default/1%20700x278@2x.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Checklist Finansial Menuju Persalinan
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/articles/image/image-thumb__795__default/5-masalah-kesehatan-si-kecil-prematur_large.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                5 Masalah Kesehatan Si Kecil Prematur
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                    <a class="btn btn-csection-gold d-inline-block d-md-none" href="#">
                        Video & Artikel lainnya
                    </a>
                    <a class="toggle-close text-center mt-4" href="javascript:void(0)">
                      <img class="d-inline-block d-md-none" src="/assets/images/csection-landing/close-btn-purple.svg" width="35" height="35">
                    </a>
            </div>
        </div>
    </div>
    <div id="sixth-page" class="slides sixth-page">
      <img src="/assets/images/csection-landing/slides-06-mob.png" class="slides-mob-banner d-block d-md-none">
        <div class="d-none d-md-flex slides__image">
            <img src="/assets/images/csection-landing/slide-06.png" class="w-100">
        </div>
        <div class="slides__description">
            <h4>Apakah ada pengaruhnya untuk si Kecil?</h4>
            <p>Tahukah Mama bahwa proses Caesar memungkinkan
            si Kecil melewatkan kesempatan mendapatkan bakteri baik yang penting bagi sistem imunnya.</p>
            <div class="slides__cta my-md-5">
            <p class="d-block d-md-none legends">5 of 9</p>
            <a class="btn btn-csection-gold" href="https://www.nutriclub.co.id/article/?_ga=2.10340469.1506377902.1599456068-1557687739.1583312913">
            Kenali pengaruh caesar terhadap imunitas si kecil
                    <img src="/assets/images/csection-landing/arrow-right.svg">
                </a>
            </div>
            <a class="toggle-article" href="#">Video & Artikel</a>
            <div class="slides__cursor">
                <p>5 of 9</p>
                <span class="next">
                    <a class="next-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/next.svg">
                    </a>
                </span>
                <span class="prev">
                    <a class="prev-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/prev.svg" class="d-none d-md-block">
                        <img src="/assets/images/csection-landing/arrow-left-white.svg" class="d-block d-md-none">
                    </a>
                </span>
            </div>
        </div>
        <div class="popup-article">
            <div class="popup-article__wrapper">
                <div class="d-none d-md-flex justify-content-between">
                    <h3 class="text-blue">Video & Artikel</h3>
                    <a class="toggle-close" href="javascript:void(0)">
                      <img src="/assets/images/csection-landing/close-btn.svg" width="35" height="35">
                    </a>
                </div>
                <div class="article-items py-5">
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/_default_upload_bucket/image-thumb__2708__default/3-700x278_1.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Keistimewaan Bayi Prematur yang Perlu Mama Ketahui
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-kehamilan/gaya-hidup/keuangan/checklist-finansial-menuju-persalinan">
                                <img src="/_default_upload_bucket/image-thumb__2705__default/1%20700x278@2x.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Checklist Finansial Menuju Persalinan
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/articles/image/image-thumb__795__default/5-masalah-kesehatan-si-kecil-prematur_large.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                5 Masalah Kesehatan Si Kecil Prematur
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                    <a class="btn btn-csection-gold d-inline-block d-md-none" href="#">
                        Video & Artikel lainnya
                    </a>
                    <a class="toggle-close text-center mt-4" href="javascript:void(0)">
                      <img class="d-inline-block d-md-none" src="/assets/images/csection-landing/close-btn-purple.svg" width="35" height="35">
                    </a>
            </div>
        </div>
    </div>
    <div id="seventh-page" class="slides sevent-page">
      <img src="/assets/images/csection-landing/slides-07-mob.png" class="slides-mob-banner d-block d-md-none">
        <div class="d-none d-md-flex slides__image">
            <img src="/assets/images/csection-landing/slide-07.png" class="w-100">
        </div>
        <div class="slides__description">
            <h4>Mengembalikan keseimbangan bakteri dalam usus</h4>
            <p>Tahukah Mama bahwa keseimbangan bakteri baik dalam tubuh si 
                Kecil bisa didapat dari pemberian ASI yang mengandung nutrisi lengkap termasuk 
                prebiotik dan probiotik</p>
            <div class="slides__cta my-md-5">
            <p class="d-block d-md-none legends">6 of 9</p>
            <a class="btn btn-csection-gold" href="https://www.nutriclub.co.id/article/?_ga=2.10340469.1506377902.1599456068-1557687739.1583312913">
            Siapkan Nutrisi Si Kecil
                    <img src="/assets/images/csection-landing/arrow-right.svg">
                </a>
            </div>
            <a class="toggle-article" href="#">Video & Artikel</a>
            <div class="slides__cursor">
                <p>6 of 9</p>
                <span class="next">
                    <a class="next-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/next.svg">
                    </a>
                </span>
                <span class="prev">
                    <a class="prev-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/prev.svg" class="d-none d-md-block">
                        <img src="/assets/images/csection-landing/arrow-left-white.svg" class="d-block d-md-none">
                    </a>
                </span>
            </div>
        </div>
        <div class="popup-article">
            <div class="popup-article__wrapper">
                <div class="d-none d-md-flex justify-content-between">
                    <h3 class="text-blue">Video & Artikel</h3>
                    <a class="toggle-close" href="javascript:void(0)">
                      <img src="/assets/images/csection-landing/close-btn.svg" width="35" height="35">
                    </a>
                </div>
                <div class="article-items py-5">
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/_default_upload_bucket/image-thumb__2708__default/3-700x278_1.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Keistimewaan Bayi Prematur yang Perlu Mama Ketahui
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-kehamilan/gaya-hidup/keuangan/checklist-finansial-menuju-persalinan">
                                <img src="/_default_upload_bucket/image-thumb__2705__default/1%20700x278@2x.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Checklist Finansial Menuju Persalinan
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/articles/image/image-thumb__795__default/5-masalah-kesehatan-si-kecil-prematur_large.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                5 Masalah Kesehatan Si Kecil Prematur
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                    <a class="btn btn-csection-gold d-inline-block d-md-none" href="#">
                        Video & Artikel lainnya
                    </a>
                    <a class="toggle-close text-center mt-4" href="javascript:void(0)">
                      <img class="d-inline-block d-md-none" src="/assets/images/csection-landing/close-btn-purple.svg" width="35" height="35">
                    </a>
            </div>
        </div>
    </div>
    <div id="eight-page" class="slides eight-page">
      <img src="/assets/images/csection-landing/slides-08-mob.png" class="slides-mob-banner d-block d-md-none">
        <div class="d-none d-md-flex slides__image">
            <img src="/assets/images/csection-landing/slide-08.png" class="w-100">
        </div>
        <div class="slides__description">
            <h4>Sinbiotik untuk perkuat sistem imun si Kecil</h4>
            <p>Sinbiotik dengan kombinasi seperti Prebiotik (FOS : GOS) dan seperti Probiotik (B Breve) dalam level spesifik dapat meningkatkan jumlah bakteri baik dalam usus agar sistem imun
            si Kecil lebih optimal.</p>
            <div class="slides__cta my-md-5">
            <p class="d-block d-md-none legends">7 of 9</p>
            <a class="btn btn-csection-gold" href="/product">
            Pelajari lebih lanjut mengenai sinbiotik
                    <img src="/assets/images/csection-landing/arrow-right.svg">
                </a>
            </div>
            <a class="toggle-article" href="#">Video & Artikel</a>
            <div class="slides__cursor">
                <p>7 of 9</p>
                <span class="next">
                    <a class="next-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/next.svg">
                    </a>
                </span>
                <span class="prev">
                    <a class="prev-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/prev.svg" class="d-none d-md-block">
                        <img src="/assets/images/csection-landing/arrow-left-white.svg" class="d-block d-md-none">
                    </a>
                </span>
            </div>
        </div>
        <div class="popup-article">
            <div class="popup-article__wrapper">
                <div class="d-none d-md-flex justify-content-between">
                    <h3 class="text-blue">Video & Artikel</h3>
                    <a class="toggle-close" href="javascript:void(0)">
                      <img src="/assets/images/csection-landing/close-btn.svg" width="35" height="35">
                    </a>
                </div>
                <div class="article-items py-5">
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/_default_upload_bucket/image-thumb__2708__default/3-700x278_1.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Keistimewaan Bayi Prematur yang Perlu Mama Ketahui
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-kehamilan/gaya-hidup/keuangan/checklist-finansial-menuju-persalinan">
                                <img src="/_default_upload_bucket/image-thumb__2705__default/1%20700x278@2x.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Checklist Finansial Menuju Persalinan
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/articles/image/image-thumb__795__default/5-masalah-kesehatan-si-kecil-prematur_large.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                5 Masalah Kesehatan Si Kecil Prematur
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                    <a class="btn btn-csection-gold d-inline-block d-md-none" href="#">
                        Video & Artikel lainnya
                    </a>
                    <a class="toggle-close text-center mt-4" href="javascript:void(0)">
                      <img class="d-inline-block d-md-none" src="/assets/images/csection-landing/close-btn-purple.svg" width="35" height="35">
                    </a>
            </div>
        </div>
    </div>
    <div id="ninth-page" class="slides ninth-page">
      <img src="/assets/images/csection-landing/slides-09-mob.png" class="slides-mob-banner d-block d-md-none">
        <div class="d-none d-md-flex slides__image">
            <img src="/assets/images/csection-landing/slide-09.png" class="w-100">
        </div>
        <div class="slides__description">
            <h4>Kembali Jadi Tangguh</h4>
            <p>Kehadiran si Kecil kini menjadi bagian penting dalam hidup Mama. Untuk itu, kondiri fisik dan mental Mama juga harus dijaga</p>
            <div class="slides__cta my-md-5">
            <p class="d-block d-md-none legends">8 of 9</p>
            <a class="btn btn-csection-gold" href="https://www.nutriclub.co.id/article/?_ga=2.10340469.1506377902.1599456068-1557687739.1583312913">
            Cari tahu pemulihan pasca caesar
                    <img src="/assets/images/csection-landing/arrow-right.svg">
                </a>
            </div>
            <a class="toggle-article" href="#">Video & Artikel</a>
            <div class="slides__cursor">
                <p>8 of 9</p>
                <span class="next">
                    <a class="next-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/next.svg">
                    </a>
                </span>
                <span class="prev">
                    <a class="prev-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/prev.svg" class="d-none d-md-block">
                        <img src="/assets/images/csection-landing/arrow-left-white.svg" class="d-block d-md-none">
                    </a>
                </span>
            </div>
        </div>
        <div class="popup-article">
            <div class="popup-article__wrapper">
                <div class="d-none d-md-flex justify-content-between">
                    <h3 class="text-blue">Video & Artikel</h3>
                    <a class="toggle-close" href="javascript:void(0)">
                      <img src="/assets/images/csection-landing/close-btn.svg" width="35" height="35">
                    </a>
                </div>
                <div class="article-items py-5">
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/_default_upload_bucket/image-thumb__2708__default/3-700x278_1.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Keistimewaan Bayi Prematur yang Perlu Mama Ketahui
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-kehamilan/gaya-hidup/keuangan/checklist-finansial-menuju-persalinan">
                                <img src="/_default_upload_bucket/image-thumb__2705__default/1%20700x278@2x.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                Checklist Finansial Menuju Persalinan
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <div class="card-article">
                        <div class="card-article__top">
                            <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                <img src="/articles/image/image-thumb__795__default/5-masalah-kesehatan-si-kecil-prematur_large.webp">
                            </a>
                        </div>
                        <div class="card-article__body">
                            <h4>                            
                                <a href="/article-prematur/kesehatan/informasi/keistimewaan-bayi-prematur-yang-perlu-mama-ketahui">
                                5 Masalah Kesehatan Si Kecil Prematur
                                </a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                    <a class="btn btn-csection-gold d-inline-block d-md-none" href="#">
                        Video & Artikel lainnya
                    </a>
                    <a class="toggle-close text-center mt-4" href="javascript:void(0)">
                      <img class="d-inline-block d-md-none" src="/assets/images/csection-landing/close-btn-purple.svg" width="35" height="35">
                    </a>
            </div>
        </div>
    </div>
    <div id="ninth-page" class="slides ninth-page">
    <img src="/assets/images/csection-landing/slides-10-mob.png" class="slides-mob-banner d-block d-md-none">
        <div class="d-none d-md-flex slides__image">
            <img src="/assets/images/csection-landing/slide-10.png" class="w-100">
        </div>
        <div class="slides__description">
            <h4>Ikuti perkembangan si Kecil dalam kandungan agar persiapan persalinan Mama semakin matang.</h4>
            <div class="slides__cta my-md-5">
            <p class="d-block d-md-none legends">9 of 9</p>
            <a class="btn btn-csection-gold" href="/my-pregnancy-today/">
            Lihat perkembangannya di sini!
                    <img src="/assets/images/csection-landing/arrow-right.svg">
                </a>
            </div>
            <a href="/siap-lewati-caesar">Kembali ke halaman utama</a>
            <div class="slides__cursor">
                <p>9 of 9</p>
                <span class="next">
                    <a class="next-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/next.svg">
                    </a>
                </span>
                <span class="prev">
                    <a class="prev-slide" href="javascript:void(0)">
                        <img src="/assets/images/csection-landing/prev.svg" class="d-none d-md-block">
                        <img src="/assets/images/csection-landing/arrow-left-white.svg" class="d-block d-md-none">
                    </a>
                </span>
            </div>
        </div>
    </div>
</div>
