<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.43
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/csection.css', 'screen', false);
$this->headLink()->offsetSetStylesheet(5, '/assets/vendor/jquery-ui/jquery-ui.min.css', 'screen', false);

$this->headScript()->offsetSetFile(10, '/assets/vendor/jquery-ui/jquery-ui.min.js', 'text/javascript');
$this->headScript()->offsetSetFile(11, '/assets/js/pages/csection.js', 'text/javascript'); ?>

<div class="csection --preass">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-5">
        <h1>Bantu kami mengenali Mama untuk hasil test yang lebih akurat</h1>
        <form>
          <div class="form-group">
            <label>Nama Mama</label>
            <input type="text" class="form-control" name="nama-mama" id="nama-mama" onkeypress="return isHuruf(event)" />
            <span class="error-msg" id="error-nama"></span>
          </div>
          <div class="form-group">
            <label>Hari Pertama Haid Terakhir (HPHT)</label>
            <div class="input-group">
              <input type="text" class="form-control" name="hpht" id="hpht" readonly />
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fa fa-calendar" aria-hidden="true"></i>
                </div>
              </div>
            </div>
            <span class="error-msg" id="error-hpht"></span>
          </div>
          <div class="form-group">
            <label>Usia Kehamilan</label>
            <input type="text" class="form-control" name="minggu-kehamilan" id="minggu-kehamilan" readonly />
          </div>

          <input type="hidden" id="date-ep" />
          <input type="hidden" id="date-tri" />
          <input type="hidden" id="is-tri" />
          <!-- <a href="/siap-lewati-caesar/questions" class="btn btn-primary btn-block">Selanjutnya</a> -->
          <button type="button" class="btn btn-primary btn-block" id="btn-preass">Selanjutnya</button>
        </form>
      </div>
    </div>
  </div>
</div>