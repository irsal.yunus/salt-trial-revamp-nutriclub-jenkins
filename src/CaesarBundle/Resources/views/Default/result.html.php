<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.43
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/csection.css', false);

$this->headScript()->offsetSetFile(10, '/assets/js/pages/csection.js', 'text/javascript'); ?>

<div class="csection --result">
  <div class="container">

    <div class="row justify-content-center">
      <div class="col-md-6 result-box">
        
        <div class="result-summary">
          <h1>Hi Mama <span>Lorem Ipsum</span></h1>
          <figure>
            <img class="result-1" src="/assets/images/c-section/result-low.png" alt=""/>
            <img class="result-2 result-3" src="/assets/images/c-section/result-high.png" alt=""/>
          </figure>
          <p class="result-2 result-3">“<strong>Ya,</strong> kemungkinan Mama memiliki faktor risiko untuk melahirkan secara Caesar”</p>
          <p class="result-1">"<strong>Tidak,</strong> kemungkinan Mama tidak perlu melahirkan secara Caesar"</p>
        </div>

        <div class="result-yuk">
          <h4>Yuk Ma, konsultasikan kondisi Mama ini dengan dokter kepercayaan agar persiapan Mama lebih matang!</h4>
          <p>*Dokter Anda mungkin menanyakan beberapa pertanyaan tambahan terkait kondisi Mama saat ini</p>
          <figure>
            <img src="/assets/images/c-section/ask-your-doctor.png" alt="">
          </figure>
          <div class="result-buttons">
             <button type="button" class="btn-print btn-primary btn">Download Hasil Test</button>
             <a href="/siap-lewati-caesar/tes-potensi-caesar" class="btn-reset btn">Ulangi Test</a>
          </div>
        </div>

        <div class="result-high">
          <p class="result-2">Dari jawaban Mama ditemukan adanya beberapa kondisi Mama yang mengarah pada potensi melahirkan secara Caesar.</p>
          <p class="result-3">Tahukah Mama, di usia trimester 3 kehamilan Mama, ditemukan adanya beberapa kondisi Mama yang mengarah pada potensi melahirkan secara Caesar.</p>
        </div>

        <div class="result-list">
          <div class="result-list-item" id="q1-1">
            <div class="result-list-img">
              <img src="/assets/images/c-section/q1-1.png" alt="">
            </div>
            <div class="result-list-text">
              <p>Apakah usia Mama lebih dari 35 tahun?</p>
            </div>
            <div class="result-list-answer">
              <p>Value</p>
            </div>
          </div>
          <div class="result-list-item" id="q1-2">
            <div class="result-list-img">
              <img src="/assets/images/c-section/q1-2.png" alt="">
            </div>
            <div class="result-list-text">
              <p>Apakah ini merupakan momen melahirkan pertama Mama?</p>
            </div>
            <div class="result-list-answer">
              <p>Value</p>
            </div>
          </div>
          <div class="result-list-item" id="q2-1">
            <div class="result-list-img">
              <img src="/assets/images/c-section/q2-1.png" alt="">
            </div>
            <div class="result-list-text">
              <p>Apakah tinggi badan Mama di atas 150cm?</p>
            </div>
            <div class="result-list-answer">
              <p>Value</p>
            </div>
          </div>
          <div class="result-list-item" id="q2-2">
            <div class="result-list-img">
              <img src="/assets/images/c-section/q2-2.png" alt="">
            </div>
            <div class="result-list-text">
              <p>Bagaimana Body Mass Index (BMI) Mama pada saat sebelum hamil / trimester 1?</p>
            </div>
            <div class="result-list-answer">
              <p>Value</p>
            </div>
          </div>
          <div class="result-list-item" id="q3-1">
            <div class="result-list-img">
              <img src="/assets/images/c-section/q3-1.png" alt="">
            </div>
            <div class="result-list-text">
              <p>Apakah sebelumnya Mama melahirkan secara spontan
                (tanpa menggunakan alat bantu)?</p>
            </div>
            <div class="result-list-answer">
              <p>Value</p>
            </div>
          </div>
          <div class="result-list-item" id="q3-2">
            <div class="result-list-img">
              <img src="/assets/images/c-section/q3-2.png" alt="">
            </div>
            <div class="result-list-text">
              <p>Apakah Mama pernah melakukan operasi yang berhubungan
                dengan rahim?</p>
            </div>
            <div class="result-list-answer">
              <p>Value</p>
            </div>
          </div>
          <div class="result-list-item" id="q4-1">
            <div class="result-list-img">
              <img src="/assets/images/c-section/q4-1.png" alt="">
            </div>
            <div class="result-list-text">
              <p>Apakah selama ini Mama mengikuti program kehamilan (Assisted Reproductive Technology)?</p>
            </div>
            <div class="result-list-answer">
              <p>Value</p>
            </div>
          </div>
          <div class="result-list-item" id="q4-2">
            <div class="result-list-img">
              <img src="/assets/images/c-section/q4-2.png" alt="">
            </div>
            <div class="result-list-text">
              <p>Apakah Mama mengandung bayi/janin kembar?</p>
            </div>
            <div class="result-list-answer">
              <p>Value</p>
            </div>
          </div>
        </div>

        <div class="result-last">
          <div class="result-last-row result-2 result-3">
            <figure>
              <img src="/assets/images/c-section/last-high.png" alt="">
            </figure>
            <div class="result-last-text">
              <h5>Tahukah Mama bahwa metode melahirkan Caesar berpengaruh terhadap sistem kekebalan tubuh si Kecil.</h5>
              <p>Ayo cari tahu lebih banyak informasi seputar metode Caesar</p>
              <a href="#" class="btn btn-primary">Disini</a>
            </div>
          </div>
          <div class="result-last-row result-1">
            <figure>
              <img src="/assets/images/c-section/last-low.png" alt="">
            </figure>
            <div class="result-last-text">
              <h5>Melahirkan membutuhkan persiapan yang matang loh, Ma.</h5>
              <p>Ayo cari tahu lebih banyak informasi seputar metode Caesar</p>
              <a href="#" class="btn btn-primary">Disini</a>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>