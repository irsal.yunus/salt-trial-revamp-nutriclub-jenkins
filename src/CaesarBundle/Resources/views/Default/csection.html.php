<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.43
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/csection.css', 'screen', false);

$this->headScript()->offsetSetFile(10, '/assets/js/pages/csection.js', 'text/javascript'); ?>

<div class="csection --landing">
  <div class="container">
    <figure>
      <img src="/assets/images/c-section/logo.png" alt="Tes Potensi Caesar">
    </figure>
    <div class="row justify-content-center">
      <div class="col-md-7">
        <h1>Hi Mama!</h1>
        <p>Melahirkan butuh persiapan matang, oleh karena itu, penting bagi Mama untuk memilih metode kelahiran yang tepat.</p>
        <p class="small">Yuk Ma, ambil Tes Potensi Caesar untuk cari tahu
          seberapa besar potensi Mama melahirkan dengan
          metode Caesar.
        </p>
        <figure id="decoration-xs">
          <img src="/assets/images/c-section/dec-1.png" alt="decoration" class="w-100">
        </figure>
        <a href="/siap-lewati-caesar/preassesment" class="btn btn-primary">Ambil Tes</a>
      </div>
    </div>
    <p class="footnote">*Berdasarkan literature review oleh Dr dr Rima Irwinda, SpOG(K)</p>
    <figure id="decoration">
      <img src="/assets/images/c-section/dec-1.png" alt="decoration">
    </figure>
  </div>
</div>