<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.43
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/csection.css', 'screen', false);
$this->headLink()->offsetSetStylesheet(5, '/assets/vendor/jquery-ui/jquery-ui.min.css', 'screen', false);

$this->headScript()->offsetSetFile(10, '/assets/vendor/jquery-ui/jquery-ui.min.js', 'text/javascript');
$this->headScript()->offsetSetFile(11, '/assets/js/pages/csection.js', 'text/javascript'); ?>

<div class="csection --form --preass">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-5 decoration">
        <img src="/assets/images/c-section/img-reg.png" alt="" class="w-100" />
      </div>
      <div class="col-md-5">
        <h1>Yay Selesai!</h1>
        <p>Yuk ma, registrasi dulu untuk melihat hasil tes Mama.</p>
        <form>
          <div class="form-group">
            <label>Nama lengkap mama</label>
            <input type="text" class="form-control" name="nama-mama" id="nama-lengkap-mama" readonly />
          </div>
          <div class="form-group">
            <label>Nomor telefon</label>
            <input type="text" class="form-control number-only" name="no-telp" id="no-telp" onkeypress="return isNumber(event)" maxlength="12" />
            <span class="error-msg" id="error-telp"></span>
          </div>
          <div class="form-group">
            <label>Alamat Email</label>
            <input type="email" class="form-control" name="email" id="email" />
            <span class="error-msg" id="error-email"></span>
          </div>
          <div class="form-group">
            <label>Kondisi Saat Ini</label>
            <select name="kondisi-ibu" id="kondisi-ibu" class="form-control">
              <option value="">-----</option>
              <option value="7">Saya sedang hamil</option>
              <option value="8">Belum hamil</option>
              <option value="9">Sedang hamil &amp; memiliki anak</option>
              <option value="10">Tidak hamil &amp; memiliki anak</option>
            </select>
            <span class="error-msg" id="error-kondisi"></span>
          </div>
          <div class="form-group hamil">
            <label>Usia Kehamilan</label>
            <input type="text" class="form-control" name="usia-kehamilan" id="usia-kehamilan-reg" readonly />
          </div>
          <div class="form-group anak" style="display:none;">
            <label>Nama Anak</label>
            <input type="text" class="form-control" name="nama-anak" id="nama-anak" />
            <span class="error-msg" id="error-anak"></span>
          </div>
          <div class="form-group anak" style="display:none;">
            <label>Tanggal Lahir Anak Mama</label>
            <input type="text" class="form-control" name="tanggal-anak" id="tanggal-anak" />
            <span class="error-msg" id="error-tanggal-anak"></span>
          </div>
          <div class="form-group">
            <label for="" class="label-checkbox">
              <input type="checkbox" class="" name="syarat-ketentuan" id="syarat-ketentuan"/>
              <span>Saya setuju
                <b class="term-color font-weight-bold">menjadi member Nutriclub</b>
                dan telah membaca
                <a href="https://old.nutriclub.co.id/syarat-dan-ketentuan/" target="_blank" class="term-color font-weight-bold">Syarat &amp; Ketentuan</a></span>
            </label>
            <span class="error-msg" id="error-syarat"></span>
          </div>
          <button class="btn btn-primary btn-result" type="button">Lihat Hasilnya <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
        </form>
      </div>
    </div>
  </div>
</div>