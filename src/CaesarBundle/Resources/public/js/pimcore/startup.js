pimcore.registerNS("pimcore.plugin.CaesarBundle");

pimcore.plugin.CaesarBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.CaesarBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("CaesarBundle ready!");
    }
});

var CaesarBundlePlugin = new pimcore.plugin.CaesarBundle();
