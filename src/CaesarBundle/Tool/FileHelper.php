<?php

namespace CaesarBundle\Tool;

use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Pimcore\Model\Document;

class FileHelper
{
	/**
	 * Get a folder and create it if not exists.
	 *
	 * @param 	string 		$name 	The name of the folder.
	 * @param 	int|string 	$parent The parent id or path for the folder. Default is Home.
	 * @param 	string 		$type 	The type for the folder. object (default) / asset / document.
	 *
	 * @return 	object
	 */
	public static function folder($name, $parent = 1, $type = 'object')
	{
		switch ($type) {
			case 'asset':
				$folder = ! is_string($parent) ? Asset::getByPath('/' . $name) : Asset::getByPath($parent . '/' . $name);

				if (empty($folder)) {
					$folder = new Asset\Folder();
					$folder->setFilename($name);
					! is_string($parent) ? $folder->setParentId($parent) : $folder->setParentId(Asset::getByPath($parent)->getId());
					$folder->save();
				}

				break;
			case 'document':
				$folder = ! is_string($parent) ? Document::getByPath('/' . $name) : Document::getByPath($parent . '/' . $name);

				if (empty($folder)) {
					$folder = new Document\Folder();
					$folder->setKey($name);
					! is_string($parent) ? $folder->setParentId($parent) : $folder->setParentId(Document::getByPath($parent)->getId());
					$folder->save();
				}

				break;
			case 'object':
				$folder = ! is_string($parent) ? DataObject::getByPath('/' . $name) : DataObject::getByPath($parent . '/' . $name);

				if (empty($folder)) {
					$folder = new DataObject\Folder();
					$folder->setKey($name);
					! is_string($parent) ? $folder->setParentId($parent) : $folder->setParentId(DataObject::getByPath($parent)->getId());
					$folder->save();
				}
		}

		return $folder;
	}
}
