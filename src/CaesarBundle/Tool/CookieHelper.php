<?php
/** 
 * PT. Ako Media Asia (https://salt.co.id)
 * Copyright 2020
 *
 * Lisenced Under MIT Lisence
 * Redistributions of files must retain the above copyright notice.
 *
 * @ Author: Tommy Priambodo
 * @ Website: http://tommypriambodo.com
 * @ Create Time: 2020-09-22 15:51:45
 * @ Modified By: undefined
 * @ Modified Time: 2020-09-22 15:51:51
 * @ Descriptions:
 */

namespace CaesarBundle\Tool;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RequestStack;

class CookieHelper 
{
    private $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function hasCookie($key)
    {
        $cookies = $this->request->cookies;

        if (!$cookies->has($key)) {
            throw new AccessDeniedException('Access denied. Transaction ID not found');
        }

        return true;
    }
}