<?php
/** 
 * PT. Ako Media Asia (https://salt.co.id)
 * Copyright 2020
 *
 * Lisenced Under MIT Lisence
 * Redistributions of files must retain the above copyright notice.
 *
 * @ Author: Tommy Priambodo
 * @ Website: http://tommypriambodo.com
 * @ Create Time: 2020-09-22 20:58:36
 * @ Modified By: undefined
 * @ Modified Time: 2020-09-22 20:58:41
 * @ Descriptions:
 */

namespace CaesarBundle\EventListener;

use Carbon\Carbon;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class AssesmentListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        // set priority into 31, after Symfony\Component\HttpKernel\EventListener\RouterListener::onKernelRequest()
        // so we can get _route, for more detail php bin/console debug:event-dispatcher
        // reference https://symfony.com/doc/current/event_dispatcher.html
        return [
            KernelEvents::REQUEST => [
                'onKernelRequest', 31
            ]
        ];
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $request = $requestEvent->getRequest();

        $referer = $request->headers->get('referer', null);
        $regex = '/\/(caesar)?\/?(?=[^\/]*$)/';

        preg_match_all($regex, $referer, $matches, PREG_SET_ORDER, 0);

        if ($matches[0][1]) {
            if (
                $request->getMethod() === 'GET' &&
                (
                    $request->get('_route') === 'account-login' ||
                    $request->get('_route') === 'account-register'
                )
            ) {
                $this->cookieCounter('refererCS', $referer);
            }
        }
    }

    private function cookieCounter($cookieName, $value, $expired = false)
    {
        $dotNetDomain = env('WILDCARD_DOMAIN_FOR_DOT_NET', null);

        $expiredCookie = $expired ? 1 : Carbon::now()->addMinutes(30)->timestamp;

        setrawcookie(
            $cookieName,
            $value,
            $expiredCookie,
            '/',
            $dotNetDomain
        );
    }
}