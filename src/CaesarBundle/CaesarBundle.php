<?php

namespace CaesarBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class CaesarBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/caesar/js/pimcore/startup.js'
        ];
    }
}