<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineFaq.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 30/07/20
 * @time 14.40
 *
 */

namespace CarelineBundle\Model\DataObject;

use AppBundle\Model\RouterInterface;
use Pimcore\Model\DataObject\CarelineFaq as BaseCarelineFaq;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Tool;

class CarelineFaq extends BaseCarelineFaq implements RouterInterface
{
    public function getRouterParams(): array
    {
        return [
            'carelineFaqSlug' => $this->getSlug()
        ];
    }

    public function getRouter(): string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $protocol = getenv('HTTP_PROTOCOL', 'https');
            $url = Tool::getHostUrl($protocol) . $linkGenerator;
        } catch (\Exception $e) {
            dd($e->getMessage());
        } finally {
            return $url;
        }
    }
}
