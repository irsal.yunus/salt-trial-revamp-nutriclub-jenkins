<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineServiceExpert.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 03/08/20
 * @time 17.21
 *
 */

namespace CarelineBundle\Model\DataObject;

use AppBundle\Model\RouterInterface;
use Pimcore\Model\DataObject\CarelineServiceExpert as BaseCarelineServiceExpert;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Tool;

class CarelineServiceExpert extends BaseCarelineServiceExpert implements RouterInterface
{
    public function getRouterParams(): array
    {
        return [
            'carelineServiceExpertSlug' => $this->getSlug()
        ];
    }

    public function getRouter(): string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $protocol = getenv('HTTP_PROTOCOL', 'https');
            $url = Tool::getHostUrl($protocol) . $linkGenerator;
        } catch (\Exception $e) {
            dd($e->getMessage());
        } finally {
            return $url;
        }
    }
}
