<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Listing.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 03/08/20
 * @time 17.21
 *
 */

namespace CarelineBundle\Model\DataObject\CarelineServiceExpert;

use Pimcore\Model\DataObject\CarelineServiceExpert\Listing as BaseCarelineServiceExpertListing;

class Listing extends BaseCarelineServiceExpertListing
{

}
