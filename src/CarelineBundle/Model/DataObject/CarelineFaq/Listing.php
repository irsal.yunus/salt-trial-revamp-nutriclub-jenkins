<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Listing.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 30/07/20
 * @time 15.39
 *
 */

namespace CarelineBundle\Model\DataObject\CarelineFaq;

use Pimcore\Model\DataObject\CarelineFaq\Listing as BaseCarelineFaqListing;

class Listing extends BaseCarelineFaqListing
{

}
