<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineServiceList.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.58
 *
 */

namespace CarelineBundle\Document\Areabrick;

use CarelineBundle\Services\CarelineServiceTypeService;
use Pimcore\Model\Document\Tag\Area\Info;

class CarelineServiceList extends AbstractAreabrick
{
    private $carelineServiceTypesService;

    public function __construct(CarelineServiceTypeService $carelineServiceTypesService)
    {
        $this->carelineServiceTypesService = $carelineServiceTypesService;
    }

    public function action(Info $info)
    {
        $info->getView()->carelineServiceTypes = $this->carelineServiceTypesService->getCarelineServiceTypes();
    }
}
