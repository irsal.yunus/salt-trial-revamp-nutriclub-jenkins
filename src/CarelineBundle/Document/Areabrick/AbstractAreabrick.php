<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource AbstractAreabrick.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.39
 *
 */

namespace CarelineBundle\Document\Areabrick;

use Pimcore\Extension\Document\Areabrick\AbstractTemplateAreabrick;
use Pimcore\Model\Document\Tag\Area\Info;

abstract class AbstractAreabrick extends AbstractTemplateAreabrick
{
    /**
     * Lazy programmer tips :) Widget name based on short class name.
     *
     * @return string|string[]|null
     * @throws \ReflectionException
     */
    public function getName()
    {
        return preg_replace('/(?<!\ )[A-Z]/', ' $0', (new \ReflectionClass($this))->getShortName());
    }

    public function getDescription()
    {
        return 'Careline Widget Collection';
    }

    /**
     * @return string
     */
    public function getTemplateSuffix()
    {
        return parent::TEMPLATE_SUFFIX_PHP;
    }

    /**
     * @return string
     */
    public function getTemplateLocation()
    {
        return parent::TEMPLATE_LOCATION_BUNDLE;
    }

    /**
     * @return bool
     */
    public function hasEditTemplate()
    {
        return true;
    }

    public function action(Info $info)
    {
        $info->view->prefixName = $info->getId();
    }
}
