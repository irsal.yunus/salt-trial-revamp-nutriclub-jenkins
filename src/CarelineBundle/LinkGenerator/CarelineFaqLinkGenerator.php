<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineFaqLinkGenerator.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 03/08/20
 * @time 15.06
 *
 */

namespace CarelineBundle\LinkGenerator;

use CarelineBundle\Model\DataObject\CarelineFaq;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Model\DataObject\Concrete;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RouterInterface;

class CarelineFaqLinkGenerator implements LinkGeneratorInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function generate(Concrete $object, array $params = []): string
    {
        if (!$object instanceof CarelineFaq) {
            throw new \InvalidArgumentException(sprintf('Object must be an instance of %s', CarelineFaq::class));
        }

        $routeParams = $object->getRouterParams();

        $referenceType = $params['referenceType'] ?? UrlGenerator::ABSOLUTE_PATH;

        return $this->router->generate('CARELINE_FAQ', $routeParams, $referenceType);
    }
}
