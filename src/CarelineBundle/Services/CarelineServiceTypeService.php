<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineServiceTypesService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 30/07/20
 * @time 15.55
 *
 */

namespace CarelineBundle\Services;

use AppBundle\Services\ReferrerService;
use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Pimcore\Model\DataObject\CarelineServiceType;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CarelineServiceTypeService
{
    private $request;

    private $breadcrumbHelperService;

    public function __construct(
        RequestStack $requestStack,
        BreadcrumbHelperService  $breadcrumbHelperService,
        ReferrerService $referrerService
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->breadcrumbHelperService = $breadcrumbHelperService;
        $this->referrerService = $referrerService;
    }

    public function getCarelineServiceTypes()
    {
        $carelineServiceType = new CarelineServiceType\Listing();

        $carelineServiceType->setOrderKey('oo_id');
        $carelineServiceType->setOrder('desc');

        return $carelineServiceType->getObjects();
    }

    public function getCarelineServiceType()
    {
        $carelineServiceTypeSlug = $this->request->get('carelineServiceTypeSlug');

        /** @var \CarelineBundle\Model\DataObject\CarelineServiceType $carelineServiceTypeObject */
        $carelineServiceTypeObject = CarelineServiceType::getBySlug($carelineServiceTypeSlug, 1);

        if (!$carelineServiceTypeObject) {
            throw new NotFoundHttpException('Careline Service Type not found.');
        }

        $document = WebsiteSetting::getByName('CARELINE_SERVICE_TYPE_PAGE') ?
            WebsiteSetting::getByName('CARELINE_SERVICE_TYPE_PAGE')->getData() : null;
        $this->breadcrumbHelperService->enrichCarelineServiceTypePage($carelineServiceTypeObject, $document);

        $this->referrerService->getReferrer();

        return $carelineServiceTypeObject;
    }
}
