<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineServiceExpertService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 30/07/20
 * @time 16.42
 *
 */

namespace CarelineBundle\Services;

use AppBundle\Services\ReferrerService;
use AppBundle\Website\Navigation\BreadcrumbHelperService;
use Pimcore\Model\DataObject\CarelineServiceExpert;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CarelineServiceExpertService
{
    private $request;

    private $breadcrumbHelperService;

    public function __construct(
        RequestStack $requestStack,
        BreadcrumbHelperService  $breadcrumbHelperService,
        ReferrerService $referrerService
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->breadcrumbHelperService = $breadcrumbHelperService;
        $this->referrerService = $referrerService;
    }

    public function getCarelineServiceExpert()
    {
        $carelineServiceExpertSlug = $this->request->get('carelineServiceExpertSlug');

        /** @var \CarelineBundle\Model\DataObject\CarelineServiceExpert $carelineServiceExpertObject */
        $carelineServiceExpertObject = CarelineServiceExpert::getBySlug($carelineServiceExpertSlug, 1);

        if (!$carelineServiceExpertObject) {
            throw new NotFoundHttpException('Careline Service Expert not found');
        }

        $document = WebsiteSetting::getByName('CARELINE_SERVICE_EXPERT_PAGE') ?
            WebsiteSetting::getByName('CARELINE_SERVICE_EXPERT_PAGE')->getData() : null;
        $this->breadcrumbHelperService->enrichCarelineServiceExpertPage($carelineServiceExpertObject, $document);

        $this->referrerService->getReferrer();

        return $carelineServiceExpertObject;
    }
}
