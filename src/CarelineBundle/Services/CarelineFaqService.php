<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineFaqService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 30/07/20
 * @time 14.59
 *
 */

namespace CarelineBundle\Services;

use AppBundle\Services\ReferrerService;
use AppBundle\Website\Navigation\BreadcrumbHelperService;
use CarelineBundle\Model\DataObject\CarelineFaq;
use Pimcore\Model\Document;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CarelineFaqService
{
    private $request;

    private $breadcrumbHelperService;

    private $referrerService;

    public function __construct(
        RequestStack $requestStack,
        BreadcrumbHelperService $breadcrumbHelperService,
        ReferrerService $referrerService
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->breadcrumbHelperService = $breadcrumbHelperService;
        $this->referrerService = $referrerService;
    }

    public function getCarelineFaq()
    {
        $carelineSlug = $this->request->get('carelineFaqSlug');

        /** @var CarelineFaq $carelineFaqObj */
        $carelineFaqObj = CarelineFaq::getBySlug($carelineSlug, 1);

        if (!$carelineFaqObj) {
            throw new NotFoundHttpException('Careline Faq Not Found');
        }

        $document = WebsiteSetting::getByName('CARELINE_FAQ_PAGE') ?
            WebsiteSetting::getByName('CARELINE_FAQ_PAGE')->getData() : null;
        $this->breadcrumbHelperService->enrichCarelineFaqPage($carelineFaqObj, $document);

        $this->referrerService->getReferrer();

        return $carelineFaqObj;
    }

    public function getCarelineFaqs()
    {
        $carelineFaqs = new CarelineFaq\Listing();

        $carelineFaqs->setOrderKey('oo_id');
        $carelineFaqs->setOrder('desc');

        $carelineFaqs->load();

        return $carelineFaqs->getObjects();
    }
}
