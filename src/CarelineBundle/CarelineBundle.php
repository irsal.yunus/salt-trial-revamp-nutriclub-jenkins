<?php

namespace CarelineBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class CarelineBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/careline/js/pimcore/startup.js'
        ];
    }
}