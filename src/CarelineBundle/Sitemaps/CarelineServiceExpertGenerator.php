<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineServiceExpertGenerator.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 03/08/20
 * @time 17.25
 *
 */

namespace CarelineBundle\Sitemaps;

use CarelineBundle\Model\DataObject\CarelineServiceExpert;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Sitemap\Element\{AbstractElementGenerator, GeneratorContext};
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CarelineServiceExpertGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'careline.service.expert') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'careline.service.expert';

        $carelineServiceExperts = new CarelineServiceExpert\Listing();
        $carelineServiceExperts->setOrderKey('oo_id');
        $carelineServiceExperts->setOrder('DESC');

        foreach ($carelineServiceExperts as $carelineServiceExpert) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($carelineServiceExpert);

            // run URL through registered processors
            $url = $this->process($url, $carelineServiceExpert, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(CarelineServiceExpert $carelineServiceExpert)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $carelineServiceExpert->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for Careline Faq class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($carelineServiceExpert, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
