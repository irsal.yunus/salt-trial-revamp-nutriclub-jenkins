<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineFaqGenerator.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 03/08/20
 * @time 15.04
 *
 */

namespace CarelineBundle\Sitemaps;

use CarelineBundle\Model\DataObject\CarelineFaq;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Sitemap\Element\{AbstractElementGenerator, GeneratorContext};
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CarelineFaqGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'careline.faq') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'careline.faq';

        $carelineFaqs = new CarelineFaq\Listing();
        $carelineFaqs->setOrderKey('oo_id');
        $carelineFaqs->setOrder('DESC');

        foreach ($carelineFaqs as $carelineFaq) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($carelineFaq);

            // run URL through registered processors
            $url = $this->process($url, $carelineFaq, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(CarelineFaq $carelineFaq)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $carelineFaq->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for Careline Faq class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($carelineFaq, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
