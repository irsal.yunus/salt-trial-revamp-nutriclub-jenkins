<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineServiceTypeGenerator.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 03/08/20
 * @time 15.32
 *
 */

namespace CarelineBundle\Sitemaps;

use CarelineBundle\Model\DataObject\CarelineServiceType;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Sitemap\Element\{AbstractElementGenerator, GeneratorContext};
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CarelineServiceTypeGenerator extends AbstractElementGenerator
{
    /**
     * @var LinkGeneratorInterface
     */
    private $linkGenerator;

    public function populate(UrlContainerInterface $urlContainer, string $section = null)
    {
        if (null !== $section && $section !== 'careline.service.type') {
            // do not add entries if section doesn't match
            return;
        }

        $section = 'careline.service.type';

        $carelineServiceTypes = new CarelineServiceType\Listing();
        $carelineServiceTypes->setOrderKey('oo_id');
        $carelineServiceTypes->setOrder('DESC');

        foreach ($carelineServiceTypes as $carelineServiceType) {
            $context = new GeneratorContext($urlContainer, $section, []);

            $url = $this->generateUrl($carelineServiceType);

            // run URL through registered processors
            $url = $this->process($url, $carelineServiceType, $context);

            if (null === $url) {
                continue;
            }

            $urlContainer->addUrl($url, $section);
        }
    }

    private function generateUrl(CarelineServiceType $carelineServiceType)
    {
        if (null === $this->linkGenerator) {
            $this->linkGenerator = $carelineServiceType->getClass()->getLinkGenerator();

            if (null === $this->linkGenerator) {
                throw new \RuntimeException('Link generator for Careline Faq class is not defined.');
            }
        }

        $url = $this->linkGenerator->generate($carelineServiceType, [
            'referenceType' => UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return new UrlConcrete($url);
    }
}
