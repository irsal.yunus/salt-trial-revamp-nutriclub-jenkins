<?php

namespace CarelineBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="careline-landing-page")
     */
    public function indexAction(Request $request)
    {

    }
}
