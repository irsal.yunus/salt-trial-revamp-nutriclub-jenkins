<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource FaqController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 11.49
 *
 */

namespace CarelineBundle\Controller;

use AppBundle\Services\FaqSearchService;
use CarelineBundle\Model\DataObject\CarelineFaq;
use CarelineBundle\Services\CarelineFaqService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FaqController extends AbstractController
{
    public function indexAction(Request $request, CarelineFaqService $carelineFaqService)
    {
        $this->view->carelineFaqs = $carelineFaqService->getCarelineFaqs();
    }

    public function detailAction(Request $request, CarelineFaqService $carelineFaqService)
    {
        $this->view->carelineFaqObject = $carelineFaqService->getCarelineFaq();
    }

    /**
     * @param Request $request
     *
     * @param FaqSearchService $faqSearchService
     * @return JsonResponse
     * @Route("/faq/load/more")
     */
    public function more(FaqSearchService $faqSearchService)
    {
        $subFaqs = $faqSearchService->getSubFaqs();
        $data = $subFaqs['data'] ?? [];

        $html = $this->render('Search/faq.partial.html.php', [
            'data' => $data
        ]);

        return $this->json([
            'success' => true,
            'data' => $html->getContent()
        ]);
    }
}
