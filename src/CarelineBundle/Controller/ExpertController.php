<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ExpertController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 30/07/20
 * @time 16.37
 *
 */

namespace CarelineBundle\Controller;

use CarelineBundle\Services\CarelineServiceExpertService;
use Symfony\Component\HttpFoundation\RequestStack;

class ExpertController extends AbstractController
{
    public function detailAction(RequestStack $requestStack, CarelineServiceExpertService $carelineServiceExpertService)
    {
        $this->view->carelineServiceExpertObject = $carelineServiceExpertService->getCarelineServiceExpert();
    }
}
