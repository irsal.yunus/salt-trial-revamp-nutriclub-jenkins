<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ServiceController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 11.50
 *
 */

namespace CarelineBundle\Controller;

use CarelineBundle\Services\CarelineServiceTypeService;
use Symfony\Component\HttpFoundation\Request;

class ServiceController extends AbstractController
{
    public function indexAction(Request $request)
    {

    }

    public function detailAction(Request $request, CarelineServiceTypeService $carelineServiceTypeService)
    {
        $this->view->carelineServiceType = $carelineServiceTypeService->getCarelineServiceType();
    }
}
