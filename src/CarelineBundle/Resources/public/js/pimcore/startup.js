pimcore.registerNS("pimcore.plugin.CarelineBundle");

pimcore.plugin.CarelineBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.CarelineBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("CarelineBundle ready!");
    }
});

var CarelineBundlePlugin = new pimcore.plugin.CarelineBundle();
