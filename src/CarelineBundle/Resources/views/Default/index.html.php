<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.43
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/home-careline.css', 'screen', false);

echo $this->areablock('careline-index-area-block', [
    'allowed' => [
        'breadcrumbs',
        'careline-contact',
        'careline-feature',
        'careline-home-banner',
        'careline-question',
        'careline-service-list'
    ]
]);
?>
<?php $this->headScript()
           ->offsetSetFile(10, '/assets/js/pages/home-careline.js', 'text/javascript')
           ->offsetSetFile(11, '/assets/js/pages/expert-advisor.js', 'text/javascript'); ?>

<div class="modal modal-disclaimer fade" id="modalDisclaimer" tabindex="-1" role="dialog" aria-labelledby="modalDisclaimer" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-body__content">
                    <p
                    >Informasi, tips dan saran yang diberikan oleh Nutriclub Expert Advisor tidak bermaksud menggantikan rekomendasi tenaga medis. Nutriclub Expert Advisor tidak akan memberikan saran yang berkaitan dengan kondisi medis atau pun produk di bawah 1 tahun.
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-agree" data-dismiss="modal">Lanjut dan Setuju</button>
            </div>
        </div>
    </div>
</div>
<?php if (!$this->editmode) { ?>
<div class="modal modal-time fade" id="modalDisclaimer" tabindex="-1" role="dialog" aria-labelledby="modalDisclaimer" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
<?php } ?>
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-body__content">
                    <?= $this->wysiwyg('expert-advisor-modal', []) ?>
                </div>
            </div>
        </div>
<?php if (!$this->editmode) { ?>
    </div>
</div>
<?php } ?>
