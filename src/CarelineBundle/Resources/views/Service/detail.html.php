<?php

use Pimcore\Model\DataObject\{CarelineServiceType, CarelineServiceExpert};

/**
 * phase1
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource detail.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 16.10
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/service.css', 'screen', false);

/** @var CarelineServiceType $carelineServiceTypeObject */
$carelineServiceTypeObject = $this->carelineServiceType;
?>

<div class="service">
    <div class="service__content container">
        <a href="<?= $this->path('careline-landing-page') ?>" class="service__content-back">
            <i class="fa fa-chevron-left"></i><?= $this->t('EVENT_BACK_BUTTON') ?>
        </a>
        <div class="service__content-head">
            <h1><?= $this->t('CARELINE_EXPERT_TEAM') ?></h1>
            <h2>
                <?= $carelineServiceTypeObject->getName() ?>
            </h2>
            <span><?= $this->t('CARELINE_NEED_HELP_WE_ARE_READY') ?></span>
        </div>
        <div class="service__content-body row">
            <?php
            $carelineExperts = $carelineServiceTypeObject->getExperts();

            foreach ($carelineExperts as $carelineExpert) {
                ?>
                <div class="service__content-body-item col-xs-3">
                    <a href="<?= $carelineExpert->getRouter() ?>">
                        <?= $carelineExpert->getProfilePicture() ?
                            $carelineExpert->getProfilePicture()
                                ->getThumbnail('careline-service-expert-thumbnail')
                                ->getHtml([]) : null
                        ?>
                        <div class="desc">
                            <h3>
                                <?= $carelineExpert->getName() ?>
                            </h3>
                            <span>
                                <?= $carelineExpert->getExpertAt() ?>
                            </span>
                        </div>
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<?= $this->inc('/snippets/careline', []) ?>

<?php
$this->headScript()->offsetSetFile(4, "/assets/js/pages/expert-advisor-team.js");
?>
