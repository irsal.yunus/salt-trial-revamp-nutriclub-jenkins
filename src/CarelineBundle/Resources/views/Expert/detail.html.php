<?php

use CarelineBundle\Model\DataObject\CarelineServiceExpert;

/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource detail.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 30/07/20
 * @time 16.38
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/service-detail.css', 'screen', false);

/** @var CarelineServiceExpert $carelineServiceExpertObject */
$carelineServiceExpertObject = $this->carelineServiceExpertObject;
?>

<div class="service-detail">
    <div class="service-detail__content container">
        <a href="<?= $this->session()->get('globalReferrer') ?? $this->path('careline-landing-page') ?>" class="service-detail__content-back">
            <i class="fa fa-chevron-left"></i><?= $this->t('EVENT_BACK_BUTTON') ?>
        </a>
        <div class="service-detail__content-head">
            <?= $carelineServiceExpertObject->getProfilePicture() ?
                $carelineServiceExpertObject->getProfilePicture()
                    ->getThumbnail('careline-profile-picture-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true
                    ]) : null
            ?>
        </div>
        <div class="service-detail__content-body">
            <div class="title">
                <h1><?= $carelineServiceExpertObject->getName() ?></h1>
                <h3><?= $carelineServiceExpertObject->getExpertAt() ?></h3>
            </div>
            <div class="desc">
                <?= $carelineServiceExpertObject->getDescription() ?>
            </div>
        </div>
    </div>
</div>

<?= $this->inc('/snippets/careline', []) ?>

<?php
    $this->headScript()->offsetSetFile(10, "/assets/js/pages/service-detail.js");
?>
