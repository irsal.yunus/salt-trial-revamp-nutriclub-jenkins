<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 11.04
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Title :</h4>
<?= $this->input('title', []) ?>

<h4>Feature Block :</h4>
<?php while($this->block('feature-block')->loop()) { ?>
    <h4>Feature Title :</h4>
    <?= $this->input('feature-title', []) ?>
    <h4>Feature Link :</h4>
    <?= $this->link('feature-link', []) ?>
    <h4>Feature Image :</h4>
    <?= $this->image('feature-image', [
        'uploadPath' => '/careline-feature/'
    ]) ?>
<?php } ?>
