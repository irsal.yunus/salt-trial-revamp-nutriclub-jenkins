<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 11.04
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<div class="careline-feature" id="feature">
    <div class="careline-feature__content container">
        <div class="careline-feature__content-head">
            <h2>
                <?= $this->input('title')->frontend() ?>
            </h2>
        </div>
        <div class="careline-feature__content-body">
            <div class="careline-feature__content-body-parrent">
                <div class="row">
                    <?php while($this->block('feature-block')->loop()) { ?>
                        <div class="careline-feature__content-body-parrent-item col-sm-12 col-lg-4">
                            <?= str_replace('</a>', null, $this->link('feature-link')->frontend()) ?>
                                <?= !$this->image('feature-image')->isEmpty() ?
                                    $this->image('feature-image')
                                        ->getThumbnail('feature-image-thumbnail')
                                        ->getHtml([]) : null
                                ?>
                                <span class="text-title">
                                    <?= $this->input('feature-title')->frontend() ?>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
