<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 11.01
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Title :</h4>
<?= $this->input('title', []) ?>

<h4>Description :</h4>
<?= $this->wysiwyg('description', []) ?>

<h4>FAQs block :</h4>
<?php while ($this->block('faqs-block')->loop()) { ?>
    <?php $position = $this->block('faqs-block')->getCurrentIndex() ?>
    <h4>Careline FAQ Object <?= $position ?> : </h4>
    <?= $this->relation('careline-faq-object', [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => [
            'CarelineFaq'
        ],
    ]) ?>
<?php } ?>

<h4>Link :</h4>
<?= $this->link('link', []) ?>

<h4>Button Appointment :</h4>
<?= $this->link('button-appointment-careline-question', []) ?>
