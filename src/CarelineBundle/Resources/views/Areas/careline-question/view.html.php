<?php

use CarelineBundle\Model\DataObject\CarelineFaq;

/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 11.01
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<div class="careline-question" id="question">
    <div class="careline-question__content container">
        <div class="careline-question__content-head">
            <h2><?= $this->input('title')->frontend() ?></h2>
            <!-- <div class="form-group-search">
                <img src="/assets/images/home-careline/search.png" alt="">
                <input type="text" placeholder="Search issues here.....">
            </div> -->
            <div class="desc">
                <?= $this->wysiwyg('description')->frontend() ?>
            </div>
        </div>
        <div class="careline-question__content-body">
            <div id="accord" class="parent-accord row justify-content-center">
                <?php while ($this->block('faqs-block')->loop()) { ?>
                    <?php
                    /** @var CarelineFaq $carelineFaqObject */
                    $carelineFaqObject = $this->relation('careline-faq-object')->getElement();
                    if (!$carelineFaqObject) {
                        continue;
                    }
                    ?>
                    <div class="parent-accord__item col-md-6 col-lg-6">
                        <a href="<?= $carelineFaqObject->getRouter() ?>">
                            <div class="parent-accord__item-head">
                                <h3>
                                    <?= $carelineFaqObject->getQuestion() ?>
                                </h3>
                            </div>
                            <div class="parent-accord__item-body">
                                <?= $carelineFaqObject->getAnswer() ?>
                                <div class="arrow">
                                    <strong>...</strong>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <?php
            if (!$this->link('button-appointment-careline-question')->isEmpty()) {
                ?>
                <div class="row">
                    <div class="col-10 col-md-4 offset-1 offset-md-4">
                        <?= $this->link('button-appointment-careline-question')->frontend() ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="bg-down"></div>
    <!-- <div class="block"></div> -->
</div>
