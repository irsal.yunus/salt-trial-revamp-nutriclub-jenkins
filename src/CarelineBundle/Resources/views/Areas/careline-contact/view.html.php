<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 11.03
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="careline-contact" id="contact">
    <div class="careline-contact__content container">
        <div class="careline-contact__content-head">
            <h2>
                <?= $this->input('title')->frontend() ?>
            </h2>
            <div class="desc">
                <span class="d-block"><?= $this->input('sub-title')->frontend() ?></span>
                <?= !$this->image('image-for-sub-title')->isEmpty() ?
                    $this->image('image-for-sub-title')
                        ->getThumbnail('image-for-sub-title')
                        ->getHtml([
                            'disableWidthHeightAttributes' => true
                        ]) : null
                ?>
            </div>
        </div>
        <div class="careline-contact__content-body">
            <div class="careline-contact__content-body-parrent">
                <div class="row">
                    <?php while ($this->block('contact-block')->loop()) { ?>
                        <div class="careline-contact__content-body-parrent-item col-sm-12 col-lg-6">
                            <a href="<?= $this->link('contact-block-link')->getHref() ?>" target="_blank" rel="noreferrer">
                                <?= !$this->image('contact-block-image')->isEmpty() ?
                                    $this->image('contact-block-image')
                                        ->getThumbnail('contact-block-image-thumbnail')
                                        ->getHtml([
                                            'disableWidthHeightAttributes' => true
                                        ]) : null
                                ?>
                                <span class="text-title">
                                    <?= $this->input('contact-block-title')->frontend() ?>
                                </span>
                                <span class="text-phone">
                                    <?= $this->input('contact-block-phone')->frontend() ?>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
