<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 11.03
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Title :</h4>
<?= $this->input('title', []) ?>

<h4>Sub Title :</h4>
<?= $this->input('sub-title', []) ?>

<h4>Image For Sub Title :</h4>
<?= $this->image('image-for-sub-title', [
    'uploadPath' => '/careline-contact/'
]) ?>

<h4>Contact Block :</h4>
<?php while ($this->block('contact-block')->loop()) { ?>
    <?php $position = $this->block('contact-block')->getCurrentIndex(); ?>
    <h4>Title <?= $position ?> :</h4>
    <?= $this->input('contact-block-title', []) ?>
    <h4>Phone <?= $position ?> :</h4>
    <?= $this->input('contact-block-phone', []) ?>
    <h4>Link <?= $position ?> :</h4>
    <?= $this->link('contact-block-link', []) ?>
    <h4>Image <?= $position ?> :</h4>
    <?= $this->image('contact-block-image', []) ?>
<?php } ?>
