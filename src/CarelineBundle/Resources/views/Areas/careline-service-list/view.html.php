<?php

use CarelineBundle\Model\DataObject\CarelineServiceType;

/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.59
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$carelineServiceTypes = $this->carelineServiceTypes ?? [];
?>
<div class="careline-service">
    <div class="careline-service__list">
        <div class="careline-service__list-content container">
            <div class="title">
                <h1>
                    <?= $this->input('title')->frontend() ?>
                </h1>
                <div class="title-desc">
                    <?= $this->wysiwyg('description')->frontend() ?>
                </div>
            </div>
            <div class="section-list row" id="service">
                <?php
                /** @var CarelineServiceType $carelineServiceType */
                foreach ($carelineServiceTypes as $carelineServiceType) {
                ?>
                <div class="section-list-item col-sm-3 col-xs-3">
                    <a href="<?= $carelineServiceType->getRouter() ?>" class="">
                        <?= $carelineServiceType->getImageIcon() ?
                            $carelineServiceType->getImageIcon()
                                ->getThumbnail('')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true
                                ]) : null
                        ?>
                        <div class="decs">
                            <span>
                                <?= $carelineServiceType->getName() ?>
                            </span>
                        </div>
                    </a>
                </div>
                <?php
                }
                ?>
            </div>
            <?php
            if (!$this->link('button-appointment-service-list')->isEmpty()) {
            ?>
            <div class="row">
                <div class="col-10 col-md-4 offset-1 offset-md-4">
                   <?= $this->link('button-appointment-service-list')->frontend() ?>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
    <div class="bg-down"></div>
    <!-- <div class="block"></div> -->
</div>
