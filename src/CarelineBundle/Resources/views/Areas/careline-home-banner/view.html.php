<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 10.53
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="careline-home">
    <div class="careline-home__banner">
        <div class="img">
            <?= !$this->image('careline-home-banner-image-desktop')->isEmpty() ?
                $this->image('careline-home-banner-image-desktop')
                    ->getThumbnail('careline-home-banner-image-desktop-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'img-deskop',
                    ]) : null
            ?>
            <?= !$this->image('careline-home-banner-image-mobile')->isEmpty() ?
                $this->image('careline-home-banner-image-mobile')
                    ->getThumbnail('careline-home-banner-image-mobile-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'img-mobile',
                    ]) : null
            ?>
        </div>
        <div class="title">
            <h1>
                <?= $this->input('title')->frontend() ?>
            </h1>
            <div class="title-desc">
                <?= $this->wysiwyg('description')->frontend() ?>
            </div>
        </div>
    </div>
</div>
