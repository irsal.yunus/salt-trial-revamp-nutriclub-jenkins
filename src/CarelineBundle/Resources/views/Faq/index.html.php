<?php
/**
 * phase1
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 11.50
 *
 */

use CarelineBundle\Model\DataObject\CarelineFaq;

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/faq.css', 'screen', false);

$carelineFaqs = $this->carelineFaqs ?? [];
?>

<div class="faq">
    <div class="faq__content container">
        <a href="/careline" class="faq__content-back">
            <i class="fa fa-chevron-left"></i><?= $this->t('CARELINE_FAQ_LANDING_BACK_BUTTON') ?>
        </a>
        <div class="faq__content-head">
            <h2><?= $this->t('CARELINE_FAQ_LANDING_QUESTION_FOR_MOMS_AND_DADS') ?></h2>
            <!-- <div class="form-group-search">
                <img src="/assets/images/home-careline/search.png" alt="">
                <input type="text" placeholder="Search issues here.....">
            </div> -->
        </div>
        <div class="faq__content-body">
            <div class="parent-accord row justify-content-center">
                <?php
                /** @var CarelineFaq $carelineFaq */
                foreach ($carelineFaqs as $carelineFaq) {
                ?>
                    <div class="parent-accord__item  col-md-6 col-lg-6"">
                        <a href="<?= $carelineFaq->getRouter() ?>">
                            <div class="parent-accord__item-head">
                                <h3>
                                    <?= $carelineFaq->getQuestion() ?>
                                </h3>
                            </div>
                            <div class="parent-accord__item-body">
                                <?= $carelineFaq->getAnswer() ?>
                                <div class="arrow">
                                    <strong>...</strong>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?= $this->inc('/snippets/careline', []) ?>

