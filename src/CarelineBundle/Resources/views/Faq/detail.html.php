<?php

use CarelineBundle\Model\DataObject\CarelineFaq;
use Pimcore\Model\DataObject\Fieldcollection\Data\CarelineSubFaq;

/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource detail.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 18.01
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$this->extend('layout.html.php');
$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/faq.css', 'screen', false);

/** @var CarelineFaq $carelineFaqObject */
$carelineFaqObject = $this->carelineFaqObject;
?>
<?php $this->headScript()->offsetSetFile(4, '/assets/vendor/jquery-ui/jquery-ui.min.js', 'text/javascript'); ?>
<?php $this->headScript()->offsetSetFile(10, '/assets/js/pages/faq.js', 'text/javascript'); ?>

<div class="faq-detail">
    <div class="faq-detail__content container">
        <a href="<?= $this->path('careline-landing-page') ?>" class="faq-detail__content-back">
            <i class="fa fa-chevron-left"></i><?= $this->t('CARELINE_FAQ_LANDING_BACK_BUTTON') ?>
        </a>
        <div class="faq__content-head">
            <h2><?= $this->t('CARELINE_FAQ_LANDING_QUESTION_FOR_MOMS_AND_DADS') ?></h2>
            <!-- <div class="form-group-search">
                <img src="/assets/images/home-careline/search.png" alt="">
                <input type="text" placeholder="Search issues here.....">
            </div> -->
        </div>
        <div class="head">
            <h2>
                <?= $carelineFaqObject->getQuestion()  ?>
            </h2>
        </div>
        <div id="accordion" class="accordion">
            <?php
            $subFaqs = $carelineFaqObject->getSubFaqs() && $carelineFaqObject->getSubFaqs()->getItems() ?
                $carelineFaqObject->getSubFaqs()->getItems() : [];

            /** @var CarelineSubFaq $subFaq */
            foreach ($subFaqs as $subFaq) {
            ?>
                <div class="accordion-title" id="<?= $subFaq->getIndex() ?>">
                    <span>
                        <?= $subFaq->getQuestion() ?>
                    </span>
                    <div class="arrow">
                        <img src="/assets/images/home-careline/arrow.png" alt="">
                    </div>
                </div>
                <div class="accordion-body acc-<?= $subFaq->getIndex() ?>">
                    <?= $subFaq->getAnswer() ?>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>

<?= $this->inc('/snippets/careline', []) ?>
