<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ResponseListener.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 14/08/20
 * @time 14.11
 *
 */

namespace MyPregnancyTodayBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Services\UserService;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

class ResponseListener implements EventSubscriberInterface
{
    private $router;

    private $userService;

    public function __construct(RouterInterface $router, UserService $userService)
    {
        $this->router = $router;
        $this->userService = $userService;
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        $request = $event->getRequest();

        $uri = $request->getUri();

        $regex = '/\/(my-pregnancy-today)?\/?(?=[^\/]*$)/';

        preg_match_all($regex, $uri, $matches, PREG_SET_ORDER, 0);

        if ($matches[0][1]) {
            $hasSecurity = $request->attributes->get('_security');
            if ($hasSecurity && !$this->userService->getUser()) {
                $response = new RedirectResponse($this->router->generate('my-pregnancy-today-join'));
                $event->setResponse($response);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => 'onKernelResponse'
        ];
    }
}

