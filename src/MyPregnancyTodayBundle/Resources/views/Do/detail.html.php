<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource detail.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 10/08/20
 * @time 06.48
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\DataObject\Fieldcollection\Data\DoList;
use Pimcore\Model\DataObject\MamaShouldDo;

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/my-pregnancy-today/mom-should-do.css', 'screen', false, [
    'defer' => 'defer'
]);

$this->extend('layout.html.php');

/** @var MamaShouldDo $mamaShouldDo */
$mamaShouldDo = $this->mamaShouldDo;

$weekTotal = (int) $this->websiteConfig('MY_PREGNANCY_TODAY_TOTAL_WEEKS', 42);
?>

<div class="mom-do">
    <div id="nutriclub-header"></div>
    <div class="mom-do__wrapper">
        <div class="status-page grey-background">
            <h1>
                <?= $this->t('MAMA_SHOULD_DO_TITLE') ?>
            </h1>
        </div>
        <div class="mom-do__figure">
            <?php if ($mamaShouldDo->getCoverMobile()) { ?>
                <?= $mamaShouldDo->getCoverMobile()
                    ->getThumbnail('my-pregnancy-today-cover-mobile-thumbnail')->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'mom-do__figure-mobile'
                    ])
                ?>
            <?php } ?>
            <?php if ($mamaShouldDo->getCoverDesktop()) { ?>
                <?= $mamaShouldDo->getCoverDesktop()
                    ->getThumbnail('my-pregnancy-today-cover-desktop-thumbnail')->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'mom-do__figure-desktop'
                    ])
                ?>
            <?php } ?>
        </div>
        <div class="mom-do__trimester">

            <div class="wave">
                <div class="mom-do__trimester-heading">
                    <h3><?= $mamaShouldDo->getTrimester() ?></h3>
                </div>
                <img
                    src="/assets/images/mom_should_know/background-blue-desktop.png"
                    alt="wave-desktop"
                    class="wave__desktop img-fluid"
                />
                <!-- <img
                  src="/assets/images/mom_should_know/background-blue-mobile.png"
                  alt="wave-mobile"
                  class="wave__mobile"
                /> -->
                <div class="mom-do__trimester-images row px-3">
                    <?php if ($mamaShouldDo->getFetusGrowthImage()) { ?>
                    <div class="mom-do__trimester-item col-4 align-self-center">
                        <?= $mamaShouldDo->getFetusGrowthImage()
                            ->getThumbnail('fetus-growth-image-thumbnail')->getHtml([
                                'disableWidthHeightAttributes' => true,
                                'class' => 'img-fluid rounded-circle'
                            ])
                        ?>
                        <p><?= $mamaShouldDo->getFetusCondition() ?></p>
                    </div>
                    <?php } ?>
                    <?php if ($mamaShouldDo->getNutritionImage()) { ?>
                    <div class="mom-do__trimester-item col-4 p-0">
                        <?= $mamaShouldDo->getNutritionImage()
                            ->getThumbnail('nutrition-thumbnail')->getHtml([
                                'disableWidthHeightAttributes' => true,
                                'class' => 'img-fluid rounded-circle'
                            ])
                        ?>
                    </div>
                    <?php } ?>
                    <div class="mom-do__trimester-item col-4 align-self-center">
                        <h3 class="rounded-circle">
                            <?= $weekTotal - $mamaShouldDo->getWeeks() ?>
                        </h3>
                        <p>
                            <?= $this->t('WEEKS_TO_GO') ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <?php
                $doLists = $mamaShouldDo->getDoLists() ? $mamaShouldDo->getDoLists()->getItems() : [];
                $countDoLists = count($doLists);

                /** @var DoList $doList */
                foreach ($doLists as $key => $doList) {

                ?>
                    <div class="mom-do__header container">
                        <h3>
                            <?= $doList->getTitle()  ?>
                        </h3>
                    </div>
                    <div class="mom-do__description container">
                        <?= $doList->getContent() ?>
                    </div>

                    <?php if ($countDoLists !== ($key+1)) { ?>
                    <div class="container">
                        <hr class="mom-do__border" />
                    </div>
                    <?php } ?>
                <?php
                }
                ?>

                <div class="row justify-content-center">
                    <div class="mom-do__buttons col-6 col-md-3">
                        <a href="<?= $this->previousMamaShouldDo ?>" class="btn btn-block <?= $this->previousMamaShouldDo ? '' : 'disabled' ?>" >
                            <?= $this->t('PREVIOUS_TRIMESTER') ?>
                        </a>
                    </div>
                    <div class="mom-do__buttons col-6 col-md-3">
                        <a href="<?= $this->nextMamaShouldDo ?>" class="btn btn-block <?= $this->nextMamaShouldDo ? '' : 'disabled' ?>">
                            <?= $this->t('NEXT_TRIMESTER') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->inc('/snippets/breadcrumbs', []) ?>
