<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource detail.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 10/08/20
 * @time 08.18
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\DataObject\Fieldcollection\Data\KnowList;
use Pimcore\Model\DataObject\MamaShouldKnow;

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/my-pregnancy-today/mom-should-know.css', 'screen', false, [
    'defer' => 'defer'
]);

$this->extend('layout.html.php');

/** @var MamaShouldKnow $mamaShouldKnow */
$mamaShouldKnow = $this->mamaShouldKnow;
$weekTotal = (int) $this->websiteConfig('MY_PREGNANCY_TODAY_TOTAL_WEEKS', 42);
?>
<div class="momknow">
    <div id="nutriclub-header"></div>
    <div class="momknow__wrapper">
        <div class="status-page blue-background">
            <h1>
                <?= $this->t('MAMA_SHOULD_KNOW_TITLE') ?>
            </h1>
        </div>
        <div class="momknow__figure">
            <?php if ($mamaShouldKnow->getCoverMobile()) { ?>
                <?= $mamaShouldKnow->getCoverMobile()
                    ->getThumbnail('my-pregnancy-today-cover-mobile-thumbnail')->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'momknow__figure-mobile'
                    ])
                ?>
            <?php } ?>
            <?php if ($mamaShouldKnow->getCoverDesktop()) { ?>
                <?= $mamaShouldKnow->getCoverDesktop()
                    ->getThumbnail('my-pregnancy-today-cover-desktop-thumbnail')->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'momknow__figure-desktop'
                    ])
                ?>
            <?php } ?>
        </div>
        <div class="momknow__trimester">
            <div class="wave">
                <div class="momknow__trimester-heading">
                    <h3><?= $mamaShouldKnow->getTrimester() ?></h3>
                </div>
                <img
                    src="/assets/images/mom_should_know/background-blue-desktop.png"
                    alt="wave-desktop"
                    class="wave__desktop img-fluid"
                />
                <div class="momknow__trimester-images row px-3">
                    <?php if ($mamaShouldKnow->getFetusGrowthImage()) { ?>
                        <div class="momknow__trimester-item col-4 align-self-center">
                            <?= $mamaShouldKnow->getFetusGrowthImage()
                                ->getThumbnail('fetus-growth-image-thumbnail')->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'img-fluid rounded-circle'
                                ])
                            ?>
                            <p><?= $mamaShouldKnow->getFetusCondition() ?></p>
                        </div>
                    <?php } ?>
                    <?php if ($mamaShouldKnow->getNutritionImage()) { ?>
                        <div class="momknow__trimester-item col-4 p-0">
                            <?= $mamaShouldKnow->getNutritionImage()
                                ->getThumbnail('nutrition-thumbnail')->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'img-fluid rounded-circle'
                                ])
                            ?>
                        </div>
                    <?php } ?>
                    <div class="momknow__trimester-item col-4 align-self-center">
                        <h3 class="rounded-circle">
                            <?= $weekTotal - $mamaShouldKnow->getWeeks() ?>
                        </h3>
                        <p>
                            <?= $this->t('WEEKS_TO_GO') ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <?php
                $knowLists = $mamaShouldKnow->getKnowLists() ? $mamaShouldKnow->getKnowLists()->getItems() : [];
                $countKnowLists = count($knowLists);

                /** @var KnowList $knowList */
                foreach ($knowLists as $key => $knowList) {

                    ?>
                    <div class="momknow__header container">
                        <h3>
                            <?= $knowList->getTitle()  ?>
                        </h3>
                    </div>
                    <div class="momknow__description container">
                        <?= $knowList->getContent() ?>
                    </div>

                    <?php if (1 === ($key+1)) { ?>
                        <div class="container">
                            <hr class="momknow__border" />
                        </div>
                    <?php } ?>
                    <?php
                }
                ?>
                <div class="row justify-content-center">
                    <div class="momknow__buttons col-5 momknow__buttons col-5 col-md-3">
                        <a href="<?= $this->previousMamaShouldKnow ?>" class="btn btn-block <?= $this->previousMamaShouldKnow ? '' : 'disabled' ?>">
                            <?= $this->t('PREVIOUS_TRIMESTER') ?>
                        </a>
                    </div>
                    <div class="momknow__buttons col-5 col-md-3">
                        <a href="<?= $this->nextMamaShouldKnow ?>" class="btn btn-block <?= $this->nextMamaShouldKnow ? '' : 'disabled' ?>">
                            <?= $this->t('NEXT_TRIMESTER') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->inc('/snippets/breadcrumbs', []) ?>
