<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 11/08/20
 * @time 22.22
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/my-pregnancy-today/reminder.css', 'screen', false, [
    'defer' => 'defer'
]);
?>

<div class="reminder">
    <div class="reminder__wrapper container">
        <div class="reminder__figure">
            <img src="/assets/images/Reminder/reminder-contents.png" alt="reminder-images">
        </div>
        <div class="reminder__description">
            <div class="reminder__header">
                <h1><?= $this->input('title') ?></h1>
            </div>
            <div class="reminder__text">
                <?= $this->wysiwyg('description') ?>
            </div>
            <div class="reminder__button">
                <?= $this->link('join-link', [
                    'class' => 'reminder__button-join btn btn-primary'
                ]) ?>
                <div class="reminder__join-text">
                    <?= $this->wysiwyg('login-here') ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->areablock('join-index-area-block', [
    'allowed' => [
        'breadcrumbs'
    ]
]);
