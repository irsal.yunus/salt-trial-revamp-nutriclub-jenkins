<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 19.17
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\Tool;
use Pimcore\Model\DataObject\MyPregnancyTodayRelatedTools;

$this->extend('layout.html.php');

/** @var MyPregnancyTodayRelatedTools $relatedTools */
$relatedTools = $this->relatedTools;
$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/my-pregnancy-today/related-tools.css', 'screen', false, [
    'defer' => 'defer'
]);
?>

<div class="related-tools">
    <div id="nutriclub-header"></div>
    <div class="related-tools__wrapper">
        <div class="status-page blue-background">
            <h1><?= $this->t('RELATED_TOOLS') ?></h1>
        </div>
        <div class="related-tools__figure">
            <?= $relatedTools && $relatedTools->getCoverMobile() ?
                $relatedTools->getCoverMobile()
                    ->getThumbnail('my-pregnancy-today-related-tools-mobile-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'related-tools__figure-mobile'
                    ]) : null
            ?>
            <?= $relatedTools && $relatedTools->getCoverDesktop() ?
                $relatedTools->getCoverDesktop()
                    ->getThumbnail('my-pregnancy-today-related-tools-desktop-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'related-tools__figure-desktop'
                    ]) : null
            ?>
        </div>
        <div class="related-tools__header container">
            <h3><?= $this->t('TOOLS_FOR_MOM') ?></h3>
        </div>
        <div class="related-tools__lists container">
            <div class="row">
                <?php
                if ($tools = $relatedTools->getTools()) {
                    $counter = 0;
                    /** @var Tool $tool */
                    foreach ($tools as $tool) {
                        $counter++;
                        ?>
                        <div class="col-6 col-md-2 <?= $counter === 1 ? 'offset-md-4' : null ?> pr-md-0">
                            <a href="<?= $tool->getCurrentLink()->getHref() ?>" class="card container related-tools__item">
                                <div class="related-tools__item-figure">
                                    <img src="<?= $tool->getProperty('IMAGE_ICON') ?>" class="img-fluid" alt="related-tools-image">
                                </div>
                                <div class="related-tools__item-name">
                                    <p><?= $tool->getName() ?></p>
                                </div>
                            </a>
                        </div>
                        <?php
                        $counter = $counter >= 2 ? 0 : $counter;
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->areablock('tool-index-area-block', [
    'allowed' => [
        'breadcrumbs'
    ]
]);
