<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource detail.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 16.39
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use MyPregnancyTodayBundle\Model\DataObject\MyPregnancyTodayRecipe;

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/my-pregnancy-today/recipe-detail.css', 'screen', false, [
    'defer' => 'defer'
]);

$this->extend('layout.html.php');

/** @var MyPregnancyTodayRecipe $recipe */
$recipe = $this->recipe;
?>
<div class="recipe-tipsdetail">
    <div id="nutriclub-header"></div>
    <div class="recipe-tipsdetail__wrapper">
        <div class="status-page blue-background">
            <h1><?= $this->t('RECIPE_TIPS') ?></h1>
        </div>
        <div class="recipe-tipsdetail__figure">
            <?= $recipe->getFoodImage() ? $recipe->getFoodImage()
                ->getThumbnail('my-pregnancy-today-food-image-thumbnail')
                ->getHtml([
                    'disableWidthHeightAttributes' => true,
                    'class' => 'recipe-tipsdetail__figure-mobile'
                ]) : null
            ?>
            <?= $recipe->getCoverDesktop() ? $recipe->getCoverDesktop()
                ->getThumbnail('my-pregnancy-today-cover-desktop-thumbnail')
                ->getHtml([
                    'disableWidthHeightAttributes' => true,
                    'class' => 'recipe-tipsdetail__figure-desktop'
                ]) : null
            ?>
        </div>
        <div class="recipe-tipsdetail__header container">
            <div class="recipe-tipsdetail__header-figure">
                <?= $recipe->getFoodImage() ? $recipe->getFoodImage()
                    ->getThumbnail('my-pregnancy-today-food-image-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'img-fluid'
                    ]) : null
                ?>
            </div>
            <h3><?= $recipe->getTitle() ?></h3>
        </div>
        <div class="row recipe-tipsdetail__description container">
            <div class="col-12 col-md-4 offset-md-3">
                <h4 class="recipe-tipsdetail__description-heading"><?= $this->t('INGREDIENTS') ?></h4>
                <?= $recipe->getIngredients() ?>
            </div>
            <div class="col-12 col-md-5">
                <h4 class="recipe-tipsdetail__description-heading"><?= $this->t('HOW_TO_COOK') ?></h4>
                <?= $recipe->getHowTo() ?>
            </div>
        </div>
        <?php if ($product = $recipe->getProduct()) { ?>
        <div class="recipe-tipsdetail__product">
            <div class="recipe-tipsdetail__header container">
                <h3><?= $product->getName() ?></h3>
            </div>
            <div class="row">
                <div class="col-12 col-md-2 offset-md-3">
                    <div class="recipe-tipsdetail__product-figure">
                        <?= $product->getImgIcon() ?
                            $product->getImgIcon()
                                ->getThumbnail('product-list-thumbnail')
                                ->getHtml([
                                    'disableWidthHeightAttributes' => true,
                                    'class' => 'img-fluid'
                                ]) : null
                        ?>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="recipe-tipsdetail__product-description container">
                        <?= $product->getMyPregnancyTodayShortDescription() ?>
                        <div class="recipe-tipsdetail__product-buttonwrapper">
                            <div class="recipe-tipsdetail__product-button">
                                <?= $product->getMyPregnancyTodayLink() ?>
                                <img src="/assets/images/recipe-tips/arrow.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>

<?php
echo $this->areablock('recipe-index-area-block', [
    'allowed' => [
        'breadcrumbs'
    ]
]);
