<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 16.39
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use MyPregnancyTodayBundle\Model\DataObject\{MyPregnancyTodayRecipe};
use Pimcore\Model\DataObject\MyPregnancyTodayRecipeTips;


$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/my-pregnancy-today/recipe.css', 'screen', false, [
    'defer' => 'defer'
]);

$this->extend('layout.html.php');

/** @var MyPregnancyTodayRecipeTips $recipeTips */
$recipeTips = $this->recipeTips;

?>

<div class="recipe-tips">
    <div id="nutriclub-header"></div>
    <div class="recipe-tips__wrapper">
        <div class="status-page blue-background">
            <h1>
                <?= $this->t('RECIPE_TIPS') ?>
            </h1>
        </div>
        <div class="recipe-tips__figure">
            <?= $recipeTips->getCoverMobile() ?
                $recipeTips->getCoverMobile()
                    ->getThumbnail('my-pregnancy-today-cover-mobile-thumbnail')->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'recipe-tips__figure-mobile'
                    ]) : null
            ?>
            <?= $recipeTips->getCoverDesktop() ?
                $recipeTips->getCoverDesktop()
                    ->getThumbnail('my-pregnancy-today-cover-desktop-thumbnail')->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'recipe-tips__figure-desktop'
                    ]) : null
            ?>
        </div>
        <div class="recipe-tips__header container">
            <h3><?= $recipeTips->getTitle() ?></h3>
        </div>
        <div class="row">
            <div class="recipe-tips__description-text col-12 offset-sm-1 col-md-6 offset-md-3 container">
                <?= $recipeTips->getDescription() ?>
                <div class="container">
                    <hr class="recipe-tips__border">
                </div>
            </div>
        </div>
        <div class="row container">
            <div class="col-12 col-md-6 offset-md-4">
                <?php
                if ($recipes = $recipeTips->getRecipes()) {
                    /** @var MyPregnancyTodayRecipe $recipe */
                    foreach ($recipes as $recipe) {
                ?>
                        <a href="<?= $recipe->getRouter() ?>" class="recipe-tips__item row">
                            <div class="recipe-tips__item-figure col-5 col-sm-2 col-md-4">
                                <?= $recipe->getFoodImage() ?
                                    $recipe->getFoodImage()->getThumbnail('my-pregnancy-today-food-image-thumbnail')
                                    ->getHtml([
                                        'disableWidthHeightAttributes' => true,
                                        'class' => 'img-fluid'
                                    ]) : null
                                ?>
                            </div>
                            <div class="recipe-tips__item-description-heading col-7 col-sm-8">
                                <p>
                                    <?= $recipe->getTitle() ?>
                                </p>
                            </div>
                        </a>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->areablock('recipe-index-area-block', [
    'allowed' => [
        'breadcrumbs'
    ]
]);
