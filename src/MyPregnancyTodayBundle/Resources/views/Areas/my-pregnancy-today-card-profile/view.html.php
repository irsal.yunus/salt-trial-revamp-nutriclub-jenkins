<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:33
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

/** @var \AppBundle\Model\DataObject\Customer $user */
$user = $this->user;
$userCurrentWeek = $this->userCurrentWeek;
$greeting = $this->greeting;
?>
<div class="after-login">
    <div class="after-login__wrapper">
        <div class="after-login__heading">
            <h1><?= $this->t('MY_PREGNANCY_TODAY') ?></h1>
        </div>
        <!-- card section -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="<?= $greeting ? 'col-12 col-md-5' : 'col-12 col-md-4' ?>">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="card-profile container">
                        <img src="<?= $user->getProfilePicture() ?>" alt="profile-picture" class="card-profile__avatar">

                        <?php if ($user && $userCurrentWeek >= 0 && !$greeting) { ?>
                            <img src="/assets/images/after-login/normal-card-desktop.png" alt="card-profile-white" class="card-profile__normal-desktop img-fluid w-100">
                            <img src="/assets/images/after-login/normal-card.png" alt="card-profile-white" class="card-profile__normal-mobile img-fluid w-100">

                            <div class="card-profile__content container">
                                <div class="card-profile__content-normal container">
                                    <h3>
                                        <?= $this->t('MY_PREGNANCY_TODAY_HI', [
                                            '%genderFriendlyName' => $user ?
                                                ucfirst($user->getGenderFriendlyName()) : null,
                                            '%name' => $user ? $user->getFullname() : null
                                        ]) ?>
                                    </h3>
                                    <?php if ($user && $userCurrentWeek > 0) { ?>
                                        <p>
                                            <?= $this->t('MY_PREGNANCY_TODAY_ON_PREGNANT', [
                                                '%genderFriendlyName' => $user ?
                                                    ucfirst($user->getGenderFriendlyName()) : null,
                                            ])
                                            ?>
                                        </p>
                                        <div class="container">
                                            <hr class="mom-do__border" />
                                        </div>
                                        <p>
                                            <?= $this->t('MY_PREGNANCY_TODAY_ON_AGE_PREGNANT', [
                                                '%agePregnant' => $user ?
                                                    $userCurrentWeek : '',
                                            ])
                                            ?>
                                        </p>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if ($user && $userCurrentWeek >= 0 && $greeting) { ?>
                            <img src="/assets/images/after-login/birthday-desktop.png" alt="card-profile-white" class="card-profile__birthday-desktop w-100 img-fluid">
                            <div class="card-profile__content-birthday container">
                                <div class="card-profile__content-birthday-heading">
                                    <h3>
                                        <?= $this->t('MY_PREGNANCY_TODAY_HI', [
                                            '%genderFriendlyName' => $user ?
                                                ucfirst($user->getGenderFriendlyName()) : null,
                                            '%name' => $user ? $user->getFullname() : null
                                        ]) ?>
                                    </h3>
                                    <p><?= $this->t('MY_PREGNANCY_TODAY_WARMEST_CONGRATS') ?></p>
                                </div>
                                <div class="container">
                                    <hr class="mom-do__border" />
                                </div>
                                <p class="card-profile__content-birthday-description">
                                    <?= $this->t('MY_PREGNANCY_TODAY_BABY_WAS_BORN') ?>
                                </p>
                                <p class="card-profile__content-birthday-promo">
                                    <?= $this->t('MY_PREGNANCY_TODAY_PROMO') ?>
                                </p>
                                <div class="card-profile__content-birthday-buttons">
                                    <?= $this->link('next-stage')->frontend() ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of card section -->
    </div>
</div>
