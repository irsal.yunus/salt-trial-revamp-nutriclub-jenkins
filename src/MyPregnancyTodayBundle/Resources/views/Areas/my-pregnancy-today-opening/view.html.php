<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:24
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="homepage">
    <div class="homepage__header container">
        <h2 class="homepage__heading">
            <?= $this->input('title')->frontend() ?>
        </h2>
        <?= !$this->image('image-logo')->isEmpty() ?
            $this->image('image-logo')
                ->getThumbnail('my-pregnancy-today-image-logo-thumbnail')
                ->getHtml([
                    'disableWidthHeightAttributes' => false,
                    'class' => "homepage__header-image"
                ]) : null
        ?>
        <div class="homepage__header-figure">
            <?= !$this->image('image-banner')->isEmpty() ?
                $this->image('image-banner')
                    ->getThumbnail('my-pregnancy-today-image-banner-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => false,
                        'class' => 'img-fluid'
                    ]) : null
            ?>
        </div>
        <h3 class="homepage__heading-description">
            <?= $this->input('sub-title')->frontend() ?>
        </h3>
        <div class="homepage__descriptions-wrapper">
            <?= $this->wysiwyg('description', [
                'class' => 'homepage__descriptions'
            ])->frontend() ?>
        </div>
        <?= $this->link('cta', [
            'class' => 'homepage__button btn btn-primary'
        ])->frontend() ?>
    </div>
</div>
