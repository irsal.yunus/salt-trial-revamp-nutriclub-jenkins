<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:22
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Title :</h4>
<?= $this->input('title', []) ?>

<h4>Image Logo :</h4>
<?= $this->image('image-logo', [
    'uploadPath' => '/my-pregnancy-today-opening/'
]) ?>

<h4>Image Banner :</h4>
<?= $this->image('image-banner', [
    'uploadPath' => '/my-pregnancy-today-opening/'
]) ?>

<h4>Sub Title :</h4>
<?= $this->input('sub-title', []) ?>

<h4>Description :</h4>
<?= $this->wysiwyg('description', []) ?>

<h4>Link :</h4>
<?= $this->link('cta', []) ?>
