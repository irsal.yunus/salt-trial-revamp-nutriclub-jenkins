<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:36
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Title :</h4>
<?= $this->input('title', [])?>

<h4>Tool Block :</h4>
<?php while ($this->block('tool-list')->loop()) { ?>
    <h4>Link :</h4>
    <?= $this->link('tool-link') ?>

    <h4>Tool Image :</h4>
    <?= $this->image('image', []) ?>

    <h4>Tool Description :</h4>
    <?= $this->wysiwyg('description', []) ?>
<?php } ?>
