<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:36
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<!-- tools pregnancy -->
<div class="tools-pregnancy">
    <div class="tools-pregnancy__wrapper container">
        <div class="tools-pregnancy__heading">
            <h4>
                <?= $this->input('title')->frontend() ?>
            </h4>
        </div>
        <div class="tools-pregnancy__lists row">
            <?php while ($this->block('tool-list')->loop()) { ?>
                <?= $this->link('tool-link')->isEmpty() ? '<div class="tools-pregnancy__item col-4 col-md-1">' : str_replace('</a>', null, $this->link('tool-link', [
                    'class' => 'tools-pregnancy__item col-4 col-md-1'
                ])->frontend()) ?>
                        <div class="tools-pregnancy__item-figure">
                            <?= !$this->image('image')->isEmpty() ?
                                $this->image('image')
                                    ->getThumbnail('mpt-tool-list-thumbnail')
                                    ->getHtml([
                                        'disableWidthHeightAttributes' => true,
                                        'class' => 'img-fluid'
                                    ]) : null ?>
                        </div>
                        <div class="tools-pregnancy__item-title">
                            <?= $this->wysiwyg('description')->frontend() ?>
                        </div>
                <?= $this->link('tool-link')->isEmpty() ? '</div>' : '</a>' ?>
            <?php } ?>
        </div>
    </div>
</div>
<!-- end of tools pregnancy -->
