<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:35
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\DataObject\MamaShouldDo;

/** @var MamaShouldDo $mamaShouldDo */
$mamaShouldDo = $this->mamaShouldDo;

$weekTotal = (int) $this->websiteConfig('MY_PREGNANCY_TODAY_TOTAL_WEEKS', 42);
$greeting = $this->greeting;
?>
<!-- three dot section -->
<div class="momknow__trimester">
    <div class="wave">
        <div class="momknow__trimester-heading">
            <h3><?= $mamaShouldDo && !$greeting ? $mamaShouldDo->getTrimester() : null ?></h3>
        </div>
        <img
            src="/assets/images/mom_should_know/background-blue-desktop.png"
            alt="wave-desktop"
            class="wave__desktop img-fluid"
        />
        <div class="momknow__trimester-images row <?= $greeting ? 'd-none' : null ?> px-3">
            <div class="momknow__trimester-item col-4 align-self-center">
                <?= $mamaShouldDo && $mamaShouldDo->getFetusGrowthImage() ? $mamaShouldDo
                    ->getFetusGrowthImage()
                    ->getThumbnail('fetus-growth-image-thumbnail')
                    ->getHtml([
                        'class' => 'img-fluid rounded-circle w-100',
                        'disableWidthHeightAttributes' => true,
                    ]) : null
                ?>
                <p>
                    <?= $mamaShouldDo && $mamaShouldDo->getFetusGrowthImage() ? $mamaShouldDo->getFetusCondition() : null ?>
                </p>
            </div>
            <div class="momknow__trimester-item col-4 p-0">
                <?= $mamaShouldDo && $mamaShouldDo->getNutritionImage() ? $mamaShouldDo
                    ->getNutritionImage()
                    ->getThumbnail('nutrition-image-thumbnail')
                    ->getHtml([
                        'disableWidthHeightAttributes' => true,
                        'class' => 'img-fluid rounded-circle'
                    ]) : null
                ?>
            </div>
            <div class="momknow__trimester-item col-4 align-self-center <?= $mamaShouldDo ? '' : 'd-none' ?>">
                <h3 class="rounded-circle"><?= $weekTotal - ($mamaShouldDo ? $mamaShouldDo->getWeeks() : $weekTotal) ?></h3>
                <p><?= $this->t('WEEKS_TO_GO') ?></p>
            </div>
        </div>
    </div>
</div>
<!-- end of three dot section -->
