<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:25
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<div class="feature">
    <div class="feature__header container">
        <h2 class="feature__heading text-center">
            <?= $this->input('title')->frontend() ?>
        </h2>
        <?= $this->wysiwyg('description', [
            'class' => 'feature__description'
        ])->frontend() ?>
    </div>
    <div class="feature__lists container">
        <?php while($this->block('feature-block', [])->loop()) { ?>
            <div class="feature__lists-item">
                <div class="feature__item-figure">
                <?= !$this->image('feature-image')->isEmpty() ?
                    $this->image('feature-image')
                        ->getThumbnail('feature-image-thumbnail')
                        ->getHtml([
                            'disableWidthHeightAttributes' => false
                        ]) : null
                ?>
                </div>
                <div class="feature__item-description">
                    <h3><?= $this->input('feature-title')->frontend() ?></h3>
                    <?= $this->wysiwyg('feature-description')->frontend() ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
