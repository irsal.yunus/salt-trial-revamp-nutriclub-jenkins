<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:25
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Title :</h4>
<?= $this->input('title', []) ?>

<h4>Description :</h4>
<?= $this->wysiwyg('description', []) ?>

<h4>Feature Block :</h4>
<?php while($this->block('feature-block', [])->loop()) { ?>
    <?php $current = $this->block('feature-block')->getCurrentIndex() ?>

    <h4>Feature Image <?= $current ?>: </h4>
    <?= $this->image('feature-image', [
        'uploadPath' => '/my-pregnancy-today-features/feature-image/'
    ]) ?>

    <h4>Feature Title <?= $current ?>: </h4>
    <?= $this->input('feature-title', []) ?>

    <h4>Feature Description <?= $current ?>: </h4>
    <?= $this->wysiwyg('feature-description', []) ?>

<?php } ?>
