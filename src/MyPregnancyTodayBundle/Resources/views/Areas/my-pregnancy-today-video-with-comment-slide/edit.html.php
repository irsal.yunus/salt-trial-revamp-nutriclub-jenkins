<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:27
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<h4>Description :</h4>
<?= $this->wysiwyg('description', []) ?>

<h4>Video Block :</h4>
<?php while ($this->block('video-block')->loop()) { ?>
    <h4>Video :</h4>
    <?= $this->video('yt-video', []) ?>
<?php } ?>

<h4>Comment Block :</h4>
<?php while ($this->block('comment-block')->loop()) { ?>
    <h4>Profile Image :</h4>
    <?= $this->image('profile-image', []) ?>

    <h4>Name :</h4>
    <?= $this->input('name', []) ?>

    <h4>Description :</h4>
    <?= $this->wysiwyg('description', []) ?>
<?php } ?>

