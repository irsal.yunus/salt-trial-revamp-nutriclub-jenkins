<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:27
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<!-- video lists section -->
<div class="video-lists">
    <div class="video-lists__wrapper container">
        <div class="video-lists__content">
            <div class="row">
                <?php while ($this->block('video-block')->loop()) { ?>
                    <div class="col-md-8 col-6">
                        <?= $this->video('yt-video')->isEmpty() ? null :
                            $this->video('yt-video')->frontend()
                        ?>
                    </div>
                <?php } ?>
                <div class="video-lists__content-summary col-md-4 col-6">
                    <?php while ($this->block('comment-block')->loop()) { ?>
                        <div class="video-lists__summary-item">
                            <div class="item-mobile">
                                <div class="video-lists__summary-wrapper">
                                    <div class="video-lists__summary-figure">
                                        <?= !$this->image('profile-image')->isEmpty() ?
                                            $this->image('profile-image')
                                                ->getThumbnail('profile-image-thumbnail')
                                                ->getHtml([
                                                    'disableWidthHeightAttributes' => true,
                                                ]) : null
                                        ?>
                                    </div>
                                    <div class="video-lists__summary-description">
                                        <h3><?= $this->input('name')->frontend() ?></h3>
                                    </div>
                                </div>
                                <div class="video-lists__summary-description">
                                    <?= $this->wysiwyg('description')->frontend() ?>
                                </div>
                            </div>
                            <div class="item-desktop">
                                <div class="video-lists__summary-figure">
                                    <?= !$this->image('profile-image')->isEmpty() ?
                                        $this->image('profile-image')
                                            ->getThumbnail('profile-image-thumbnail')
                                            ->getHtml([
                                                'disableWidthHeightAttributes' => true,
                                            ]) : null
                                    ?>
                                </div>
                                <div class="video-lists__summary-description">
                                    <h3><?= $this->input('name')->frontend() ?></h3>
                                    <?= $this->wysiwyg('description')->frontend() ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of video lists section -->
