<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:27
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<div class="video-lists">
    <div class="video-lists__wrapper container">
        <div class="video-lists__description">
            <?= $this->wysiwyg('description')->frontend() ?>
        </div>
        <div class="video-lists__content">
            <div class="row">
                <div class="col-md-8">
                    <div id="carousel-slide" class="carousel slide">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <?= $this->video('yt-video')->isEmpty() ? null :
                                    $this->video('yt-video')->frontend() ?>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carousel-slide" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-slide" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="video-lists__content-summary col-md-4">
                    <?php while ($this->block('comment-block')->loop()) { ?>
                        <div class="video-lists__summary-item">
                            <div class="video-lists__summary-figure">
                                <?= !$this->image('profile-image')->isEmpty() ?
                                    $this->image('profile-image')
                                        ->getThumbnail('my-pregnancy-today-video-with-comment-profile-image-thumbnail')
                                        ->getHtml([
                                            'disableWidthHeightAttributes' => true
                                        ]) : null
                                ?>
                            </div>
                            <div class="video-lists__summary-description">
                                <h3><?= $this->input('name')->frontend() ?></h3>
                                <?= $this->wysiwyg('description')->frontend() ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
