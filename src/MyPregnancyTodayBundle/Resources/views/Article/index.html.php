<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 16.39
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\DataObject\MyPregnancyTodayRelatedArticle;

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/my-pregnancy-today/article.css', 'screen', false, [
    'defer' => 'defer'
]);

$this->extend('layout.html.php');

/** @var MyPregnancyTodayRelatedArticle $relatedArticle */
$relatedArticle = $this->relatedArticle;
?>

<div class="related-articles">
    <div id="nutriclub-header"></div>
    <div class="related-articles__wrapper">
        <div class="status-page blue-background">
            <h1><?= $this->t('MY_PREGNANCY_TODAY_RELATED_TOOLS') ?></h1>
        </div>
        <div class="related-articles__figure">
            <?= $relatedArticle && $relatedArticle->getCoverDesktop() ? $relatedArticle->getCoverDesktop()
                ->getThumbnail('my-pregnancy-today-cover-desktop-thumbnail')->getHtml([
                    'disableWidthHeightAttributes' => true,
                    'class' => 'related-articles__figure-desktop'
                ]) : null ?>
            <?= $relatedArticle && $relatedArticle->getCoverMobile() ? $relatedArticle->getCoverMobile()
                ->getThumbnail('my-pregnancy-today-cover-mobile-thumbnail')->getHtml([
                    'disableWidthHeightAttributes' => true,
                    'class' => 'related-articles__figure-mobile'
                ])  : null ?>
        </div>
        <div class="related-articles__header container">
            <h3><?= $this->t('MY_PREGNANCY_TODAY_ARTICLE_FOR_MOMS') ?></h3>
        </div>
        <div class="related-articles__lists container row">
            <div class="col-12 col-md-8 offset-md-3">
                <?php if ($relatedArticle && $articles = $relatedArticle->getArticle()) { ?>
                    <?php foreach ($articles as $article) { ?>
                        <a href="<?= $article->getRouter() ?>" class="related-articles__item row">
                            <div class="related-articles__item-figure col-4 col-sm-2 col-md-4">
                                <?= $article && $article->getImgDesktop() ? $article->getImgDesktop()
                                    ->getThumbnail('my-pregnancy-today-cover-mobile-thumbnail')->getHtml([
                                        'disableWidthHeightAttributes' => true,
                                        'class' => 'img-fluid'
                                    ]) : null  ?>
                            </div>
                            <div class="related-articles__item-description col-8 col-sm-8">
                                <p class="related-articles__item-description-heading">
                                    <?= $article ? $article->getTitle() : null ?>
                                </p>
                                <p class="related-articles__item-description-text">
                                <?= $article ? \AppBundle\Helper\GeneralHelper::trimText($article->getContent(), 80) : null ?>
                                </p>
                                <p class="related-articles__item-description-readmore">
                                    <?= $this->t('MY_PREGNANCY_TODAY_ARTICLE_READ_MORE') ?>
                                </p>
                            </div>
                        </a>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->areablock('article-index-area-block', [
    'allowed' => [
        'breadcrumbs'
    ]
]);
