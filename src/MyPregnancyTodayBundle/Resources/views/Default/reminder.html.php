<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource reminder.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 24/07/2020
 * @time 13:27
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->headLink()->offsetSetStylesheet(4, '/assets/css/my-pregnancy-today/reminder.css', 'screen', false, [
    'defer' => 'defer'
]);

$this->extend('layout.html.php');
?>

<div class="reminder">
    <div class="reminder">
        <div class="reminder__wrapper container">
            <div class="reminder__figure">
                <img src="/assets/images/Reminder/reminder-contents.png" alt="reminder-images">
            </div>
            <div class="reminder__description">
                <div class="reminder__header">
                    <h1>Mama Belum Menjadi Member</h1>
                </div>
                <div class="reminder__text">
                    <p>Yuk, daftarkan diri Mama
                        terlebih dahulu</p>
                </div>
                <div class="reminder__button">
                    <button class="reminder__button-join btn btn-primary">
                        Join Now
                    </button>
                    <div class="reminder__join-text">
                        Have an account? Log in here!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
