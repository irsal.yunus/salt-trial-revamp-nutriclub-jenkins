<?php
/**
 * phase1
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource homelogin.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 28/07/20
 * @time 16.04
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->headLink()
    ->offsetSetStylesheet(4, '/assets/css/vendor/slick.css')
    ->offsetSetStylesheet(5, '/assets/css/vendor/slick-theme.css')
    ->offsetSetStylesheet(22, "https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css", 'screen', false, [
    'defer' => 'defer'
    ])
    ->offsetSetStylesheet(23, "https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.theme.min.css", 'screen', false, [
        'defer' => 'defer'
    ])
    ->offsetSetStylesheet(24, '/assets/css/pages/my-pregnancy-today/homepage-afterlogin.css', 'screen', false, [
    'defer' => 'defer'
    ]);

echo $this->areablock('homelogin-area-block', [
    'allowed' => [
        'breadcrumbs',
        'my-pregnancy-today-card-profile',
        'my-pregnancy-today-weekly-info',
        'my-pregnancy-today-tools',
        'my-pregnancy-today-video-with-comment-slide'
    ]
]);
?>

<?php
$haidPopups = $this->placeholder('haidPopup')->getValue();

if (is_array($haidPopups)) {
    foreach ($haidPopups as $haidPopup) {
        echo $haidPopup;
    }
}

if (is_string($haidPopups)) {
    echo $haidPopups;
}
?>
<?php
$bornPopups = $this->placeholder('bornPopup')->getValue();

if (is_array($bornPopups)) {
    foreach ($bornPopups as $variable) {
        echo $variable;
    }
}

if (is_string($bornPopups)) {
    echo $bornPopups;
}
?>
<?php
    $this->headScript()
        ->offsetSetFile(10, '/assets/js/vendor/slick.min.js', 'text/javascript', [])
        ->offsetSetFile(24, 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', 'text/javascript' )
        ->offsetSetFile(25, '/assets/js/widgets/my-pregnancy-today-popup.js', 'text/javascript' )
        ->offsetSetFile(26, '/assets/js/pages/homeafterlogin-mpt.js', 'text/javascript' );
?>

