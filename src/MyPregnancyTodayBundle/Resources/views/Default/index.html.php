<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/07/2020
 * Time: 15:53
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Symfony\Component\Security\Core\User\UserInterface;

$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/my-pregnancy-today/homepage-beforelogin.css', 'screen', false, [
    'defer' => 'defer'
]);

$this->extend('layout.html.php');
?>

<?php
if (!$app->getUser() instanceof UserInterface) {
    echo $this->areablock('my-pregnancy-today-index-area-block', []);
}
?>
<?php
if ($app->getUser() instanceof UserInterface) {
    echo $this->inc('/my-pregnancy-today/homelogin', []);
}
?>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/my-pregnancy-today-video-with-comment.js', 'text/javascript', [
    ])
?>
