<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource born.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 11/08/20
 * @time 11.55
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<!-- modal for is birth -->
<div class="modal isBirth p-0" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content isBirth__content">
            <div class="modal-body container isBirth__body container">
                <div class="isBirth__body-figure">
                    <img src="/assets/images/Reminder/reminder-contents.png" alt="reminder-contents">
                </div>
                <div class="isBirth__body-heading">
                    <p><?= $this->t('MY_PREGNANCY_TODAY_BABY_GIVE_A_BIRTH') ?></p>
                </div>
            </div>
            <div class="modal-footer isBirth__footer">
                <a href="<?= $this->url('my-pregnancy-today-greeting') ?>" type="button" class="btn isBirth__button mr-0">Sudah</a>
                <button type="button" class="btn isBirth__button ml-0" data-dismiss="modal">Belum</button>
            </div>
        </div>
    </div>
</div>
