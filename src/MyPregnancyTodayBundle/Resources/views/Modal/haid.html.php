<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource haid.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 11/08/20
 * @time 23.17
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<!--  modal for counting due date -->
<div class="modal due-date p-0" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content due-date__content">
            <div class="first-step">
                <div class="modal-header due-date__header">
                    <h5 class="modal-title">Due Date Calculator</h5>
                </div>
                <div class="modal-body container due-date__body container">
                    <div class="due-date__body-figure">
                        <img src="/assets/images/Reminder/reminder-contents.png" alt="reminder-contents">
                    </div>
                    <div class="due-date__body-calendar">
                        <label for="date">Hari pertama siklus haid terakhir</label>
                        <div class="due-date__body-calendar-wrapper">
                            <input type="text" class="form-control date shadow-none">
                            <img src="/assets/images/after-login/date-logo.png" alt="calendar-logo">
                            <div class="invalid-feedback">
                                Kalender Wajib Di isi
                            </div>
                        </div>
                    </div>
                    <div class="due-date__body-dropdown">
                        <label for="drop-down-trigger">Periode Siklus</label>
                        <div class="due-date__dropdown-wrapper">
                            <select class="form-control shadow-none" id="drop-down-trigger" class="due-date__body-dropdown-option">
                            </select>
                            <img src="/assets/images/after-login/arrow-down.png" alt="arrow-down">
                        </div>
                    </div>
                </div>
                <div class="modal-footer due-date__footer">
                    <button type="button" class="btn next-step">Hitung</button>
                </div>
            </div>
            <div class="second-step container">
                <div class="modal-header due-date__header">
                    <h5 class="modal-title">Pregnancy Calendar</h5>
                </div>
                <div class="modal-body container due-date__body container">
                    <div class="row">
                        <div class="col-6 second-step__item">
                            <h3 class="second-step__item-heading">Perkiraan Pembuahan</h3>
                            <p class="second-step__item-date conception">01 April 2020</p>
                        </div>
                        <div class="col-6 second-step__item">
                            <h3 class="second-step__item-heading ">Trimester Pertama</h3>
                            <p class="second-step__item-date trimester-one">18 Maret 2020</p>
                        </div>
                        <div class="col-6 second-step__item">
                            <h3 class="second-step__item-heading">Trimester Kedua</h3>
                            <p class="second-step__item-date trimester-two">21 Juni 2020</p>
                        </div>
                        <div class="col-6 second-step__item">
                            <h3 class="second-step__item-heading ">Trimester Ketiga</h3>
                            <p class="second-step__item-date trimester-three">21 Juni 2020</p>
                        </div>
                        <div class="col-12 second-step__item birth-result">
                            <h3 class="second-step__item-heading">Perkiraan Siap Dilahirkan</h3>
                            <div class="result">
                                <div class="result__text birthdate">
                                    <p>23 Desember 2020</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer due-date__footer">
                    <div class="due-date__footer-countback">
                        <button type="button" class="btn count-again">Hitung Ulang</button>
                    </div>
                    <div class="due-date__footer-visitprofile">
                        <a href="<?= $this->path('my-pregnancy-today-index') ?>" type="button" class="btn go-to-homepage" >Kunjungi Profile</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
