pimcore.registerNS("pimcore.plugin.MyPregnancyTodayBundle");

pimcore.plugin.MyPregnancyTodayBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.MyPregnancyTodayBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("MyPregnancyTodayBundle ready!");
    }
});

var MyPregnancyTodayBundlePlugin = new pimcore.plugin.MyPregnancyTodayBundle();
