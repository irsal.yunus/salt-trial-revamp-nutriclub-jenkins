<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource MyPregnancyTodayWeeklyInfo.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:34
 */

namespace MyPregnancyTodayBundle\Document\Areabrick;

use MyPregnancyTodayBundle\Service\Document\Areabrick\WeeklyInfoService;
use Pimcore\Model\Document\Tag\Area\Info;

class MyPregnancyTodayWeeklyInfo extends AbstractAreabrick
{
    private $weeklyInfoService;

    public function __construct(WeeklyInfoService $weeklyInfoService)
    {
        $this->weeklyInfoService = $weeklyInfoService;
    }

    public function action(Info $info)
    {
        $info->getView()->mamaShouldDo = $this->weeklyInfoService->getWeeklyInfoFromMamaShouldDo();
    }
}
