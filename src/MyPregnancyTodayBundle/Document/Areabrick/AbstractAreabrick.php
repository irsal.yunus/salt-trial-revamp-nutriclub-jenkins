<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 23/07/2020
 * Time: 12:49
 */

namespace MyPregnancyTodayBundle\Document\Areabrick;

use Pimcore\Extension\Document\Areabrick\AbstractTemplateAreabrick;
use Pimcore\Model\Document\Tag\Area\Info;

abstract class AbstractAreabrick extends AbstractTemplateAreabrick
{
    /**
     * Lazy programmer tips :) Widget name based on short class name.
     *
     * @return string|string[]|null
     * @throws \ReflectionException
     */
    public function getName()
    {
        return preg_replace('/(?<!\ )[A-Z]/', ' $0', (new \ReflectionClass($this))->getShortName());
    }

    public function getDescription()
    {
        return 'My Pregnancy Today Widget Collection';
    }

    /**
     * @return string
     */
    public function getTemplateSuffix()
    {
        return parent::TEMPLATE_SUFFIX_PHP;
    }

    /**
     * @return string
     */
    public function getTemplateLocation()
    {
        return parent::TEMPLATE_LOCATION_BUNDLE;
    }

    /**
     * @return bool
     */
    public function hasEditTemplate()
    {
        return true;
    }

    public function action(Info $info)
    {
        $info->view->prefixName = $info->getId();
    }
}
