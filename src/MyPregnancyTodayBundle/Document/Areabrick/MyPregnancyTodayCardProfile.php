<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource MyPregnancyTodayCardProfile.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:33
 */

namespace MyPregnancyTodayBundle\Document\Areabrick;

use AppBundle\Model\DataObject\Customer;
use MyPregnancyTodayBundle\Service\Document\Areabrick\CardProfileService;
use Pimcore\Model\Document\Tag\Area\Info;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class MyPregnancyTodayCardProfile extends AbstractAreabrick
{
    private $session;

    private $cardProfileService;

    public function __construct(SessionInterface $session, CardProfileService $cardProfileService)
    {
        $this->session = $session;
        $this->cardProfileService = $cardProfileService;
    }

    public function action(Info $info)
    {
        /** @var Customer $user */
        $user = $this->session->get('UserProfile');

        $info->getView()->user = $user;
        $info->getView()->userCurrentWeek = $this->cardProfileService->getCurrentWeek();
    }
}
