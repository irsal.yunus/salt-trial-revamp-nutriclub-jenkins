<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource MyPregnancyTodayFeatures.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Yulius Ardian Febrianto <yulius.febrianto@salt.co.id|yuliusardin@gmail.com>
 * @since 23/07/2020
 * @time 15:23
 */

namespace MyPregnancyTodayBundle\Document\Areabrick;

class MyPregnancyTodayFeatures extends AbstractAreabrick
{

}
