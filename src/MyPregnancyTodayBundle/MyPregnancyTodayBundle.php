<?php

namespace MyPregnancyTodayBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class MyPregnancyTodayBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/mypregnancytoday/js/pimcore/startup.js'
        ];
    }
}