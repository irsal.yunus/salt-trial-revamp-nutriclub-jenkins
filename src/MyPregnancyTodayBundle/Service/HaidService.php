<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource HaidService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 11/08/20
 * @time 20.45
 *
 */

namespace MyPregnancyTodayBundle\Service;

use AppBundle\Services\UtmService;
use MyPregnancyTodayBundle\Service\UserService;
use AppBundle\Tool\Folder;
use Carbon\Carbon;
use Pimcore\File;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\HaidCycle;
use Pimcore\Templating\Helper\HeadScript;
use Pimcore\Templating\Helper\Placeholder;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Templating\EngineInterface;
use AppBundle\Services\UserService as AppBundleUserService;

class HaidService
{
    private $request;

    private $userService;

    /** @var HeadScript $headScript */
    private $headScript;

    private $placeholder;

    /** @var EngineInterface $engineInterface */
    private $engineInterface;

    /** @var AppBundleUserService $appBundleUserService */
    private $appBundleUserService;

    private $utmService;

    public function __construct(
        RequestStack $requestStack,
        UserService $userService,
        HeadScript $headScript,
        Placeholder $placeholder,
        EngineInterface $engineInterface,
        AppBundleUserService $appBundleUserService,
        UtmService $utmService
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->userService = $userService;
        $this->headScript = $headScript;
        $this->placeholder = $placeholder;
        $this->engineInterface = $engineInterface;
        $this->appBundleUserService = $appBundleUserService;
        $this->utmService = $utmService;
    }

    public function setData()
    {
        $memberId = (int)$this->request->get('id');
        $name = $this->request->get('name');
        $email = $this->request->get('email');
        $conceptDate = $this->request->get('conceptDate');
        $trimesterOne = $this->request->get('trimesterOne');
        $trimesterTwo = $this->request->get('trimesterTwo');
        $trimesterThree = $this->request->get('trimesterThree');
        $birthDate = $this->request->get('birthDate');

        if (!is_int($memberId)) {
            $response['code'] = '401';
            $response['message'] = 'Wrong memberId';

            return $response;
        }

        $userProfile = $this->appBundleUserService->getUser();
        if (!$userProfile) {
            $response['code'] = '401';
            $response['message'] = 'Unauthorized';

            return $response;
        }

        if ($userProfile->getId() !== $memberId) {
            $response['code'] = '404';
            $response['message'] = 'Not Found.';

            return $response;
        }

        $haidCycle = HaidCycle::getByMemberId($memberId, 1);

        if ($haidCycle) {
            $response['code'] = '202';
            $response['message'] = 'Found';

            return $response;
        }

        $folderHaidCycle = Folder::checkAndCreate('HaidCycle');
        $folderHaidCycleId = Folder::checkAndCreate($memberId, $folderHaidCycle);

        $newHaidCycle = new HaidCycle();

        $newHaidCycle->setParent($folderHaidCycleId);
        $newHaidCycle->setKey(File::getValidFilename($memberId));

        $newHaidCycle->setMemberId($memberId);
        $newHaidCycle->setName($name);
        $newHaidCycle->setEmail($email);
        $newHaidCycle->setGender(strtolower($userProfile->getGender()) === 'm' ? 'male' : 'female');
        $newHaidCycle->setConceptDate(Carbon::createFromTimestamp($conceptDate));
        $newHaidCycle->setTrimesterOne(Carbon::createFromTimestamp($trimesterOne));
        $newHaidCycle->setTrimesterTwo(Carbon::createFromTimestamp($trimesterTwo));
        $newHaidCycle->setTrimesterThree(Carbon::createFromTimestamp($trimesterThree));
        $newHaidCycle->setBirthDate(Carbon::createFromTimestamp($birthDate));

        $utms = $this->utmService->getUtms();
        $utmsFilter = count(array_filter($utms));

        if ($utmsFilter > 0) {
            $newHaidCycle->setUtmSource($utms['utm_source'] ?? null);
            $newHaidCycle->setUtmMedium($utms['utm_medium'] ?? null);
            $newHaidCycle->setUtmCampaign($utms['utm_campaign'] ?? null);
            $newHaidCycle->setUtmTerm($utms['utm_term'] ?? null);
            $newHaidCycle->setUtmContent($utms['utm_content'] ?? null);
        }

        $newHaidCycle->setPublished(1);

        $response['code'] = '00';
        $response['message'] = 'Success';

        try {
            $newHaidCycle->save([
                'versionNote' => 'HAID_SERVICE_AUTO_CREATE'
            ]);
        } catch (\Exception $exception) {
            $response['code'] = '999';
            $response['message'] = $exception->getMessage();
            Simple::log('HAID_SERVICE_SET_DATA', $exception->getMessage());
        }

        return $response;
    }

    public function isFinishHaidPopup()
    {
        $haidCycle = $this->userService->getHaidCycleByCurrentUser();

        if (!$haidCycle) {
            return false;
        }

        return true;
    }

    public function injectPopup()
    {
        if ($this->isFinishHaidPopup()) {
            return null;
        }

        $haidPopup = $this->engineInterface->render(
            '@MyPregnancyTodayBundle/Resources/views/Modal/haid.html.php', [
        ]);

        $this->placeholder->__invoke('haidPopup')->set($haidPopup);
        $this->headScript->offsetSetFile(101, '/bundles/mypregnancytoday/js/front-end/haid.js');
    }
}
