<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ArticleService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 11/08/20
 * @time 14.30
 *
 */

namespace MyPregnancyTodayBundle\Service;

use AppBundle\Services\UserService;
use MyPregnancyTodayBundle\Model\DataObject\MyPregnancyTodayArticle;
use Pimcore\Model\DataObject\MyPregnancyTodayRelatedArticle;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleService
{
    /** @var UserService $userService */
    private $userService;

    /** @var \Symfony\Component\HttpFoundation\Request|null  */
    private $request;

    public function __construct(UserService $userService, RequestStack $requestStack)
    {
        $this->userService = $userService;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getRelatedArticle()
    {
        $userProfile = $this->userService->getUser();

        if (!$userProfile) {
            return null;
        }

        $agePregnant = $userProfile->getAgePregnant() < 1 ? 1 : $userProfile->getAgePregnant();

        if (!$agePregnant) {
            return null;
        }

        /** @var MyPregnancyTodayRelatedArticle $relatedArticle */
        $relatedArticle = MyPregnancyTodayRelatedArticle::getByWeeks(1, 1);

        if (!$relatedArticle) {
            return null;
        }

        return $relatedArticle;
    }

    public function getArticle()
    {
        $myPregnancyTodayArticleSlug = $this->request->get('articleSlug');

        if (!$myPregnancyTodayArticleSlug) {
            return null;
        }

        /** @var MyPregnancyTodayArticle $myPregnancyTodayArticleObj */
        $myPregnancyTodayArticleObj = MyPregnancyTodayArticle::getBySlug($myPregnancyTodayArticleSlug, 1);

        if (!$myPregnancyTodayArticleObj) {
            throw new NotFoundHttpException('Article not found');
        }

        return $myPregnancyTodayArticleObj;
    }
}
