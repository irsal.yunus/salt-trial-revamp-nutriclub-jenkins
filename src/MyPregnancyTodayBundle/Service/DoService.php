<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource DoService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 10/08/20
 * @time 06.34
 *
 */

namespace MyPregnancyTodayBundle\Service;

use AppBundle\Model\DataObject\Customer;
use Pimcore\Model\DataObject\MamaShouldDo;
use Pimcore\Model\DataObject\MamaShouldDoGroup;
use Symfony\Component\HttpFoundation\{Request, RequestStack, Session\SessionInterface};
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class DoService
{
    /** @var Request $request */
    private $request;

    private $session;

    private $router;

    /** @var MamaShouldDoGroup */
    private $currentMamaShouldDoGroup;

    private $userService;

    public function __construct(
        RequestStack $requestStack,
        SessionInterface $session,
        RouterInterface $router,
        UserService $userService
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->session = $session;
        $this->router = $router;
        $this->userService = $userService;
    }

    public function getMamaShouldDo()
    {
        $slug = $this->request->get('mamaShouldDoGroupSlug');

        if (!$slug) {
            throw new NotFoundHttpException('Page not found.');
        }

        /** @var MamaShouldDoGroup $mamaShouldDoGroup */
        $mamaShouldDoGroup = MamaShouldDoGroup::getBySlug($slug, 1);

        if (!$mamaShouldDoGroup->getMain()) {
            throw new NotFoundHttpException('Mama should do not found.');
        }

        $this->currentMamaShouldDoGroup = $mamaShouldDoGroup;

        return $mamaShouldDoGroup->getMain();
    }

    public function getMamaShouldDoPreviousTrimester()
    {
        $currentTrimesterNumber = (int)$this->currentMamaShouldDoGroup->getTrimesterNumber();
        $currentTrimesterNumber--;

        $mamaShouldDoGroup = MamaShouldDoGroup::getByTrimesterNumber($currentTrimesterNumber, 1);

        if (!$mamaShouldDoGroup) {
            return null;
        }

        return $mamaShouldDoGroup;
    }

    public function getMamaShouldDoCurrentTrimester()
    {
        $mamaShouldDo = $this->userService->getCurrentTrimester();
        if (!$mamaShouldDo) {
            throw new NotFoundHttpException('Mama should do not found');
        }

        return $mamaShouldDo;
    }

    public function getMamaShouldDoNextTrimester()
    {
        $currentTrimesterNumber = (int)$this->currentMamaShouldDoGroup->getTrimesterNumber();
        $currentTrimesterNumber++;

        $mamaShouldDoGroup = MamaShouldDoGroup::getByTrimesterNumber($currentTrimesterNumber, 1);

        if (!$mamaShouldDoGroup) {
            return null;
        }

        return $mamaShouldDoGroup;
    }

    public function getPreviousUrl()
    {
        $previousUrl = $this->getMamaShouldDoPreviousTrimester() ?
            $this->mamaShouldDoUrlBuilder($this->getMamaShouldDoPreviousTrimester()) : null;

        return $previousUrl;
    }

    public function getNextUrl()
    {
        $previousUrl = $this->getMamaShouldDoNextTrimester() ?
            $this->mamaShouldDoUrlBuilder($this->getMamaShouldDoNextTrimester()) : null;

        return $previousUrl;
    }

    public function mamaShouldDoUrlBuilder(MamaShouldDoGroup $mamaShouldDoGroup)
    {
        $url = $this->router->generate('MAMA_SHOULD_DO', [
            'mamaShouldDoGroupSlug' => $mamaShouldDoGroup->getSlug()
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        return $url;
    }
}
