<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource UserService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 13/08/20
 * @time 15.11
 *
 */

namespace MyPregnancyTodayBundle\Service;

use AppBundle\Services\UserService as BaseUserService;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Pimcore\Model\DataObject\HaidCycle;
use Pimcore\Model\DataObject\MamaShouldDo;
use Pimcore\Model\DataObject\MamaShouldDoGroup;
use Pimcore\Model\WebsiteSetting;

class UserService
{
    /** @var BaseUserService  */
    private $baseUserService;

    public function __construct(BaseUserService $baseUserService)
    {
        $this->baseUserService = $baseUserService;
    }

    public function getCurrentWeekFromObjectHaidCycle()
    {
        $userProfileRaw = $this->baseUserService->getUserRaw();
        /** @var HaidCycle $haidCycle */
        $haidCycle = $this->getHaidCycleByCurrentUser();

        if (!$haidCycle) {
            return null;
        }

        if ($haidCycleWeek = $userProfileRaw['haidCycleWeek']) {
            return $haidCycleWeek;
        }

        $start = Carbon::now();
        $end = $haidCycle->getBirthDate();

        $period = CarbonPeriod::create($start, '1 week', $end);
        $weekTotal = WebsiteSetting::getByName('MY_PREGNANCY_TODAY_TOTAL_WEEKS') ?
            WebsiteSetting::getByName('MY_PREGNANCY_TODAY_TOTAL_WEEKS')->getData() : 42;

        $preTotal = $weekTotal - count($period);
        $postTotal = $preTotal <= 0 ? 1 : $preTotal;

        $this->dataMerger($userProfileRaw, ['haidCycleWeek' => $postTotal]);

        return $postTotal;
    }

    public function getCurrentTrimester()
    {
        $currentWeek = $this->getCurrentWeekFromObjectHaidCycle();

        $mamaShouldDo = MamaShouldDo::getByWeeks($currentWeek, 1);

        if (!$mamaShouldDo) {
            return null;
        }

        $mamaShouldDoGroup = new MamaShouldDoGroup\Listing();
        $mamaShouldDoGroup
            ->setCondition('mamaShouldDo LIKE ?', [
                '%'. $mamaShouldDo->getId() . '%'
        ]);
        $mamaShouldDoGroup->setLimit(1);

        $calculate = $mamaShouldDoGroup->getCount() === 1 ? $mamaShouldDoGroup->getObjects()[0] : null;

        if ($calculate === null) {
            $calculate = MamaShouldDoGroup::getByTrimesterNumber(1, 1);
        }

        return $calculate;
    }

    public function getHaidCycleByCurrentUser()
    {
        $userProfileRaw = $this->baseUserService->getUserRaw();
        if (!$userProfileRaw) {
            return null;
        }

        if ($haidCycleObj = $userProfileRaw['haidCycleObj']) {
            return $haidCycleObj;
        }

        $memberId = $userProfileRaw['ID'];

        if (!$memberId) {
            return null;
        }

        /** @var HaidCycle $haidCycle */
        $haidCycle = HaidCycle::getByMemberId($memberId, 1);

        if (!$haidCycle) {
            return null;
        }

        $this->dataMerger($userProfileRaw, ['haidCycleObj' => $haidCycle]);

        return $haidCycle;
    }

    private function dataMerger(array $userProfileRaw, array $data)
    {
        $newUserProfileRaw = array_merge_recursive($userProfileRaw, $data);
        $this->baseUserService->setUserRaw($newUserProfileRaw);
    }
}
