<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource BornService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 11/08/20
 * @time 11.51
 *
 */

namespace MyPregnancyTodayBundle\Service;

use MyPregnancyTodayBundle\Service\UserService;
use Pimcore\Model\WebsiteSetting;
use Pimcore\Templating\Helper\{HeadScript, Placeholder};
use Symfony\Component\Templating\EngineInterface;

class BornService
{
    private $userService;

    /** @var HeadScript $headScript */
    private $headScript;

    private $placeholder;

    /** @var EngineInterface $engineInterface */
    private $engineInterface;

    public function __construct(
        UserService $userService,
        HeadScript $headScript,
        Placeholder $placeholder,
        EngineInterface $engineInterface
    ) {
        $this->userService = $userService;
        $this->headScript = $headScript;
        $this->placeholder = $placeholder;
        $this->engineInterface = $engineInterface;
    }

    public function isBorn()
    {
        $weekToAskIsBorn = WebsiteSetting::getByName('MY_PREGNANCY_TODAY_IS_BORN_WEEK_TO_REMIND') ?
            WebsiteSetting::getByName('MY_PREGNANCY_TODAY_IS_BORN_WEEK_TO_REMIND')->getData() : 37;

        return $this->userService->getCurrentWeekFromObjectHaidCycle() === (int)$weekToAskIsBorn;
    }

    public function injectPopup()
    {
        if (!$this->isBorn()) {
            return null;
        }

        $bornPopup = $this->engineInterface->render(
            '@MyPregnancyTodayBundle/Resources/views/Modal/born.html.php', [

        ]);
        $this->placeholder->__invoke('bornPopup')->set($bornPopup);
        $this->headScript->offsetSetFile(100, '/bundles/mypregnancytoday/js/front-end/born.js');
    }
}
