<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource KnowService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 10/08/20
 * @time 08.00
 *
 */

namespace MyPregnancyTodayBundle\Service;

use AppBundle\Model\DataObject\Customer;
use Pimcore\Model\DataObject\MamaShouldKnow;
use Pimcore\Model\DataObject\MamaShouldKnowGroup;
use Symfony\Component\HttpFoundation\{Request, RequestStack, Session\SessionInterface};
use Symfony\Component\Routing\{RouterInterface, Generator\UrlGeneratorInterface};
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class KnowService
{
    /** @var Request $request */
    private $request;

    private $session;

    private $router;

    private $currentMamaShouldKnowGroup;

    private $userService;

    public function __construct(
        RequestStack $requestStack,
        SessionInterface $session,
        RouterInterface $router,
        UserService $userService
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->session = $session;
        $this->router = $router;
        $this->userService = $userService;
    }

    public function getMamaShouldKnow()
    {
        $slug = $this->request->get('mamaShouldKnowGroupSlug');

        if (!$slug) {
            throw new NotFoundHttpException('Page not found.');
        }

        /** @var MamaShouldKnowGroup $mamaShouldKnowGroup */
        $mamaShouldKnowGroup = MamaShouldKnowGroup::getBySlug($slug, 1);

        if (!$mamaShouldKnowGroup->getMain()) {
            throw new NotFoundHttpException('Mama should know not found.');
        }

        $this->currentMamaShouldKnowGroup = $mamaShouldKnowGroup;

        return $mamaShouldKnowGroup->getMain();
    }

    public function getMamaShouldKnowPreviousTrimester()
    {
        $currentTrimesterNumber = (int)$this->currentMamaShouldKnowGroup->getTrimesterNumber();
        $currentTrimesterNumber--;

        $mamaShouldKnowGroup = MamaShouldKnowGroup::getByTrimesterNumber($currentTrimesterNumber, 1);

        if (!$mamaShouldKnowGroup) {
            return null;
        }

        return $mamaShouldKnowGroup;
    }

    public function getMamaShouldKnowCurrentTrimester()
    {
        $mamaShouldDoGroup =  $this->userService->getCurrentTrimester();
        $trimesterNumber = $mamaShouldDoGroup->getTrimesterNumber();

        $mamaShouldDoGroup = MamaShouldKnowGroup::getByTrimesterNumber($trimesterNumber, 1);

        if (!$mamaShouldDoGroup) {
            throw new NotFoundHttpException('Mama should know group not found.');
        }

        return $mamaShouldDoGroup;
    }

    public function getMamaShouldKnowNextTrimester()
    {
        $currentTrimesterNumber = (int)$this->currentMamaShouldKnowGroup->getTrimesterNumber();
        $currentTrimesterNumber++;

        $mamaShouldKnowGroup = MamaShouldKnowGroup::getByTrimesterNumber($currentTrimesterNumber, 1);

        if (!$mamaShouldKnowGroup) {
            return null;
        }

        return $mamaShouldKnowGroup;
    }

    public function getPreviousUrl()
    {
        $previousUrl = $this->getMamaShouldKnowPreviousTrimester() ?
            $this->mamaShouldKnowUrlBuilder($this->getMamaShouldKnowPreviousTrimester()) : null;

        return $previousUrl;
    }

    public function getNextUrl()
    {
        $previousUrl = $this->getMamaShouldKnowNextTrimester() ?
            $this->mamaShouldKnowUrlBuilder($this->getMamaShouldKnowNextTrimester()) : null;

        return $previousUrl;
    }

    public function mamaShouldKnowUrlBuilder(MamaShouldKnowGroup $mamaShouldKnowGroup)
    {
        $url = $this->router->generate('MAMA_SHOULD_KNOW', [
            'mamaShouldKnowGroupSlug' => $mamaShouldKnowGroup->getSlug()
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        return $url;
    }
}
