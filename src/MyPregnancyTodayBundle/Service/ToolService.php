<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ToolService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 10/08/20
 * @time 09.19
 *
 */

namespace MyPregnancyTodayBundle\Service;

use AppBundle\Model\DataObject\Customer;
use Pimcore\Model\DataObject\MyPregnancyTodayRelatedTools;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ToolService
{
    private $request;

    private $session;

    public function __construct(RequestStack $requestStack, SessionInterface $session)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->session = $session;
    }

    public function getRelatedTools()
    {
        /** @var Customer $userProfile */
        $userProfile = $this->session->get('UserProfile');

        if (!$userProfile) {
            return null;
        }

        $currentAgePregnant = $userProfile->getAgePregnant() ?: 1;

        $relatedTools = MyPregnancyTodayRelatedTools::getByWeeks(1, 1);

        if (!$relatedTools) {
            throw new NotFoundHttpException('Related Tools not found');
        }

        return $relatedTools;
    }
}
