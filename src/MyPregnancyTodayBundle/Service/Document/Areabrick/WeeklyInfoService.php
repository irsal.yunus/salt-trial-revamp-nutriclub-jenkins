<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource WeeklyInfoService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 11/08/20
 * @time 12.28
 *
 */

namespace MyPregnancyTodayBundle\Service\Document\Areabrick;

use MyPregnancyTodayBundle\Service\UserService;
use Pimcore\Model\DataObject\MamaShouldDo;

class WeeklyInfoService
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getWeeklyInfoFromMamaShouldDo()
    {
        $agePregnant = $this->userService->getCurrentWeekFromObjectHaidCycle();

        /** @var MamaShouldDo $mamaShouldDo */
        $mamaShouldDo = MamaShouldDo::getByWeeks($agePregnant, 1);

        return $mamaShouldDo;
    }
}
