<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource MyPregnancyTodayCardProfile.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 14/08/20
 * @time 08.56
 *
 */

namespace MyPregnancyTodayBundle\Service\Document\Areabrick;

use MyPregnancyTodayBundle\Service\UserService;

class CardProfileService
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getCurrentWeek()
    {
        return $this->userService->getCurrentWeekFromObjectHaidCycle();
    }
}
