<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RecipeService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 10/08/20
 * @time 10.17
 *
 */

namespace MyPregnancyTodayBundle\Service;

use AppBundle\Model\DataObject\Customer;
use MyPregnancyTodayBundle\Model\DataObject\MyPregnancyTodayRecipe;
use Pimcore\Model\DataObject\MyPregnancyTodayRecipeTips;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RecipeService
{
    private $request;

    private $session;

    public function __construct(RequestStack $requestStack, SessionInterface $session)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->session = $session;
    }

    public function getRecipeTips()
    {
        /** @var Customer $userProfile */
        $userProfile = $this->session->get('UserProfile');

        if (!$userProfile) {
            return null;
        }

        $agePregnant = $userProfile->getAgePregnant() ?: 1;

        /** @var MyPregnancyTodayRecipeTips $recipeTips */
        $recipeTips = MyPregnancyTodayRecipeTips::getByWeeks(1, 1);

        if (!$recipeTips) {
            throw new NotFoundHttpException('Recipe tips not found.');
        }

        return $recipeTips;
    }

    public function getRecipe()
    {
        $slug = $this->request->get('recipeSlug');

        if (!$slug) {
            return null;
        }

        /** @var MyPregnancyTodayRecipe $recipeObject */
        $recipeObject = MyPregnancyTodayRecipe::getBySlug($slug, 1);

        if (!$recipeObject) {
            throw new NotFoundHttpException('Recipe not found');
        }

        return $recipeObject;
    }
}
