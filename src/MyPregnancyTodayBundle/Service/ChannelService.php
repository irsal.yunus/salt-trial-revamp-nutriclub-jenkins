<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ChannelService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 14/08/20
 * @time 15.22
 *
 */

namespace MyPregnancyTodayBundle\Service;

use Carbon\Carbon;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ChannelService
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function setChannel()
    {
        $channelMyPregnancyToday = WebsiteSetting::getByName('MY_PREGNANCY_TODAY_CHANNEL_CODE') ?
            WebsiteSetting::getByName('MY_PREGNANCY_TODAY_CHANNEL_CODE')->getData() : 'my_pregnancy_today';
        $this->cookieCounter('channel', $channelMyPregnancyToday);
    }

    private function cookieCounter($cookieName, $value, $expired = false)
    {
        $dotNetDomain = env('WILDCARD_DOMAIN_FOR_DOT_NET', null);

        $expiredCookie = $expired ? 1 : Carbon::now()->addMinutes(15)->timestamp;

        setrawcookie(
            $cookieName,
            $value,
            $expiredCookie,
            '/',
            $dotNetDomain
        );
    }
}
