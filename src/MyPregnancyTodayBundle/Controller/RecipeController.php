<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RecipeController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 16.35
 *
 */

namespace MyPregnancyTodayBundle\Controller;

use MyPregnancyTodayBundle\Service\RecipeService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class RecipeController extends AbstractController
{
    /**
     * @param Request $request
     * @param RecipeService $recipeService
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(Request $request, RecipeService $recipeService)
    {
        $this->view->recipeTips = $recipeService->getRecipeTips();
    }

    /**
     * @param Request $request
     * @param RecipeService $recipeService
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function detailAction(Request $request, RecipeService $recipeService)
    {
        $this->view->recipe = $recipeService->getRecipe();
    }
}
