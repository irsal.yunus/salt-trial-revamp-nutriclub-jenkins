<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ArticleController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 16.34
 *
 */

namespace MyPregnancyTodayBundle\Controller;

use MyPregnancyTodayBundle\Service\{ArticleService, DoService};
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ArticleController extends AbstractController
{
    /**
     * @param ArticleService $articleService
     * @return void
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(ArticleService $articleService)
    {
        $this->view->relatedArticle = $articleService->getRelatedArticle();
    }
}
