<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Know.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 17.08
 *
 */

namespace MyPregnancyTodayBundle\Controller;

use MyPregnancyTodayBundle\Service\KnowService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class KnowController extends AbstractController
{
    /**
     * @param Request $request
     * @param KnowService $knowService
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return RedirectResponse
     */
    public function indexAction(Request $request, KnowService $knowService)
    {
        return $this->redirect($knowService->mamaShouldKnowUrlBuilder(
            $knowService->getMamaShouldKnowCurrentTrimester()
        ), 301);
    }

    /**
     * @param Request $request
     * @param KnowService $knowService
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function detailAction(Request $request, KnowService $knowService)
    {
        $this->view->mamaShouldKnow = $knowService->getMamaShouldKnow();
        $this->view->previousMamaShouldKnow = $knowService->getPreviousUrl();
        $this->view->nextMamaShouldKnow = $knowService->getNextUrl();
    }
}
