<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ToolController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 19.18
 *
 */

namespace MyPregnancyTodayBundle\Controller;

use MyPregnancyTodayBundle\Service\ToolService;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ToolController extends AbstractController
{
    /**
     * @param Request $request
     * @param ToolService $toolService
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(Request $request, ToolService $toolService)
    {
        $this->view->relatedTools = $toolService->getRelatedTools();
    }
}
