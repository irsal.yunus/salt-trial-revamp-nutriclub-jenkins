<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource JoinNowController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 11/08/20
 * @time 22.20
 *
 */

namespace MyPregnancyTodayBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class JoinController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @Route("/join-now", name="my-pregnancy-today-join")
     *
     * @return RedirectResponse
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser() instanceof UserInterface) {
            return $this->redirect($this->generateUrl('my-pregnancy-today-index'));
        }
    }
}
