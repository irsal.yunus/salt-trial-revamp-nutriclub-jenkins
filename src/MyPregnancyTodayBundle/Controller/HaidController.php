<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource MensController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 11/08/20
 * @time 20.43
 *
 */

namespace MyPregnancyTodayBundle\Controller;

use MyPregnancyTodayBundle\Service\HaidService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class HaidController extends AbstractController
{
    /**
     * @param HaidService $haidService
     *
     * @Route("/haid/cycle", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function cycle(HaidService $haidService)
    {
        return $this->json($haidService->setData(), 200);
    }
}
