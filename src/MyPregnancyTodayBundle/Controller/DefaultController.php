<?php

namespace MyPregnancyTodayBundle\Controller;

use MyPregnancyTodayBundle\Service\BornService;
use MyPregnancyTodayBundle\Service\HaidService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Templating\EngineInterface;

class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @Route("/", name="my-pregnancy-today-index")
     */
    public function indexAction(Request $request)
    {

    }

    /**
     * @param Request $request
     *
     * @Route("/reminder")
     */
    public function reminderAction(Request $request)
    {

    }

    /**
     * @param BornService $bornService
     * @param HaidService $haidService
     *
     * @Route("/homelogin")
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function homeloginAction(BornService $bornService, HaidService $haidService)
    {
        $bornService->injectPopup();
        $haidService->injectPopup();
    }

    /**
     * @param Request $request
     *
     * @Route("/greeting", name="my-pregnancy-today-greeting")
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function greetingAction(Request $request)
    {
        $this->view->greeting = true;
    }
}
