<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 20/07/2020
 * Time: 15:55
 */

namespace MyPregnancyTodayBundle\Controller;

use AppBundle\Controller\AbstractController as AppBundleAbstractController;
use MyPregnancyTodayBundle\Service\ChannelService;

abstract class AbstractController extends AppBundleAbstractController
{
    public function __construct(ChannelService $channelService)
    {
        $channelService->setChannel();
    }
}
