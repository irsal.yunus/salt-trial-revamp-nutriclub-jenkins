<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource DoController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 29/07/20
 * @time 17.08
 *
 */

namespace MyPregnancyTodayBundle\Controller;

use MyPregnancyTodayBundle\Service\DoService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DoController extends AbstractController
{
    /**
     * @param Request $request
     * @param DoService $doService
     * @return RedirectResponse
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(Request $request, DoService $doService)
    {
        return $this->redirect(
            $doService->mamaShouldDoUrlBuilder($doService->getMamaShouldDoCurrentTrimester()),
            301
        );
    }

    /**
     * @param Request $request
     * @param DoService $doService
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function detailAction(Request $request, DoService $doService)
    {
        $this->view->mamaShouldDo = $doService->getMamaShouldDo();
        $this->view->previousMamaShouldDo = $doService->getPreviousUrl();
        $this->view->nextMamaShouldDo = $doService->getNextUrl();
    }
}
