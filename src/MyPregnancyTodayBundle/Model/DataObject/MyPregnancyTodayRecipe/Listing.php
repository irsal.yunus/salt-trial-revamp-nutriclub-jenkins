<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Listing.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 10/08/20
 * @time 12.28
 *
 */

namespace MyPregnancyTodayBundle\Model\DataObject\MyPregnancyTodayRecipe;

use Pimcore\Model\DataObject\MyPregnancyTodayRecipe\Listing as BaseMyPregnancyTodayRecipeListing;

class Listing extends BaseMyPregnancyTodayRecipeListing
{

}
