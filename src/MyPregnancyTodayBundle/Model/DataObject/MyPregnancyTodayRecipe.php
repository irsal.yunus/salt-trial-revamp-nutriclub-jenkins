<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource MyPregnancyTodayRecipe.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 10/08/20
 * @time 12.23
 *
 */

namespace MyPregnancyTodayBundle\Model\DataObject;

use AppBundle\Model\RouterInterface;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\MyPregnancyTodayRecipe as BaseMyPregnancyTodayRecipe;
use Pimcore\Tool;

class MyPregnancyTodayRecipe extends BaseMyPregnancyTodayRecipe implements RouterInterface
{
    public function getRouterParams(): array
    {
        return [
            'recipeSlug' => $this->getSlug()
        ];
    }

    public function getRouter(): string
    {
        $url = '';

        try {
            $classDefinition = ClassDefinition::getById($this->getClassId());

            $linkGenerator = $classDefinition->getLinkGenerator() ?
                $classDefinition->getLinkGenerator()->generate($this) : '';

            $protocol = getenv('HTTP_PROTOCOL', 'https');
            $url = Tool::getHostUrl($protocol) . $linkGenerator;
        } catch (\Exception $e) {
            dd($e->getMessage());
        } finally {
            return $url;
        }
    }
}
