<?php

namespace SimplePageBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class SimplePageBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/simplepage/js/pimcore/startup.js'
        ];
    }
}