<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource AbstractController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.39
 *
 */

namespace SimplePageBundle\Controller;

use AppBundle\Controller\AbstractController as AppBundleAbstractController;

abstract class AbstractController extends AppBundleAbstractController
{

}
