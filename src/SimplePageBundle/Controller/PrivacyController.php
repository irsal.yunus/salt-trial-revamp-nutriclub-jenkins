<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource PrivacyController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.47
 *
 */

namespace SimplePageBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class PrivacyController
{
    public function indexAction(Request $request)
    {

    }
}
