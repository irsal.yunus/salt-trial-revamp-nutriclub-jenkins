pimcore.registerNS("pimcore.plugin.SimplePageBundle");

pimcore.plugin.SimplePageBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.SimplePageBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("SimplePageBundle ready!");
    }
});

var SimplePageBundlePlugin = new pimcore.plugin.SimplePageBundle();
