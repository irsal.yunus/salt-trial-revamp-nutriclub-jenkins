<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/about.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>

<div class="about">
    <div class="about__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="about__heading">
                        <h1>Tentang Nutriclub</h1>
                        <hr>
                    </div>
                    <div class="about__description">
                        <p>
                            Nutriclub adalah layanan dari Nutricia yang berdedikasi untuk mendampingi para Mama Indonesia dalam perjalanan kehamilan dan tumbuh kembang bayi dan balita. Nutricia memahami bahwa hadirnya seorang bayi adalah pengalaman yang menakjubkan namun penuh dengan pertanyaan dan tantangan, terlebih bagi mama-mama baru. Nutriclub selalu siap untuk menjawab pertanyaan yang Ibu miliki seputar kehamilan dan tumbuh kembang bayi dan balita. Tim Ahli Kami akan dengan senang hati menjawab pertanyaan-pertanyaan yang pasti mengisi pikiran kebanyakan ibu.
                        </p>
                        <p>
                            Nutriclub juga menghadirkan program-program yang tujuannya untuk mempermudah para mama untuk mendapatkan informasi tentang imunitas, nutrisi, tumbuh kembang si buah hati serta memberikan layanan istimewa lainnya untuk mama maupun si Kecil. Program Nutriclub dibuat untuk mendampingi dan membantu para mama memberikan yang terbaik di masa kehamilan dan masa pertumbuhan emas balitanya.
                        </p>
                        <p>
                            Melalui, Nutriclub menampilkan ratusan artikel tentang berbagai aspek pertumbuhan maupun nutrisi yang bisa diakses oleh semua mama. Bersama dengan Nutriclub Careline di nomor telpon bebas pulsa Nutricia dan Whatsapp Nutriclub, kami memberikan layanan untuk melakukan konsultasi langsung dengan Tim Ahli Nutriclub.

                            Lebih jauh tentang Nutricia, bisa klik di www.nutricia.co.id
                        </p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card" id="side-card">
                        <div class="row no-gutter">
                            <div class="col-12">
                                <img src="/assets/images/faq/three-doctor.png" alt="three doctors" class="img-fluid">
                            </div>
                            <div class="col-12">
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3>Konsultasi Langsung dengang Dokter dan Tim Ahli Kami</h3>
                                    </div>
                                    <div class="card-text">
                                        <p>
                                            Dapatkan jawaban dari pertanyaan seputar tumbuh kembang Mama dan si Kecil.
                                        </p>
                                    </div>
                                    <a href="#" class="btn">TANYA DOKTER</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>
