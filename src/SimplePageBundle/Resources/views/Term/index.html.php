<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/tac.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>
<div class="tac">
    <div class="tac__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="tac__heading">
                        <h1>Term & Condition Nutriclub Indonesia</h1>
                        <hr>
                    </div>
                    <div class="tac__description">
                        <p>
                            Dengan mengakses Nutriclub.co.id, anda dianggap telah menyetujui syarat dan ketentuan serta batasan hokum yang ditentukan untuk website ini. Syarat, ketentuan dan batasan-batasa tersebut akan terus menerus dimodifikasi sesuai perkembangan website.
                        </p>
                    </div>
                    <div class="tac__menu container">
                        <ol>
                            <li>
                                <a href="#penggunaan-website">
                                    Penggunaan Website
                                </a>
                            </li>
                            <li>
                                <a href="#hak-hak">
                                    Hak Hak
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Penggunaan Isi, Mengutip dan berbagi
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Ijin Untuk Memproduksi isi
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Batasan Tanggung Jawab Dan Ganti Rugi
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Perubahan Terhadap Ketentuan Ini
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Layanan Nutriclub membership
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    MyNutriclub
                                </a>
                            </li>
                        </ol>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="penggunaan-website">
                            <h3>Penggunaan Website</h3>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                Di Danone kami berkomitmen untuk melindungi hak Anda untuk privasi. Kami bertujuan untuk melindungi data pribadi apa pun yang kami miliki, untuk mengelola data pribadi Anda secara bertanggung jawab dan transparan dalam praktik kami. Kepercayaan anda penting bagi kami. Karena itu kami berkomitmen pada prinsip-prinsip dasar berikut:
                            </p>
                        </div>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="hak-hak">
                            <h3>Hak Hak</h3>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                Semua hak kekayaan intelektual di dalam website ini, termasuk kekayaan intelektual dalam bentuk teks, foto, grafik, materi audio, perangkat lunak maupun bentuk lainnya merupakan milik PT Nutricia Sejahtera Indonesia atau pemegang lisensinya. Semua hak tersebut dengan ini dilindungi. Semua konten di dalam website ini dapat diakses oleh konsumen sesuai syarat dan ketentuan yang berlaku untuk para anggota (member) dan non anggota (non-member) Nutriclub Indonesia.
                            </p>
                        </div>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="share">
                            <h3>PENGGUNAAN ISI, MENGUTIP DAN BERBAGI</h3>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                Website ini dan seluruh konten di dalamnya hanya boleh digunakan untuk keperluan pribadi konsumen, bukan untuk keperluan komersil. Konsumen individual diperkenankan untuk mengutip sebagian dari konten yang kami sediakan di website dan media sosial Nutriclub Indonesia sepanjang mencantumkan sumber halaman konten di website kami, yaitu menyisipkan back-link atau tautan balik ke halaman spesifik (url) yang dikutip. Kami juga mendorong konsumen untuk menggunakan fitur sharing ke media sosial yang kami sediakan di setiap halaman konten kami untuk berbagi informasi terpercaya dengan publik.
                            </p>
                        </div>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="share">
                            <h3>BATASAN TANGGUNG JAWAB DAN GANTI RUGI</h3>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                Jika konsumen ingin mempublikasikan ulang, meredistribusi atau mengeksploitasi isi website ini dengan cara tertentu, untuk keperluan komersil, konsumen dapat meminta ijin tertulisan terlebih dahulu dari PT Nutrica Indonesia Sejahtera. PT Nutricia Indonesia Sejahtera tidak memberikan jaminan bahwa ijin dapat diberikan.
                            </p>
                        </div>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="share">
                            <h3>BATASAN TANGGUNG JAWAB DAN GANTI RUGI</h3>
                            <h4>5.1 Konten Editorial</h4>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                Selain hasil riset kami, informasi, data, foto, video, dan artikel yang dimuat di dalam website ini dapat juga berasal dari berbagai macam sumber yang terpercaya dan tervalidasi oleh Tim Ahli Nutriclub. Khusus untuk tautan dari luar, konsumen diharapkan membaca dan memanfaatkan informasi yang ada di dalamnya secara bijaksana. Bilamana konsumen melakukan kontak atau hubungan dengan pihak ketiga yang namanya tercantum dalam website kami adalah tanggung jawab konsumen pribadi. Setiap bagian dari website ini hanya dapat digunakan untuk keperluan pribadi dan informasi bagi konsumen secara umum dan bukan untuk rekomendasi atau saran yang spesifik dan pasti. Sebelum mengambil suatu tindakan atau keputusan berdasarkan seluruh atau sebagian dari isi website ini, konsumen harus selalui melakukan pengecekan pribadi secara independen terhadap informan yang dianggap penting untuk menjadi dasar pengampilan keputusan atau tindakan, seperti medis, investasi, keuangan, pendidik, dan masalah-masalah lain yang membutuhkan saran professional. PT Nutricia Indonesia tidak dapat dimintai pertanggung jawaban atas klaim, kerugian, cedera, denda, kerusakan, biaya atau pengeluaran yang timbul sebagai akibat pemanfaatan atau ketidak mampuan untuk memanfaatkan sebagian atau seluruh isi website kami, atau pengabaian suatu tindakan yang seharusnya diambil, sebagai hasil dari penggunaan konten. 5.2 User Generated Content (UGC) Jika konsumen mengunggah suatu materi ke dalam website ini (teks berupa data diri, pertanyaan atau komentar, grafik, foto, gambar, video atau audio) dengan demikian konsumen telah setuju untuk memberikan hak atau lisensi, sub-lisensi, bebas royalty, non-eksklusif dan hak untuk memberikan materia ini kepada PT Nutricia Indonesia sejahtera. PT Nutricia Indonesia Sejahtea dapat menggunakan, mereproduksi, memodifikasi, mengadaptasi, mempublikasi, menerjemahkan maupun menciptakan karya baru dan materi yang konsumen unggah. PT Nutricia Indonesia Sejahtera juga berhak untuk mendistribusikan, menampilkan, memunculkan materi tersebut dengan ketentuan hak cipta dan hak publikasi pada Nutriclub Indonesia. Dengan memberikan data diri konsumen ke dalam website ini, konsumen memberikan persetujuan kepada PT Nutricia Indonesia Sejahtera untuk:
                            </p>
                        </div>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="share">
                            <h4>5.2 User Generated Content (UGC)</h4>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                Selain hasil riset kami, informasi, data, foto, video, dan artikel yang dimuat di dalam website ini dapat juga berasal dari berbagai macam sumber yang terpercaya dan tervalidasi oleh Tim Ahli Nutriclub. Khusus untuk tautan dari luar, konsumen diharapkan membaca dan memanfaatkan informasi yang ada di dalamnya secara bijaksana. Bilamana konsumen melakukan kontak atau hubungan dengan pihak ketiga yang namanya tercantum dalam website kami adalah tanggung jawab konsumen pribadi. Setiap bagian dari website ini hanya dapat digunakan untuk keperluan pribadi dan informasi bagi konsumen secara umum dan bukan untuk rekomendasi atau saran yang spesifik dan pasti. Sebelum mengambil suatu tindakan atau keputusan berdasarkan seluruh atau sebagian dari isi website ini, konsumen harus selalui melakukan pengecekan pribadi secara independen terhadap informan yang dianggap penting untuk menjadi dasar pengampilan keputusan atau tindakan, seperti medis, investasi, keuangan, pendidik, dan masalah-masalah lain yang membutuhkan saran professional. PT Nutricia Indonesia tidak dapat dimintai pertanggung jawaban atas klaim, kerugian, cedera, denda, kerusakan, biaya atau pengeluaran yang timbul sebagai akibat pemanfaatan atau ketidak mampuan untuk memanfaatkan sebagian atau seluruh isi website kami, atau pengabaian suatu tindakan yang seharusnya diambil, sebagai hasil dari penggunaan konten.
                                Jika konsumen mengunggah suatu materi ke dalam website ini (teks berupa data diri, pertanyaan atau komentar, grafik, foto, gambar, video atau audio) dengan demikian konsumen telah setuju untuk memberikan hak atau lisensi, sub-lisensi, bebas royalty, non-eksklusif dan hak untuk memberikan materia ini kepada PT Nutricia Indonesia sejahtera. PT Nutricia Indonesia Sejahtea dapat menggunakan, mereproduksi, memodifikasi, mengadaptasi, mempublikasi, menerjemahkan maupun menciptakan karya baru dan materi yang konsumen unggah. PT Nutricia Indonesia Sejahtera juga berhak untuk mendistribusikan, menampilkan, memunculkan materi tersebut dengan ketentuan hak cipta dan hak publikasi pada Nutriclub Indonesia. Dengan memberikan data diri konsumen ke dalam website ini, konsumen memberikan persetujuan kepada PT Nutricia Indonesia Sejahtera untuk:
                            </p>
                        </div>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="share">
                            <h4>5.3 Promosi, Kompetisi, Penarikan Undian</h4>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                PT Nutricia Sejahtera Indonesia melalui website www.nutriclub.co.id ini mungkin dalam sewaktu-waktu tertentu akan menyelenggarakan aktifitas Promo, Kompetisi, Penarikan Undian, baik atas inisiatif sendiri maupun bekerjasama dengan rekanan kompersiil. Ketententuan dan persyaratan tambahan akan disediakan pada waktu promosi, kompetisi dan penarikan undian tersebut diselenggarakan.
                            </p>
                        </div>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="share">
                            <h4>5.4 PERUBAHAN TERHADAP KETENTUAN INI</h4>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                Jika konsumen ingin mempublikasikan ulang, meredistribusi atau mengeksploitasi isi website ini dengan cara tertentu, untuk keperluan komersil, konsumen dapat meminta ijin tertulisan terlebih dahulu dari PT Nutrica Indonesia Sejahtera. PT Nutricia Indonesia Sejahtera tidak memberikan jaminan bahwa ijin dapat diberikan.
                            </p>
                        </div>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="share">
                            <h4>LAYANAN NUTRICLUB MEMBERSHIP</h4>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                Nutriclub Membership merupakan layanan keanggotaan yang tersedia di dalam Nutriclub website, sebagai wujud komitmen Nutriclub untuk mendampingi konsumen dalam perjalanan kehamilan serta tumbuh kembang anak sehingga konsumen dapat menjadi selangkah lebih maju (One Step Ahead) melalui benefit utama. 7.1 Syarat dan Ketentuak Keanggotaan Nutriclub (Nutriclub Membership) Nutriclub Membership / Keanggotaan Nutriclub dapat diikuti oleh seluruh Warga Negara Indonesia. Untuk mendaftarkan diri menjadi anggota Nutriclub, konsumen dapat mencari menu “membership” pada halaman nutriclub.co.id atau mengakses langsung melalui www.nutriclub.co.id/membership Untuk menjadi anggota, konsumen diwajibkan untuk mengisi data diri berupa Nama Lengkap, Email, No. Ponsel, Jenis Kelamin, Tanggal Lahir yang sebenar-benarnya, serta menentukan password yang diinginkan. Selain itu konsumen juga diharuskan untuk melengkapi data tambahan yaitu status tahapan Ibu (status kehamilan dan atau data diri anak) untuk dapat menikmati benefit Nutriclub Memebership secara lengkap. Dengan memberikan data diri konsumen ke dalam website dan atau melalui layanan keanggotaan Nutriclub, konsumen memberikan persetujuan kepada PT Nutricia Indonesia Sejahtera untuk:
                            </p>
                            <ul>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium corporis ratione necessitatibus excepturi veniam est illum, reprehenderit aliquid, eos neque aut velit qui omnis at vel inventore ea sed similique!
                                </li>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, facere mollitia maiores ipsum id dolores aut enim explicabo recusandae atque incidunt. Aperiam nostrum totam nisi eos soluta! Ut, voluptatem eum!
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="share">
                            <h4>
                                7.2 Benefit Nutriclub Membership
                            </h4>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                Dengan menjadi anggota Nutriclub, konsumen berhak mendapatkan benefit di bawah ini:
                            </p>
                            <ul>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium corporis ratione necessitatibus excepturi veniam est illum, reprehenderit aliquid, eos neque aut velit qui omnis at vel inventore ea sed similique!
                                </li>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, facere mollitia maiores ipsum id dolores aut enim explicabo recusandae atque incidunt. Aperiam nostrum totam nisi eos soluta! Ut, voluptatem eum!
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tac__subheading">
                        <div class="tac__subheading-header" id="share">
                            <h3>
                                ROYAL REWADS
                            </h3>
                        </div>
                        <div class="tac__subheading-description">
                            <p>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dicta, cumque, dolorem perspiciatis ratione optio ipsum minima sunt cupiditate ea repellat similique in voluptatum reiciendis explicabo! Provident aut mollitia eaque soluta.
                            </p>
                            <ul>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium corporis ratione necessitatibus excepturi veniam est illum, reprehenderit aliquid, eos neque aut velit qui omnis at vel inventore ea sed similique!
                                </li>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, facere mollitia maiores ipsum id dolores aut enim explicabo recusandae atque incidunt. Aperiam nostrum totam nisi eos soluta! Ut, voluptatem eum!
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card" id="side-card">
                        <div class="row no-gutter">
                            <div class="col-12">
                                <img src="/assets/images/faq/three-doctor.png" alt="three doctors" class="img-fluid">
                            </div>
                            <div class="col-12">
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3>Konsultasi Langsung dengang Dokter dan Tim Ahli Kami</h3>
                                    </div>
                                    <div class="card-text">
                                        <p>
                                            Dapatkan jawaban dari pertanyaan seputar tumbuh kembang Mama dan si Kecil.
                                        </p>
                                    </div>
                                    <a href="#" class="btn">TANYA DOKTER</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>

