<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/expert.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>
?>

<div class="expert-team">
    <div class="expert-team__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-10 offset-md-1">
                    <div class="row">
                        <div class="col-12 col-md-8 offset-md-2">
                            <?php
                            $this->headScript()
                                ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
                            ?>
                            <div class="expert-team__heading">
                                <h1>Tim Ahli Nutriclub</h1>
                                <hr>
                            </div>
                            <div class="expert-team__description">
                                <p>Kami siap melayani 24 jam sehari, 7 hari seminggu untuk mendukung Mama menjalankan peran pentingnya dalam merencanakan, mendukung, dan menjaga tumbuh kembang Ibu dan Si Kecil agar tetap optimal.</p>
                            </div>
                        </div>
                    </div>
                    <div class="expert-team__lists">
                        <div class="row">
                            <div class="expert-team__lists-item col-12 col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <h3>Nutriclub Careline</h3>
                                    </div>
                                    <div class="card-body">
                                        <p class="card-text">Tim berpengalaman dengan latar belakang pengetahuan nutrisi yang memahami Ibu dalam membesarkan si Kecil.</p>
                                        <a href="#" class="card-text__link text-blue">Selengkapnya ></a>
                                    </div>
                                </div>
                            </div>
                            <div class="expert-team__lists-item col-12 col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <h3>Tim Dokter</h3>
                                    </div>
                                    <div class="card-body">
                                        <p class="card-text">
                                            Para dokter spesialis dalam berbagai bidang seputar nutrisi, tumbuh kembang anak, serta perawatan ibu dan bayi.
                                        </p>
                                        <a href="#" class="card-text__link text-blue">Selengkapnya ></a>
                                    </div>
                                </div>
                            </div>
                            <div class="expert-team__lists-item col-12 col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <h3>Nutricia Research</h3>
                                    </div>
                                    <div class="card-body">
                                        <p class="card-text">
                                            Nutricia Research mempekerjakan lebih dari 1.400 karyawan dari 20 negara dalam memahami gizi tahap awal kehidupan.
                                        </p>
                                        <a href="#" class="card-text__link text-blue">Selengkapnya ></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>
