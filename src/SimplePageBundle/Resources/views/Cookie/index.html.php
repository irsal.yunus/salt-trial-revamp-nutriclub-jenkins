<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/cookie.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>

<div class="cookie">
    <div class="cookie__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="cookie__heading">
                        <h1>Pernyataan Cookie</h1>
                        <hr>
                    </div>
                    <div class="cookie__description">
                        <p>
                            Di Danone, kami ingin terbuka dan transparan dalam cara kami menggunakan cookie dan apa artinya ini bagi Anda. Beberapa cookie yang kami gunakan sangat penting untuk fungsionalitas situs web kami, tetapi ada juga cookie yang dapat Anda pilih atau blokir.
                        </p>
                        <p>
                            Kami berusaha untuk memberikan Anda informasi yang cukup tentang cookie yang kami gunakan sehingga Anda dapat membuat pilihan berdasarkan informasi yang Anda bagikan kepada kami.
                        </p>
                        <p>
                            Jika Anda memiliki pertanyaan atau komentar lebih lanjut, jangan ragu untuk menghubungi kami melalui halaman
                            <a href="#">
                                kontak kami
                            </a>.
                        </p>
                    </div>
                    <div class="cookie__menu container">
                        <ul>
                            <li>
                                <a href="#penggunaan-website">
                                    Apa itu Cookie ?
                                </a>
                            </li>
                            <li>
                                <a href="#hak-hak">
                                    Siapa yang mengumpulkan?
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Mengapa kami menggunakan cookie?
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Cookie yang sangat diperlukan
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Analisis dan cookie terkait kinerja
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Cookie untuk personalisasi
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Cookie pihak ketiga
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Preferensi Anda
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Bagaimana cara menghubungi kami
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="cookie__subheading">
                        <div class="cookie__subheading-header" id="hak-hak">
                            <h3>Apa itu Cookie ?</h3>
                        </div>
                        <div class="cookie__subheading-description">
                            <p>
                                Cookie adalah file kecil yang disimpan di komputer atau perangkat seluler Anda. Ini memiliki ID unik yang ditetapkan untuk perangkat Anda dan memungkinkan situs web untuk mengingat tindakan dan  Preferensi Anda. (seperti lokasi, bahasa, ukuran font, dan preferensi tampilan lainnya) selama periode waktu tertentu. Dengan begitu, Anda tidak harus terus memasukkan kembali informasi kapan pun Anda kembali ke situs web atau menjelajah dari satu halaman ke halaman lainnya. Cookie juga dapat membantu menyesuaikan pengalaman penelusuran Anda.
                            </p>
                            <p>
                                Tidak semua cookie berisi informasi pribadi tetapi jika ada, kami akan memperlakukan informasi ini sebagai data pribadi sesuai dengan Pernyataan Privasi kami. Kami juga memperlakukan informasi cookie sebagai data pribadi yang terkait dengan akun Anda atau informasi kontak lainnya.
                            </p>
                        </div>
                    </div>
                    <div class="cookie__subheading">
                        <div class="cookie__subheading-header" id="share">
                            <h3>Jenis Cookie</h3>
                        </div>
                        <div class="cookie__subheading-description">
                            <p>
                                Ada dua kategori besar cookie: Cookie persisten dan Cookie sesi. Cookie tetap ada di perangkat Anda hingga dihapus secara manual atau otomatis. Cookie sesi tetap ada di perangkat Anda hingga Anda menutup browser dan dengan demikian secara otomatis dihapus.
                            </p>
                        </div>
                    </div>
                    <div class="cookie__subheading">
                        <div class="cookie__subheading-header" id="share">
                            <h4>Mengapa kami menggunakan cookie?</h4>
                        </div>
                        <div class="cookie__subheading-description">
                            <p>
                                Secara umum, ada beberapa alasan mengapa kami dapat menggunakan cookie:
                            </p>
                            <ul>
                                <li>
                                    Cookie yang sangat diperlukan agar situs web berfungsi dan memungkinkan Anda untuk menggunakan layanan yang kami sediakan;
                                </li>
                                <li>
                                    Cookie yang mengumpulkan data terkait penggunaan situs web kami dan kinerjanya. Kami menggunakan informasi ini untuk meningkatkan produk dan layanan online kami;
                                </li>
                                <li>
                                    Cookie yang kami gunakan untuk mempersonalisasi dan mengoptimalkan pengalaman Anda di situs web kami. Cookies ini menyimpan informasi tentang pilihan Anda seperti ukuran teks, bahasa dan resolusi layar.
                                </li>
                                <li>
                                    Cookie yang ditempatkan oleh pihak ketiga untuk layanan yang ditingkatkan (seperti cookie media sosial) atau untuk tujuan iklan
                                </li>
                                <li>
                                    Silakan lihat di bawah untuk informasi lebih lanjut tentang jenis cookie yang kami gunakan.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="cookie__subheading">
                        <div class="cookie__subheading-header" id="share">
                            <h4>Cookie yang sangat diperlukan</h4>
                        </div>
                        <div class="cookie__subheading-description">
                            <p>
                                Ini adalah cookie yang kami butuhkan untuk dapat menawarkan kepada Anda situs web yang berfungsi dengan baik dan yang memungkinkan Anda untuk menggunakan layanan yang kami sediakan. Jika cookie ini diblokir, Anda tidak akan dapat menggunakan situs web kami.
                            </p>
                        </div>
                    </div>
                    <div class="cookie__subheading">
                        <div class="cookie__subheading-header" id="share">
                            <h4>Analisis dan cookie terkait kinerja</h4>
                        </div>
                        <div class="cookie__subheading-description">
                            <p>
                                Cookie ini membantu kami mengukur pola lalu lintas untuk menentukan area mana dari situs web kami yang telah dikunjungi. Kami menggunakan ini untuk meneliti kebiasaan pengunjung sehingga kami dapat meningkatkan produk dan layanan online kami. Kami dapat mencatat alamat IP (yaitu, alamat elektronik komputer yang terhubung ke internet) untuk menganalisis tren, mengelola situs web, melacak pergerakan pengguna, dan mengumpulkan informasi demografis yang luas. Cookie ini tidak penting untuk situs web dan kami akan selalu meminta persetujuan Anda sebelum menempatkan cookie ini. Anda dapat menarik persetujuan Anda setiap saat. Lihat informasi lebih lanjut Preferensi Anda di bawah.
                            </p>
                        </div>
                    </div>
                    <div class="cookie__subheading">
                        <div class="cookie__subheading-header" id="share">
                            <h4>Cookie untuk personalisasi</h4>
                        </div>
                        <div class="cookie__subheading-description">
                            <p>
                                Cookies ini memungkinkan kami untuk mengenali komputer Anda dan menyambut Anda setiap kali Anda mengunjungi situs web kami tanpa mengganggu Anda dengan permintaan untuk mendaftar atau untuk masuk. Cookies ini tidak penting untuk situs web tetapi meningkatkan pengalaman untuk Anda. Kami akan selalu meminta persetujuan Anda sebelum menempatkan cookie ini. Anda dapat menarik persetujuan Anda setiap saat. Lihat informasi lebih lanjut  <a href="#">Preferensi Anda</a> di bawah.
                            </p>
                        </div>
                    </div>
                    <div class="cookie__subheading">
                        <div class="cookie__subheading-header" id="share">
                            <h4>Cookie pihak ketiga</h4>
                        </div>
                        <div class="cookie__subheading-description">
                            <p>
                                Ini adalah cookie yang ditempatkan di situs web kami oleh pihak ketiga atas nama kami. Cookie memungkinkan kami untuk menghasilkan iklan yang dipersonalisasi di situs web lain, yang didasarkan pada informasi tentang Anda seperti tampilan halaman Anda di situs web kami. Situs web kami juga dapat menempatkan cookie untuk layanan pihak ketiga seperti media social. Kami tidak memberikan informasi pribadi apa pun kepada pihak ketiga yang menempatkan cookie di situs web kami. Namun, pihak ketiga dapat berasumsi bahwa pengguna yang berinteraksi dengan atau mengklik iklan atau konten yang dipersonalisasi yang ditampilkan di situs web kami termasuk dalam grup yang mengarahkan iklan atau konten tersebut. Kami tidak memiliki akses ke atau kontrol atas cookie atau fitur lain yang dapat digunakan pengiklan dan situs pihak ketiga. Praktik informasi dari pihak ketiga ini tidak dicakup oleh Pernyataan Privasi atau Cookie kami. Silakan hubungi mereka secara langsung untuk informasi lebih lanjut tentang praktik privasi mereka. Cookie ini tidak penting untuk situs web dan kami akan selalu meminta persetujuan Anda sebelum menempatkan cookie ini. Anda dapat menarik persetujuan Anda setiap saat. Lihat informasi lebih lanjut <a href="#">Preferensi Anda</a> di bawah.
                            </p>
                        </div>
                    </div>
                    <div class="cookie__subheading">
                        <div class="cookie__subheading-header" id="share">
                            <h4>
                                Bagaimana cara menghubungi kami
                            </h4>
                        </div>
                        <div class="cookie__subheading-description">
                            <p>
                                Jika Anda memiliki pertanyaan, komentar, atau keluhan terkait cookie, silakan hubungi kami melalui halaman kontak kami.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card" id="side-card">
                        <div class="row no-gutter">
                            <div class="col-12">
                                <img src="/assets/images/faq/three-doctor.png" alt="three doctors" class="img-fluid">
                            </div>
                            <div class="col-12">
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3>Konsultasi Langsung dengang Dokter dan Tim Ahli Kami</h3>
                                    </div>
                                    <div class="card-text">
                                        <p>
                                            Dapatkan jawaban dari pertanyaan seputar tumbuh kembang Mama dan si Kecil.
                                        </p>
                                    </div>
                                    <a href="#" class="btn">TANYA DOKTER</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

