<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/sitemap.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>

<div class="sitemap">
    <div class="sitemap__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="sitemap__heading">
                        <h1>Sitemap</h1>
                        <hr>
                    </div>
                    <div class="sitemap__description">
                        <ul>
                            <li>
                                <a href="#">
                                    Nutriclub
                                </a>
                                <ul>
                                    <li>
                                        <a href="#">Kehamilan</a>
                                    </li>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Nutrisi
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Aktivitas
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Kalender Kehamilan
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Resep Untuk Ibu Hamil
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Did You Know
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Persiapan Kehamilan
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Kehamilan
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Menyusui
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Lactamil
                                            </a>
                                        </li>
                                    </ul>
                                </ul>
                                <ul>
                                    <li>
                                        <a href="#">Bayi</a>
                                    </li>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Nutrisi
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Kesehatan
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Rutinitas
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Stimulasi
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Mitos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Tumbuh Kembang Bayi
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Aktivitas
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Resep Untuk Bayi
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Did You Know
                                            </a>
                                        </li>
                                    </ul>
                                </ul>
                                <ul>
                                    <li>
                                        <a href="#">Balita</a>
                                    </li>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Nutrisi
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Aktivitas & Edukasi
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Rutinitas
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Kesehatan
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Mitos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Tumbuh Kembang Balita
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Resep Untuk Balita
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Did You Know
                                            </a>
                                        </li>
                                    </ul>
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a href="#">
                                    Products
                                </a>
                                <ul>
                                    <li>
                                        <a href="#">Nutrilon Royal</a>
                                    </li>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Nutrilon Royal 3 ActiDuoBio +
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Nutrilon Royal 4 ActiDuoBio +
                                            </a>
                                        </li>
                                    </ul>
                                </ul>
                                <ul>
                                    <li>
                                        <a href="#">Nutrilon</a>
                                    </li>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Nutrilon 3
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Nutrilon 4
                                            </a>
                                        </li>
                                    </ul>
                                </ul>
                                <ul>
                                    <li>
                                        <a href="#">Advance Medical Nutrition</a>
                                    </li>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Nutrinidrink Cair
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Nutrinidrink Bubuk
                                            </a>
                                        </li>
                                    </ul>
                                </ul>
                                <ul>
                                    <li>
                                        <a href="#">
                                            Lactamil
                                        </a>
                                    </li>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Lactamil Inisis
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Lactamil Pregnasis
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Lactamil Lactasis
                                            </a>
                                        </li>
                                    </ul>
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a href="#">
                                    News & Highlights
                                </a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a href="#">
                                    Tools
                                </a>
                                <ul>
                                    <li>
                                        <a href="#">
                                            OSA Academy
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Tes Alergi
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            9 out of 10
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Mitos Kehamilan
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">
                                    Expert
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Tentang
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Kebijakan Privasi
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Syarat dan Ketentuan
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Royal Rewards
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    FAQ
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card" id="side-card">
                        <div class="row no-gutter">
                            <div class="col-12">
                                <img src="/assets/images/faq/three-doctor.png" alt="three doctors" class="img-fluid">
                            </div>
                            <div class="col-12">
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3>Konsultasi Langsung dengang Dokter dan Tim Ahli Kami</h3>
                                    </div>
                                    <div class="card-text">
                                        <p>
                                            Dapatkan jawaban dari pertanyaan seputar tumbuh kembang Mama dan si Kecil.
                                        </p>
                                    </div>
                                    <a href="#" class="btn">TANYA DOKTER</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>
