<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/corporat.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>
?>

<div class="corporat">
    <div class="corporat__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="corporat__heading">
                        <h1>Tentang Nutriclub</h1>
                        <hr>
                    </div>
                    <div class="corporat__description">
                        <p>
                            ASI adalah yang terbaik bagi si Kecil, Nutriclub sangat mendukung pemberian ASI. Saat Mama menyusui, harap perhatikan asupan nutrisi yang sehat dan seimbang. Penggunaan susu formula harus sesuai dengan petunjuk dan dikosultasikan terlebih dahulu ke dokter.
                        </p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card" id="side-card">
                        <div class="row no-gutter">
                            <div class="col-12">
                                <img src="/assets/images/faq/three-doctor.png" alt="three doctors" class="img-fluid">
                            </div>
                            <div class="col-12">
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3>Konsultasi Langsung dengang Dokter dan Tim Ahli Kami</h3>
                                    </div>
                                    <div class="card-text">
                                        <p>
                                            Dapatkan jawaban dari pertanyaan seputar tumbuh kembang Mama dan si Kecil.
                                        </p>
                                    </div>
                                    <a href="#" class="btn">TANYA DOKTER</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>
