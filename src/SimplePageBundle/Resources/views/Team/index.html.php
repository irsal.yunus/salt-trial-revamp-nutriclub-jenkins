<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/doctor-team.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>
<div class="doctor-team">
    <div class="doctor-team__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3">
                            <div class="doctor-team__heading">
                                <h1>Tim Dokter</h1>
                                <hr>
                            </div>
                            <div class="doctor-team__description">
                                <p>
                                    Para dokter spesialis dalam berbagai bidang seputar nutrisi, tumbuh kembang anak, serta perawatan mama dan bayi.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="doctor-team__lists">
                        <div class="row">
                            <div class="doctor-team__lists-item col-12 col-md-4 offset-md-2">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-5 ">
                                            <img src="/assets/images/doctor-team/doctor-1.png" alt="doctor-team-picture" class="img-fluid">
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body">
                                                <h5 class="card-title">dr. Etisa Adi Murbawani, M.Si, SpGK</h5>
                                                <p class="card-text">SPESIALIS GIZI KLINIS RS HERMINA PANDANARAN SEMARANG</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="doctor-team__lists-item col-12 col-md-4">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-5 ">
                                            <img src="/assets/images/doctor-team/doctor-2.png" alt="doctor-team-picture" class="img-fluid">
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body">
                                                <h5 class="card-title">dr. Cindiawaty Josito Pudjiadi MARS, MS, SpGK</h5>
                                                <p class="card-text">SPESIALIS GIZI KLINIK RS MEDISTRA JAKARTA</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="doctor-team__lists-item col-12  col-md-4 offset-md-2">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-5 ">
                                            <img src="/assets/images/doctor-team/doctor-3.png" alt="doctor-team-picture" class="img-fluid">
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body">
                                                <h5 class="card-title">dr. Ricky SpOG</h5>
                                                <p class="card-text">RS BETHSAIDA SERPONG, TANGERANG</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="doctor-team__lists-item col-12  col-md-4">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-5 ">
                                            <img src="/assets/images/doctor-team/doctor-4.png" alt="doctor-team-picture" class="img-fluid">
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body">
                                                <h5 class="card-title">dr. Jimmy Panji, SpOG</h5>
                                                <p class="card-text">SPESIALIS OBSTETRI DAN GINEKOLOGI RSB. PERMATA HATI LAMPUNG</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="doctor-team__lists-item col-12  col-md-4 offset-md-2">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-5 ">
                                            <img src="/assets/images/doctor-team/doctor-5.png" alt="doctor-team-picture" class="img-fluid">
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body">
                                                <h5 class="card-title">Dr. dr. Luciana B Sutanto SpGK</h5>
                                                <p class="card-text">SPESIALIS GIZI KLINIS RS HERMINA PANDANARAN SEMARANG</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="doctor-team__lists-item col-12  col-md-4">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-5 ">
                                            <img src="/assets/images/doctor-team/doctor-6.png" alt="doctor-team-picture" class="img-fluid">
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body">
                                                <h5 class="card-title">dr. Marissa, SpA</h5>
                                                <p class="card-text">RS. PREMIER JATINEGARA - SPESIALIS ANAK</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="doctor-team__lists-item col-12  col-md-4 offset-md-2">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-5 ">
                                            <img src="/assets/images/doctor-team/doctor-7.png" alt="doctor-team-picture" class="img-fluid">
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body">
                                                <h5 class="card-title">Dr. Vicka Farah Diba, Msc, SpA</h5>
                                                <p class="card-text">RS CONDONG CATUR YOGYAKARTA ASRI MEDICAL CENTER YOGYAKARTA</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="doctor-team__lists-item col-12  col-md-4">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-5 ">
                                            <img src="/assets/images/doctor-team/doctor-8.png" alt="doctor-team-picture" class="img-fluid">
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body">
                                                <h5 class="card-title">Dr. Marlisye Marpaung, M.Ked (Ped), SpA</h5>
                                                <p class="card-text">RS. MEDIKA BSD</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="doctor-team__lists-item col-12  col-md-4 offset-md-2">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-5 ">
                                            <img src="/assets/images/doctor-team/doctor-1.png" alt="doctor-team-picture" class="img-fluid">
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body">
                                                <h5 class="card-title">dr. Pustika Efar, SpA</h5>
                                                <p class="card-text">RSIA BUDHI JAYA</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- card form -->
                    <div class="doctor-team__form" id="send_question">
                        <div class="row">
                            <div class="col-12 col-md-8 offset-md-2">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="doctor-team__form-heading">
                                            <h1>Tanya Tim Dokter</h1>
                                            <hr/>
                                        </div>
                                        <div class="doctor-team__form-description">
                                            <p>
                                                Mama bisa mengirim keluhan dan pertanyaan Ibu langsung pada kami dengan menuliskannya di sini.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <form id="submitQuestion">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="name">Nama <span>*</span></label>
                                                        <input type="text" id="name" class="form-control" aria-describedby="user name">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="email">Email <span>*</span></label>
                                                        <input type="email" id="email" class="form-control" aria-describedby="user email">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="phonenumber">Nomor Telepon</label>
                                                        <input type="tel" id="phonenumber" class="form-control" aria-describedby="user phonenumber" placeholder="+62">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="topicquestion">Pilih Topik Pertanyaan</label>
                                                        <select class="form-control" id="topicquestion" aria-describedby="topic of question">
                                                            <option selected disabled value="0">Pilih Topik Umum</option>
                                                            <option value="1">Nutrisi Seimbang</option>
                                                            <option value="2">Kondisi Medis Anak</option>
                                                            <option value="3">Produk Nutricia</option>
                                                            <option value="4">Kehamilan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="issue">Keluhan Utama <span>*</span></label>
                                                        <input type="text" id="issue" class="form-control" aria-describedby="user phonenumber" maxlength="80">
                                                        <div class="d-flex justify-content-end">
                                                            <p class="maxCharacter">
                                                                0
                                                            </p>
                                                            <p>
                                                                /80
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="issue-description">Jelaskan Lebih Lengkap <span>**</span></label>
                                                        <textarea name="issue-description" id="issue-description" rows="3" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-2 pr-0 col-md-1">
                                                            <span>*</span>
                                                        </div>
                                                        <div class="col-10 pl-0 col-md-10">
                                                            <p>Wajib Diisi</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-2 pr-0 col-md-1">
                                                            <span>**</span>
                                                        </div>
                                                        <div class="col-10 pl-0 col-md-10">
                                                            <p>Informasi <b>Usia Anak</b> dan <b>Berat Badan</b> atau <b>Usia Kehamilan</b>  diperlukan untuk membantu penjawaban oleh dokter secara lebih tepat.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 offset-md-3">
                                                    <button class="doctor-team__button-submit btn btn-primary btn-block p-3" type="submit">TANYA DOKTER</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of card -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' )
    ->offsetSetFile(24, '/assets/js/pages/doctor-team.js', 'text/javascript' );
?>

