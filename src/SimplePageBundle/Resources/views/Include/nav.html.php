<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Asset\Image;
use Pimcore\Model\Document;
use Pimcore\Model\WebsiteSetting;
use Pimcore\Navigation\Page;
use AppBundle\Model\DataObject\Customer;

// get root node if there is no document defined (for pages which are routed directly through static route)
$document = $this->document;
if(!$document instanceof Document\Page) {
    $document = Document\Page::getById(1);
}

// get the document which should be used to start in navigation | default home
$mainNavStartNode = $this->document->getProperty("navigationRoot");
if(!$mainNavStartNode instanceof Document\Page) {
    $mainNavStartNode = Document\Page::getById(1);
}

// this returns us the navigation container we can use to render the navigation
$mainNavigation = $this->navigation()->buildNavigation($document, $mainNavStartNode);

// later you can render the navigation
//echo $this->navigation()->render($mainNavigation);

$user = $app->getUser();

if ($user) {
    /** @var Customer $userSso */
    $userSso = $this->getRequest()->getSession()->get('UserProfile');
}

$form = $this->formSearch;
?>
<nav id="site-header" class="navbar navbar-expand-lg fixed-top">
    <div class="container header header-form">
        <div class="row m-md-0" id="navbarNav">
            <div class="col-2 pl-md-0">
                <div class="navbar-brand header__brand" id="hamburger-menu">
                    <img src="/assets/images/icons/hamburger-menu.png" alt="hamburger menu nutriclub"/>
                </div>
            </div>
            <div class="col-8">
                <ul class="navbar-nav header__tool text-center">
                    <?php
                    /** @var Page $page */
                    foreach ($mainNavigation->getPages() as $page) {
                        $pagesChild = $page->getPages();

                        $document = Document::getById($page->getId(), 1);
                        if (!$document->hasProperty('NAV_TOP')) {
                            continue;
                        }

                        if ($page) {
                            if (count($pagesChild) > 0) {
                                ?>
                                <li class="nav-item dropdown header__dropdown mb-0 mt-0">
                                    <a class="nav-link dropdown-toggle header__caption" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?= $page->getLabel() ?>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <?php
                                        foreach ($pagesChild as $pageChild) {
                                            ?>
                                            <a class="dropdown-item" href="<?= $pageChild->getHref() ?>"><?= $pageChild->getLabel() ?></a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </li>
                                <?php
                            }
                            if (count($pagesChild) === 0) {
                                ?>
                                <li class="nav-item">
                                    <a class="nav-link header__caption"
                                       data-name="<?= $page->getLabel() ?>"
                                       href="<?= $document->getHref() ?>"
                                       event-name="CLICK_MENU_HEADER"
                                    >
                                        <?= $page->getLabel() ?>
                                    </a>
                                </li>
                                <?php
                            }
                        }

                        if ($document->hasProperty('ADD_LOGO_AFTER_ME')) {

                            /** @var Image $logo */
                            $logo = WebsiteSetting::getByName('NUTRICLUB_LOGO') ?
                                WebsiteSetting::getByName('NUTRICLUB_LOGO')->getData() : null;

                            $logoImg = null;

                            if (!$logo) {
                                $logoImg = '<img src="https//via.placeholder.com/250" class="logo-nutriclub"/>';
                            }

                            if ($logo) {
                                $logoImg = $logo->getThumbnail('website-logo')->getHtml([
                                    'class' => 'logo-nutriclub',
                                    'disableWidthHeightAttributes' => true
                                ]);
                            }

                            ?>
                            <li class="nav-item header__logo">
                                <a class="nav-link mt-0 mb-0" href="/">
                                    <?= $logoImg ?>
                                </a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="col-2 d-flex align-items-right justify-content-end pr-md-0">
                <form class="form-inline header__search">
                    <button type="button" class="header__search-submit" aria-label="search submit"></button>
                </form>
            </div>
        </div>
    </div>
    <!-- search resutls section -->
    <div class="container header header-result d-none">
        <div class="row">
            <div class="col-md-12">
                <?= $this->form()->start($form, [
                    'attr' => [
                        'class' => 'form-inline header__search'
                    ]
                ])?>
                <div class="row header__search-wrapper">
                    <div class="col-1 col-md-1">
                        <div class="navbar-brand header__brand" id="back-menu">
                            <img src="/assets/images/icons/arrow-back.svg" />
                        </div>
                    </div>
                    <div class="col-11 col-md-10">
                        <div class="position-relative">
                        <?= $this->form()->widget($form['q'], [
                            'attr' => [
                                'class' => 'form-control search-form live-search shadow-none',
                                'type' => 'search',
                                'style' => 'width: 100%;',
                                'placeholder' => $this->t('FIND')
                            ]
                        ]) ?>
                        <input type="hidden" name="type[0]" value="article">
                        <input type="hidden" name="type[1]" value="articlevideo">
                        <input type="hidden" name="type[2]" value="articlepodcast">
                        <input type="hidden" name="type[3]" value="articleebook">
                        <img src="/assets/images/icons/close-circle.png" alt="search-img" class="close-query-icon">
                        </div>
                        <!-- results lists -->
                        <ul class="search__results-lists">
                        <!-- results lits item -->
                        </ul>
                    </div>

                </div>
                <?= $this->form()->end($form) ?>
            </div>
        </div>
    </div>
</nav>
<div class="hamburger-menu__lists d-none">
    <div class="container">
        <div class="navbar-brand header__brand d-none" id="close-menu">
            <img src="/assets/images/icons/hamburger-menu-close.png" />
        </div>
    </div>

        <div class="hamburger-menu__header text-center primary p-3">
            <?php if (!$user || !$userSso) { ?>
            <!-- user tab before login -->
            <div class="hamburger-menu__header-login-text">
                <a href="<?= $this->path('account-login') ?>"
                   data-name="<?= $this->t('LOGIN_OR_REGISTER') ?>"
                   event-name="CLICK_MENU_SIDEBAR"
                >
                    <h3><?= $this->t('LOGIN_OR_REGISTER') ?></h3>
                </a>
            </div>
            <?php } ?>
            <!-- user tab after login -->
            <?php if ($user && $userSso) { ?>
            <!-- header-profile -->
            <div class="hamburger-menu__profile ">
                <div class="row">
                    <div class="hamburger-menu__profile-header col-md-8 offset-md-2 col-10">
                        <a href="#">
                            <img src="<?= ($userSso ? $userSso->getProfilePicture() : null) ?>" alt="user-photo">
                        </a>
                        <div class="hamburger-menu__profile-header-text">
                            <h3><?= $userSso ? $userSso->getFullname() : null ?></h3>
                            <h4>ID <span><?= $userSso ? $userSso->getId() : null ?></span></h4>
                        </div>
                    </div>
                    <div class="hamburger-menu__profile-arrow col-md-1 col-2">
                       <a href="#">
                            <img src="/assets/images/contents/forward-arrow.png">
                       </a>
                    </div>
                </div>
                <div class="row pregnancy-today">
                    <div class="col-xs-4 offset-xs-2 col-md-2 offset-md-0 pregnancy-today-figure">
                        <img src="/assets/images/page_pregnancy_today/nutriclub_logo.png" class="img-fluid" alt="nutriclub-logo">
                    </div>
                    <div class="col-xs-4 col-md-2 pregnancy-today-path">
                        <a href="/">Home</a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="hamburger-menu__lists-wrapper">
            <ul class="hamburger-menu__lists-item">
<!-- for homepage -->
                <li class="hamburger-menu__item">
                    <a href="/">Home</a>
                </li>
<!--  for index my pregnancny today -->
<!--                <li class="hamburger-menu__item pregnancy-today">-->
<!--                   <div class="row">-->
<!--                       <div class="col-4  offset-3 col-md-6">-->
<!--                           <img src="/assets/images/page_pregnancy_today/nutriclub_logo.png" class="img-fluid" alt="nutriclub-logo">-->
<!--                       </div>-->
<!--                       <div class="col-4 col-md-6 text-left align-self-center">-->
<!--                           <a href="/">Home</a>-->
<!--                       </div>-->
<!--                   </div>-->
<!--                </li>-->
<!-- end of index my pregnancy today-->
                <?php try {
                    $navBread = $this->navigation()
                        ->menu()
                        ->setPartial('Include/nav.hamburger.partial.html.php')
                        ->setMinDepth(null)
                        ->render($mainNavigation);

                    echo $navBread;
                } catch (Exception $e) {

                }
                ?>
                <?php if ($user && $userSso) { ?>
                <li class="hamburger-menu__item blue-text">
                    <a class="link-logout"
                       href="<?= $this->path('account-logout') ?>"
                       event-name="CLICK_MENU_SIDEBAR"
                       data-name="<?= $this->t('LOGOUT') ?>"
                    >
                        <b><?= $this->t('LOGOUT') ?></b>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
</div>

<div class="search__results d-none">
       <div class="container">
            <div class="search__results-wrapper">
                <div class="search__results-text-btn-wrapper <?= !$user ? 'd-none' : '' ?>">
                    <p class="search__results-text">Riwayat Pencarian</p>
                    <button class="search__results-btn btn delete-cookie pt-0" data-toggle="modal" data-target="#confirm-delete-modal">Hapus</button>
                </div>
                <div class="search__results-history <?= !$user ? 'd-none' : '' ?>">
                    <!-- show list results -->
                </div>
                <p class="search__results-text">Pencarian Populer</p>
                <div class="search__results-populer">
                </div>

            </div>
       </div>
</div>
<!-- confirm page for deleting application -->
<div class="modal__confirm">
    <div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="dialog" aria-labelledby="confirm Delete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="confirm Delete">Apakah Anda yakin ingin menghapus?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
            <button type="button" class="modal__confirm-delete btn btn-primary">Ya Hapus</button>
        </div>
        </div>
    </div>
    </div>
</div>
