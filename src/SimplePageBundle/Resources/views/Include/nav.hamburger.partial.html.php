<?php
/**
 * Created by PhpStorm.
 * User: Yulius Ardian Febrianto <yuliusardin@gmail.com>
 * Date: 18/02/2020
 * Time: 16:08
 */

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\DataObject\Customer;
use Pimcore\Model\Document;
use Pimcore\Navigation\Page;

$user = $app->getUser();

if ($user) {
    /** @var Customer $userSso */
    $userSso = $this->getRequest()->getSession()->get('UserProfile');
}
?>

<?php
/** @var Page $page */
foreach ($this->pages as $page):

    $document = Document::getById($page->getId(), 1);

    if (!$document->hasProperty('NAV_BURGER')) {
        continue;
    }
    $page->setClass(null);
    ?>

    <?php
    $pageChild = $page->getPages();
    if ($document->hasProperty('NAV_NAME_BASED_ON_GENDER')) {
        if ($userSso ? $userSso->getGender() === 'F' : null) {
            $page->setLabel($this->t('YOU_MUST_KNOW_FEMALE'));
        }
        if ($userSso ? $userSso->getGender() === 'M' : null) {
            $page->setLabel($this->t('YOU_MUST_KNOW_MALE'));
        }
    }
    if (count($pageChild) > 0) {
        ?>
        <ul class="hamburger-menu__lists-item bg-grey">
            <li class="hamburger-menu__item"><?= $this->navigation()->menu()->htmlify($page) ?></li>
            <?php
            foreach ($pageChild as $pageC) {
                $documentChild = Document::getById($pageC->getId(), 1);
                if (!$documentChild->hasProperty('NAV_BURGER')) {
                    continue;
                }
                $name = $pageC->getLabel();
                $pageC->setCustomHtmlAttribs([
                    'data-name' => $name,
                    'event-name' => 'CLICK_MENU_SIDEBAR'
                ])
                ?>
                <li class="hamburger-menu__item">
                    <?= $this->navigation()->menu()->htmlify($pageC) ?>
                </li>
                <?php
            }
            ?>
        </ul>
        <?php
    }

    if (count($pageChild) === 0) {
        $name = $page->getLabel();
        $page->setCustomHtmlAttribs([
            'data-name' => $name,
            'event-name' => 'CLICK_MENU_SIDEBAR'
        ])
        ?>
        <li class="hamburger-menu__item">
            <?= $this->navigation()->menu()->htmlify($page) ?>
        </li>
        <?php
    }
    ?>

<?php endforeach; ?>
