<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/08/20
 * @time 00.49
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()
    ->offsetSetStylesheet(25, "/assets/css/pages/simple-page/privacy.css", 'screen', false, [
        'defer' => 'defer'
    ]);
?>
<div class="privacy">
    <div class="privacy__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8" id="side-description">
                    <div class="privacy__heading">
                        <h1>Pertanyaan Privasi</h1>
                        <hr>
                    </div>
                    <div class="privacy__description">
                        <p>
                            Danone tahu bahwa Anda peduli bagaimana data pribadi Anda digunakan dan kami menyadari pentingnya melindungi privasi Anda.

                            Pernyataan Privasi ini menjelaskan bagaimana PT Nutricia Indonesia Sejahtera (“Danone”) mengumpulkan dan mengelola data pribadi Anda.

                            Pernyataan ini berisi informasi tentang data apa yang kami kumpulkan, bagaimana kami menggunakannya, mengapa kami membutuhkannya dan bagaimana hal itu bisa menguntungkan Anda.

                            Hubungi kami di sini jika Anda memiliki pertanyaan dan komentar, atau jika Anda ingin mengajukan permintaan mengenai hak subyek data Anda.

                        </p>
                        <div class="alert alert-secondary" role="alert">
                            <p>Pernyataan privasi ini terakhir diperbarui pada 20 Januari 2020.</p>
                        </div>
                    </div>
                    <div class="privacy__menu container">
                        <ul>
                            <li>
                                <a href="#penggunaan-website">
                                    Prinsip dasar dan komitmen privasi kami
                                </a>
                            </li>
                            <li>
                                <a href="#hak-hak">
                                    Siapa yang mengumpulkan?
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Data pribadi apa yang kami kumpulkan?
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Mengapa kami mengumpulkan dan menggunakan data pribadi Anda?
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Hak Anda
                                </a>
                                <ul>
                                    <li>
                                        <a href="#">
                                            Hak portabilitas data
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Hak untuk menghapus data pribadi Anda
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Hak atas pembatasan pemrosesan
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Hak untuk menolak
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">
                                    Bagaimana kami melindungi data pribadi Anda
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Berbagi data pribadi Anda
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Layanan Nutriclub membership
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Berbagi data secara internasional
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Pengambilan keputusan dan pembuatan profil otomatis
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Cookie dan teknologi lainnya
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Bagaimana cara menghubungi kami
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Bagaimana cara kami memperbarui Pernyataan ini?
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Ketentuan atau Pernyataan Privasi Tambahan
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="penggunaan-website">
                            <h3>Prinsip dasar dan komitmen privasi kami</h3>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                Di Danone kami berkomitmen untuk melindungi hak Anda untuk privasi. Kami bertujuan untuk melindungi data pribadi apa pun yang kami miliki, untuk mengelola data pribadi Anda secara bertanggung jawab dan transparan dalam praktik kami. Kepercayaan anda penting bagi kami. Karena itu kami berkomitmen pada prinsip-prinsip dasar berikut:
                            </p>
                            <ul>
                                <li>
                                    Anda tidak memiliki kewajiban untuk memberikan data pribadi yang diminta oleh kami. Namun, jika Anda memilih untuk tidak melakukannya, kami mungkin tidak dapat menyediakan beberapa layanan atau produk kepada Anda;
                                </li>
                                <li>
                                    Anda tidak memiliki kewajiban untuk memberikan data pribadi yang diminta oleh kami. Namun, jika Anda memilih untuk tidak melakukannya, kami mungkin tidak dapat menyediakan beberapa layanan atau produk kepada Anda;
                                </li>
                                <li>
                                    Anda tidak memiliki kewajiban untuk memberikan data pribadi yang diminta oleh kami. Namun, jika Anda memilih untuk tidak melakukannya, kami mungkin tidak dapat menyediakan beberapa layanan atau produk kepada Anda;
                                </li>
                                <li>
                                    Anda tidak memiliki kewajiban untuk memberikan data pribadi yang diminta oleh kami. Namun, jika Anda memilih untuk tidak melakukannya, kami mungkin tidak dapat menyediakan beberapa layanan atau produk kepada Anda;
                                </li>
                                <li>
                                    Anda tidak memiliki kewajiban untuk memberikan data pribadi yang diminta oleh kami. Namun, jika Anda memilih untuk tidak melakukannya, kami mungkin tidak dapat menyediakan beberapa layanan atau produk kepada Anda;
                                </li>
                                <li>
                                    Anda tidak memiliki kewajiban untuk memberikan data pribadi yang diminta oleh kami. Namun, jika Anda memilih untuk tidak melakukannya, kami mungkin tidak dapat menyediakan beberapa layanan atau produk kepada Anda;
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="hak-hak">
                            <h3>Siapa yang mengumpulkan?</h3>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                Setiap data pribadi yang diberikan kepada atau dikumpulkan oleh Danone sebagai pengendali data (“Danone”).
                            </p>
                            <p>
                                Pernyataan Privasi ini berlaku untuk data pribadi yang dikumpulkan oleh Danone sehubungan dengan layanan dan produk yang kami tawarkan. Pernyataan ini juga berlaku untuk untuk konten pemasaran Danone, termasuk penawaran dan iklan untuk produk dan layanan Danone, kegiatan promosi Danone, yang kami (atau penyedia layanan bertindak atas nama kami) kirimkan kepada Anda di situs web pihak ketiga, platform dan aplikasi berdasarkan informasi penggunaan situs Anda. Situs web pihak ketiga tersebut umumnya memiliki Pernyataan Privasi dan Syarat dan Ketentuan sendiri. Kami sangat menganjurkan Anda untuk membacanya sebelum menggunakan situs web pihak ketiga tersebut.
                            </p>
                        </div>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="share">
                            <h3>Data pribadi apa yang kami kumpulkan?</h3>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                Data pribadi yang kami kumpulkan bervariasi tergantung pada tujuan pengumpulan dan produk dan/atau layanan yang kami sediakan untuk Anda.
                            </p>
                            <p>
                                Secara umum, kami dapat mengumpulkan jenis data pribadi berikut dari Anda secara langsung:
                            </p>
                            <p>
                                a) Data kontak pribadi, seperti nama Anda, alamat email, alamat fisik, tanggal lahir, nama anak, dan nomor telepon;
                            </p>
                            <p>
                                b) Detail login akun, seperti ID pengguna Anda, nama pengguna email dan kata sandi. Ini diperlukan untuk membuat akun pengguna pribadi di salah satu komunitas online kami;
                            </p>
                        </div>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="share">
                            <h3>BATASAN TANGGUNG JAWAB DAN GANTI RUGI</h3>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                Jika konsumen ingin mempublikasikan ulang, meredistribusi atau mengeksploitasi isi website ini dengan cara tertentu, untuk keperluan komersil, konsumen dapat meminta ijin tertulisan terlebih dahulu dari PT Nutrica Indonesia Sejahtera. PT Nutricia Indonesia Sejahtera tidak memberikan jaminan bahwa ijin dapat diberikan.
                            </p>
                        </div>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="share">
                            <h3>BATASAN TANGGUNG JAWAB DAN GANTI RUGI</h3>
                            <h4>5.1 Konten Editorial</h4>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                Selain hasil riset kami, informasi, data, foto, video, dan artikel yang dimuat di dalam website ini dapat juga berasal dari berbagai macam sumber yang terpercaya dan tervalidasi oleh Tim Ahli Nutriclub. Khusus untuk tautan dari luar, konsumen diharapkan membaca dan memanfaatkan informasi yang ada di dalamnya secara bijaksana. Bilamana konsumen melakukan kontak atau hubungan dengan pihak ketiga yang namanya tercantum dalam website kami adalah tanggung jawab konsumen pribadi. Setiap bagian dari website ini hanya dapat digunakan untuk keperluan pribadi dan informasi bagi konsumen secara umum dan bukan untuk rekomendasi atau saran yang spesifik dan pasti. Sebelum mengambil suatu tindakan atau keputusan berdasarkan seluruh atau sebagian dari isi website ini, konsumen harus selalui melakukan pengecekan pribadi secara independen terhadap informan yang dianggap penting untuk menjadi dasar pengampilan keputusan atau tindakan, seperti medis, investasi, keuangan, pendidik, dan masalah-masalah lain yang membutuhkan saran professional. PT Nutricia Indonesia tidak dapat dimintai pertanggung jawaban atas klaim, kerugian, cedera, denda, kerusakan, biaya atau pengeluaran yang timbul sebagai akibat pemanfaatan atau ketidak mampuan untuk memanfaatkan sebagian atau seluruh isi website kami, atau pengabaian suatu tindakan yang seharusnya diambil, sebagai hasil dari penggunaan konten. 5.2 User Generated Content (UGC) Jika konsumen mengunggah suatu materi ke dalam website ini (teks berupa data diri, pertanyaan atau komentar, grafik, foto, gambar, video atau audio) dengan demikian konsumen telah setuju untuk memberikan hak atau lisensi, sub-lisensi, bebas royalty, non-eksklusif dan hak untuk memberikan materia ini kepada PT Nutricia Indonesia sejahtera. PT Nutricia Indonesia Sejahtea dapat menggunakan, mereproduksi, memodifikasi, mengadaptasi, mempublikasi, menerjemahkan maupun menciptakan karya baru dan materi yang konsumen unggah. PT Nutricia Indonesia Sejahtera juga berhak untuk mendistribusikan, menampilkan, memunculkan materi tersebut dengan ketentuan hak cipta dan hak publikasi pada Nutriclub Indonesia. Dengan memberikan data diri konsumen ke dalam website ini, konsumen memberikan persetujuan kepada PT Nutricia Indonesia Sejahtera untuk:
                            </p>
                        </div>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="share">
                            <h4>5.2 User Generated Content (UGC)</h4>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                Selain hasil riset kami, informasi, data, foto, video, dan artikel yang dimuat di dalam website ini dapat juga berasal dari berbagai macam sumber yang terpercaya dan tervalidasi oleh Tim Ahli Nutriclub. Khusus untuk tautan dari luar, konsumen diharapkan membaca dan memanfaatkan informasi yang ada di dalamnya secara bijaksana. Bilamana konsumen melakukan kontak atau hubungan dengan pihak ketiga yang namanya tercantum dalam website kami adalah tanggung jawab konsumen pribadi. Setiap bagian dari website ini hanya dapat digunakan untuk keperluan pribadi dan informasi bagi konsumen secara umum dan bukan untuk rekomendasi atau saran yang spesifik dan pasti. Sebelum mengambil suatu tindakan atau keputusan berdasarkan seluruh atau sebagian dari isi website ini, konsumen harus selalui melakukan pengecekan pribadi secara independen terhadap informan yang dianggap penting untuk menjadi dasar pengampilan keputusan atau tindakan, seperti medis, investasi, keuangan, pendidik, dan masalah-masalah lain yang membutuhkan saran professional. PT Nutricia Indonesia tidak dapat dimintai pertanggung jawaban atas klaim, kerugian, cedera, denda, kerusakan, biaya atau pengeluaran yang timbul sebagai akibat pemanfaatan atau ketidak mampuan untuk memanfaatkan sebagian atau seluruh isi website kami, atau pengabaian suatu tindakan yang seharusnya diambil, sebagai hasil dari penggunaan konten.
                                Jika konsumen mengunggah suatu materi ke dalam website ini (teks berupa data diri, pertanyaan atau komentar, grafik, foto, gambar, video atau audio) dengan demikian konsumen telah setuju untuk memberikan hak atau lisensi, sub-lisensi, bebas royalty, non-eksklusif dan hak untuk memberikan materia ini kepada PT Nutricia Indonesia sejahtera. PT Nutricia Indonesia Sejahtea dapat menggunakan, mereproduksi, memodifikasi, mengadaptasi, mempublikasi, menerjemahkan maupun menciptakan karya baru dan materi yang konsumen unggah. PT Nutricia Indonesia Sejahtera juga berhak untuk mendistribusikan, menampilkan, memunculkan materi tersebut dengan ketentuan hak cipta dan hak publikasi pada Nutriclub Indonesia. Dengan memberikan data diri konsumen ke dalam website ini, konsumen memberikan persetujuan kepada PT Nutricia Indonesia Sejahtera untuk:
                            </p>
                        </div>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="share">
                            <h4>5.3 Promosi, Kompetisi, Penarikan Undian</h4>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                PT Nutricia Sejahtera Indonesia melalui website www.nutriclub.co.id ini mungkin dalam sewaktu-waktu tertentu akan menyelenggarakan aktifitas Promo, Kompetisi, Penarikan Undian, baik atas inisiatif sendiri maupun bekerjasama dengan rekanan kompersiil. Ketententuan dan persyaratan tambahan akan disediakan pada waktu promosi, kompetisi dan penarikan undian tersebut diselenggarakan.
                            </p>
                        </div>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="share">
                            <h4>5.4 PERUBAHAN TERHADAP KETENTUAN INI</h4>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                Jika konsumen ingin mempublikasikan ulang, meredistribusi atau mengeksploitasi isi website ini dengan cara tertentu, untuk keperluan komersil, konsumen dapat meminta ijin tertulisan terlebih dahulu dari PT Nutrica Indonesia Sejahtera. PT Nutricia Indonesia Sejahtera tidak memberikan jaminan bahwa ijin dapat diberikan.
                            </p>
                        </div>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="share">
                            <h4>LAYANAN NUTRICLUB MEMBERSHIP</h4>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                Nutriclub Membership merupakan layanan keanggotaan yang tersedia di dalam Nutriclub website, sebagai wujud komitmen Nutriclub untuk mendampingi konsumen dalam perjalanan kehamilan serta tumbuh kembang anak sehingga konsumen dapat menjadi selangkah lebih maju (One Step Ahead) melalui benefit utama. 7.1 Syarat dan Ketentuak Keanggotaan Nutriclub (Nutriclub Membership) Nutriclub Membership / Keanggotaan Nutriclub dapat diikuti oleh seluruh Warga Negara Indonesia. Untuk mendaftarkan diri menjadi anggota Nutriclub, konsumen dapat mencari menu “membership” pada halaman nutriclub.co.id atau mengakses langsung melalui www.nutriclub.co.id/membership Untuk menjadi anggota, konsumen diwajibkan untuk mengisi data diri berupa Nama Lengkap, Email, No. Ponsel, Jenis Kelamin, Tanggal Lahir yang sebenar-benarnya, serta menentukan password yang diinginkan. Selain itu konsumen juga diharuskan untuk melengkapi data tambahan yaitu status tahapan Ibu (status kehamilan dan atau data diri anak) untuk dapat menikmati benefit Nutriclub Memebership secara lengkap. Dengan memberikan data diri konsumen ke dalam website dan atau melalui layanan keanggotaan Nutriclub, konsumen memberikan persetujuan kepada PT Nutricia Indonesia Sejahtera untuk:
                            </p>
                            <ul>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium corporis ratione necessitatibus excepturi veniam est illum, reprehenderit aliquid, eos neque aut velit qui omnis at vel inventore ea sed similique!
                                </li>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, facere mollitia maiores ipsum id dolores aut enim explicabo recusandae atque incidunt. Aperiam nostrum totam nisi eos soluta! Ut, voluptatem eum!
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="share">
                            <h4>
                                7.2 Benefit Nutriclub Membership
                            </h4>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                Dengan menjadi anggota Nutriclub, konsumen berhak mendapatkan benefit di bawah ini:
                            </p>
                            <ul>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium corporis ratione necessitatibus excepturi veniam est illum, reprehenderit aliquid, eos neque aut velit qui omnis at vel inventore ea sed similique!
                                </li>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, facere mollitia maiores ipsum id dolores aut enim explicabo recusandae atque incidunt. Aperiam nostrum totam nisi eos soluta! Ut, voluptatem eum!
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="privacy__subheading">
                        <div class="privacy__subheading-header" id="share">
                            <h3>
                                ROYAL REWADS
                            </h3>
                        </div>
                        <div class="privacy__subheading-description">
                            <p>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dicta, cumque, dolorem perspiciatis ratione optio ipsum minima sunt cupiditate ea repellat similique in voluptatum reiciendis explicabo! Provident aut mollitia eaque soluta.
                            </p>
                            <ul>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium corporis ratione necessitatibus excepturi veniam est illum, reprehenderit aliquid, eos neque aut velit qui omnis at vel inventore ea sed similique!
                                </li>
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, facere mollitia maiores ipsum id dolores aut enim explicabo recusandae atque incidunt. Aperiam nostrum totam nisi eos soluta! Ut, voluptatem eum!
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card" id="side-card">
                        <div class="row no-gutter">
                            <div class="col-12">
                                <img src="/assets/images/faq/three-doctor.png" alt="three doctors" class="img-fluid">
                            </div>
                            <div class="col-12">
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3>Konsultasi Langsung dengang Dokter dan Tim Ahli Kami</h3>
                                    </div>
                                    <div class="card-text">
                                        <p>
                                            Dapatkan jawaban dari pertanyaan seputar tumbuh kembang Mama dan si Kecil.
                                        </p>
                                    </div>
                                    <a href="#" class="btn">TANYA DOKTER</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->headScript()
    ->offsetSetFile(23, '/assets/js/widgets/simple-page-card.js', 'text/javascript' );
?>
