<?php

namespace RoyalRewardsBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class RoyalRewardsBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/royalrewards/js/pimcore/startup.js'
        ];
    }
}