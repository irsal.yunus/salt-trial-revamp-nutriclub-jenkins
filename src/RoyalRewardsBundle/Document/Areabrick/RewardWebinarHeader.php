<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RewardWebinarHeader.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 30, 2020
 * @time 17:18:32 PM
 *
 **/

namespace RoyalRewardsBundle\Document\Areabrick;

class RewardWebinarHeader extends AbstractAreabrick
{
}
