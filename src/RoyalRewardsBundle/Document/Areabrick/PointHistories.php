<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource PointHistories.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 20, 2020
 * @time 5:19:13 PM
 *
 **/

namespace RoyalRewardsBundle\Document\Areabrick;

use RoyalRewardsBundle\Services\PointService;
use Pimcore\Model\Document\Tag\Area\Info;

class PointHistories extends AbstractAreabrick
{
    /** @var PointService $pointService */
    private $pointService;

    public function __construct(PointService $pointService)
    {
        $this->pointService = $pointService;
    }

    public function action(Info $info)
    {
        $info->getView()->actionCodes = $this->pointService->getActionCodes();
        $info->getView()->pointHistories = $this->pointService->getPointHistories();
        $info->getView()->pagination = $this->pointService->arrayPagination();
    }
}
