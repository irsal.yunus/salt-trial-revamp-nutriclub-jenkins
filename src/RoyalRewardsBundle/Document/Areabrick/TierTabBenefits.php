<?php
namespace RoyalRewardsBundle\Document\Areabrick;

use Pimcore\Extension\Document\Areabrick\AbstractTemplateAreabrick;

class TierTabBenefits extends AbstractTemplateAreabrick
{
    public function getName()
    {
        return 'Tier Tab Benefits';
    }

    public function getDescription()
    {
        return 'Royal Rewards';
    }

    public function getTemplateLocation()
    {
        return static::TEMPLATE_LOCATION_BUNDLE;
    }
}