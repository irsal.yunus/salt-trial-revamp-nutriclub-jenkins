<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource HeroBanner.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 2:50:37 PM
 *
 **/

namespace RoyalRewardsBundle\Document\Areabrick;

class HeroBanner extends AbstractAreabrick
{
}
