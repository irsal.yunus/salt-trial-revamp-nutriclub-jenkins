<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource UserRewards.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Aug 12, 2020
 * @time 3:47:29 AM
 *
 **/

namespace RoyalRewardsBundle\Document\Areabrick;

use RoyalRewardsBundle\Services\DashboardService;
use Pimcore\Model\Document\Tag\Area\Info;

class UserRewards extends AbstractAreabrick
{
    /** @var DashboardService $dashboardService */
    private $dashboardService;

    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }

    public function action(Info $info)
    {
        $info->getView()->customerRewards = $this->dashboardService->getCustomerRewards();
    }
}
