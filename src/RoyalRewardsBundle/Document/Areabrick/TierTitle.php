<?php
namespace RoyalRewardsBundle\Document\Areabrick;

use Pimcore\Extension\Document\Areabrick\AbstractTemplateAreabrick;

class TierTitle extends AbstractTemplateAreabrick
{
    public function getName()
    {
        return 'Tier Title';
    }

    public function getDescription()
    {
        return 'Royal Rewards';
    }

    public function getTemplateLocation()
    {
        return static::TEMPLATE_LOCATION_BUNDLE;
    }
}