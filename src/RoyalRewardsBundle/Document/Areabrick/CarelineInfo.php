<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineInfo.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Aug 30, 2020
 * @time 1:40:38 PM
 *
 **/

namespace RoyalRewardsBundle\Document\Areabrick;

use Pimcore\Model\Document\Tag\Area\Info;

class CarelineInfo extends AbstractAreabrick {
	
	public function action(Info $info) {
		$id = $info->getRequest()->get('id');
		$info->getView()->id = $id;
		
	}
}
