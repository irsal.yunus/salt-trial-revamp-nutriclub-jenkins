<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineSchedule.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Aug 30, 2020
 * @time 1:44:01 PM
 *
 **/

namespace RoyalRewardsBundle\Document\Areabrick;

use Carbon\Carbon;
use Pimcore\Model\Document\Tag\Area\Info;
use RoyalRewardsBundle\Services\CarelineService;
use AppBundle\Services\UserService;

class CarelineSchedule extends AbstractAreabrick
{
	private $carelineService;

	private $userService;

	private $dates;

	public function __construct(CarelineService $carelineService, UserService $userService)
    {
		$this->carelineService = $carelineService;
		$this->userService = $userService;
		$this->dates = [];
	}

	public function action(Info $info)
	{
		$memberId = $this->userService->getUser() ? $this->userService->getUser()->getId() : null;

		$id = $info->getRequest()->get('id');
		$info->getView()->id = $id;

		if ($serviceCareline = $this->carelineService->getService($memberId)) {
			foreach ($serviceCareline as $service) {
				$this->dates[Carbon::parse($service['Date'])->format('d-m-Y')] = $service['IsStock'];
			}
		}

		$info->getView()->dates = $this->dates;

		if($id !== 0) {
 			if ($detailCareline = $this->carelineService->getServiceDetail($id)) {
 				$info->getView()->detailCareline = $detailCareline;
 			}
		}

	}

}
