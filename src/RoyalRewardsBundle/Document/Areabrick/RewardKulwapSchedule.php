<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RewardKulwapSchedule.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 30, 2020
 * @time 17:18:32 PM
 *
 **/

namespace RoyalRewardsBundle\Document\Areabrick;

use RoyalRewardsBundle\Services\KulwapService;
use Pimcore\Model\Document\Tag\Area\Info;

class RewardKulwapSchedule extends AbstractAreabrick
{
    /** @var KulwapService $dashboardService */
    private $kulwapService;

    public function __construct(KulwapService $kulwapService)
    {
        $this->kulwapService = $kulwapService;
    }

    public function action(Info $info)
    {
        $info->getView()->scheduleKulwap = $this->kulwapService->getScheduleKulwap('0');
        $info->getView()->pagination = $this->kulwapService->pagination();
    }
}
