<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RecommendedArticle.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Aug 12, 2020
 * @time 2:32:19 AM
 *
 **/

namespace RoyalRewardsBundle\Document\Areabrick;

class RecommendedArticle extends AbstractAreabrick
{
}
