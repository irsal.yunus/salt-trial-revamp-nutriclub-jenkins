<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RewardWebinarSchedule.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 30, 2020
 * @time 17:18:32 PM
 *
 **/

namespace RoyalRewardsBundle\Document\Areabrick;

use RoyalRewardsBundle\Services\WebinarService;
use Pimcore\Model\Document\Tag\Area\Info;

class RewardWebinarSchedule extends AbstractAreabrick
{
    /** @var WebinarService $webinarService */
    private $webinarService;

    public function __construct(WebinarService $webinarService)
    {
        $this->webinarService = $webinarService;
    }

    public function action(Info $info)
    {
        $info->getView()->scheduleWebinar = $this->webinarService->getScheduleWebinar('0');
        $info->getView()->pagination = $this->webinarService->pagination();
    }
}
