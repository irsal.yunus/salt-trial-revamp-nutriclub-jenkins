pimcore.registerNS("pimcore.plugin.RoyalRewardsBundle");

pimcore.plugin.RoyalRewardsBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.RoyalRewardsBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("RoyalRewardsBundle ready!");
    }
});

var RoyalRewardsBundlePlugin = new pimcore.plugin.RoyalRewardsBundle();
