<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>

<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-Benefit.css'); ?>

<div class="p-4"></div>
<div class="content-rr_benefit">
  <div class="container">
    <div class="back">
      <img src="/assets/rrr_images/benefit/arrow.png" alt="lorep ipsum" /> Kembali
    </div>
    <div class="content-rr_title">
      <img src="/assets/rrr_images/benefit/iconCart.png" alt="lorep ipsum" />
      <h1>Benefit Tambahan</h1>
      <p>Anda bisa mendapatkan rewards tambahan dengan melakukan pembelian produk
        <b>Nutriclub 4x800g</b> selama 1 bulan</p>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="card-box-rr">
          <div class="card-box-rr_title">Total Gramasi Anda :<b> 1600g </b></div>
          <div class="card-box-rr_progress">
            <div class="progress">
              <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                aria-valuemax="100"></div>
            </div>
            <small>Anda membutuhkan <b>1600g</b> lagi untuk mendapatkan rewards</small>
            <div class="btn-upload">
              <input type="file" class="input-upload" />
              Upload Struk</div>
          </div>
        </div>

        <div class="card-box-rr">
          <a href="#">
            <div class="history">
              <img src="/assets/rrr_images/benefit/iconTime.png" alt="lorep ipsum" />
              <p>Lihat Histori Pembelian</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="label-bold">Rewards</div>
        <div class="card-box-rr p-0">
          <div class="img-cover orange">
            <img src="/assets/rrr_images/benefit/rockstar.png" alt="lorep ipsum" />
          </div>
          <div class="">
            <div class="card-box-rr_desc">
              <div class="d-flex align-items-center">
                <i class="fa fa-tag" aria-hidden="true"></i>
                <div class="pl-2">
                  <label>Nilai Voucher</label>
                  <p>Rp. 200.000,-</p>
                </div>
              </div>
              <div class="d-flex align-items-center">
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <div class="pl-2">
                  <label>Expire Date</label>
                  <p>21 Sept '20</p>
                </div>
              </div>
            </div>
            <div class="card-box-rr_button">
              <div>Detail</div>
              <div><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
            </div>
          </div>
        </div>

        <div class="card-box-rr p-0">
          <div class="img-cover blue">
            <img src="/assets/rrr_images/benefit/traveloka.png" alt="lorep ipsum" />
          </div>
          <div class="">
            <div class="card-box-rr_desc">
              <div class="d-flex align-items-center">
                <i class="fa fa-tag" aria-hidden="true"></i>
                <div class="pl-2">
                  <label>Nilai Voucher</label>
                  <p>Rp. 200.000,-</p>
                </div>
              </div>
              <div class="d-flex align-items-center">
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <div class="pl-2">
                  <label>Expire Date</label>
                  <p>21 Sept '20</p>
                </div>
              </div>
            </div>
            <div class="card-box-rr_button">
              <div>Detail</div>
              <div><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
            </div>
          </div>
        </div>


        <div class="done-claim">
          <div class="text-center">
            <img src="/assets/rrr_images/benefit/doneClaim.png" alt="lorep ipsum" />
            <p>Anda sudah mengklaim reward Anda</p>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
