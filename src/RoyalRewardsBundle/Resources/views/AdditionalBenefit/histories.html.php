<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>

<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-Purchase-History.css'); ?>

<?php $this->headScript()->offsetSetFile(10, '/assets/js/widgets/rr-modal-filter.js', 'text/javascript', []); ?>

<div class="container royal-poin-history">

    <div class="row">
      <div class="col-12 mt-5">
        <a href="#" class="btn-kembali">
          <img src="/assets/rrr_images/icon-royal/icon-arrow-left.png" alt="" />
          Kembali
        </a>
      </div>
    </div>

    <div class="row text-center royal-heading">
      <div class="col-12">
        <img src="/assets/rrr_images/icon-royal/icon-history.png" alt="" />
        <h1>Histori Pembelian Saya</h1>
        <p>Anda bisa melihat histori poin disini</p>
      </div>
    </div>

    <div class="row history-filter">
      <div class="col-12 text-center">
        <a href="#" class="history-filter__button">
          Terbaru <img src="/assets/rrr_images/icon-royal/icon-sort-amount.png" alt="" />
        </a>

        <button class="history-filter__button" data-toggle="modal" data-target="#modalFilter">
          Filter <img src="assets/images/icon-royal/icon-filter.png" alt="" />
        </button>

        <div class="modal fade" id="modalFilter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Filter Histori Poin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              
              <div class="modal-body">
                <h6>Kategori</h6>
                <div class="form-check">
                  <label class="form-check-label" for="exampleRadios1">
                    Berhasil
                  </label>
                  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1">
                </div>
                <div class="form-check">
                  <label class="form-check-label" for="exampleRadios2">
                    Sedang diproses
                  </label>
                  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                </div>
                <div class="form-check">
                  <label class="form-check-label" for="exampleRadios3">
                    Tidak Berhasil
                  </label>
                  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3">
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="apply-filter" data-dismiss="modal" disabled>Terapkan Filter</button>
                <button type="button" class="reset-filter" disabled>Reset Filter</button>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="history-list --pembelian">
      <div class="row history-list__item">
        <div class="col-8">
          <h4 class="yellow">Sedang diproses</h4>
          <p>Nutrilon Royal 800g x1</p>
          <span>Senin, 7 Juli 2020</span>
          <strong>Receipt ID : 198308840</strong>
        </div>
        <div class="col-4">
          <p class="history-list__poin">+800 Poin</p>
        </div>
      </div>
      <div class="row history-list__item">
        <div class="col-8">
          <h4 class="green">Berhasil</h4>
          <p>Nutrilon Royal 800g x1</p>
          <span>Senin, 7 Juli 2020</span>
          <strong>Receipt ID : 198308840</strong>
        </div>
        <div class="col-4">
          <p class="history-list__poin">+800 Poin</p>
        </div>
      </div>
      <div class="row history-list__item">
        <div class="col-8">
          <h4 class="red">Tidak Berhasil</h4>
          <p>Nutrilon Royal 800g x1</p>
          <span>Senin, 7 Juli 2020</span>
          <strong>Receipt ID : 198308840</strong>
        </div>
        <div class="col-4">
          <p class="history-list__poin">+800 Poin</p>
        </div>
      </div>
    </div>

    <nav class="royal-pagination" aria-label="page navigation">
      <ul class="pagination justify-content-center mt-5">
        <li class="page-item first"><a class="page-link" href="#">First</a></li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&lsaquo;</span>
          </a>
        </li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&rsaquo;</span>
          </a>
        </li>
        <li class="page-item last"><a class="page-link" href="#">Last</a></li>
      </ul>
    </nav>

</div>