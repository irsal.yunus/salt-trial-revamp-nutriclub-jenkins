<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource stage.partial.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 08/09/20
 * @time 22.19
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\Article;

/** @var \AppBundle\Model\DataObject\Stage $obj */
$obj = $this->obj;
$init = $this->init;
$count = $this->count;
$currentIndex = $this->currentIndex;
$except = $this->except;
$stageOffset = $this->stageOffset;

$article = new Article\Listing();
if ($except) {
    $article->setCondition('stage__id = ? AND oo_id NOT IN(' . implode(',', $except) . ')', [$obj->getId()]);
}
if (!$except) {
    $article->setCondition('stage__id = ?', [$obj->getId()]);
}
$article->setOrder('desc');
$article->setOrderKey('oo_id');
$article->setOffset($stageOffset);
$article->setLimit(1);

$html = '';

$objects = $article->getObjects();
$countObject = $article->getCount();

if ($countObject < 1) {
    $article = new Article\Listing();
    if ($except) {
        $article->setCondition('oo_id NOT IN(' . implode(',', $except) . ')', []);
    }
    $article->setOrder('desc');
    $article->setOrderKey('oo_id');
    $article->setOffset($stageOffset);
    $article->setLimit(1);

    $objects = $article->getObjects();
}

$html .= $this->render(
    '@RoyalRewardsBundle/Resources/views/Partials/Widgets/RecommendedArticle/global.partial.html.php',
    [
        'obj' => $objects[0],
        'init' => $init,
        'count' => $count,
        'currentIndex' => $currentIndex,
    ]
);

echo $html;
?>
