<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource random.partial.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 08/09/20
 * @time 21.47
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\Article;

$limit = $this->limit;
$except = $this->except;

$article = new Article\Listing();
if ($except) {
    $article->setCondition('oo_id NOT IN('. implode(',', $except) .')');
}
$article->setOrderKey("RAND()", false);
$article->setLimit(is_int($limit) ? $limit : 4);

$html = '';
$init = 0;

$objects = $article->getObjects();
$countObject = $article->getCount();

foreach ($objects as $key => $object) {
    $init++;

    $html .= $this->render(
        '@RoyalRewardsBundle/Resources/views/Partials/Widgets/RecommendedArticle/global.partial.html.php',
        [
            'obj' => $object,
            'init' => $init,
            'count' => $countObject,
            'currentIndex' => $key,
        ]
    );

    $init = $init >= 2 ? 0 : $init;
}

echo $html;
?>

