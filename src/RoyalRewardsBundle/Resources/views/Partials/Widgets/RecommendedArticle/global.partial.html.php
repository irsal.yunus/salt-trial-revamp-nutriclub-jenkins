<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource global.partial.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 08/09/20
 * @time 21.55
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\{Article};

$html = '';

/** @var Article $obj */
$obj = $this->obj;
$init = $this->init;
$count = $this->count;
$currentIndex = $this->currentIndex;

$html .= $init === 1 ? '<ul class="list-recommendation">' : null;
$html .= '<li>';
$html .= '<a href="'. $obj->getRouter() .'">';
$html .= $obj->getImgDekstopHtml('default', ['disableWidthHeightAttributes' => true]);
$html .= '<div class="recommendation-text">';
$html .= '<p>' . ($obj->getCategory() ? $obj->getCategory()->getName() : null) . '</p>';
$html .= '<h6>' . $obj->getTitle() . '</h6>';
$html .= '</div>';
$html .= '</a>';
$html .= '</li>';

$html .= $init === 2 || ($count === $currentIndex) ? '</ul>' : null;
$html .= $init === 2 || ($count === $currentIndex) ? '<div class="list-separator"></div>' : null;

echo $html;
