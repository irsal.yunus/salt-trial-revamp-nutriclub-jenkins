<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource empty.partial.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 09/09/20
 * @time 19.34
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use AppBundle\Model\DataObject\{Article, Stage};

$obj = null;
$init = $this->init;
$count = $this->count;
$currentIndex = $this->currentIndex;
$except = $this->except;
$offset = $this->offset;
$isCalculate = $this->isCalculate;

/** @var array $user */
$user = $this->session()->get('UserProfileRaw');
if ($user && $isCalculate) {
    $childs = $user['Childs'] ?? [];

    $youngestChild = \AppBundle\Helper\GeneralHelper::rearrangeChildBasedOnAge($childs);
    $dateFormat = env('RRR_LIVE') ? 'd/m/Y' : 'd-m-Y';

    $firstChild = $youngestChild[0] && $youngestChild[0]['Birthdate'] ?
        \Carbon\Carbon::createFromFormat($dateFormat, $youngestChild[0]['Birthdate'])->age : null;

    if ($firstChild !== null) {
        $obj = Stage::getBySlug($firstChild < 1 ? 'bayi' : 'balita', 1);
    }
}

$article = new Article\Listing();
if ($obj && $except) {
    $article->setCondition('stage__id = ? AND oo_id NOT IN('. implode(',', $except) .')', [
        $obj->getId()
    ]);
}
if ($obj && !$except) {
    $article->setCondition('stage__id = ?', [
        $obj->getId()
    ]);
}
$article->setOrder('desc');
$article->setOrderKey('oo_id');
$article->setOffset($offset);
$article->setLimit(1);

$html = '';

$objects = $article->getObjects();
$countObject = $article->getCount();

if ($countObject < 1) {
    return;
}

$html .= $this->render(
    '@RoyalRewardsBundle/Resources/views/Partials/Widgets/RecommendedArticle/global.partial.html.php',
    [
        'obj' => $objects[0],
        'init' => $init,
        'count' => $count,
        'currentIndex' => $currentIndex,
    ]
);

echo $html;
?>
