<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

	$this->extend('layout.html.php');
	
	$list_tier = [1 => 'Blue', 2 => 'Silver', 3 => 'Gold', 4 => 'Royal'];

	$member_dashboard = $this->member_dashboard;

	$percent_progress = ($member_dashboard['Point'] / $member_dashboard['Tier']['EndPoint']) * 100;
?>

<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-Dashboard.css'); ?>

<section id="user-info">
    <div class="container">
        <div class="title-wrapper">
            <h2 class="medium-title">Hai, <?php echo ucfirst($member_dashboard['FirstName']) . ' ' . ucfirst($member_dashboard['LastName']); ?></h2>
            <p class="subtitle">Status : <?= $member_dashboard['StagesValue']['Name']; ?></p>
            <div class="notif-bell-button">
<?php
	if($member_dashboard['UnreadNotif'] > 0) {
?>
                <span><?= $member_dashboard['UnreadNotif']; ?></span>
<?php
	}
?>
                <i class="fa fa-bell" aria-hidden="true"></i>
            </div>
        </div>
        <div class="tier-card-wrapper">
            <div class="left-content">
                <div class="user-tier-card <?= strtolower($member_dashboard['Tier']['Name']); ?>-tier">
                    <div class="badge-wrapper"></div>
                    <div class="tier-point-wrapper">
                        <div class="tier-status">
                            <h2><?= ucfirst($member_dashboard['Tier']['Name']); ?> Tier</h2>
                            <a href="/royal-rewards/tier">Lihat info tier</a>
                        </div>
                        <div class="point-info">
                            <img src="/assets/rrr_images/logo/star.png" alt="">
                            <h6><?= $member_dashboard['Point']; ?> Poin</h6>
                        </div>
                    </div>
                    <div class="point-progress-bar">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: <?= $percent_progress; ?>%" aria-valuenow="<?= $percent_progress; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="next-tier-info">
<?php 
	if(isset($list_tier[($member_dashboard['TierId'] + 1)])) {
?>
                        <p>Mama membutuhkan <b><?= ($member_dashboard['Tier']['EndPoint'] - $member_dashboard['Point']) + 1; ?></b> poin lagi untuk naik ke <b><?= $list_tier[($member_dashboard['TierId'] + 1)] ?></b></p>
<?php 
	}
?>
                    </div>
                </div>
            </div>
            <div class="right-content">
                <ul class="tools-card-wrapper">
                    <li onclick="location.assign('/royal-rewards/point/histories');">
                        <img src="/assets/rrr_images/content/card-logo/point-history.png" alt="">
                        <h6>Lihat Histori Poin Saya</h6>
                    </li>
                    <li onclick="location.assign('/royal-rewards/point');">
                        <img src="/assets/rrr_images/content/card-logo/get-point.png" alt="">
                        <h6>Lihat Cara Mendapatkan Poin</h6>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php echo $this->areablock('royalrewards-dashboardpage-areablock', [
    "allowed" => ["user-rewards", "recommended-article", "recommended-podcast", "recommended-tools"]
]); ?>

<!-- section id="user-rewards"></section -->

<!-- section id="recommended-article"></section -->

<!-- section id="recommended-podcast"></section -->

<!-- section id="recommended-tools"></section -->
