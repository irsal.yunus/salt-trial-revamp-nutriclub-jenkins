<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>

<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-Kulwap-List.css'); ?>


<div class="pt-5"></div>

<section id="button-back">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="/royal-rewards/dashboard" class="btn-kembali">
                    <img src="/assets/rrr_images/icon-royal/icon-arrow-left.png" alt="Icon Back">
                    Kembali
                </a>
            </div>
        </div>
    </div>
</section>

<?php echo $this->areablock('royalrewards-webinar-areablock', [
        "allowed" => ["reward-webinar-header", "reward-webinar-schedule"]
]); ?>