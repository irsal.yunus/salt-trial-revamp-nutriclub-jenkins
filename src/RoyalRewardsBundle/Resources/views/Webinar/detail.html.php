<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource detail.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Sep 5, 2020
 * @time 13:20:13 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-Kulwap-List-Detail.css');
$this->headScript()->offsetSetFile(10, '/assets/js/widgets/rr-modal-register.js', 'text/javascript', []); 

$urlImage   = ($detail['ListUrlImages'] > 0) ? current($detail['ListUrlImages']) : '/assets/rrr_images/homepage/hero-banner.png';
$mentorPicture = ($detail['ListUrlImagesMentor'] > 0) ? current($detail['ListUrlImagesMentor']) : '/assets/rrr_images/content/card-photo/male.png';
$category   = ucwords($detail['CatalogueCategory']['Name']);
$date       = \Carbon\Carbon::parse($schedule['CreatedDate'])->locale('id')->isoFormat("dddd, D MMMM YYYY");
$time       = \Carbon\Carbon::parse($schedule['CreatedDate'])->locale('id')->isoFormat("HH.ss");
?>

<div class="pt-5"></div>

<section id="button-back">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= $this->path('rrr-dashboard') ?>" class="btn-kembali">
                    <img src="/assets/rrr_images/icon-royal/icon-arrow-left.png" alt="Icon Back">
                    <?= $this->t('BACK') ?>
                </a>
            </div>
        </div>
    </div>
</section>

<section id="header-content" class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="header-list-detail">
                <h3><?= $category ?></h3>
                <h1><?= $detail['Name'] ?></h1>
                <p><?= $date ?>, Jam <?= $time ?> WIB</p>
            </div>
        </div>
    </div>
</section>

<section id="body-content" class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="detail-content">
                <img class="image-content" src="<?= $urlImage ?>" alt="">
                <h3>Pembicara :</h3>
                <div class="speaker">
                    <img src="<?= $mentorPicture ?>" alt="">
                    <h6><?= $detail['MentorName'] ?></h6>
                </div>
                <?= html_entity_decode($detail['Description']) ?>
            </div>
        </div>
        <div class="col-md-8">
            <div class="register-services">
                <?php if (!$detail['IsRedeemed']) { ?>
                <button class="register-button" data-toggle="modal" data-target="#modal-register">Daftar Webinar Ini</button>
                <?php } ?>
                <div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div id="main-register" class="modal-content">
                            <h5>Apakah Anda sudah yakin memilih Webinar ini?</h5>
                            <p>
                                Setelah daftar Webinar, maka tidak bisa dibatalkan atau tukar dengan topik lain. Pastikan topik dan jadwal sudah sesuai.
                            </p>
                            <button type="button" class="accept" onclick="postRedemption('webinar', <?= $detail['ID'] ?>)">Ya, Sudah Yakin</button>
                            <button type="button" class="decline" data-dismiss="modal">Belum, Saya ingin ganti</button>
                        </div>

                        <?php if ($redemption['modal']) {
                            if ($redemption['success']) {?>
                                <!-- Modal when Success -->
                                <div id="success-register" class="modal-content">
                                    <img src="/assets/rrr_images/icon-royal/success-webinar.png" alt="Icon Webinar" />
                                    <h5>Anda berhasil daftar Webinar!</h5>
                                    <p>
                                        Setelah daftar Webinar, maka tidak bisa dibatalkan atau tukar dengan topik lain. Pastikan topik dan jadwal sudah sesuai.
                                    </p>
                                    <button type="button" class="decline" onclick="hideRegister()">Kembali</button>
                                </div>
                            <?php } else { ?>
                                <!-- Modal when Fail -->
                                <div id="success-register" class="modal-content">
                                    <img src="/assets/rrr_images/icon-royal/fail-webinar.png" alt="Icon Webinar" />
                                    <h5>Anda tidak berhasil daftar Webinar!</h5>
                                    <p>
                                        <?= ucwords($redemption['message']) ?>
                                    </p>
                                    <p>
                                        Silahkan mencoba kembali untuk mendaftar di webinar
                                    </p>
                                    <button type="button" class="decline" onclick="hideFailRegister()">Kembali</button>
                                </div>
                        <?php }} ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>