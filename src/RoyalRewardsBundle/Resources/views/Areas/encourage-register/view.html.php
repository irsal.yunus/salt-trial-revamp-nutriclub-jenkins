<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:19:13 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

	$register_title = (!$this->input('encourage-register-title')->isEmpty()) ? $this->input('encourage-register-title')->getData() : 'Yuk Mama, daftar sekarang untuk mendapatkan Rewards!';
	$register_text = (!$this->input('encourage-register-text')->isEmpty()) ? $this->input('encourage-register-text')->getData() : 'Dengan menjadi member Royal Rewards,Mama akan mendapatkan banyak benefit dan informasi terupdate dari kami';
	$register_button_title = (!$this->input('encourage-register-button-text')->isEmpty()) ? $this->input('encourage-register-button-text')->getData() : 'daftar';

?>

<section id="encourage-register">
    <div class="container">
        <h3 class="medium-title"><?= $register_title; ?></h3>
        <p class="subtitle"><?= $register_text; ?></p>
        <button class="lg-button-bluish" onclick="location.assign('<?= $this->path('account-register') ?>');"><?= $register_button_title; ?></button>
    </div>
</section>
