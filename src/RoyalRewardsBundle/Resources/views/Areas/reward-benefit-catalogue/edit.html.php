<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:18:32 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

?>

<h4>Reward Benefit Logo</h4>
<?= $this->image('reward-benefit-catalogue-logo', [
		'title' => 'Drag your image here',
		'thumbnail'   => 'reward-benefit-catalogue-logo-thumbnail',
		'uploadPath'  => '/royalrewards/images/',
		'reload'      => false,
		'hidetext'    => true
	])
?>
<h4>Reward Benefit Title</h4>
<?= $this->input('reward-benefit-catalogue-title', []) ?>

<h4>Reward Benefit Text</h4>
<?= $this->input('reward-benefit-catalogue-text', []) ?>

<hr>

<h4>Catalogue</h4>
<h5>Title</h5>
<?= $this->input('reward-benefit-catalogue-card-title', []) ?>
<?php 
	$no = 1;
	while($this->block('catalogue-card-block')->loop()) { 
?>
	<h5>Card Image <?= $no ?></h5>
	<?= $this->image('reward-benefit-catalogue-card', [
			'title' => 'Drag your image here',
			'thumbnail'   => 'reward-benefit-catalogue-card-thumbnail',
			'uploadPath'  => '/royalrewards/images/',
			'reload'      => false,
			'hidetext'    => true
		])
	?>
	<h5>Card Text <?= $no ?></h5>
	<?= $this->input('reward-benefit-catalogue-card-text', []) ?>
<?php
		$no++;
	}
?>

<hr>
<h4>Information Title</h4>
<?= $this->input('reward-benefit-information-title', []) ?>
<h4>Information Text</h4>
<?= $this->input('reward-benefit-information-text', []) ?>
<h4>Information Image</h4>
<?= $this->image('reward-benefit-information-image', [
		'title' => 'Drag your image here',
		'thumbnail'   => 'reward-benefit-information-image-thumbnail',
		'uploadPath'  => '/royalrewards/images/',
		'reload'      => false,
		'hidetext'    => true
	])
?>
<h4>Information Link</h4>
<?= $this->link('reward-benefit-information-link-image', [])?>
