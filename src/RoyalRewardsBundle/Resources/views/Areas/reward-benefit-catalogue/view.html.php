<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:19:13 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

	$reward_logo = (!$this->image('reward-benefit-catalogue-logo')->isEmpty()) ? $this->image('reward-benefit-catalogue-logo')->getThumbnail('reward-benefit-catalogue-logo-thumbnail')
					->getHtml([
						'class' => 'reward-logo'
					]) : NULL;

	$reward_title = (!$this->input('reward-benefit-catalogue-title')->isEmpty()) ? $this->input('reward-benefit-catalogue-title')->getData() : 'Dapatkan Poinnya dengan Mengakses Konten dan Tools';
	$reward_text = (!$this->input('reward-benefit-catalogue-text')->isEmpty()) ? $this->input('reward-benefit-catalogue-text')->getData() : 'Mama bisa mendapatkan poin rewards dengan mengakses konten yang ada di Nutriclub.';
	
	$card_title = (!$this->input('reward-benefit-catalogue-card-title')->isEmpty()) ? $this->input('reward-benefit-catalogue-card-title')->getData() : 'Berikut konten yang bisa Mama akses :';

	$cards = '';
	while ($this->block('catalogue-card-block')->loop()) {
		$card_text = (!$this->input('reward-benefit-catalogue-card-text')->isEmpty()) ? $this->input('reward-benefit-catalogue-card-text')->getData() : '';
		if (!$this->image('reward-benefit-catalogue-card')->isEmpty()) {
			$cards .= '
            <li>
                ' . $this->image('reward-benefit-catalogue-card', [])
						->getThumbnail('reward-benefit-catalogue-card-thumbnail')
						->getHtml() . '
                <h6>' . $card_text . '</h6>
            </li>
';
		}
	}
	
	$tiering_info = '';
	if (!$this->image('reward-benefit-information-image')->isEmpty()) {
		$info_title = (!$this->input('reward-benefit-information-title')->isEmpty()) ? $this->input('reward-benefit-information-title')->getData() : '';
		$info_text = (!$this->input('reward-benefit-information-text')->isEmpty()) ? $this->input('reward-benefit-information-text')->getData() : '';
		$info_link = (!$this->input('reward-benefit-information-link-image')->isEmpty()) ? $this->input('reward-benefit-information-link-image')->getHref() : '/royal-rewards/tier';

		$tiering_info .= '
		<p class="subtitle">' . $info_title . '</p>
        <div class="button-tiering-info">
            <a href="' . $info_link . '" class="button-content-wrapper">
                <div class="button-image">' .
                $this->image('reward-benefit-information-image', [])
	                ->getThumbnail('reward-benefit-information-image-thumbnail')
	                ->getHtml()
                . '</div>
                <div class="button-text">
                    <h6>Informasi Tiering</h6>
                    <p>' . $info_text . '</p>
                </div>
            </a>
        </div>
';
		
	}
?>

<section id="reward-benefit-catalogue">

    <div class="container">
        <?= $reward_logo; ?>
        <h3 class="medium-title"><?= $reward_title; ?></h3>
        <p class="subtitle"><?= $reward_text; ?></p>
        <p class="subtitle gotham-medium"><?= $card_title; ?></p>

        <ul class="tools-card-wrapper">
        	<?= $cards; ?>
            <!-- li>
                <img src="/assets/rrr_images/content/card-logo/podcast.png" alt="">
                <h6>Podcast</h6>
            </li>
            <li>
                <img src="/assets/rrr_images/content/card-logo/article.png" alt="">
                <h6>Artikel</h6>
            </li>
            <li>
                <img src="/assets/rrr_images/content/card-logo/video.png" alt="">
                <h6>Video</h6>
            </li>
            <li>
                <img src="/assets/rrr_images/content/card-logo/digital-tools.png" alt="">
                <h6>Digital Tools</h6>
            </li -->
        </ul>
        <?= $tiering_info; ?>
    </div>
</section>
