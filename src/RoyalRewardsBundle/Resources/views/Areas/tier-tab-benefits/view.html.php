<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since 06/07/2020
 * @time 15:25
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

    <ul class="nav nav-pills royal-tab justify-content-center" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="blue-tab" data-toggle="tab" href="#blue-tab-content" role="tab" aria-controls="blue-tab-content" aria-selected="false">Blue</a>
      </li>
    </ul>

    <div class="tab-content">
      <div class="tab-pane fade show active" id="blue-tab-content" role="tabpanel" aria-labelledby="blue-tab">
        <div class="row justify-content-center">
          <div class="col-12 col-md-10">

            <!-- AFTER LOGIN HTML DIFFERENCE -->
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="royal-tier blue">
                  <div class="royal-tier__info">
                    <div class="royal-tier__img">
                      <img src="/assets/rrr_images/icon-royal/tier-blue.png" alt="">
                    </div>
                    <div class="royal-tier__content">
                      <h2>Blue Tier</h2>
                      <p>Blue tier adalah tier pertama saat pertama kali masuk ke Royal rewards.</p>
                      <div class="royal-tier__poin d-sm-block">
                        <p>0-600 poin</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6 pt-sm-1">
                <div class="cara-alert">
                  <h5><?= $this->input("note-title"); ?></h5>
                  <p><?= $this->wysiwyg("note-description"); ?></p>
                </div>
              </div>
            </div>
            <!-- END AFTER LOGIN HTML DIFFERENCE-->

            <div class="royal-card-benefit">
              <h3 class="text-center"><?= $this->input("main-benefit-title"); ?></h3>
              <p class="text-center"><?= $this->wysiwyg("main-benefit-description"); ?></p>
              <div class="royal-card-benefit__wrap row justify-content-center">
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-kulwap.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Kulwap dengan topik menarik</h4>
                      <p>Dapatkan informasi terupdate lewat kuliah Whatsapp</p>
                      <div class="absolute-icon">2x</div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-montessori.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="royal-card-benefit">
              <h3 class="text-center"><?= $this->input("additional-benefit-title"); ?></h3>
              <p class="text-center"><?= $this->wysiwyg("additional-benefit-description"); ?></p>
              <div class="royal-card-benefit__wrap row justify-content-center">
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-parentstory.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>