<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 30, 2020
 * @time 17:18:32 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/
?>

<h4>Title:</h4>
<?= $this->input('title', []) ?>

<h4>Description:</h4>
<?= $this->input('sub-title', []) ?>

<h4>Sub Title:</h4>
<?= $this->input('info-text-new', []) ?>

<h4>Info Title:</h4>
<?= $this->input('info-text-update', []) ?>

<h4>Info Items:</h4>
<?php 
	$no = 1;
	while($this->block('webinar-detail-info-item-block')->loop()) { 
	
?>
    <h4>Info  <?= $no ?></h4>
	<?= $this->input('info-item', []) ?>

<?php
		$no++;
	}
?>