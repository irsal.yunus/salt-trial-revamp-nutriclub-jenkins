<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 30, 2020
 * @time 17:18:32 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/
	$info_title = (!$this->input('title')->isEmpty()) ? $this->input('title')->getData() : '';
	$info_subtitle = (!$this->input('sub-title')->isEmpty()) ? $this->input('sub-title')->getData() : '';
	$info_text_new = (!$this->input('info-text-new')->isEmpty()) ? $this->input('info-text-new')->getData() : '';
	$info_text_update = (!$this->input('info-text-update')->isEmpty()) ? $this->input('info-text-update')->getData() : '';

	$items = '';
	$no = 1;
	while ($this->block('webinar-detail-info-item-block')->loop()) {
		if (!$this->input('info-item')->isEmpty()) {
			$items .= '<p class="dropdown-item"><span>' . $no . '.</span>' . $this->input('info-item')->getData() . '</p>';
			$no++;
		}
	}

?>

<section class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="header-detail">
				<img src="/assets/rrr_images/icon-royal/icon-webinar.png" alt="Icon Kulwap" />
				<h1><?= $info_title; ?></h1>
				<p><?= $info_subtitle; ?></p>
                <p class="highlight"><?= $info_text_new; ?></p>
			</div>
		</div>
		<div class="col-md-8">
			<div class="dropdown dropdown-info">
				<button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?= $info_text_update; ?>
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<?= $items; ?>

				</div>
			</div>
		</div>
	</div>
</section>

<div class="separator-rr"></div>
