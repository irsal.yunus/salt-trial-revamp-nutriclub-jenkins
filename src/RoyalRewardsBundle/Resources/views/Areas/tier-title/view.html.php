<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since 06/07/2020
 * @time 15:25
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
    <div class="row text-center royal-heading">
      <div class="col-12">
        <?= $this->image("logo", [
            'uploadPath' => '/royal-rewards/pages/'
        ]); ?>
        <h1><?= $this->input("title"); ?></h1>
        <p><?= $this->wysiwyg("description"); ?></p>
      </div>
    </div>