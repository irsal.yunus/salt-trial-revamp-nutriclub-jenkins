<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since 06/07/2020
 * @time 15:25
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Logo :</h4>
<?= $this->image('logo', [
        'uploadPath' => '/royal-rewards/pages/'
    ]) ?>

<h4>Title :</h4>
<?= $this->input('title', []) ?>

<h4>Description :</h4>
<?= $this->wysiwyg('description', []) ?>
