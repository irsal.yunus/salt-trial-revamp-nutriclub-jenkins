<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Aug 30, 2020
 * @time 1:39:45 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/
?>

<h4>Title :</h4>
<?= $this->input('title', []) ?>

<h4>Sub Title :</h4>
<?= $this->input('sub-title', []) ?>

<h4>Info Text New:</h4>
<?= $this->input('info-text-new', []) ?>

<h4>Info Text Update:</h4>
<?= $this->input('info-text-update', []) ?>

<h4>Item Info :</h4>
<?php 
	$no = 1;
	while($this->block('careline-detail-info-item-block')->loop()) { 
	
?>
    <h4>Info  <?= $no ?></h4>
	<?= $this->input('info-item', []) ?>

<?php
		$no++;
	}
?>