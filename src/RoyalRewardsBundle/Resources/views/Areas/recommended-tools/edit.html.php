<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:18:32 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

?>
<h4>Recommended Tools</h4>
<h5>Title</h5>
<?= $this->input('recommended-tools-title', []) ?>
<h5>Text</h5>
<?= $this->input('recommended-tools-text', []) ?>
<h4>Tools Block :</h4>
<?php while($this->block('recommended-tools-card-block')->loop()) { ?>
    <h4>Tools</h4>
    <?= $this->relation('tools', [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Tool'],
    ]) ?>
<?php } ?>
<h5>Link : </h5>
<?= $this->link('recommended-tools-other', []) ?>
