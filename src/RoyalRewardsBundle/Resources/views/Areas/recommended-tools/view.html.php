<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:19:13 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

	$recommended_tools_title = (!$this->input('recommended-tools-title')->isEmpty()) ? $this->input('recommended-tools-title')->getData() : 'Dapatkan Poinnya dengan Mengakses Konten dan Tools';
	$recommended_tools_text = (!$this->input('recommended-tools-text')->isEmpty()) ? $this->input('recommended-tools-text')->getData() : 'Mama bisa mendapatkan poin rewards dengan mengakses konten yang ada di Nutriclub.';

	$cards = '';
	while ($this->block('recommended-tools-card-block')->loop()) {
	    /** @var \AppBundle\Model\DataObject\Tool $tools */
        $tools = $this->relation('tools')->getElement();
        if (!$tools) {
            continue;
        }
        //$tools->getRouter();
        //$tools->getCurrentLink()->getHref();
        $cards .= '
        <li>
            <a href="'. $tools->getCurrentLink()->getHref() .'">
            ' . $tools->getIconThumbnail('', ['disableWidthHeightAttributes' => true]) . '
            <h6>' . $tools->getName() . '</h6>
            </a>
        </li>';

	}

?>

<div class="separator-rr"></div>

<section id="recommended-tools">
    <div class="container">
        <h2 class="medium-title"><?= $recommended_tools_title ?></h2>
        <p class="subtitle"><?= $recommended_tools_text ?></p>
        <ul class="tools-card-wrapper">
        	<?= $cards ?>
        </ul>
        <?= $this->link('recommended-tools-other', [
            'class' => 'see-more-link'
        ])->frontend() ?>
    </div>
</section>
