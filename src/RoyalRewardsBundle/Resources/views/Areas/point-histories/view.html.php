<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 20, 2020
 * @time 5:19:13 PM
 *
 **/

    $pointHistoryTitle = (!$this->input('point-histories-title')->isEmpty()) ? $this->input('point-histories-title')->getData() : 'Histori Poin Saya';
    $pointHistoryDesc = (!$this->input('point-histories-desc')->isEmpty()) ? $this->input('point-histories-desc')->getData() : 'Anda bisa melihat histori poin disini';
    function getPoint($type, $poin) {
        if ($type == 'C') {
            return ($poin > 0) ? '+'.$poin : $poin;
        } else {
            return ($poin > 0) ? '-'.$poin : $poin;
        }
    }
?>  


    <div class="row text-center royal-heading">
      <div class="col-12">
        <img src="/assets/rrr_images/icon-royal/icon-history.png" alt="" />
        <h1><?= $pointHistoryTitle ?></h1>
        <p><?= $pointHistoryDesc ?></p>
      </div>
    </div>

    <div class="row history-filter">
      <div class="col-12 text-center">
        <button class="history-filter__button history-filter-newest__button">
          Terbaru <img src="/assets/rrr_images/icon-royal/icon-sort-amount.png" alt="" />
        </button>
        
        <button class="history-filter__button history-filter-type__button" data-toggle="modal" data-target="#modalFilter">
          Filter <img src="/assets/rrr_images/icon-royal/icon-filter.png" alt="" />
        </button>

        <div class="modal fade" id="modalFilter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Filter Histori Poin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              
              <div class="modal-body">
                <h6>Kategori</h6>
                <?php foreach ($actionCodes as $key => $action) { ?>
                <div class="form-check">
                  <label class="form-check-label" for="action-<?= $key ?>">
                    <?= ucwords($action['Text']) ?>
                  </label>
                  <input class="form-check-input" type="radio" name="type" id="action-<?= $key ?>" value="<?= $key ?>">
                </div>
                <?php } ?>
              </div>

              <div class="modal-footer">
                <button type="button" class="apply-filter" data-dismiss="modal" disabled>Terapkan Filter</button>
                <button type="button" class="reset-filter" disabled>Reset Filter</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="history-list">

    <?php
    foreach ($pointHistories as $key => $history) { ?>
      <div class="row history-list__item">
        <div class="col-8">
          <h4><?= $history['ActionName'] ?></h4>
          <a href="#"><?= $history['Description'] ?></a>
          <span><?= \Carbon\Carbon::parse($reward['CreatedDate'])->locale('id')->isoFormat("dddd, D MMMM YYYY") ?></span>
        </div>
        <div class="col-4">
          <p class="history-list__poin"><?= getPoint($history['Type'], $history['Ponts']) ?> Poin</p>
        </div>
      </div>
    <?php } ?>
      
    </div>

    <nav class="royal-pagination" aria-label="page navigation">
      <ul class="pagination justify-content-center mt-5">
        <?php
        foreach ($pagination as $key => $value) { ?>
          <li class="page-item <?= $value['class'] ?>"><button class="page-link" value="<?= $value['value'] ?>"><?= $value['text'] ?></button></li>
        <?php } ?>
      </ul>
    </nav>
