<?php
use Pimcore\Model\WebsiteSetting;

/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:19:13 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

	$login_title = (!$this->input('encourage-login-title')->isEmpty()) ? $this->input('encourage-login-title')->getData() : 'Dapatkan Rewards dengan Menjadi Member Royal Rewards';
	$login_text = (!$this->input('encourage-login-text')->isEmpty()) ? $this->input('encourage-login-text')->getData() : 'Login/Register untuk menggunakan Royal Rewards';
	$login_button_title = (!$this->input('encourage-login-button-text')->isEmpty()) ? $this->input('encourage-login-button-text')->getData() : 'masuk';

?>

<section id="encourage-login">
    <div class="container">
        <h3 class="medium-title"><?= $login_title; ?></h3>
        <p class="subtitle"><?= $login_text; ?></p>
        <button class="lg-button-bluish" onclick="location.assign('<?= $this->path('account-login') ?>');"><?= $login_button_title; ?></button>
    </div>
</section>
