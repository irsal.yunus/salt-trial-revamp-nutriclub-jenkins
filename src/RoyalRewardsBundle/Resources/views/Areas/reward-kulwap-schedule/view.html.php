<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 30, 2020
 * @time 17:18:32 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

	$rewardScheduleTitle = (!$this->input('kulwap-schedule-title')->isEmpty()) ? $this->input('kulwap-schedule-title')->getData() : 'Jadwal Kulwap';
?>

<section id="list-kulwap" class="container service-content-wrapper">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="list-title"><?= $rewardScheduleTitle; ?></h1>
        </div>
        <div class="col-md-12">
            <ul class="list-reward-card">
            <?php
            foreach ($this->scheduleKulwap as $key => $schedule) { ?>
                <li class="reward-card kulwap">
                    <div class="reward-card-header">
                        <p><?= $schedule['Topic'] ?></p>
                        <h6><?= ucwords($schedule['Name']) ?></h6>
                    </div>
                    <div class="reward-card-body">
                        <div class="time-and-date">
                            <div>
                                <img src="/assets/rrr_images/content/card-logo/calendar-icon.png" alt="">
                                <p><?= \Carbon\Carbon::parse($schedule['StartDate'])->locale('id')->isoFormat("dddd, D MMMM 'YY") ?></p>
                            </div>
                            <div>
                                <img src="/assets/rrr_images/content/card-logo/time-icon.png" alt="">
                                <p><?= \Carbon\Carbon::parse($schedule['StartDate'])->locale('id')->isoFormat("HH.ss") ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="reward-card-footer">
                        <a href="/royal-rewards/kulwap/<?= $schedule['ID'] ?>" class="detail-link">
                            <p>Detail</p>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
            <?php } ?>
            </ul>
        </div>
    </div>

    <nav class="royal-pagination" aria-label="page navigation">
        <ul class="pagination justify-content-center mt-5">
        <?php
        foreach ($pagination as $key => $value) { ?>
          <li class="page-item <?= $value['class'] ?>"><button class="page-link" value="<?= $value['value'] ?>"><?= $value['text'] ?></button></li>
        <?php } ?>
        </ul>
    </nav>
</section>
