<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:18:32 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

?>

<h4>Title :</h4>
<?= $this->input('title', []) ?>

<h4>Sub Title :</h4>
<?= $this->wysiwyg('sub-title', []) ?>

<h4>Podcast Block :</h4>
<?php while ($this->block('podcast-block')->loop()) { ?>
    <h4>Podcast</h4>
    <?= $this->relation('podcast', [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['ArticlePodcast'],
    ]) ?>
<?php } ?>

<h4>See More : </h4>
<?= $this->link('more', []) ?>
