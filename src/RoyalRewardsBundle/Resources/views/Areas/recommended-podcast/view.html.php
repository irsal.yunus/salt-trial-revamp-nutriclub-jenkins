<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:19:13 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

?>

<div class="separator-rr"></div>

<section id="recommended-podcast">
    <div class="container">
        <h2 class="medium-title">
            <?= $this->input('title')->frontend() ?>
        </h2>
        <div class="subtitle">
            <?= $this->wysiwyg('sub-title', [
                'class' => 'subtitle'
            ])->frontend() ?>
        </div>
        <?php
        $html = '';
        $init = 0;
        ?>
        <?php while ($this->block('podcast-block')->loop()) { ?>
            <?php
            $currentIndex = (int)$this->block('podcast-block')->getCurrentIndex();
            $count = $this->block('podcast-block')->getCount();
            /** @var \AppBundle\Model\DataObject\ArticlePodcast $articlePodcast */
            $articlePodcast = $this->relation('podcast')->getElement();
            $init++;

            $html .= $init === 1 ? '<ul class="list-recommendation">' : null;
            if ($articlePodcast instanceof \AppBundle\Model\DataObject\ArticlePodcast) {
                //$articlePodcast->getRouter();

                $html .= '<li>';
                $html .= '<a href="'. $articlePodcast->getRouter() .'">';
                $html .= $articlePodcast->getImageDesktop()->getThumbnail('default')->getHtml(['disableWidthHeightAttributes' => true]);
                $html .= '<div class="recommendation-text">';
                $html .= '<p>' . ($articlePodcast->getCategory() ? $articlePodcast->getCategory()->getName() : null) . '</p>';
                $html .= '<h6>' . $articlePodcast->getTitle() . '</h6>';
                $html .= '</div>';
                $html .= '</a>';
                $html .= '</li>';
            }
            $html .= $init === 2 || ($count === $currentIndex) ? '</ul>' : null;
            $html .= $init === 2 || ($count === $currentIndex) ? '<div class="list-separator"></div>' : null;

            $init = $init >= 2 ? 0 : $init;
            ?>
        <?php } ?>
        <?= $html ?>
        <?= $this->link('more', [
            'class' => 'see-more-link'
        ]) ?>
    </div>
</section>
