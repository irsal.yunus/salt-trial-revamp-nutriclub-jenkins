<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:19:13 PM
 *
 * @var PhpEngine $this
 * @var PhpEngine $view
 * @var GlobalVariables $app
 *
 **/

use Carbon\Carbon;
use Pimcore\Templating\GlobalVariables;
use Pimcore\Templating\PhpEngine;

$customerRewards = $this->customerRewards;
?>

<div class="separator-rr"></div>

<section id="user-rewards">

    <div class="container">
        <h2 class="medium-title"><?= $this->t('RRR_YOUR_REWARDS') ?></h2>
        <p class="subtitle"><?= $this->t('RRR_SEE_ALL_REWARDS_HERE') ?></p>

        <div class="rr-tab-wrapper">
            <ul class="nav nav-tabs rr-tab" id="reward-tab" role="tablist">
            <?php
            foreach ($customerRewards as $key => $value) { ?>
                <li class="nav-item">
                    <a class="nav-link <?= !$key ? 'active' : '' ?>" id="<?= $value['name'] ?>-tab" data-toggle="tab" href="#<?= $value['name'] ?>" role="tab" aria-selected="true"><?= $this->t("RRR_".strtoupper($value['name'])) ?></a>
                </li>
            <?php } ?>
            </ul>
        </div>

        <div class="tab-content rr-tab-content" id="reward-tab-content">

            <?php
            foreach ($customerRewards as $key => $category) {
                if ($category['name'] === 'kulwap') { ?>
                    <!-- Kulwap-tab -->
                    <div class="tab-pane fade <?= !$key ? 'show active' : '' ?>" id="<?= $category['name'] ?>" role="tabpanel" aria-labelledby="<?= $category['name'] ?>-tab">
                        <div class="reward-chance-card">
                            <img src="/assets/rrr_images/content/card-logo/kulwap.png" alt="">
                            <p><?= $this->t('RRR_KULWAP_OPPORTUNITIES') ?> <b><?= $category['total']['chance'] ?></b></p>
                        </div>
                        <?php if (count($category['value']) > 0) { ?>
                        <ul class="list-reward-card">
                            <?php
                            foreach ($category['value'] as $reward) { ?>
                            <li class="reward-card <?= $category['name'] ?>">
                                <div class="reward-card-header">
                                    <p><?= $reward['Topic'] ?? '' ?></p>
                                    <h6><?= ucwords($reward['Name']) ?></h6>
                                </div>
                                <div class="reward-card-body">
                                    <div class="time-and-date">
                                        <div>
                                            <img src="/assets/rrr_images/content/card-logo/calendar-icon.png" alt="">
                                            <p><?= Carbon::parse($reward['StartDate'])->locale('id')->isoFormat('dddd, D MMMM \'YY') ?></p>
                                        </div>
                                        <div>
                                            <img src="/assets/rrr_images/content/card-logo/time-icon.png" alt="">
                                            <p><?= Carbon::parse($reward['StartDate'])->isoFormat("HH.mm") ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="reward-card-footer">
                                    <a href="/royal-rewards/kulwap/<?= $reward['ID'] ?>" class="detail-link">
                                        <p><?= $this->t('DETAIL') ?></p>
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } else { ?>
                        <div class="unregistered-reward">
                            <img src="/assets/rrr_images/content/card-logo/kulwap-grey.png" alt="">
                            <p><?= $this->t('RRR_KULWAP_DONT_HAVE') ?></p>
                        </div>
                        <?php } ?>
                        <?php if ($category['total']['chance'] > '0') {?>
                            <button class="mt-5 lg-button-bluish" onclick="window.location.href = '/royal-rewards/<?= $category['name'] ?>'"><?= $this->t('RRR_BTN_CHOOSE_KULWAP') ?></button>
                        <?php } ?>
                    </div>
                <?php }
                if ($category['name'] === 'webinar') { ?>
                    <!-- Webinar-tab -->
                    <div class="tab-pane fade" id="<?= $category['name'] ?>" role="tabpanel" aria-labelledby="<?= $category['name'] ?>-tab">
                        <div class="reward-chance-card">
                            <img src="/assets/rrr_images/content/card-logo/webinar.png" alt="">
                            <p><?= $this->t('RRR_WEBINAR_OPPORTUNITIES') ?> <b><?= $category['total']['chance'] ?></b></p>
                        </div>
                        <?php if (count($category['value']) > 0) { ?>
                        <ul class="list-reward-card">
                            <?php
                            foreach ($category['value'] as $reward) { ?>
                            <li class="reward-card webinar">
                                <div class="reward-card-header">
                                    <p><?= $reward['Topic'] ?? '' ?></p>
                                    <h6><?= ucwords($reward['Name']) ?></h6>
                                </div>
                                <div class="reward-card-body">
                                    <div class="time-and-date">
                                        <div>
                                            <img src="/assets/rrr_images/content/card-logo/calendar-icon.png" alt="">
                                            <p><?= Carbon::parse($reward['StartDate'])->locale('id')->isoFormat("dddd, D MMMM 'YY") ?></p>
                                        </div>
                                        <div>
                                            <img src="/assets/rrr_images/content/card-logo/time-icon.png" alt="">
                                            <p><?= Carbon::parse($reward['StartDate'])->locale('id')->isoFormat("HH.ss") ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="reward-card-footer">
                                    <a href="/royal-rewards/webinar/<?= $reward['ID'] ?>" class="detail-link">
                                        <p><?= $this->t('DETAIL') ?></p>
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } else { ?>
                        <div class="unregistered-reward">
                            <img src="/assets/rrr_images/content/card-logo/webinar-grey.png" alt="">
                            <p><?= $this->t('RRR_WEBINAR_DONT_HAVE') ?></p>
                        </div>
                        <?php } ?>
                        <?php if ($category['total']['chance'] > '0') {?>
                        <button class="mt-5 lg-button-bluish" onclick="window.location.href = '/royal-rewards/<?= $category['name'] ?>'"><?= $this->t('RRR_BTN_CHOOSE_WEBINAR') ?></button>
                        <?php } ?>
                    </div>
                <?php }
                if ($category['name'] === 'careline') { ?>
                    <!-- Careline-tab -->
                    <div class="tab-pane fade" id="<?= $category['name'] ?>" role="tabpanel" aria-labelledby="<?= $category['name'] ?>-tab">
                        <div class="reward-chance-card">
                            <img src="/assets/rrr_images/content/card-logo/careline.png" alt="">
                            <p><?= $this->t('RRR_CARELINE_OPPORTUNITIES') ?> <b><?= $category['total']['chance'] ?></b></p>
                        </div>
                        <?php if (count($category['value']) > 0) { ?>
                        <ul class="list-reward-card">
                            <?php
                            foreach ($category['value'] as $reward) { ?>
                            <li class="reward-card careline">
                                <div class="reward-card-header">
                                    <p><?= $this->t('RRR_CARELINE_CONSULT_WITH') ?></p>
                                    <h6><?= ucwords($reward['Name']) ?></h6>
                                </div>
                                <div class="reward-card-body">
                                    <div class="time-and-date">
                                        <div>
                                            <img src="/assets/rrr_images/content/card-logo/calendar-icon.png" alt="">
                                            <p><?= Carbon::parse($reward['StartDate'])->locale('id')->isoFormat("dddd, D MMMM 'YY") ?></p>
                                        </div>
                                        <div>
                                            <img src="/assets/rrr_images/content/card-logo/time-icon.png" alt="">
                                            <p><?= Carbon::parse($reward['StartDate'])->locale('id')->isoFormat("HH.ss") ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="reward-card-footer">
                                    <a href="/royal-rewards/careline/<?= $reward['ID'] ?>" class="detail-link">
                                        <p><?= $this->t('RRR_CARELINE_CHANGE') ?></p>
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } else { ?>
                        <div class="unregistered-reward">
                            <img src="/assets/rrr_images/content/card-logo/careline-grey.png" alt="">
                            <p><?= $this->t('RRR_CARELINE_DONT_HAVE') ?></p>
                        </div>
                        <?php } ?>
                        <?php if ($category['total']['chance'] > '0') {?>
                        <button class="mt-5 lg-button-bluish" onclick="window.location.href = '/royal-rewards/<?= $category['name'] ?>'"><?= $this->t('RRR_BTN_CHOOSE_CARELINE') ?></button>
                        <?php } ?>
                    </div>
                <?php }
                if ($category['name'] === 'voucher') { ?>
                    <!-- Voucher-tab -->
                    <div class="tab-pane fade" id="<?= $category['name'] ?>" role="tabpanel" aria-labelledby="<?= $category['name'] ?>-tab">
                        <?php if (count($category['value']) > 0) { ?>
                        <ul class="list-reward-card">
                            <?php
                            foreach ($category['value'] as $reward) { ?>
                            <li class="reward-card voucher">
                                <div class="reward-card-header" style="background-image: url(<?= isset($reward['Images'][0]) ? $reward['Images'][0] : '/assets/rrr_images/content/card-bg/montessori.png' ?>);">
                                    <p><?= ucwords($category['name']) ?> Royal Rewards</p>
                                    <h6><?= ucwords($reward['Name']) ?></h6>
                                </div>
                                <div class="reward-card-body">
                                    <div class="time-and-date">
                                        <div>
                                            <img src="/assets/rrr_images/content/card-logo/tag-icon.png" alt="">
                                            <div class="two-text">
                                                <p><?= $this->t('RRR_VOUCHER_VALUE') ?></p>
                                                <h6>Rp. <?= number_format($reward['Nominal'],0,',','.') ?>,-</h6>
                                            </div>
                                        </div>
                                        <div>
                                            <img src="/assets/rrr_images/content/card-logo/time-icon.png" alt="">
                                            <div class="two-text">
                                                <p><?= $this->t('RRR_VOUCHER_EXPIRED_DATE') ?></p>
                                                <h6><?= Carbon::parse($reward['StartDate'])->locale('id')->isoFormat("D MMM 'YY") ?></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="reward-card-footer">
                                    <a href="<?= $this->path('rrr-voucher-detail', [
                                        'id' => $reward['ID'] ?? 0
                                    ]) ?>" class="detail-link">
                                        <p><?= $this->t('DETAIL') ?></p>
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } else { ?>
                        <div class="unregistered-reward">
                            <img src="/assets/rrr_images/content/card-logo/voucher-grey.png" alt="">
                            <p><?= $this->t('RRR_VOUCHER_DONT_HAVE') ?></p>
                        </div>
                        <?php } ?>
                        <button class="mt-5 lg-button-bluish" onclick="window.location.href = '/royal-rewards/additional-benefit'"><?= $this->t('RRR_BTN_CHOOSE_ADDITIONAL_BENEFIT') ?></button>
                    </div>
            <?php
                }
            }
            ?>

        </div>
    </div>
</section>

