<?php
use Carbon\Carbon;

/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Aug 30, 2020
 * @time 1:39:01 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

$detailCarelineDate = null;
$detailCarelineTime = null;

if($this->id !== 0) {
	$detailCareline = $this->detailCareline;
	$detailCarelineDate = Carbon::parse($detailCareline['DateTime'] ?? null)->format('d-m-Y');
    $detailCarelineTime = Carbon::parse($detailCareline['DateTime'] ?? null)->format('H:i:s');
}

?>
<script type="text/javascript">
	var cId = <?= $this->id; ?>;
</script>
<section id="schedule-careline" class="container service-content-wrapper">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<h1 class="list-title">Jadwal Careline</h1>
		</div>

		<div class="col-md-8">
			<div class="date-list-wrapper">
				<h6>Pilih Tanggal</h6>
				<ul class="date-list">
<?php
	$startDate = Carbon::now();
	$startDate->addDays(2);

	$optDate = Carbon::now();
	$optDate->addDays(2);
	for($i = 0; $i < 10; $i++) {
		$optDate->addDays(1);
		$optDate->setLocale('id');

		$optDateFormated = $optDate->format('d-m-Y');
?>
					<li class="
					    <?= !$this->dates[$optDateFormated] && $optDateFormated !== $detailCarelineDate ? 'disabled' : null ?>
					    <?= $optDateFormated !== $detailCarelineDate ? null : 'selected' ?>
                    ">
						<p class="date" value="<?= $optDate->isoFormat('D MMMM Y') ?>" data-date="<?= $optDate->format('Y-m-d') ?>"><?= $optDate->isoFormat('D MMM') ?></p>
						<p class="day" value="<?= $optDate->isoFormat('dddd') ?>"><?= $optDate->isoFormat('ddd') ?></p>
					</li>
<?php
	}
?>
				</ul>

				<h6>Pilih Jam</h6>

				<div class="time-list-preloader">
                    <?= $this->render('Include/preloader.html.php') ?>
                </div>

				<ul class="time-list"></ul>

				<button class="register-button disabled" data-toggle="modal" data-target="#modal-register" onclick="getTimeDate()" disabled>
                    <?= $this->id ? $this->t('RRR_CARELINE_CHANGE_SCHEDULE') : $this->t('RRR_CARELINE_CREATE_SCHEDULE') ?>
                </button>
				<div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div id="main-register" class="modal-content">
							<h5>Apakah Jadwal yang Anda Pilih Sudah Benar?</h5>
							<p class="detail-date">Hari : <span class="day"></span>, <span class="date"></span></p>
							<p class="detail-time">Jam : <span class="time"></span></p>
							<p>
								Setelah membuat jadwal konsultasi, Anda hanya bisa <strong>1 kali</strong> mengubah jadwal dengan batas waktu <strong>H-2</strong>.
							</p>
							<button type="button" class="accept" onclick="carelinePostRedeem()">Ya, Sudah Benar</button>
							<button type="button" class="decline" data-dismiss="modal">Belum, Saya ingin ganti</button>
						</div>

						<!-- Modal when Success -->
						<div id="success-register" class="modal-content">
							<img src="/assets/rrr_images/icon-royal/success-careline.png" alt="Icon Kulwap" />
							<h5>Anda berhasil membuat jadwal!</h5>
							<p>
								Tim Careline kami akan menghubungi Anda sesuai jadwal yang Anda buat
							</p>
							<button type="button" class="decline" data-dismiss="modal" onclick="hideRegister()">Kembali</button>
						</div>

						<!-- Modal when Fail -->
						<div id="fail-register" class="modal-content">
							<img src="/assets/rrr_images/icon-royal/fail-careline.png" alt="Icon Kulwap" />
							<h5>Anda tidak berhasil daftar Kulwap!</h5>
							<p>
								Silahkan mencoba kembali untuk membuat jadwal
							</p>
							<button type="button" class="decline" onclick="hideFailRegister()">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
