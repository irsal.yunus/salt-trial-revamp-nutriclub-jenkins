<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:18:32 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

?>

<h4>Board of Resilient Experts</h4>
<h5>Image</h5>
	<?= $this->image('board-of-resilient-image', [
			'title' => 'Drag your image here',
			'thumbnail'   => 'board-of-resilient-image-thumbnail',
			'uploadPath'  => '/royalrewards/images/',
			'reload'      => false,
			'hidetext'    => true
		])
	?>
<h5>Title</h5>
<?= $this->input('board-of-resilient-title', []) ?>
<h5>Text</h5>
<?= $this->input('board-of-resilient-text', []) ?>

<hr>

<h4>Expert</h4>
<?php 
	$no = 1;
	while($this->block('board-of-resilient-expert-block')->loop()) { 
?>
	<h5>Expert Image <?= $no ?></h5>
	<?= $this->image('board-of-resilient-expert-image', [
			'title' => 'Drag your image here',
			'thumbnail'   => 'board-of-resilient-expert-image-thumbnail',
			'uploadPath'  => '/royalrewards/images/',
			'reload'      => false,
			'hidetext'    => true
		])
	?>
	<h5>Expert Name</h5>
	<?= $this->input('board-of-resilient-expert-name', []) ?>
	<h5>Expert Title</h5>
	<?= $this->input('board-of-resilient-expert-title', []) ?>
<?php
		$no++;
	}
?>

<hr>

<h4>Personalized Rewards</h4>
<h5>Title</h5>
<?= $this->input('personalized-reward-title', []) ?>
<h5>Text</h5>
<?= $this->input('personalized-reward-text', []) ?>
<?php 
	$no = 1;
	while($this->block('personalized-reward-block')->loop()) { 
?>
	<h5>Card Image <?= $no ?></h5>
	<?= $this->image('personalized-reward-card-image', [
			'title' => 'Drag your image here',
			'thumbnail'   => 'personalized-reward-card-image-thumbnail',
			'uploadPath'  => '/royalrewards/images/',
			'reload'      => false,
			'hidetext'    => true
		])
	?>
	<h5>Title</h5>
	<?= $this->input('personalized-reward-card-title', []) ?>
	<h5>Text</h5>
	<?= $this->input('personalized-reward-card-text', []) ?>
<?php
		$no++;
	}
?>

<hr>

<h4>Partner</h4>
<h5>Title</h5>
<?= $this->input('reward-benefit-partner-title', []) ?>
<h5>Text</h5>
<?= $this->input('reward-benefit-partner-text', []) ?>
<?php 
	$no = 1;
	while($this->block('reward-benefit-partner-block')->loop()) { 
?>
	<h5>Partner Image <?= $no ?></h5>
	<?= $this->image('reward-benefit-partner-image', [
			'title' => 'Drag your image here',
			'thumbnail'   => 'reward-benefit-partner-thumbnail',
			'uploadPath'  => '/royalrewards/images/',
			'reload'      => false,
			'hidetext'    => true
		])
	?>
<?php
		$no++;
	}
?>
