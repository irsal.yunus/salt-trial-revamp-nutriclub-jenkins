<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:19:13 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/
	
	$resilient_logo = (!$this->image('board-of-resilient-image')->isEmpty()) ? $this->image('board-of-resilient-image')->getThumbnail('board-of-resilient-image-thumbnail')
		->getHtml([
			'class' => 'img-resilient',
			'alt' => 'Board of Resilient'
		]) : NULL;
	
	$resilient_title = (!$this->input('board-of-resilient-title')->isEmpty()) ? $this->input('board-of-resilient-title')->getData() : 'PERSONALIZED INTERACTION WITH THE EXPERTS';
	$resilient_text = (!$this->input('board-of-resilient-text')->isEmpty()) ? $this->input('board-of-resilient-text')->getData() : 'Nutriclub berkomitmen memberikan yang terbaik untuk Mama dan Papa. Board of Resilient adalah bukti kami untuk Mama dan Papa dengan memberikan interaksi personal langsung dari para expert kami.';
	
	$experts = '';
	while ($this->block('board-of-resilient-expert-block')->loop()) {
		$expert_name = (!$this->input('board-of-resilient-expert-name')->isEmpty()) ? $this->input('board-of-resilient-expert-name')->getData() : '';
		$expert_title = (!$this->input('board-of-resilient-expert-title')->isEmpty()) ? $this->input('board-of-resilient-expert-title')->getData() : '';
		if (!$this->image('board-of-resilient-expert-image')->isEmpty()) {
			$experts .= '
            <li class="expert-card">
                ' . $this->image('board-of-resilient-expert-image', [])
		                ->getThumbnail('board-of-resilient-expert-image-thumbnail')
		                ->getHtml() . '
                <h6>' . $expert_name . '</h6>
                <p>' . $expert_title . '</p>
            </li>
';
		}
	}
	
	$personalized_title = (!$this->input('personalized-reward-title')->isEmpty()) ? $this->input('personalized-reward-title')->getData() : 'Personalized Rewards';
	$personalized_text = (!$this->input('personalized-reward-text')->isEmpty()) ? $this->input('personalized-reward-text')->getData() : 'Papa dan Mama akan mendapatkan rewards yang berupa konten khusus yang dibuat oleh para expert Board of Resilient. Konten tersebut berupa :';
	
	$personalized = '';
	while ($this->block('personalized-reward-block')->loop()) {
		$personalized_card_title = (!$this->input('personalized-reward-card-title')->isEmpty()) ? $this->input('personalized-reward-card-title')->getData() : '';
		$personalized_card_text = (!$this->input('personalized-reward-card-text')->isEmpty()) ? $this->input('personalized-reward-card-text')->getData() : '';
		if (!$this->image('personalized-reward-card-image')->isEmpty()) {
			$personalized .= '
            <li class="reward-card">
                ' . $this->image('personalized-reward-card-image', [])
		                ->getThumbnail('personalized-reward-card-image-thumbnail')
		                ->getHtml() . '
		        <div class="reward-card-content">
		            <h6>' . $personalized_card_title . '</h6>
                    <p>' . $personalized_card_text . '</p>
                </div>
            </li>
';
		}
	}
	
	$partner_title = (!$this->input('reward-benefit-partner-title')->isEmpty()) ? $this->input('reward-benefit-partner-title')->getData() : 'Royal Rewards’s Partnership';
	$partner_text = (!$this->input('reward-benefit-partner-text')->isEmpty()) ? $this->input('reward-benefit-partner-text')->getData() : 'Untuk memberikan yang terbaik, Nutriclub Royal Rewards bekerja sama dengan expert-expert lain.';
	
	$partners = '';
	while ($this->block('reward-benefit-partner-block')->loop()) {
		if (!$this->image('reward-benefit-partner-image')->isEmpty()) {
			$partners .= '
            <li>
                ' . $this->image('reward-benefit-partner-image', [])
                ->getThumbnail('reward-benefit-partner-thumbnail')
                ->getHtml() . '
		    </li>
';
		}
	}
	
?>

<section id="resilient-board">
    <div class="container">
        <h3 class="medium-title">Perkenalkan</h3>
        <?= $resilient_logo ?>
        <h6 class="small-title"><?= $resilient_title; ?></h6>
        <p class="subtitle"><?= $resilient_text; ?></p>
        <h3 class="medium-title">Board of Resilient Experts :</h3>
        <ul class="expert-card-slider"><?= $experts ?>
            <!-- li class="expert-card">
                <img src="/assets/rrr_images/content/card-photo/female.png" alt="">
                <h6>Prof. Dr. Mariabella SumawaJumali, M. Pg, Sp. M, M. Gm</h6>
                <p>Peditrician</p>
            </li>
            <li class="expert-card">
                <img src="/assets/rrr_images/content/card-photo/male.png" alt="">
                <h6>Prof. Dr. Alesandro Kumbara, Sp. G, Sp. M</h6>
                <p>Peditrician</p>
            </li>
            <li class="expert-card">
                <img src="/assets/rrr_images/content/card-photo/female.png" alt="">
                <h6>Dr. Anindita Florensia</h6>
                <p>Psychology</p>
            </li>
            <li class="expert-card">
                <img src="/assets/rrr_images/content/card-photo/male.png" alt="">
                <h6>Jouska</h6>
                <p>Financial Advisor</p>
            </li>
            <li class="expert-card">
                <img src="/assets/rrr_images/content/card-photo/female.png" alt="">
                <h6>Prof. Dr. Mariabella SumawaJumali, M. Pg, Sp. M, M. Gm</h6>
                <p>Peditrician</p>
            </li -->
        </ul>
        <a href="" class="see-more-link">Lihat Lebih Banyak</a>
        <h3 class="medium-title"><?= $personalized_title ?></h3>
        <p class="subtitle"><?= $personalized_text ?></p>
        <ul class="reward-card-wrapper">
        	<?= $personalized ?>
            <!-- li class="reward-card">
                <img src="/assets/rrr_images/content/card-logo/kulwap.png" alt="">
                <div class="reward-card-content">
                    <h6>Kulwap dengan topik menarik</h6>
                    <p>Dapatkan informasi terupdate lewat kuliah Whatsapp</p>
                </div>
            </li>
            <li class="reward-card">
                <img src="/assets/rrr_images/content/card-logo/webinar.png" alt="">
                <div class="reward-card-content">
                    <h6>Webinar dengan Expert</h6>
                    <p>Mama bisa mengikuti seminar online dengan para expert</p>
                </div>
            </li>
            <li class="reward-card">
                <img src="/assets/rrr_images/content/card-logo/careline.png" alt="">
                <div class="reward-card-content">
                    <h6>Careline Ekslusif</h6>
                    <p>Kami menyediakan Careline khusus untuk Royal Rewards</p>
                </div>
            </li -->
        </ul>
        <h3 class="medium-title"><?= $partner_title; ?></h3>
        <p class="subtitle"><?= $partner_text; ?></p>
        <ul class="partner-list">
        	<?= $partners; ?>
            <!-- li><img src="/assets/rrr_images/content/brand/montessori.png" alt=""></li>
            <li><img src="/assets/rrr_images/content/brand/halodoc.png" alt=""></li>
            <li><img src="/assets/rrr_images/content/brand/rockstar-gym.png" alt=""></li -->
        </ul>
    </div>        
</section>
