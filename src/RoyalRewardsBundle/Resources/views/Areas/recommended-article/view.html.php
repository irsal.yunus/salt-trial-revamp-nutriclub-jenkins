<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:19:13 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

use AppBundle\Model\DataObject\{Article, Stage};
use Pimcore\Model\DataObject\StageGroup;

?>

<div class="separator-rr"></div>

<section id="recommended-article">
    <div class="container">
        <h2 class="medium-title">
            <?= $this->input('title')->frontend() ?>
        </h2>
        <div class="subtitle">
            <?= $this->wysiwyg('sub-title', [
                'class' => 'subtitle'
            ])->frontend() ?>
        </div>
        <?php
        $html = '';
        $init = 0;
        $except = [];
        $stageArrayStack = [];
        ?>
        <?php while ($this->block('article-block')->loop()) { ?>
            <?php
            $currentIndex = (int)$this->block('article-block')->getCurrentIndex();
            $count = $this->block('article-block')->getCount();
            /** @var Article|Stage|StageGroup $obj */
            $obj = $this->relation('article')->getElement();
            $init++;

            if ($obj instanceof Article) {
                $except[] = $obj->getId();

                $html .= $this->render(
                    '@RoyalRewardsBundle/Resources/views/Partials/Widgets/RecommendedArticle/global.partial.html.php',
                    [
                        'obj' => $obj,
                        'init' => $init,
                        'count' => $count,
                        'currentIndex' => $currentIndex,
                    ]
                );
            }

            if ($obj instanceof Stage) {
                $stageArrayStack[$obj->getId()][] = 1;
                $stageOffset = count($stageArrayStack[$obj->getId()]);

                $html .= $this->render(
                    '@RoyalRewardsBundle/Resources/views/Partials/Widgets/RecommendedArticle/stage.partial.html.php',
                    [
                        'obj' => $obj,
                        'init' => $init,
                        'count' => $count,
                        'currentIndex' => $currentIndex,
                        'except' => $except,
                        'stageOffset' => $stageOffset
                    ]
                );
            }

            if ($this->relation('article')->isEmpty()) {
                $offset[] = 1;
                $isCalculate = $this->checkbox('calculate-based-on-child')->isChecked();

                $html .= $this->render(
                    '@RoyalRewardsBundle/Resources/views/Partials/Widgets/RecommendedArticle/empty.partial.html.php',
                    [
                        'init' => $init,
                        'count' => $count,
                        'currentIndex' => $currentIndex,
                        'except' => $except,
                        'offset' => count($offset),
                        'isCalculate' => $isCalculate
                    ]
                );
            }

            $init = $init >= 2 ? 0 : $init;
            ?>
        <?php } ?>

        <?= $this->block('article-block')->isEmpty() ?
            $this->render('@RoyalRewardsBundle/Resources/views/Partials/Widgets/RecommendedArticle/random.partial.html.php', [
                'limit' => 4,
                'except' => []
            ]) : ''
        ?>

        <?= $html ?>
        <?= $this->link('more', [
            'class' => 'see-more-link'
        ]) ?>
    </div>
</section>
