<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:18:32 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/
?>

<h4>Title :</h4>
<?= $this->input('title', []) ?>

<h4>Sub Title :</h4>
<?= $this->wysiwyg('sub-title', []) ?>

<h4>Article Block :</h4>
<?php while ($this->block('article-block')->loop()) { ?>
    <h4>Article/Stage/StageGroup</h4>
    <?= $this->relation('article', [
        'width' => '500px',
        'types' => ['object'],
        'subtypes' => [
            'object' => ['object']
        ],
        'classes' => ['Article', 'Stage', 'StageGroup'],
    ]) ?>
    <span>Info : If empty will render random content.</span>

    <h4>Calculate Based On Latest Child Age(GUM/BOP), Let previous field empty</h4>
    <?= $this->checkbox('calculate-based-on-child', []) ?>
<?php } ?>

<h4>See More : </h4>
<?= $this->link('more', []) ?>
