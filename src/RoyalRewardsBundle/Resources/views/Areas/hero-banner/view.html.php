<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:19:13 PM
 *
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 * 
 */
?>

<section id="hero-banner" class="">
    <ul class="slidable-hero-banner">
<?php 
	while ($this->block('hero-banner-slider-block')->loop()) { 
		$tags = '
 			<img class="bg-imagebanner-desktop" src="/assets/rrr_images/homepage/hero-banner.png" alt="">
            <img class="bg-imagebanner-mobile" src="/assets/rrr_images/homepage/hero-banner-mobile.png" alt="">
 		';
		if (
			!$this->image('hero-banner-slider-desktop')->isEmpty() &&
			!$this->image('hero-banner-slider-mobile')->isEmpty()) {
			$imgDesktop = $this->image('hero-banner-slider-desktop', [])
								->getThumbnail('hero-banner-slider-desktop-thumbnail')
								->getHtml([
									'class' => 'bg-imagebanner-desktop'
								]);
			
			$imgMobile = $this->image('hero-banner-slider-mobile', [])
								->getThumbnail('hero-banner-slider-mobile-thumbnail')
								->getHtml([
									'class' => 'bg-imagebanner-mobile'
								]);
			
			$tags = $imgDesktop . $imgMobile;
		}
?>
        <li class="banner-image">
        	<?= $tags; ?>
        </li>
<?php 
	}
?>
    </ul>
</section>
