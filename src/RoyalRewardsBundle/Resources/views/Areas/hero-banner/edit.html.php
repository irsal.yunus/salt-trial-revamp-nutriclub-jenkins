<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource edit.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Jul 30, 2020
 * @time 5:18:32 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

?>
<h4>Banner Slider</h4>
<?php 
	$no = 1;
	while($this->block('hero-banner-slider-block')->loop()) { 
?>
		<h5>Dekstop Banner <?= $no ?></h5>
		<?= $this->image('hero-banner-slider-desktop', [
			'title' => 'Drag your image here',
			'thumbnail'   => 'hero-banner-slider-desktop-thumbnail',
			'uploadPath'  => '/banner-slider/desktop/',
			'reload'      => false,
			'hidetext'    => true
		])
		?>
		<h5>Mobile Banner <?= $no ?></h5>
		<?= $this->image('hero-banner-slider-mobile', [
			'title' => 'Drag your image here',
			'thumbnail'   => 'hero-banner-slider-mobile-thumbnail',
			'uploadPath'  => '/banner-slider/mobile/',
			'reload'      => false,
			'hidetext'    => true
		])
		?>
<?php
		$no++;
	}
?>