<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>

<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-Info-Tier.css'); ?>

<div class="container info-tier">

    <div class="row">
      <div class="col-12 mt-5">
        <a href="/royal-rewards/dashboard" class="btn-kembali">
          <img src="/assets/rrr_images/icon-royal/icon-arrow-left.png" alt="" />
          Kembali
        </a>
      </div>
    </div>

    <div class="row text-center royal-heading">
      <div class="col-12">
        <img src="/assets/rrr_images/icon-royal/icon-tier.png" alt="" />
        <h1>Informasi Tier Royal Rewards</h1>
        <p>Royal Rewards Nutriclub memiliki sistem mekanisme yang baru. Agar Mama tidak bingung dalam menggunakan sistem baru ini, kami menyediakan informasi mengenai sistem baru ini.</p>
      </div>
    </div>

    <!-- TAB BUTTON -->
    <ul class="nav nav-pills royal-tab justify-content-center" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link" id="blue-tab" data-toggle="tab" href="#blue-tab-content" role="tab" aria-controls="blue-tab-content" aria-selected="true">Blue</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="silver-tab" data-toggle="tab" href="#silver-tab-content" role="tab" aria-controls="silver-tab-content" aria-selected="false">Silver</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="gold-tab" data-toggle="tab" href="#gold-tab-content" role="tab" aria-controls="gold-tab-content" aria-selected="false">Gold</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" id="royal-tab" data-toggle="tab" href="#royal-tab-content" role="tab" aria-controls="royal-tab-content" aria-selected="false">Royal</a>
      </li>
    </ul>
    <!-- END TAB BUTTON -->

    <!-- TAB TIER KONTEN -->
    <div class="tab-content">

      <!-- BLUE-->
      <div class="tab-pane fade" id="blue-tab-content" role="tabpanel" aria-labelledby="blue-tab">
        <div class="row justify-content-center">
          <div class="col-12 col-md-10">

            <div class="royal-tier blue">
              <div class="royal-tier__info">
                <div class="royal-tier__img">
                  <img src="/assets/rrr_images/icon-royal/tier-blue.png" alt="">
                </div>
                <div class="royal-tier__content">
                  <h2>Blue Tier</h2>
                  <p>Blue tier adalah tier pertama saat pertama kali masuk ke Royal rewards.</p>
                  <div class="royal-tier__poin d-block d-sm-none">
                    <p>0-600 poin</p>
                  </div>
                </div>
              </div>
              <div class="royal-tier__poin d-none d-sm-block">
                <p>0-600 poin</p>
              </div>
            </div>

            <div class="royal-card-benefit">
              <h3 class="text-center">Benefit Utama</h3>
              <p class="text-center">Di tier ini, Mama akan mendapatkan berbagai macam rewards yang bisa dilihat dibawah ini:</p>
              <div class="royal-card-benefit__wrap row justify-content-center">
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-kulwap.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Kulwap dengan topik menarik</h4>
                      <p>Dapatkan informasi terupdate lewat kuliah Whatsapp</p>
                      <div class="absolute-icon">2x</div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-montessori.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="royal-card-benefit">
              <h3 class="text-center">Benefit Tambahan</h3>
              <p class="text-center">Mama bisa mendapatkan rewards tambahan dengan melakukan<br/>pembelian produk <strong>Nutriclub 4x800g</strong> selama 1 bulan</p>
              <div class="royal-card-benefit__wrap row justify-content-center">
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-parentstory.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- END BLUE -->

      <!-- SILVER-->
      <div class="tab-pane fade" id="silver-tab-content" role="tabpanel" aria-labelledby="silver-tab">
        
        <div class="row justify-content-center">
          <div class="col-12 col-md-10">

            <div class="royal-tier silver">
              <div class="royal-tier__info">
                <div class="royal-tier__img">
                  <img src="/assets/rrr_images/icon-royal/tier-silver.png" alt="">
                </div>
                <div class="royal-tier__content">
                  <h2>Silver Tier</h2>
                  <p>Silver tier adalah tier diatas Blue setelah Mama mencapai batas poin</p>
                  <div class="royal-tier__poin d-block d-sm-none">
                    <p>601 - 1200 Poin</p>
                  </div>
                </div>
              </div>
              <div class="royal-tier__poin d-none d-sm-block">
                <p>601 - 1200 Poin</p>
              </div>
            </div>
    
            <div class="royal-card-benefit">
              <h3 class="text-center">Benefit Utama</h3>
              <p class="text-center">Di tier ini, Mama akan mendapatkan berbagai macam rewards yang bisa dilihat dibawah ini:</p>
              <div class="royal-card-benefit__wrap row justify-content-center">
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-kulwap.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Kulwap dengan topik menarik</h4>
                      <p>Dapatkan informasi terupdate lewat kuliah Whatsapp</p>
                      <div class="absolute-icon">2x</div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-careline.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Konsultasi dengan Careline</h4>
                      <p>Careline Ekslusif Royal Rewards siap membantu Mama kapan saja</p>
                      <div class="absolute-icon">2x</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="royal-card-benefit">
              <h3 class="text-center">Benefit Tambahan</h3>
              <p class="text-center">Mama bisa mendapatkan rewards tambahan dengan melakukan<br/>pembelian produk <strong>Nutriclub 4x800g</strong> selama 1 bulan</p>
              <div class="royal-card-benefit__wrap row justify-content-center">
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-montessori.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
      <!-- END SILVER-->

      <!-- GOLD -->
      <div class="tab-pane fade" id="gold-tab-content" role="tabpanel" aria-labelledby="gold-tab">
        
        <div class="row justify-content-center">
          <div class="col-12 col-md-10">

            <div class="royal-tier gold">
              <div class="royal-tier__info">
                <div class="royal-tier__img">
                  <img src="/assets/rrr_images/icon-royal/tier-gold.png" alt="">
                </div>
                <div class="royal-tier__content">
                  <h2>Gold Tier</h2>
                  <p>Gold Tier adalah tier diatas Silver setelah Mama mencapai batas poin</p>
                  <div class="royal-tier__poin d-block d-sm-none">
                    <p>1201 - 1800 Poin</p>
                  </div>
                </div>
              </div>
              <div class="royal-tier__poin d-none d-sm-block">
                <p>1201 - 1800 Poin</p>
              </div>
            </div>
    
            <div class="royal-card-benefit">
              <h3 class="text-center">Benefit Utama</h3>
              <p class="text-center">Di tier ini, Mama akan mendapatkan berbagai macam rewards yang bisa dilihat dibawah ini:</p>
              <div class="royal-card-benefit__wrap row justify-content-center">
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-kulwap.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Kulwap dengan topik menarik</h4>
                      <p>Dapatkan informasi terupdate lewat kuliah Whatsapp</p>
                      <div class="absolute-icon">1x</div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-careline.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Konsultasi dengan Careline</h4>
                      <p>Careline Ekslusif Royal Rewards siap membantu Mama kapan saja</p>
                      <div class="absolute-icon">2x</div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-webinar.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Webinar dengan Expert</h4>
                      <p>Ikuti webinar yang dibawakan oleh para expert Nutriclub</p>
                      <div class="absolute-icon">2x</div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-montessori.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-halodoc.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="royal-card-benefit">
              <h3 class="text-center">Benefit Tambahan</h3>
              <p class="text-center">Mama bisa mendapatkan rewards tambahan dengan melakukan<br/>pembelian produk <strong>Nutriclub 4x800g</strong> selama 1 bulan</p>
              <div class="royal-card-benefit__wrap row justify-content-center">
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-rockstar.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
      <!-- END GOLD -->

      <!-- ROYAL-->
      <div class="tab-pane fade show active" id="royal-tab-content" role="tabpanel" aria-labelledby="royal-tab">
        <div class="row justify-content-center">
          <div class="col-12 col-md-10">

            <div class="royal-tier royal">
              <div class="royal-tier__info">
                <div class="royal-tier__img">
                  <img src="/assets/rrr_images/icon-royal/tier-royal.png" alt="">
                </div>
                <div class="royal-tier__content">
                  <h2>Royal Tier</h2>
                  <p>Royal Tier adalah tier diatas Gold setelah mencapai batas poin</p>
                  <div class="royal-tier__poin d-block d-sm-none">
                    <p>1801 - 2400 Poin</p>
                  </div>
                </div>
              </div>
              <div class="royal-tier__poin d-none d-sm-block">
                <p>1801 - 2400 Poin</p>
              </div>
            </div>
    
            <div class="royal-card-benefit">
              <h3 class="text-center">Benefit Utama</h3>
              <p class="text-center">Di tier ini, Mama akan mendapatkan berbagai macam rewards yang bisa dilihat dibawah ini:</p>
              <div class="royal-card-benefit__wrap row justify-content-center">
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-kulwap.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Kulwap dengan topik menarik</h4>
                      <p>Dapatkan informasi terupdate lewat kuliah Whatsapp</p>
                      <div class="absolute-icon">1x</div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-careline.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Konsultasi dengan Careline</h4>
                      <p>Careline Ekslusif Royal Rewards siap membantu Mama kapan saja</p>
                      <div class="absolute-icon">2x</div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-webinar.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Webinar dengan Expert</h4>
                      <p>Ikuti webinar yang dibawakan oleh para expert Nutriclub</p>
                      <div class="absolute-icon">2x</div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-montessori.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-halodoc.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="royal-card-benefit">
              <h3 class="text-center">Benefit Tambahan</h3>
              <p class="text-center">Mama bisa mendapatkan rewards tambahan dengan melakukan<br/>pembelian produk <strong>Nutriclub 4x800g</strong> selama 1 bulan</p>
              <div class="royal-card-benefit__wrap row justify-content-center">
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-rockstar.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="royal-card-benefit__item">
                    <img src="/assets/rrr_images/icon-royal/icon-voucher.png" class="royal-card-benefit__img" alt="">
                    <div class="royal-card-benefit__content">
                      <h4>Voucher <span class="inline-voucher"><img src="/assets/rrr_images/icon-royal/voucher-traveloka.png" alt=""></span></h4>
                      <p>Dapatkan potongan sebesar <strong>50%</strong> dengan voucher ini</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- END ROYAL -->


      <!-- MEKANISME -->
      <div class="royal-card-mekanisme">
        <h3 class="text-center">Mekanisme Mendapatkan Poin</h3>
        <p class="text-center">Untuk mendapatkan poin, ada mekanisme yang harus Mama lakukan. </p>

        <div class="royal-card-mekanisme__wrap row justify-content-center">
          <div class="col-12 col-md-3">
            <div class="royal-card-mekanisme__item">
              <img src="/assets/rrr_images/icon-royal/mekanisme-1.png" class="royal-card-mekanisme__img" alt="">
              <div class="royal-card-mekanisme__content">
                <h4>1. Daftar di Royal Rewards</h4>
                <p>Mama harus terlebih dahulu daftar menjadi member Royal Rewards</p>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-3">
            <div class="royal-card-mekanisme__item">
              <img src="/assets/rrr_images/icon-royal/mekanisme-3.png" class="royal-card-mekanisme__img" alt="">
              <div class="royal-card-mekanisme__content">
                <h4>2. Mengakses Konten</h4>
                <p>Untuk mendapat poin, Mama harus mengakses konten di Nutriclub</p>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-3">
            <div class="royal-card-mekanisme__item">
              <img src="/assets/rrr_images/icon-royal/mekanisme-4.png" class="royal-card-mekanisme__img" alt="">
              <div class="royal-card-mekanisme__content">
                <h4>3. Poin Akan Otomatis Masuk</h4>
                <p>Setelah selesai mengakses konten, poin akan masuk ke akun Mama</p>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-3">
            <div class="royal-card-mekanisme__item">
              <img src="/assets/rrr_images/icon-royal/mekanisme-2.png" class="royal-card-mekanisme__img" alt="">
              <div class="royal-card-mekanisme__content">
                <h4>Poin untuk benefit tambahan</h4>
                <p>Untuk benefit tambahan, Mama harus mengupload struk belanja</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END MEKANISME -->

    </div>
    <!-- END TAB TIER KONTEN-->

</div>
  <!-- END CONTAINER -->
  
  <!-- CTA SECTION -->
  <div class="container-fluid royal-cta-section">
    <div class="container text-center">
      <h3>Yuk Mama, daftar sekarang untuk mendapatkan Rewards!</h3>
      <p>Dengan menjadi member Royal Rewards,Mama akan mendapatkan banyak benefit
        dan informasi terupdate dari kami</p>
      <a href="#" target="_blank">Register</a>
    </div>
  </div>
  <!-- END CTA SECTION -->