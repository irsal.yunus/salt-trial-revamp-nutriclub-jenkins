<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>

<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-Point-History.css'); ?>

<?php $this->headScript()->offsetSetFile(10, '/assets/js/widgets/rr-modal-filter.js', 'text/javascript', []); ?>

<div class="container royal-poin-history">

    <div class="row">
      <div class="col-12 mt-5">
        <a href="/royal-rewards/dashboard" class="btn-kembali">
          <img src="/assets/rrr_images/icon-royal/icon-arrow-left.png" alt="" />
          Kembali
        </a>
      </div>
    </div>

    <?= 
    $this->areablock('royalrewards-pointHistoriesPage-areablock', [
        "allowed" => ["point-histories"]
    ]);
    ?>

</div>