<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>

<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-How-Get-Point.css'); ?>

<div class="container royal-cara-poin">

    <div class="row">
      <div class="col-12 mt-5">
        <a href="/royal-rewards/dashboard" class="btn-kembali">
          <img src="/assets/rrr_images/icon-royal/icon-arrow-left.png" alt="" />
          Kembali
        </a>
      </div>
    </div>

    <div class="row text-center royal-heading">
      <div class="col-12">
        <img src="/assets/rrr_images/icon-royal/icon-dapat-poin.png" alt="" />
        <h1>Cara Mendapatkan Poin</h1>
        <p>Setiap konten atau aktifitas yang Anda akses di Nutriclub akan<br/>memberikan poin untuk rewards Anda</p>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-md-8 col-sm-12">
        <div class="row cara-list">
          <div class="col-md-3 col-6">
            <div class="cara-list__item">
              <img src="/assets/rrr_images/icon-royal/icon-podcast.png" alt="">
              <p>Podcast</p>
              <h5>10 Poin</h5>
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="cara-list__item">
              <img src="/assets/rrr_images/icon-royal/icon-artikel.png" alt="">
              <p>Artikel</p>
              <h5>10 Poin</h5>
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="cara-list__item">
              <img src="/assets/rrr_images/icon-royal/icon-video.png" alt="">
              <p>Video</p>
              <h5>10 Poin</h5>
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="cara-list__item">
              <img src="/assets/rrr_images/icon-royal/icon-digital.png" alt="">
              <p>Digital Tools</p>
              <h5>10 Poin</h5>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-md-8 col-sm-12 pb-3">
        <div class="row">
          <div class="col-12">
            <div class="cara-alert">
              <h5>Catatan Penting!</h5>
              <p>Maksimum poin yang Anda bisa dapatkan <strong>dalam 1 bulan adalah 300 Poin</strong></p>
            </div>
          </div>
        </div>
      </div>
    </div>    

</div>