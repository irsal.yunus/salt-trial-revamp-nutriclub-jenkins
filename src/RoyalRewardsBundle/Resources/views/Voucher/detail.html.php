<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource detail.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi Laxono <budi.laksono@salt.co.id>
 * @since Aug 25, 2020
 * @time 1:53:49 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

use Carbon\Carbon;

$this->extend('layout.html.php');

$this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-Voucher-Detail.css');
$this->headScript()->offsetSetFile(10, '/assets/js/pages/rr-voucher-detail.js', 'text/javascript', []);

$voucher = $this->voucher;

$voucherName = $voucher['Name'];
$imgs = $voucher['ListUrlImages'];
$expDate = $voucher['ExpiryDate'];

$isUsed = $voucher['IsUsed'];
$buttonStage = $isUsed ? 'disabled' : null;
?>

<script>
    var isVoucherUsed = "<?= $isUsed ?>";
    var isDefaultVoucher = true;
</script>

<div class="pt-5"></div>

<section id="button-back">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="/royal-rewards/dashboard" class="btn-kembali">
                    <img src="/assets/rrr_images/icon-royal/icon-arrow-left.png" alt="Icon Back">
                    <?= $this->t('BACK') ?>
                </a>
            </div>
        </div>
    </div>
</section>

<section id="header-content" class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="header-list-detail">
                <h3><?= $this->t('VOUCHER') ?></h3>
                <h1><?= $voucherName ?></h1>
                <p>
                    <?=
                    $this->t('RRR_VOUCHER_EXP_DATE', [
                        '%date' => Carbon::parse($expDate)->locale('id')->isoFormat("dddd, D MMMM YYYY")
                    ])
                    ?>
                </p>
            </div>
        </div>
    </div>
</section>

<section id="body-content" class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="detail-content">
                <?php
                if ($imgs) {
                    foreach ($imgs as $img) {
                ?>
                        <img src="<?= $img ?>" alt="No ALT">
                <?php
                    }
                }
                ?>
                <?= html_entity_decode($voucher['Description']) ?>
            </div>
        </div>
        <div class="col-md-8">
            <button class="register-button" onclick="showVoucherCode('<?= $voucher['ID'] ?? 0 ?>', '<?= $voucher['CatalogueVoucherId'] ?? 0 ?>')">
                <span id="default">
                    <?= $this->t('REDEEM_VOUCHER') ?>
                </span>
                <!-- hidden element untuk kode vocuher -->
                <span id="code" class="voucher-code"><?= $voucher['CodeVoucher'] ?></span>
                <span class="copy-icon" onclick="copyCode()"><i class="fa fa-files-o" aria-hidden="true"></i></span>
            </button>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-copy" tabindex="-1" role="dialog" aria-labelledby="modal-copy" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-copy">
                    Info
                </h5>
            </div>
            <div class="modal-body">
                <p><?= $this->t('RRR_VOUCHER_COPY_CODE') ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= $this->t('OK') ?>
                </button>
            </div>
        </div>
    </div>
</div>
