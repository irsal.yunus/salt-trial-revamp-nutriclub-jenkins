<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi Laxono <budi.laksono@salt.co.id>
 * @since Aug 25, 2020
 * @time 1:53:49 PM
 *
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 *
 **/

$this->extend('layout.html.php');
?>

<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/vendor/slick.css'); ?>
<?php echo $this->headLink()->offsetSetStylesheet(7, '/assets/css/vendor/slick-theme.css'); ?>
<?php echo $this->headLink()->offsetSetStylesheet(8, '/assets/css/pages/royal-revamp/RR-Careline-List.css'); ?>

<?php $this->headScript()->offsetSetFile(10, '/assets/js/vendor/slick.min.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(11, '/assets/js/pages/rr-careline-list.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(12, '/assets/js/widgets/rr-slider-careline-date.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(13, '/assets/js/widgets/rr-modal-register.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(14, '/assets/js/pages/rr-careline-detail.js', 'text/javascript', []); ?>

<div class="pt-5"></div>

<section id="button-back">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= $this->path('rrr-dashboard') ?>" class="btn-kembali">
                    <img src="/assets/rrr_images/icon-royal/icon-arrow-left.png" alt="Icon Back">
                    <?= $this->t('BACK') ?>
                </a>
            </div>
        </div>
    </div>
</section>

<?php echo $this->areablock('royalrewards-carelineservice-areablock', [
    "allowed" => ["careline-info", "careline-schedule"]
]); ?>
