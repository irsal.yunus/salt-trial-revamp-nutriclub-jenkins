<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$profileInfo = $this->profileInfo;
$stages = $this->stages ?? [];
$provinceName = $this->provinceName;
$districtName = $this->districtName;
$subDistrictName = $this->subDistrictName;
$motherProductBeforeName = $this->motherProductBefore['Name'];

$productBeforeItems = $this->productBeforeItems ?? [];
$pregnancyProductBeforeItems = $this->pregnancyProductBeforeItems ?? [];
$childs = $this->childs;

$form = $this->formEditProfile;
?>

<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/vendor/jquery-ui/jquery-ui.min.css'); ?>
<?php echo $this->headLink()->offsetSetStylesheet(7, '/assets/css/pages/royal-revamp/RR-Edit-Profile.css'); ?>


<?php $this->headScript()->offsetSetFile(10, '/assets/vendor/jquery-ui/jquery-ui.min.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(11, '/assets/js/widgets/rr-upload-photo.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(12, '/assets/js/widgets/rr-validation-form-profile.js', 'text/javascript', []); ?>

<div class="pt-5"></div>

<section id="button-back">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="#" class="btn-kembali">
                    <img src="/assets/rrr_images/icon-royal/icon-arrow-left.png" alt="Icon Back">
                    Kembali
                </a>
            </div>
        </div>
    </div>
</section>

<section class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="header-detail">
                <img src="/assets/rrr_images/icon-royal/icon-edit-profile.png" alt="Icon Kulwap" />
                <h1>Edit Profile</h1>
                <p>Anda bisa mengubah data profile dan data anak disini</p>
            </div>
        </div>
    </div>
</section>

<div class="separator-rr"></div>

<section class="form-wrapper">
    <?= $this->form()->start($form, []) ?>
        <div class="container">
            <?php if ($this->updateProfile) { ?>
                <div class="alert alert-<?= $this->updateProfile['StatusCode'] === '00' ? 'success' : 'danger'?>" role="alert">
                    <?= $this->updateProfile['StatusMessage'] ?>
                </div>
            <?php } ?>
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="parent-profile">
                        <img class="profile-photo" src="<?= env('BASE_URL_MEDIA_DOT_NET') . $profileInfo['ProfilePicture'] ?? '#'?>" alt="">

                        <div class="upload-wrapper">
                            <div class="input">
                                <input id="ProfilePicture" type="hidden" name="ProfilePicture" value="">
                                <input id="file" type="file">
                                <span >Upload Foto Profile</span>
                            </div>
                        </div>

                        <div class="gender">
                            <button <?= $this->femaleToHtml ?> id="mama" type="button">Mama</button>
                            atau
                            <button <?= $this->maleToHtml ?> id="papa" type="button">Papa</button>

                            <input type="hidden" id="set-gender" name="Gender" value="<?= $profileInfo['Gender'] ?>">
                        </div>

                        <div class="input-group">
                            <div class="input-wrapper order-1 order-md-1">
                                <label for="front-name">Nama Depan <span>*</span></label>
                                <input type="text" id="front-name" name="FirstName" placeholder="Masukan Nama Depan Anda" value="<?= $profileInfo['FirstName']?>">
                            </div>

                            <div class="input-wrapper order-5 order-md-2">
                                <label for="old-password">Password Lama</label>
                                <input type="password" id="old-password" placeholder="Masukan Password Lama Anda">
                            </div>

                            <div class="input-wrapper order-2 order-md-3">
                                <label for="back-name">Nama Belakang <span>*</span></label>
                                <input type="text" id="back-name" name="LastName" placeholder="Masukan Nama Belakang Anda" value="<?= $profileInfo['LastName']?>">
                            </div>

                            <div class="input-wrapper order-6 order-md-4">
                                <label for="new-password">Password Baru</label>
                                <input type="password" id="new-password" name="Password" placeholder="Masukan Password Lama Anda">
                            </div>

                            <div class="input-wrapper order-3 order-md-5">
                                <label for="email">Email <span>*</span></label>
                                <input type="email" id="email" name="Email" placeholder="Masukan Alamat Email Anda" value="<?= $profileInfo['Email'] ?>">
                            </div>

                            <div class="input-wrapper order-7 order-md-6">
                                <label for="confirm-new-password">Konfirmasi Password Baru</label>
                                <input type="password" id="confirm-new-password" placeholder="Konfirmasi Kembali Password Anda">
                            </div>

                            <div class="input-wrapper order-4 order-md-7">
                                <label for="phone-number">No. Ponsel <span>*</span></label>
                                <input type="number" id="phone-number" name="Phone" placeholder="Masukan No. Ponsel Anda" value="<?= $profileInfo['Phone'] ?>">
                            </div>

                            <div class="input-wrapper order-8 order-md-8">
                                <label for="birth-date">Tanggil Lahir Anda <span>*</span></label>
                                <input class="birthdate-picker" type="text" id="birth-date" name="Birthdate" placeholder="Masukan Tanggal Lahir Anda" readonly value="<?= $profileInfo['Birthdate']?>">
                            </div>
                        </div>

                        <div class="separator-form"></div>

                        <div class="input-group">
                            <div class="input-wrapper select order-1 order-md-1">
                                <label for="province">Provinsi <span>*</span></label>
                                <input type="hidden" name="ProvinceID" value="<?= $profileInfo['ProvinceID'] ?>">
                                <input class="select-input" type="text" id="province" placeholder="Pilih Provinsi" readonly value="<?= $profileInfo['Province']['ProvinceName'] ?>">
                                <ul class="dropdown-select" id="select-province"></ul>
                            </div>

                            <div class="input-wrapper select order-3 order-md-2">
                                <label for="subdistrict">Kecamatan <span>*</span></label>
                                <input type="hidden" name="SubDistrictID" value="<?= $profileInfo['SubDistrictID'] ?>">
                                <input class="select-input" type="text" id="subdistrict" placeholder="Pilih Kecamatan" readonly value="<?= $profileInfo['SubDistrict']['SubDistrictName'] ?>">
                                <ul class="dropdown-select" id="select-subdistrict">
                                    <!-- <li>Pilih Kabupaten/Kota Sebelumnya</li> -->
                                </ul>
                            </div>

                            <div class="input-wrapper select order-2 order-md-3">
                                <label for="city">Kotamadya / Kabupaten <span>*</span></label>
                                <input type="hidden" name="DistrictID" value="<?= $profileInfo['DistrictID'] ?>">
                                <input class="select-input" type="text" id="city" placeholder="Pilih Kotamadya" readonly value="<?= $profileInfo['District']['DistrictName'] ?>">
                                <ul class="dropdown-select" id="select-city">
                                    <!-- <li>Pilih Provinsi Sebelumnya</li> -->
                                </ul>
                            </div>

                            <div class="input-wrapper order-4 order-md-4">
                                <label for="address">Alamat <span>*</span></label>
                                <textarea id="address" name="Address" rows="2" placeholder="Tuliskan Alamat Lengkap Anda"><?= $profileInfo['Address']?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="separator-rr"></div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="children-profile">
                        <div class="header-detail">
                            <h1>Lengkapi Data Dibawah Ini</h1>
                            <p>Dapatkan update terbaru Nutriclub berupa Artikel dan Tips menarik <br> seputar kehamilan, persalinan dan tumbuh kembang si kecil.</p>
                        </div>

                        <div class="input-group">
                            <div class="input-wrapper status select">
                                <label for="parent-status">Pilihan Status Anda <span>*</span></label>
                                <input type="hidden" name="StagesID" value="<?= $profileInfo['StagesID'] ?>">
                                <input class="select-input" type="text" id="parent-status" id="get-default-stageid" placeholder="Pilih Status Kehamilan" readonly value="<?= $profileInfo['StagesValue'] ?>">
                                <ul class="dropdown-select" id="select-status">
                                    <?php
                                    foreach ($stages as $stage) {
                                    ?>
                                        <li value="<?= $stage['ID'] ?>">
                                            <?= $stage['Name'] ?>
                                        </li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>

                        <div id="mother-info">
                            <div class="input-group">
                                <h6 class="child-title">Data Ibu</h6>

                                <div class="input-wrapper select">
                                    <label for="gestational-age">Usia Kehamilan <span>*</span></label>
                                    <input type="hidden" name="AgePregnant" value="<?= $profileInfo['AgePregnant'] ?>">
                                    <input class="select-input" type="text" id="gestational-age" placeholder="Pilih Usia Kehamilan" readonly value="<?= $profileInfo['AgePregnant'] ?> Minggu">
                                    <ul class="dropdown-select" id="gestational-age-select">
                                        <?php
                                            for ($x = 1; $x <= 43; $x++) {
                                                echo "<li value='$x'>$x Minggu</li>";
                                            }
                                        ?>
                                    </ul>
                                </div>

                                <div class="input-wrapper select">
                                    <label for="using-product">Produk yang Digunakan <span>*</span></label>
                                    <input type="hidden" name="ProductBeforeID" value="<?= $profileInfo['ProductBeforeID'] ?>">
                                    <input class="select-input" type="text" id="using-product" placeholder="Pilih Produk yang Digunakan" readonly value="<?= $motherProductBeforeName ?>">
                                    <ul class="dropdown-select" id="using-product-select">
                                    <?php
                                    foreach ($pregnancyProductBeforeItems as $pregnancyProductBeforeItem) {
                                        ?>
                                        <li value="<?= $pregnancyProductBeforeItem['ID'] ?>">
                                            <?= $pregnancyProductBeforeItem['Name'] ?>
                                        </li>
                                    <?php
                                    }
                                    ?>
                                    </ul>

                                </div>
                            </div>
                        </div>

                        <div class="separator-form" id="parent-separator"></div>

                        <div id="child-info">
                            <div class="input-group child-group">
                                <?php
                                $no = 1;
                                if ($childs) {
                                foreach ($childs as $key => $child) {
                                    $childProductName = $child['ChildProductBefore']['Name'];
                                    ?>
                                    <div class="child-wrapper">
                                        <h6 class="child-title">Data Anak <?= $no ?></h6>
                                        <input type="hidden" class="set-id-child" id="set-id-<?= $no ?>" name="Childs[<?= $key ?>][ID]" value="<?= $child['ID'] ?>">
                                        <input type="hidden" class="set-id-parent" id="set-parentId-<?= $no ?>" name="Childs[<?= $key ?>][ParentID]" value="<?= $child['ParentID'] ?>">
                                        <div class="input-wrapper child">
                                            <label class="child-name-label" for="child-name-1">Nama Anak <span>*</span></label>
                                            <input class="child-name-input" type="text" id="child-name-<?= $no ?>" name="Childs[<?= $key ?>][Name]" value="<?= $child['Name'] ?>" placeholder="Masukan Nama Anak Anda">
                                        </div>

                                        <div class="input-wrapper child">
                                            <label class="child-birth-label" for="child-birth-date-1">Tanggal Lahir Anak <span>*</span></label>
                                            <input class="child-birth-input" type="text" id="child-birth-date-<?= $no ?>" name="Childs[<?= $key ?>][Birthdate]" value="<?= $child['Birthdate'] ?>" placeholder="Masukan Tanggal Lahir Anak Anda" readonly>
                                        </div>

                                        <div class="input-wrapper child select">
                                            <label class="child-product-label" for="product-before-1">Product Sebelumnya <span>*</span></label>
                                            <input type="hidden" class="hidden-child-product" name="Childs[<?= $key ?>][ProductBeforeID]" value="<?= $child['ProductBeforeID'] ?>">
                                            <input class="select-input child-product-input" type="text" id="product-before-<?= $no ?>" value="<?= $childProductName ?>" placeholder="Pilih Produk" readonly>
                                            <ul class="dropdown-select">
                                                <?php
                                                foreach ($productBeforeItems as $productBeforeItem) {
                                                    ?>
                                                    <li value="<?= $productBeforeItem['ID'] ?>">
                                                        <?= $productBeforeItem['Name'] ?>
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>

                                    <?php
                                    $no++;
                                }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <button class="add-child" type="button">Tambah Anak</button>
                </div>
            </div>
        </div>

        <div class="separator-rr"></div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="civil-registration">
                        <div class="input-group">
                            <div class="input-wrapper">
                                <label for="id-card-number">No. KTP <span>*</span></label>
                                <input type="number" id="id-card-number" name="KTPNumber" placeholder="Masukan No. KTP Anda" value="<?= $profileInfo['KTPNumber'] ?>">
                            </div>

                            <div class="input-wrapper">
                                <label for="family-card-number">No. Kartu Keluarga <span>*</span></label>
                                <input type="number" id="family-card-number" name="KKNumber" placeholder="Masukan No. Kartu Keluarga Anda" value="<?= $profileInfo['KKNumber'] ?>">
                            </div>

                            <label class="check-wrapper">
                                <input type="checkbox" id="check-receivement" value="<?= $profileInfo['Subscriber'] ?>">
                                <span class="checkmark"></span>
                                Saya bersedia menerima SMS Journey informasi dan tips menarik seputar kehamilan atau tumbuh kembang Si Kecil
                            </label>

                            <label class="check-wrapper">
                                <input type="checkbox" id="check-agreement">
                                <span class="checkmark"></span>
                                Saya setuju dengan syarat dan ketentuan yang berlaku
                            </label>
                        </div>
                    </div>
                    <p class="foot-note">Harap cek kembali kelengkapan formulir di atas. Tidak boleh ada field yang kosong.</p>
                    <button type="submit" class="btn-update disabled" disabled>Update</button>
                </div>
            </div>
        </div>
    <?= $this->form()->widget($form['_csrf_token'], []) ?>
    <?= $this->form()->end($form, ['render_rest' => false]) ?>
</section>
