<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
?>

<!-- css -->
<?php 
    $this->headLink()->offsetSetStylesheet(4, '/assets/css/vendor/slick.css');
    $this->headLink()->offsetSetStylesheet(5, '/assets/css/vendor/slick-theme.css');
?>
<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-Homepage.css'); ?>

<!-- js -->
<?php $this->headScript()->offsetSetFile(9, '/assets/js/vendor/slick.min.js', 'text/javascript', []); ?>
<?php $this->headScript()->offsetSetFile(10, '/assets/js/pages/rr-homepage.js', 'text/javascript', []); ?>

<?php echo $this->areablock('royalrewards-homepage-areablock', [
    "allowed" => [
        "hero-banner",
        "encourage-login",
        "encourage-register",
        "resilient-board",
        "reward-benefit-catalogue",
    ]
]); ?>
<!-- section id="hero-banner" class=""></section-->

<!-- section id="encourage-login"></section -->

<!-- section id="resilient-board"></section -->

<!-- section id="reward-benefit-catalogue"></section -->

<!-- section id="encourage-register"></section -->