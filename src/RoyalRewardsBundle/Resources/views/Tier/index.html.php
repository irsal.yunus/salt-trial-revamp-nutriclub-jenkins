<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');

$data = $this->data;

$tiers = $data['tier'];
$user = $data['user'] ?? null;
?>

<?php echo $this->headLink()->offsetSetStylesheet(6, '/assets/css/pages/royal-revamp/RR-Info-Tier.css'); ?>

<div class="container info-tier">

    <div class="row">
      <div class="col-12 mt-5">
        <a href="<?= (!$data['user']) ? '/royal-rewards' : '/royal-rewards/dashboard' ?>" class="btn-kembali">
          <img src="/assets/rrr_images/icon-royal/icon-arrow-left.png" alt="" />
          <?= $this->t('BACK') ?>
        </a>
      </div>
    </div>

    <?= $this->areablock('tier-information', [
        "allowed" => ["tier-title", "tier-tab-benefits"],
    ]); ?>

    <!-- TAB BUTTON -->
    <ul class="nav nav-pills royal-tab justify-content-center" id="myTab" role="tablist">
        <?php
        if ($tiers) {
            /** @var \Pimcore\Model\DataObject\Tier $tier */
            foreach ($tiers as $index => $tier) {
                $name = $tier->getName();
                $key = strtolower($name);

                $activeTabMenuClass = ($user && $user->getTierId() === $tier->getTierId())
                    ? 'active' : null;
        ?>
                <li class="nav-item">
                    <a class="nav-link <?= (!$user && $index === 0) ? 'active' : $activeTabMenuClass ?>" id="<?= $key ?>-tab" data-toggle="tab" href="#<?= $key ?>-tab-content" role="tab" aria-controls="<?= $key ?>-tab-content" aria-selected="true">
                        <?= $name ?>
                    </a>
                </li>
        <?php
            }
        }
        ?>
    </ul>
    <!-- END TAB BUTTON -->

    <!-- TAB TIER KONTEN -->
    <div class="tab-content">

        <?php
        if ($tiers) {
            /** @var \Pimcore\Model\DataObject\Tier $tier */
            foreach ($tiers as $index => $tier) {
                $name = $tier->getName();
                $key = strtolower($name);

                $tierColourClass = $tier->getProperty('TIER_COLOUR');

                $activeTabClass = ($user && $user->getTierId() === $tier->getTierId())
                    ? 'show active' : null;
        ?>
                <div class="tab-pane fade <?= (!$user && $index === 0) ? 'show active' : $activeTabClass ?>" id="<?= $key ?>-tab-content" role="tabpanel" aria-labelledby="<?= $key ?>-tab">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-10">

                            <?php if (!$user) { ?>
                                <!-- BEFORE LOGIN HTML DIFFERENCE-->
                                <div class="royal-tier <?= $tierColourClass ?>">
                                    <div class="royal-tier__info">
                                        <div class="royal-tier__img">
                                            <?= $tier->getLogo() ?
                                                $tier->getLogo()->getThumbnail('tier-logo-thumbnail')->getHtml([
                                                    'disableWidthHeightAttributes' => true
                                                ]) : null ?>
                                        </div>
                                        <div class="royal-tier__content">
                                            <h2>
                                                <?= $this->t('RR_TIER_NAME', [
                                                    '%name' => $name
                                                ]) ?>
                                            </h2>
                                            <?= $tier->getDescription() ?>
                                            <div class="royal-tier__poin d-block d-sm-none">
                                                <p>
                                                    <?= $this->t('RR_FROM_TO_POINT', [
                                                        '%from' => $tier->getPointFrom(),
                                                        '%to' => $tier->getPointTo()
                                                    ]) ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="royal-tier__poin d-none d-sm-block">
                                        <p>
                                            <?= $this->t('RR_FROM_TO_POINT', [
                                                '%from' => $tier->getPointFrom(),
                                                '%to' => $tier->getPointTo()
                                            ]) ?>
                                        </p>
                                    </div>
                                </div>
                                <!-- END BEFORE LOGIN HTML DIFFERENCE-->
                            <?php } else { ?>
                                <!-- AFTER LOGIN HTML DIFFERENCE -->
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="royal-tier <?= $tierColourClass ?>">
                                            <div class="royal-tier__info">
                                                <div class="royal-tier__img">
                                                    <?= $tier->getLogo() ?
                                                        $tier->getLogo()->getThumbnail('tier-logo-thumbnail')->getHtml([
                                                            'disableWidthHeightAttributes' => true
                                                        ]) : null ?>
                                                </div>
                                                <div class="royal-tier__content">
                                                    <h2>
                                                        <?= $this->t('RR_TIER_NAME', [
                                                            '%name' => $name
                                                        ]) ?>
                                                    </h2>
                                                    <?= $tier->getDescription() ?>
                                                    <div class="royal-tier__poin d-sm-block">
                                                        <p>
                                                            <?= $this->t('RR_FROM_TO_POINT', [
                                                                '%from' => $tier->getPointFrom(),
                                                                '%to' => $tier->getPointTo()
                                                            ]) ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 pt-sm-1">
                                        <div class="cara-alert">
                                            <h5>
                                                <?= $this->t('RRR_TIER_IMPORTANT_NOTES') ?>
                                            </h5>
                                            <?= $tier->getImportantNotes() ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END AFTER LOGIN HTML DIFFERENCE-->
                            <?php } ?>

                            <?php
                            $mainBenefit = $tier->getMainBenefit();
                            if ($mainBenefit) {
                            ?>
                                <div class="royal-card-benefit">
                                    <h3 class="">
                                        <?= $this->t('RRR_MAIN_BENEFITS') ?>
                                    </h3>
                                    <?= $tier->getMainBenefitDescription() ?>
                                    <div class="royal-card-benefit__wrap row justify-content-center">
                                        <?php
                                        $mainBenefitItems = $mainBenefit->getItems();
                                        /** @var \Pimcore\Model\DataObject\Fieldcollection\Data\Benefits $mainBenefitItem */
                                        foreach ($mainBenefitItems as $mainBenefitItem) {
                                        ?>
                                            <div class="col-12 col-md-6">
                                                <div class="royal-card-benefit__item">
                                                    <?= $mainBenefitItem->getBenefits()->getGroup()->getLogo() ?
                                                        $mainBenefitItem->getBenefits()
                                                            ->getGroup()
                                                            ->getLogo()
                                                            ->getThumbnail('benefit-group-logo-thumbnail')
                                                            ->getHtml([
                                                                'disableWidthHeightAttributes' => true
                                                            ]) : null
                                                    ?>
                                                    <div class="royal-card-benefit__content">
                                                        <h4>
                                                            <?= $mainBenefitItem && $mainBenefitItem->getBenefits() ?
                                                                $mainBenefitItem->getBenefits()->getName() : null
                                                            ?>
                                                            <?php if ($mainBenefitItem->getBenefits() &&
                                                                $mainBenefitItem->getBenefits()->getPartnership()) { ?>
                                                                <span class="inline-voucher">
                                                                    <?= $mainBenefitItem->getBenefits()->getPartnership()->getLogo() ?
                                                                        $mainBenefitItem->getBenefits()
                                                                            ->getPartnership()
                                                                            ->getLogo()
                                                                            ->getThumbnail('partnership-logo-thumbnail')
                                                                            ->getHtml([
                                                                                'disableWidthHeightAttributes' => true
                                                                            ]) : null
                                                                    ?>
                                                                </span>
                                                            <?php } ?>
                                                        </h4>
                                                        <?= $mainBenefitItem->getBenefits()->getDescription() ?>
                                                        <?php
                                                        if ($mainBenefitItem->getQuantity()) {
                                                        ?>
                                                        <div class="absolute-icon">
                                                            <?= $mainBenefitItem->getQuantity() ?>x
                                                        </div>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>

                            <?php
                            $additionalBenefit = $tier->getAdditionalBenefit();
                            if ($additionalBenefit) {
                            ?>
                            <div class="royal-card-benefit">
                                <h3 class="">
                                    <?= $this->t('RRR_ADDITIONAL_BENEFITS') ?>
                                </h3>
                                <?= $tier->getAdditionalBenefitDescription() ?>
                                <div class="royal-card-benefit__wrap row justify-content-center">
                                    <?php
                                    $additionalBenefitItems = $additionalBenefit->getItems();

                                    /** @var \Pimcore\Model\DataObject\Fieldcollection\Data\Benefits $additionalBenefitItem */
                                    foreach ($additionalBenefitItems as $additionalBenefitItem) {
                                    ?>
                                    <div class="col-12 col-md-6">
                                        <div class="royal-card-benefit__item">
                                            <?= $additionalBenefitItem->getBenefits()->getGroup()->getLogo() ?
                                                $additionalBenefitItem->getBenefits()
                                                    ->getGroup()
                                                    ->getLogo()
                                                    ->getThumbnail('benefit-group-logo-thumbnail')
                                                    ->getHtml([
                                                        'disableWidthHeightAttributes' => true
                                                    ]) : null
                                            ?>
                                            <div class="royal-card-benefit__content">
                                                <h4>
                                                    <?= $additionalBenefitItem->getBenefits() && $additionalBenefitItem->getBenefits()->getName() ?
                                                        $additionalBenefitItem->getBenefits()->getName() : null
                                                    ?>
                                                    <?php if ($additionalBenefitItem->getBenefits() &&
                                                    $additionalBenefitItem->getBenefits()->getPartnership()) { ?>
                                                        <span class="inline-voucher">
                                                            <?= $additionalBenefitItem->getBenefits()->getPartnership()->getLogo() ?
                                                                $additionalBenefitItem->getBenefits()
                                                                ->getPartnership()
                                                                ->getLogo()
                                                                ->getThumbnail('partnership-logo-thumbnail')
                                                                ->getHtml([
                                                                    'disableWidthHeightAttributes' => true
                                                                ]) : null
                                                            ?>
                                                        </span>
                                                    <?php } ?>
                                                </h4>
                                                <?= $additionalBenefitItem->getBenefits()->getDescription() ?>
                                                <?php
                                                if ($additionalBenefitItem->getQuantity() > 1) {
                                                    ?>
                                                    <div class="absolute-icon">
                                                        <?= $additionalBenefitItem->getQuantity() ?>x
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
        <?php
            }
        }
        ?>
    </div>
    <!-- END TAB TIER KONTEN-->

      <!-- MEKANISME -->
      <?php if (!$user) { ?>
      <div class="royal-card-mekanisme">
        <h3 class="text-center">
            <?= $this->input('mechanism-title', [])?>
        </h3>
        <div class="text-center">
          <?= $this->wysiwyg('mechanism-description', [
              'class' => 'text-center'
          ]) ?>
        </div>

        <div class="royal-card-mekanisme__wrap row justify-content-center">
            <?php
            while ($this->block('mechanism-block')->loop()) {
            ?>
                <div class="col-12 col-md-3">
                    <div class="royal-card-mekanisme__item">
                        <?= $this->image('mechanism-block-image', [
                            'class' => 'royal-card-mekanisme__img'
                        ]) ?>
                        <div class="royal-card-mekanisme__content">
                            <h4>
                                <?= $this->input('mechanism-block-title') ?>
                            </h4>
                            <?= $this->wysiwyg('mechanism-block-description') ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
      </div>
      <?php } ?>
      <!-- END MEKANISME -->

  </div>
  <!-- END CONTAINER -->

  <!-- CTA SECTION -->
  <?php if (!$user) { ?>
  <div class="container-fluid royal-cta-section">
    <div class="container text-center">
      <h3>
          <?= $this->input('cta-title') ?>
      </h3>
        <?= $this->wysiwyg('cta-description') ?>
        <?= $this->link('cta-link', []) ?>
    </div>
  </div>
  <?php } ?>
  <!-- END CTA SECTION -->
