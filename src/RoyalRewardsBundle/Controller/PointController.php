<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource PointController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 20, 2020
 * @time 5:19:13 PM
 *
 **/

namespace RoyalRewardsBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class PointController extends AbstractController
{
    /**
     * @Route("/point")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(Request $request)
    {
    }
    /**
     * @Route("/point/histories")
     * @Security("has_role('ROLE_USER')")
     */
    public function historiesAction(Request $request)
    {
    }
}
