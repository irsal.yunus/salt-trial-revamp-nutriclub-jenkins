<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource TestController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author sandi <sandi.permana@salt.co.id>
 * @since 28/07/20
 * @time 11:17
 *
 **/

namespace RoyalRewardsBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use RoyalRewardsBundle\Services\DashboardService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard/", name="rrr-dashboard")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(Request $request, DashboardService $dashboardService)
    {
        $member_dashboard = $dashboardService->getDashboardService();
        if($member_dashboard) {
        	$this->view->member_dashboard = $member_dashboard['dashboard']['Value'];
        }
    }
}
