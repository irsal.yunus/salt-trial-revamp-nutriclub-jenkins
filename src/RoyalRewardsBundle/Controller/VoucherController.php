<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource VoucherController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi Laxono <budi.laksono@salt.co.id>
 * @since Aug 25, 2020
 * @time 2:27:32 PM
 *
 **/

namespace RoyalRewardsBundle\Controller;

use RoyalRewardsBundle\Api\v1\Voucher;
use RoyalRewardsBundle\Services\VoucherService;
use AppBundle\Services\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\{Route, Security};
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class VoucherController
 * @package RoyalRewardsBundle\Controller
 *
 * @Route("/voucher")
 *
 */
class VoucherController extends AbstractController
{
    /**
     * @param Request $request
     * @param VoucherService $voucherService
     * @param int $id
     *
     * @Route("/{id}", name="rrr-voucher-detail")
     *
     * @Security("has_role('ROLE_USER')")
     */
	public function detailAction(Request $request, VoucherService $voucherService, $id = 0)
    {
        $this->view->voucher = $voucherService->getServiceDetail($id);
	}

    /**
     * @Route("/redeem/{id}")
     *
     * @param integer $id
     * @param VoucherService $voucherService
     * @param UserService $userService
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return JsonResponse
     */
	public function redeem($id, VoucherService $voucherService, UserService $userService)
    {
        $result = $voucherService->postRedemptionCreate(
            $userService->getUser()->getId(),
            $id
        );

		return $this->json($result);
	}

    /**
     * @Route("/update/{id}")
     *
     * @param $id
     * @param VoucherService $voucherService
     * @param UserService $userService
     *
     * @return JsonResponse
     */
    public function update($id, VoucherService $voucherService, UserService $userService)
    {
        $data = $voucherService->updateStatusVoucher($id);

        return $this->json($data);
	}
}
