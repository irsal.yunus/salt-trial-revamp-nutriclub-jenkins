<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ProfileController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author sandi <sandi.permana@salt.co.id>
 * @since 01/09/20
 * @time 09:37
 *
 **/

namespace RoyalRewardsBundle\Controller;

use AppBundle\Model\DataObject\Customer;
use RoyalRewardsBundle\Form\EditProfileFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use RoyalRewardsBundle\Services\ProfileService;
use RoyalRewardsBundle\Api\v1\Profile;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile")
     */
    public function indexAction(Request $request)
    {
    }

    /**
     * @Route("/profile/edit")
     * @Security("has_role('ROLE_USER')")
     * @param Request $request
     * @param ProfileService $profileService
     */
    public function editAction(Request $request, ProfileService $profileService)
    {
        $formEditProfile = $this->createForm(EditProfileFormType::class, [], []);
        $this->view->formEditProfile = $formEditProfile->createView();

        $profileInfo = $profileService->getProfileService();
        if ($profileInfo) {
            $profileInfo = $profileInfo['profile']['Value'];
            $this->view->provinceName = $profileInfo['Province']['ProvinceName'];
            $this->view->districtName = $profileInfo['District']['DistrictName'];
            $this->view->subDistrictName = $profileInfo['SubDistrict']['SubDistrictName'];
            $this->view->profileInfo   = $profileInfo;
            $this->view->maleToHtml = $profileService->maleToHtml($profileInfo['Gender']);
            $this->view->femaleToHtml = $profileService->femaleToHtml($profileInfo['Gender']);
            $this->view->stages = $profileService->getAllStages();
            $this->view->productBeforeItems = $profileService->getProductBefore($profileInfo['ID'], false);
            $this->view->pregnancyProductBeforeItems = $profileService->getProductBefore($profileInfo['ID'], true);
            $this->view->motherProductBefore = $profileService->getProductBeforeById($profileInfo['ProductBeforeID']);
            $this->view->childs = $profileService->getChildProductBefore($profileInfo['Childs'] ?? []) ?? [];
        }

        if ($request->getMethod() === 'POST') {
            $updateProfile = $profileService->postMemberUpdate();

            $this->view->updateProfile = $updateProfile;
        }
    }

    /**
     * @param Request $request
     * @param ProfileService $profileService
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/profile/all-stages")
     */
    public function getAllStages(Request $request, ProfileService $profileService)
    {
        $allStages =  $profileService->getAllStages();
        return $this->json($allStages, 200);
    }
}
