<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi Laxono <budi.laksono@salt.co.id>
 * @since Aug 25, 2020
 * @time 1:48:49 PM
 *
 **/

namespace RoyalRewardsBundle\Controller;

use AppBundle\Services\UserService;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use RoyalRewardsBundle\Services\CarelineService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use RoyalRewardsBundle\Services\DashboardService;


class CarelineController extends AbstractController {

	private $carelineService;

	private $userService;

	public function __construct(CarelineService $carelineService, UserService $userService)
    {
		$this->carelineService = $carelineService;
		$this->userService = $userService;
	}

    /**
     * @Route("/careline/{id}")
     *
     * @param Request $request
     * @param integer $id
     *
     * @Security("has_role('ROLE_USER')")
     */
	public function indexAction(Request $request, $id = 0)
    {

	}

    /**
     * @Route("/careline/time/{date}")
     *
     * @param Request $request
     * @param string $date
     *
     * @return JsonResponse
     */
	public function getTimeAction(Request $request, string $date)
    {
		$result = [];
		$memberId = $this->userService->getUser() ? $this->userService->getUser()->getId() : null;
		[$y, $m, $d] = explode('-', $date);

        $serviceCareline = $this->carelineService->getServiceByDate($memberId, "$d/$m/$y");

		return $this->json($serviceCareline);
	}

    /**
     * @Route("/careline/redeem/{id}/{newId}")
     *
     * @param integer $id
     * @param integer $newId
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return JsonResponse
     */
	public function redeemAction(int $id, int $newId)
    {
		$result = [];
		$memberId = $this->userService->getUser() ? $this->userService->getUser()->getId() : null;

		if($id !== 0) {	// update
			$result = $this->carelineService->postRedemptionReshcedule($memberId, $id, $newId);
		} else {	// new
			$result = $this->carelineService->postRedemptionCreate($memberId, $newId);
		}

		return $this->json($result);
	}

}
