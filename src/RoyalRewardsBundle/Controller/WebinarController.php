<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource WebinarController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author sandi <sandi.permana@salt.co.id>
 * @since 31/08/20
 * @time 11:17
 *
 **/

namespace RoyalRewardsBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use RoyalRewardsBundle\Services\WebinarService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class WebinarController extends AbstractController
{
    /**
     * @Route("/webinar/")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(Request $request)
    {
    }

    /** 
     * @param WebinarService $webinarService
     * @Security("has_role('ROLE_USER')")
     */
    public function detailAction(WebinarService $webinarService)
    {
        $this->view->detail = $webinarService->getDetailWebinar();
        $this->view->redemption = $webinarService->postRedemptionCreate();
    }
}
