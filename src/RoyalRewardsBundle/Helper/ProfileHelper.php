<?php
/**
 *
 */

namespace RoyalRewardsBundle\Helper;

class ProfileHelper
{
    public function maleToHtml($profileGender)
    {
        if ($profileGender)
        {
            return $profileGender === 'M' ? 'class="active"': '';
        }
        return false;
    }
}
