<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Profile.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <budi.perkasa@salt.co.id>
 * @since Sept 09, 2020
 * @time 5:19:13 PM
 *
 **/

namespace RoyalRewardsBundle\Api\v1;

use Elasticsearch\Endpoints\Get;
use http\Params;
use RoyalRewardsBundle\Api\v1\BaseApi;

class Profile extends BaseApi
{
    public function __construct()
    {
        parent::__construct();
        $this->baseUrl = getenv('NEWNUTRILOYAL_API_BASE_URL');
    }

    public function getMemberInfo($accessToken)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ];

        $executor = $this->theExecutor('GET', $this->baseUrl . '/api/member/get', $params);

        return $executor;
    }

    public function getAllStages($accessToken)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ];

        $executor = $this->theExecutor('GET', $this->baseUrl . '/api/stages/getall', $params);

        return $executor;
    }

    public function getProductBefore($clientId, $accessToken, bool $isPregnancyProduct)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            'json' => [
                'ClientID' => $clientId,
                'ClientSecret' => $accessToken,
                'IsPregnancyProduct' => $isPregnancyProduct
            ]
        ];
        $executor = $this->theExecutor('POST', $this->baseUrl . 'api/productbefore/search', $params);

        return $executor;
    }
    public function getProductBeforeById($productId, $accessToken)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            'query' => [
                'id' => $productId
            ]
        ];
        $executor = $this->theExecutor('GET', $this->baseUrl . 'api/productbefore/get', $params);

        return $executor;
    }

    public function postMemberUpdate($data, $accessToken)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            'json' => $data
        ];
        $executor = $this->theExecutor('POST', $this->baseUrl . 'api/member/update', $params);

        return $executor;
    }

}
