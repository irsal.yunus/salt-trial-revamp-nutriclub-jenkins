<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Voucher.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Aug 31, 2020
 * @time 2:19:31 PM
 *
 **/

namespace RoyalRewardsBundle\Api\v1;

use RoyalRewardsBundle\Api\v1\BaseApi;

class Voucher extends BaseApi
{
	
	public function __construct()
	{
		parent::__construct();
		// override baseUrl from construct.
		$this->baseUrl = getenv('NEWNUTRILOYAL_API_BASE_URL', null);
	}
	
	public function getMemberServicesVoucher ($token, $memberID) {
		$params = [
			'headers' => [
				'Authorization' => 'Bearer ' . $token
			],
			'query' => [
				'Category' => 'voucher',
				'MemberID' => $memberID
			]
		];
		$executor = $this->theExecutor('GET', $this->baseUrl . '/api/member/getmemberservices', $params);
		
		return $executor;
	}
	
	public function getVoucherDetail ($token, $id) {
		$params = [
			'headers' => [
				'Authorization' => 'Bearer ' . $token
			],
			'query' => [
				'id' => $id
			]
		];
		$executor = $this->theExecutor('GET', $this->baseUrl . '/api/member/getservicedetail', $params);
		
		return $executor;
	}
	
	public function postRedemptionCreate (
		$token,
		$memberID,
		$catalogueId,
		int $quantity = 1
		) {
			$params = [
				'headers' => [
					'Authorization' => 'Bearer ' . $token
				],
				'json' => [
					'MemberID' => $memberID,
					'CatalogueId' => $catalogueId,
					'Quantity' => $quantity
				]
			];
			$executor = $this->theExecutor('POST', $this->baseUrl . '/api/Redemption/uploadstatusvoucher', $params);
			
			return $executor;
	}
	
}