<?php
/**
 * Created by PhpStorm.
 * User: Sandi <sandi.permana@salt.co.id>
 * Date: 22/04/2020
 * Time: 21:54
 */

namespace RoyalRewardsBundle\Api\v1;

use RoyalRewardsBundle\Api\v1\BaseApi;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Dashboard extends BaseApi
{

    public function __construct()
    {
        parent::__construct();

        // override baseUrl from construct.
        $this->baseUrl = getenv('NEWNUTRILOYAL_API_BASE_URL', null);
    }

    public function getProfile(int $memberId)
    {
        $params = [
           'query' => [
               'id' => $memberId
           ]
        ];
        $executor = $this->theExecutor('GET', $this->baseUrl . '/api/member/getdashboard', $params);

        return $executor;
    }
    
    public function getNotificationIsRead(bool $isRead = FALSE)
    {
    	$params = [
    		'query' => [
    			'IsRead' => $isRead
    		]
    	];
    	$executor = $this->theExecutor('GET', $this->baseUrl . '/api/Notification/GetNotificationIsRead', $params);
    	
    	return $executor;
    }
    
    public function getAllStages()
    {
    	$executor = $this->theExecutor('GET', $this->baseUrl . '/api/stages/getall', []);
    	
    	return $executor;
    }
    
    public function getStages(int $stagesID)
    {
    	$params = [
    		'query' => [
    			'id' => $stagesID
    		]
    	];
    	$executor = $this->theExecutor('GET', $this->baseUrl . '/api/stages/get', $params);
    	
    	return $executor;
    }
    
    public function getMemberServices(
        $token,
        string $category,
        string $memberID,
        int $page = 1,
        int $pagesize = 10)
    {
    	$params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ],
    		'query' => [
                'memberid' => $memberID,
                'category' => $category,
                'isredeem' => '1',
                'page' => $page,
                'pagesize' => $pagesize
    		]
    	];
    	$executor = $this->theExecutor('GET', $this->baseUrl . '/api/member/getmemberservices', $params);
    	
    	return $executor;
    }
    
    public function getServiceDetail(int $serviceId)
    {
    	$params = [
    		'query' => [
    			'id' => $serviceId
    		]
    	];
    	$executor = $this->theExecutor('GET', $this->baseUrl . '/api/member/getservicedetail', $params);
    	
    	return $executor;
    }
}
