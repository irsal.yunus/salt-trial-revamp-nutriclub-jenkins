<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Webinar.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 31, 2020
 * @time 5:19:13 PM
 *
 **/

namespace RoyalRewardsBundle\Api\v1;

use RoyalRewardsBundle\Api\v1\BaseApi;

class Webinar extends BaseApi
{

    public function __construct()
    {
        parent::__construct();
        // override baseUrl from construct.
        $this->baseUrl = getenv('NEWNUTRILOYAL_API_BASE_URL', null);
    }
    
    public function getMemberServices (
        $token,
        string $category,
        string $memberID,
        int $isRedeemed = 0,
        int $page = 1,
        int $size = 10
    ) {
    	$params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ],
    		'query' => [
                'memberid' => $memberID,
                'category' => $category,
                'isredeem' => $isRedeemed,
                'page' => $page,
                'pagesize' => $size
    		]
    	];
    	$executor = $this->theExecutor('GET', $this->baseUrl . '/api/member/getmemberservices', $params);
    	
    	return $executor;
    }

    public function getServiceDetail ($token, $serviceId) {
    	$params = [ 
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ],
    		'query' => [
                'id' => $serviceId
    		]
    	];
    	$executor = $this->theExecutor('GET', $this->baseUrl . '/api/member/getservicedetail', $params);
    	
    	return $executor;
    }

    public function postRedemptionCreate (
        $token,
        $memberID,
        $catalogueId,
        int $quantity = 1
    ) {
    	$params = [ 
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ],
    		'json' => [
                'MemberID' => $memberID,
                'CatalogueId' => $catalogueId,
                'Quantity' => $quantity
    		]
    	];
    	$executor = $this->theExecutor('POST', $this->baseUrl . '/api/Redemption/Create', $params);
    	
    	return $executor;
    }
}
