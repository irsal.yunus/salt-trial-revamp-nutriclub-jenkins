<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Catalog.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 10/09/20
 * @time 14.44
 *
 */

namespace RoyalRewardsBundle\Api\v1;

use RoyalRewardsBundle\Api\v1\BaseApi;

class Catalog extends BaseApi
{
    public function updateStatusVoucher($token, $catalogVoucherId)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ],
            'query' => [
                'CatalogueVoucherId' => $catalogVoucherId
            ]
        ];

        return $this->theExecutor('POST', $this->baseUrl . 'api/Catalogue/UpdateStatusVoucher', $params);
    }
}
