<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Point.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 20, 2020
 * @time 5:19:13 PM
 *
 **/

namespace RoyalRewardsBundle\Api\v1;

use RoyalRewardsBundle\Api\v1\BaseApi;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Point extends BaseApi
{

    public function __construct()
    {
        parent::__construct();

        // override baseUrl from construct.
        $this->baseUrl = getenv('NEWNUTRILOYAL_API_BASE_URL', null);
    }
    
    public function GetHistoryPoint(
        string $memberID,
        int $timeBy = 1,
        array $actionCodes = [],
        int $actionId = null,
        int $page = 1,
        int $size = 10
    ){
    	$params = [
            'json' => [
                'memberId' => $memberID,
                'timeBy' => $timeBy,
                'page' => $page,
                'size' => $size,
                'ActionId' => $actionId,
                'actionCodes' => $actionCodes,
            ]
        ];
    	$executor = $this->theExecutor('GET', $this->baseUrl . '/api/Member/GetHistoryPoint', $params);
    	
    	return $executor;
    }

    public function getActionById ($id) {
    	$params = [ 
    		'query' => [
                'id' => $id
    		]
    	];
    	$executor = $this->theExecutor('GET', $this->baseUrl . '/api/Action/Get/', $params);
    	
    	return $executor;
    }
}
