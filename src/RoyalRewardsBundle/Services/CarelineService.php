<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Aug 31, 2020
 * @time 2:16:04 PM
 *
 **/

namespace RoyalRewardsBundle\Services;

use Carbon\Carbon;
use RoyalRewardsBundle\Api\v1\Careline;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CarelineService
{
	/** @var SessionInterface $session */
	private $session;
	private $query;
	private $total;
	private $careline;
	private $editmode;
	private $carelineSlug;
	private $request;

	public function __construct(
		SessionInterface $session,
		RequestStack $requestStack
		) {
			$this->careline = new Careline();
			$this->session = $session;
			$this->query = Request::createFromGlobals()->query->all();
			$this->editmode = $requestStack->getCurrentRequest()->get('_editmode');
			$this->request = $requestStack->getCurrentRequest();
	}

	public function getService($memberID)
    {
        if (env('CARELINE_DUMMY_DATA_PROVIDED_BY_PHP')) {
            $service = $this->getCarelineDummyDataProvidedByPhp($memberID);
        }

        if (!env('CARELINE_DUMMY_DATA_PROVIDED_BY_PHP')) {
            $service = $this->careline->getMemberServicesCareline(
                $this->session->get('AccessToken'),
                $memberID
            );
        }

		if($service['StatusCode'] === '00') {
			return $service['Value'];
		}

        return false;
    }

	public function getServiceDetail($id) {

		$service = $this->careline->getCarelineDetail($this->session->get('AccessToken'), $id);
		if($service['StatusCode'] === '00') {
			return $service['Value'];
		}

        return false;
    }

	public function getServiceByDate($memberID, $date) {

		$service = $this->careline->getMemberServicesCarelineByDate(
		    $this->session->get('AccessToken'),
            $memberID,
            $date
        );

        return $service;
    }

	public function postRedemptionCreate (
		$memberID,
		$catalogueId,
		int $quantity = 1
		) {

		$service = $this->careline->postRedemptionCreate(
			$this->session->get('AccessToken'),
			$memberID,
			$catalogueId,
			$quantity
			);

		return $service;

	}

	public function postRedemptionReshcedule (
		$memberID,
		$oldCatalogueId,
		$newCatalogueId,
		int $quantity = 1
		) {

			$service = $this->careline->postRedemptionReshcedule(
			$this->session->get('AccessToken'),
			$memberID,
			$oldCatalogueId,
			$newCatalogueId,
			$quantity
			);

		return $service;

	}

	private function getCarelineDummyDataProvidedByPhp(int $memberId)
    {
        $val = [];
        for ($i = 1; $i <= 10; $i++) {
            $day = Carbon::now()->addDays($i);

            $isStock = $i%2 === 1;

            $val[] = [
                'MemberId' => $memberId,
                'Category' => 'Careline',
                'Date' => $day->toDateTimeLocalString(),
                'IsStock' => $isStock
            ];
        }

        return [
            'StatusCode' => '00',
            'Value' => $val
        ];
    }
}
