<?php
/**
 * Created by PhpStorm.
 * User: Sandi <sandi.permana@salt.co.id>
 * Date: 25/03/2020
 * Time: 16:56
 */

namespace RoyalRewardsBundle\Services;

use Pimcore\Model\DataObject\Tier;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Model\DataObject\Customer;

class TierService
{
    /** @var SessionInterface $session */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function getTier()
    {
        /** @var Customer $customer */
        $customer = $this->session->get('UserProfile');

        $tier = new Tier\Listing();
        $tier->setOrderKey('oo_id');
        $tier->setOrder('asc');
        $tier->load();

        return [
            'user' => $customer,
            'tier' => $tier->getObjects()
        ];
    }
}
