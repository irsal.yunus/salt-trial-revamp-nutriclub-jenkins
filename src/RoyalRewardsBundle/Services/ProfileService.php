<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Profile.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <budi.perkasa@salt.co.id>
 * @since Sept 09, 2020
 * @time 5:19:13 PM
 *
 **/

namespace RoyalRewardsBundle\Services;

use RoyalRewardsBundle\Api\v1\Profile;
use AppBundle\Api\v1\Rewards\Account;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Model\DataObject\Customer;
use Pimcore\Tool;
use Symfony\Component\HttpFoundation\RequestStack;

class ProfileService
{
    /** @var SessionInterface $session */
    private $session;

    private $profile;

    private $baseUrl;

    private $account;

    private $request;

    private $accessToken;

    public function __construct(SessionInterface $session, RequestStack $requestStack)
    {
        $this->session = $session;
        $this->profile = new Profile();
        $this->request = $requestStack->getCurrentRequest();
        $this->baseUrl = Tool::getHostUrl(getenv('HTTP_PROTOCOL'));
        $this->account = new Account();
        $this->accessToken = $this->session->get('AccessToken');
    }

    public function getProfileService()
    {
        $memberInfo = $this->profile->getMemberInfo($this->accessToken);

        if ($memberInfo['StatusCode'] === '00')
        {
            return [
                "profile" => $memberInfo
            ];
        }

        return false;
    }

    public static function setImageFullPath($baseUrl, $image)
    {
        return $image ? $baseUrl . '/media' .$image : '';
    }

    public static function setDateFormat($date)
    {
        return str_replace('/', '-', $date);
    }

    public function maleToHtml($profileGender)
    {
        if ($profileGender)
        {
            return $profileGender === 'M' ? 'class="active"': '';
        }
        return false;
    }

    public function femaleToHtml($profileGender)
    {
        if ($profileGender)
        {
            return $profileGender === 'F' ? 'class="active"': '';
        }
        return false;
    }

    public function getProfilePicture($profilePicture)
    {
        return $profilePicture ? $this->baseUrl . '/media' .$profilePicture : '/assets/rrr_images/content/card-photo/male.png';
    }

    public function getAllStages()
    {
        $stages = $this->profile->getAllStages($this->accessToken);

        if (!$stages) {
            return [];
        }

        $stageValues = $stages['Value'];

        return $stageValues;
    }

    public function getProductBefore($clientId, bool $isPregnancyProduct)
    {
        $productBeforeItems =  $this->profile->getProductBefore($clientId, $this->accessToken, $isPregnancyProduct);

        if (!$productBeforeItems)
        {
            return [];
        }

        return $productBeforeItems['Value'];
    }

    public function getProductBeforeById($productId)
    {
        $productBeforeById =  $this->profile->getProductBeforeById($productId, $this->accessToken);

        if (!$productBeforeById)
        {
            return [];
        }

        return $productBeforeById['Value'];
    }

    public function getChildProductBefore(array $dataChilds)
    {
        if (!$dataChilds)
        {
            return [];
        }

        foreach ($dataChilds as $key => $dataChild) {
            $dataChilds[$key]['ChildProductBefore'] = $this->getProductBeforeById($dataChild['ProductBeforeID']);
        }
        return $dataChilds;
    }

    public function postMemberUpdate()
    {
        $data = $this->request->request->all();
        $data['ProfilePicture'] = preg_replace('/data:image\/(png|jpg|jpeg);base64\,/m', '', $data['ProfilePicture']);
        return $this->profile->postMemberUpdate($data, $this->accessToken);
    }
}
