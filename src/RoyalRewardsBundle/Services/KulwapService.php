<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource KulwapService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 31, 2020
 * @time 5:19:13 PM
 *
 **/

namespace RoyalRewardsBundle\Services;

use RoyalRewardsBundle\Api\v1\Kulwap;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Model\DataObject\Customer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class KulwapService
{
    /** @var SessionInterface $session */
    private $session;
    private $query;
    private $total;
    private $kulwap;
    private $editmode;
    private $kulwapSlug;
    private $request;

    public function __construct(
        SessionInterface $session,
        RequestStack $requestStack
    ) {
        $this->kulwap = new Kulwap();
        $this->session = $session;
        $this->query = Request::createFromGlobals()->query->all();
        $this->editmode = $requestStack->getCurrentRequest()->get('_editmode');
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getScheduleKulwap ($isRedeemed) {
        $category = 'kulwap';
        $customerId = $this->getCustomerId();
        $page = isset($this->query['page']) ? $this->query['page'] : 1;
        $size = isset($this->query['size']) ? $this->query['size'] : 10;
        $token = $this->session->get('AccessToken');
        
        $result = array();
        $services = $this->kulwap->getMemberServices($token, $category, $customerId, $isRedeemed, $page, $size);
        $result = ($services['StatusCode'] == '00' && count($services['Value']) > 0) ? $services['Value'] : [];
        
        return $result;
    }

    public function pagination() {
        $total = $this->total ? $this->total : 1;
        $size = isset($this->query['size']) ? $this->query['size'] : 10;
        $page = isset($this->query['page']) ? $this->query['page'] : 1;
        $pageLast = ceil($total/$size);
        $pagination = [];
        $number = [];
        for ($x = 1; $x <= $pageLast; $x+=1) {
            if ($x == $page-1 || $x == $page || $x == $page+1) {
                array_push($number, [
                    'text' => $x,
                    'value' => $x,
                    'class' => ($x == $page) ? 'active' : ''
                ]);
            }
        }

        // Fist
        array_push($pagination, [
            'text' => 'First',
            'value' => 1,
            'class' => ''
        ]);

        // <
        if (current($number)['text'] > 1) {
            array_push($pagination, [
                'text' => '&lsaquo;',
                'value' => 0,
                'class' => ''
            ]);
        }

        // Number
        if (count($number) > 0) {
            array_push($pagination, ...$number);
        } else {
            array_push($pagination, [
                'text' => '1',
                'value' => 1,
                'class' => ''
            ]);
        }

        // >
        if (end($number)['text'] < $pageLast) {
            array_push($pagination, [
                'text' => '&rsaquo;',
                'value' => 0,
                'class' => ''
            ]);
        }

        // Last
        array_push($pagination, [
            'text' => 'Last',
            'value' => (int)number_format($pageLast, 0, '.', ','),
            'class' => ''
        ]);

        return $pagination;
    }

    public function getCustomerId() {
        $customerEditMode = '86415';
        $customerId = $this->session->get('UserProfile')->getId();
        $result = $this->editmode ? $customerEditMode : $customerId;

        return $result;
    }

    public function getDetailKulwap () {
        $kulwapSlug = $this->getKulwapSlug();
        $token = $this->session->get('AccessToken');
        $detail = $this->kulwap->getServiceDetail($token, $kulwapSlug);
        if ($detail['StatusCode'] == '00') {
            $result = $detail['Value'];
        } else {
            throw new NotFoundHttpException('Kulwap tidak ditemukan');
        }
        return $result;
    }

    public function getKulwapSlug() {
        $kulwapSlugEditMode = '4171';
        $kulwapSlug = $this->request->get('kulwapSlug');
        $result = $this->editmode ? $kulwapSlugEditMode : $kulwapSlug;

        return $result;
    }

    public function postRedemptionCreate() {
        if ($this->query['method'] == "POST") {
            $customerId = $this->getCustomerId();
            $kulwapSlug = $this->request->get('kulwapSlug');
            $token = $this->session->get('AccessToken');
            $redemption = $this->kulwap->postRedemptionCreate($token, $customerId, $kulwapSlug);
            $success = ($redemption['StatusCode'] == '00') ? true : false;
            $result = [
                'modal' => true,
                'success' => $success,
                'message' => $redemption['StatusMessage']
            ];
        } else {
            $result = [
                'modal' => false,
                'success' => null,
                'message' => null
            ];
        }
        return $result;
    }
}
