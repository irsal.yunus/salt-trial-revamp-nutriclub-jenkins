<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource PointService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Sandi <sandi.permana@salt.co.id>
 * @since Aug 20, 2020
 * @time 5:19:13 PM
 *
 **/

namespace RoyalRewardsBundle\Services;

use RoyalRewardsBundle\Api\v1\Point;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Model\DataObject\Customer;
use Symfony\Component\HttpFoundation\Request;

class PointService
{
    /** @var SessionInterface $session */
    private $session;
    private $point;
    private $customer;
    private $query;
    private $total;

    public function __construct(
        SessionInterface $session
    ) {
        $this->session = $session;
        $this->point = new Point();
        $request = Request::createFromGlobals();
        $this->query = $request->query->all();
    }

    public function getPointHistories() {
        /** @var Customer $customer */
        $customer = $this->session->get('UserProfile');
        $customerId = $customer->getId();
        $timeBy = isset($this->query['timeBy']) ? $this->query['timeBy'] : 1;
        $actionId = null;
        $page = isset($this->query['page']) ? $this->query['page'] : 1;
        $size = isset($this->query['size']) ? $this->query['size'] : 10;
        $type = isset($this->query['type']) ? $this->query['type'] : null;
        $actionCodes = $this->getActionCodeByKey($type);
        $pointHistories = $this->point->GetHistoryPoint($customerId, $timeBy, $actionCodes, $actionId, $page, $size);

        if (isset($pointHistories['StatusMessage'])) {
            $this->countPointHistories($pointHistories['StatusMessage']);
        }

        $result = ($pointHistories['StatusCode'] == '200' && isset($pointHistories['Value'])) ? $pointHistories['Value'] : [];
        return $result;
    }

    public function arrayPagination() {
        $total = $this->total ? $this->total : 0;
        $size = isset($this->query['size']) ? $this->query['size'] : 10;
        $page = isset($this->query['page']) ? $this->query['page'] : 1;
        $pageLast = ceil($total/$size);
        $pagination = [];
        $number = [];
        for ($x = 1; $x <= $pageLast; $x+=1) {
            if ($x == $page-1 || $x == $page || $x == $page+1) {
                array_push($number, [
                    'text' => $x,
                    'value' => $x,
                    'class' => ($x == $page) ? 'active' : ''
                ]);
            }
        }

        // Fist
        array_push($pagination, [
            'text' => 'First',
            'value' => 1,
            'class' => ''
        ]);

        // <
        if (current($number)['text'] > 1) {
            array_push($pagination, [
                'text' => '&lsaquo;',
                'value' => 0,
                'class' => ''
            ]);
        }

        // Number
        if (count($number) > 0) {
            array_push($pagination, ...$number);
        } else {
            array_push($pagination, [
                'text' => '1',
                'value' => 1,
                'class' => ''
            ]);
        }

        // >
        if (end($number)['text'] < $pageLast) {
            array_push($pagination, [
                'text' => '&rsaquo;',
                'value' => 0,
                'class' => ''
            ]);
        }

        // Last
        array_push($pagination, [
            'text' => 'Last',
            'value' => (int)number_format($pageLast, 0, '.', ','),
            'class' => ''
        ]);

        return $pagination;
    }

    public function countPointHistories($statusMessage) {
        $total = str_replace("count : ", "", $statusMessage);
        $this->total = $total;
    }

    public function getActionCodes() {
        // ReadArticle, Video, Podcast, Ebook, DigitalTools
        $actionCodes = [
            [
                'Text' => 'Article',
                'Codes' => [
                    'ReadArticle'
                ]
            ],
            [
                'Text' => 'Video',
                'Codes' => [
                    'PlayVideo'
                ]
            ],
            [
                'Text' => 'Podcast',
                'Codes' => [
                    'PlayPodcast'
                ]
            ],
            [
                'Text' => 'Ebook',
                'Codes' => [
                    'DownloadEbook'
                ]
            ],
            [
                'Text' => 'Digital Tools',
                'Codes' => [
                    'FinishOSA1', 
                    'FinishOSA2', 
                    'FinishMitosKehamilan', 
                    'FinishARS', 
                    'FinishASC', 
                    'FinishPRS', 
                    'FinishMPT', 
                    'FinishBCC'
                ]
            ],
        ];
        return $actionCodes;
    }

    public function getActionCodeByKey($key = null) {
        $array = $this->getActionCodes();
        return isset($key) ? $array[$key]['Codes'] : [];
    }
}
