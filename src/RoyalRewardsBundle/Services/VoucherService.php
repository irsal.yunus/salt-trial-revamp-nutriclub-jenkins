<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.co.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource VoucherService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author Budi.Laxono <budi.laksono@salt.co.id>
 * @since Aug 31, 2020
 * @time 2:16:04 PM
 *
 **/

namespace RoyalRewardsBundle\Services;

use RoyalRewardsBundle\Api\v1\Careline;
use RoyalRewardsBundle\Api\v1\Catalog;
use RoyalRewardsBundle\Api\v1\Voucher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VoucherService
{
	/** @var SessionInterface $session */
	private $session;

	private $query;

	private $voucher;

	private $editmode;

	private $request;

	private $careline;

	private $catalog;

	public function __construct(SessionInterface $session, RequestStack $requestStack)
    {
        $this->voucher = new Voucher();
        $this->careline = new Careline();
        $this->catalog = new Catalog();

        $this->session = $session;
        $this->query = Request::createFromGlobals()->query->all();
        $this->editmode = $requestStack->getCurrentRequest()->get('_editmode');
        $this->request = $requestStack->getCurrentRequest();
	}

	public function getService($memberID)
    {
		$service = $this->voucher->getMemberServicesVoucher(
		    $this->session->get('AccessToken'),
            $memberID
        );

		if($service['StatusCode'] === '00') {
			return $service['Value'];
		}
	}

	public function getServiceDetail($id)
    {
        $service = $this->voucher->getVoucherDetail($this->session->get('AccessToken'), $id);
		if($service['StatusCode'] !== '00') {
			throw new NotFoundHttpException('Voucher Not Found');
		}

		$data = $service['Value'];

		if (!$data) {
            throw new NotFoundHttpException('Voucher Not Found');
        }

		return $data;
	}

	public function postRedemptionCreate($memberID, $catalogueId, int $quantity = 1)
    {
		return $this->careline->postRedemptionCreate(
		    $this->session->get('AccessToken'),
            $memberID,
			$catalogueId,
			$quantity
        );
	}

    public function updateStatusVoucher($id)
    {
        $updateStatus = $this->catalog->updateStatusVoucher($this->session->get('AccessToken'), $id);

        return $updateStatus;
	}
}
