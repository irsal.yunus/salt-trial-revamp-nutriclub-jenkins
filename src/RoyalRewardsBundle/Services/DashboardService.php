<?php
/**
 * Created by PhpStorm.
 * User: Sandi <sandi.permana@salt.co.id>
 * Date: 25/03/2020
 * Time: 16:56
 */

namespace RoyalRewardsBundle\Services;

use RoyalRewardsBundle\Api\v1\Dashboard;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Model\DataObject\Customer;

class DashboardService
{
    /** @var SessionInterface $session */
    private $session;

    private $dashboard;

    private $customerTier;

    public function __construct(
        SessionInterface $session
    ) {
        $this->session = $session;
        $this->dashboard = new Dashboard();
    }

    public function getDashboardService()
    {
        /** @var Customer $customer */
        $customer = $this->session->get('UserProfile');
        $customerId = $customer->getId();
        $customer_dashboard = $this->dashboard->getProfile($customerId);

        if ($customer_dashboard['StatusCode'] === '00') {
        	$customer_stage = $this->dashboard->getStages($customer_dashboard['Value']['StagesID']);
        	if($customer_stage['StatusCode'] === '00') {
        		$customer_dashboard['Value']['StagesValue'] = $customer_stage['Value'];
        	}
            // Set Tier to private
            if (isset($customer_dashboard['Value']['Tier'])) {
                $this->customerTier = $customer_dashboard['Value']['Tier'];
                $this->customerTier['MyPoint'] = $customer_dashboard['Value']['Point'];
                $customer_dashboard['Value']['Tier']['Name'] = $this->checkCustomerTierName();
            }

            $customer_dashboard['Value']['UnreadNotif'] = 0;
            $unread_notif = $this->dashboard->getNotificationIsRead();
            if($unread_notif['StatusCode'] == '00') {
            	$customer_dashboard['Value']['UnreadNotif'] = count($unread_notif['Value']);
            }

	        return [
	            "dashboard" => $customer_dashboard
	        ];
        }

        return false;
    }

    public function getCustomerRewards()
    {
        /** @var Customer $customer */
        $customer = $this->session->get('UserProfile');
        $customerId = $customer->getId();
        $tier = $this->checkCustomerTierName();
        $categories = $this->getArrayRewards();
        $result = [];

        foreach ($categories[$tier] as $category) {
            $token = $this->session->get('AccessToken');
            $services = $this->dashboard->getMemberServices($token, $category, $customerId);
            $servicesValue = ($services['StatusCode'] === '00' && count($services['Value']) > 0) ? $services['Value'] : [];
            $totalChance = ($services['StatusCode'] === '00') ? $services['Total'] : 0;
            $totalRow = ($services['StatusCode'] === '00') ? $services['TotalRow'] : 0;
            $result[] = [
                'name' => $category,
                'value' => $servicesValue,
                'total' => [
                    'chance' => $totalChance,
                    'row' => $totalRow
                ]
            ];
        }

        return $result;
    }

    public function checkCustomerTierName()
    {
        $customerTierName = null;

        if (isset($this->customerTier['Name'])) {
            $customerTierName = strtolower($this->customerTier['Name']);
        } else {
            /** @var Customer $customer */
            $customer = $this->session->get('UserProfile');
            $customerId = $customer->getId();
            $customerDashboard = $this->dashboard->getProfile($customerId);

            if ($customerDashboard['StatusCode'] === '00' && isset($customerDashboard['Value']['Tier'])) {
                // Set Tier to private
                $this->customerTier = $customerDashboard['Value']['Tier'];
                $this->customerTier['MyPoint'] = $customerDashboard['Value']['Point'];
                $customerTierName = $this->customerTier['Name'];
            }
        }
        $rewards = $this->getArrayRewards();
        return array_key_exists($customerTierName, $rewards) ? $customerTierName : 'blue';
    }

    public function getArrayRewards()
    {
        return [
            'blue' => ['kulwap', 'voucher'],
            'silver' => ['kulwap', 'careline', 'voucher'],
            'gold' => ['kulwap', 'webinar', 'careline', 'voucher'],
            'royal' => ['kulwap', 'webinar', 'careline', 'voucher']
        ];
    }

    public function getRewardsDetail($rewardId)
    {
    	$reward = $this->dashboard->getServiceDetail($rewardId);
    	if($reward['StatusCode'] === '00') {
    		return $reward['Value'];
    	}

        return false;

    }
}
