<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource HtmlToImage.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 17/09/20
 * @time 23.53
 *
 */

namespace ResilientParentalCalculatorBundle\Image;

use Carbon\Carbon;
use Pimcore\Image\HtmlToImage as PimcoreHtmlToImage;
use Pimcore\Tool\Console;
use Pimcore\Tool\Session;

class HtmlToImage extends PimcoreHtmlToImage
{
    public static function convert($url, $outputFile, $defaultOptions = [], $screenWidth = 1200, $format = 'png')
    {

        // add parameter pimcore_preview to prevent inclusion of google analytics code, cache, etc.
        $url .= (strpos($url, '?') ? '&' : '?') . 'pimcore_preview=true';

        $options = $defaultOptions ?: [
            '--width ' . $screenWidth,
            '--format ' . $format,
        ];

        if (php_sapi_name() !== 'cli') {
            $options[] = '--cookie ' .  Session::getSessionName() . ' ' . Session::getSessionId();
        }

        $arguments = ' ' . implode(' ', $options) . ' "' . $url . '" ' . $outputFile;

        // use xvfb if possible
        if ($xvfb = Console::getExecutable('xvfb-run')) {
            $command = $xvfb . ' --auto-servernum --server-args="-screen 0, 1280x1024x24" ' .
                self::getWkhtmltoimageBinary() . ' --use-xserver' . $arguments;
        } else {
            $command = self::getWkhtmltoimageBinary() . $arguments;
        }

        $regex = '/.*\/resilient-parental-calculator\/disclaimer\/share\/(\w+\-\w+\-\w+\-\w+\w+\-\w+)/m';
        $regexMatch = preg_match_all($regex, $url, $matches, PREG_SET_ORDER, 0);
        $transactionId = $regexMatch[0] && $regexMatch[0][1] ? $regexMatch[0][1] : Carbon::now()->timestamp;

        Console::exec($command, PIMCORE_LOG_DIRECTORY . '/wkhtmltoimage_'. $transactionId .'.log', null);

        return file_exists($outputFile) && filesize($outputFile) > 1000;
    }
}
