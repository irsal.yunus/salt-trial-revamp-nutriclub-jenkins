<?php

namespace ResilientParentalCalculatorBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class ResilientParentalCalculatorBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/resilientparentalcalculator/js/pimcore/startup.js'
        ];
    }
}