<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource GenerateImage.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 17/09/20
 * @time 12.05
 *
 */

namespace ResilientParentalCalculatorBundle\Command;

use Carbon\Carbon;
use Pimcore\Console\AbstractCommand;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\RpcCalculate;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;

class GenerateImageCommand extends AbstractCommand
{
    protected $router;

    public function __construct(string $name = null, RouterInterface $router)
    {
        parent::__construct($name);

        $this->router = $router;
    }

    protected function configure()
    {
        $this
            ->setName('rpc:generate:image')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('trxid', 'trxid', InputOption::VALUE_OPTIONAL)
                ])
            )
            ->setDescription('RPC Generate image');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $trxId = $input->getOption('trxid');
        if ($trxId) {
            $this->getByTransactionId($trxId);

            return;
        }

        $rpcCalculateList = new RpcCalculate\Listing();
        $rpcCalculateList->setCondition('isImageGenerate IS NULL OR isImageGenerate = 0');
        $rpcCalculateList->setOrderKey('oo_id');
        $rpcCalculateList->setOrder('desc');

        $objects = $rpcCalculateList->getObjects();
        $count = $rpcCalculateList->getCount();

        if ($count < 1) {
            return null;
        }

        /** @var RpcCalculate $object */
        foreach ($objects as $object) {
            $this->generateImage($object);
            sleep(10);
        }
    }

    private function generateImage($rpcCalculatorObj)
    {
        if (!$rpcCalculatorObj instanceof RpcCalculate) {
            return null;
        }

        $isImageGenerate = $rpcCalculatorObj->getIsImageGenerate();
        $objTransactionId = $rpcCalculatorObj->getTransactionId();

        $baseWebRoot = PIMCORE_WEB_ROOT;
        $fileFormat = 'png';
        $filePath =  '/assets/resilient-parental-calculator/' . $objTransactionId . '.' . $fileFormat;
        $optFile = $baseWebRoot . $filePath;

        $screenWidth = (int)env('RPC_SHARE_IMAGE_WIDTH', 1024);
        $screenHeight = (int)env('RPC_SHARE_IMAGE_HEIGHT', 768);
        $quality = (int)env('RPC_SHARE_IMAGE_QUALITY', 768);

        if ($isImageGenerate && file_exists($optFile)) {
            return $filePath;
        }

        $sharePage = env('RPC_BASE_URL_ASSET', 'https://localhost');
        $sharePage .= $this->router->generate('disclaimer-share-generator', [
            'id' => $objTransactionId
        ]);

        $logDateTime = Carbon::now()->format('d_m_Y');

        $enableWkhtmltoimageSsl = env('RPC_ENABLE_WKHTMLTOIMAGE_SSL', false);

        try {
            $options = [
                '--width ' . $screenWidth,
                '--height ' . $screenHeight,
                '--format ' . $fileFormat,
                '--quality ' . $quality
            ];

            if ($enableWkhtmltoimageSsl) {
                $sslCertPath = env('RPC_SSL_CERT_PATH');
                $sslKeyPath = env('RPC_SSL_KEY_PATH');

                $sslOptions = [
                    '--ssl-crt-path ' . $sslCertPath,
                    '--ssl-key-path ' . $sslKeyPath
                ];

                $options = array_merge_recursive($options, $sslOptions);
            }

            $isConverted = \ResilientParentalCalculatorBundle\Image\HtmlToImage::convert(
                $sharePage,
                $optFile,
                $options
            );

            if ($isConverted) {
                $rpcCalculatorObj->setIsImageGenerate(true);
                $rpcCalculatorObj->save([
                    'versionNote' => 'RPC_GENERATE_IMAGE_SUCCESS'
                ]);
            }

            Simple::log('RPC_GENERATE_IMAGE_FAILED_' . $logDateTime, 'Failed to generate image');

        } catch (\Exception $exception) {
            $filePath = null;
            Simple::log('RPC_GENERATE_IMAGE_ERROR_' . $logDateTime, $exception->getMessage());
        }

        return $filePath;
    }

    private function getByTransactionId($trxId)
    {
        $rpcCalculate = RpcCalculate::getByTransactionId($trxId, 1);
        $this->generateImage($rpcCalculate);
    }
}
