<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RpcSchoolGrade.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 17.01
 *
 */

namespace ResilientParentalCalculatorBundle\Model\DataObject;

use Pimcore\Model\DataObject\RpcSchoolGrade as BaseRpcSchoolGrade;

class RpcSchoolGrade extends BaseRpcSchoolGrade
{
    public function getGrade()
    {
        $data['id'] = $this->getId();
        $data['name'] = $this->getName();

        return $data;
    }
}
