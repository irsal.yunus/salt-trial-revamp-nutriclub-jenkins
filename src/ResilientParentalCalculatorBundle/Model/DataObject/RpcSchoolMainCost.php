<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RpcSchoolMainCost.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 21/08/20
 * @time 14.03
 *
 */

namespace ResilientParentalCalculatorBundle\Model\DataObject;

use Pimcore\Model\DataObject\RpcSchoolMainCost as BaseRpcSchoolMainCost;
use Pimcore\Tool;

class RpcSchoolMainCost extends BaseRpcSchoolMainCost
{
    public function getMainCost()
    {
        $data['id'] = $this->getId();
        $data['image_icon'] = $this->getImageIcon() ? Tool::getHostUrl(env('HTTP_PROTOCOL')) .
            $this->getImageIcon()->getFullPath() : null;
        $data['name'] = $this->getName();
        $data['admission_fee'] = $this->getAdmissionFee();
        $data['monthly_fee'] = $this->getMonthlyFee();
        $data['yearly_fee'] = $this->getYearlyFee();

        return $data;
    }
}
