<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RpcSchoolLevel.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 21/08/20
 * @time 14.44
 *
 */

namespace ResilientParentalCalculatorBundle\Model\DataObject;

use Pimcore\Model\DataObject\RpcSchoolLevel as BaseRpcSchoolLevel;
use Pimcore\Tool;

class RpcSchoolLevel extends BaseRpcSchoolLevel
{
    public function getLevel()
    {
        $data['id'] = $this->getId();
        $data['code'] = $this->getCode();
        $data['name'] = $this->getName();
        $data['icon'] = Tool::getHostUrl(env('HTTP_PROTOCOL')) . $this->getIcon()->getFullPath();

        return $data;
    }
}
