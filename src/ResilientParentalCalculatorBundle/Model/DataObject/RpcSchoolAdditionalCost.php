<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RpcSchoolMainCost.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 21/08/20
 * @time 14.03
 *
 */

namespace ResilientParentalCalculatorBundle\Model\DataObject;

use Pimcore\Model\DataObject\RpcSchoolAdditionalCost as BaseRpcSchoolAdditionalCost;
use Pimcore\Tool;

class RpcSchoolAdditionalCost extends BaseRpcSchoolAdditionalCost
{
    public function getAdditionalCost()
    {
        $data['id'] = $this->getId();
        $data['name'] = $this->getName();
        $data['image_icon'] = $this->getImageIcon() ? Tool::getHostUrl(env('HTTP_PROTOCOL')) .
            $this->getImageIcon()->getFullPath() : null;
        $data['book'] = $this->getBook();
        $data['uniform'] = $this->getUniform();
        $data['activities'] = $this->getActivities();
        $data['monthly_catering'] = $this->getMonthlyCatering();
        $data['monthly_shuttle'] = $this->getMonthlyShuttle();
        $data['others'] = $this->getOthers();

        return $data;
    }
}
