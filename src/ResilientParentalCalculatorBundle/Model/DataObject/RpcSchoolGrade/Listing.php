<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Listing.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 17.01
 *
 */


namespace ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolGrade;

use Pimcore\Model\DataObject\RpcSchoolGrade\Listing as BaseRpcSchoolGradeListing;
use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolGrade;

class Listing extends BaseRpcSchoolGradeListing
{
    public function getGrades($orderKey = 'oo_id', $order = 'asc')
    {
        $this->setOrderKey($orderKey);
        $this->setOrder($order);

        $objs = $this->getObjects();

        if (!$objs) {
            return null;
        }
        $data = [];

        /** @var RpcSchoolGrade $obj */
        foreach ($objs as $obj) {
            $data[] = $obj->getGrade();
        }

        return $data;
    }
}
