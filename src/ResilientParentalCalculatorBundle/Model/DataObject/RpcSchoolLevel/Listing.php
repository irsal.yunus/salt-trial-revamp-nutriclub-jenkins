<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Listing.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 21/08/20
 * @time 14.45
 *
 */

namespace ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolLevel;

use Pimcore\Model\DataObject\RpcSchoolLevel\Listing as BaseRpcSchoolLevelListing;
use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolLevel;

class Listing extends BaseRpcSchoolLevelListing
{
    public function getLevels()
    {
        $this->setOrderKey('oo_id');
        $this->setOrder('asc');

        $levels = $this->getObjects();

        if (!$levels) {
            return [];
        }

        $data = [];
        /** @var RpcSchoolLevel $level */
        foreach ($levels as $level) {
            $data[] = $level->getLevel();
        }

        return $data;
    }
}
