<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Listing.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 21/08/20
 * @time 14.21
 *
 */


namespace ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolAdditionalCost;

use Pimcore\Model\DataObject\RpcSchoolAdditionalCost\Listing as BaseRpcSchoolAdditionalCostListing;

class Listing extends BaseRpcSchoolAdditionalCostListing
{

}
