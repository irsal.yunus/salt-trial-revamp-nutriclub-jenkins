<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource RpcLocation.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 16.26
 *
 */

namespace ResilientParentalCalculatorBundle\Model\DataObject;

use Pimcore\Model\DataObject\RpcLocation as BaseRpcLocation;

class RpcLocation extends BaseRpcLocation
{
    public function getLocation($rpcSchoolLevel = null)
    {
        $data['id'] = $this->getId();
        $data['name'] = $this->getName();
        $data['code'] = $this->getCode();

        $data['school_grades'] = $this->getGrades($rpcSchoolLevel);

        return $data;
    }

    private function getGrades($rpcSchoolLevel = null)
    {
        if (!$this->getSchoolGrades()) {
            return null;
        }

        $schoolGrades = $rpcSchoolLevel && $rpcSchoolLevel instanceof RpcSchoolLevel && $rpcSchoolLevel->getSchoolGrades()
            ? $rpcSchoolLevel->getSchoolGrades() : $this->getSchoolGrades();

        /** @var RpcSchoolGrade $schoolGrade */
        foreach ($schoolGrades as $schoolGrade) {
            $data[] = $schoolGrade->getGrade();
        }

        return $data;
    }
}
