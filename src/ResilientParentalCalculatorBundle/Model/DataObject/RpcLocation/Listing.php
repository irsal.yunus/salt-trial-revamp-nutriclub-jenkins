<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource Listing.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 16.27
 *
 */

namespace ResilientParentalCalculatorBundle\Model\DataObject\RpcLocation;

use Pimcore\Model\DataObject\RpcLocation\Listing as BaseRpcLocationListing;
use ResilientParentalCalculatorBundle\Model\DataObject\RpcLocation;

class Listing extends BaseRpcLocationListing
{
    public function getLocations($orderKey = 'oo_id', $order = 'asc')
    {
        $this->setOrderKey($orderKey);
        $this->setOrder($order);

        $objs = $this->getObjects();

        if (!$objs) {
            return null;
        }

        $data = [];
        /** @var RpcLocation $obj */
        foreach ($objs as $obj) {
            $tmp = $obj->getLocation();
            unset($tmp['school_grades']);
            $data[] = $tmp;
        }

        return $data;
    }
}
