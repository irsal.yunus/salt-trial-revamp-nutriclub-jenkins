<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource LocationService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 16.21
 *
 */

namespace ResilientParentalCalculatorBundle\Services;

use ResilientParentalCalculatorBundle\Model\DataObject\RpcLocation;
use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolLevel;
use Symfony\Component\HttpFoundation\RequestStack;

class LocationService
{
    private $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getLocations($id = null)
    {
        $levelCode = $this->request->query->get('level_code');

        if ($id === null) {
            $rpcLocation = new RpcLocation\Listing();
            return $rpcLocation->getLocations();
        }

        $rpcLocation = RpcLocation::getById($id, 1);

        if (!$rpcLocation) {
            return null;
        }

        $rpcSchoolLevel = null;

        if ($levelCode) {
            $rpcSchoolLevel = RpcSchoolLevel::getByCode($levelCode, 1);
        }

        return $rpcLocation->getLocation($rpcSchoolLevel);
    }

    public function getGrade()
    {

    }
}
