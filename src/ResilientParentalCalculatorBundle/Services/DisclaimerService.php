<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource DisclaimerService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 24/08/20
 * @time 12.42
 *
 */

namespace ResilientParentalCalculatorBundle\Services;

use AppBundle\Helper\GeneralHelper;
use AppBundle\Model\DataObject\Customer;
use AppBundle\Services\UserService;
use AppBundle\Services\UtmService;
use AppBundle\Tool\Folder;
use Carbon\Carbon;
use Pimcore\File;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\Fieldcollection;
use Pimcore\Model\DataObject\RpcCalculate;
use Pimcore\Tool;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\{Request, RequestStack};
use ResilientParentalCalculatorBundle\Model\DataObject\RpcLocation;
use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolAdditionalCost;
use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolGrade;
use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolLevel;
use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolMainCost;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class DisclaimerService
{
    /** @var Request|null $request */
    private $request;

    /** @var RouterInterface $router */
    private $router;

    /** @var Customer|null $user */
    private $user;

    /** @var array $userRaw */
    private $userRaw;

    /** @var array $costArray */
    private $costArray;

    /** @var int $grandTotal */
    private $grandTotal = 0;

    private $utmService;

    public const TRX_COOKIE_CALCULATOR = 'rpc_transaction_id';

    public function __construct(
        RouterInterface $router,
        RequestStack $requestStack,
        UserService $userService,
        UtmService $utmService
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->router = $router;
        $this->user = $userService->getUser();
        $this->userRaw = $userService->getUserRaw();
        $this->utmService = $utmService;
    }

    public function disclaimer()
    {
        $transactionId = Uuid::uuid4();

        try {

            $carbonNow = Carbon::now();
            $folderRpcCalculate = Folder::checkAndCreate('ResilientParentalCalculator');
            $folderYear = Folder::checkAndCreate($carbonNow->format('Y'), $folderRpcCalculate);
            $folderMonth = Folder::checkAndCreate($carbonNow->format('m'), $folderYear);
            $folderDay = Folder::checkAndCreate($carbonNow->format('d'), $folderMonth);

            $rpcCalculator = new RpcCalculate();
            $rpcCalculator->setParent($folderDay);
            $rpcCalculator->setKey(File::getValidFilename($transactionId));

            $rpcCalculator->setMemberId($this->user ? $this->user->getId() : null);
            $rpcCalculator->setEmail($this->user ? $this->user->getEmail() : null);
            $rpcCalculator->setPhone($this->user ? $this->user->getPhone() : null);

            $utms = $this->utmService->getUtms();
            $utmsFilter = count(array_filter($utms));

            if ($utmsFilter > 0) {
                $rpcCalculator->setUtmSource($utms['utm_source'] ?? null);
                $rpcCalculator->setUtmMedium($utms['utm_medium'] ?? null);
                $rpcCalculator->setUtmCampaign($utms['utm_campaign'] ?? null);
                $rpcCalculator->setUtmTerm($utms['utm_term'] ?? null);
                $rpcCalculator->setUtmContent($utms['utm_content'] ?? null);
            }

            $rpcCalculator->setTransactionId($transactionId);
            $rpcCalculator->setPublished(true);

            $rpcCalculator->save([
                'versionNote' => 'DISCLAIMER_AGREE'
            ]);

        } catch (\Exception $exception) {
            Simple::log('DISCLAIMER_SERVICE_' . $carbonNow->format('d-m-Y'), $exception->getMessage());
            $transactionId = null;
        }

        $data['transaction_id'] = $transactionId;

        $this->setTransactionId(self::TRX_COOKIE_CALCULATOR, $transactionId);

        return $data;
    }

    public function pdf()
    {
        $transactionId = $this->request->get('id');

        if (!$transactionId) {
            return null;
        }

        $rpcCalculatorObj = $this->getRpcCalculatorObjByTransactionId($transactionId);

        if (!$rpcCalculatorObj) {
            throw new NotFoundHttpException('Data not found.');
        }

        $memberId = $this->user && $this->user->getId() ? $this->user->getId() : null;

        if ($memberId !== (int)$rpcCalculatorObj->getMemberId()) {
            throw new UnauthorizedHttpException('This file does not belongs for you');
        }

        return $rpcCalculatorObj;
    }

    public function result()
    {
        $data = [];
        $transactionId = $this->request->query->get('transaction_id');

        if (!$transactionId) {
            return null;
        }

        $rpcCalculatorObj = $this->getRpcCalculatorObjByTransactionId($transactionId);

        if (!$rpcCalculatorObj) {
            return $data;
        }

        if (0 === strpos($this->request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($this->request->getContent(), true);
            $this->request->request->replace(is_array($data) ? $data : []);
        }

        $cost = $this->cost($rpcCalculatorObj, $this->request->request->all());
        $rpcCalculatorObj->setCosts($cost);

        try {
            $rpcCalculatorObj->save([
                'versionNote' => 'DISCLAIMER_SERVICE_UPDATE_COSTS'
            ]);
        } catch (\Exception $exception) {
            Simple::log(
                'DISCLAIMER_SERVICE_' . Carbon::now()->format('dmY'),
                $exception->getMessage()
            );
        }

        $data['costs'] = $this->costArray;
        $data['transaction_id'] = $transactionId;
        $data['pdf_file'] = $this->router->generate('disclaimer-pdf-generator', [
            'id' => $transactionId
        ], UrlGeneratorInterface::ABSOLUTE_URL);
        $data['share_file'] = Tool::getHostUrl(env('HTTP_PROTOCOL')) .
            '/assets/resilient-parental-calculator/' .
            $transactionId . '.png';
        $data['grand_total'] = $this->grandTotal;

        $child = $this->userRaw && is_array($this->userRaw['Childs']) ? $this->userRaw['Childs'] : [];
        $rearrangeChild = GeneralHelper::rearrangeChildBasedOnAge($child);

        $firstChildBirthdate = $rearrangeChild[0]['Birthdate'] ?: null;
        $dateFormat = env('RRR_LIVE') ? 'd/m/Y' : 'd-m-Y';
        $firstChildBirthdateAge = $firstChildBirthdate ?
            Carbon::createFromFormat($dateFormat, $firstChildBirthdate)->age : 0;

        $data['is_show_banner'] = ($this->user && $this->user->getAgePregnant() > 0) || $firstChildBirthdateAge < 1;

        $this->setTransactionId(self::TRX_COOKIE_CALCULATOR, null, true);

        $generateImage = $this->callCommandGenerateImage($transactionId);

        return $data;
    }

    private function cost(RpcCalculate $rpcCalculate, array $data)
    {
        $costs = $data['costs'] ?? null;

        if (!$costs) {
            return $costs;
        }

        $costCollection = new Fieldcollection();
        foreach ($costs as $key => $cost) {
            $mainCost = $cost['main_cost'];
            $additionalCost = $cost['additional_cost'];

            $levelId = $cost['level_id'];
            $gradeId = $cost['grade_id'];
            $locationId = $cost['location_id'];
            $totalStudyYear = $cost['total_study_year'];

            $mainCostObj = RpcSchoolMainCost::getById($mainCost, 1);
            if (!$mainCostObj) {
                continue;
            }

            $levelObj = RpcSchoolLevel::getById($levelId, 1);
            if (!$levelObj) {
                continue;
            }

            $gradeObj = RpcSchoolGrade::getById($gradeId, 1);
            if (!$gradeObj) {
                continue;
            }

            $locationObj = RpcLocation::getById($locationId, 1);
            if (!$locationObj) {
                continue;
            }

            $additionalCostObj = RpcSchoolAdditionalCost::getById($additionalCost, 1);

            $fieldCollectionCost = new Fieldcollection\Data\Costs();
            $fieldCollectionCost->setLevel($levelObj);
            $fieldCollectionCost->setGrade($gradeObj);
            $fieldCollectionCost->setLocation($locationObj);

            $fieldCollectionCost->setTotalStudyYear($totalStudyYear);

            $fieldCollectionCost->setMainCost($mainCostObj);

            $fieldCollectionCost->setAdditionalCost($additionalCostObj);

            $costCollection->add($fieldCollectionCost);

            $this->costArray[$key]['image_head'] = Tool::getHostUrl(env('HTTP_PROTOCOL')) . $levelObj->getIcon()->getFullPath();
            $this->costArray[$key]['level_name'] = $levelObj->getName();
            $this->costArray[$key]['location_name'] = $locationObj->getName();
            $this->costArray[$key]['grade_name'] = $gradeObj->getName();
            $this->costArray[$key]['total_study_year'] = $totalStudyYear;
            $this->costArray[$key]['main_cost'] = $this->getTotalMainCost($mainCostObj, $totalStudyYear);
            $this->costArray[$key]['additional_cost'] =
                $additionalCostObj ? $this->getTotalAdditionalCost($additionalCostObj, $totalStudyYear) : 0;

            $this->grandTotal += $this->costArray[$key]['main_cost'] + $this->costArray[$key]['additional_cost'];
        }

        return $costCollection;
    }

    private function getRpcCalculatorObjByTransactionId($transactionId)
    {
        $rpcCalculatorObj = RpcCalculate::getByTransactionId($transactionId, 1);

        if (!$rpcCalculatorObj) {
            return null;
        }

        return $rpcCalculatorObj;
    }

    private function setTransactionId($cookieName, $value, $expired = false)
    {
        $dotNetDomain = env('WILDCARD_DOMAIN_FOR_DOT_NET', null);

        $expiredCookie = $expired ? 1 : Carbon::now()->addMinutes(30)->timestamp;

        setrawcookie(
            $cookieName,
            $value,
            $expiredCookie,
            '/',
            $dotNetDomain
        );
    }

    private function getTotalMainCost(RpcSchoolMainCost $mainCostObj, $totalStudyYear)
    {
        $admission = (int)$mainCostObj->getAdmissionFee();
        $montyTotal = ($mainCostObj->getMonthlyFee() * ($totalStudyYear * 12));
        $yearlyTotal = $mainCostObj->getYearlyFee() * $totalStudyYear;

        return $admission + $montyTotal + $yearlyTotal;
    }

    private function getTotalAdditionalCost(RpcSchoolAdditionalCost $additionalCostObj, $totalStudyYear)
    {
        $book = (int)$additionalCostObj->getBook();
        $uniform = (int)$additionalCostObj->getUniform();
        $activities = (int)$additionalCostObj->getActivities();
        $catering = (int)($additionalCostObj->getMonthlyCatering() * ($totalStudyYear * 12));
        $shuttle = (int)($additionalCostObj->getMonthlyShuttle()  * ($totalStudyYear * 12));
        $others = (int)$additionalCostObj->getOthers();

        return $book + $uniform + $activities + $catering + $shuttle + $others;
    }

    public function share()
    {
        $transactionId = $this->request->get('id');

        if (!$transactionId) {
            return null;
        }

        $rpcCalculatorObj = $this->getRpcCalculatorObjByTransactionId($transactionId);

        if (!$rpcCalculatorObj) {
            throw new NotFoundHttpException('Data not found.');
        }

        return $rpcCalculatorObj;
    }

    private function callCommandGenerateImage($transactionId)
    {
        $execCommand = env('RPC_EXEC_COMMAND');
        $execCommand .= $transactionId;

        exec($execCommand);
    }
}
