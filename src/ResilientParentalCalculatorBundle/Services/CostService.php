<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CostService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 21/08/20
 * @time 12.27
 *
 */

namespace ResilientParentalCalculatorBundle\Services;

use Pimcore\Model\DataObject\RpcSchoolCost;
use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolAdditionalCost;
use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolMainCost;
use Symfony\Component\HttpFoundation\RequestStack;

class CostService
{
    private $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getCost()
    {
        $locationId = $this->request->get('locationId');
        $gradeId = $this->request->get('gradeId');
        $levelId = $this->request->get('levelId');

        $cost = new RpcSchoolCost\Listing();
        $cost->setCondition('location__id = ? AND grade__id = ? AND level__id = ?', [
            $locationId, $gradeId, $levelId
        ]);

        $count = $cost->getCount();

        if ($count < 1) {
            return [
                'main_cost' => null,
                'additional_cost' => null,
            ];
        }

        $costData = [];
        foreach ($cost as $item) {
            $costData['main_cost']['cost'] = $this->gatherMainCost($item->getMainCost());

            $costData['additional_cost']['title'] = $item->getTitle();
            $costData['additional_cost']['description'] = $item->getDescription();
            $costData['additional_cost']['cost'] = $this->gatherAdditionalCost($item->getAdditionalCost());
        }

        return $costData;
    }

    private function gatherMainCost(array $mainCost)
    {
        if (!$mainCost) {
            return null;
        }

        $data = [];

        /** @var RpcSchoolMainCost $item */
        foreach ($mainCost as $item) {
            $data[] = $item->getMainCost();
        }

        return $data;
    }

    private function gatherAdditionalCost(array $additionalCost)
    {
        if (!$additionalCost) {
            return null;
        }

        $data = [];

        /** @var RpcSchoolAdditionalCost $item */
        foreach ($additionalCost as $item) {
            $data[] = $item->getAdditionalCost();

        }

        return $data;
    }
}
