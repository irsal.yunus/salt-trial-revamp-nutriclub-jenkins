<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource LevelService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 21/08/20
 * @time 14.43
 *
 */

namespace ResilientParentalCalculatorBundle\Services;

use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolLevel;

class LevelService
{
    public function getLevel()
    {
        $levels = new RpcSchoolLevel\Listing();

        return $levels->getLevels();
    }
}
