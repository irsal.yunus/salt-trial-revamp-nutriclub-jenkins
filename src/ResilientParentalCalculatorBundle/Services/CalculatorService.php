<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CalculatorService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 14/09/20
 * @time 13.03
 *
 */

namespace ResilientParentalCalculatorBundle\Services;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RequestStack;

class CalculatorService
{
    private $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function hasCookieTransactionId()
    {
        $cookies = $this->request->cookies;

        if (!$cookies->has(DisclaimerService::TRX_COOKIE_CALCULATOR)) {
            throw new AccessDeniedException('Access denied no transaction id');
        }

        return true;
    }
}
