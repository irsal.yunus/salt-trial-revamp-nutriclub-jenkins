<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource GradeService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 24/08/20
 * @time 14.03
 *
 */

namespace ResilientParentalCalculatorBundle\Services;

use ResilientParentalCalculatorBundle\Model\DataObject\RpcSchoolGrade;

class GradeService
{
    public function getGrades()
    {
        $rpcGrades = new RpcSchoolGrade\Listing();
        $data = $rpcGrades->getGrades();

        return $data;
    }
}
