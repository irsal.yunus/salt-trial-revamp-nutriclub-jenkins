<?php

namespace ResilientParentalCalculatorBundle\Controller;

use Pimcore\Model\WebsiteSetting;
use ResilientParentalCalculatorBundle\Form\DisclaimerFormType;
use ResilientParentalCalculatorBundle\Services\DisclaimerService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     * @param DisclaimerService $disclaimerService
     *
     * @Route("/")
     *
     * @return RedirectResponse
     */
    public function indexAction(Request $request, DisclaimerService $disclaimerService)
    {
        if ($request->getMethod() === 'POST') {
            $disclaimer = $disclaimerService->disclaimer();
            if ($disclaimer['transaction_id']) {
                $documentPage = WebsiteSetting::getByName('RPC_CALCULATOR_LANDING_PAGE') ?
                    WebsiteSetting::getByName('RPC_CALCULATOR_LANDING_PAGE')->getData() : 'document_1';

                $documentPage = $documentPage instanceof \Pimcore\Model\Document\Page ?
                    'document_' . $documentPage->getId() : $documentPage;

                return $this->redirect($this->generateUrl($documentPage));
            }
        }
        $formDisclaimer = $this->createForm(DisclaimerFormType::class, [], []);
        $this->view->formDisclaimer = $formDisclaimer->createView();
    }
}
