<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CostController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 21/08/20
 * @time 12.19
 *
 */

namespace ResilientParentalCalculatorBundle\Controller;

use ResilientParentalCalculatorBundle\Services\CostService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CostController
 * @package ResilientParentalCalculatorBundle\Controller
 *
 * @Route("/cost")
 */
class CostController extends AbstractController
{
    /**
     * @param Request $request
     * @param CostService $costService
     *
     * @Route("/", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function index(Request $request, CostService $costService)
    {
        return $this->json([
            'status' => '00',
            'message' => 'Success',
            'data' => $costService->getCost()
        ], 200);
    }
}
