<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource LevelController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 21/08/20
 * @time 12.19
 *
 */

namespace ResilientParentalCalculatorBundle\Controller;

use ResilientParentalCalculatorBundle\Services\LevelService;
use Symfony\Component\HttpFoundation\{JsonResponse, Request};
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LevelController
 * @package ResilientParentalCalculatorBundle\Controller
 *
 * @Route("/level")
 */
class LevelController extends AbstractController
{
    /**
     * @param Request $request
     * @param LevelService $levelService
     *
     * @Route("/", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function index(Request $request, LevelService $levelService)
    {
        return $this->json([
            'status' => '00',
            'message' => 'Success',
            'data' => $levelService->getLevel()
        ], 200);
    }
}
