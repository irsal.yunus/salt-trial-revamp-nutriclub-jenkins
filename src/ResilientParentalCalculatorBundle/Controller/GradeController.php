<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource GradeController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 17.20
 *
 */

namespace ResilientParentalCalculatorBundle\Controller;

use Symfony\Component\HttpFoundation\{JsonResponse, Request};
use ResilientParentalCalculatorBundle\Services\GradeService;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GradeController
 * @package ResilientParentalCalculatorBundle\Controller
 *
 * @Route("/grade")
 */
class GradeController extends AbstractController
{
    /**
     * @param Request $request
     * @param GradeService $gradeService
     *
     * @Route("/", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function index(Request $request, GradeService $gradeService)
    {
        return $this->json([
            'status' => '00',
            'message' => 'Success',
            'data' => $gradeService->getGrades()
        ], 200);
    }
}
