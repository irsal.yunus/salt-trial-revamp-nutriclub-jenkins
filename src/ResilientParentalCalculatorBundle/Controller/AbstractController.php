<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource AbstractController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 14.30
 *
 */

namespace ResilientParentalCalculatorBundle\Controller;

use AppBundle\Controller\AbstractController as BaseAbstractController;

abstract class AbstractController extends BaseAbstractController
{

}
