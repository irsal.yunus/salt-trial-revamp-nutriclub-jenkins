<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource DisclaimerController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 24/08/20
 * @time 12.41
 *
 */

namespace ResilientParentalCalculatorBundle\Controller;

use Carbon\Carbon;
use Symfony\Component\HttpFoundation\{JsonResponse, Request, Response};
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\RpcCalculate;
use Pimcore\Tool\Console;
use Pimcore\Web2Print\Processor;
use ResilientParentalCalculatorBundle\Services\DisclaimerService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class DisclaimerController
 * @package ResilientParentalCalculatorBundle\Controller
 *
 * @Route("/disclaimer")
 */
class DisclaimerController extends AbstractController
{
    /**
     * @param DisclaimerService $disclaimerService
     *
     * @Route("/pdf/{id}", methods={"GET"}, name="disclaimer-pdf-generator")
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function pdf(DisclaimerService $disclaimerService)
    {
        //return the pdf
        $html = $this->renderView(
            '@ResilientParentalCalculatorBundle/Resources/views/Disclaimer/pdf.html.php',
            [
                'rpcCalculatorObj' => $disclaimerService->pdf()
            ]
        );
        return new Response(
            Processor::getInstance()->getPdfFromString($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
            )
        );
    }

    /**
     * @param DisclaimerService $disclaimerService
     *
     * @Route("/result", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function result(DisclaimerService $disclaimerService)
    {
        return $this->json([
            'status' => '00',
            'message' => 'Success',
            'data' => $disclaimerService->result()
        ], 200);
    }

    /**
     * @param string $id
     * @param DisclaimerService $disclaimerService
     *
     * @Route("/share/{id}", methods={"GET"}, name="disclaimer-share-generator")
     *
     */
    public function share(DisclaimerService $disclaimerService)
    {
        $rpcCalculatorObj = $disclaimerService->share();
        $this->view->rpcCalculatorObj = $rpcCalculatorObj;
    }
}
