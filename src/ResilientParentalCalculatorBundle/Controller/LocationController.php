<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource LocationController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 16.15
 *
 */

namespace ResilientParentalCalculatorBundle\Controller;

use Symfony\Component\HttpFoundation\{JsonResponse, Request};
use ResilientParentalCalculatorBundle\Services\LocationService;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LocationController
 * @package ResilientParentalCalculatorBundle\Controller
 *
 * @Route("/location")
 */
class LocationController extends AbstractController
{
    /**
     * @param Request $request
     * @param LocationService $locationService
     *
     * @return JsonResponse
     * @Route("/", methods={"GET"})
     */
    public function index(Request $request, LocationService $locationService)
    {
        return $this->json([
            'status' => '00',
            'message' => 'Success',
            'data' => $locationService->getLocations()
        ], 200);
    }

    /**
     * @param int $id
     * @param LocationService $locationService
     *
     * @return JsonResponse
     * @Route("/{id}", methods={"GET"})
     */
    public function detail($id, LocationService $locationService)
    {
        return $this->json([
            'status' => '00',
            'message' => 'Success',
            'data' => $locationService->getLocations($id)
        ], 200);
    }
}
