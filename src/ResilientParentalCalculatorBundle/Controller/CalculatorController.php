<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CalculatorController.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 14.53
 *
 */

namespace ResilientParentalCalculatorBundle\Controller;

use ResilientParentalCalculatorBundle\Services\CalculatorService;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class CalculatorController extends AbstractController
{
    /**
     * @param Request $request
     * @param CalculatorService $calculatorService
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(Request $request, CalculatorService $calculatorService)
    {
        $calculatorService->hasCookieTransactionId();

        $this->view->user = $request->getSession()->get('UserProfile', null);
    }
}
