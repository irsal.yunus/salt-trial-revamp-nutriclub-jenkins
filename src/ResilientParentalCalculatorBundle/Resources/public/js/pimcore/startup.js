pimcore.registerNS("pimcore.plugin.ResilientParentalCalculatorBundle");

pimcore.plugin.ResilientParentalCalculatorBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.ResilientParentalCalculatorBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("ResilientParentalCalculatorBundle ready!");
    }
});

var ResilientParentalCalculatorBundlePlugin = new pimcore.plugin.ResilientParentalCalculatorBundle();
