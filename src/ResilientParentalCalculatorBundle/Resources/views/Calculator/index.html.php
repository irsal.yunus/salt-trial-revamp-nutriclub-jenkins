<?php

/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 14.54
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$baseURl = 'https://phase1.revamp-nutri.staging11.salt.id';
$this->extend('layout.html.php');
$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/resilent-parental-calculator/wizard.css', 'screen', false);

/** @var \AppBundle\Model\DataObject\Customer $user */
$user = $this->user;
?>
<?php $this->headScript()->offsetSetFile(11, '/assets/vendor/vue/vue.min.js', 'text/javascript'); ?>
<?php $this->headScript()->offsetSetFile(12, '/assets/js/pages/step.js', 'text/javascript'); ?>
<?php $this->headScript()->offsetSetFile(13, '/assets/js/pages/wizard.js', 'text/javascript'); ?>

<!-- MultiStep Form -->
<div id="StepIntances">
    <div class="wizard-step">
        <div class="grad" id="grad1">
            <form id="msform">
                <!-- progressbar -->
                <ul id="progressbar">
                    <li class="active wizard-tab" id="wizard-tab1"><strong>1</strong></li>
                    <li class="wizard-tab" id="wizard-tab2"><strong>2</strong></li>
                    <li class="wizard-tab" id="wizard-tab3"><strong>3</strong></li>
                    <li class="wizard-tab" id="wizard-tab4"><strong>4</strong></li>
                    <li class="wizard-tab" id="wizard-tab5"><strong>5</strong></li>
                    <li class="wizard-tab" id="wizard-tab6"><strong>6</strong></li>
                    <li class="wizard-tab" id="wizard-tab7"><strong>7</strong></li>
                    <li class="wizard-tab" id="wizard-tab8"><strong>8</strong></li>
                </ul> <!-- fieldsets -->
                <fieldset class="wizard-step__question">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2 col-sm-12">
                                <div class="wizard-step__question-img">
                                    <?= $this->image('calculator-image-desktop', []) ?>
                                </div>
                                <div class="wizard-step__question-content">
                                    <h4><?= $this->t('SCHOOL_GRADE_AND_LOCATION') ?></h4>
                                    <p>
                                        <?= $this->t('RPC_CALCULATOR_DESCRIPTION', [
                                            '%genderFriendlyName' => $user ?
                                                ucfirst($user->getGenderFriendlyName()) : null,
                                            '%name' => $user ? $user->getFullname() : null
                                        ]) ?>
                                    </p>
                                    <span class="sub-title">
                                        <?= $this->t('RPC_CALCULATOR_SUB_TITLE', [
                                            '%genderFriendlyName' => $user ?
                                                ucfirst($user->getGenderFriendlyName()) : null,
                                            '%name' => $user ? $user->getFullname() : null
                                        ]) ?>
                                    </span>
                                    <div class="select-content">
                                        <label for="" class="warp-select1">
                                            <select class="select1" name="state" @focus="resetGrd" v-model="selectCty" @change="getGradeByLocation($event)">
                                                <option value="0" disabled>Pilih Daerah</option>
                                                <option v-for="item in location" :value="item.id" :key="item.id">{{ item.name }}</option>
                                            </select>
                                        </label>
                                        <label for="" class="warp-select1">
                                            <select class="select1" name="state" v-model="selectGrd" :disabled="selectCty == null">
                                                <option value="0" disabled>Pilih Grade</option>
                                                <option v-for="item in gradeByLocation" :value="item.id" :key="item.id">{{ item.name }}</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary next" v-on:click="getCostFirst" :disabled="selectGrd == 0">Selanjutnya</button>
                </fieldset>
                <fieldset v-for="(item, index) in wizard" class="wizard-step__choose" :id="index" :stepid="item.id" :stepcode="item.code">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 offset-md-1 col-sm-12">
                                <div class="wizard-step__choose-title">
                                    <div class="icon">
                                        <img :src="item.icon" alt="">
                                    </div>
                                    <h1>{{ item.name }}</h1>
                                    <div class="set-plan">
                                        <span class="text-city">Mama bisa memilih perkiraan biaya sekolah di</span>
                                        <label for="" class="wrap-select2">
                                            <select class="select2" :disabled="item.code == 'KULIAH'" name="state" @focus="resetGrd" v-model="selectCty" @change="getGradeByLocation($event)">
                                                <option value="0" disabled>Pilih Daerah</option>
                                                <option v-for="item in location" :value="item.id">{{ item.name }}</option>
                                            </select>
                                        </label>
                                        <span class="text-plan">dengan grade</span>
                                        <label for="" class="wrap-select2">
                                            <select class="select2" v-model="selectGrd" @change="getCost(item.id)" name="state">
                                                <option value="0" disabled>Pilih Grade</option>
                                                <option v-for="item in gradeByLocation" :value="item.id">{{ item.name }}</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                                <div class="wizard-step__choose-plan">
                                    <div class="row justify-content-center">
                                        <div class="col-sm-12 col-md-12 col-lg-12" v-show="item.code == 'KULIAH'">
                                            <div class="alert-collage">
                                                <span>!</span>
                                                <h2>Untuk tahap kuliah, ada perubahan dalam grade, Mama bisa memilih lagi grade untuk kuliah </h2>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12" v-show="alertstep == true">
                                            <div class="alert alert-danger" role="alert">
                                                <?= $this->t('RPC_ADDITIONAL_COST_DOES_NOT_EXIST', [
                                                    '%genderFriendlyName' => $user ?
                                                        ucfirst($user->getGenderFriendlyName()) : null,
                                                    '%name' => $user ? $user->getFullname() : null
                                                ])  ?>
                                            </div>
                                        </div>
                                        <div class="loading col-md-12 col-sm-12 col-lg-12" v-show="isLoading == true">
                                            <div class="lds-ellipsis">
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-4" v-show="isLoading == false" v-for="(item_cost, index) in main_cost" :key="item_cost.id">
                                            <div class="item-choose-plan">
                                                <input type="radio" :id="item.code+item_cost.id+index" :name="item.code" v-on:change="selectedPlan(item_cost.id)" :value="item_cost.id">
                                                <label :for="item.code+item_cost.id+index" class="card-plan">
                                                    <div class="card-plan__title">
                                                        <div class="cicle">
                                                            <div class="cicle-none">
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                            </div>
                                                        </div>
                                                        <h2>{{ item_cost.name }} </h2>
                                                        <img class="img-plan" :src="item_cost.image_icon" alt="">
                                                    </div>
                                                    <div class="card-plan__body">
                                                        <ul>
                                                            <li class="item">
                                                                <span>
                                                                    Uang Pangkal
                                                                </span>
                                                                <strong v-show="item_cost.admission_fee">{{ convetRp(item_cost.admission_fee) }}</strong>
                                                                <strong v-show="!item_cost.admission_fee">-</strong>
                                                            </li>
                                                            <li class="item">
                                                                <span>
                                                                    SPP/bulan
                                                                </span>
                                                                <strong v-show="item_cost.monthly_fee">{{ convetRp(item_cost.monthly_fee) }} </strong>
                                                                <strong v-show="!item_cost.monthly_fee">- </strong>

                                                            </li>
                                                            <li class="item">
                                                                <span>
                                                                    Biaya/Tahun
                                                                </span>
                                                                <strong v-show="item_cost.yearly_fee">{{ convetRp(item_cost.yearly_fee) }}</strong>
                                                                <strong v-show="!item_cost.yearly_fee">-</strong>
                                                            </li>
                                                        </ul>
                                                        <a href="#" class="btn btn-outline-primary btn-choose">Pilih Biaya ini</a>
                                                        <a href="#" class="btn btn-notchoose">Biaya Terpilih</a>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wizard-step__choose-plan dana-sosial" v-show="additional_cost.cost">
                                    <div class="content-dana" v-show="!setNull">
                                        <h2>{{ additional_cost.title }}</h2>
                                        <div v-html="additional_cost.description"></div>
                                    </div>
                                    <div class="row justify-content-center" v-show="!setNull">
                                        <div class="loading col-md-12 col-sm-12 col-lg-12" v-show="isLoading == true">
                                            <div class="lds-ellipsis">
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-4" v-show="isLoading == false" v-for="( item_dana, index ) in additional_cost.cost" :key="item_dana.id">
                                            <div class="item-choose-plan">
                                                <input type="radio" :id="item.code + item_dana.id + index " v-on:change="selectedPlanDana(item_dana.id)" name="danasosial" :value="item_dana.id">
                                                <label :for="item.code + item_dana.id + index" class="card-plan">
                                                    <div class="card-plan__title">
                                                        <div class="cicle">
                                                            <div class="cicle-none">
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                            </div>
                                                        </div>
                                                        <h2>{{ item_dana.name }}</h2>
                                                        <img class="img-plan" :src="item_dana.image_icon" alt="">
                                                    </div>
                                                    <div class="card-plan__body">
                                                        <ul>
                                                            <li class="item">
                                                                <span>
                                                                    Buku
                                                                </span>
                                                                <strong v-show="item_dana.book">{{ convetRp(item_dana.book) }}</strong>
                                                                <strong v-show="!item_dana.book">-</strong>
                                                            </li>
                                                            <li class="item">
                                                                <span>
                                                                    Seragam
                                                                </span>
                                                                <strong v-show="item_dana.uniform">{{ convetRp(item_dana.uniform) }} </strong>
                                                                <strong v-show="!item_dana.uniform">-</strong>
                                                            </li>
                                                            <li class="item">
                                                                <span>
                                                                    Kegiatan
                                                                </span>
                                                                <strong v-show="item_dana.activities">{{ convetRp(item_dana.activities) }}</strong>
                                                                <strong v-show="!item_dana.activities">-</strong>
                                                            </li>
                                                            <li class="item">
                                                                <span>
                                                                    Ketering/Bulan
                                                                </span>
                                                                <strong v-show="item_dana.monthly_catering">{{ convetRp(item_dana.monthly_catering) }}</strong>
                                                                <strong v-show="!item_dana.monthly_catering">-</strong> </li>
                                                            <li class="item">
                                                                <span>
                                                                    Jemputan/Bulan
                                                                </span>
                                                                <strong v-show="item_dana.monthly_shuttle">{{ convetRp(item_dana.monthly_shuttle) }}</strong>
                                                                <strong v-show="!item_dana.monthly_shuttle">-</strong>
                                                            </li>
                                                            <li class="item">
                                                                <span>
                                                                    Lainnya
                                                                </span>
                                                                <strong v-show="item_dana.others">{{ convetRp(item_dana.others) }}</strong>
                                                                <strong v-show="!item_dana.others">-</strong>
                                                            </li>
                                                        </ul>
                                                        <a href="#" class="btn btn-outline-primary btn-choose">Pilih Biaya ini</a>
                                                        <a href="#" class="btn btn-notchoose">Biaya Terpilih</a>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" v-on:click="previous" class="btn btn-outline previous action-button-previous">Sebelumnya</button>
                    <button type="button" v-on:click="next" class="btn btn-primary next" :disabled="checkSelect">Selanjutnya</button>
                </fieldset>
                <fieldset stepcode="result">
                    <div class="form-card wizard-step__result">
                        <div class="">
                            <div class="col-md-10 offset-md-1 col-sm-12">
                                <div class="container">
                                    <div class="wizard-step__result-title">
                                        <h2>
                                            <?= $this->t('RPC_RESULT_HI', [
                                                '%genderFriendlyName' => $user ?
                                                    ucfirst($user->getGenderFriendlyName()) : null,
                                                '%name' => $user ? $user->getFullname() : null
                                            ]) ?>
                                        </h2>
                                        <p>
                                            <?= $this->t('RPC_RESULT_COST', [
                                                '%genderFriendlyName' => $user ?
                                                    ucfirst($user->getGenderFriendlyName()) : null,
                                                '%name' => $user ? $user->getFullname() : null
                                            ]) ?>
                                        </p>
                                        <span class="price-result">
                                            {{ convetRp(resultPDF ? resultPDF.grand_total : null) }}
                                        </span>
                                        <img src="/assets/images/resilient-calculator/momdad.svg" alt="" />
                                    </div>
                                    <div class="wizard-step__result-body">
                                        <h2>
                                            <?= $this->t('RPC_RESULT_DETAIL_COST_EACH_STEP', [
                                                '%genderFriendlyName' => $user ?
                                                    ucfirst($user->getGenderFriendlyName()) : null,
                                                '%name' => $user ? $user->getFullname() : null
                                            ]) ?>
                                        </h2>
                                        <p class="text-body">
                                            <?= $this->t('RPC_RESULT_COST_EACH_STEP', [
                                                '%genderFriendlyName' => $user ?
                                                    ucfirst($user->getGenderFriendlyName()) : null,
                                                '%name' => $user ? $user->getFullname() : null
                                            ]) ?>
                                        </p>
                                        <div class="list-colage" v-show="resultPDF">
                                            <div class="row">
                                                <div class="col-md-4 parent" v-for="(item, index) in resultPDF ? resultPDF.costs : null" :key="index">
                                                    <div class="list-colage__item oncheck-item" v-if="resultPDF.costs.length === 3">
                        
                                                        <div class="list-colage__item-title">
                                                            <div class="bg"></div>
                                                            <img :src="item.image_head" alt="">
                                                        </div>
                                                        <div class="list-colage__item-content">
                                                            <h3>{{item.level_name}}</h3>
                                                            <ul>
                                                                <li>
                                                                    {{ item.location_name }}
                                                                </li>
                                                                <li>
                                                                    |
                                                                </li>
                                                                <li>{{ item.grade_name }}</li>
                                                                <li>|</li>
                                                                <li>
                                                                    {{ item.total_study_year }} <?= $this->t('YEAR') ?>
                                                                </li>
                                                            </ul>
                                                            <div class="price-content">
                                                                <p v-show="item.main_cost" class="text">Total Dana Pendidikan</p>
                                                                <span v-show="item.main_cost">{{ convetRp(item.main_cost) }}</span>
                                                                <p v-show="item.additional_cost" class="text">Total Dana Sosial</p>
                                                                <span v-show="item.additional_cost">{{ convetRp(item.additional_cost) }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="list-colage__item" v-else>
                        
                                                        <div class="list-colage__item-title">
                                                            <div class="bg"></div>
                                                            <img :src="item.image_head" alt="">
                                                        </div>
                                                        <div class="list-colage__item-content">
                                                            <h3>{{item.level_name}}</h3>
                                                            <ul>
                                                                <li>
                                                                    {{ item.location_name }}
                                                                </li>
                                                                <li>
                                                                    |
                                                                </li>
                                                                <li>{{ item.grade_name }}</li>
                                                                <li>|</li>
                                                                <li>
                                                                    {{ item.total_study_year }} <?= $this->t('YEAR') ?>
                                                                </li>
                                                            </ul>
                                                            <div class="price-content">
                                                                <p v-show="item.main_cost" class="text">Total Dana Pendidikan</p>
                                                                <span v-show="item.main_cost">{{ convetRp(item.main_cost) }}</span>
                                                                <p v-show="item.additional_cost" class="text">Total Dana Sosial</p>
                                                                <span v-show="item.additional_cost">{{ convetRp(item.additional_cost) }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="download-pdf">
                                            <a :href="resultPDF ? resultPDF.pdf_file : null" class="btn btn-outline-primary" download>
                                                <?= $this->t('RPC_RESULT_DOWNLOAD_RESULT') ?>
                                            </a>
                                            <?= $this->link('count-over', []) ?>
                                            <ul>
                                                <li>
                                                    <?= $this->t('SHARE') ?> :
                                                </li>
                                                <li>
                                                    <a :href="shareUrlFacebook + shareImg" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a :href="shareUrlTwitter + shareImg" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li class="ic-wa">
                                                    <a :href="shareUrlWa + shareImg" target="_blank" data-action="share/whatsapp/share"><i class="fa fa-whatsapp" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 offset-md-1 col-sm-12">
                                <div class="container">
                                    <div class="news-article">
                                        <?= $this->image('image-article') ?>
                                        <h3>
                                            <?= $this->input('title-article') ?>
                                        </h3>
                                        <?= $this->wysiwyg('description-article') ?>
                                        <?= $this->link('link-article') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="mom-pregnant" v-show="resultPDF ? resultPDF.is_show_banner : null">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-10 offset-md-1 col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <?= $this->image('background') ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="mom-pregnant__body">
                                                            <h2>
                                                                <?= $this->input('title') ?>
                                                            </h2>
                                                            <?= $this->wysiwyg('description') ?>
                                                            <?= $this->link('calculate-now', [
                                                                'class' => 'btn btn-primary'
                                                            ]) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
