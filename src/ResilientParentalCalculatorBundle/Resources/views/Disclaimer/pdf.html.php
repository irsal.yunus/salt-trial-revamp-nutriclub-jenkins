<?php

/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource pdf.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 24/08/20
 * @time 13.13
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/resilent-parental-calculator/pdf.css', 'screen', true);

$baseUrl = env('RPC_BASE_URL_ASSET', 'https://localhost');

/** @var \Pimcore\Model\DataObject\RpcCalculate $rpcCalculatorObj */
$rpcCalculatorObj = $this->rpcCalculatorObj;
/** @var \AppBundle\Model\DataObject\Customer $user */
$user = $this->session()->get('UserProfile', null);
?>
<style>
    @font-face {
        font-family: 'GothamRounded-Bold';
        src: url('<?= $baseUrl ?>/assets/fonts/GothamRounded-Bold.woff2') format('woff2');
        font-weight: bold;
        font-style: normal;
    }

    @font-face {
        font-family: 'GothamRounded-Book';
        src: url('<?= $baseUrl ?>/assets/fonts/GothamRounded-Book.woff2') format('woff2');
        font-weight: normal;
        font-style: normal;
    }

    .subtitle {
        background-image: linear-gradient(95deg,
                #2384c6 3%,
                #39afff 51%,
                #2384c6);
        -webkit-background-clip: text;
        -webkit-text-fill-color: rgba(0, 0, 0, 0);
        font-size: 40px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.4;
        letter-spacing: normal;
        text-align: center;
        margin-bottom: 0;
    }

    .img-calculator {
        width: 337.4px;
        height: 35px;
        object-fit: contain;
    }

    .text-colages {
        position: relative;
        margin-top: 28px;
    }

    .text-colages hr {
        position: relative;
        top: 5px;
        width: 15px;
        height: 2px;
        background-color: #2384c6;
        z-index: 1;
        display: inline-block;
    }

    .text-colages span {
        font-family: "GothamRounded-Book";
        font-size: 25px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.4;
        letter-spacing: normal;
        text-align: center;
        color: #2384c6;
        z-index: 99;
        display: inline-block;
    }

    .text-title-mom {
        text-align: center;
        margin-top: 13px;
    }

    .text-title-mom span {
        display: block;
        font-family: "GothamRounded-Bold";
        font-size: 18px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        letter-spacing: normal;
        text-align: center;
        color: #2384c6;
        margin-bottom: -10px;
    }

    .text-title-mom p {
        display: block;
        font-family: "GothamRounded-Bold";
        font-size: 18px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: normal;
        text-align: center;
        color: #2384c6;
        margin-bottom: 0px;
    }

    .item-list {
        width: 100%;
        text-align: left;

    }

    .item-list .title-list {
        font-family: "GothamRounded-Bold";
        font-size: 22px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        letter-spacing: normal;
        text-align: left;
        color: #1a3491;
    }

    .item-list .subtitle-list {
        font-family: "GothamRounded-Book";
        font-size: 19px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.42;
        letter-spacing: normal;
        text-align: left;
        color: #555555;
    }

    .item-list .text-list {
        margin-left: 60px;
        border-bottom: 1px solid;
        border-color: #767676;
        margin-bottom: 20px;

    }

    .item-list .text-list li {
        margin-bottom: 10px;
    }

    .text-list li .text-detail {
        font-family: "GothamRounded-Book";
        font-size: 18px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: normal;
        text-align: left;
        color: #767676;
        display: inline-block;
        width: 350px;
    }

    .text-list li .text-price {
        display: inline-block;
        text-align: left;
        font-family: "GothamRounded-Book";
        font-size: 18px;
        font-weight: 600;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: normal;
        color: #2384c6;
    }


    .total-price .text-total {
        font-family: "GothamRounded-Book";
        font-size: 18px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: normal;
        text-align: left;
        color: #767676;
        display: inline-block;
        width: 410px;
    }

    .total-price .text-total-price {
        font-family: "GothamRounded-Bold";
        font-size: 18px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: normal;
        color: #2384c6;
        display: inline-block;
    }

    .all-price {
        border-top: 1px solid #767676;
        padding-top: 20px;
    }

    .all-price .text-all-price {
        font-family: "GothamRounded-Bold";
        font-size: 27px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.37;
        letter-spacing: normal;
        text-align: left;
        color: #747474;
        display: inline-block;
        width: 450px;
    }

    .all-price .num-all-price {
        font-family: "GothamRounded-Bold";
        font-size: 27px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.37;
        letter-spacing: normal;
        text-align: left;
        color: #2384c6;
        display: inline-block;
    }

    .disclaimer {
        padding-right: 10px;
        padding-left: 10px;
    }

    .disclaimer h2 {
        font-family: "GothamRounded-Bold";
        font-size: 20px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.4;
        letter-spacing: normal;
        text-align: left;
        color: #2384c6;
    }

    .disclaimer ul {
        padding: 0px;
        list-style: none;
        counter-reset: item;
    }

    .disclaimer ul li {
        counter-increment: item;
    }

    .disclaimer ul li::before {
        content: counter(item);
        text-align: center;
        display: inline-block;
        font-family: "GothamRounded-Book";
        font-size: 15px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        color: #9a9a9a;
        margin-right: 10px;
    }

    .disclaimer ul li span {
        font-family: "GothamRounded-Book";
        font-size: 15px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.13;
        letter-spacing: normal;
        text-align: left;
        color: #9a9a9a;
    }

    html {
        margin: 0px;
        padding: 0px;
    }

    body {
        background: url("<?php echo $baseUrl ?>/assets/images/resilient-calculator/bg.png") center center;
        z-index: 99;
        background-size: 100% 100%;
        background-repeat: repeat-y;
        background-position-x: 0px;
        padding: 0px;
        margin: 0px;
    }

    table {
        padding: 0px 75px 10px 75px;
    }

    .bg-pdf {
        height: 100%;
        display: block;
    }

    .img-pdf-title {
        margin-top: 40px;
    }

    table {
        border: 1px solid transparent;
    }

    table tr {
        border: 1px solid transparent;
    }

    table tr>td {
        border: 1px solid transparent;

    }
</style>
<div class="bg-pdf">
    <table style="width: 100%;" class="">
        <!-- table header to be repeated on each PDF page -->
        <thead>

        </thead>
        <!-- table body -->
        <tbody>
            <tr>
                <th>
                    <img class="img-pdf-title" style="widthV HR: 91.6px; height: 31.1px;" src="<?php echo $baseUrl ?>/assets/images/resilient-calculator/nutri.png" alt="" />
                </th>
            </tr>
            <tr>
                <th>
                    <h2 class="title" style="
                font-family: 'GothamRounded-Book';
                font-size: 35px;
                font-weight: normal;
                font-stretch: normal;
                font-style: normal;
                line-height: 1.4;
                letter-spacing: normal;
                text-align: center;
                color: #2384c6;
                margin-bottom: 4px;
              ">
                        <?= $this->t('RPC_PDF_TITLE') ?>
                    </h2>
                </th>
            </tr>
            <tr>
                <th>
                    <img class="img-calculator" src="<?php echo $baseUrl ?>/assets/images/resilient-calculator/calculator.svg" alt="" />
                </th>
            </tr>
            <tr>
                <th>
                    <div class="text-colages">
                        <hr />
                        <span>
                            <?= $this->t('RPC_PDF_STUDY_COST_DETAIL') ?>
                        </span>
                        <hr />
                    </div>
                </th>
            </tr>
            <tr>
                <th>
                    <div class="text-title-mom">
                        <span>
                            <?= $this->t('RPC_PDF_GREETING', [
                                '%genderFriendlyName' => $user ?
                                    ucfirst($user->getGenderFriendlyName()) : null,
                                '%name' => $user ? $user->getFullname() : null
                            ]) ?>
                        </span>
                        <p>
                            <?= $this->t('RPC_PDF_SHORT_DESCRIPTION', [
                                '%genderFriendlyName' => $user ?
                                    ucfirst($user->getGenderFriendlyName()) : null,
                                '%name' => $user ? $user->getFullname() : null
                            ]) ?>
                        </p>
                    </div>
                </th>
            </tr>
            <tr class="section-item">
                <td>
                    <?php
                    $costs = $rpcCalculatorObj->getCosts();
                    $checkCount = count((array)$rpcCalculatorObj->getCosts()->getItems());
                    if ($costs) {
                        $grantTotal = 0;
                        /** @var \Pimcore\Model\DataObject\Fieldcollection\Data\Costs $cost */
                        foreach ($costs as $key => $cost) {
                            $totalStudyYear = $costs && $cost->getTotalStudyYear() ? (int)$cost->getTotalStudyYear() : 0;
                            $totalStudyInMonth = $totalStudyYear * 12;

                            $mainCost = $cost->getMainCost();
                            $admissionFee = $mainCost && $mainCost->getAdmissionFee() ? $mainCost->getAdmissionFee() : 0;
                            $monthlyFee = $mainCost && $mainCost->getMonthlyFee() ? $mainCost->getMonthlyFee() * $totalStudyInMonth : 0;
                            $yearlyFee = $mainCost && $mainCost->getYearlyFee() ? $mainCost->getYearlyFee() * $totalStudyYear : 0;
                            $totalMainCost = $admissionFee + $monthlyFee + $yearlyFee;

                            $grantTotal += $totalMainCost;

                            $additionalCost = $cost->getAdditionalCost();
                            $totalAdditionalCost = 0;
                            $pageBreak = ($key != $checkCount - 1) ? "page-break-after: always;" : "page-break-after: void;";
                            echo " &nbsp;";
                            echo "&nbsp; ";
                            echo " &nbsp;";
                    ?>
                            <?php if ($checkCount == 1) : ?>
                                <table class="item-list" style="padding-top: 50px;">
                                <?php else : ?>
                                    <table class="item-list" style="padding-top: 50px;<?= $pageBreak ?>">
                                    <?php endif ?>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span class="title-list">
                                                    <?= $costs && $cost->getLevel() ? $cost->getLevel()->getName() : null ?>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="subtitle-list">
                                                    <?= $costs && $cost->getGrade() ? $cost->getGrade()->getName() . ' | ' : null ?>
                                                    <?= $costs && $cost->getLocation() ? $cost->getLocation()->getName() . ' | ' : null ?>
                                                    <?= $this->t('RPC_CALCULATOR_TOTAL_YEAR', [
                                                        '%year' => $totalStudyYear
                                                    ]) ?>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="text-list">
                                                    <ul style="padding: 0px; list-style: none;">
                                                        <li>
                                                            <span class="text-detail">
                                                                <?= $this->t('RPC_ADMISSION_FEE') ?>
                                                            </span>
                                                            <span class="text-price">
                                                                <?= $this->t('RPC_ADMISSION_FEE_RP', [
                                                                    '%value' => number_format($admissionFee, 0, 0, '.')
                                                                ]) ?>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="text-detail">
                                                                <?= $this->t('RPC_MONTHLY_FEE', [
                                                                    '%month' => $totalStudyInMonth
                                                                ]) ?>
                                                            </span>
                                                            <span class="text-price">
                                                                <?= $this->t('RPC_MONTHLY_FEE_RP', [
                                                                    '%value' => number_format($monthlyFee, 0, 0, '.')
                                                                ]) ?>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="text-detail">
                                                                <?= $this->t('RPC_YEARLY_FEE', [
                                                                    '%year' => $totalStudyYear
                                                                ]) ?>
                                                            </span>
                                                            <span class="text-price">
                                                                <?= $this->t('RPC_YEARLY_FEE_RP', [
                                                                    '%value' => number_format($yearlyFee, 0, 0, '.')
                                                                ]) ?>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>

                                        </tr>
                                        <?php if ($additionalCost) { ?>
                                            <tr>
                                                <td>
                                                    <div class="text-list">
                                                        <ul style="padding: 0px; list-style: none;">
                                                            <li>
                                                                <span class="text-detail">
                                                                    <?= $this->t('BOOK') ?>
                                                                </span>
                                                                <span class="text-price">
                                                                    <?= $this->t('RPC_PDF_RP', [
                                                                        '%value' => number_format($additionalCost->getBook() ?? 0, 0, 0, '.')
                                                                    ]) ?>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="text-detail">
                                                                    <?= $this->t('UNIFORM') ?>
                                                                </span>
                                                                <span class="text-price">
                                                                    <?= $this->t('RPC_PDF_RP', [
                                                                        '%value' => number_format($additionalCost->getUniform() ?? 0, 0, 0, '.')
                                                                    ]) ?>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="text-detail">
                                                                    <?= $this->t('ACTIVITIES') ?>
                                                                </span>
                                                                <span class="text-price">
                                                                    <?= $this->t('RPC_PDF_RP', [
                                                                        '%value' => number_format($additionalCost->getActivities() ?? 0, 0, 0, '.')
                                                                    ]) ?>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="text-detail">
                                                                    <?= $this->t('MONTHLY_CATERING') ?>
                                                                </span>
                                                                <span class="text-price">
                                                                    <?= $this->t('RPC_PDF_RP', [
                                                                        '%value' => number_format($additionalCost->getMonthlyCatering() * $totalStudyInMonth ?? 0), 0, 0, '.'
                                                                    ]) ?>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="text-detail">
                                                                    <?= $this->t('MONTHLY_SHUTTLE') ?>
                                                                </span>
                                                                <span class="text-price">
                                                                    <?= $this->t('RPC_PDF_RP', [
                                                                        '%value' => number_format($additionalCost->getMonthlyShuttle() * $totalStudyInMonth ?? 0, 0, 0, '.')
                                                                    ]) ?>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="text-detail">
                                                                    <?= $this->t('OTHERS') ?>
                                                                </span>
                                                                <span class="text-price">
                                                                    <?= $this->t('RPC_PDF_RP', [
                                                                        '%value' => number_format($additionalCost->getOthers() ?? 0, 0, 0, '.')
                                                                    ]) ?>
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                            $totalAdditionalCost += $additionalCost->getBook();
                                            $totalAdditionalCost += $additionalCost->getUniform();
                                            $totalAdditionalCost += $additionalCost->getActivities();
                                            $totalAdditionalCost += $additionalCost->getMonthlyCatering() * $totalStudyInMonth;
                                            $totalAdditionalCost += $additionalCost->getMonthlyShuttle() * $totalStudyInMonth;
                                            $totalAdditionalCost += $additionalCost->getOthers();
                                            ?>
                                        <?php } ?>
                                        <tr>
                                            <td>
                                                <div class="total-price">
                                                    <span class="text-total">
                                                        <?= $this->t('RPC_TOTAL_MAIN_COST') ?>
                                                    </span>
                                                    <span class="text-total-price">
                                                        <?= $this->t('RPC_TOTAL_MAIN_COST_RP', [
                                                            '%value' => number_format($totalMainCost + $totalAdditionalCost, 0, 0, '.')
                                                        ]) ?>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    </table>
                            <?php
                            $grantTotal += $totalAdditionalCost;
                            $totalAdditionalCost = 0;
                        }
                    }
                            ?>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="all-price">
                        <span class="text-all-price">
                            <?= $this->t('RPC_PDF_GRANT_TOTAL') ?>
                        </span>
                        <span class="num-all-price">
                            <?= $this->t('RPC_PDF_GRANT_TOTAL_RP', [
                                '%value' => number_format($grantTotal, 0, 0, '.')
                            ]) ?>
                        </span>
                    </div>
                </td>
            </tr>

        </tbody>
    </table>

    <table style="height: 100%; page-break-before: always; ">
        <tr>
            <td style="">
                <div class="disclaimer" style="height: 100%; padding-top: 200px; display: block;">
                    <h2>
                        Disclaimer
                    </h2>
                    <ul>
                        <li>
                            <span>
                                Acuan dana pendidikan adalah Tahun Akademik 2019/2020.
                            </span>
                        </li>
                        <li>
                            <span>
                                Sampling dilakukan pada 5-10 sekolah di setiap kota.
                            </span>
                        </li>
                        <li>
                            <span>
                                Metode pembyaran SPP Quarter/Semester/Tahunan disebulankan
                                dan pada sekolah bertaraf International pembayaran SPP
                                Tahunan tidak termasuk Registration Fee/Admission Fee/Yearly
                                Fee.
                            </span>
                        </li>
                        <li>
                            <span>
                                Biaya kuliah Luar Negeri tidak termasuk biasa pendukung
                                (Visa Student, Biaya Persiapan Sekolah, Pajak International
                                Student, dsb).
                            </span>
                        </li>
                        <li>
                            <span>
                                Acuan dana sosial berdasarkan research 4-5 sekolah/kurikulum
                                daerah JABODETABEK
                            </span>
                        </li>
                        <li>
                            <span>
                                Biaya buku/biaya seragam/biaya kegiatan sudah diintegrasikan
                                ke dalam dana pendidikan berdasarkan kebijakan sekolah atau
                                bisa ditentukan setelah murid sudah efektif diterima.
                            </span>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>
</div>
