<?php

/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource img.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 14/09/20
 * @time 12.06
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

/** @var \AppBundle\Model\DataObject\Customer $user */
$user = $this->user;

$rpcCalculatorObj = $this->rpcCalculatorObj;

$baseUrl = env('RPC_BASE_URL_ASSET', 'https://localhost');
?>
<html>
<head>
    <meta property="og:title" content="Resilient Parental Calculator Result">
    <meta property="og:description" content="Resilient Parental Calculator Result">
    <meta property="og:image" content="<?= $baseUrl ?>/assets/resilient-parental-calculator/<?= $rpcCalculatorObj->getTransactionId() ?>.png">
    <meta property="og:url" content="<?= $baseUrl . $this->path('disclaimer-share-generator', [
        'id' => $rpcCalculatorObj->getTransactionId()
    ]) ?>">
    <meta property="og:image:width" content="600">
    <meta property="og:image:height" content="600">

    <meta name="twitter:image" content="<?= $baseUrl ?>/resilient-parental-calculator/<?= $rpcCalculatorObj->getTransactionId() ?>.png">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@nutriclub">
    <meta name="twitter:title" content="Resilient Parental Calculator Result">
    <meta name="twitter:description" content="Resilient Parental Calculator Result">

    <meta name="title" content="Resilient Parental Calculator Result">
    <meta name="description" content="Resilient Parental Calculator Result">

    <link rel="stylesheet" href="<?= $baseUrl ?>/assets/css/vendor/bootstrap.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>/assets/css/pages/resilent-parental-calculator/wizard.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>/assets/css/pages/resilent-parental-calculator/share-image.css">
    <title>Resilient Parental Calculator Result</title>
</head>
<body>
<div class="wizard-step" style="padding-top: 0 !important;">
    <div class="form-card wizard-step__result container">
        <div class="wizard-step__result-body">
            <h2>
                <?= $this->t('RPC_RESULT_IMG_DETAIL_COST_EACH_STEP', [
                    '%genderFriendlyName' => $user ?
                        ucfirst($user->getGenderFriendlyName()) : null,
                    '%name' => $user ? $user->getFullname() : null
                ]) ?>
            </h2>
            <p class="text-body">
                <?= $this->t('RPC_RESULT_IMG_COST_EACH_STEP', [
                    '%genderFriendlyName' => $user ?
                        ucfirst($user->getGenderFriendlyName()) : null,
                    '%name' => $user ? $user->getFullname() : null
                ]) ?>
            </p>
            <div class="list-colage">
                <div class="list-colage__content">
                        <?php
                        $costs = $rpcCalculatorObj->getCosts();
                        $countCosts = 0;
                        if ($costs) {
                            $countCosts = count($costs->getItems());
                            $countInit = 0;
                            $grantTotal = 0;
                            /** @var \Pimcore\Model\DataObject\Fieldcollection\Data\Costs $cost */
                            foreach ($costs as $cost) {
                                $countInit++;
                                $totalStudyYear = $costs && $cost->getTotalStudyYear() ? (int)$cost->getTotalStudyYear() : 0;
                                $totalStudyInMonth = $totalStudyYear * 12;

                                $mainCost = $cost->getMainCost();
                                $admissionFee = $mainCost && $mainCost->getAdmissionFee() ? $mainCost->getAdmissionFee() : 0;
                                $monthlyFee = $mainCost && $mainCost->getMonthlyFee() ? $mainCost->getMonthlyFee() * $totalStudyInMonth : 0;
                                $yearlyFee = $mainCost && $mainCost->getYearlyFee() ? $mainCost->getYearlyFee() * $totalStudyYear : 0;
                                $totalMainCost = $admissionFee + $monthlyFee + $yearlyFee;

                                $grantTotal += $totalMainCost;

                                $additionalCost = $cost->getAdditionalCost();

                                $levelObj = $cost->getLevel();
                                ?>

                                <div class="list-colage__content-item">
                                    <?php if ($countCosts == 3) : ?>
                                    <div class="list-colage__item oncheck-item">
                                    <?php else : ?>
                                    <div class="list-colage__item">
                                    <?php endif ?>
                                        <div class="list-colage__item-title">
                                            <div class="bg"></div>
                                            <img src="<?= $levelObj && $levelObj->getIcon() ? $baseUrl . $levelObj->getIcon()->getFullPath() : '' ?>" alt="">
                                        </div>
                                        <div class="list-colage__item-content">
                                            <h3><?= $costs && $cost->getLevel() ? $cost->getLevel()->getName() : null ?></h3>
                                            <ul>
                                                <li>
                                                    <?= $costs && $cost->getLocation() ? $cost->getLocation()->getName() : null ?>
                                                </li>
                                                <li>
                                                    |
                                                </li>
                                                <li>
                                                    <?= $costs && $cost->getGrade() ? $cost->getGrade()->getName() : null ?>
                                                </li>
                                                <li>|</li>
                                                <li>
                                                    <?= $this->t('RPC_CALCULATOR_TOTAL_YEAR', [
                                                        '%year' => $totalStudyYear
                                                    ]) ?>
                                                </li>
                                            </ul>
                                            <div class="price-content">
                                                <p class="text">Total Dana Pendidikan</p>
                                                <span>
                                        <?= $this->t('RPC_TOTAL_MAIN_COST_RP', [
                                            '%value' => number_format($totalMainCost, 0, 0, '.')
                                        ]) ?>
                                    </span>
                                                <?php if ($additionalCost) { ?>
                                                    <?php
                                                    $totalAdditionalCost = 0;
                                                    $totalAdditionalCost += $additionalCost->getBook();
                                                    $totalAdditionalCost += $additionalCost->getUniform();
                                                    $totalAdditionalCost += $additionalCost->getActivities();
                                                    $totalAdditionalCost += $additionalCost->getMonthlyCatering();
                                                    $totalAdditionalCost += $additionalCost->getMonthlyShuttle();
                                                    $totalAdditionalCost += $additionalCost->getOthers();
                                                    ?>
                                                    <p class="text">Total Dana Sosial</p>
                                                    <span>
                                            <?= $this->t('RPC_ADDITIONAL_MAIN_COST_RP', [
                                                '%value' => number_format($totalAdditionalCost, 0, 0, '.')
                                            ]) ?>
                                        </span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if ($countCosts !== $countInit) { ?>
                                <div class="break"></div>
                                <?php } ?>

                                <?php
                            }
                        }
                        ?>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
