<?php

/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource index.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 18/08/20
 * @time 13.28
 *
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');
$this->headLink()->offsetSetStylesheet(4, '/assets/css/pages/resilent-parental-calculator/homepage.css', 'screen', false);

$formDisclaimer = $this->formDisclaimer;
?>
<?php $this->headScript()->offsetSetFile(11, '/assets/js/pages/wizard.js', 'text/javascript'); ?>

<div class="resilient-calculator">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <?= $this->image('resilient-index-image-desktop', [
                    'class' => 'img-resilient'
                ]) ?>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="resilient-calculator__content">
                    <div class="img">
                        <h1>Resilient Parental</h1>
                        <h2>Calculator</h2>
                    </div>
                    <div class="desc">
                        <?= $this->wysiwyg('description', []) ?>
                        <span>
                            <?= $this->input('sub-title') ?>
                        </span>
                        <?php
                        if ($app->getUser() || $this->editmode) {
                        ?>
                            <?= $this->link('button-after-login', []) ?>
                        <?php
                        }
                        ?>
                        <?php
                        if (!$app->getUser() || $this->editmode) {
                        ?>
                            <?= $this->link('button-before-login', []) ?>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!$this->editmode) { ?>
<div class="modal modal-disclaimer-rpc" id="modal-disclaimer" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
<?php } ?>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="<?= $this->editmode ? 'width: 100%;' : null ?>">
                    <?= $this->input('disclaimer-title') ?>
                </h5>
            </div>
            <div class="modal-body">
                <?= $this->wysiwyg('disclaimer-description', []) ?>
            </div>
            <div class="modal-footer">
                <?= $this->form()->start($formDisclaimer, [
                    'attr' => [
                        'class' => null
                    ]
                ])
                ?>
                    <?= $this->form()->widget($formDisclaimer['_csrf_token'], []) ?>
                    <?= $this->link('disclaimer-button', [
                        'class' => 'btn btn-primary btn-disc'
                    ]) ?>
                    <?= $this->form()->widget($formDisclaimer['_submit'], [
                        'label' => $this->t('RPC_CALCULATE_NOW'),
                    ]) ?>
                <?= $this->form()->end($formDisclaimer, ['render_rest' => false]) ?>
            </div>
        </div>
<?php if (!$this->editmode) { ?>
    </div>
</div>
<?php } ?>
