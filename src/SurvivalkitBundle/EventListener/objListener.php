<?php
/*
 * Filename: /var/www/html/survivalkit/src/SurvivalkitBundle/EventListener/objListener.php
 * Path: /var/www/html/survivalkit/src/SurvivalkitBundle/EventListener
 * Created Date: Thursday, August 13th 2020, 11:12:04 am
 * Author: saldri
 * 
 * Copyright (c) 2020 Your Company
 */

namespace SurvivalkitBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Symfony\Component\HttpFoundation\Response;

class objListener
{

    /* thansk to : https://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string */
    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public function onPreUpdate(ElementEventInterface $e)
    {
        if ($e instanceof \Pimcore\Event\Model\DataObjectEvent) {

            $obj = $e->getObject();
            /* article */
            if ($obj instanceof \Pimcore\Model\DataObject\SurvivalkitArticles) {
                $obj->setKey($this->slugify($obj->getTitle()));
                $obj->setSlug((!empty($obj->getSlug()) ? $this->slugify($obj->getSlug()) : $this->slugify($obj->getTitle())));
            }

            if ($obj instanceof \Pimcore\Model\DataObject\SurvivalkitCategoryArticles) {
                $obj->setKey($this->slugify($obj->getTitle()));

                if(empty($obj->getSlug())){
                    $obj->setSlug($this->slugify($obj->getTitle()));
                }else{
                    $obj->setSlug($this->slugify($obj->getSlug()));
                }
            }

            if ($obj instanceof \Pimcore\Model\DataObject\SurvivalkitJurnalOnline) {
                $obj->setKey($this->slugify($obj->getTitle()));
                $obj->setSlug((!empty($obj->getSlug()) ? $this->slugify($obj->getSlug()) : $this->slugify($obj->getTitle())));
            }

            if ($obj instanceof \Pimcore\Model\DataObject\SurvivalkitWebminar) {
                $obj->setKey($this->slugify($obj->getTitle()));
                $obj->setSlug((!empty($obj->getSlug()) ? $this->slugify($obj->getSlug()) : $this->slugify($obj->getTitle())));
            }
        }
    }
}
