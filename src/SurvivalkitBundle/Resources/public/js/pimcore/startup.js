pimcore.registerNS("pimcore.plugin.SurvivalkitBundle");

pimcore.plugin.SurvivalkitBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.SurvivalkitBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("SurvivalkitBundle ready!");
    }
});

var SurvivalkitBundlePlugin = new pimcore.plugin.SurvivalkitBundle();
