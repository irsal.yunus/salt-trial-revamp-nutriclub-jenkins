<?php
/**
* @var \Pimcore\Templating\PhpEngine $this
* @var \Pimcore\Templating\PhpEngine $view
* @var \Pimcore\Templating\GlobalVariables $app
*/

use Pimcore\Model\Document;

if (!empty($document)) $this->document = $document;
$document = $this->document;

$mainNavStartNode = $this->document->getProperty('navigationRoot');

if (!$mainNavStartNode instanceof \Pimcore\Model\Document\Page) {
    $mainNavStartNode = \Pimcore\Model\Document\Page::getById(1);
}

$mainNavigation = $this->navigation()->buildNavigation($document, $mainNavStartNode);

$home = Document::getById(1);

$menuRenderer = $this->navigation()->menu();
$token_session = $this->get('session')->get('token');
?>

<!DOCTYPE html>
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/assets/assets/images/favicon.png">

    <!-- Css -->
    <link rel="stylesheet" href="/survivalkit/assets/css/vendor/slick.css">  
    <link rel="stylesheet" href="/survivalkit/assets/css/vendor/slick-theme.css">  
    <link rel="stylesheet" href="/survivalkit/assets/css/vendor/font-awesome.min.css">  
    <link rel="stylesheet" href="/survivalkit/assets/css/vendor/ionicons.min.css">  
    <link rel="stylesheet" href="/survivalkit/assets/css/main.css">

    <?php
    $this->headTitle()->append($this->document->getType() != 'email' ? $this->document->getTitle() : '');
    $this->headTitle()->setSeparator(" : ");

    echo $this->headTitle();

    $metaData = $this->document->getType() != 'email' ? $this->document->getMetaData() : [];

    foreach ($metaData as $k) {
        echo $k . "\r\n";
    }

    echo $this->headMeta();

    // $this->headLink()->offsetSetStylesheet(1, '/assets/assets/css/site.css');
    echo $this->headLink(); 
    ?>

</head>
<style type="text/css">
    .x-btn.x-btn-default-toolbar-small, .x-btn.x-btn-default-small {
        padding: 3px;
        z-index: 99999999;
    }
</style>
<body>

<?= $menuRenderer->renderPartial($mainNavigation, '@SurvivalkitBundle/Resources/views/Includes/nav.html.php'); ?>

<?php $this->slots()->output('_content') ?>

<!-- footer -->
<?= $this->snippet("survivalkit-footer", ["height" => 100]); ?>
<!-- footer -->

<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
<!-- <div class="modal-header">
<h4 class="modal-title">Modal Heading</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div> -->

<!-- Modal body -->
<div class="modal-body">
    <div class="h5 text-center pb-2 title">Apakah Anda Petugas Kesehatan Profesional?</div>
    <p class="text-center pb-2">Beberapa konten yang ada disini membutuhkan pengetahuan dari petugas kesehatan profesional. 
        Jika Anda bukan petugas kesehatan profesional, kami 
        menyarankan untuk melihat konten lainnya yang 
    lebih cocok untuk Anda</p>
    <div class="pl-5 pr-5 pb-3">
        <button  type="button" class="btn --blue w-100 no-border-radius clickPetugas">Ya, Saya seorang Petugas Kesehatan Profesional</button>
    </div>
    <div class="pl-5 pr-5 pb-3">
        <button type="button" class="btn --blue w-100 no-border-radius" data-dismiss="modal">Tidak. Saya bukan Petugas Kesehatan Profesional</button>
    </div>
</div>

<!-- Modal footer -->
<!-- <div class="modal-footer">
<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div> -->

</div>
</div>
</div>

<script src="/survivalkit/assets/js/vendor/jquery.min.js" ></script>
<script src="/survivalkit/assets/js/vendor/bootstrap.bundle.min.js" ></script>
<script src="/survivalkit/assets/js/vendor/slick.min.js" ></script>
<script src="/survivalkit/assets/js/app.min.js" ></script>
<script src="/survivalkit/assets/js/includes/homepage.js" ></script>

<?= $this->headScript();?>
<script>
$(document).ready(function(){
  
 var offset        = $('#offset').val();
 var limit         = $('#loadmoreJurnal').data('limit');

 $("#loadmoreJurnal").click(function(){
     
     $("#loadmoreJurnal").html("LOADING...");
     $.ajax({  
         type: "POST",
         url: "/survivalkit/jurnal/show",
         dataType: "html",
         data: {
             offset:offset,
             limit:limit,
         },         
         success: function(data){         
             console.log(data);    
             if(data.length > 1){

                offset = parseInt(limit) + parseInt(offset);
                $('#offset').val(offset);

                $("#loadmoreJurnal").html("Lihat Lebih Banyak");                 
                $('#jurnal_load_more').append(data);
             }else{
                $("#loadmoreJurnal").html("NO DATA");
             }                
         }
     });
 });
});
</script>
</body>
</html>
