<?= $this->extend('@SurvivalkitBundle/Resources/views/Layout/layout.html.php'); ?>

<?php
//CSS
// $this->headLink()->offsetSetStylesheet(4, '/assets/assets/css/pages/detail-page.css');
//JS
// $this->headScript()->offsetSetFile(4, '/assets/assets/js/Widgets/banner-min.js');
?>

<?php

use Pimcore\Model\WebsiteSetting;

$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$url_segments = explode('/', $uri_path);
?>

<?php
/** @var Pimcore\Model\DataObject\Articles\Listing $article_detail */
if ($article_detail) {
    foreach ($article_detail as $detail) {

        $viewer = $detail->getTotalView();
        // increase viewer
        $detail->setTotalView((int)$viewer + 1);
        $detail->save();

        $host = \Pimcore\Tool::getHostUrl();

        $meta_url = $host . $uri_path;

        $this->headTitle()->append($detail->getTitle() ? $detail->getTitle() : "");
        $this->headMeta()->appendName('title', '');
        $this->headMeta()->appendName('description', '');
        $this->headMeta()->appendName('keyword', '');
        $this->headMeta()->appendName('author', '');

        $this->headMeta()->setProperty('og:type', 'article');
        $this->headMeta()->setProperty('og:url', '');
        $this->headMeta()->setProperty('og:title', '');

        // $content = $detail->getContent();
        // foreach ($content as $k => $v) {
        //     $this->headMeta()->setProperty('og:description', $v['bodyText']->getData() ? $TitleHelper->trim_text($v['bodyText']->getData(), 300) : "");
        // };

        $this->headMeta()->setProperty('og:image', '');
        $this->headMeta()->setProperty('twitter:card', 'summary');
        ?>
          <main class="detail-artikel">
            <div class="container-medium">
                <div class="hero__detail my-4">
                    <img src="<?= $detail->getImageDesktop();?>" alt="">
                </div>
                <div class="font--14 color--ungulight">
                    <?=  $detail->getCategory()->getTitle();?>
                </div>
                <div class="my-3 color--violet">
                    <h1><?= $detail->getTitle();?></h1>
                </div>
                <div class="my-3 font--14">
                    <img class="cursor--pointer" src="/assets/img/wa.png" alt="" width="30" height="30">
                    <img class="cursor--pointer" src="/assets/img/fb.png" alt="" width="30" height="30">
                    <img class="cursor--pointer" src="/assets/img/twit.png" alt="" width="30" height="30">
                    <img class="cursor--pointer" src="/assets/img/doc.png" alt="" width="30" height="30">
                </div>
                <?= $detail->getBody();?>
                <hr>
                <h2 class="text-center color--violet">Artikel Terkait</h2>
                <div class="row">
                    <?php foreach($article_terkait as $terkait){?>
                    <div class="col-sm-12 col-md-4 mb-2">
                        <a href="<?= $this->path('SURVIVALKIT_ARTICLE_DETAIL', ['category' => $terkait->getCategory()->getSlug(),'slug' => $terkait->getSlug()]); ?>" class="btn-card">
                        <div class="cardy">
                          <div class="row">
                            <div class="col col-sm-6 col-md-12">
                              <img src="<?= $terkait->getImageDesktop();?>" alt="" />
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <label for="category" class="cardy--category"><?= $terkait->getCategory()->getTitle();?></label>
                                <h3 class="cardy--title"><?= $terkait->getTitle(); ?></h3>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <?php }?>
                </div>
            </div>
          </main>
    <?php } ?>
<?php } ?>
<!-- END TAG -->
