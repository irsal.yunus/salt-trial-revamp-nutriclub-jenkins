<?= $this->extend('@SurvivalkitBundle/Resources/views/Layout/layout.html.php'); ?>

<?php
//CSS
// $this->headLink()->offsetSetStylesheet(4, '/assets/assets/css/pages/detail-page.css');
//JS
$this->headScript()->offsetSetFile(1, '/assets/js/includes/navpills.js');
$this->headScript()->offsetSetFile(2, '/assets/js/includes/slide.js');
?>

  <main class="artikel-list">
      <div class="position-relative height--155">
        <div class="hero__artikel">
              <img class="position-absolute" src="assets/img/artikel.png" alt="">
              <h1 class="position-absolute hero__artikel--title">Informasi mengenai perkembangan ilmu pengetahuan terkini</h1>
        </div>
      </div>
      <div class="container-medium">
        <ul class="navpills my-3 slick-artikel" id="artikel-list">
            <a href="/survivalkit/artikel"><li class="navpills--list <?= ($catSlug == 'all') ? 'active' : ''; ?>" id="semua">Semua</li></a>
            <?php foreach($category as $cat){?>
                <a href="/survivalkit/artikel/<?= $cat->getSlug();?>"><li class="navpills--list <?= ($catSlug == $cat->getSlug()) ? 'active' : ''; ?>" id="<?= $cat->getSlug();?>"><?= $cat->getTitle();?></li></a>
            <?php }?>
        </ul>

        <div class="all-content my-5" id="show-semua">
            <div class="row">
                <?php foreach($paginator as $article){?>
                <div class="col-sm-12 col-md-4 mb-2">
                    <a href="<?= $this->path('SURVIVALKIT_ARTICLE_DETAIL', ['category' => $article->getCategory()->getSlug(),'slug' => $article->getSlug()]); ?>" class="btn-card">
                    <div class="cardy">
                      <div class="row">
                        <div class="col col-sm-6 col-md-12">
                          <img src="<?= $article->getImageDesktop();?>" alt="" />
                        </div>
                        <div class="col col-sm-6 col-md-12">
                            <label for="category" class="cardy--category"><?= $article->getCategory()->getTitle();?></label>
                            <h3 class="cardy--title"><?= $article->getTitle()?></h3>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
                <?php }?>
            </div>
            <!-- Pagination -->
            <?= $this->render("@SurvivalkitBundle/Resources/views/Includes/pagination.html.php",
                    get_object_vars($this->paginator->getPages("Sliding")),
                    [
                        'urlprefix' => $this->document->getFullPath() . '?page=',
                        'appendQueryString' => true
                    ]
                );
            ?>
            <!-- End Pagination -->
        </div>
        <div class="all-content" id="show-keluarga" hidden>
            <h1>keluarga</h1>
        </div>
        <div class="all-content" id="show-nutrisi" hidden>
            <h1>nutrisi</h1>
        </div>
        <div class="all-content" id="show-psikologis" hidden>
            <h1>psikologis</h1>
        </div>
        <div class="all-content" id="show-penyakit" hidden>
            <h1>penyakit</h1>
        </div>
        <div class="all-content" id="show-makanan" hidden>
            <h1>makanan</h1>
        </div>
        <div class="all-content" id="show-olahraga" hidden>
            <h1>olahraga</h1>
        </div>
        <div class="all-content" id="show-lili" hidden>
            <h1>konten 1</h1>
        </div>
        <div class="all-content" id="show-lele" hidden>
            <h1>konten lainnya</h1>
        </div>
        <div class="all-content" id="show-lala" hidden>
            <h1>konten terakhir</h1>
        </div>
      </div>
  </main>
