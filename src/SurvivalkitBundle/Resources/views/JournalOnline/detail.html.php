<?= $this->extend('@SurvivalkitBundle/Resources/views/Layout/layout.html.php'); ?>

<?php
//CSS
// $this->headLink()->offsetSetStylesheet(4, '/assets/assets/css/pages/detail-page.css');
//JS
// $this->headScript()->offsetSetFile(4, '/assets/assets/js/Widgets/banner-min.js');
?>

<?php

use Pimcore\Model\WebsiteSetting;

$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$url_segments = explode('/', $uri_path);
?>

<?php
/** @var Pimcore\Model\DataObject\Articles\Listing $article_detail */
if ($jurnal_detail) {
    foreach ($jurnal_detail as $detail) {
        
        $viewer = $detail->getTotalView();
        // increase viewer
        $detail->setTotalView((int)$viewer + 1);
        $detail->save();

        $host = \Pimcore\Tool::getHostUrl();

        $meta_url = $host . $uri_path;

        $this->headTitle()->append($detail->getTitle() ? $detail->getTitle() : "");
        $this->headMeta()->appendName('title', '');
        $this->headMeta()->appendName('description', '');
        $this->headMeta()->appendName('keyword', '');
        $this->headMeta()->appendName('author', '');

        $this->headMeta()->setProperty('og:type', 'article');
        $this->headMeta()->setProperty('og:url', '');
        $this->headMeta()->setProperty('og:title', '');

        // $content = $detail->getContent();
        // foreach ($content as $k => $v) {
        //     $this->headMeta()->setProperty('og:description', $v['bodyText']->getData() ? $TitleHelper->trim_text($v['bodyText']->getData(), 300) : "");
        // };

        $this->headMeta()->setProperty('og:image', '');
        $this->headMeta()->setProperty('twitter:card', 'summary');
        ?>
          <main class="push-right">

            <!-- JURNAL-->
            <section class="section bg-half-grey ">
              <div class="container-small">
                <div class="row">
                  <div class="col-12 col-md-5 isDesktop">
                    <img src="<?= $detail->getImageDesktop();?>" alt="">
                  </div>
                  <div class="col-12 col-md-7">
                    <div class="heading --left --jurnal mb-20">
                      <h2 class="heading__title"><?= $detail->getTitle();?></h2>
                    </div>
                    
                    <div class="author mb-20">
                      <img class="author__avatar" src="<?= $detail->getAvatar();?>">
                      <div class="author__content">
                        <p class="author__name"><?= $detail->getName();?></p>
                        <p class="author__title"><?= $detail->getPosition();?></p>
                      </div>
                    </div>

                    <div class="isMobile mb-20">
                      <img src="./assets/img/buku.jpg" alt="">
                    </div>

                    <p class="mb-20"><?= $detail->getContent();?></p>
                    
                    <div class="mb-20">
                      <a href="<?= $detail->getLinkJurnal()->getHref(); ?>" class="btn --large --blue">Baca Selengkapnya</a>
                      <a href="/survivalkit/jurnal-online" class="btn --large --link">Lihat Jurnal Lain</a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <!-- END JURNAL -->
            </main>
    <?php } ?>
<?php } ?>
<!-- END TAG -->
