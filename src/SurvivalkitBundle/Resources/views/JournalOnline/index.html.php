<?= $this->extend('@SurvivalkitBundle/Resources/views/Layout/layout.html.php'); ?>

<?php
//CSS
// $this->headLink()->offsetSetStylesheet(4, '/assets/assets/css/pages/detail-page.css');
//JS
$this->headScript()->offsetSetFile(1, '/assets/js/includes/navpills.js');
$this->headScript()->offsetSetFile(2, '/assets/js/includes/slide.js');
?>

<main class="push-right">

<!-- JURNAL-->
<section class="section --start">
  <div class="heading --background">
    <h2 class="heading__title">Jurnal Online</h2>
      <div class="container-small">
    <p class="heading__subtitle">
      <?= $this->input('sub-title') ?>
    </p>
  </div>
  </div>

  <div class="container-small">
    <div class="row" id="jurnal_load_more">
        <?php foreach($jurnal as $j){?>
            <div class="col-6 col-md-3">
                <div class="card --jurnal">
                <img src="<?= $j->getImageDesktop();?>" class="card__img" alt="">
                <div class="card__content">
                    <h3 class="card__title"><?= $j->getTitle();?></h3>
                    <p class="card__author"><?= $j->getName();?></p>
                    <p class="card__meta"><?= $j->getPosition();?></p>
                    <a href="<?= $this->path('SURVIVALKIT_JOURNAL_DETAIL', ['slug' => $j->getSlug()]); ?>" class="card__link">Baca Selengkapnya</a>
                </div>
                </div>
            </div>
        <?php }?>
    </div>

    <div class="text-center mt-20">
      <input type="hidden" id="offset" value="4">
      <a href="#" class="btn --large --ghost-blue" id="loadmoreJurnal" data-limit="8">Lihat Lebih Banyak</a>
    </div>
  </div>
</section>
<!-- END JURNAL -->
</main>


