<?php foreach($jurnal as $j){?>
    <div class="col-6 col-md-3">
        <div class="card --jurnal">
        <img src="<?= $j->getImageDesktop();?>" class="card__img" alt="">
        <div class="card__content">
            <h3 class="card__title"><?= $j->getTitle();?></h3>
            <p class="card__meta"><?= $j->getPosition();?></p>
            <a href="<?= $this->path('SURVIVALKIT_JOURNAL_DETAIL', ['slug' => $j->getSlug()]); ?>" class="card__link">Baca Selengkapnya</a>
        </div>
        </div>
    </div>
<?php }?>
