<?= $this->extend('@SurvivalkitBundle/Resources/views/Layout/layout.html.php'); ?>
<?php
//perpage
$this->headLink()->offsetSetStylesheet(4,'/assets/assets/css/pages/sitemap.css');
?>
<main class="hubungi-kami">
    <div class="hero__hubungi">
      <div class="container-medium">
        <h1 class="hero__title hero__hubungi--white py-5">Hubungi Kami</h1>
        <label class="hero__hubungi--white">Layanan Bagı Tenaga Kesehatan</label>
        <div class="my-3">
          <img src="assets/img/mail.png" alt="" srcset="">
          <label class="hero__hubungi--white ml-1">Email</label> <br>
          <a class="hero__hubungi--white normal--white" href="mailto:nutricia.sarihusada-hn@danone.com">nutricia.sarihusada-hn@danone.com</a>
        </div>

        <div class="my-4">
          <img src="assets/img/maps.png" alt="" srcset="">
          <label class="hero__hubungi--white ml-1">Alamat Perusahaan</label> <br>
            <div class="normal--white mt-1">Cyber 2 Tower, Jl. H.R. Rasuna Said</div>
            <div class="normal--white mt-1">Blok X-5 No. 13, Jakarta, Indonesia</div>
        </div>
      </div>
    </div>
    <div class="height--400 position-relative bg--greyinput">
      <div class="container-medium form-hubungi position-absolute py-3 px-4">
        <h2 class="form-hubungi__title">Informasi Pengirim</h2>
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <div class="my-2">
              <label for="name">Nama</label> <br>
              <input type="text" id="name" class="form-control">
            </div>
            <div class="my-3">
              <label for="alamat">Alamat</label> <br>
              <textarea type="text" id="alamat" class="form-control" rows="4"> </textarea>
            </div>
            <div class="my-3">
              <label for="email">Email</label> <br>
              <input type="text" id="email" class="form-control">
            </div>
            <div class="my-3">
              <label for="telp">No Telepon</label> <br>
              <input type="text" id="telp" class="form-control">
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div class="my-2">
              <label for="no">No Anggota Profesi (Opsional)</label> <br>
              <input type="text" id="no" class="form-control">
            </div>
            <div class="my-3">
              <label for="pesan">Pesan</label> <br>
              <textarea type="text" id="pesan" class="form-control" rows="8"> </textarea>
            </div>
          </div>
        </div>
        <div class="mt-3">
          <label class="wrap color--greyopt">
            Saya menyatakan bahwa data di atas adalah benar dan menyetujui <a class="link-text" href="/kebijakan-privasi.html">Pernyataan Privasi</a> dalam situs web ini.
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label>

          <label class="wrap color--greyopt">
            Saya bersedia menerima penawaran dan informasi terkait produk - produk Danone bai melalui telepon, SMS/email, dan/atau media lainnya, sepanjang diizinkan oleh peraturan yang berlaku.
            <input type="checkbox">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="float--right" id="submit-hubungi">
          <button class="btn btn--hubungi float-right">Kirim</button>
        </div>
      </div>
    </div>
  </main>
