<?= $this->extend('@SurvivalkitBundle/Resources/views/Layout/layout.html.php'); ?>
<?php
//perpage
$this->headLink()->offsetSetStylesheet(4,'/assets/assets/css/pages/sitemap.css');
?>
<main class="pernyataan-cookie">
    <div class="bg--greyinput">
        <div class="container-medium py-5">
            <h1 class="static--title"><?= $this->document->getTitle(); ?></h1>
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <?= $this->area('survivalkit-page-content', ['type' => 'survivalkit-page-content']); ?>
                </div>
            </div>
        </div>
    </div>
</main>

