<?= $this->extend('@SurvivalkitBundle/Resources/views/Layout/layout.html.php'); ?>
<?php
    //perpage
	$this->headLink()->offsetSetStylesheet(4,'/assets/assets/css/pages/sitemap.css');
?>
    <main class="push-right ">

        <!-- SURVIVAL KIT-->
        <section class="section --start">
            <div class="heading --background">
                <h2 class="heading__title">We Are by Your Side, as Committed as Ever</h2>
            </div>
            <div class="heading">
                <h2 class="heading__title pt-3">Webinar</h2>
                <div class="container-medium _new heading__subtitle pb-3">
                    <p class="heading__subtitle">
                    Kesempatan untuk mengikuti seminar online dengan topik terkini dan relevan untuk mendukung praktik klinis sehari-hari
                    </p>
                </div>
            </div>
            <div class="container-medium _new">
                <div class="survival-slider">
                <?php foreach($webminar as $web){?>

                    <div class="col-12 col-md-4">
                        <!-- link be here -->
                        <a href="<?= $this->path('SURVIVALKIT_WEBMINAR', ['slug' => $web->getSlug()]); ?>">
                        <div class="card">
                            <div class="card__picture">
                                <img src="<?= $web->getImageDesktop(); ?>">
                            </div>
                            <div class="card__content">
                                <span class="card__meta"><?= date('d F Y', $web->getCreationDate()) ?></span>
                                <h3 class="card__title"><?= $web->getTitle(); ?></h3>
                                <p class="card__desc"><?= implode(' ', array_slice(explode(' ', $web->getContent()), 0, 10)); ?></p>
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="author">
                                        <img class="author__avatar" src="<?= $web->getAvatar(); ?>">
                                        <div class="author__content">
                                            <p class="author__name"><?= $web->getName(); ?></p>
                                            <p class="author__title"><?= $web->getPosition(); ?></p>
                                        </div>
                                    </div>
                                    <span class="card__link">Lihat Detail</span>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                <?php }?>
                </div>
            </div>
        </section>
        <!-- END SURVIVAL KIT-->

        <!-- JURNAL-->
        <section class="section --jurnal">
            <div class="heading">
                <h2 class="heading__title">Jurnal Online</h2>
                <p class="heading__subtitle pb-3">
                Kumpulan jurnal ilmiah terpilih dari seluruh dunia yang sudah kami kurasi untuk memudahkan Anda dalam mencari informasi
                </p>
            </div>

            <div class="container-medium _new">
                <div class="row">
                <?php foreach($journal as $j){?>
                    <div class="col-6 col-md-3">
                        <div class="card --jurnal">
                            <img src="<?= $j->getImageDesktop();?>" class="card__img" alt="">
                            <div class="card__content">
                                <h3 class="card__title"><?= $j->getTitle();?></h3>
                                <p class="card__author"><?= $j->getName();?></p>
                                <p class="card__meta"><?= $j->getPosition();?></p>
                                <a href="<?= $this->path('SURVIVALKIT_JOURNAL_DETAIL', ['slug' => $j->getSlug()]); ?>" class="card__link">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                <?php }?>
                </div>

                <div class="text-center mt-20">
                    <a href="/survivalkit/jurnal-online" class="btn --large --ghost-blue">Lihat Index Jurnal</a>
                </div>
            </div>
        </section>

        <!-- END JURNAL -->

        <!-- ARTIKEL -->
        <section class="section">
            <div class="heading --single">
                <h2 class="heading__title">Artikel</h2>
                <p class="heading__subtitle pb-3">Informasi mengenai perkembangan ilmu pengetahuan terkini</p>
            </div>

            <div class="container-medium _new">
                <div class="row">
                    <?php foreach($articles as $article){?>
                    <div class="col-12 col-md-4">
                        <div class="card">
                            <div class="card__picture">
                                <img src="<?= $article->getImageDesktop();?>">
                            </div>
                            <div class="card__content">
                                <span class="card__meta jurnal"><?= $article->getCategory()->getTitle();?></span>
                                <h3 class="card__title --article"><?= $article->getTitle()?></h3>

                                <a href="<?= $this->path('SURVIVALKIT_ARTICLE_DETAIL', ['category' => $article->getCategory()->getSlug(),'slug' => $article->getSlug()]); ?>" class="btn --violet">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>

                <div class="text-center mt-20">
                    <a href="/survivalkit/artikel" class="btn --large --ghost-blue">Lihat Artikel Lainnnya</a>
                </div>
            </div>
        </section>
        <!-- END ARTIKEL-->

        <!-- new design no need this  -->
<!-- <section id="call-center">
<div class="container-medium">
<div class="wrap-callcenter">
<div class="row">
<div class="col-md-6">
<h3 class="isMobile">
Mama dan Papa memiliki pertanyaan lebih lanjut?
</h3>
<div class="callcenter-img">
<img src="./assets/img/image-contact.png" alt="call center">
</div>
</div>
<div class="col-md-6">
<div class="wrap-callcenter-content">
<div class="top-callcenter-content">
<h3 class="isDesktop">
Mama dan Papa memiliki pertanyaan lebih
lanjut?
</h3>
<h5>
Hubungi Tim Careline Nutricia yang siap
menjawab pertanyaan Mama dan Papa.
</h5>
</div>
<ul class="bottom-callcenter-content">
<li class="flex">
<div class="icon-callcenter">
<img src="./assets/img/phone.png" alt="">
</div>
<div class="hotline">
<h5>Call Careline (Bebas Biaya)</h5>
<h3>0800-1360360</h3>
</div>
</li>
<li class="flex">
<div class="icon-callcenter">
<img src="./assets/img/whatsapp.png" alt="">
</div>
<div class="hotline">
<h5>Whatsapp Careline</h5>
<h3>0821-2000100</h3>
</div>
</li>
<li class="flex">
<div class="icon-callcenter">
<img src="./assets/img/email.png" alt="">
</div>
<div class="hotline">
<h3>Tanya Nutriclub Careline</h3>
<h5>Respon selambatnya 1x24 jam</h5>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</section> -->


</main>
