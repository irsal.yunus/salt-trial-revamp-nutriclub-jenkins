<?php
// use AppBundle\Helper\TitleHelper;
?>
<section id="home" class="push-right">
    <div class="home-top">
        <div class="container">
            <h5>Home</h5>
        </div>
    </div>
    <div class="container-medium _new">
        <div class="home-lists">
        <?php $no = 1;?>
        <?php while($this->block("contentblock")->loop()) { ?>
            <div class="item-list" data-toggle-content="list<?= $no;?>">
                <button class="btn-list flex-center">
                    <?= $this->input("title"); ?>
                    <span><i class="ion-ios-arrow-down"></i></span>
                </button>
                <div class="list-dropdown" id="list<?= $no;?>">
                    <?= $this->wysiwyg("content"); ?>
                </div>
            </div>
            <?php $no++; ?>
        <?php } ?>
        </div>
    </div>
</section>
<footer class="push-right" id="footer">
    <div class="top-footer">
        <div class="top-footer-center">
        </div>
    </div>
    <!-- top footer -->
    <div class="middle-footer">
        <div class="container">
            <!-- social media -->
            <div class="d-sm-flex justify-content-center link-footer text-center">
            <?php while ($this->block("linkblock")->loop()): ?>
                    <?= $this->link("footerLink"); ?>
            <?php endwhile; ?>
            </div>
        </div>
    </div>
    <!-- middle footer -->
    <div class="bottom-footer">
        <p>2020 Nutricia Indonesia. All rights reserved</p>
    </div>
</footer>
