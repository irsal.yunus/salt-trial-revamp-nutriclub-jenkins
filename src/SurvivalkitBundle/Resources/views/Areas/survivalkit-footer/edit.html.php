<?php
/**
 * Created by PhpStorm.
 * User: Saldri Andika Putra <saldriandika@gmail.com>
 * Date: 10/01/2020
 * Time: 14:19
 */
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<h4>Footer Tab</h4>

<?php $no = 1;?>
<?php while($this->block("contentblock")->loop()) { ?>
    <h4>Title <?= $no; ?></h4>
    <?= $this->input("title",[
            'htmlspecialchars'  => false,
            'placeholder'       => 'Title',
    ]); ?>

    <h4>Content <?= $no; ?></h4>
    <?= $this->wysiwyg("content"); ?>

    <?php $no++; ?>
<?php } ?>

<h4>Footer Links</h4>
<ul>
    <?php while ($this->block("linkblock")->loop()): ?>
        <li>
            <?= $this->link("footerLink"); ?>
        </li>
    <?php endwhile; ?>
</ul>
