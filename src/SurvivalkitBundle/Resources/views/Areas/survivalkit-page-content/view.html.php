<?php
/**
 * nutriclub
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource view.html.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author ario
 * @since 19/08/20 07.13
 *
 *
 **/
echo $this->wysiwyg("content");
