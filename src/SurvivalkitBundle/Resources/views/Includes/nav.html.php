<?php
// use AppBundle\Helper\TitleHelper;
?>

<div class="push-right header_up bg-ungu pt-2 pb-2">
      <div class="container-medium">
          <div class="d-flex justify-content-between">
              <div class="fw-300">For Professional Healthcare</div>
              <div class="d-none d-md-flex align-items-center">
                <div><a href="https://www.nutriclub.co.id/membership" class="text-white">Register</a></div>
              </div>
          </div>
      </div>
  </div>
  <header class="push-right header no-shadow">
    <div class="container-medium d-flex justify-content-between align-items-center">
      <div class="d-md-none d-sm-block">
        <div class="header__menu login-navbar">
          <img src="/survivalkit/assets/img/menu-burger.png" alt="login" />
        </div>
      </div>
      <div class="header_img pb-3">
        <a href="/survivalkit">
          <img src="/survivalkit/assets/img/nutriexpert.png" class="header__logo" alt="logo" />
        </a>
      </div>
      <div class="d-md-none d-sm-block">
        <div class="search-">
        </div>
      </div>
      <div class="header_menu d-none d-md-flex align-items-center">
          <div><a href="/survivalkit"><span>Home</span></a></div>
          <div><span class="open-modal" data-href="">Webinar</span></div>
          <div><a href="/survivalkit/jurnal-online">Jurnal Online</a></div>
          <div><a href="/survivalkit/artikel"><span>Artikel</span></a></div>
          <!-- <div><a href="product.html">Produk</a></div> -->
          <div><span class="open-modal" data-href="https://old.nutriclub.co.id/products/">Produk</span></div>
      </div>
    </div>
  </header>

  <div class="navbar d-md-none d-sm-block">
    <div class="wrap-navbar flex-column">
      <ul class="w-100">
        <li class="li-close">
          <div class="new-closed" class="btn_white">
            <strong>MENU</strong>
            <i class="ion-ios-close-empty"></i>
          </div>
        </li>
        <li class="islink">
          <span class="open-modal" data-href="/survivalkit" rel="noreferrer">Home</span>
        </li>
        <li class="islink">
          <span class="open-modal" data-href="#" rel="noreferrer">Webinar</span>
        </li>
        <li class="islink">
          <span class="open-modal" data-href="/survivalkit/jurnal-online" rel="noreferrer">Jurnal Online</span>
        </li>
        <li class="islink">
          <span class="open-modal" data-href="/survivalkit/artikel" rel="noreferrer">Artikel</span>
        </li>
        <li class="islink">
          <!-- <a href="product.html" rel="noreferrer">Produk</a> -->
          <span class="open-modal" data-href="https://old.nutriclub.co.id/products/" rel="noreferrer">Produk</span>
        </li>
      </ul>
      <div class="d-block w-100 pb-2">
      <div class="d-flex justify-content-between p-3">
          <a href="https://www.nutriclub.co.id/membership" class=" btn-register" rel="noreferrer">Register</a>
          <a href="https://www.nutriclub.co.id/membership" class=" btn-login"  rel="noreferrer">Login</a>
      </div>
    </div>
    </div>
  </div>
