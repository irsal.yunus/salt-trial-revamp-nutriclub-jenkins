<!-- pagination -->
<div class="pagination mt-3">
  <ul>
    <li class="<?= (!isset($this->previous)) ? 'disabled' : ''; ?>"><a href="<?= $this->pimcoreUrl(['page' => $this->first]); ?>">Prev</a></li>
  
        <!-- Previous page link -->
        <li class="<?= (!isset($this->previous)) ? 'disabled' : ''; ?>"><a href="<?= $this->pimcoreUrl(['page' => $this->previous]); ?>"><img src="/survivalkit/assets/img/left.png"></a></li>
 
        <!-- Numbered page links -->
        <?php foreach ($this->pagesInRange as $page): ?>
            <?php if ($page != $this->current): ?>
                <li><a href="<?= $this->pimcoreUrl(['page' => $page]); ?>"><?= $page; ?></a></li>
            <?php else: ?>
                <li class="disabled"><a href="#"><?= $page; ?></a></li>
            <?php endif; ?>
        <?php endforeach; ?>
         
        <!-- Next page link -->
        <li class="<?= (!isset($this->next)) ? 'disabled' : ''; ?>"><a href="<?= $this->pimcoreUrl(['page' => $this->next]); ?>"><img src="/survivalkit/assets/img/right.png"></a></li>
         
        <!-- Last page link -->
        <li class="<?= (!isset($this->next)) ? 'disabled' : ''; ?>"><a href="<?= $this->pimcoreUrl(['page' => $this->last]); ?>">Last</a></li>
  </ul>
</div>
