<main class="detail-artikel">
    <div class="container-medium">
        <div class="hero__detail my-4">
            <img src="assets/img/bayi-big.png" alt="">
        </div>
        <div class="font--14 color--ungulight">
            Nutrisi
        </div>
        <div class="my-3 color--violet">
            <h1>15 Cara menjaga nutrisi Anak dengan cara yang paling menyenangkan menurut pakar</h1>
        </div>
        <div class="my-3 font--14">
            Share:
            <img class="cursor--pointer" src="assets/img/wa.png" alt="" width="30" height="30">
            <img class="cursor--pointer" src="assets/img/fb.png" alt="" width="30" height="30">
            <img class="cursor--pointer" src="assets/img/twit.png" alt="" width="30" height="30">
            <img class="cursor--pointer" src="assets/img/doc.png" alt="" width="30" height="30">
        </div>

        <!-- isi content -->
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, odit? Vel quis iure quaerat sint recusandae, natus provident ipsum distinctio in animi pariatur iste facere ea, ex, omnis enim fugit! Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati libero, tempore inventore at asperiores unde laudantium perferendis odio, enim provident minima eos delectus, quam veritatis quod totam corporis excepturi pariatur! Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque suscipit provident qui tenetur, aliquam expedita hic accusamus ab dolorem saepe quaerat obcaecati architecto iure molestiae quas nobis animi ut nulla.</p>
        <ol>
            <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quae necessitatibus nisi nulla, consequatur maxime explicabo labore quo recusandae dolorem in amet reiciendis, nesciunt, corrupti similique ea provident qui blanditiis libero.</li>
            <li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
            <li>Lorem ipsum dolor, sit amet</li>
        </ol>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas ab aliquam eum architecto placeat sed, quod ratione in eligendi deleniti ducimus recusandae. Consectetur laudantium alias saepe nisi cumque dolor harum? Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis assumenda eligendi, repellat quos magnam culpa debitis ipsam. Enim fugiat aspernatur debitis laboriosam nostrum, nihil cumque adipisci dicta eaque quidem itaque?</p>
        <!-- end isi content -->
        <hr>
        <h2 class="text-center color--violet">Artikel Terkait</h2>
        <div class="row">
            <div class="col-sm-12 col-md-4 mb-2">
                <a href="#" class="btn-card">
                <div class="cardy">
                  <div class="row">
                    <div class="col col-sm-6 col-md-12">
                      <img src="assets/img/bayi.png" alt="" />
                    </div>
                    <div class="col col-sm-6 col-md-12">
                        <label for="category" class="cardy--category">Nutrisi</label>
                        <h3 class="cardy--title">15 Cara menjaga nutrisi Anak dengan cara yang paling menyenangkan menurut pakar</h3>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-sm-12 col-md-4 mb-2">
                <a href="#" class="btn-card">
                <div class="cardy">
                  <div class="row">
                    <div class="col col-sm-6 col-md-12">
                      <img src="assets/img/anak.png" alt="" />
                    </div>
                    <div class="col col-sm-6 col-md-12">
                        <label for="category" class="cardy--category">Keluarga</label>
                        <h3 class="cardy--title">Keluarga sebagai pendidakan pertama bagi anak</h3>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-sm-12 col-md-4 mb-2">
                <a href="#" class="btn-card">
                <div class="cardy">
                  <div class="row">
                    <div class="col col-sm-6 col-md-12">
                      <img src="assets/img/psikologis.png" alt="" />
                    </div>
                    <div class="col col-sm-6 col-md-12">
                        <label for="category" class="cardy--category">Psikologis</label>
                        <h3 class="cardy--title">Perkembangan rasa empati dan kerjasama anak</h3>
                    </div>
                  </div>
                </div>
              </a>
            </div>
        </div>
    </div>
  </main>