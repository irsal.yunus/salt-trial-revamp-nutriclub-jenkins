<?= $this->extend('@SurvivalkitBundle/Resources/views/Layout/layout.html.php'); ?>
<?php
//perpage
$this->headLink()->offsetSetStylesheet(4,'/assets/assets/css/pages/sitemap.css');
?>
<main class="kebijakan">
    <div class="bg--greyinput">
        <div class="container-medium py-5">
            <h1 class="static--title">Pernyataan Privasi</h1>
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <!-- isi content -->
                    <p>Danone tahu bahwa Anda peduli bagaimana data pribadi Anda digunakan dan kami menyadari pentingnya melindungi privasi Anda.
                        Pernyataan Privasi ini menjelaskan bagaimana [“PT Nutricia Indonesia Sejahtera, PT Nutricia Medical Nutrition dan PT Sarihusada Generasi Mahardhika] (“Danone”) mengumpulkan dan mengelola data pribadi Anda melalui pendaftaran di www.Nutriexpert.co.id (alamat sementara) yang terafiliasi dengan email, social media, podcast di bawah brand nutriexpert.  Pernyataan ini berisi informasi tentang data apa yang kami kumpulkan, bagaimana kami menggunakannya, mengapa kami membutuhkannya .
                        Pernyataan privasi ini terakhir diperbarui pada [30 June 2020].</p>
                    <h2 class="static--subtitle">Prinsip dasar dan komitmen privasi kami</h2>
                    <p>Di Danone kami berkomitmen untuk melindungi hak Anda untuk privasi. Kami bertujuan melindungi data pribadi apa pun yang kami miliki, untuk mengelola data pribadi Anda secara bertanggung jawab dan transparan dalam praktik kami. Kepercayaan anda penting bagi kami. Karena itu kami berkomitmen pada prinsip-prinsip dasar berikut:</p>
                    <ul class="list--bullet">
                        <li><span>Anda tidak memiliki kewajiban untuk memberikan data pribadi yang diminta oleh kami. Namun, jika Anda memilih untuk tidak melakukannya, kami mungkin tidak dapat menyediakan beberapa layanan atau produk kepada Anda;</span></li>
                        <li><span>Kami hanya mengumpulkan dan memproses data Anda untuk tujuan yang ditetapkan dalam Pernyataan Privasi ini atau untuk tujuan tertentu yang kami bagikan dengan Anda dan/ atau yang telah Anda setujui;</span></li>
                        <li><span>Kami bertujuan untuk mengumpulkan, memproses, dan menggunakan sesedikit mungkin data pribadi;</span></li>
                        <li><span>Ketika kami mengumpulkan data pribadi Anda, kami bertujuan untuk menjaga data tersebut seakurat dan setepat mungkin.</span></li>
                        <li><span>Jika data pribadi yang kami kumpulkan tidak lagi diperlukan untuk tujuan apa pun dan kami tidak diharuskan oleh hukum untuk menyimpannya, kami akan melakukan apa yang kami bisa untuk menghapus, menghancurkan, atau tidak mengidentifikasi secara permanen.</span></li>
                        <li><span>Data pribadi Anda tidak akan dibagikan, dijual, disewakan, atau diungkapkan selain sebagaimana dijelaskan dalam Pernyataan Privasi ini.</span></li>
                    </ul>
                    <h2 class="static--subtitle">Siapa yang mengumpulkan?</h2>
                    <p>Setiap data pribadi yang diberikan kepada atau dikumpulkan oleh Danone dikendalikan oleh PT Nutricia Indonesia Sejahtera, PT Nutricia Medical Nutrition dan PT Sarihusada Generasi Mahardhika sebagai pengendali data.</p>
                    <p>Pernyataan Privasi ini berlaku untuk data pribadi yang dikumpulkan oleh Danone sehubungan dengan layanan dan informasi yang kami tawarkan, baik informasi terkait produk, informasi edukasi mengenai nutrisi dan kesehatan maupun informasi lain yang dapat meningkatkan serta mendorong kemajuan dalam bidang medis. Pernyataan ini juga berlaku untuk konten pemasaran Danone, termasuk informasi terkait produk dan layanan Danone, yang kami (atau penyedia layanan bertindak atas nama kami) kirimkan kepada Anda di situs web pihak ketiga, platform dan aplikasi berdasarkan informasi penggunaan situs Anda. Situs web pihak ketiga umumnya memiliki Pernyataan Privasi dan Syarat dan Ketentuan sendiri. Kami mendorong Anda untuk membacanya sebelum menggunakan situs web tersebut.</p>
                    <h2 class="static--subtitle">Data pribadi apa yang kami kumpulkan?</h2>
                    <p>Secara umum, kami dapat mengumpulkan jenis data pribadi berikut dari Anda secara langsung:</p>
                    <ul class="list--abjad">
                        <li>Data kontak pribadi, seperti nama Anda, alamat email, alamat fisik, tanggal lahir, nomor telepon; dan nomer keanggotaan di organisasi profesi</li>
                        <li>Detail login akun, seperti ID pengguna Anda, nama pengguna email dan kata sandi. Ini diperlukan untuk membuat akun pengguna pribadi di salah satu komunitas online kami</li>
                        <li>Komunikasi dengan kami, yang dapat mencakup perincian percakapan kami melalui obrolan dan/atau saluran layanan pelanggan</li>
                        <li>Informasi demografis, seperti usia dan jenis kelamin</li>
                        <li>Riwayat Danone support, seperti program edukasi yang telah diikuti, sertifikat kegiatan, foto kegiatan</li>
                        <li>Riwayat browser, seperti halaman yang diakses, tanggal akses, lokasi ketika diakses, dan alamat IP</li>
                        <li>Profil media sosial</li>
                        <li>Pendapat Anda terkait topik-topik nutrisi dan kesehatan; terkait produk kami dan penggunaannya; serta terkait program-program edukasi yang kami jalankan dan yang Anda butuhkan dalam rangka peningkatan kompetensi di bidang nutrisi dan kesehatan</li>
                    </ul>
                    <h2 class="static--subtitle">
                        Mengapa kami mengumpulkan dan menggunakan data pribadi Anda?
                    </h2>
                    <p>Kami mengumpulkan data pribadi Anda agar kami dapat memberi Anda pengalaman online terbaik dan memberikan Anda dukungan berupa informasi maupun program-program edukasi yang bermanfaat dan dapat meningkatkan serta mendorong kemajuan di bidang nutrisi dan kesehatan. Secara khusus kami dapat mengumpulkan, menyimpan, menggunakan, dan mengungkapkan data pribadi Anda untuk tujuan berikut:</p>
                    <ul class="list--abjad">
                        <li>Untuk memberikan informasi kepada Anda terkait topik-topik nutrisi dan kesehatan; terkait produk kami dan penggunaannya; serta terkait program-program edukasi yang kami jalankan dalam rangka peningkatan kompetensi di bidang nutrisi dan kesehatan</li>
                        <li>Untuk memproses dan menjawab pertanyaan Anda atau untuk menghubungi Anda untuk menjawab pertanyaan dan/atau permintaan Anda</li>
                        <li>Untuk mengembangkan dan meningkatkan produk, layanan, metode komunikasi, dan fungsionalitas situs web kami</li>
                        <li>Untuk mengelola pendaftaran Anda dan/atau berlangganan newsletter kami atau komunikasi lainnya</li>
                        <li>Untuk mengelola kebutuhan bisnis kami sehari-hari sehubungan dengan partisipasi Anda dalam program-program edukasi yang kami selenggarakan maupun respon Anda terhadap informasi yang kami berikan, baik terkait produk maupun informasi lain di bidang nutrisi dan kesehatan</li>
                        <li>Untuk mengotentikasi/memverifikasi identitas orang yang menghubungi kami melalui telepon, alat elektronik atau lainnya</li>
                        <li>Untuk pelatihan internal dan tujuan jaminan kualitas</li>
                        <li>Untuk memahami dan menilai minat, keinginan, dan perubahan kebutuhan di bidang nutrisi dan kesehatan, untuk meningkatkan situs web kami, produk dan program kami saat ini, dan/ atau mengembangkan produk dan program baru</li>
                        <li>Untuk menyediakan produk dan Program yang dipersonalisasi.</li>
                    </ul>
                    <p>
                        Informasi dan program-program yang kami maksud di sini merupakan informasi dan program yang dikembangkan untuk Tenaga Kesehatan dan sesuai dengan peraturan yang berlaku di Indonesia.</p>

                    <p>Kami juga mungkin memerlukan data pribadi Anda untuk mematuhi kewajiban hukum atau dalam konteks hubungan kontraktual yang kami miliki dengan Anda.</p>

                    <p>
                        Ketika kami mengumpulkan dan menggunakan data pribadi Anda untuk tujuan yang disebutkan di atas atau untuk tujuan lain, kami akan memberi tahu Anda sebelum atau pada saat pengumpulan.
                    </p>

                    <p>Apabila diperlukan, kami akan meminta persetujuan Anda untuk memproses data pribadi. Di mana Anda telah memberikan persetujuan untuk kegiatan pemrosesan, Anda memiliki hak untuk menarik persetujuan Anda kapan saja.</p>

                    <h2 class="static--subtitle">Hak Anda</h2>
                    <ul class="bottom-list">
                        <li>Hak untuk mengakses data pribadi Anda dan koreksi<br />
                            <ul>
                                <li>Anda memiliki hak untuk mengakses, memperbaiki, atau memperbarui data pribadi Anda kapan saja.</li>
                            </ul>
                        </li>
                        <li>Hak portabilitas data
                            <ul>
                                <li>Data pribadi Anda portabel. Ini berarti dapat dipindahkan, disalin, atau ditransmisikan secara elektronik. Namun, hak ini hanya berlaku jika:
                                    <ul>
                                        <li>Pemrosesan didasarkan pada persetujuan Anda</li>
                                        <li>Proses berlangsung untuk pelaksanaan kontrak</li>
                                        <li>Proses berlangsung dengan cara otomatis</li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>Hak untuk menghapus data pribadi Anda.
                            <ul>
                                <li>Anda berhak meminta kami menghapus data Anda jika:
                                    <ul>
                                        <li>Data pribadi Anda tidak lagi diperlukan sehubungan dengan tujuan yang kami kumpulkan</li>
                                        <li>Anda menarik persetujuan yang sebelumnya Anda berikan kepada kami untuk memproses data pribadi Anda, dan tidak ada dasar hukum lain untuk memproses data pribadi itu</li>
                                        <li>Anda keberatan jika kami memproses data pribadi Anda untuk tujuan yang disebutkan di atas</li>
                                        <li>Anda keberatan kami memproses data pribadi Anda untuk kepentingan sah Danone (seperti meningkatkan pengalaman pengguna secara keseluruhan di situs web)</li>
                                        <li>Data pribadi tidak diproses secara sah</li>
                                        <li>Data pribadi Anda perlu dihapus untuk mematuhi hukum.</li>
                                    </ul>
                                </li>
                                <li>Jika data pribadi yang kami kumpulkan tidak lagi diperlukan untuk tujuan apa pun dan kami tidak diharuskan oleh hukum untuk menyimpannya, kami akan melakukan apa yang kami bisa untuk menghapus, menghancurkan, atau tidak mengidentifikasi secara permanen.</li>
                            </ul>
                        </li>
                        <li>Hak atas pembatasan pemrosesan
                            <ul>
                                <li>Anda berhak membatasi pemrosesan data pribadi Anda jika :
                                    <ul>
                                        <li>Anda tidak percaya bahwa data pribadi yang kami miliki tentang Anda adalah akurat</li>
                                        <li>Data pribadi tidak diproses secara sah, tetapi alih-alih menghapus data pribadi, Anda lebih suka kami membatasi pemrosesan sebagai gantinya</li>
                                        <li>Kami tidak lagi membutuhkan data pribadi Anda untuk tujuan kami mengumpulkannya, tetapi Anda memerlukan data untuk membuat, melaksanakan atau mempertahankan klaim hukum</li>
                                        <li>Anda berkeberatan dengan pemrosesan data pribadi Anda dan sedang menunggu verifikasi apakah minat Anda yang terkait dengan keberatan tersebut lebih besar daripada alasan yang sah untuk memproses data Anda.</li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>Hak untuk menolak
                            <ul>
                                <li>Anda memiliki hak untuk menolak pemrosesan data pribadi Anda kapan saja sebelum pemrosesan data dilakukan.</li>
                            </ul>
                        </li>
                        <li>Bagaimana kami melindungi data pribadi Anda
                            <ul>
                                <li>Kami memahami bahwa keamanan data pribadi Anda penting. Kami melakukan upaya terbaik kami untuk melindungi data pribadi Anda dari penyalahgunaan, gangguan, kehilangan, akses tidak sah, modifikasi, atau pengungkapan. Kami telah menerapkan sejumlah langkah keamanan untuk membantu melindungi data pribadi Anda. Misalnya, kami menerapkan kontrol akses, menggunakan firewall dan server yang aman, dan kami mengenkripsi data pribadi.</li>
                            </ul>
                        </li>
                        <li>Berbagi data pribadi Anda
                            <ul>
                                <li>Saat kami membagikan data pribadi Anda dengan afiliasi dan organisasi lain, kami memastikan kami hanya melakukannya dengan organisasi yang melindungi dan melindungi data pribadi Anda dan mematuhi undang-undang privasi yang berlaku dengan cara yang sama atau serupa yang kami lakukan.</li>
                                <li>Data pribadi Anda tidak akan dibagikan, dijual, disewakan, atau diungkapkan selain sebagaimana dijelaskan dalam Pernyataan Privasi ini. Namun, kami dapat membagikan data Anda untuk keperluan hukum dan/atau saat diminta otoritas pemerintah.</li>
                            </ul>
                        </li>
                        <li>Berbagi data secara internasional
                            <ul>
                                <li>Data pribadi dapat diproses di luar wilayah Republik Indonesia. Saat diproses di luar wilayah Republik Indonesia, Danone akan memastikan bahwa pemrosesan data lintas batas ini dilindungi oleh pengamanan yang memadai.</li>
                            </ul>
                        </li>
                        <li>Pengambilan keputusan dan pembuatan profil otomatis
                            <ul>
                                <li>
                                    <p>Untuk beberapa layanan dan produk, kami dapat memproses data pribadi Anda menggunakan cara otomatis. Pada dasarnya ini berarti bahwa keputusan diambil secara otomatis tanpa campur tangan manusia.</p>
                                </li>
                                <li>
                                    <p>Kami juga dapat memproses data pribadi Anda untuk tujuan pembuatan profil guna memprediksi perilaku Anda di situs web</p>
                                </li>
                                <li>
                                    <p>Jika kami bermaksud menggunakan metode tersebut, kami tentu saja akan memberi tahu Anda dan kami akan memberi Anda kesempatan untuk menolak proses ini sebelumnya. Anda juga bebas untuk menghubungi kami untuk informasi lebih lanjut tentang pemrosesan tersebut.</p>
                                </li>
                            </ul>
                        </li>
                        <li>Cookie dan teknologi lainnya
                            <ul>
                                <li>Kami juga dapat mengumpulkan data pribadi tentang Anda melalui penggunaan cookie dan teknologi lainnya. Ini dapat terjadi ketika Anda mengunjungi situs kami atau situs pihak ketiga, melihat promosi online kami, atau menggunakan aplikasi seluler pihak ketiga kami dan mungkin termasuk informasi berikut:
                                    <ul>
                                        <li>Informasi tentang peramban perangkat dan sistem operasi Anda</li>
                                        <li>Alamat IP perangkat yang Anda gunakan</li>
                                        <li>Halaman web kami yang Anda lihat</li>
                                        <li>Tautan yang Anda klik saat interaktif dengan layanan kami</li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>Bagaimana cara menghubungi kami
                            <ul>
                                <li>Jika Anda memiliki pertanyaan, komentar atau keluhan mengenai Pernyataan Privasi ini atau pemrosesan data pribadi Anda, silakan hubungi kami melalui halaman kontak kami</li>
                            </ul>
                        </li>
                        <li>Bagaimana cara kami memperbarui Pernyataan ini?
                            <ul>
                                <li>Kami akan memperbarui Pernyataan Privasi ini bila perlu untuk memenuhi permintaan pelanggan dan menyesuaikan perubahan dalam produk dan layanan kami. Ketika kami memposting perubahan pada pernyataan ini, kami akan merevisi tanggal &ldquo;terakhir diperbarui&rdquo; di bagian atas Pemberitahuan ini. Jika perubahannya signifikan, kami akan memberikan pemberitahuan yang lebih menonjol (termasuk, untuk layanan tertentu, pemberitahuan email tentang perubahan Pemberitahuan Privasi).</li>
                            </ul>
                        </li>
                        <li>Ketentuan atau Pernyataan Privasi Tambahan
                            <ul>
                                <li>Selain Pernyataan Privasi ini, mungkin ada program edukasi yang akan diatur oleh persyaratan atau pernyataan privasi tambahan. Kami menyarankan Anda untuk membaca ketentuan atau pemberitahuan tambahan ini sebelum berpartisipasi dalam program edukasi apa pun karena Anda harus mematuhinya jika Anda ingin berpartisipasi. Ketentuan atau pernyataan privasi tambahan apa pun akan tersedia bagi Anda secara jelas selama periode program edukasi.</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</main>
