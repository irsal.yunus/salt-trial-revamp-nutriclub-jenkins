<?= $this->extend('@SurvivalkitBundle/Resources/views/Layout/layout.html.php'); ?>
<?php
//perpage
$this->headLink()->offsetSetStylesheet(4,'/assets/assets/css/pages/sitemap.css');
?>
<main class="pernyataan-cookie">
    <div class="bg--greyinput">
        <div class="container-medium py-5">
            <h1 class="static--title">Pernyataan Cookie</h1>
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <!-- isi content -->
                    <p>
                        Di Danone, kami ingin terbuka dan transparan dalam cara kami menggunakan cookie. Beberapa cookie yang kami gunakan sangat penting untuk fungsionalitas situs web kami, tetapi ada juga cookie yang dapat Anda pilih atau blokir.
                        Kami berusaha untuk memberikan Anda informasi yang cukup tentang cookie yang kami gunakan sehingga Anda dapat membuat pilihan berdasarkan informasi yang Anda bagikan kepada kami.
                        Jika Anda memiliki pertanyaan atau komentar lebih lanjut, jangan ragu untuk menghubungi kami melalui halaman <a class="link-text" href="/nutriexpert/hubungi-kami.html">kontak kami</a>.
                    </p>
                    <h2 class="static--title">Apa itu cookie?</h2>
                    <p>Cookie adalah file kecil yang disimpan di komputer atau perangkat seluler Anda. Ini memiliki ID unik yang ditetapkan untuk perangkat Anda dan memungkinkan situs web untuk mengingat tindakan dan preferensi Anda (seperti lokasi, bahasa, ukuran font, dan preferensi tampilan lainnya) selama periode waktu tertentu. Dengan begitu, Anda tidak harus terus memasukkan kembali informasi kapan pun Anda kembali ke situs web atau menjelajah dari satu halaman ke halaman lainnya. Cookie juga dapat membantu menyesuaikan pengalaman penelusuran Anda.</p>

                    <p>Tidak semua cookie berisi informasi pribadi tetapi jika ada, kami akan memperlakukan informasi ini sebagai data pribadi sesuai dengan Pernyataan Privasi kami [masukkan hyperlink ke pernyataan privasi]. Kami juga memperlakukan informasi cookie sebagai data pribadi yang terkait dengan akun Anda atau informasi kontak lainnya.
                    </p>

                    <h2 class="static--title">Jenis Cookie</h2>
                    <p>Ada dua kategori besar cookie: Cookie persisten dan Cookie sesi. Cookie tetap ada di perangkat Anda hingga dihapus secara manual atau otomatis. Cookie sesi tetap ada di perangkat Anda hingga Anda menutup browser dan dengan demikian secara otomatis dihapus.</p>

                    <h2 class="static--title">Mengapa kami menggunakan cookie?</h2>
                    <p>Secara umum, ada beberapa alasan mengapa kami dapat menggunakan cookie:</p>
                    <ul class="list--bullet">
                        <li>Cookie yang sangat diperlukan agar situs web berfungsi dan memungkinkan Anda untuk menggunakan layanan yang kami sediakan</li>
                        <li>Cookie yang mengumpulkan data terkait penggunaan situs web kami dan kinerjanya. Kami menggunakan informasi ini untuk meningkatkan produk dan layanan online kami</li>
                        <li>Cookie yang kami gunakan untuk mempersonalisasi dan mengoptimalkan pengalaman Anda di situs web kami. Cookies ini menyimpan informasi tentang pilihan Anda seperti ukuran teks, bahasa dan resolusi layar.
                        </li>
                        <li>Cookie yang ditempatkan oleh pihak ketiga untuk layanan yang ditingkatkan (seperti cookie media sosial) atau untuk tujuan iklan.
                        </li>
                        <li>Silakan lihat di bawah untuk informasi lebih lanjut tentang jenis cookie yang kami gunakan.</li>
                    </ul>

                    <h2 class="static--title">Cookie yang sangat diperlukan</h2>
                    <p>Ini adalah cookie yang kami butuhkan untuk dapat menawarkan kepada Anda situs web yang berfungsi dengan baik dan yang memungkinkan Anda untuk menggunakan layanan yang kami sediakan.
                        Jika cookie ini diblokir, Anda tidak akan dapat menggunakan situs web kami.</p>

                    <h2 class="static--title">Analisis dan cookie terkait kinerja</h2>
                    <p>Cookie ini membantu kami mengukur pola lalu lintas untuk menentukan area mana dari situs web kami yang telah dikunjungi. Kami menggunakan ini untuk meneliti kebiasaan pengunjung sehingga kami dapat meningkatkan produk dan layanan online kami. Kami dapat mencatat alamat IP (yaitu, alamat elektronik komputer yang terhubung ke internet) untuk menganalisis tren, mengelola situs web, melacak pergerakan pengguna, dan mengumpulkan informasi demografis yang luas.
                        Cookie ini tidak penting untuk situs web dan kami akan selalu meminta persetujuan Anda sebelum menempatkan cookie ini. Anda dapat menarik persetujuan Anda setiap saat. Lihat informasi lebih lanjut di bawah [hyperlink ke ‘Preferensi Anda’ di bawah].
                    </p>

                    <h2 class="static--title">Cookie untuk personalisasi </h2>
                    <p>Cookies ini memungkinkan kami untuk mengenali komputer Anda dan menyambut Anda setiap kali Anda mengunjungi situs web kami tanpa mengganggu Anda dengan permintaan untuk mendaftar atau untuk masuk.
                        Cookies ini tidak penting untuk situs web tetapi meningkatkan pengalaman untuk Anda. Kami akan selalu meminta persetujuan Anda sebelum menempatkan cookie ini. Anda dapat menarik persetujuan Anda setiap saat. Lihat informasi lebih lanjut di bawah.</p>

                    <h2 class="static--title">Cookie pihak ketiga</h2>
                    <p>Ini adalah cookie yang ditempatkan di situs web kami oleh pihak ketiga atas nama kami. Cookie memungkinkan kami untuk menghasilkan iklan yang dipersonalisasi di situs web lain, yang didasarkan pada informasi tentang Anda seperti tampilan halaman Anda di situs web kami. Situs web kami juga dapat menempatkan cookie untuk layanan pihak ketiga seperti media social.
                        Kami tidak memberikan informasi pribadi apa pun kepada pihak ketiga yang menempatkan cookie di situs web kami. Namun, pihak ketiga dapat berasumsi bahwa pengguna yang berinteraksi dengan atau mengklik iklan atau konten yang dipersonalisasi yang ditampilkan di situs web kami termasuk dalam grup yang mengarahkan iklan atau konten tersebut. Kami tidak memiliki akses ke atau kontrol atas cookie atau fitur lain yang dapat digunakan pengiklan dan situs pihak ketiga. Praktik informasi dari pihak ketiga ini tidak dicakup oleh Pernyataan Privasi atau Cookie kami. Silakan hubungi mereka secara langsung untuk informasi lebih lanjut tentang praktik privasi mereka.
                        Cookie ini tidak penting untuk situs web dan kami akan selalu meminta persetujuan Anda sebelum menempatkan cookie ini. Anda dapat menarik persetujuan Anda setiap saat. Lihat informasi lebih lanjut di bawah [tautan ke ‘Preferensi Anda’ di bawah].
                    </p>

                    <h2 class="static--title">Preferensi Anda</h2>
                    <p>Dimungkinkan untuk membatasi atau memblokir cookie atau bahkan menghapus cookie yang telah ditempatkan pada perangkat Anda. Jika Anda melakukannya, Anda mungkin tidak dapat mengakses bagian-bagian tertentu dari situs web kami dan kami mungkin berpikir bahwa Anda belum pernah mengunjungi kami sebelumnya dan karena itu akan menunjukkan kepada Anda Pemberitahuan Cookie lagi dan meminta persetujuan Anda dalam menempatkan cookie.
                        [Hanya sertakan, jika berlaku untuk situs web:] Situs web kami memiliki alat untuk membantu Anda mengelola cookie. Alat ini terletak di tautan “[Sisipkan nama tautan]” atau dengan mengklik di sini [masukkan hyperlink ke alat].
                        Anda juga dapat membatasi atau memblokir cookie dengan mengubah pengaturan browser Anda. Periksa informasi spesifik di bagian bantuan browser Anda untuk mengetahui bagaimana mengelola pengaturan cookie Anda.
                    </p>

                    <h2 class="static--title">Bagaimana cara menghubungi kami</h2>
                    <p>Jika Anda memiliki pertanyaan, komentar, atau keluhan terkait cookie, silakan hubungi kami melalui halaman kontak kami [masukkan hyperlink ke halaman kontak]. </p>

                </div>
            </div>
        </div>
    </div>
</main>
