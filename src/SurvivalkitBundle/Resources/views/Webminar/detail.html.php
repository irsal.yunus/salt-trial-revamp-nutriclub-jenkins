<?= $this->extend('@SurvivalkitBundle/Resources/views/Layout/layout.html.php'); ?>

<?php
//CSS
// $this->headLink()->offsetSetStylesheet(4, '/assets/assets/css/pages/detail-page.css');
//JS
// $this->headScript()->offsetSetFile(4, '/assets/assets/js/Widgets/banner-min.js');
?>

<?php

use Pimcore\Model\WebsiteSetting;

$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$url_segments = explode('/', $uri_path);
?>

<?php
/** @var Pimcore\Model\DataObject\Articles\Listing $article_detail */
if ($webminar_detail) {
    foreach ($webminar_detail as $detail) {
        
        $viewer = $detail->getTotalView();
        // increase viewer
        $detail->setTotalView((int)$viewer + 1);
        $detail->save();

        $host = \Pimcore\Tool::getHostUrl();

        $meta_url = $host . $uri_path;

        $this->headTitle()->append($detail->getTitle() ? $detail->getTitle() : "");
        $this->headMeta()->appendName('title', '');
        $this->headMeta()->appendName('description', '');
        $this->headMeta()->appendName('keyword', '');
        $this->headMeta()->appendName('author', '');

        $this->headMeta()->setProperty('og:type', 'article');
        $this->headMeta()->setProperty('og:url', '');
        $this->headMeta()->setProperty('og:title', '');

        // $content = $detail->getContent();
        // foreach ($content as $k => $v) {
        //     $this->headMeta()->setProperty('og:description', $v['bodyText']->getData() ? $TitleHelper->trim_text($v['bodyText']->getData(), 300) : "");
        // };

        $this->headMeta()->setProperty('og:image', '');
        $this->headMeta()->setProperty('twitter:card', 'summary');
        ?>
          <main class="push-right">
            <div class="hero">
              <div class="hero__content">
                <div>
                  <span class="hero__date"><?= date('d F Y', $detail->getCreationDate()) ?></span>
                  <h1 class="hero__title"><?= $detail->getTitle(); ?></h1>  
                  <div class="hero__time">
                    <span><i class="ion-ios-clock-outline"></i> 10:00 - 11:00</span>
                    <span>Durasi : 90 Menit</span>
                  </div> 
                  <div class="author --white">
                    <img class="author__avatar" src="<?= $detail->getAvatar(); ?>">
                    <div class="author__content">
                      <p class="author__name"><?= $detail->getName(); ?></p>
                      <p class="author__title"><?= $detail->getPosition(); ?></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="hero__overlay"></div>
            </div>
            
            <div class="counter" data-date="2020-05-01 10:00:00">
              <div class="counter__content">
                <div class="counter__time">
                  <span class="js-day">0</span>
                  <span>Hari</span>
                </div>
                <div class="counter__time">
                  <span class="js-hour">0</span>
                  <span>Jam</span>
                </div>
                <div class="counter__time">
                  <span class="js-minute">0</span>
                  <span>Menit</span>
                </div>
                <div class="counter__time">
                  <span class="js-second">0</span>
                  <span>Detik</span>
                </div>
              </div>
            </div>
            
            <!-- <section class="section">
              <div class="container-small text-center">
                <p class="f-18">Nutrisi merupakan hal terpenting dalam menjaga daya tahan tubuh. Temukan tipsnya di seminar ini</p>
                <button class="btn --large --blue mt-20" data-toggle="modal" data-target="#bookWebinar">Book Now</button>
              </div>
            </section> -->
            
            <!-- <section class="section --bluesky">
              <div class="heading --small">
                <h2 class="heading__title">Apa yang Mama dapatkan di Webinar kit ini?</h2>
                <p class="heading__subtitle">
                  Dengan mengikuti seminar ini, Mama bisa mendapatkan banyak informasi mengenai nutrisi apa saja yang harus dipenunhi agar daya tahan Adik tidak turun. Berikut keuntungan untuk Mama jika mengikuti : 
                </p>
              </div> -->
              <section class="section">
                <div class="container-small">
                <div class="heading --small">
                  <h2 class="heading__title">Apa yang Mama dapatkan di Webinar kit ini?</h2>
                  <p class="heading__subtitle">
                    Dalam webinar ini, mama akan mendapatkan informasi dan fakta menarik tentang bagaimana menjaga daya tahan tubuh si Kecil di masa pandemi ini yang akan dibahas oleh expert dari Nutricia
                  </p>
                  <a href="<?= !empty($detail->getLinkWebminar()) ? $detail->getLinkWebminar()->getHref() : ''; ?>"><button class="btn --large --blue mt-20" >Book Now</button></a>
                </div>
                </div>

              <!-- <div class="container-list">
                <ol class="list-number">
                  <li>Mama bisa mendapatkan informasi mengenai nutrisi yang akurat dari para expert kami</li>
                  <li>Mama juga bisa menanyakan secara langsung tentang nutrisi terbaik untuk Si Adik</li>
                  <li>Kami akan mengirimkan sertifikat sebagai tanda bukti Mama telah mengikuti seminar ini</li>
                </ol>
              </div> -->
            </section>
            
            <section class="section">
              <div class="container-small">
                <iframe src="https://www.youtube.com/embed/<?= !empty($detail->getYoutubeEmbed()) ? $detail->getYoutubeEmbed()->getData() : ''; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen frameborder="0" style="overflow:hidden;width:100%" height="350" width="100%"></iframe>
              </div>
            </section>

            <section class="section">
              <div class="container-small">
                <div class="author --large mb-20 d-flex align-items-center">
                  <img class="author__avatar" src="<?= $detail->getAvatar(); ?>" alt="">
                  <div class="author__content">
                    <p class="mb-0">Tentang Pembicara</p>
                    <p class="author__name"><?= $detail->getName(); ?></p>
                    <p class="author__title"><?= $detail->getPosition(); ?></p>
                  </div>
                </div>
                <?= $detail->getContent(); ?>
              </div>
            </section>

            <section class="section --book">
              <div class="container-small">
                <div class="heading --small --white">
                  <h3 class="heading__title">Yuk Mama, segera booking sekarang sebelum tempat habis</h3>
                  <p class="heading__subtitle">
                    Jangan lewatkan kesempatan ini untuk memberikan yang terbaik bagi si Adik.
                  </p>
                </div>
                <div class="text-center">
                  <a href="<?= !empty($detail->getLinkWebminar()) ? $detail->getLinkWebminar()->getHref() : ''; ?>"><button class="btn --large --white mt-20">Book Now</button></a>
                </div>
              </div>
            </section>
            
            <!-- Modal -->
            <div class="modal modal-iframe fade" id="bookWebinar" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <iframe src="https://my.webinarninja.com/embedding-form/2669" scrolling="no"></iframe>
                  </div>
                </div>
              </div>
            </div>
          </main> 
    <?php } ?>
<?php } ?>
<!-- END TAG -->
