<?php

namespace SurvivalkitBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Model\DataObject;
use SurvivalkitBundle\Services\ArticlesService;

class ArticleController extends FrontendController
{
    public function defaultAction(Request $request)
    {
    }

    public function indexAction(Request $request, ArticlesService $articles)
    {
        [$paginator, $catSlug] = $articles->getLandingArticles(); 

        $category = new DataObject\SurvivalkitCategoryArticles\Listing();
        $category->load();

        $this->view->paginator      = $paginator;
        $this->view->catSlug        = $catSlug;
        $this->view->category       = $category;
    }

    public function detailAction(Request $request, ArticlesService $articles)
    {
        [$article_detail, $article_terkait] = $articles->getDetailArticle(); 

        $this->view->article_terkait    = $article_terkait;
        $this->view->article_detail     = $article_detail;
    }
}
