<?php

namespace SurvivalkitBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Model\DataObject;
use SurvivalkitBundle\Services\WebminarService;

class WebminarController extends FrontendController
{
    public function defaultAction(Request $request)
    {
    }

    public function detailAction(Request $request, WebminarService $webminar)
    {
        $webminar_detail = $webminar->getDetailWebminar();
        $this->view->webminar_detail     = $webminar_detail;
    }

    public function showAction(Request $request)
    {
        $offset       = $request->get('offset');
        $limit        = $request->get('limit');

        $jurnal   = new DataObject\SurvivalkitJurnalOnline\Listing();
        $jurnal->setOrderKey('oo_id');
        $jurnal->setOrder("desc");
        $jurnal->setOffset($offset);
        $jurnal->setLimit($limit);
        $jurnal->load();

        return $this->render('@SurvivalkitBundle/Resources/views/JurnalOnline/load-more.html.php', ['jurnal' => $jurnal]);
    }
}
