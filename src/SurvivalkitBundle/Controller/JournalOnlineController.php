<?php

namespace SurvivalkitBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Model\DataObject;
use SurvivalkitBundle\Services\JournalOnlineService;

class JournalOnlineController extends FrontendController
{
    public function defaultAction(Request $request)
    {
    }

    public function indexAction(Request $request, JournalOnlineService $journal)
    {
    	$jurnal                 = $journal->getLandingJournal(8);
        $this->view->jurnal     = $jurnal;
    }

    public function detailAction(Request $request, JournalOnlineService $journal)
    {
        $jurnal_detail                  = $journal->getDetailJournal();
        $this->view->jurnal_detail      = $jurnal_detail;
    }

    public function showAction(Request $request)
    {
        $offset       = $request->get('offset');
        $limit        = $request->get('limit');

        $jurnal   = new DataObject\SurvivalkitJurnalOnline\Listing();
        $jurnal->setOrderKey('oo_id');
        $jurnal->setOrder("desc");
        $jurnal->setOffset($offset);
        $jurnal->setLimit($limit);
        $jurnal->load();

        return $this->render('@SurvivalkitBundle/Resources/views/JournalOnline/load-more.html.php', ['jurnal' => $jurnal]);
    }
}
