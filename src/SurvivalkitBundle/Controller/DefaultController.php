<?php

namespace SurvivalkitBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Model\DataObject;
use SurvivalkitBundle\Services\WebminarService;
use SurvivalkitBundle\Services\JournalOnlineService;
use SurvivalkitBundle\Services\ArticlesService;

class DefaultController extends FrontendController
{
    /**
     * @Route("/survivalkit")
     */
    public function indexAction(Request $request, WebminarService $webminar, JournalOnlineService $journal, ArticlesService $articles)
    {
        $articles   = $articles->getHomeArticles();
        $journal    = $journal->getHomeJournal();
        $webminar   = $webminar->getHomeWebminar();

        $this->view->articles   = $articles;
        $this->view->journal    = $journal;
        $this->view->webminar   = $webminar;
    }

    public function defaultAction(Request $request) {

    }
}
