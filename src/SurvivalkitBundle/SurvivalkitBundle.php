<?php

namespace SurvivalkitBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class SurvivalkitBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/survivalkit/js/pimcore/startup.js'
        ];
    }
    
}