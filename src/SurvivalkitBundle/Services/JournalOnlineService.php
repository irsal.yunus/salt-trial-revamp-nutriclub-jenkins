<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineFaqService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 30/07/20
 * @time 14.59
 *
 */

namespace SurvivalkitBundle\Services;

use Pimcore\Model\Document;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Pimcore\Model\DataObject;

class JournalOnlineService
{
    private $request;    

    public function __construct(
        RequestStack $requestStack        
    ) {
        $this->request = $requestStack->getCurrentRequest();
    }

  
    public function getHomeJournal()
    {
        $jurnal   = new DataObject\SurvivalkitJurnalOnline\Listing();
        $jurnal->setOrderKey('oo_id');
        $jurnal->setOrder("desc");
        $jurnal->setLimit(4);
        $jurnal->load();

        return $jurnal->getObjects();
    }

    public function getLandingJournal($limit = null)
    {
        $jurnal   = new DataObject\SurvivalkitJurnalOnline\Listing();
        $jurnal->setOrderKey('oo_id');
        $jurnal->setOrder("desc");
        $jurnal->setLimit($limit);
        $jurnal->load();

        return $jurnal->getObjects();
    }

    public function getDetailJournal()
    {
        $slug       = $this->request->get('slug');

        if(empty($slug)){
            return new RedirectResponse('/survivalkit', 302);
        }

        $jurnal_detail = new DataObject\SurvivalkitJurnalOnline\Listing();
        $jurnal_detail->setCondition("Slug = ?", [$slug]);
        $jurnal_detail->setLimit(1);
        $jurnal_detail->load();

        return $jurnal_detail->getObjects();
    }
}
