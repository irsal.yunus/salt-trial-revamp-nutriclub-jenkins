<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineFaqService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 30/07/20
 * @time 14.59
 *
 */

namespace SurvivalkitBundle\Services;

use Pimcore\Model\Document;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Pimcore\Model\DataObject;

class WebminarService
{
    private $request;    

    public function __construct(
        RequestStack $requestStack        
    ) {
        $this->request = $requestStack->getCurrentRequest();
    }

  
    public function getHomeWebminar()
    {
        $webminar   = new DataObject\SurvivalkitWebminar\Listing();
        $webminar->setOrderKey('oo_id');
        $webminar->setOrder("desc");
        $webminar->setLimit(4);
        $webminar->load();

        return $webminar->getObjects();
    }

    public function getDetailWebminar()
    {
        $slug       = $this->request->get('slug');        

        if(empty($slug)){
            return new RedirectResponse('/survivalkit', 302);
        }

        $webminar_detail = new DataObject\SurvivalkitWebminar\Listing();
        $webminar_detail->setCondition("Slug = ?", [$slug]);
        $webminar_detail->setLimit(1);
        $webminar_detail->load();

        return $webminar_detail->getObjects();
    }
}
