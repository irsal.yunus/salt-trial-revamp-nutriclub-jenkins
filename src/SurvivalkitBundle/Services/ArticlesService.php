<?php
/**
 * Nutriclub Revamp
 * PT. Ako Media Asia (https://salt.id/)
 * Copyright 2020
 *
 * Licensed under The MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource CarelineFaqService.php
 * @copyright Copyright 2020, PT. Ako Media Asia
 * @author yuliusardian
 * @since 30/07/20
 * @time 14.59
 *
 */

namespace SurvivalkitBundle\Services;

use Pimcore\Model\Document;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Pimcore\Model\DataObject;

class ArticlesService
{
    private $request;    

    public function __construct(
        RequestStack $requestStack        
    ) {
        $this->request = $requestStack->getCurrentRequest();
    }
  
    public function getHomeArticles()
    {
        $articles   = new DataObject\SurvivalkitArticles\Listing();
        $articles->setOrderKey('oo_id');
        $articles->setOrder("desc");
        $articles->setLimit(3);
        $articles->load();

        return $articles->getObjects();
    }

    public function getLandingArticles()
    {
        $catSlug = !empty($this->request->get('category')) ? $this->request->get('category') : 'all';

        if($catSlug != 'all'){
            $catId      = DataObject\SurvivalkitCategoryArticles::getBySlug($catSlug, 1);
            $articles   = new DataObject\SurvivalkitArticles\Listing();
            $articles->setCondition("Category__id = ?", [$catId->getId()]);
            $articles->setOrderKey('oo_id');
            $articles->setOrder("desc");
            $articles->setLimit(6);
            $articles->load();
        }else{
            $articles   = new DataObject\SurvivalkitArticles\Listing();
            $articles->setOrderKey('oo_id');
            $articles->setOrder("desc");
            $articles->setLimit(6);
            $articles->load();
        }

        $paginator = new \Zend\Paginator\Paginator($articles);
        $paginator->setCurrentPageNumber( $this->request->get('page') );
        $paginator->setItemCountPerPage(6);

        return [$paginator, $catSlug];
    }

    public function getDetailArticle()
    {
        $slug       = $this->request->get('slug');
        $catSlug    = $this->request->get('category');

        $catId = DataObject\SurvivalkitCategoryArticles::getBySlug($catSlug, 1);

        if(empty($catId)){
            return new RedirectResponse('/survivalkit', 302);
        }

        $article_detail = new DataObject\SurvivalkitArticles\Listing();
        $article_detail->setCondition("Category__id = ? AND Slug = ?", [$catId->getId(), $slug]);
        $article_detail->setLimit(1);
        $article_detail->load();

        $article_terkait = [];
        if($article_detail->getObjects()){
            $getCatId = $article_detail->getObjects()[0]->getCategory()->getId();
            $getArticleId = $article_detail->getObjects()[0]->getId();
            $article_terkait = new DataObject\SurvivalkitArticles\Listing();
            $article_terkait->setCondition("oo_id != ? AND Category__id = ?", [$getArticleId, $getCatId]);
            $article_terkait->setLimit(3);
            $article_terkait->load();
        }
        
        return [$article_detail->getObjects(), $article_terkait->getObjects()];
    }
}
