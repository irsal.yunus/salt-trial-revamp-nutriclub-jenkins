<?php 

return [
    1 => [
        "id" => 1,
        "name" => "NAV_TOP",
        "description" => "This property is  a flagging for which document should be shown in navigation",
        "key" => "NAV_TOP",
        "type" => "bool",
        "data" => "",
        "config" => "",
        "ctype" => "document",
        "inheritable" => FALSE,
        "creationDate" => 1582015095,
        "modificationDate" => 1582015214
    ],
    2 => [
        "id" => 2,
        "name" => "ADD_LOGO_AFTER_ME",
        "description" => "This property is a flagging for a navigation and should be add a logo after the navigation thats define this property",
        "key" => "ADD_LOGO_AFTER_ME",
        "type" => "bool",
        "data" => "",
        "config" => "",
        "ctype" => "document",
        "inheritable" => FALSE,
        "creationDate" => 1582015164,
        "modificationDate" => 1582015279
    ],
    3 => [
        "id" => 3,
        "name" => "NAV_BURGER",
        "description" => "This property is  a flagging for which document should be shown in burger menu",
        "key" => "NAV_BURGER",
        "type" => "bool",
        "data" => "",
        "config" => NULL,
        "ctype" => "document",
        "inheritable" => FALSE,
        "creationDate" => 1582018185,
        "modificationDate" => 1582018976
    ],
    4 => [
        "id" => 4,
        "name" => "IMAGE_ICON",
        "description" => "Icon for object in html",
        "key" => "IMAGE_ICON",
        "type" => "asset",
        "data" => NULL,
        "config" => NULL,
        "ctype" => "object",
        "inheritable" => FALSE,
        "creationDate" => 1582018994,
        "modificationDate" => 1582019021
    ],
    5 => [
        "id" => 5,
        "name" => "NAV_FOOTER",
        "description" => "This property is a flagging for which document should be shown in footer menu",
        "key" => "NAV_FOOTER",
        "type" => "bool",
        "data" => NULL,
        "config" => NULL,
        "ctype" => "document",
        "inheritable" => FALSE,
        "creationDate" => 1582197482,
        "modificationDate" => 1582197519
    ],
    6 => [
        "id" => 6,
        "name" => "NAV_NAME_BASED_ON_GENDER",
        "description" => "Update navigation name based on gender\n",
        "key" => "NAV_NAME_BASED_ON_GENDER",
        "type" => "bool",
        "data" => "",
        "config" => NULL,
        "ctype" => "document",
        "inheritable" => FALSE,
        "creationDate" => 1585191980,
        "modificationDate" => 1585192031
    ],
    7 => [
        "id" => 7,
        "name" => "CARELINE_NAV_BURGER",
        "description" => "Careline Navigation Bar Property",
        "key" => "CARELINE_NAV_BURGER",
        "type" => "bool",
        "data" => NULL,
        "config" => NULL,
        "ctype" => "document",
        "inheritable" => FALSE,
        "creationDate" => 1596583946,
        "modificationDate" => 1596583985
    ],
    8 => [
        "id" => 8,
        "name" => "MY_PREGNANCY_TODAY_NAV_BURGER",
        "description" => "",
        "key" => "MY_PREGNANCY_TODAY_NAV_BURGER",
        "type" => "bool",
        "data" => NULL,
        "config" => NULL,
        "ctype" => "document",
        "inheritable" => FALSE,
        "creationDate" => 1597141067,
        "modificationDate" => 1597141078
    ]
];
