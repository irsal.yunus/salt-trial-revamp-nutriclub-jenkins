<?php 

return [
    "default" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "default",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1582779674,
        "creationDate" => 1582779006,
        "id" => "default"
    ],
    "banner-slider-desktop" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "banner-slider-desktop",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1583919325,
        "creationDate" => 1583919319,
        "id" => "banner-slider-desktop"
    ],
    "banner-slider-mobile" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "banner-slider-mobile",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1583919334,
        "creationDate" => 1583919331,
        "id" => "banner-slider-mobile"
    ],
    "card-with-banner-medium-desktop" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-with-banner-medium-desktop",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1583920029,
        "creationDate" => 1583920026,
        "id" => "card-with-banner-medium-desktop"
    ],
    "card-with-banner-medium-mobile" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-with-banner-medium-mobile",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1583920038,
        "creationDate" => 1583920036,
        "id" => "card-with-banner-medium-mobile"
    ],
    "card-with-banner-small-desktop" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-with-banner-small-desktop",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1583920597,
        "creationDate" => 1583920594,
        "id" => "card-with-banner-small-desktop"
    ],
    "card-with-banner-small-mobile" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-with-banner-small-mobile",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1583920605,
        "creationDate" => 1583920602,
        "id" => "card-with-banner-small-mobile"
    ],
    "card-with-banner-small-image" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-with-banner-small-image",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1584207613,
        "creationDate" => 1584207610,
        "id" => "card-with-banner-small-image"
    ],
    "tool-list-icon" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "tool-list-icon",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1584207859,
        "creationDate" => 1584207856,
        "id" => "tool-list-icon"
    ],
    "card-slider-one-icon" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-slider-one-icon",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1584207864,
        "creationDate" => 1584207861,
        "id" => "card-slider-one-icon"
    ],
    "card-slider-three-icon" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-slider-three-icon",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1584207870,
        "creationDate" => 1584207868,
        "id" => "card-slider-three-icon"
    ],
    "website-logo" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => 120,
                    "height" => 41
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "website-logo",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1596433534,
        "creationDate" => 1584208421,
        "forcePictureTag" => FALSE,
        "id" => "website-logo"
    ],
    "card-with-banner-medium-version-two-banner-desktop" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-with-banner-medium-version-two-banner-desktop",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1584686144,
        "creationDate" => 1584686141,
        "id" => "card-with-banner-medium-version-two-banner-desktop"
    ],
    "banner-rounded-bottom-tool-desktop" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "banner-rounded-bottom-tool-desktop",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1584706719,
        "creationDate" => 1584706717,
        "id" => "banner-rounded-bottom-tool-desktop"
    ],
    "card-article-category-icon" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-article-category-icon",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1584707682,
        "creationDate" => 1584707679,
        "id" => "card-article-category-icon"
    ],
    "ebook-page" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "ebook-page",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1584935823,
        "creationDate" => 1584935819,
        "id" => "ebook-page"
    ],
    "card-postcast-list-desktop-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-postcast-list-desktop-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1585018449,
        "creationDate" => 1585018447,
        "id" => "card-postcast-list-desktop-thumbnail"
    ],
    "banner-slider-tablet" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "banner-slider-tablet",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1585722910,
        "creationDate" => 1585722906,
        "id" => "banner-slider-tablet"
    ],
    "ebook-detail-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "ebook-detail-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1586761675,
        "creationDate" => 1586761670,
        "id" => "ebook-detail-thumbnail"
    ],
    "ebook-detail-mobile-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "ebook-detail-mobile-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1586761846,
        "creationDate" => 1586761846,
        "id" => "ebook-detail-mobile-thumbnail"
    ],
    "banner-slider-rounded-bottom-banner-desktop-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "banner-slider-rounded-bottom-banner-desktop-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1587353847,
        "creationDate" => 1587353843,
        "forcePictureTag" => FALSE,
        "id" => "banner-slider-rounded-bottom-banner-desktop-thumbnail"
    ],
    "ebook-page-related" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "ebook-page-related",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1591330699,
        "creationDate" => 1591330689,
        "forcePictureTag" => FALSE,
        "id" => "ebook-page-related"
    ],
    "event-image-desktop-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "event-image-desktop-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1592128324,
        "creationDate" => 1592128317,
        "forcePictureTag" => FALSE,
        "id" => "event-image-desktop-thumbnail"
    ],
    "event-speaker-profile-picture-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "event-speaker-profile-picture-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1592130198,
        "creationDate" => 1592130193,
        "forcePictureTag" => FALSE,
        "id" => "event-speaker-profile-picture-thumbnail"
    ],
    "card-product-banner" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-product-banner",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1594961069,
        "creationDate" => 1594961069,
        "forcePictureTag" => FALSE,
        "id" => "card-product-banner"
    ],
    "card-product-group-banner" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-product-group-banner",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1594968973,
        "creationDate" => 1594967471,
        "forcePictureTag" => FALSE,
        "id" => "card-product-group-banner"
    ],
    "card-product-group-detail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-product-group-detail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1594974709,
        "creationDate" => 1594974454,
        "forcePictureTag" => FALSE,
        "id" => "card-product-group-detail"
    ],
    "content-product-image" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "content-product-image",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1595090947,
        "creationDate" => 1595090853,
        "forcePictureTag" => FALSE,
        "id" => "content-product-image"
    ],
    "banner-static-image" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "banner-static-image",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1595221941,
        "creationDate" => 1595221132,
        "forcePictureTag" => FALSE,
        "id" => "banner-static-image"
    ],
    "img-inner-tab" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "img-inner-tab",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1595239388,
        "creationDate" => 1595239283,
        "forcePictureTag" => FALSE,
        "id" => "img-inner-tab"
    ],
    "product-image" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "product-image",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1595247013,
        "creationDate" => 1595246987,
        "forcePictureTag" => FALSE,
        "id" => "product-image"
    ],
    "testimoni-avatar" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "testimoni-avatar",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1595289963,
        "creationDate" => 1595289901,
        "forcePictureTag" => FALSE,
        "id" => "testimoni-avatar"
    ],
    "product-group-small-banner-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "product-group-small-banner-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1595407761,
        "creationDate" => 1595407757,
        "forcePictureTag" => FALSE,
        "id" => "product-group-small-banner-thumbnail"
    ],
    "product-group-small-banner-mobile-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "product-group-small-banner-mobile-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1595407773,
        "creationDate" => 1595407771,
        "forcePictureTag" => FALSE,
        "id" => "product-group-small-banner-mobile-thumbnail"
    ],
    "product-flavor-image-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "product-flavor-image-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1595582718,
        "creationDate" => 1595582714,
        "forcePictureTag" => FALSE,
        "id" => "product-flavor-image-thumbnail"
    ],
    "banner-slider-version-two-desktop-thumbnail" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => 1366,
                    "height" => 400
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "banner-slider-version-two-desktop-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1596432675,
        "creationDate" => 1596432551,
        "forcePictureTag" => FALSE,
        "id" => "banner-slider-version-two-desktop-thumbnail"
    ],
    "banner-slider-version-two-mobile-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "banner-slider-version-two-mobile-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1596432597,
        "creationDate" => 1596432577,
        "forcePictureTag" => FALSE,
        "id" => "banner-slider-version-two-mobile-thumbnail"
    ],
    "card-slider-two-image-desktop-thumbnail" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => 270,
                    "height" => 140
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "card-slider-two-image-desktop-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1596432821,
        "creationDate" => 1596432774,
        "forcePictureTag" => FALSE,
        "id" => "card-slider-two-image-desktop-thumbnail"
    ],
    "card-banner-careline-image-circle-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-banner-careline-image-circle-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1596806232,
        "creationDate" => 1596806230,
        "forcePictureTag" => FALSE,
        "id" => "card-banner-careline-image-circle-thumbnail"
    ],
    "card-banner-careline-image-wa-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-banner-careline-image-wa-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1596806243,
        "creationDate" => 1596806241,
        "forcePictureTag" => FALSE,
        "id" => "card-banner-careline-image-wa-thumbnail"
    ],
    "card-banner-careline-image-phone-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "card-banner-careline-image-phone-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1596806262,
        "creationDate" => 1596806260,
        "forcePictureTag" => FALSE,
        "id" => "card-banner-careline-image-phone-thumbnail"
    ],
    "fetus-growth-image-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "fetus-growth-image-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1597017941,
        "creationDate" => 1597017941,
        "forcePictureTag" => FALSE,
        "id" => "fetus-growth-image-thumbnail"
    ],
    "nutrition-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "nutrition-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1597017961,
        "creationDate" => 1597017961,
        "forcePictureTag" => FALSE,
        "id" => "nutrition-thumbnail"
    ],
    "my-pregnancy-today-cover-mobile-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "my-pregnancy-today-cover-mobile-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1597018252,
        "creationDate" => 1597018252,
        "forcePictureTag" => FALSE,
        "id" => "my-pregnancy-today-cover-mobile-thumbnail"
    ],
    "my-pregnancy-today-cover-desktop-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "my-pregnancy-today-cover-desktop-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1597018277,
        "creationDate" => 1597018277,
        "forcePictureTag" => FALSE,
        "id" => "my-pregnancy-today-cover-desktop-thumbnail"
    ],
    "my-pregnancy-today-related-tools-desktop-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "my-pregnancy-today-related-tools-desktop-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1597026831,
        "creationDate" => 1597026831,
        "forcePictureTag" => FALSE,
        "id" => "my-pregnancy-today-related-tools-desktop-thumbnail"
    ],
    "my-pregnancy-today-related-tools-mobile-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "my-pregnancy-today-related-tools-mobile-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1597026862,
        "creationDate" => 1597026862,
        "forcePictureTag" => FALSE,
        "id" => "my-pregnancy-today-related-tools-mobile-thumbnail"
    ],
    "free-sample-mobile-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "free-sample-mobile-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1598430266,
        "creationDate" => 1598430243,
        "forcePictureTag" => FALSE,
        "id" => "free-sample-mobile-thumbnail"
    ],
    "free-sample-desktop-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "free-sample-desktop-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1598430280,
        "creationDate" => 1598430275,
        "forcePictureTag" => FALSE,
        "id" => "free-sample-desktop-thumbnail"
    ],
    "tier-logo-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "tier-logo-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1599201311,
        "creationDate" => 1599201311,
        "forcePictureTag" => FALSE,
        "id" => "tier-logo-thumbnail"
    ],
    "benefit-group-logo-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "benefit-group-logo-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1599201908,
        "creationDate" => 1599201908,
        "forcePictureTag" => FALSE,
        "id" => "benefit-group-logo-thumbnail"
    ],
    "partnership-logo-thumbnail" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "partnership-logo-thumbnail",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => NULL,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1599202225,
        "creationDate" => 1599202225,
        "forcePictureTag" => FALSE,
        "id" => "partnership-logo-thumbnail"
    ]
];
