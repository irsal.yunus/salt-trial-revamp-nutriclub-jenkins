<?php 

return [
    1 => [
        "id" => 1,
        "name" => "ARTICLE",
        "pattern" => "/\\/article-([\\w\\-]+)\\/?([\\w\\-]+)?\\/?([\\w\\-]+)?\\/?(.*)?\\/?/",
        "reverse" => "/article-%stageSlug/%articleCategorySlug/%articleSubCategorySlug/%articleSlug",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\ArticleController",
        "action" => "detail",
        "variables" => "stageSlug,articleCategorySlug,articleSubCategorySlug,articleSlug",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1578651833,
        "modificationDate" => 1584520942
    ],
    2 => [
        "id" => 2,
        "name" => "ARTICLE_PODCAST",
        "pattern" => "/\\/podcast-([\\w\\-]+)\\/?([\\w\\-]+)?\\/?([\\w\\-]+)?\\/?(.*)?\\/?/",
        "reverse" => "/podcast-%stageSlug/%articleCategorySlug/%articleSubCategorySlug/%articleSlug",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\PodcastController",
        "action" => "detail",
        "variables" => "stageSlug,articleCategorySlug,articleSubCategorySlug,articleSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1578976574,
        "modificationDate" => 1585038453
    ],
    3 => [
        "id" => 3,
        "name" => "ARTICLE_VIDEO",
        "pattern" => "/\\/video-([\\w\\-]+)\\/?([\\w\\-]+)?\\/?([\\w\\-]+)?\\/?(.*)?\\/?/",
        "reverse" => "/video-%stageSlug/%articleCategorySlug/%articleSubCategorySlug/%articleSlug",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\VideoController",
        "action" => "detail",
        "variables" => "stageSlug,articleCategorySlug,articleSubCategorySlug,articleSlug",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1578976621,
        "modificationDate" => 1586573804
    ],
    4 => [
        "id" => 4,
        "name" => "STAGE",
        "pattern" => "/\\/stage\\/?([\\w\\-\\.]+)?\\/?",
        "reverse" => "/stage/{%slug}",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\StageController",
        "action" => "default",
        "variables" => "slug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1580798081,
        "modificationDate" => 1596783293
    ],
    5 => [
        "id" => 5,
        "name" => "ARTICLE_CATEGORY",
        "pattern" => "/\\/article\\/category\\/?([\\w\\-\\.]+)?\\/?/",
        "reverse" => "/article/category/{%slug}",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\ArticleController",
        "action" => "category",
        "variables" => "slug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1580820347,
        "modificationDate" => 1596783980
    ],
    6 => [
        "id" => 6,
        "name" => "TOOL",
        "pattern" => "/\\/tool\\/(.*)/",
        "reverse" => "/tool/%slug",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\ToolController",
        "action" => "detail",
        "variables" => "slug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1580822076,
        "modificationDate" => 1584689733
    ],
    7 => [
        "id" => 7,
        "name" => "HOME_AFTER_LOGIN",
        "pattern" => "/\\/homelogin",
        "reverse" => "/homelogin",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\DefaultController",
        "action" => "login",
        "variables" => "",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1582203900,
        "modificationDate" => 1582203918
    ],
    8 => [
        "id" => 8,
        "name" => "ARTICLE_EBOOK",
        "pattern" => "/\\/ebook-([\\w\\-\\.]+)\\/?([\\w\\-\\.]+)?\\/?([\\w\\-\\.]+)?\\/?(.*)?\\/?/",
        "reverse" => "/ebook-%stageSlug/%articleCategorySlug/%articleSubCategorySlug/%articleSlug",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\EbookController",
        "action" => "detail",
        "variables" => "stageSlug,articleCategorySlug,articleSubCategorySlug,articleSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1586759460,
        "modificationDate" => 1597809972
    ],
    9 => [
        "id" => 9,
        "name" => "EVENT_THANKYOU",
        "pattern" => "/\\/event\\/?([\\w\\-\\.]+)?\\/?(thank-you)/",
        "reverse" => "/event/{%slug}/thank-you",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\EventController",
        "action" => "thankyou",
        "variables" => "slug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1592125969,
        "modificationDate" => 1597809962
    ],
    10 => [
        "id" => 10,
        "name" => "EVENT",
        "pattern" => "/\\/event\\/?([\\w\\-\\.]+)?\\/?/",
        "reverse" => "/event/{%slug}",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\EventController",
        "action" => "detail",
        "variables" => "slug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1592193350,
        "modificationDate" => 1597809954
    ],
    11 => [
        "id" => 11,
        "name" => "CARELINE_FAQ",
        "pattern" => "/\\/expert-advisor\\/faq-([\\w\\-\\.]+)/",
        "reverse" => "/expert-advisor/faq-%carelineFaqSlug",
        "module" => "CarelineBundle",
        "controller" => "@CarelineBundle\\Controller\\FaqController",
        "action" => "detail",
        "variables" => "carelineFaqSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1596095841,
        "modificationDate" => 1597809893
    ],
    12 => [
        "id" => 12,
        "name" => "CARELINE_SERVICE",
        "pattern" => "/\\/expert-advisor\\/team\\/(.*)/",
        "reverse" => "/expert-advisor/team/%carelineServiceTypeSlug",
        "module" => "CarelineBundle",
        "controller" => "@CarelineBundle\\Controller\\ServiceController",
        "action" => "detail",
        "variables" => "carelineServiceTypeSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1596099833,
        "modificationDate" => 1596695586
    ],
    13 => [
        "id" => 13,
        "name" => "CARELINE_EXPERT",
        "pattern" => "/\\/expert-advisor\\/expert\\/(.*)/",
        "reverse" => "/expert-advisor/expert/%carelineServiceExpertSlug",
        "module" => "CarelineBundle",
        "controller" => "@CarelineBundle\\Controller\\ExpertController",
        "action" => "detail",
        "variables" => "carelineServiceExpertSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1596101937,
        "modificationDate" => 1596695598
    ],
    14 => [
        "id" => 14,
        "name" => "PRODUCT_FREE_SAMPLE_CLAIM",
        "pattern" => "/\\/product\\/([\\w\\-\\.]+)\\/([\\w\\-\\.]+)\\/(free-sample)\\/(claim)\\/?\$/",
        "reverse" => "/product/%groupSlug/%productSlug/free-sample/claim",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\ProductController",
        "action" => "claim",
        "variables" => "groupSlug,productSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1595931164,
        "modificationDate" => 1597810095
    ],
    15 => [
        "id" => 15,
        "name" => "PRODUCT_FREE_SAMPLE_THANKYOU",
        "pattern" => "/\\/product\\/([\\w\\-\\.]+)\\/([\\w\\-\\.]+)\\/(free-sample)\\/(thank-you)\\/?\$/",
        "reverse" => "/product/%groupSlug/%productSlug/free-sample/thank-you",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\ProductController",
        "action" => "thankyou",
        "variables" => "groupSlug,productSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1596004235,
        "modificationDate" => 1597810107
    ],
    16 => [
        "id" => 16,
        "name" => "PRODUCT_FREE_SAMPLE",
        "pattern" => "/\\/product\\/([\\w\\-\\.]+)\\/([\\w\\-\\.]+)\\/(free-sample)\\/?\$/",
        "reverse" => "/product/%groupSlug/%productSlug/free-sample",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\ProductController",
        "action" => "sample",
        "variables" => "groupSlug,productSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1594720585,
        "modificationDate" => 1597810006
    ],
    17 => [
        "id" => 17,
        "name" => "PRODUCT",
        "pattern" => "/\\/product\\/([\\w\\-\\.]+)\\/([\\w\\-\\.]+)\\/?\$/",
        "reverse" => "/product/%groupSlug/{%productSlug}",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\ProductController",
        "action" => "detail",
        "variables" => "groupSlug,productSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1594720585,
        "modificationDate" => 1597809999
    ],
    18 => [
        "id" => 18,
        "name" => "PRODUCT_GROUP",
        "pattern" => "/\\/product\\/([\\w\\-\\.]+)\\/?\$/",
        "reverse" => "/product/{%groupSlug}",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\ProductController",
        "action" => "group",
        "variables" => "groupSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1594720111,
        "modificationDate" => 1597810079
    ],
    19 => [
        "id" => 19,
        "name" => "MAMA_SHOULD_KNOW",
        "pattern" => "/\\/my-pregnancy-today\\/mama-should-know\\/([\\w\\-\\.]+)\\/?\$/",
        "reverse" => "/my-pregnancy-today/mama-should-know/%mamaShouldKnowGroupSlug",
        "module" => "MyPregnancyTodayBundle",
        "controller" => "@MyPregnancyTodayBundle\\Controller\\KnowController",
        "action" => "detail",
        "variables" => "mamaShouldKnowGroupSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1597016396,
        "modificationDate" => 1597810021
    ],
    20 => [
        "id" => 20,
        "name" => "MAMA_SHOULD_DO",
        "pattern" => "/\\/my-pregnancy-today\\/mama-should-do\\/([\\w\\-\\.]+)\\/?\$/",
        "reverse" => "/my-pregnancy-today/mama-should-do/%mamaShouldDoGroupSlug",
        "module" => "MyPregnancyTodayBundle",
        "controller" => "@MyPregnancyTodayBundle\\Controller\\DoController",
        "action" => "detail",
        "variables" => "mamaShouldDoGroupSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1597016401,
        "modificationDate" => 1597810025
    ],
    21 => [
        "id" => 21,
        "name" => "MY_PREGNANCY_TODAY_RECIPE",
        "pattern" => "/\\/my-pregnancy-today\\/recipe-tips\\/([\\w\\-\\.]+)\\/?\$/",
        "reverse" => "/my-pregnancy-today/recipe-tips/%recipeSlug",
        "module" => "MyPregnancyTodayBundle",
        "controller" => "@MyPregnancyTodayBundle\\Controller\\RecipeController",
        "action" => "detail",
        "variables" => "recipeSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1597036678,
        "modificationDate" => 1597810029
    ],
    22 => [
        "id" => 22,
        "name" => "SURVIVALKIT_ARTICLE_DETAIL",
        "pattern" => "/\\/survivalkit\\/detail\\/(.*)\\/(.*)/",
        "reverse" => "/survivalkit/detail/%category/%slug",
        "module" => "SurvivalkitBundle",
        "controller" => "@SurvivalkitBundle\\Controller\\ArticleController",
        "action" => "detail",
        "variables" => "category,slug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1596686076,
        "modificationDate" => 1596686297
    ],
    23 => [
        "id" => 23,
        "name" => "SURVIVALKIT_JOURNAL_DETAIL",
        "pattern" => "/\\/survivalkit\\/jurnal-online\\/(.*)/",
        "reverse" => "/survivalkit/jurnal-online/%slug",
        "module" => "SurvivalkitBundle",
        "controller" => "@SurvivalkitBundle\\Controller\\JournalOnlineController",
        "action" => "detail",
        "variables" => "slug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1596694526,
        "modificationDate" => 1596699068
    ],
    24 => [
        "id" => 24,
        "name" => "SURVIVALKIT_ARTICLE",
        "pattern" => "/\\/survivalkit\\/artikel\\/(.*)/",
        "reverse" => "/survivalkit/artikel/%category",
        "module" => "SurvivalkitBundle",
        "controller" => "@SurvivalkitBundle\\Controller\\ArticleController",
        "action" => "index",
        "variables" => "category",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1596772259,
        "modificationDate" => 1596772339
    ],
    25 => [
        "id" => 25,
        "name" => "SURVIVALKIT_JOURNAL_SHOW",
        "pattern" => "/\\/survivalkit\\/jurnal\\/show/",
        "reverse" => "/survivalkit/jurnal/show",
        "module" => "SurvivalkitBundle",
        "controller" => "@SurvivalkitBundle\\Controller\\JournalOnlineController",
        "action" => "show",
        "variables" => NULL,
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1596786573,
        "modificationDate" => 1596786668
    ],
    26 => [
        "id" => 26,
        "name" => "SURVIVALKIT_WEBMINAR",
        "pattern" => "/\\/survivalkit\\/web-minar\\/(.*)/",
        "reverse" => "/survivalkit/web-minar/%slug",
        "module" => "SurvivalkitBundle",
        "controller" => "@SurvivalkitBundle\\Controller\\WebminarController",
        "action" => "detail",
        "variables" => "slug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1596791426,
        "modificationDate" => 1597219960
    ],
    27 => [
        "id" => 27,
        "name" => "MY_PREGNANCY_TODAY_ARTICLE",
        "pattern" => "/\\/my-pregnancy-today\\/related-article\\/([\\w\\-\\.]+)\\/?\$/",
        "reverse" => "/my-pregnancy-today/related-article/%articleSlug",
        "module" => "MyPregnancyTodayBundle",
        "controller" => "@MyPregnancyTodayBundle\\Controller\\ArticleController",
        "action" => "detail",
        "variables" => "articleSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1597131431,
        "modificationDate" => 1597131507
    ],
    28 => [
        "id" => 28,
        "name" => "ROYAL_REWARDS_KULWAP",
        "pattern" => "/\\/royal-rewards\\/kulwap\\/([\\w\\-\\.]+)\\/?\$/",
        "reverse" => "/royal-rewards/kulwap/%kulwapSlug",
        "module" => "RoyalRewardsBundle",
        "controller" => "@RoyalRewardsBundle\\Controller\\KulwapController",
        "action" => "detail",
        "variables" => "kulwapSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1599013903,
        "modificationDate" => 1599018231
    ],
    29 => [
        "id" => 29,
        "name" => "ROYAL_REWARDS_WEBINAR",
        "pattern" => "/\\/royal-rewards\\/webinar\\/([\\w\\-\\.]+)\\/?\$/",
        "reverse" => "/royal-rewards/webinar/%webinarSlug",
        "module" => "RoyalRewardsBundle",
        "controller" => "@RoyalRewardsBundle\\Controller\\WebinarController",
        "action" => "detail",
        "variables" => "webinarSlug",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "creationDate" => 1599095496,
        "modificationDate" => 1599096611
    ]
];
