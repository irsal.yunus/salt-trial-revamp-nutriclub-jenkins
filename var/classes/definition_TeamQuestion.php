<?php 

/** 
* Generated at: 2020-08-18T16:23:15+07:00
* Inheritance: no
* Variants: no
* IP: 172.19.0.1


Fields Summary: 
*/ 


return Pimcore\Model\DataObject\ClassDefinition::__set_state(array(
   'id' => '42',
   'name' => 'TeamQuestion',
   'description' => NULL,
   'creationDate' => NULL,
   'modificationDate' => 1597742595,
   'userOwner' => 2,
   'userModification' => NULL,
   'parentClass' => NULL,
   'implementsInterfaces' => NULL,
   'listingParentClass' => '',
   'useTraits' => '',
   'listingUseTraits' => '',
   'encryption' => false,
   'encryptedTables' => 
  array (
  ),
   'allowInherit' => false,
   'allowVariants' => false,
   'showVariants' => false,
   'layoutDefinitions' => NULL,
   'icon' => NULL,
   'previewUrl' => NULL,
   'group' => NULL,
   'showAppLoggerTab' => false,
   'linkGeneratorReference' => NULL,
   'propertyVisibility' => 
  array (
    'grid' => 
    array (
      'id' => true,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
    'search' => 
    array (
      'id' => true,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
  ),
   'dao' => NULL,
));
