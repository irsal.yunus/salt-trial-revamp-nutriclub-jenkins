function clickedButton(obj, buttonId, personaName) {
    Countly.q.push(['add_event',{
        key: 'BUTTON_CLICKED',
        "segmentation": {
            "id": obj.getAttribute('data-id'),
            "persona_name": personaName
        }
    }]);
}

function readActicle(obj) {
    Countly.q.push(['add_event', {
        key: 'READ_ARTICLE',
        'segmentation': {
            'id': obj.getAttribute('data-id'),
            'title': obj.getAttribute('data-title')
        }
    }]);
}

function clickBannerSlider(obj, bannerTitle) {
    Countly.q.push(['add_event', {
        key: 'BANNER_SLIDER_CLICK',
        'segmentation': {
            'title': bannerTitle
        }
    }]);
}

function clickStage(obj) {
    Countly.q.push(['add_event', {
        key: 'STAGE_CLICK',
        'segmentation': {
            'name': obj.getAttribute('data-name')
        }
    }]);
}

function clickArticleCategory(obj) {
    Countly.q.push(['add_event', {
        key: 'ARTICLE_CATEGORY_CLICK',
        'segmentation': {
            'name': obj.getAttribute('data-name')
        }
    }]);
}

// @todo should be updated on phase 2.
function clickTool(obj, toolName) {
    Countly.q.push(['add_event', {
        key: 'TOOL_CLICK',
        'segmentation': {
            'name': toolName
        }
    }]);
}

function clickPodcast(obj) {
    Countly.q.push(['add_event', {
        key: 'PODCAST_CLICK',
        'segmentation': {
            'slug': obj.getAttribute('data-slug'),
            'title': obj.getAttribute('data-title')
        }
    }]);
}