#!/usr/bin/env bash

baseDir="/var/www/app-nutri-jenkins/revamp"

echo "Change directory into : " ${baseDir}

cd ${baseDir}

echo "Update permission var/"

chmod 777 -R var/

echo "Pulling"

git pull

echo "Composer install if there is any update"

wget https://getcomposer.org/composer-stable.phar

mv composer-stable.phar composer.phar

chmod +x composer.phar

./composer.phar install  --no-interaction --optimize-autoloader