#!/usr/bin/env bash

baseDir="/var/www/app-nutriclub/revamp_p1"

echo "Change directory into : " ${baseDir}

cd ${baseDir}

php bin/console cache:clear -v

php bin/console pimcore:cache:clear

php bin/console pimcore:deployment:classes-rebuild -cv

php bin/console pimcore:migration:migrate -v

php bin/console assets:install --symlink web

chmod 777 -R var/

chmod 777 -R web/