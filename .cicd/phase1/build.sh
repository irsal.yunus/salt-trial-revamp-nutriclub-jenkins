#!/usr/bin/env bash

baseDir="/var/www/app-nutriclub/revamp_p1"

echo "Change directory into : " ${baseDir}

cd ${baseDir}

echo "Update permission var/"

chmod 777 -R var/

echo "Pulling"

git pull

echo "Composer install if there is any update"

composer install  --no-interaction --optimize-autoloader