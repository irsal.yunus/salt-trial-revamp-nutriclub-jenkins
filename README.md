# Nutriclub Revamp Personalization

This website was build using Pimcore 6 and every line of code made with love. 
If you are new in this project, so please keep it clean and readable 😉

## Table of contents
- [Requirements](#requirements)
- [Getting Started](#getting-started)
- [Notes For Frontend](#notes-for-frontend)
  - [Front End Structure](#front-end-structure)
  - [Environment](#environment)
  - [Custom Element](#custom-element)
  - [SASS](#sass)
  - [Fonts color](#fonts-color)
  - [Breakpoints](#breakpoints)
  - [Javascript](#javascript)
  - [General Issue](#general-issue)
- [Notes For Backend](#notes-for-backend)

## Requirements
This requirements only for development both for front-end and back-end

- Web Server :
  - Nginx > v.1.8
    - pagespeed module (optional)
    - brotli (optional)
  - Apache
    - mod_rewrite
    - .htaccess support (AllowOverride All)
    - pagespeed module (optional)
    - brotli (optional)    
- PHP >= 7.2
  - php-fpm
  - memory_limit = -1
  - upload_max_filesize and post_max_size >= 100M (depending on your data)
  - pdo_mysql or mysqli
  - iconv
  - dom
  - simplexml
  - gd
  - exif
  - file_info
  - mbstring
  - zlib
  - zip
  - intl
  - opcache
  - curl
  - [composer](https://getcomposer.org/)
- Database Server
  - MariaDB >= 10.0.0.5 (I do recommend = 10.3.*)
  - MySQL >= 5.6.4
  - AWS Aurora (MySQL)
  - Percona Server
- Node Js
  - Node Js Version > 12.XX

## Getting started 

Clone this project using `https` or `ssh`, I do recommend using `ssh` :

Using `https` :
```bash
$ git clone https://gitlab.com/salt-php/revamp-nutriclub/cms.git
``` 

Using `ssh` :
```bash
$ git clone git@gitlab.com:salt-php/revamp-nutriclub/cms.git
```

Copy `extensions.php.example` into `extensions.php`
```bash
$ cp -rv var/config/extensions.php.example var/config/extensions.php
```

Copy `parameters.example.yml` into `parameters.yml`
```bash
$ cp -rv app/config/parameters.example.yml app/config/parameters.example.yml
```

Copy `.env.example` into `.env`
```bash
$ cp -rv .env.example .env
```

Create database according to [Pimcore Official Documentation (DB Setup)](https://pimcore.com/docs/6.x/Development_Documentation/Installation_and_Upgrade/System_Setup_and_Hosting/DB_Setup.html)

Running composer install :
```bash
$ composer -vvv install
``` 

Install Pimcore and follow the instruction :

```bash
$ ./vendor/bin/pimcore-install
```

Copy database from staging into your local, ask lead or teammates for instruction.

Running SQL command for import SQL Function :
- Customer Management Framework Asset SQL Function :
```bash
$ mysql -uYourUserName -p --database=YourDatabase < vendor/pimcore/customer-management-framework-bundle/src/Resources/sql/segmentAssignment/storedFunctionAsset.sql
```
- Customer Management Framework Document SQL Function :
```bash
$ mysql -uYourUserName -p --database=YourDatabase < vendor/pimcore/customer-management-framework-bundle/src/Resources/sql/segmentAssignment/storedFunctionDocument.sql
```
- Customer Management Framework Object SQL Function :
```bash
$ mysql -uYourUserName -p --database=YourDatabase < vendor/pimcore/customer-management-framework-bundle/src/Resources/sql/segmentAssignment/storedFunctionObject.sql
```

Done, now if you are [Front-end follow this instruction](#notes-for-frontend) and if you are a [Back-end follow this instruction](#notes-for-backend)

## Notes for Frontend

### Front End Structure
```
|-- CMS
    |-- .gitignore
    |-- Web
        |-- assets
        |-- css
        |   |-- main.css
        |   |-- maps
        |   |   |-- main.css.map
        |   |   |-- pages
        |   |-- pages
        |   |-- vendor
        |       |-- bootstrap.css
        |       |-- font-awesome.min.css
        |       |-- slick-theme.css
        |       |-- slick.css
        |       |-- maps
        |           |-- bootstrap.css.map
        |           |-- font-awesome.min.css.map
        |           |-- slick-theme.css.map
        |           |-- slick.css.map
        |-- fonts
        |       |-- fontawesome-webfont.eot
        |       |-- fontawesome-webfont.svg
        |       |-- fontawesome-webfont.ttf
        |       |-- fontawesome-webfont.woff
        |       |-- fontawesome-webfont.woff2
        |       |-- FontAwesome.otf
        |       |-- glyphicons-halflings-regular.eot
        |       |-- glyphicons-halflings-regular.svg
        |       |-- glyphicons-halflings-regular.ttf
        |       |-- glyphicons-halflings-regular.woff
        |       |-- glyphicons-halflings-regular.woff2
        |-- images
        |-- js
        |    |-- app.min.js
        |-- helper
        |    |-- helper.js
        |-- includes
        |    |-- homepage.js
        |-- vendor
        |   |-- bootstrap.bundle.min.js
        |    |-- jquery.min.js
        |    |-- slick.min.js
        |
        |-- bundles
        |-- front-end
            |-- Core
            |   |-- .babelrc
            |   |-- .browserslistrc
            |   |-- gulpfile.js
            |   |-- package-lock.json
            |   |-- package.json
            |   |-- sass
            |   |   |-- main.scss
            |   |   |-- base
            |   |   |   |-- Fonts.scss
            |   |   |   |-- Helper.scss
            |   |   |   |-- Mixins.scss
            |   |   |   |-- Variables.scss
            |   |   |-- components
            |   |   |   |-- Buttons.scss
            |   |   |-- layout
            |   |   |   |-- Container.scss
            |   |   |   |-- Footer.scss
            |   |   |   |-- Header.scss
            |   |   |   |-- Navigation.scss
            |   |   |-- pages
            |   |   |   |-- Homepage.scss
            |   |   |-- Widgets
            |   |       |-- your-widget-name.scss
            |   |
            |   |-- script
            |   |   |-- main.js
            |   |   |-- helper
            |   |   |   |-- helper.js
            |   |   |-- includes
            |   |   |   |-- homepage.js
            |   |   |    |-- pagination.js
            |   |   |-- pages
            |   |   |    |-- your-pagesname.js
            |   |   |-- pages
            |   |   |   |-- your-pagesname.js
            |   |   |-- widgets
            |   |   |   |-- your-widgetsname.js
            |   |   |-- tracking
            |   |   |   |-- widgets
            |   |-- vendor
            |   |   |-- font-awesome
            |   |       |-- HELP-US-OUT.txt
            |   |       |-- css
            |   |       |   |-- font-awesome.css
            |   |       |   |-- font-awesome.min.css
            |   |       |-- fonts
            |   |       |   |-- fontawesome-webfont.eot
            |   |       |   |-- fontawesome-webfont.svg
            |   |       |   |-- fontawesome-webfont.ttf
            |   |       |   |-- fontawesome-webfont.woff
            |   |       |   |-- fontawesome-webfont.woff2
            |   |       |   |-- FontAwesome.otf
            |   |       |-- scss
            |   |           |-- font-awesome.scss
            |   |           |-- _animated.scss
            |   |           |-- _bordered-pulled.scss
            |   |           |-- _core.scss
            |   |           |-- _fixed-width.scss
            |   |           |-- _icons.scss
            |   |           |-- _larger.scss
            |   |           |-- _list.scss
            |   |           |-- _mixins.scss
            |   |           |-- _path.scss
            |   |           |-- _rotated-flipped.scss
            |   |           |-- _screen-reader.scss
            |   |           |-- _stacked.scss
            |   |           |-- _variables.scss
            |   |-- views (abandoned)
            |       |-- pages
            |       |   |-- about.html
            |       |   |-- index.html
            |       |-- shared
            |           |-- Footer.html
            |           |-- Header.html
 ```

After you clone the project and doing backend stuff 

- Run `npm install` at the `web/front-end/Core`
- Run `gulp` at the `web/front-end/Core`
- All then ouput when running `gulp` task are located at `web/assets`

## Environment
There are two kinds of env `gulp` running task, and the env is located at `web/front-end/Core/.env`
Choose `DEV` when you are the `local` and choose `PRODUCTION` when you are ready to commit the file

The use of dev and production are, when you are at `dev` the source maps will be rebuild, so you can detecting your tag style in the inspect element and `production` will be opossite for `dev` condition
so before you running gulp, make sure what kind of gulp that you want to use

### Custom Element
Custom your tag at `widgets` and `pages`
- For `widgets`it is located at
 ```
 |--src
    |--AppBundle
        |--Resources
           |--views
              |--Areas
                 |-- widgets folder here
                     |-- view.html.php 
 ```
- For `pages`it is located at
 ```
   |--app
      |--Resources
         |--views
            |--pages folder here
               |-- *.html.php (depend on where backend adjust it)
 ```
### SASS
- `base`, `components`, `layouts`, are imported in the `main.scss` file and the output of gulp would be imported in the 'layout.html.php'
- The mainly concern for front-end developer are ``pages`` and ``widgets`` 
- Use bem style naming for sass, you can see the [BEM docs](getbem.com/introduction/)
- Every Styles are located at`web/front-end/Core/sass/widgets` and `@import` it at `web/front-end/Core/sass/pages`

#### Notes!!!
```
Do not make any styles at pages folder, if you do you have to separated it into widgets at web/front-end/Core/sass/widgets
```
- At pimcore widgets / pages,  use type name of class div here
```html
<div class="your-widgets-name">
    <div class="your-widgets-name__header">XXX</div>
</div>
```
So at widgets we use styles like this
```scss
 .your-widgets-name{
    // your styles
    &__header{
    // your block header styles
    }
  }
```
- Find the ``pages  php ``folder that ``widgets`` already applied in the backend 
- Use this kind of `php script` to import style 
```php
<?php $this->headLink()->offsetSetStylesheet(indexNumber, 'css output path'); ?>
```
  - *indexNumber depend on ``layout.html.php`` if you use the number that are already applied, you have to use another index number, if you do, you gonna override another style at `layout.html.php`
  - *css output path is the output of pages css that are located at `web/assets/css/pages/your-css-pages`
### Fonts color
1. For fonts we use `GothamRounded` family, and you can use the fonts and colors configuration at `web/front-end/Core/sass/base/variables`
2. The used of font-family is.
```scss
.selector{
    font-family : "GothamRounded-Medium"
 }
```

### Breakpoints
1. We used several setup for media queries configuration starting from small device to bigger devices. The lists of devices is located at `web/front-end/Core/sass/base/variables`
2. The used of `breakpoint` is.
```scss
.selector{
 @include breakpoint(desktop){
        background-color : $bluesky-nutri;
 } 
}
```
### Javascript
- At javacript we can use ES5 or ES6, for customization we have to define the script whether the tag we are targeting is use for `widgets` or `pages`
- For the output of ``main.js`` it is located at ``layout.html.php``
- For `widgets` script, it is located at  `web/front-end/Core/script/widgets` and for `pages` script it is located at `web/front-end/Core/script/pages`.
- After running `gulp` task make sure your css or js output target at `web/assets/`
- Follow the name of widget and page that are already define in the backend
- Use this kind of `php script` to import javascript at `pages php folder`
```php
<?php $this->headScript()->offsetSetFile(indexNumber, 'js path folder', 'text/javascript',[]); ?>
```
- *indexNumber depend on ``layout.html.php`` if you use the number that are already applied, you have to use another index number, if you do, you gonna override another style at `layout.html.php`
- *js path folder is the output of pages js that are located at `web/assets/js/widgets/*js` or `web/assets/js/pages/*js`

### General Issue
- Sometimes when you are finish for styling, we can't see the changes. To resolved it you have to
   - Open the web in incognito mode
   - Clear cache with `Ctrl + Shift + R`
   - Remove the cache folder in the ``var/cache/*`` ( hardcode mode)
   
Done, if you have any question of this documentation, you can ask the front end lead or the front end team of this project. 
## Notes for Backend

It's mandatory for Backend to using `Docker`, but only for development.
Simply just running `docker-compose up` once the container up you can use
`docker-compose start` and `docker-compose stop` 

```bash
$ docker-compose up
```

Now you can access it at : 
- `localhost:4001`
- `localhost:4001/admin`

If you are having problem using `docker` or this is your first time using `docker`
then just ask to your lead or teammates that expert in `docker`
